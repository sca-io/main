function Rotativo(args) {	
    var me = this;
    var aux = 0;
    var timerIntv = false;
    var automatico = false;
    this.index = 0;
    this.instNome = args.instanceName;	
    this.conts = [];
    this.intervalo = args.intervalo;
    var tipoDisplay = "block";
    if(args.displayInline) tipoDisplay = "inline";
    this.selectImage = false;
    this.init = function(){	
        $(args.conteudos).each(function(i){
            me.conts[i] = $(this);	
        }).css('display','none');		
        if(args.fade){$(args.conteudos).css("position","absolute");}
        me.qntd = me.conts.length;
        if(me.conts[me.index]) me.conts[me.index].css('display',tipoDisplay);
        if(args.marcadores) me.changeMarcador();
        if(me.intervalo) me.initTimer();
        if(me.onInit) me.onInit();

        if(args.btn_anterior) $(args.btn_anterior).click(me.anterior);
        if(args.btn_proximo) $(args.btn_proximo).click(me.proximo);
    }
    this.initTimer = function(){
        automatico = true;	
        timerIntv = setInterval(function(){			
                me.proximo();		
        }, me.intervalo);	
    }
    this.stopTimer = function(){
        clearInterval(timerIntv);
    }
    this.changeMarcador = function(){		
        $(args.marcadores).each(function(i){			
            $(this).attr("class",$(this).attr("class").replace(" active",""));
            if(i == me.index){
                    $(this).attr("class",$(this).attr("class")+" active");
            }
        });
    }
    this.proximo = function(){
        if(me.index < me.qntd-1)    me.index++;
        else                        me.index = 0;
        if(me.selectImage)    me.selectImage(me.index)	
        else                  me.setIndex(me.index);		
    }
    this.anterior = function(){
        if(me.index > 0)        me.index--;
        else   		me.index = me.qntd-1;				
        if(me.selectImage)	me.selectImage(me.index)	
        else			me.setIndex(me.index);			
    }
    this.setIndex = function(indx){
        if(indx != aux){
            me.index = indx;
            if(args.fade){
                me.conts[me.index].fadeIn();							
                me.conts[aux].fadeOut();
            }else{
                me.conts[me.index].css('display',tipoDisplay);							
                me.conts[aux].css('display','none');				
            }		
            aux = me.index;
            if(args.marcadores){me.changeMarcador();}	
            if(!automatico && me.intervalo){
                me.stopTimer();
                me.initTimer();
            };
            if(me.onChange)	me.onChange();
        }
    }	
}
