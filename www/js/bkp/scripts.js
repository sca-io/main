var LNG_PT = 1;
var LNG_EN = 2;
function tradutor(pt,en){ return ((IDIOMA==LNG_PT)?pt:en); }
getTextoByLang = tradutor;
var popupAlerta = false;
function alerta(texto,onclose){
    var template = '<div class="pop-assine-nossa-newsletter pop-alert"><h2>'+tradutor('Atenção','Caution')+'</h2><p class="msg">'+texto+'<a href="javascript:;" class="bt-ok fechar-pop" title="OK">OK</a></p></div>';
    if(!popupAlerta){
        popupAlerta = new Popup({
            idPopup:"alertAtomica",			  
            bt_close:'.fechar-pop',
            mask:true,
            maskColor:'#000',
            Fixed:false,
            posRelScroll:true,
            fade:false
        });    
    }
    popupAlerta.open(template);
    if(onclose){popupAlerta.onClose = onclose;}
}
var popup = false;
function openPop(pagina){
    pagina = (pagina.indexOf('?')!=-1)?pagina+"&nocache="+Math.random():pagina+"?nocache="+Math.random();
    /*if(popup)
        popup.close();*/
    if(!popup){
        $.post(pagina, function(data){
            popup = new Popup({
                bt_close:'#btFechar',
                mask:true,
                Fixed:false,
                posRelScroll:true,//posicao relacionada ao Scroll 
                closeToEsc:true,
                fade:true
            });
            popup.open(data);
            popup.onClose = function(){
                popup = false;
            }
        });	
    }
}
function sapo_replace(strFind,newValue,str){
    while(str.indexOf(strFind)!=-1){
        str = str.replace(strFind,newValue); 
    } 
    return str;	
}
function inArray(obj,valor){
    for(var i=0;i<obj.length;i++) {
        if(obj[i] == valor) {
            return true;
        }
    }
    return false;
}
/*** VARIAVEIS DE VALIDACAO DE FORMULARIO ***/
var msg = Array();
var vld = Array();
var destino_frm = "";
var target_frm = "adm_miolo";
/********************************************/
 function geraQueryString(frm,encodar){
	var query = '';
	var cont = 0;
	for (var i = 0; i < frm.elements.length; i++) {
		if(frm.elements[i].type){
			if(encodar){
				var valor = encodeURIComponent(frm.elements[i].value);
			}else{
				var valor = frm.elements[i].value;
			}

			if(frm.elements[i].type.indexOf('text') == 0){
				query = query + '&' + frm.elements[i].name + '=' + valor;
				cont++;
			}else if(frm.elements[i].type.indexOf('checkbox')== 0){
				if(frm.elements[i].checked){
					query = query + '&' + frm.elements[i].name + '=true';
					cont++;
				}
			}else{
				query = query + '&' + frm.elements[i].name + '=' + valor;
				cont++;
			}
		}else{

		}
	}
	return (query);
}
function wopen(page,nwin,larg,altu, scroll){
    window.open(page,nwin,'width='+larg+',height='+altu+',toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars='+scroll+',resizable=no,menubar=no,top=80,left=100');
}
function pressedEnter(event){
    var keynum;       
    if(window.event) { //IE  
        keynum = event.keyCode  
    } else if(event.which) { // Netscape/Firefox/Opera AQUI ESTAVA O PEQUENINO ERRO ao invés de "e." é "event."  
        keynum = event.which  
    }  
    if( keynum==13 ) { /* 13 é o código do Enter */
        return true;		
    }else{
        return false;
    } 
}
function checarTecla(event,funcao){
    if(pressedEnter(event)){		
        funcao();
    }
}
function buscar(){
    var frm = document.getElementById('form_busca');
    if(!frm.busca.value){
        alerta(getTextoByLang("Preencha o campo de busca antes!","Fill in the search before!"));
        return false;
    }
    frm.submit();   
}
function filtarAssunto(){
    var form = document.getElementById("formFiltro");
    var assunto = document.getElementById("assunto");
    //if(assunto.value){
    var urlSelf = document.getElementById("urlSelf");
    document.location = urlSelf.value+assunto.value+"/";
    /*}else{
        alerta(getTextoByLang("Selecione um assunto antes!","Select a subject before"))
    }*/
}
function limparFiltroAssunto(){
    var form = document.getElementById("formFiltro");
    var urlSelf = document.getElementById("urlSelf");
    document.location = urlSelf.value;
}
function filtarResultado(){
    var form = document.getElementById("formFiltro");
    var urlAmig = document.getElementById("urlAmig");
    var order = document.getElementById("order");
    if(order.value){
        var urlSelf = document.getElementById("urlSelf");
        document.location = urlSelf.value+urlAmig.value+"/"+order.value+"/";
    }
}
function downloadDoc(idM){  if(idM) document.location = DIRETORIO_RAIZ+"download_media.php?idM="+idM    }
function notificarAguardeEnvio(){    alerta(getTextoByLang("Dados sendo enviados, por favor aguarde!","Data being sent, please wait!")); }
/*** GESTAO DE PESSOAS ***/
var enviandoForm = false;
function salvarCadGestaoPessoa(){
    if(enviandoForm){
        notificarAguardeEnvio();
        return;
    }
    enviandoForm = true;
    var frm = document.getElementById("formulario");
    msg['nome'] = $("#lbl-nome").html();
    msg['endereco'] = $("#lbl-endereco").html();
    msg['num'] = $("#lbl-num").html(); //vld['num'] = 3;  /* Número não é apenas numero, tem letra tbm*/
    msg['cidade'] = $("#lbl-cidade").html();
    msg['estado'] = $("#lbl-estado").html(); vld['estado'] = 10; 
    msg['ddd'] = "DDD";//$("#lbl-ddd").html()
    msg['telefone'] = $("#lbl-telefone").html(); vld['telefone'] = 4;	       
    msg['email'] = $("#lbl-email").html(); vld['email'] = 2;		
    msg['area'] = $("#lbl-area").html();
    msg['formacaoAcad'] = $("#lbl-formacao-acad").html();
    switch(IDIOMA){
        case LNG_PT:
            if(frm.formacaoAcad.value == FA_SUPERIOR_INCOM || frm.formacaoAcad.value == FA_SUPERIOR_COM || frm.formacaoAcad.value == FA_POS_GRAD){
                msg['curso'] = "Especifique o curso";    
            } 
        break; 
        case LNG_EN:
            if(frm.formacaoAcad.value == HIGHER_EDUCATION_INCOM || frm.formacaoAcad.value == HIGHER_EDUCATION_COM || frm.formacaoAcad.value == GRADUATE_STUDIES){
                msg['curso'] = "Specify the course";    
            }
        break;   
    }
    fSucess = function(url){
        var url = DIRETORIO_RAIZ+'controle_site.php?acao=salvarCadGestaoPessoa';
        $.post(url,$(frm).serialize(),function(retorno){
            enviandoForm = false;
            if(retorno == "sucesso"){
                document.location = DIRETORIO_RAIZ+"gestao-de-pessoas/sucesso/";
                limpaform(frm.id,"idioma");
                //popupAtomica.close();
            }else
            alerta(sapo_replace("<br>","\n",retorno))
        });
    }
    var func = function(strErro){enviandoForm = false;fErro(strErro)}
    validaFormulario(frm.id,fSucess,func)
}
/*** CONTATOS ***/
function salvarContato(){
    if(enviandoForm){
        notificarAguardeEnvio();
        return;
    }
    enviandoForm = true;
    var frm = document.getElementById("formulario");
    msg = [];
    vld = [];
    msg['nome'] = $("#lbl-nome").html();
    msg['email'] = $("#lbl-email").html(); vld['email'] = 2;	
    msg['cidade'] = $("#lbl-cidade").html();
    msg['uf'] = $("#lbl-uf").html().toString().replace(/&nbsp;/gi,""); vld['uf'] = 10;
    msg['ddd'] = "DDD";//$("#lbl-telefone").html()
    msg['telefone'] = $("#lbl-telefone").html(); vld['telefone'] = 4;	
    msg['assunto'] = $("#lbl-assunto").html();
    msg['mensagem'] = $("#lbl-mensagem").html();
    fSucess = function(url){	
        var url = 'controle_site.php?acao=salvarContato';
        $.post(url,$(frm).serialize(),function(retorno){
            enviandoForm = false;
            if(retorno == "sucesso"){
                switch(IDIOMA){
                        case LNG_PT:
                                alerta("Mensagem enviada com sucesso!");
                        break;
                        case LNG_EN:
                                alerta("Message sent successfully!");
                        break;		
                }
                limpaform(frm.id,"idioma");
                //popupAtomica.close();
            }else
                alerta(sapo_replace("<br>","\n",retorno))
        });
    }
    var func = function(strErro){enviandoForm = false;fErro(strErro)}
    validaFormulario($(frm).attr("id"),fSucess,func);
}
/***************/
function mostrarBoxCotacoes(){
     $("#box-mais-cotacoes").fadeIn();
     $("#cotacoes").css("opacity",0.3);
    $(function(){	
        $('#pane1').jScrollPane({
            showArrows:true, 
            scrollbarMargin:0
        });
    });
	 //setTimeout(function(){
	/*var $pane = $('.scroll-pane');
	$pane.jScrollPane({
			showArrows:false,
			animateTo:false,
			scrollbarWidth:17
			, scrollbarMargin:10
	});*/	
	// },2000);
}
function ocultarBoxCotacoes(){
    $("#box-mais-cotacoes").fadeOut()
    $("#cotacoes").css("opacity",1);
}
function showMsgNews(str){
    var frm = document.getElementById("formNews");
    $(frm).css("display","none"); 
    $(".msg").html(str);    
}
function hidenMsgNews(){
    var frm = document.getElementById("formNews");
    $(frm).css("display","block"); 
    $(".msg").html("");    
}
function salvarNews(){
    var frm = document.getElementById("formNews");
    msg = []; vld = [];
    msg['nome'] = $("#lbl-nome").html();
    msg['email'] = $("#lbl-email").html(); vld['email'] = 2;	
    var funcErrNews = function(strErro){
        var strVal = (sapo_replace(",","<br />",strErro))+" <a href='javascript:hidenMsgNews()'>Voltar</a>";
        switch(IDIOMA){
            case LNG_PT:showMsgNews("<strong>Preencha corretamente os campos:</strong><br/>"+strVal);break;
            case LNG_EN: showMsgNews("<strong>Fill out the fields:</strong><br/>"+strVal);break;		
        } 
    }
    fSucess = function(url){	
        var url = 'controle_site.php?acao=salvarNews';
        $.post(url,$(frm).serialize(),function(retorno){
            if(retorno == "sucesso"){
                switch(IDIOMA){
                    case LNG_PT:showMsgNews("<strong>Seu email foi cadastrado com sucesso!</strong>");break;
                    case LNG_EN: showMsgNews("<strong>Your email has been registered successfully!</strong>");break;		
                }                      
                limpaform(frm.id,"idioma");
            }else{
                showMsgNews(retorno)                    
            }
        });
    }
    validaFormulario($(frm).attr("id"),fSucess,funcErrNews);
}
function showMsgBox(idForm,idBox,str,tipo){
    switch(IDIOMA){
        case LNG_PT:txtVoltar = "Voltar";break;
        case LNG_EN:txtVoltar = "Back";break;
    } 
    if(tipo == 'erro-validacao'){   
        str = sapo_replace(",","<br />",str);
        switch(IDIOMA){
            case LNG_PT:str = "<strong>Preencha corretamente os campos:</strong><br />"+str;break;
            case LNG_EN: str = "<strong>Fill out the fields:</strong><br />"+str;break;		
        }
        $(idBox).css({"text-align":"left"});
        str += "<br /><a href='javascript:hidenMsgBox(\""+idForm+"\", \""+idBox+"\")'>"+txtVoltar+"</a>";       
    }else{
        $(idBox).css({"text-align":"center"});
        str += "<br /><a href='javascript:window.location.reload();'>"+txtVoltar+"</a>";    
    }
    
    $(idForm).css("display","none"); 
    $(idBox).css({"display":"block"}); 
    $(idBox).html(str);    
}
function hidenMsgBox(idForm,idBox){
    $(idForm).css("display","block"); 
    $(idBox).css("display","none"); 
    $(idBox).html("");    
}
function salvarCliente(){
    var frm = document.getElementById("fomCliente");
    msg = []; vld = [];
    msg['nome'] = $("#lbl-nome").html();
    msg['email'] = $("#lbl-email").html(); vld['email'] = 2;
    msg['mensagem'] = $("#lbl-mensagem").html();
    var funcErrNews = function(strErro){
        showMsgBox("#content-form","#box-msg",strErro,"erro-validacao");
    }
    fSucess = function(url){
        var url = DIRETORIO_RAIZ+'/controle_site.php?acao=salvarCliente';
        $.post(url,$(frm).serialize(),function(retorno){
            if(retorno == "sucesso"){
                switch(IDIOMA){
                    case LNG_PT:resp = "<strong>Sua mensagem foi enviada com sucesso!<br />Assim que possível, entraremos em contato.</strong>";break;
                    case LNG_EN: resp = "<strong>Your message was sent successfully! <br /> Soon as possible, we will contact you.</strong>";break;		
                }         
                showMsgBox("#content-form","#box-msg",resp,"sucesso");                
                //limpaform(frm.id,"idioma,tipoCliente");
            }else{   
                showMsgBox("#content-form","#box-msg",retorno,"erro");
            }
        });
    }
    validaFormulario($(frm).attr("id"),fSucess,funcErrNews);
}

function changeTextoTipoCliente(opt){
    $("#textos-tipo-cliente DIV").css("display","none");
    $("#texto-tipo-"+opt.value).css("display","block");    
}

