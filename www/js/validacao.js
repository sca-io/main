/*** ESTADOS ***/ 
var arrEstados = [];
arrEstados["AC"] = "Acre";// Rio Branco
arrEstados["AL"] = "Alagoas";// Maceió	
arrEstados["AP"] = "Amapá";// Macapá	
arrEstados["AM"] = "Amazonas";// Manaus	
arrEstados["BA"] = "Bahia";// Salvador
arrEstados["CE"] = "Ceará";// Fortaleza
arrEstados["DF"] = "Distrito Federal";// Brasilia
arrEstados["ES"] = "Espírito Santo";// Vitória
arrEstados["GO"] = "Goiás";// Gaiânia
arrEstados["MA"] = "Maranhão";// São Luiz
arrEstados["MT"] = "Mato Grosso";// Cuiabá
arrEstados["MS"] = "Mato Grosso do Sul";// Campo Grande
arrEstados["MG"] = "Minas Gerais";// Belo Horizonte
arrEstados["PA"] = "Pará";// Belém
arrEstados["PB"] = "Paraíba";// João Pessoa
arrEstados["PR"] = "Paraná";// Curitiba
arrEstados["PE"] = "Pernambuco";// Recife
arrEstados["PI"] = "Piauí";// Terezina
arrEstados["RJ"] = "Rio de Janeiro";// Rio de Janeiro
arrEstados["RN"] = "Rio Grande do Norte";// Natal
arrEstados["RS"] = "Rio Grande do Sul";// Porto Alegre
arrEstados["RO"] = "Rondônia";// Porto Velho
arrEstados["RR"] = "Rorâima";// Boa Vista
arrEstados["SC"] = "Santa Catarina";// Florianópolis
arrEstados["SP"] = "São Paulo";// São Paulo
arrEstados["SE"] = "Sergipe";// Aracaju
arrEstados["TO"] = "Tocantins";// Palmas
/****************/
function realTypeOf(obj) {
    return Object.prototype.toString.call(obj).slice(8, -1);
}

function isNotNumberRep(val){ //Verifica se o numero é 1111 e assim por diante
	if(val == "") return false;
	val = val.replace(/[^0-9]/g, '');
	invalidos = [];
	for(var i=0;i<10;i++){
		invalidos[i] = "";
		for(var j=0;j<val.length;j++){
			invalidos[i] += ""+i;
		}
		if(val == invalidos[i]){
			return false;	
		}
	}
	return true;
}

function isStateValid(val){ //Verifica se o numero é 1111 e assim por diante
	if(val == "") return false;
	if(arrEstados[val])	return true;
	else false;
}

function isCpfValid(cpf){
    var numeros, digitos, soma, i, resultado, digitos_iguais;
    cpf = cpf.replace(/[^0-9]/g, '');
    digitos_iguais = 1;
    if (cpf.length < 11)
        return false;
    for (i = 0; i < cpf.length - 1; i++)
    if (cpf.charAt(i) != cpf.charAt(i + 1)){
        digitos_iguais = 0;
        break;
    }
    if (!digitos_iguais){
        numeros = cpf.substring(0,9);
        digitos = cpf.substring(9);
        soma = 0;
        for (i = 10; i > 1; i--)
           soma += numeros.charAt(10 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
           return false;
        numeros = cpf.substring(0,10);
        soma = 0;
        for (i = 11; i > 1; i--)
            soma += numeros.charAt(11 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
           return false;
        return true;
    }else
        return false;
}

function isRgValid(numero){
    if(numero == "") return false;
    numero = numero.replace(/[^0-9]/g, '');
    numero = numero.split("");
    tamanho = numero.length;
    vetor = new Array(tamanho);
    if(tamanho>=1) vetor[0] = parseInt(numero[0]) * 2; 
    if(tamanho>=2) vetor[1] = parseInt(numero[1]) * 3; 
    if(tamanho>=3) vetor[2] = parseInt(numero[2]) * 4; 
    if(tamanho>=4) vetor[3] = parseInt(numero[3]) * 5; 
    if(tamanho>=5) vetor[4] = parseInt(numero[4]) * 6; 
    if(tamanho>=6) vetor[5] = parseInt(numero[5]) * 7; 
    if(tamanho>=7) vetor[6] = parseInt(numero[6]) * 8; 
    if(tamanho>=8) vetor[7] = parseInt(numero[7]) * 9; 
    if(tamanho>=9) vetor[8] = parseInt(numero[8]) * 100; 
    total = 0;
    if(tamanho>=1) total += vetor[0];
    if(tamanho>=2) total += vetor[1]; 
    if(tamanho>=3) total += vetor[2]; 
    if(tamanho>=4) total += vetor[3]; 
    if(tamanho>=5) total += vetor[4]; 
    if(tamanho>=6) total += vetor[5]; 
    if(tamanho>=7) total += vetor[6];
    if(tamanho>=8) total += vetor[7]; 
    if(tamanho>=9) total += vetor[8]; 
    resto = total % 11;
    if(resto!=0) return false;
    else	return true;
}

function validaCampo(campo){ return campo.value != ""; }
function validaEmail(campo){ return (campo.value != "" && campo.value.indexOf("@") > 0 && campo.value.indexOf(".") > 0);}
function isNumber(valor){  return !isNaN(valor); }
function apenasNum(campo){ campo.value = campo.value.replace(/[^0-9]/g, ''); }
function isDateInfToday(valor){ if(!isNotNumberRep(valor)) return false;}

function isCnpjValid( c ) {
	var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais, cnpj = c.replace(/\D+/g, '');
	digitos_iguais = 1;
	if(cnpj.length > 3){
		if (cnpj.length != 14){
			return false;
		}	
		for (i = 0; i < cnpj.length - 1; i++){
			if (cnpj.charAt(i) != cnpj.charAt(i + 1)){
				digitos_iguais = 0;
				break;
			}
		}
		if (!digitos_iguais){
			tamanho = cnpj.length - 2
			numeros = cnpj.substring(0,tamanho);
			digitos = cnpj.substring(tamanho);
			soma = 0;
			pos = tamanho - 7;
			for (i = tamanho; i >= 1; i--){
				 soma += numeros.charAt(tamanho - i) * pos--;
				 if (pos < 2){
					   pos = 9;
				 }
			}
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(0)){
				return false;
			}
		
			tamanho = tamanho + 1;
			numeros = cnpj.substring(0,tamanho);
			soma = 0;
			pos = tamanho - 7;
			for (i = tamanho; i >= 1; i--){
				 soma += numeros.charAt(tamanho - i) * pos--;
				 if (pos < 2){
					   pos = 9;
				 }
			}
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(1)){
				return false;
			}else{
				return true;
			}
		}else{
		   return false;
		}
	}
}

function convertToUSDate(str){
    arrData = str.split(" ");
    arrD = arrData[0].split('/');
    var strData = "";
    if(arrData[1]) strData = " "+arrData[1];
    return arrD[2]+"-"+arrD[1]+"-"+arrD[0]+strData;
}
function getObjData(str){
    arrData = str.split(" ");
    arrD = arrData[0].split('-');
    if(arrData[1]){
        arrH = arrData[1].split(':');
    }else{
        arrH = ['00','00','00'];
    }
    return new Date(arrD[0],arrD[1],arrD[2],arrH[0],arrH[1],arrH[2]);
}

isDateValid = function(year,month,date){	
    if(year && month && date){
        year = parseInt(year);
        month = parseInt(month);
        date = parseInt(date);
        if(month==2){
            if((year%4 == 0) && date>29)      return false
            else if((year%4!=0) && date > 28) return false;
        }
        if((month!=2 && month!=8) && ((month%2 != 1) && date>30))	return false;
        else	return true;
    }
    else  return false;
}

function isDateAboveNow(strData){
    objData = getObjData(strData);
    now = new Date();
    now.setMonth(now.getMonth()+1);
    return objData.getTime() > now.getTime();
}
function isDateBelowNow(strData){
    objData = getObjData(strData);
    now = new Date();
    now.setMonth(now.getMonth()+1);
    return objData.getTime() < now.getTime();
}

function validaFormulario(idForm,funcSucesso,funcErro){	
    var frm = document.getElementById(idForm);
    var checagem = 1;
    var count = frm.elements.length;
    var str_campo = "";
    var obsExtra = [];
    for(i=0; i<frm.elements.length; i++){
            //alert(frm.elements[i].name+":"+msg[frm.elements[i].name])
            var msgCampo = msg[frm.elements[i].name];
            if(msgCampo){
                    if(!vld[frm.elements[i].name]) vld[frm.elements[i].name] = 1
                    //checagem = false;

                    switch(vld[frm.elements[i].name]){
                            case 1: //checagem simples
                                    checagem = validaCampo(frm.elements[i]);
                            break;
                            case 2: //checagem de email
                                    checagem = validaEmail(frm.elements[i]);
                            break;
                            case 3: //so numero
                                    checagem = isNumber(frm.elements[i].value)
                            break;
                            case 4: //numero nao repetido
                                    checagem = isNotNumberRep(frm.elements[i].value);
                            break;	
                            case 5: //rg
                                    checagem = isRgValid(frm.elements[i].value)
                            break;	
                            case 6: //data
                                    var arrDate = frm.elements[i].value.split("/");
                                    checagem = isDateValid(arrDate[2],arrDate[1],arrDate[0]);
                            break;
                            case 7: //data inferior a de hoje
                                    var strData = frm.elements[i].value;	
                                    var arrDate = strData.split("/");
                                    checagem = isDateValid(arrDate[2],arrDate[1],arrDate[0]);
                                    checagem = parseInt(arrDate[2]) > 1900;
                                    if(checagem){
                                            checagem = isDateBelowNow(convertToUSDate(strData));
                                            obsExtra[frm.elements[i].name] = " <i>*Esta data deve ser inferior à data de hoje</i>";
                                    }else{

                                    }
                            break;
                            case 8: //data superior a de hoje
                                    var strData = frm.elements[i].value;
                                    var arrDate = strData.value.split("/");
                                    checagem = isDateValid(arrDate[2],arrDate[1],arrDate[0]);
                                    checagem = parseInt(arrDate[2]) > 1900;
                                    if(checagem){
                                            checagem = isDateAboveNow(convertToUSDate(strData))
                                            obsExtra[frm.elements[i].name] = " <i>*Esta data deve ser superior à data de hoje</i>";
                                    }else{

                                    }
                            break;	
                            case 9: //cnpj
                                    checagem = isCnpjValid(frm.elements[i].value);
                            break;	
                            case 10: //estado
                                    frm.elements[i].value = frm.elements[i].value.toUpperCase();
                                    checagem = isStateValid(frm.elements[i].value);
                            break;	
                            case 11: //cnh
                                    //rever
                            break;	
                            case 12: //cpf
                                    checagem = isCpfValid(frm.elements[i].value)
                            break;			
                    }
                    if(!checagem){
                            var obs = obsExtra[frm.elements[i].name]?obsExtra[frm.elements[i].name]:'';
                            msgCampo = msgCampo.replace("*","");
                            str_campo +=  msgCampo+obs+ ",";
                            count --;
                    }
            }
    }
    if(count == frm.elements.length){
        if(funcSucesso)  funcSucesso(destino_frm);
    }else{	
        if(funcErro) funcErro(str_campo);
        return false;
    }

}

function limpaCampos(idForm,strCamposAux){
    var frm = document.getElementById(idForm);
    if(strCamposAux){
            var arr = strCamposAux.split(',');
            for(i = 0; i < arr.length; i++){
                    for (var j = 0; j < frm.elements.length; j++) {
                            if(arr[i] == frm.elements[j].id){
                                    frm.elements[j].value='';							
                            }
                    }
            }		
    }
    for (var i = 0; i < frm.elements.length; i++) {
        if(frm.elements[i].type){
            if(frm.elements[i].type.indexOf('text') == 0){
                frm.elements[i].value='';
            }else if(frm.elements[i].type.indexOf('checkbox')== 0){			
                frm.elements[i].checked = false;			
            }else if(frm.elements[i].type.indexOf('select')== 0){	
                var sele = frm.elements[i];	
                if(sele[0]){
                        sele[0].selected = true;
                }
            }
        }		
    }	
}

function limpaform(idformulario,excessoes){
	if(excessoes) excessoes = excessoes.split(',')
	var frm =document.forms[idformulario];
	for (i=0;i<frm.elements.length;i++){
		var nome = frm.elements[i].getAttribute("id") || frm.elements[i].getAttribute("name");
		limpar = false;
		if(excessoes){
			if(!inArray(excessoes,nome)) limpar = true;
		}else{
			limpar = true;
		}
		if(limpar){
			if(frm.elements[i].type == "checkbox"){
				frm.elements[i].checked = false;
			}else{
				frm.elements[i].value = '';
			}
		}
	}
}

var fErro = function(strErro){
    var str = sapo_replace(",",";<br />",strErro);
    switch(IDIOMA){
        case LNG_PT:
            alerta("<strong>Preencha os campos:</strong><br />"+str);
        break;
        case LNG_EN:
            alerta("<strong>Fill in the fields:</strong><br />"+str);
        break;		
    }
    enviando = 0;
}

function onCompleteFocusTo(campo,idCampo){
   if(campo.value.indexOf("_") == -1){
       if(campo.value.length == $(campo).attr("maxlength")){
           document.getElementById(idCampo).focus();

       }    
    }
}