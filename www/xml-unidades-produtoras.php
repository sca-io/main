<?php 
require_once("conf.php");
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header ("Cache-Control: no-cache, must-revalidate"); 
header ("Pragma: no-cache"); 	
header("Content-type: text/xml; charset=UTF-8");
print '<?xml version="1.0" encoding="ISO-8859-1"?>';
print '<root>';

$idioma = getIdioma();
$idSecao = UNIDADES_PRODUTORAS;
$listagem = UnidadeProdutora::listar("id,nome,estado,link,descricao,pos_mapa","status = ".ATIVO." AND idioma = $idioma","estado ASC,nome ASC");
//print UnidadeProdutora::getLogSql();

function filtrarTexto($txt){
    $txt = str_replace('"', "&quot;", $txt);
    $txt = html_entity_decode($txt,ENT_QUOTES,"UTF-8");
    //$txt = str_replace('\n', "", $txt);
    return $txt;
}

if($listagem){
    foreach($listagem as $l){
        $nome = $l->getNome();
        $estado = $l->getEstado();
        $link = $l->getLink();     
        $descricao = "";   
        $posMapa = $l->getPosMapa();
        $posx = $posy = "";
        if($posMapa){
            $posMapa = explode(",",$posMapa);
            if(count($posMapa) > 0){
                $posx =  $posMapa[0];
                $posy =  $posMapa[1];
            }            
        }
        
        $descricao = (response_attr(filtrarTexto($l->getDescricao()),false));
        //$distancias = (response_attr(filtrarTexto($l->getDistancias())));
        print "<unidade_produtora>";
        print "
            <nome><![CDATA[".filtrarTexto(response_attr($nome,false))."]]></nome>
            <estado><![CDATA[".response_attr($estado)."]]></estado>
            <link><![CDATA[".filtrarTexto(response_attr($link))."]]></link>
            <lat><![CDATA[".response_attr($posx)."]]></lat>
            <long><![CDATA[".response_attr($posy)."]]></long>
            <descricao><![CDATA[".$descricao."]]></descricao>
            <idTrabalho><![CDATA[".$l->getId()."]]></idTrabalho>
        ";    
        print "</unidade_produtora>";

        //print '<unidade_produtora nome="'.response_attr($nome,false).'" estado="'.response_attr($estado).'"  link="'.response_attr($link).'" lat="'.response_attr($posx).'" long="'.response_attr($posy).'" descricao="'.$descricao.'" idTrabalho="'.$l->getId().'" />';
    }    
}
print '</root>';
?>