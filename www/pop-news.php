<?  require_once("conf.php");
$idioma = getIdioma();
?>
<div class="pop-assine-nossa-newsletter">
    <h2><?=getTextoByLang("Assine nossa newsletter","Subscribe to our newsletter");?></h2>
    <a href="javascript:;" class="bt-fecha-pop" id="btFechar">fechar</a>
    <form action="" id="formNews">
    	<p class="hidden"><input type="hidden" id="idioma" name="idioma" value="<?=$idioma?>" /></p><!--coloque aqui os inputs hidden-->    	
        <p><label id="lbl-nome"><?=getTextoByLang("Nome","Name");?></label><input type="text" id="nome" name="nome"></p>
        <p><label id="lbl-email">E-mail</label><input type="text" id="email" name="email"><a href="javascript:salvarNews()" class="bt-ok" title="OK">OK</a></p>
        
    </form>
    <p class="msg"></p>
</div>