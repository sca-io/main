<%@ Language=VBScript %>
<% session.LCID = 1046 %>


<!-- #include file = "script/bloqueio.asp" -->

<!-- #include file = "script/logado.asp" -->

<!-- #include file = "script/funcao.asp" -->

<!-- #include file = "script/seguir.asp" -->

<!-- #include file = "script/topo.asp" -->

<% 'VerificarNovosDados %>

<table width='95%' bgcolor='#37833E' align='center' border="0" cellspacing="1" cellpadding="0" ID="Table1">
		<tr>
			<td class='maintitle'>
				<table width='100%' border='0' cellspacing='0' cellpadding='3' id="Table13">
					<tr>
						<td><IMG src='<%=Link("img/gif_ico_nav_titulo.gif")%>' alt='' width="8" height="8" border="0"></td>
						<td width='100%' class='maintitle'>Central de controle</td>
						<td align='right' class='maintitle' nowrap></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
		<td>
			<table class='mainbg' width="100%" border="0" cellspacing="1" cellpadding="4" ID="Table2">
				<tr>
					<td nowrap class='titlemedium'>&nbsp;</td>
					<td width="100%" nowrap class='titlemedium'>&nbsp;</td>
				</tr>
				<tr>
					<td class='row1' valign="top">
						<!-- #include file = "script/montarMenu.asp" -->
					</td>
					<td valign='top' class='row1'>
						<table cellpadding='4' cellspacing='3' width='100%' align='left' border='0' ID="Table4">
							
							<!-- In�cio do conte�do -->
							
								<%
									
								Set BD = CreateObject("ADODB.Connection")
								Set RS = CreateObject("ADODB.Recordset")
								
								BD.Open Application("bd-logped")
								
								If Request.QueryString("acao") <> "fim" then
									RS.Open "SELECT nome_usina, cnpj, data_emissao, n_pedido, modalidade, produto, volume, volume_carregado, volume_devolvido FROM andamento WHERE id = " & Request.QueryString("id") & ";", BD
								else
									RS.Open "SELECT nome_usina, cnpj, data_emissao, n_pedido, modalidade, produto, volume, volume_carregado, volume_devolvido, encerradoem FROM carregado WHERE id = " & Request.QueryString("id") & ";", BD
								End If
								
								If RS.EOF then
									if TabelaPedido(Request.QueryString("id")) = "carregado" then
										'o sistema foi atualizado e o pedido foi carregado completamente
										MostrarSubTitulo "Pedido atualizado! Verifique hist�rico de pedidos."
										Acao "Hist�rico de pedido rec�m completado (ID " & Request.QueryString("id") & ")"
									else
										MostrarSubTitulo "Pedido inv�lido"
										Acao "Hist�rico de pedido inv�lido (ID " & Request.QueryString("id") & ")"
									end if
								else
								
								Set RSVer = CreateObject("ADODB.Recordset")
								RSVer.Open "Select cnpj From cliente WHERE usuarioid = " & Session("ID") & " AND cnpj LIKE '" & CortaCNPJ(RS("cnpj")) & "%';", BD
								
								If RSVer.EOF then
								'Pedido de uma distribuidora que n�o � associada a este usu�rio
									Acao "Acesso a hist pedido de outra distr. PedID " & Request.QueryString("id")
									response.Redirect Link("acesso.asp?menu=acesso_pedido_invalido")
									RSVer.Close
									set RSVer = nothing
									RS.Close
									BD.Close
									set RS = nothing
									set BD = nothing
									Response.End
								End If
								
								RSVer.Close
								Set RSVer = nothing
								
								Acao "Hist�rico de pedido (ID " & Request.QueryString("id") & " - " & RS("n_pedido") & "-" & RS("modalidade") & ")"
								
								MostrarSubTitulo "Hist�rico do pedido " & RS("n_pedido") & " - " & RS("modalidade")
							'>>>>>>>>>>>>>>>>>>>>>
								MostrarTituloSecao "Distribuidor"
									MostrarConteudoSecao "Raz�o social:", "<b>" & NomeCliente(RS("cnpj")) & "</b>"
									MostrarConteudoSecao "CNPJ: ", CNPJ(RS("cnpj"))

								MostrarTituloSecao "Unidade Produtora"
									MostrarConteudoSecao "Raz�o social:", "<b>" & RS("nome_usina") & "</b>"

								MostrarTituloSecao "Pedido"
									MostrarConteudoSecao "N�mero: ", FormatNumber(RS("n_pedido"), 0)
									MostrarConteudoSecao "Modalidade: ", RS("modalidade")
									'If Request.QueryString("acao") = "fim" then MostrarConteudoSecao "Encerrado Em: ", MostrarData(RS("encerradoem"))
									MostrarConteudoSecao "Produto: ", RS("produto")
									MostrarConteudoSecao "<b>Volume</b>: ", FormatNumber(RS("volume"), 3) & " m�"
									
								MostrarTituloSecao "Volumes retirados (m�)"
									%>
									<TR>
									<TD class=row1 valign=top nowrap align=left>Carregado (<%=PorcentagemCarregada(Request.QueryString("id"))%>): </TD>
									<TD class=row1 align=right width="80"><%=FormatNumber(calcularCarregamento(Request.QueryString("id")), 3)%></TD></TR>
									<TR>
									<TD class=row1 valign=top nowrap align=left>Devolvido: </TD>
									<TD class=row1 align=right width="80"><%=FormatNumber(RS("volume_devolvido"), 3)%></TD></TR>
									<%
									'MostrarConteudoSecao "Carregado (-): ", FormatNumber(RS("volume_carregado"), 3) & " m� (" & PorcentagemCarregada(Request.QueryString("id")) & ")"
									'MostrarConteudoSecao "Devolvido (+): ", FormatNumber(RS("volume_devolvido"), 3) & " m�"
									'Dim mostrarSaldoTemp
									'mostrarSaldoTemp = RS("volume") + RS("volume_devolvido") - RS("volume_carregado") 
									'if mostrarSaldoTemp < 0 then mostrarSaldoTemp = 0
									'MostrarConteudoSecao "Saldo (=):", FormatNumber(mostrarSaldoTemp, 3) & " m�"
									%>
									<TR>
									<TD class=row1 valign=top nowrap align=left>Saldo: </TD>
									<TD class=row1 align=right width="80"><%=FormatNumber(calcularSaldo(Request.QueryString("id")), 3)%></TD></TR>


<%
								'>>>>>>>>>>>>>>>>>>>>>								
								MostrarTituloSecao "Extrato dos carregamentos"
								%>
				</tr><tr>
                 <td valign='top' class='row1' colspan='2'>
                 <table cellpadding='4' cellspacing='0' align='center' width='400' style='border:1px solid ' ID="tblCarregamentos">
                 
                 <tr>
                   <td class='row2' align='left' valign="top" class='titlemedium' nowrap>Faturamento</td>
                   <!--<td class='row2' align='left' valign="top"  class='titlemedium'><i>--
                   <%
                    If Request.QueryString("acao") = "fim" then 
						response.Write Request.QueryString("id") & ":" & RS("n_pedido") & "-" & RS("modalidade")
                    else 
						response.Write Request.QueryString("id") & ":" & RS("n_pedido") & "-" & RS("modalidade")
                    end if
                   %> --</i></td>-->
                   <td class='row2' align='center' valign="top"  class='titlemedium'>Nota Fiscal</td>
                   <td class='row2' align='right' valign="top"  class='titlemedium'>Volume (m�)</td>
                 </tr>
                 
                 
                 <!-- Acompanhamento de altera��es -->
                 
                 <%
                 
                 Set RSAc = CreateObject("ADODB.Recordset")
                 'Procurar pelos carregamentos anteriores a 30 dias
                 RSAc.Open "SELECT id, volume, volume_devolvido, nota_fiscal, quando From carregar_bkp WHERE pedidoid = " & Request.QueryString("id") & " ORDER BY quando;", BD
                 
                 If Not RSAc.EOF Then
                 
					While Not RSAc.EOF
					
						If intControleCorLinha = 1 then intControleCorLinha = 2 Else intControleCorLinha = 1
					
					%>
					
						<!-- Acompanhamento <%=RSAc("id")%> -->
		                 
							<tr>
							<td class='row<%=intControleCorLinha%>' align='left' valign="top" class='titlemedium' nowrap><font face="Courier New" size="2"><%=FormatDateTime(RSAc("quando"), 2)%></font></td>
							<td class='row<%=intControleCorLinha%>' align='center' valign="top" class='titlemedium' nowrap><font face="Courier New" size="2"><%= RSAc("nota_fiscal")%></font></td>
							<td class='row<%=intControleCorLinha%>' align='right' valign="top"  class='titlemedium'><font face="Courier New" size="2">
							<%
							if RSAc("volume") > 0 then
								response.Write FormatNumber(RSAc("volume"), 3) & "+"
							else
								response.Write FormatNumber(RSAc("volume_devolvido"), 3) & "-"
							end if
							%>
							</font></td>
							</tr>
		                 
						<!-- Acompanhamento <%=RSAc("id")%> -->
					
					<%
					
						RSAc.MoveNext
					
					Wend
				
                 End If
                 
                 'Carregamentos com menos de 30 dias
                RSAc.Close
				RSAc.Open "SELECT id, volume, volume_devolvido, nota_fiscal, quando From carregar WHERE pedidoid = " & Request.QueryString("id") & " ORDER BY quando;", BD
                 
                 If Not RSAc.EOF Then
                 
					While Not RSAc.EOF
					
						If intControleCorLinha = 1 then intControleCorLinha = 2 Else intControleCorLinha = 1
					
					%>
					
						<!-- Acompanhamento - <%=RSAc("id")%> -->
		                 
							<tr>
							<td class='row<%=intControleCorLinha%>' align='left' valign="top" class='titlemedium' nowrap><font face="Courier New" size="2"><%=FormatDateTime(RSAc("quando"), 2)%></font></td>
							<td class='row<%=intControleCorLinha%>' align='center' valign="top" class='titlemedium' nowrap><font face="Courier New" size="2"><%= RSAc("nota_fiscal")%></font></td>
							<td class='row<%=intControleCorLinha%>' align='right' valign="top"  class='titlemedium'><font face="Courier New" size="2">
							<%
							if RSAc("volume") > 0 then
								response.Write FormatNumber(RSAc("volume"), 3) & "+"
							else
								response.Write FormatNumber(RSAc("volume_devolvido"), 3) & "-"
							end if
							%>
							</font></td>
							</tr>
		                 
						<!-- Acompanhamento - <%=RSAc("id")%> -->
					
					<%
					
						RSAc.MoveNext
					
					Wend
				
                 End If
                 
                 RSAc.Close
                 Set RSAc = nothing
                 
                 %>
                 
                 <!-- Acompanhamento de altera��es --
                 
                 <tr>
                   <td class='row2' align='left' valign="top" class='titlemedium' nowrap><%=MostrarData(Now)%></td>
                   <td class='row2' align='left' valign="top" class='titlemedium'><i>-- Fim das informa��es --</i></td>
                 </tr>
                 
                 -->
                 
                 </table></td></tr>

                 
                 <%
								End If
								
								RS.Close
								BD.Close
								set RS = nothing
								set BD = nothing
								
								%>
								<tr><td><br></td></tr>
								<form method="get" action="<%=Link("ped_info.asp")%>" ID="Form1">
								<tr>
								<td class='row2' align='center' colspan='2'>
									<input type="hidden" name="acao" value="<%=Request.QueryString("acao")%>" ID="Hidden2">
									<input type="hidden" name="id" value="<%=Request.QueryString("id")%>">
									<input type="hidden" name="sid" value="<%=Session("Controle")%>" ID="Hidden1">
									<!--<input type="button" value="Voltar" class='forminput' ID="Voltar" NAME="Voltar" onclick="javascript:window.location='<%=Link(Request.ServerVariables("http_referer"))%>';">-->
									<A href="<%=Link(Request.ServerVariables("http_referer"))%>"><IMG src="<%=Link("img/gif_bot_voltar.gif")%>" alt="Voltar" border="0" title=""></A>&nbsp;&nbsp;&nbsp;
									<!--<input type="submit" value="Resumo" class='forminput' ID="VerHist" NAME="VerHist">-->
									<INPUT type="image" src="<%=Link("img/gif_bot_resumo.gif")%>" name="VerResumo" value="Exibir resumo" id="VerResumo">
								</td></tr>
								</form>
							
							<!-- Fim do conte�do -->
								
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!-- #include file = "script/fim.asp" -->