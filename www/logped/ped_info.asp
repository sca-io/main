<%@ Language=VBScript %>
<% session.LCID = 1046 %>

<!-- #include file = "script/bloqueio.asp" -->

<!-- #include file = "script/logado.asp" -->

<!-- #include file = "script/funcao.asp" -->

<!-- #include file = "script/seguir.asp" -->

<%

If Request.QueryString("acao") = "alt" then
	'Solicita��o de atuatiza��o do volume do pedido
	Response.Redirect Link("ped_alt_vol.asp?id=" & Request.QueryString("id"))
	Response.End
End If

%>

<!-- #include file = "script/topo.asp" -->

<% 'VerificarNovosDados %>

<table width='95%' bgcolor='#37833E' align='center' border="0" cellspacing="1" cellpadding="0" ID="Table1">
		<tr>
			<td class='maintitle'>
				<table width='100%' border='0' cellspacing='0' cellpadding='3' id="Table13">
					<tr>
						<td><IMG src='<%=Link("img/gif_ico_nav_titulo.gif")%>' alt='' width="8" height="8" border="0"></td>
						<td width='100%' class='maintitle'>Central de controle</td>
						<td align='right' class='maintitle' nowrap></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
		<td>
			<table class='mainbg' width="100%" border="0" cellspacing="1" cellpadding="4" ID="Table2">
				<tr>
					<td nowrap class='titlemedium'>&nbsp;</td>
					<td width="100%" nowrap class='titlemedium'>&nbsp;</td>
				</tr>
				<tr>
					<td class='row1' valign="top">
						<!-- #include file = "script/montarMenu.asp" -->
					</td>
					<td valign='top' class='row1'>
						<table cellpadding='4' cellspacing='3' width='100%' align='left' border='0' ID="Table4">
							
							<!-- In�cio do conte�do -->
							
								<%
									
								Set BD = CreateObject("ADODB.Connection")
								Set RS = CreateObject("ADODB.Recordset")
								
								BD.Open Application("bd-logped")
								
								If Request.QueryString("acao") <> "fim" then
									RS.Open "SELECT * FROM andamento WHERE id = " & Request.QueryString("id") & ";", BD
								else
									RS.Open "SELECT * FROM carregado WHERE id = " & Request.QueryString("id") & ";", BD
								End If
								
								If RS.EOF then
									if TabelaPedido(Request.QueryString("id")) = "carregado" then
										'o sistema foi atualizado e o pedido foi carregado completamente
										MostrarSubTitulo "Pedido atualizado! Verifique hist�rico de pedidos."
										Acao "Visualiza��o de pedido rec�m completado (ID " & Request.QueryString("id") & ")"
									else
										MostrarSubTitulo "Pedido inv�lido"
										Acao "Visualiza��o de pedido inv�lido (ID " & Request.QueryString("id") & ")"
									end if
								else
								
								Set RSVer = CreateObject("ADODB.Recordset")
								RSVer.Open "Select cnpj From cliente WHERE usuarioid = " & Session("ID") & " AND cnpj LIKE '" & CortaCNPJ(RS("cnpj")) & "%';", BD
								
								If RSVer.EOF then
								'Pedido de uma distribuidora que n�o � associada a este usu�rio
									Acao "Acesso a pedido de outra distr. PedID " & Request.QueryString("id")
									response.Redirect Link("acesso.asp?menu=acesso_pedido_invalido")
									RSVer.Close
									set RSVer = nothing
									RS.Close
									BD.Close
									set RS = nothing
									set BD = nothing
									Response.End
								End If
								
								RSVer.Close
								Set RSVer = nothing
								
								If Request.QueryString("acao") <> "fim" then
									'Tirar os sinais de novo e atualizado
									bd.Execute "UPDATE andamento SET novo = 0, atualizado = 0 WHERE  id = " & Request.QueryString("id") & ";"
									Acao "Visualiza��o de pedido (ID " & Request.QueryString("id") & " - " & RS("n_pedido") & "-" & RS("modalidade") & ")"
									MostrarSubTitulo "Informa��es de carregamento " & RS("n_pedido") & "-" & RS("modalidade")
								else
									Acao "Visualiza��o de hist�rico de pedido (ID " & Request.QueryString("id") & " - " & RS("n_pedido") & "-" & RS("modalidade") & ")"
									MostrarSubTitulo "Hist�rico do pedido " & RS("n_pedido") & " - " & RS("modalidade")
								end if
								
								MostrarTituloSecao "Distribuidor"
									MostrarConteudoSecao "Raz�o social:", "<b>" & NomeCliente(RS("cnpj")) & "</b>"
									MostrarConteudoSecao "CNPJ: ", CNPJ(RS("cnpj"))

								MostrarTituloSecao "Unidade Produtora"
									MostrarConteudoSecao "Raz�o social:", "<b>" & RS("nome_usina") & "</b>"

								MostrarTituloSecao "Pedido"
									MostrarConteudoSecao "N�mero: ", FormatNumber(RS("n_pedido"), 0)
										'if Ucase(RS("modalidade")) = "C" then
										'	MostrarConteudoSecao "Modalidade: ", "Carburante"
										'else
										'	MostrarConteudoSecao "Modalidade: ", "Outros fins"
										'end if
									MostrarConteudoSecao "Modalidade: ", RS("modalidade")
									'If Request.QueryString("acao") = "fim" then MostrarConteudoSecao "Encerrado Em: ", MostrarData(RS("encerradoem"))
									MostrarConteudoSecao "Produto: ", RS("produto")
									MostrarConteudoSecao "<b>Volume</b>: ", FormatNumber(RS("volume"), 3) & " m�"
									
								MostrarTituloSecao "Volumes retirados (m�)"
									%>
									<TR>
									<TD class=row1 valign=top nowrap align=left>Carregado (<%=PorcentagemCarregada(Request.QueryString("id"))%>): </TD>
									<TD class=row1 align=right width="80"><%=FormatNumber(calcularCarregamento(Request.QueryString("id")), 3)%></TD></TR>
									<TR>
									<TD class=row1 valign=top nowrap align=left>Devolvido: </TD>
									<TD class=row1 align=right width="80"><%=FormatNumber(RS("volume_devolvido"), 3)%></TD></TR>
									<%
									'MostrarConteudoSecao "Carregado (-): ", FormatNumber(RS("volume_carregado"), 3) & " m� (" & PorcentagemCarregada(Request.QueryString("id")) & ")"
									'MostrarConteudoSecao "Devolvido (+): ", FormatNumber(RS("volume_devolvido"), 3) & " m�"
									'Dim mostrarSaldoTemp
									'mostrarSaldoTemp = RS("volume") - (RS("volume_carregado") - RS("volume_devolvido"))
									'if mostrarSaldoTemp < 0 then mostrarSaldoTemp = 0
									'MostrarConteudoSecao "Saldo (=):", FormatNumber(mostrarSaldoTemp, 3) & " m�"
									%>
									<TR>
									<TD class=row1 valign=top nowrap align=left>Saldo: </TD>
									<TD class=row1 align=right width="80"><%=FormatNumber(calcularSaldo(Request.QueryString("id")), 3)%></TD></TR>


<%

								'>>>>>>>>>>>>>>>>>>>>>			
								

								end if
								
								RS.Close
								BD.Close
								set RS = nothing
								set BD = nothing
								
								%>
								
								<form method="get" action="<%=Link("ped_hist.asp")%>" ID="Form1">
								<tr>
								<td class='row2' align='center' colspan='2'>
									<input type="hidden" name="id" value="<%=Request.QueryString("id")%>">
									<input type="hidden" name="acao" value="<%=Request.QueryString("acao")%>" ID="Hidden1">
									<input type="hidden" name="sid" value="<%=Session("Controle")%>" ID="Hidden2">
									<!--<input type="button" value="Voltar" class='forminput' ID="Voltar" NAME="Voltar" onclick="javascript:window.location='<%=Link(Request.ServerVariables("http_referer"))%>';">-->
									<A href="<%=Link(Request.ServerVariables("http_referer"))%>"><IMG src="<%=Link("img/gif_bot_voltar.gif")%>" alt="Voltar" border="0" title=""></A>&nbsp;&nbsp;&nbsp;
									<!--<input type="submit" value="Extrato" class='forminput' ID="VerHist">-->
									<INPUT type="image" src="<%=Link("img/gif_bot_extrato.gif")%>" name="VerHist" value="Exibir extrato" id="VerHist">
								</td></tr>
								</form>
							
							<!-- Fim do conte�do -->
								
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!-- #include file = "script/fim.asp" -->