<%@ Language=VBScript %>

<!-- #include file = "script/funcao.asp" -->

<%
Response.Buffer = false
Response.Expires = 0
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"
%>


<%

'Atualiza��o via FTP Online
if request.QueryString("modo") = "remoto" then

		if ContarRegistrosPreImportados = "zero" then

		VerificarNovosDados()

		Set BDImpSCA = CreateObject("ADODB.Connection")
		BDImpSCA.Open Application("bd-logped")
		
		'Opera��es de manuten��o para retirar dados com mais de 30 dias
		BDImpSCA.Execute "INSERT INTO sessao_bkp SELECT * FROM sessao WHERE TO_DAYS(inicio) < TO_DAYS(NOW()) - 15"
		BDImpSCA.Execute "DELETE FROM sessao WHERE TO_DAYS(inicio) < TO_DAYS(NOW()) - 15"
		BDImpSCA.Execute "INSERT INTO acao_bkp SELECT * FROM acao WHERE TO_DAYS(quando) < TO_DAYS(NOW()) - 15"
		BDImpSCA.Execute "DELETE FROM acao WHERE TO_DAYS(quando) < TO_DAYS(NOW()) - 15"
		BDImpSCA.Execute "INSERT INTO carregado_bkp SELECT * FROM carregado WHERE TO_DAYS(encerradoem) < TO_DAYS(NOW()) - 30"
		BDImpSCA.Execute "DELETE FROM carregado WHERE TO_DAYS(encerradoem) < TO_DAYS(NOW()) - 30"
		BDImpSCA.Execute "INSERT INTO carregar_bkp SELECT carregar.* FROM carregar, carregado_bkp WHERE carregado_bkp.id = carregar.pedidoid;"
		BDImpSCA.Execute "DELETE FROM carregar USING carregar, carregado_bkp WHERE carregado_bkp.id = carregar.pedidoid;"

		'Reparar tabelas
		BDImpSCA.Execute   "REPAIR TABLE `acao` , `acao_bkp` , `admin` , `andamento` , `carregado` , `carregado_bkp` , `carregar` , `carregar_bkp` , `cliente` , `cnpj` , `importar` , `nivelacesso` , `sessao` , `sessao_bkp` , `usuario` ;"

		'Otimizar tabelas
		BDImpSCA.Execute "OPTIMIZE TABLE `acao` , `acao_bkp` , `admin` , `andamento` , `carregado` , `carregado_bkp` , `carregar` , `carregar_bkp` , `cliente` , `cnpj` , `importar` , `nivelacesso` , `sessao` , `sessao_bkp` , `usuario` ;"

		'''''''''''''''''''''''''''''''
		'processamento dos pedidos
		Set RSAnd = CreateObject("ADODB.Recordset")
		Set RSNotaFiscal = CreateObject("ADODB.Recordset")
		Set RS_Verificar_Carregado = CreateObject("ADODB.Recordset") 'verifica se o pedido j� est� registrado nos Carregados


		RSAnd.Open "SELECT id FROM andamento;", BDImpSCA
		
		While Not RSAnd.EOF
			'Atualizar pedido

			BDImpSCA.Execute "SELECT @volant:=SUM( carregar.volume ),@voldevant:=SUM( carregar.volume_devolvido ) FROM carregar WHERE carregar.pedidoid = " & RSAnd("id") & ";"
			BDImpSCA.Execute "UPDATE andamento SET andamento.volume_carregado = @volant, andamento.volume_devolvido = @voldevant  WHERE andamento.id = " & RSAnd("id") & " ;"

			
			RSAnd.MoveNext
		Wend
		
		RSAnd.Close
		
		'Atualizar as informa��es dos pedidos carregados
		RSAnd.Open "SELECT id FROM carregado;", BDImpSCA
		
		While Not RSAnd.EOF
			'Atualizar pedido

			BDImpSCA.Execute "SELECT @volant:=SUM( carregar.volume ),@voldevant:=SUM( carregar.volume_devolvido ) FROM carregar WHERE carregar.pedidoid = " & RSAnd("id") & ";"
			BDImpSCA.Execute "UPDATE carregado SET carregado.volume_carregado = @volant, carregado.volume_devolvido = @voldevant WHERE carregado.id = " & RSAnd("id") & " ;"
			
			RSAnd.MoveNext
		Wend
		
		RSAnd.Close

		RSAnd.Open "SELECT * FROM andamento WHERE volume_carregado >= volume + volume_devolvido;", BDImpSCA

		While not RSAnd.EOF 'enviar os pedidos totalmente carregados para o hist�rico

			'Mandar pedido para o Hist�rico

					Dim EncerradoEm
					EncerradoEm = DataSQL(Now()) & " " & Time()

					'Inserir na tabela de pedidos carregados
					
					'Verificar se o pedido j� est� no hist�rico
					RS_Verificar_Carregado.Open "SELECT id FROM carregado WHERE n_pedido = " & RSAnd("n_pedido") & " AND modalidade = '" & RSAnd("modalidade") & "' AND nome_usina = '" & RSAnd("nome_usina") & "' AND cnpj = '" & CortaCNPJ(RSAnd("cnpj")) & "';", BDImpSCA
					
					if RS_Verificar_Carregado.EOF then
						'O pedido n�o est� no hist�rico, deve-se inclu�-lo
						
						BDImpSCA.Execute "SELECT @volcarregadohist:=SUM( carregar.volume ),@voldevolvidohist:=SUM( carregar.volume_devolvido ) FROM carregar WHERE carregar.pedidoid = " & RSAnd("id") & ";"
						BDImpSCA.Execute "INSERT INTO carregado VALUES (" & RSAnd("id") & ", " & RSAnd("n_pedido") & ", '" & RSAnd("modalidade") & "', '" & RSAnd("nome_usina") & "', '" & CortaCNPJ(RSAnd("cnpj")) & "', '" & RSAnd("produto") & "', '" & RSAnd("nota_fiscal") & "', '" & DataSQL(RSAnd("data_emissao")) & "', '" & RSAnd("volume") & "', @volcarregadohist, @voldevolvidohist, '" & DataSQL(RSAnd("inseridoem")) & " " & FormatDateTime(RSAnd("inseridoem"), 4) & "',  '" & EncerradoEm & "',  " & Session("id") & ");"
						Acao "Pedido carregado e encerrado (ID " & RSAnd("id") & " - " & RSAnd("n_pedido") & "-" & RSAnd("modalidade") & ")"
					end if
					
					RS_Verificar_Carregado.Close
						
					'Deletar de ped em carregamento, pois j� est� no hist�rico
					BDImpSCA.Execute "DELETE FROM andamento WHERE id = " & RSAnd("id") & ";"

			RSAnd.MoveNext
		Wend

		RSAnd.Close
		
		set RSAnd = nothing
		set RS_Verificar_Carregado = nothing

	
		'response.Write "processado - ."
		response.Write ContarRegistrosPreImportados
		response.End
		
		
		end if
		
		response.Write ContarRegistrosPreImportados
		response.End
		
end if
%>

<html>
	<head>
		<title>SCA - Log�stica</title>
		<link rel="stylesheet" href="script/LogPed.css" type="text/css">
	</head>
	<body>
		<!-- In�cio Cabe�alho -->
		<table width='95%' align='center' border='0' cellspacing='0' cellpadding='8' id="tblTopo">
			<tr>
				<td width='100%' valign='middle' align='center' class='row1'>
					<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center' id="tblInfoTopo">
						<tr>
							<td align='left' valign='middle'>
								<%="Sess�o para importa��o de dados"%>
								<br><br>
								<b><%= WeekDayName(Weekday(Date), True, 1) & "&nbsp;" & Date%></b>
								&nbsp;<iframe width="60" height="14" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" src="script/horaCerta.asp"></iframe>
								<br>
							</td>
							<td align='right' valign='middle'><a href="<%=link("raiz")%>"><img src="<%=Link("img/gif_mnu_logo.gif")%>" border=0></a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br />
		<!-- Fim Cabe�alho -->

<table width='95%' bgcolor='#37833E' align='center' border="0" cellspacing="1" cellpadding="0" ID="Table1">
		<tr>
			<td class='maintitle'>
				<table width='100%' border='0' cellspacing='0' cellpadding='3' id="Table13">
					<tr>
						<td><IMG src='<%=Link("img/gif_ico_nav_titulo.gif")%>' alt='' width="8" height="8" border="0"></td>
						<td width='100%' class='maintitle'>Central de controle</td>
						<td align='right' class='maintitle' nowrap></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
		<td>
			<table class='mainbg' width="100%" border="0" cellspacing="1" cellpadding="4" ID="Table2">
				<tr>
					<td nowrap class='titlemedium'>&nbsp;</td>
					<td width="100%" nowrap class='titlemedium'>&nbsp;</td>
				</tr>
				<tr>
					<td class='row1' valign="top">&nbsp;
					</td>
					<td valign='top' class='row1'>
						<table cellpadding='4' cellspacing='3' width='100%' align='left' border='0' ID="Table4">
							
							<!-- In�cio do conte�do -->
								
								<%
								MostrarSubTitulo "Importa��o manual de dados"
								
								MostrarTituloSecao "Iniciando importa��o dos dados<tr><td></td></tr>"
									VerificarNovosDados()
								MostrarTituloSecao "Importa��o conclu�da, movendo dados antigos<tr><td></td></tr>"
								
								Set BDImpSCA = CreateObject("ADODB.Connection")
								BDImpSCA.Open Application("bd-logped")
											'Opera��es de manuten��o para retirar dados com mais de 30 dias
										BDImpSCA.Execute "INSERT INTO sessao_bkp SELECT * FROM sessao WHERE TO_DAYS(inicio) < TO_DAYS(NOW()) - 30"
										BDImpSCA.Execute "DELETE FROM sessao WHERE TO_DAYS(inicio) < TO_DAYS(NOW()) - 30"
										
										BDImpSCA.Execute "INSERT INTO acao_bkp SELECT * FROM acao WHERE TO_DAYS(quando) < TO_DAYS(NOW()) - 30"
										BDImpSCA.Execute "DELETE FROM acao WHERE TO_DAYS(quando) < TO_DAYS(NOW()) - 30"
										
										BDImpSCA.Execute "INSERT INTO carregado_bkp SELECT * FROM carregado WHERE TO_DAYS(encerradoem) < TO_DAYS(NOW()) - 30"
										BDImpSCA.Execute "DELETE FROM carregado WHERE TO_DAYS(encerradoem) < TO_DAYS(NOW()) - 30"
										
										BDImpSCA.Execute "INSERT INTO carregar_bkp SELECT carregar.* FROM carregar, carregado_bkp WHERE carregado_bkp.id = carregar.pedidoid;"
										BDImpSCA.Execute "DELETE FROM carregar USING carregar, carregado_bkp WHERE carregado_bkp.id = carregar.pedidoid;"
										
										'Reparar tabelas
										BDImpSCA.Execute "REPAIR TABLE acao;"
										BDImpSCA.Execute "REPAIR TABLE carregado;"
										BDImpSCA.Execute "REPAIR TABLE carregar;"
										BDImpSCA.Execute "REPAIR TABLE sessao;"
										BDImpSCA.Execute "REPAIR TABLE acao_bkp;"
										BDImpSCA.Execute "REPAIR TABLE carregado_bkp;"
										BDImpSCA.Execute "REPAIR TABLE carregar_bkp;"
										BDImpSCA.Execute "REPAIR TABLE sessao_bkp;"
										
										'Otimizar tabelas
										BDImpSCA.Execute "OPTIMIZE TABLE importar;"
										BDImpSCA.Execute "OPTIMIZE TABLE carregado;"
										BDImpSCA.Execute "OPTIMIZE TABLE carregar;"
										BDImpSCA.Execute "OPTIMIZE TABLE sessao;"
										BDImpSCA.Execute "OPTIMIZE TABLE importar_bkp;"
										BDImpSCA.Execute "OPTIMIZE TABLE carregado_bkp;"
										BDImpSCA.Execute "OPTIMIZE TABLE carregar_bkp;"
										BDImpSCA.Execute "OPTIMIZE TABLE sessao_bkp;"
										
								BDImpSCA.Close
								Set BDImpSCA = nothing
								
										MostrarTituloSecao "Opera��es conclu�das em " & Now
								
								%>
								<br>
								<tr><td></td></tr>
								<tr><td><BR><BR></td></tr>
							<!-- Fim do conte�do -->
								
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!-- #include file = "script/fim.asp" -->