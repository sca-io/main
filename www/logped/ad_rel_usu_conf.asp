<%@ Language=VBScript %>

<!-- #include file = "script/bloqueio.asp" -->

<!-- #include file = "script/logado.asp" -->

<!-- #include file = "script/funcao.asp" -->

<!-- #include file = "script/seguir.asp" -->

<!-- #include file = "script/topo.asp" -->

<% 'VerificarNovosDados %>

<script language=javascript>

function Confirmar_Apagar_Rel(NumeroCNPJ, IDCNPJ){
	if (confirm("Deseja apagar o relacionamento do CNPJ "+ NumeroCNPJ +"?")) 
	{
		document.frmApagarDOC.nmrcnpj.value = IDCNPJ;
		document.frmApagarDOC.submit();
	}
}
</script>

<form method="post" action="<%=Link("ad_rel_usu_del.asp")%>" name="frmApagarDOC" ID="frmApagarDOC">
<input type="hidden" name="nmrcnpj" ID="nmrcnpj">
</form>

<table width='95%' bgcolor='#37833E' align='center' border="0" cellspacing="1" cellpadding="0" ID="Table1">
		<tr>
			<td class='maintitle'>
				<table width='100%' border='0' cellspacing='0' cellpadding='3' id="Table13">
					<tr>
						<td><IMG src='<%=Link("img/gif_ico_nav_titulo.gif")%>' alt='' width="8" height="8" border="0"></td>
						<td width='100%' class='maintitle'>Central de controle</td>
						<td align='right' class='maintitle' nowrap></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
		<td>
			<table class='mainbg' width="100%" border="0" cellspacing="1" cellpadding="4" ID="Table2">
				<tr>
					<td nowrap class='titlemedium'>&nbsp;</td>
					<td width="100%" nowrap class='titlemedium'>&nbsp;</td>
				</tr>
				<tr>
					<td class='row1' valign="top">
						<!-- #include file = "script/montarMenu.asp" -->
					</td>
					<td valign='top' class='row1'>
						<table cellpadding='4' cellspacing='3' width='100%' align='left' border='0' ID="Table4">
							
							<!-- In�cio do conte�do -->
							
								<%
									
								Set BD = CreateObject("ADODB.Connection")
								Set RS = CreateObject("ADODB.Recordset")
								
								BD.Open Application("bd-logped")
								RS.Open "SELECT cnpj, descricao FROM cnpj WHERE id = " & SoNumeros(Request.QueryString("cnpjid")) & ";", BD

								
								Select Case Request.QueryString("modo")
									Case "apagar"
									
										Dim strCorRespEdi, strTxtRespEdi
									
										Set RSverUsu = CreateObject("ADODB.Recordset") 'verificar se o usu�rio j� est� relacionado
											RSverUsu.Open "SELECT usuarioid FROM cliente WHERE usuarioid = " & Request.QueryString("usuarioid") & " AND cnpj = '" & RS("cnpj") & "';", BD
										If Not RSverUsu.EOF Then 'usu�rio est� relacionado = apagar
											BD.Execute "DELETE FROM cliente WHERE cnpj = '" & RS("CNPJ") & "' AND usuarioid = " & Request.QueryString("usuarioid") & ";"
											BD.Execute "DELETE FROM admin   WHERE cnpj = '" & RS("CNPJ") & "' AND usuarioid = " & Request.QueryString("usuarioid") & ";"
											Acao "ad_rel_usu apagou " & Request.QueryString("usuarioid") & " de " & RS("cnpj")
											strCorRespEdi = "blue"
											strTxtRespEdi = "O usu�rio '" & NomeUsuario(Request.QueryString("usuarioid")) & "' foi removido."
										Else
											strCorRespEdi = "red"
											strTxtRespEdi = "O usu�rio '" & NomeUsuario(Request.QueryString("usuarioid")) & "' n�o estava relacionado."
										End If
											RSverUsu.Close
											Set RSverUsu = nothing
											
									Case "relacionar"
									
										Set RSverUsu = CreateObject("ADODB.Recordset") 'verificar se o usu�rio j� est� relacionado
											RSverUsu.Open "SELECT usuarioid FROM cliente WHERE usuarioid = " & Request.QueryString("usuarioid") & " AND cnpj = '" & RS("cnpj") & "';", BD
										If RSverUsu.EOF Then 'usu�rio n�o est� relacionado = relacionar
											BD.Execute "INSERT INTO cliente (usuarioid, cnpj) VALUES (" & Request.QueryString("usuarioid") & ", '" & RS("cnpj") & "');"
											BD.Execute "INSERT INTO admin (usuarioid, cnpj) VALUES (" & Request.QueryString("usuarioid") & ", '" & RS("cnpj") & "');"
											Acao "ad_rel_usu relacionou " & Request.QueryString("usuarioid") & " a " & RS("cnpj")
											strCorRespEdi = "blue"
											strTxtRespEdi = "O usu�rio '" & NomeUsuario(Request.QueryString("usuarioid")) & "' foi relacionado."
										Else
											strCorRespEdi = "red"
											strTxtRespEdi = "O usu�rio '" & NomeUsuario(Request.QueryString("usuarioid")) & "' j� estava relacionado."
										End If
											RSverUsu.Close
											Set RSverUsu = nothing
								End Select
								
								
								Dim NmrCNPJ
								NmrCNPJ = RS("CNPJ")
								
								If RS.EOF then 'cnpj inv�lido
									MostrarSubTitulo "CNPJ inv�lido"
									Acao "ad_rel_usu_conf cnpj inv�lido: " & Request.QueryString("cnpjid") & ")"
								else
								
								MostrarSubTitulo "Relacionamento CNPJ " & CNPJ(RS("cnpj"))
								
								MostrarTituloSecao "CNPJ"
									'MostrarConteudoSecao "N�mero:", "<b>" & CNPJ(RS("cnpj")) & "</b>"
									MostrarConteudoSecao "Raz�o social: ", RS("descricao")
									
								if strTxtRespEdi <> empty then 'mostrar mensagem-resultado da edi��o
								
									MostrarTituloSecao "Resultado do relacionamento"
								
									Select Case strCorRespEdi
										Case "red" 'erro
											MostrarConteudoSecao "<b>Erro:</b>", "<font color=""red"">" & strTxtRespEdi & "</font>"
										Case "blue" 'sucesso
											MostrarConteudoSecao "<b>Sucesso:</b>", "<font color=""blue"">" & strTxtRespEdi & "</font>"
									End Select
								end if

								MostrarTituloSecao "Usu�rios atualmente relacionados"
										
										RS.Close
										RS.Open "SELECT usuarioid FROM cliente WHERE cnpj LIKE '" & NmrCNPJ & "%';", BD

											TopoTabelaUsuarios "Raz�o social / Nome do usu�rio", "Apagar"
											Dim intControleCorLinha

										If RS.EOF then 'n�o h� nenhum cnpj cadastrado
											%>
											<tr>
											<td class='row1' align='center' valign='middle' colspan='2'><b>N�o existe nenhum usu�rio relacionado.</b></td>
											
											</tr>
											<tr>
											<td class='row1' align='center' valign=middle colspan='2'>[ <a href="javascript:Confirmar_Apagar_Rel('<%=CNPJ(SoNumeros(NmrCNPJ))%>',<%=SoNumeros(Request.QueryString("cnpjid"))%>);">Apagar</a> ] o relacionamento <%=CNPJ(NmrCNPJ)%></td>
											
											</tr>
											<%
										Else
											
											While Not RS.EOF
												If intControleCorLinha = 1 then intControleCorLinha = 2 Else intControleCorLinha = 1
												%>
												<tr>
												<td class='row<%=intControleCorLinha%>' align='left' valign='middle'><b><%=RazaoSocialUsuario(RS("UsuarioID")) & "<br>" & NomeUsuario(RS("UsuarioID"))%></b></td>
												<td class='row<%=intControleCorLinha%>' align='left' valign='middle'>[&nbsp;<a href='<%=link("ad_rel_usu_conf.asp?modo=apagar&usuarioid=" & RS("usuarioid") & "&cnpjid=" & Request.QueryString("cnpjid"))%>'>Apagar</a>&nbsp;]</td>
												</tr>
												<%
												RS.MoveNext
											Wend
											
										End If 'rs.eof
										
											FimTabelaUsuarios
											
								MostrarTituloSecao "Relacionar usu�rios"
								
										RS.Close
										RS.Open "SELECT id, usuario, nome, razao_social FROM usuario WHERE nivelacesso <> 6 ORDER BY razao_social ASC, nome ASC;", BD
										'Mostrar os nomes de todos os usu�rios que podem ser relacionados (n�o podem ser admin = 6)

											TopoTabelaUsuarios "Raz�o Social / Nome do usu�rio / login", "Relacionar"

										If RS.EOF then 'n�o h� nenhum cnpj cadastrado
											%>
											<tr>
											
											<td class='row1' align='left' valign='middle'>&nbsp;</td>
											<td class='row1' align='left' valign='middle'><b>N�o existe nenhum usu�rio cadastrado.</b></td>
											</tr>
											<%
										Else
											intControleCorLinha = 2
											While Not RS.EOF
												If intControleCorLinha = 1 then intControleCorLinha = 2 Else intControleCorLinha = 1
												%>
												<tr>
												<td class='row<%=intControleCorLinha%>' align='left' valign='middle'><b><%=RS("razao_social") & "</b><br>" & RS("nome") & "<br><b>" & RS("usuario") & ""%></b></td>
												<td class='row<%=intControleCorLinha%>' align='left' valign='middle'>[&nbsp;<a href='<%=link("ad_rel_usu_conf.asp?modo=relacionar&usuarioid=" & RS("id") & "&cnpjid=" & Request.QueryString("cnpjid"))%>'>Relacionar</a>&nbsp;]</td>
												</tr>
												<%
												RS.MoveNext
											Wend
											
										End If 'rs.eof

								FimTabelaUsuarios
								
								End if 'rs.eof
								
								RS.Close
								BD.Close
								set RS = nothing
								set BD = nothing
								%>
								
								<tr><td></td></tr>
								<form ID="Form1">
								<tr>
								<td class='row2' align='center' colspan='4'>
									<!--<input type="button" value="Voltar" class='forminput' ID="Button1" NAME="Voltar" onclick="javascript:window.location='<%=Link("acesso.asp?menu=ad_rel_usu")%>';">-->
									<A href="<%=Link("acesso.asp?menu=ad_rel_usu")%>"><IMG src="<%=Link("img/gif_bot_voltar.gif")%>" alt="Voltar" border="0" title=""></A>&nbsp;&nbsp;&nbsp;
								</td></tr>
								</form>

							
							<!-- Fim do conte�do -->
								
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<%

Private Sub TopoTabelaUsuarios(TituloEsquerdo, TituloDireito)
%>
				<tr>
                 <td valign='top' class='row1' colspan='2'><br>
                 <table cellpadding='4' cellspacing='0' align='center' width='100%' style='border:1px solid ' ID="tbl">
                 <tr>
                   <td class='row2' align='left' width='50%' class='titlemedium'><b><%=TituloEsquerdo%></b></td>
                   <td class='row2' align='left' width='50%' class='titlemedium'><b><%=TituloDireito%></b></td>
                 </tr>
<%
End Sub

Private Sub FimTabelaUsuarios()
	Response.Write "</table></td></tr><tr>"
End Sub

%>

<!-- #include file = "script/fim.asp" -->