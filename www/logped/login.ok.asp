<%
Response.Buffer = false
Response.Expires = 0
%>

<!-- #include file = "script/bloqueio.asp" -->

<%

'Verificar se o POST veio deste dom�nio
    	Dim lblnPost		' user posted data to page?
    	Dim lstrReferer		' page user is comming from
    	Dim lstrHost		' server user is on
    	lblnPost = Request.ServerVariables("REQUEST_METHOD") = "POST"
    	' if data wasn't posted, everything is ok
    	If Not lblnPost Then
    		Response.Redirect "login.asp?erro=542"
    		Response.End
    	End If
    	lstrReferer = Request.ServerVariables("HTTP_REFERER")
    	lstrHost = Request.ServerVariables("HTTP_HOST")
    	' If user is posting from antoher website
    	If InStr(1, lstrReferer, "//" & lstrHost & "/", vbTextCompare) = 0 Then
    		Acao "Login dom�nio inv�lido '" & lstrReferer & "'"
			'Session("Erro") = "Insira novamente seus dados."
			Response.Redirect "login.asp?erro=962"
    		Response.End
    	End If

%>

<!-- #include file = "script/funcao.asp" -->

<%

Set BD = CreateObject("ADODB.Connection")
Set RS = CreateObject("ADODB.Recordset")

BD.Open Application("bd-logped")

RS.Open "Select senha, habilitado From usuario Where usuario = '" & ucase(Form("txtUsuario")) & "';", BD

'Limpar a mensagem de erro
'Session("Erro") = Empty

If RS.EOF Then
	'N�o existe o usu�rio informado
	Acao "Login com usu�rio inv�lido '" & Form("txtUsuario") & "'"
	'Session("Erro") = "O usu�rio informado n�o est� cadastrado."
	Response.Redirect "login.asp?erro=390"
	RS.Close
	BD.Close
	Set BD = nothing
	set RS = nothing
	Response.End
Else
	If lcase(RS("senha")) <> lcase(Form("txtSenha")) Then
		'Senha incorreta
		Acao "Login com senha incorreta '" & Form("txtUsuario") & "'/'" & Form("txtSenha") & "'"
		'Session("Erro") = "A senha informada est� incorreta."
		Response.Redirect "login.asp?erro=859"
		RS.Close
		BD.Close
		Set BD = nothing
		set RS = nothing
		Response.End
	End If
	
	If RS("habilitado") <> 1 then '1 = true
		'Usu�rio desabilitado
		Acao "Login com usu�rio desabilitado '" & Form("txtUsuario") & "'"
		'Session("Erro") = "O usu�rio informado est� <u>desabilitado</u>."
		Response.Redirect "login.asp?erro=693"
		RS.Close
		BD.Close
		Set BD = nothing
		set RS = nothing
		Response.End
	End If
End If

RS.Close 'Valida��o do usu�rio e senha

RS.Open "Select id, nivelacesso, nome, usuario, dataaltsenha From usuario Where usuario = '" & ucase(Form("txtUsuario")) & "' AND senha = '" & Form("txtSenha") & "';", BD

If Not RS.EOF Then
	Session("ID") = RS("id")
	Session("Nivel") = RS("nivelacesso")
	Session("Nome") = RS("nome")
	Session("Usuario") = RS("usuario")
	
		'Localizar �ltimo acesso
		Set RSInfo = CreateObject("ADODB.Recordset")
		RSInfo.Open "SELECT MAX(inicio), COUNT(inicio) FROM sessao WHERE usuarioid = " & RS("id") & ";", BD
			Dim dadosUltimoAcesso
			dadosUltimoAcesso = rsinfo.GetString(,,"|||")
			
			If mid(dadosUltimoAcesso, 1, 4) <> "|||0" then 'n�o � primeiro acesso
				dim dataUltimoAcesso
				dataUltimoAcesso = Mid(dadosUltimoAcesso, 1, InStr(1, dadosUltimoAcesso, "|||") - 1)
				Session("UltimoAcesso") = "�ltimo acesso: " & FormatDateTime(dataUltimoAcesso, 1) & " �s " & FormatDateTime(dataUltimoAcesso, 3)
				'N�mero de acessos
				Session("NAcessos") = Mid(dadosUltimoAcesso, InStr(1, dadosUltimoAcesso, "|||") + 3) + 1
			Else
				Session("UltimoAcesso") = "Este � o seu primeiro acesso, seja bem-vindo."
				Session("NAcessos") = "1"
			End If
		
		
		RSInfo.Close
		'Fim Localizar �ltimo acesso

		'Verificar se � administrador de alguma empresa
		RSInfo.Open "SELECT cnpj FROM admin WHERE usuarioid = " & Session("ID") & ";", BD, 1, 3
			If Not RSInfo.EOF Then
				
				while not RSInfo.EOF
					Session("AdminCNPJ") = Session("admincnpj") & " " & CortaCNPJ(RSInfo("cnpj"))
					RSInfo.MoveNext
				wend
					'Tirar v�rgula inicial
					'Session("AdminCNPJ") = Right(Session("AdminCNPJ"), Len(Session("AdminCNPJ")) - 1)
					'Tirar espa�o inicial
					Session("AdminCNPJ") = trim(Session("AdminCNPJ"))
			else
				Session("AdminCNPJ") = "zero"
			End If
			
			RSInfo.Close
		'Fim Verificar se � administrador de alguma empresa

		'Verificar se tem clientes para gerenciar
		RSInfo.Open "SELECT cnpj FROM cliente WHERE usuarioid = " & Session("ID") & ";", BD, 1, 3
			If Not RSInfo.EOF Then

				while not RSInfo.EOF
					Session("CNPJCli") = Session("CNPJCli") & " " & CortaCNPJ(RSInfo("cnpj"))
					RSInfo.MoveNext
				wend
					'Tirar v�rgula inicial
					Session("CNPJCli") = Right(Session("CNPJCli"), Len(Session("CNPJCli")) - 1)
					'Tirar espa�o inicial
					Session("CNPJCli") = trim(Session("CNPJCli"))
			else
				Session("CNPJCli") = "zero"
			End If

			RSInfo.Close
			Set RSInfo = Nothing
		'Fim Verificar se tem clientes para gerenciar
		
		'Vincular a sess�o ao usu�rio
		BD.Execute "UPDATE sessao SET usuarioid = " & RS("id") & " WHERE sessaoid = '" & Session("Controle") & "';"
		
		Acao "Efetuou login '" & ucase(Form("txtUsuario")) & "'"
	
	
	'Verifica��o de expira��o da senha
	'� necess�rio troc�-la a cada 90 dias,
	'Caso n�o seja feito, a Se��o de Altera��o
	'de Senha � exibida em cada login
	'at� q o usu�rio a altere.

		Dim DataAltSenha, PrecisaAltSenha
		DataAltSenha = CDate(RS("DataAltSenha"))
		
			If IsNull(DataAltSenha) Then
				PrecisaAltSenha = true
			ElseIf DateDiff("d", DataAltSenha, Date) >= 90 Then
				PrecisaAltSenha = true
			Else
				PrecisaAltSenha = false
			End If
					
		'Caso n�o seja necess�rio troc�-la, o
		'usu�rio � redirecionado para
		'a p�gina inicial normalmente.

			'Limpa msg de erro
			'Session("Erro") = Empty

			If Not PrecisaAltSenha AND isSenha(Form("txtSenha")) then
				Response.Redirect Link("acesso.asp")
			else
				Acao("Alerta para altera��o de senha. DataAltSenha:" & DataAltSenha)
				Response.Redirect Link("acesso.asp?menu=altsenha")
			end if

Else

	Acao "Acesso negado. Us: '" & Form("txtUsuario") & "', Se: '" & Form("txtSenha") & "'"

	'Session("Erro") = "Usu�rio ou senha incorreta."
	Response.Redirect "login.asp?erro=421"

End If

RS.Close
BD.Close
Set BD = nothing
set RS = nothing
%>