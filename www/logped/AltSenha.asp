<%@ Language=VBScript %>

<!-- #include file = "script/bloqueio.asp" -->

<!-- #include file = "script/logado.asp" -->

<!-- #include file = "script/funcao.asp" -->

<!-- #include file = "script/seguir.asp" -->

<!-- #include file = "script/topo.asp" -->

<% 'VerificarNovosDados %>

<table width='95%' bgcolor='#37833E' align='center' border="0" cellspacing="1" cellpadding="0" ID="tblCentral">
		<tr>
			<td class='maintitle'>
				<table width='100%' border='0' cellspacing='0' cellpadding='3' id="tblSubCentral">
					<tr>
						<td><IMG src='<%=Link("img/gif_ico_nav_titulo.gif")%>' alt='' width="8" height="8" border="0"></td>
						<td width='100%' class='maintitle'>Central de controle</td>
						<td align='right' class='maintitle' nowrap></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
		<td>
			<table class='mainbg' width="100%" border="0" cellspacing="1" cellpadding="4" ID="tblCeont">
				<tr>
					<td nowrap class='titlemedium'>&nbsp;</td>
					<td width="100%" nowrap class='titlemedium'>&nbsp;</td>
				</tr>
				<tr>
					<td class='row1' valign="top">
						<!-- #include file = "script/montarMenu.asp" -->
					</td>
					<td valign='top' class='row1'>
						<table cellpadding='4' cellspacing='3' width='100%' align='left' border='0' ID="tblMostrar">
							
							<!-- In�cio do conte�do -->
							
								<%
									
								'Verifica��es para altera��o de senha:
									Dim SenhaAtual, NovaSenha, Rejeitar

									Rejeitar = Empty
									SenhaAtual = lcase(Form("SenhaAtual"))
									NovaSenha = lcase(Form("NovaSenha"))

									If SenhaAtual = Empty OR NovaSenha = Empty then
										Rejeitar = "|Voc� deve completar o formul�rio inteiramente antes de prosseguir"
									End If

									If SenhaAtual = NovaSenha then
										Rejeitar = Rejeitar & "|Voc� deve escolher uma <b>nova</b> senha diferente da sua atual"
									End If

									If lcase(Form("NovaSenha")) <> lcase(Form("NovaSenhaConf")) then
										Rejeitar = Rejeitar & "|A confirma��o e a nova senha n�o conferem"
									End If

									'If Len(NovaSenha) > 5 then
									'	Rejeitar = Rejeitar & "|Voc� deve usar 5 caracteres no m�ximo"
									'End If

									If Len(NovaSenha) <> 5 then
										Rejeitar = Rejeitar & "|Use cinco caracteres"
									End If

									If lcase(NovaSenha) = lcase(Session("Usuario")) then
										Rejeitar = Rejeitar & "|N�o use seu nome de usu�rio como senha"
									End If
									
									If Not isSenha(NovaSenha) then
										Rejeitar = Rejeitar & "|Use letras <b>E</b> n�meros"
									End If

									Set BD = CreateObject("ADODB.Connection")
									Set RS = CreateObject("ADODB.Recordset")

									BD.Open Application("bd-logped")
									RS.Open "Select senha From usuario Where id = " & Session("ID") & ";", BD

									If lcase(Form("SenhaAtual")) <> lcase(RS("senha")) AND Form("SenhaAtual") <> Empty Then
										Rejeitar = Rejeitar & "|Sua <b>Senha atual</b> est� incorreta"
									End If

									RS.Close
									set rs=nothing
								
									'Erros:
									
									If Rejeitar <> Empty then
									'Ocorreu um erro
									
									Acao "Erro na altera��o de senha (Atual:" & SenhaAtual & ", Nova:" & NovaSenha & "): " & Rejeitar
									MostrarSubTitulo "Erro na altera��o de senha"
								%>
								
									<tr> 
									<td class='row1' valign='top'>
										A senha n�o foi alterada por algum erro.<br><br>
									<b>Segue o relat�rio do(s) erro(s):</b>
										<br>
										<span class='highlight'>
										<%=Replace(Rejeitar, "|", "<br>&#149; ")%><br><br>Por favor, retorne e cheque os dados digitados.</span>
										<br><br>
										<br><br>
										<b>Para evitar outros erros, siga as seguintes recomenda��es:</b>
										<br><br>
										&#149; Use 5 caracteres<br>
										&#149; N�o use seu nome de usu�rio como senha<br>
										&#149; Use letras e n�meros</p>
									</td>
									</tr>
									<tr> 
									<td class='titlefoot' align='center'><A href="javascript:history.go(-1);"><IMG src="<%=Link("img/gif_bot_voltar.gif")%>" alt="Voltar" border="0" title=""></A></td>
									</tr>
								<%
								else 'senha alterada
								
								BD.Execute "UPDATE usuario SET senha = '" & lcase(FormataSenha(NovaSenha)) & "', dataaltsenha = '" & DataSQL(Date) & "' WHERE id = " & Session("ID") & ";"
								Acao "Altera��o de senha (Atual:" & SenhaAtual & ", Nova:" & FormataSenha(NovaSenha) & ")"
								MostrarSubTitulo "Altera��o de senha efetivada"
								%>
									<tr> 
									<td class='row1' valign='top'>
										<img src="<%=Link("img/gif_ico_aviso.gif")%>">&nbsp;&nbsp;Sua senha foi alterada com sucesso.<br><br>
										<br><br>
										<b>Por motivos de seguran�a, voc� ser� avisado(a) a alter�-la novamente em 90 dias.</b>
										<br><br></p>
									</td>
									</tr>
									<tr> 
									<td class='titlefoot' align='center'><A href="<%=Link("acesso.asp")%>"><IMG src="<%=Link("img/gif_bot_voltar.gif")%>" alt="Voltar" border="0" title=""></A></td>
									</tr>								
								<%
								end if 'teve erro?
								
								BD.Close
								set bd=nothing

								%>
									
							<!-- Fim do conte�do -->
								
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!-- #include file = "script/fim.asp" -->