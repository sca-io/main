<%
session.LCID = 1046
'--------------
'Fun��es do Sistema
'Verificar e alterar a fun��o "Link"
'> definir protocolo e pasta
'--------------
'10/10/03 - 18:45
'15/11/03 - 22:14
'09/01/04 - 17:45
'__FUN��ES USADAS PELO SISTEMA_____________________________________________________________________

Sub VerificarNovosDados()
Server.ScriptTimeOut = 1000
if Session("ID") = empty then Session("ID") = "0"

		'Fun��o que importa os dados do bd
		'da SCA com as informa��es de carregamento
		'e as grava numa tabela tempor�ria [Importar]
		
		'Caso j� exista um procedimento de importa��o, n�o pode-se executar outro
		if ContarRegistrosPreImportados <> "zero" then exit sub

		'Antes de tudo, compactar e reparar o bancos de dado da SCA
		CompactaDB Application("cam-dbs") & "logistica.mdb", "true"

		'Inserir dados do bd da SCA no bd da Log�stica, numa tabela tempor�ria [Importar]
		ImportarDadosAccessParaDBA()

		Set BDImpSCA = CreateObject("ADODB.Connection")

		'Processar os pedidos
		'Verificar os pedidos, caso existam, verificar se
		'as informa��es s�o novas
		BDImpSCA.Open Application("bd-logped")
		
		Set RSImp = CreateObject("ADODB.Recordset") 'Tabela [Importar]

		RSImp.Open "SELECT * FROM importar ORDER BY cnpj_cliente ASC;", BDImpSCA 'Dados tempor�rios para importa��o
		
		If RSImp.BOF = False AND RSImp.EOF = False then 'Existem dados a serem processados
		
			Set RSNotaFiscal = CreateObject("ADODB.Recordset")
			Set RS_Verificar_Carregado = CreateObject("ADODB.Recordset") 'verifica se o pedido j� est� registrado nos Carregados


			While not RSImp.EOF
				'Apenas insere as informa��es do pedido, para depois verificar os carregamentos

				if CStr(PedidoID(RSImp("n_pedido"), RSImp("modalidade"), RSImp("cnpj_cliente"))) = "nenhum" then
					'O pedido importado � novo (ainda � h� acompanhamento), deve-se inclu�-lo
					
					'Antes deve-se verificar se ele j� n�o est� no hist�rico
					
					RS_Verificar_Carregado.Open "SELECT id FROM carregado WHERE n_pedido = " & RSImp("n_pedido") & " AND modalidade = '" & RSImp("modalidade") & "' AND nome_usina = '" & RSImp("nome_usina") & "' AND cnpj = '" & CortaCNPJ(RSImp("cnpj_cliente")) & "';", BDImpSCA
					
					iF RS_Verificar_Carregado.EOF then
						'O pedido ainda � foi carregado,
						'deve-se cadastr�-lo como Novo
					
							BDImpSCA.Execute "INSERT INTO andamento(n_pedido, modalidade, nome_usina, cnpj, produto, data_emissao, volume, volume_devolvido, volume_carregado, nota_fiscal, novo, atualizado, inseridoem) VALUES (" & RSImp("n_pedido") & ", '" & RSImp("modalidade") & "', '" & RSImp("nome_usina") & "', '" & CortaCNPJ(RSImp("cnpj_cliente")) & "', '" & RSImp("nome_produto") & "', '" & DataSQL(RSImp("emissao_pedido")) & "', " & NumeroSQL(RSImp("volume")) & ", 0, 0, '" & RSImp("nota_fiscal") & "', 1, 0, '" & DataSQL(Now()) & " " & Time() & "');"
												
							BDImpSCA.Execute "INSERT INTO carregar(pedidoid, volume, volume_devolvido, quando, sessaoid, usuarioid, nota_fiscal) VALUES (" & PedidoID(RSImp("n_pedido"), RSImp("modalidade"), CortaCNPJ(RSImp("cnpj_cliente"))) & ", " & NumeroSQL(RSImp("volume_carregado")) & ", " & NumeroSQL(RSImp("volume_devolvido")) & ", '" & DataSQL(RSImp("data_emissao")) & "', '0', 0, '" & RSImp("nota_fiscal") & "');"
					
					
					else 'pedido j� carregado (finalizado), tratar as notas fiscais
					
									'Verificar se o carregamento j� foi registrado
									RSNotaFiscal.Open "SELECT pedidoid FROM carregar WHERE nota_fiscal = '" & RSImp("nota_fiscal") & "' AND pedidoid = " & PedidoID_bkp(RSImp("n_pedido"), RSImp("modalidade"), CortaCNPJ(RSImp("cnpj_cliente"))) & ";", BDImpSCA
								
									'Nota fiscal n�o foi registrada ainda
									If RSNotaFiscal.EOF AND RSNotaFiscal.BOF then				
										'O pedido j� existe, por�m este � um carregamento n�o registrado
										BDImpSCA.Execute "INSERT INTO carregar(pedidoid, volume, volume_devolvido, quando, sessaoid, usuarioid, nota_fiscal) VALUES ('" & PedidoID_bkp(RSImp("n_pedido"), RSImp("modalidade"), CortaCNPJ(RSImp("cnpj_cliente"))) & "', '" & NumeroSQL(RSImp("volume_carregado")) & "', '" & NumeroSQL(RSImp("volume_devolvido")) & "', '" & DataSQL(RSImp("data_emissao")) & " " & Time & "', '0', '0', '" & RSImp("nota_fiscal") & "');"
									Else 
										'A nota fiscal j� existe - deve-se atualiz�-la
										BDImpSCA.Execute "UPDATE carregar SET volume = '" & NumeroSQL(RSImp("volume_carregado")) & "', volume_devolvido = '" & NumeroSQL(RSImp("volume_devolvido")) & "', quando = '" & DataSQL(RSImp("data_emissao")) & "' WHERE nota_fiscal = '" & RSImp("nota_fiscal") & "' AND pedidoid = " & PedidoID(RSImp("n_pedido"), RSImp("modalidade"), CortaCNPJ(RSImp("cnpj_cliente"))) & ";"
									End If
									
									RSNotaFiscal.Close
					
					eND iF
					
					RS_Verificar_Carregado.Close
					
				else
				
					'Verificar se o carregamento j� foi registrado
					RSNotaFiscal.Open "SELECT pedidoid FROM carregar WHERE nota_fiscal = '" & RSImp("nota_fiscal") & "' AND pedidoid = " & PedidoID(RSImp("n_pedido"), RSImp("modalidade"), CortaCNPJ(RSImp("cnpj_cliente"))) & ";", BDImpSCA
				
					'Nota fiscal n�o foi registrada ainda
					If RSNotaFiscal.EOF AND RSNotaFiscal.BOF then				
						'O pedido j� existe, por�m este � um carregamento n�o registrado
						BDImpSCA.Execute "INSERT INTO carregar(pedidoid, volume, volume_devolvido, quando, sessaoid, usuarioid, nota_fiscal) VALUES ('" & PedidoID(RSImp("n_pedido"), RSImp("modalidade"), CortaCNPJ(RSImp("cnpj_cliente"))) & "', '" & NumeroSQL(RSImp("volume_carregado")) & "', '" & NumeroSQL(RSImp("volume_devolvido")) & "', '" & DataSQL(RSImp("data_emissao")) & " " & Time & "', '0', '0', '" & RSImp("nota_fiscal") & "');"
						'Marcar o pedido como atualizado
						BDImpSCA.Execute "UPDATE andamento SET atualizado = 1, dataultatualizacao = NOW() WHERE id = '" & PedidoID(RSImp("n_pedido"), RSImp("modalidade"), CortaCNPJ(RSImp("cnpj_cliente"))) & "';"
					Else 
						'A nota fiscal j� existe - deve-se atualiz�-la
						BDImpSCA.Execute "UPDATE carregar SET pedidoid = '" & PedidoID(RSImp("n_pedido"), RSImp("modalidade"), CortaCNPJ(RSImp("cnpj_cliente"))) & "', volume = '" & NumeroSQL(RSImp("volume_carregado")) & "', volume_devolvido = '" & NumeroSQL(RSImp("volume_devolvido")) & "', quando = '" & DataSQL(RSImp("data_emissao")) & "' WHERE nota_fiscal = '" & RSImp("nota_fiscal") & "' AND pedidoid = " & PedidoID(RSImp("n_pedido"), RSImp("modalidade"), CortaCNPJ(RSImp("cnpj_cliente"))) & ";"
					End If
					
					RSNotaFiscal.Close
					
					'Atualizar o volume do pedido, segundo a nota fiscal
						BDImpSCA.Execute "UPDATE andamento SET volume = " & NumeroSQL(RSImp("volume")) & " WHERE id = '" & PedidoID(RSImp("n_pedido"), RSImp("modalidade"), CortaCNPJ(RSImp("cnpj_cliente"))) & "';"
						
					
				end if
				
				BDImpSCA.Execute "DELETE FROM importar WHERE nota_fiscal = '" & RSImp("nota_fiscal") & "' AND cnpj_cliente = '" & RSImp("cnpj_cliente") & "';"

				RSImp.MoveNext
			Wend
			
				Set RSNotaFiscal = Nothing



		'Atualizar pedidos carregados
		'e as informa��es dos ped. em andamento

		Set RSAnd = CreateObject("ADODB.Recordset")
		'Atualizar as informa��es dos pedidos em andamento
		RSAnd.Open "SELECT id FROM andamento;", BDImpSCA
		
		While Not RSAnd.EOF
			'Atualizar pedido

			BDImpSCA.Execute "SELECT @volant:=SUM( carregar.volume ),@voldevant:=SUM( carregar.volume_devolvido ) FROM carregar WHERE carregar.pedidoid = " & RSAnd("id") & ";"
			'BDImpSCA.Execute "SELECT @volantbkp:=SUM( carregar_bkp.volume ),@voldevantbkp:=SUM( carregar_bkp.volume_devolvido ) FROM carregar_bkp WHERE carregar_bkp.pedidoid = " & RSAnd("id") & ";"
			BDImpSCA.Execute "UPDATE andamento SET andamento.volume_carregado = @volant, andamento.volume_devolvido = @voldevant WHERE andamento.id = " & RSAnd("id") & " ;"

			
			RSAnd.MoveNext
		Wend
		
		RSAnd.Close
		
		'Atualizar as informa��es dos pedidos carregados
		RSAnd.Open "SELECT id FROM carregado;", BDImpSCA
		
		While Not RSAnd.EOF
			'Atualizar pedido

			BDImpSCA.Execute "SELECT @volant:=SUM( carregar.volume ),@voldevant:=SUM( carregar.volume_devolvido ) FROM carregar WHERE carregar.pedidoid = " & RSAnd("id") & ";"
			'BDImpSCA.Execute "SELECT @volantbkp:=SUM( carregar_bkp.volume ),@voldevantbkp:=SUM( carregar_bkp.volume_devolvido ) FROM carregar_bkp WHERE carregar_bkp.pedidoid = " & RSAnd("id") & ";"
			BDImpSCA.Execute "UPDATE carregado SET carregado.volume_carregado = @volant, carregado.volume_devolvido = @voldevant WHERE carregado.id = " & RSAnd("id") & " ;"
			'BDImpSCA.Execute "UPDATE carregado_bkp SET carregado_bkp.volume_carregado = @volant + @volantbkp, carregado_bkp.volume_devolvido = @voldevant + @voldevantbkp WHERE carregado_bkp.id = " & RSAnd("id") & " ;"
			
			RSAnd.MoveNext
		Wend
		
		RSAnd.Close

		RSAnd.Open "SELECT * FROM andamento WHERE (volume_carregado - volume_devolvido) >= volume;", BDImpSCA

		While not RSAnd.EOF 'enviar os pedidos totalmente carregados para o hist�rico

			'Mandar pedido para o Hist�rico

					Dim EncerradoEm
					EncerradoEm = DataSQL(Now()) & " " & Time()

					'Inserir na tabela de pedidos carregados
					
					'Verificar se o pedido j� est� no hist�rico
					RS_Verificar_Carregado.Open "SELECT id FROM carregado WHERE n_pedido = " & RSAnd("n_pedido") & " AND modalidade = '" & RSAnd("modalidade") & "' AND nome_usina = '" & RSAnd("nome_usina") & "' AND cnpj = '" & CortaCNPJ(RSAnd("cnpj")) & "';", BDImpSCA
					
					if NOT RS_Verificar_Carregado.EOF then
						'o pedido j� est� no hist�rico, deve-se mov�-lo para o carregado_bkp
								BDImpSCA.Execute "INSERT INTO carregado_bkp SELECT * FROM carregado WHERE id = " & RS_Verificar_Carregado("id")
								BDImpSCA.Execute "DELETE FROM carregado WHERE id = " & RS_Verificar_Carregado("id")
							Acao "Pedido duplicado no hist (movido_bkp) id " & RS_Verificar_Carregado("id")
					end if
					
						'O pedido n�o est� no hist�rico, deve-se inclu�-lo
						BDImpSCA.Execute "SELECT @volcarregadohist:=SUM( carregar.volume ),@voldevolvidohist:=SUM( carregar.volume_devolvido ) FROM carregar WHERE carregar.pedidoid = " & RSAnd("id") & ";"
						BDImpSCA.Execute "INSERT INTO carregado VALUES (" & RSAnd("id") & ", " & RSAnd("n_pedido") & ", '" & RSAnd("modalidade") & "', '" & RSAnd("nome_usina") & "', '" & CortaCNPJ(RSAnd("cnpj")) & "', '" & RSAnd("produto") & "', '" & RSAnd("nota_fiscal") & "', '" & DataSQL(RSAnd("data_emissao")) & "', '" & RSAnd("volume") & "', @volcarregadohist, @voldevolvidohist, '" & DataSQL(RSAnd("inseridoem")) & " " & FormatDateTime(RSAnd("inseridoem"), 4) & "',  '" & EncerradoEm & "',  " & Session("id") & ");"
						Acao "Pedido movido hist (ID " & RSAnd("id") & " - " & RSAnd("n_pedido") & "-" & RSAnd("modalidade") & ")"

					
					RS_Verificar_Carregado.Close
						
					'Deletar de ped em carregamento, pois j� est� no hist�rico
					BDImpSCA.Execute "DELETE FROM andamento WHERE id = " & RSAnd("id") & ";"

			RSAnd.MoveNext
		Wend

		RSAnd.Close
		
		set RSAnd = nothing
		set RS_Verificar_Carregado = nothing

		End If

		RSImp.Close
		Set RSImp = Nothing

		BDImpSCA.Close
		Set BDImpSCA = nothing

End Sub

'__________________________________________________________________________________________________


Sub ImportarDadosAccessParaDBA()

	dsn2 = Application("bd-logped")
	
	tabla1 = "logistica" ' this is the table from where you want to export
	tabla2 = "importar" ' this is the table that receives the data
	
	basedatos1 = Application("cam-dbs") & "logistica.mdb" ' The full path in the server
			
	ncampos = 11 ' number of columns that you want to export/import
	columna = Array("cnpj_cliente", "nome_usina", "modalidade", "n_pedido", "nome_produto", "nota_fiscal", "data_emissao", "volume", "volume_carregado", "volume_devolvido", "emissao_pedido") ' put as names as number of columns into the array 


'>>>>>>>>>>>>>>>>>>>>>>>>>>>Here we delete all the data from the table 2
			Set objConn = Server.CreateObject("ADODB.Connection")
					objConn.Open dsn2
			'SQL= "DELETE FROM " & tabla2 & ""
			'objConn.Execute(SQL)
			'objConn.close

'>>>>>>>>>>>>>>>>>>>>>>>>>>>Here we open the big database for reading the data database1
		
				Set objConn = Server.CreateObject("ADODB.Connection")

					'objConn.Provider="Microsoft.Jet.OLEDB.4.0"  ' ***** Uncomment this line if you don't use DSN
					'objConn.Mode = 1
					'objConn.Open basedatos1
					objConn.Open Application("sca-dbs") & "logistica.mdb"

				set rs1 = Server.CreateObject ("ADODB.RecordSet")

				SQL1= "SELECT * from " & tabla1  
				rs1.Source = SQL1
				rs1.Activeconnection = objConn
				rs1.CursorType = 0
				rs1.LockType = 1
				rs1.open

'>>>>>>>>>>>>>>>>>>>>>>>>>>> here is where we make the loop for updating the data in the second database
			if rs1.EOF = false AND rs1.BOF = false then
				
				Dim cnUpdate,rsupdate,  SQLu, i
				
				
				
				Set cnUpdate = Server.CreateObject("ADODB.Connection")
				cnUpdate.Open dsn2

				do while not rs1.EOF
					SQLu= "INSERT INTO " & tabla2 & " ("
						for i = 0 to ncampos-1  ' loop for reading the fields (columns)
							SQLu= SQLu & columna(i)     
							if i <> ncampos-1 then SQLu = SQLu & "," 'filtering the ","
							next
							SQLu = SQLu & ") VALUES ("
							for i = 0 to ncampos-1  ' number of columns
							
								'Tratamento de tipo de dados
								Select case Left(columna(i), 6)
									Case "data_e", "emissao_p"
										SQLu= SQLu & "'" & DataSQL(rs1(columna(i))) & "'"
									Case "emissa"
										SQLu= SQLu & "'" & DataSQL(rs1(columna(i))) & "'"
									Case "volume"
										SQLu= SQLu & "'" & NumeroSQL(rs1(columna(i))) & "'"  
									Case else
										SQLu= SQLu & "'" & rs1(columna(i)) & "'"
								End select
							
							if i <> ncampos-1 then SQLu = SQLu & "," 
						next
					SQLu = SQLu & ")"

					Set rsupdate = cnUpdate.Execute(SQLu) 'executing the SQL insert.
					rs1.movenext
				loop
				
				
				

		'Apagar todos os dados tempor�rios do bd da SCA
		objConn.execute "DELETE * FROM logistica;"
		'Optimizar a tabela importar
		cnUpdate.Execute "REPAIR TABLE importar;"
		cnUpdate.Execute "OPTIMIZE TABLE importar;"
		
				cnUpdate.close
				set cnUpdate = nothing
				
		end if 'rs1.EOF = false AND rs1.BOF = false
				
				
				rs1.close
				set rs1 = nothing
				objConn.close
				set objConn = nothing
				
				
'>>>>>>>>>>>>>>>>>>here we draw the link to the updated file

End Sub


'__________________________________________________________________________________________________


Function ContarRegistrosPreImportados()
	'Retorna o n� de pedidos em processamento da tabela Importar

		Set BDPreImpSCA = CreateObject("ADODB.Connection")
		BDPreImpSCA.Open Application("bd-logped")
		Set RSPreImp = CreateObject("ADODB.Recordset") 'Tabela [Importar]
		
		RSPreImp.Open "SELECT COUNT(*) FROM importar;", BDPreImpSCA
		if RSPreImp.GetString <> "0" AND RSPreImp.EOF = false then
			ContarRegistrosPreImportados = CStr(RSPreImp.GetString)
		else
			ContarRegistrosPreImportados = "zero"
		end if
		RSPreImp.Close
		set RSPreImp = nothing
		BDPreImpSCA.Close
		set BDPreImpSCA = nothing

end Function

'__________________________________________________________________________________________________


Function SoNumeros(ParteTxt) 'Retorna somente os n�meros de uma string

	if ParteTxt = Null OR ParteTxt = Empty then
		SoNumeros = "0"
		exit function
	end if
	
	Dim xTemp, Caracter, TermoTemp
	
	For xTemp = 1 To Len(ParteTxt)

    Caracter = Mid(ParteTxt, xTemp, 1)
    
		If Asc(Caracter) >= 48 And Asc(Caracter) <= 57 Then
			TermoTemp = TermoTemp & Caracter
		End If
        
    Next
    
    SoNumeros = TermoTemp

End Function

'__________________________________________________________________________________________________


Function CNPJ(Numero)
' Formata��o do CNPJ

	If Len(Numero) = 8 then
		CNPJ = Left(Numero, 2) & "." & Mid(Numero, 3, 3) & "." & Mid(Numero, 6, 3) & "/0000-00"
	Elseif Len(Numero) <> 14 then
		CNPJ = Numero
	Else
		Numero = SoNumeros(Numero)
		CNPJ = Left(Numero, 2) & "." & Mid(Numero, 3, 3) & "." & Mid(Numero, 6, 3) & "/" & Mid(Numero, 9, 4) & "-" & Mid(Numero, 13, 2)
	End If

End Function

'__________________________________________________________________________________________________


Function Link(Pagina)
	'Gera um link: ex http://www.site.com.br

	If left(Pagina, 7) = "http://" then
		Link = trim(Pagina)
		exit Function
	End If

	Dim PgLink, dominio, protocolo, pasta, raiz

	''''''''''
	protocolo	= "http://"		'http:// ou https://
	pasta		= "/logped/"	'subpasta do sistema
	''''''''''
	
	dominio = request.servervariables("SERVER_NAME")
	
	'if dominio = "rud" then pasta = "/sca" & pasta

	if left(pasta, 1) <> "/" then pasta = "/" & pasta
	if protocolo <> "http://" AND protocolo <> "https://" Then protocolo = "http://"

	raiz = protocolo & dominio & pasta

	Select Case lcase(Pagina)
		Case "sca"
			PgLink = protocolo & dominio
		Case "raiz"
			PgLink = raiz
		Case "login"
			PgLink = raiz & "/login.asp"
		Case "timeout"
			PgLink = raiz & "/login.asp?timeout"
		Case "urlexterna"
			Link = raiz
			Exit function
		Case Empty
			PgLink = raiz & "acesso.asp"
		Case Else
			PgLink = raiz & Pagina
	End Select

	If InStr(1, Pagina, "img/") > 0 Then 'se � est� na pasta de imagens (img)
		Link = PgLink
	ElseIf InStr(1, Pagina, "?") = 0 Then 'Verificar se j� h� infos via get
		Link = PgLink & "?sid=" & Session("Controle")
	Else
		Link = PgLink & "&sid=" & Session("Controle")
	End If

End Function

'__________________________________________________________________________________________________


Function CortaCNPJ(NmrCNPJ)
	'Retorna apenas os n�meros do CNPJ referentes � empresa (antes da barra)

	CortaCNPJ = Mid(SoNumeros(NmrCNPJ), 1, 8)

End Function


'__________________________________________________________________________________________________

function pedidoid(n_pedido, modalidade, cnpj)

	set bdpedidoid = createobject("adodb.connection")
	set rspedidoid = createobject("adodb.recordset")
	bdpedidoid.open application("bd-logped")
	
	 rspedidoid.open "select id from andamento where n_pedido = " & replace(n_pedido, "'", "''") & " and modalidade = '" & replace(modalidade, "'", "''") & "' and cnpj = '" & cortacnpj(cnpj) & "';", bdpedidoid
	
	if rspedidoid.eof and rspedidoid.bof then
		'Verificar se j� n�o foi carregado
			rspedidoid.Close
				rspedidoid.open "select id from carregado where n_pedido = " & replace(n_pedido, "'", "''") & " and modalidade = '" & replace(modalidade, "'", "''") & "' and cnpj = '" & cortacnpj(cnpj) & "';", bdpedidoid
					
					if rspedidoid.eof and rspedidoid.bof then
						pedidoid = "nenhum"
					else
						pedidoid = rspedidoid("id")
					end if

	else
		pedidoid = rspedidoid("id")
		'>//>'response.write "pedidoid: " & pedidoid & "</span><br>"
	end if
	
	rspedidoid.close
	set rspedidoid = nothing
	bdpedidoid.close
	set bdpedidoid = nothing
	
end function

'Function PedidoID(N_Pedido, Modalidade, CNPJ)
'	'Retorna o ID do sistema referente ao pedido em andamento
'
'	Set BDPedidoID = CreateObject("ADODB.Connection")
'	Set RSPedidoID = CreateObject("ADODB.Recordset")
'	BDPedidoID.Open Application("bd-logped")
'	
'	RSPedidoID.Open "SELECT ID FROM [Andamento] WHERE N_Pedido = " & Replace(N_Pedido, "'", "''") & " AND Modalidade = '" & Replace(Modalidade, "'", "''") & "' AND CNPJ = '" & CortaCNPJ(CNPJ) & "';", BDPedidoID', BDImpSCA
'	'>//>'response.Write "<span style=""background-color: #C0C0C0"">SELECT ID FROM [Andamento] WHERE N_Pedido = " & Replace(N_Pedido, "'", "''") & " AND Modalidade = '" & Replace(Modalidade, "'", "''") & "' AND CNPJ = '" & CortaCNPJ(CNPJ) & "';<BR>"
'	
'	If RSPedidoID.EOF AND RSPedidoID.BOF then
'		PedidoID = "nenhum"
'		'>//>'response.Write "PedidoID: " & PedidoID & "</span><BR>"
'	Else
'		PedidoID = RSPedidoID("ID")
'		'>//>'response.Write "PedidoID: " & PedidoID & "</span><BR>"
'	End If
'	
'	RSPedidoID.Close
'	Set RSPedidoID = Nothing
'	BDPedidoID.Close
'	Set BDPedidoID = Nothing
'	
'End Function

'__________________________________________________________________________________________________

function PedidoID_bkp(n_pedido, modalidade, cnpj)

	set bdPedidoID_bkp = createobject("adodb.connection")
	set rsPedidoID_bkp = createobject("adodb.recordset")
	bdPedidoID_bkp.open application("bd-logped")
	
	 rsPedidoID_bkp.open "select id from carregado where n_pedido = " & replace(n_pedido, "'", "''") & " and modalidade = '" & replace(modalidade, "'", "''") & "' and cnpj = '" & cortacnpj(cnpj) & "';", bdPedidoID_bkp
	
	if rsPedidoID_bkp.eof and rsPedidoID_bkp.bof then
		'Verificar se j� n�o foi carregado
			rsPedidoID_bkp.Close
				rsPedidoID_bkp.open "select id from carregado where n_pedido = " & replace(n_pedido, "'", "''") & " and modalidade = '" & replace(modalidade, "'", "''") & "' and cnpj = '" & cortacnpj(cnpj) & "';", bdPedidoID_bkp
					
					if rsPedidoID_bkp.eof and rsPedidoID_bkp.bof then
						PedidoID_bkp = "nenhum"
					else
						PedidoID_bkp = rsPedidoID_bkp("id")
					end if

	else
		PedidoID_bkp = rsPedidoID_bkp("id")
		'>//>'response.write "PedidoID_bkp: " & PedidoID_bkp & "</span><br>"
	end if
	
	rsPedidoID_bkp.close
	set rsPedidoID_bkp = nothing
	bdPedidoID_bkp.close
	set bdPedidoID_bkp = nothing
	
end function

'__________________________________________________________________________________________________

Sub Acao(Realizado)
	'Grava no banco de dados um relat�rio das a��es dos usu�rios

	Set BDRegAcao = CreateObject("ADODB.Connection")
	
	BDRegAcao.Open Application("bd-logped")
	
		BDRegAcao.Execute "INSERT INTO acao VALUES ('" & Session("Controle") & "', '" & dataSQL(Date) & " " & Time & "', '" & Replace(Realizado, "'", "''") & "');"
	
	BDRegAcao.Close
	Set BDRegAcao = Nothing

End Sub


'__________________________________________________________________________________________________


Function Acesso(NomeAcao)
	'Verifica o n�vel de acesso para cada usu�rio

	Dim Acao
	Acao = Replace(NomeAcao, "'", "''")
	if Acao = empty then
		Acesso = false
		exit function
	end if


		'A��es:
		'1 : VerPedidos
		'2 : VerDados
		'3 : InserirDados
		'4 : AlterarDados
		'5 : VerUsuarios
		'6 : AlterarUsuarios
		'7 : InserirUsuarios
		'8 : ApagarUsuarios
		'9 : AlterarPermissoes
		'10: InserirPedidos
		
		select case acao
			case "1"
				acao = "verpedidos"
			case "2"
				acao = "verdados"
			case "3"
				acao = "inserirdados"
			case "4"
				acao = "alterardados"
			case "5"
				acao = "verusuarios"
			case "6"
				acao = "alterarusuarios"
			case "7"
				acao = "inserirusuarios"
			case "8"
				acao = "apagarusuarios"
			case "9"
				acao = "alterarpermissoes"
			case "10"
				acao = "inserirpedidos"
			case "11"
				acao = "relacionarcnpj"
		end select
			
			
		
		Set BD = CreateObject("ADODB.Connection")
		Set RS = CreateObject("ADODB.Recordset")
		
		BD.Open Application("bd-logped")
		
		RS.Open "SELECT " & acao & " FROM nivelacesso WHERE nivelacesso = " & Session("Nivel") & ";", BD
		
		IF RS.EOF then
			Acesso = false
		Else
			if RS(Acao) = 1 then '(true = 1)
				Acesso = true
			else
				Acesso = false
			end if
		end IF
		
		RS.Close
		BD.Close
		Set RS = Nothing
		Set BD = Nothing

End Function

'__________________________________________________________________________________________________


Function NomeUsuario(UsuarioID)
	'Procura o ID do usu�rio e mostra seu nome

	If UsuarioID = 0 then 'ID usado na importa��o autom�tica de pedidos
		NomeUsuario = "Sistema"
		exit function
	End If

	Set BDNomeUsuario = CreateObject("ADODB.Connection")
	Set RSNomeUsuario = CreateObject("ADODB.Recordset")
	BDNomeUsuario.Open Application("bd-logped")
	
	RSNomeUsuario.Open "SELECT nome From usuario Where id = " & UsuarioID & ";", BDNomeUsuario
	
	If Not RSNomeUsuario.EOF then
		NomeUsuario = RSNomeUsuario("nome")
	Else
		NomeUsuario = "#" & UsuarioID
	End If
	
	RSNomeUsuario.Close
	Set RSNomeUsuario = Nothing
	BDNomeUsuario.Close
	Set BDNomeUsuario = Nothing

End Function

'__________________________________________________________________________________________________


Function NumeroDoPedido(PedidoID)
	'Procura o ID do pedido e mostra seu n�mero

	If PedidoID = 0 then
		NumeroDoPedido = "0"
		exit function
	End If

	Set BDNumeroDoPedido = CreateObject("ADODB.Connection")
	Set RSNumeroDoPedido = CreateObject("ADODB.Recordset")
	BDNumeroDoPedido.Open Application("bd-logped")
	
	RSNumeroDoPedido.Open "SELECT n_pedido From andamento Where id = " & PedidoID & ";", BDNumeroDoPedido
	
	If Not RSNumeroDoPedido.EOF then
		NumeroDoPedido = RSNumeroDoPedido("n_pedido")
	Else
		'N�o est� em andamento, procurar nos carregados
		RSNumeroDoPedido.Close
		RSNumeroDoPedido.Open "SELECT n_pedido From carregado Where id = " & PedidoID & ";", BDNumeroDoPedido
		
			If Not RSNumeroDoPedido.EOF then
				NumeroDoPedido = RSNumeroDoPedido("n_pedido")
			Else
				'N�o est� em carregado, procurar em carregado_bkp
				RSNumeroDoPedido.Close
				RSNumeroDoPedido.Open "SELECT n_pedido From carregado_bkp Where id = " & PedidoID & ";", BDNumeroDoPedido
				
				If Not RSNumeroDoPedido.EOF then
					NumeroDoPedido = RSNumeroDoPedido("n_pedido")
				Else
					'N�o est� em nenhum lugar
					NumeroDoPedido = "N�o localizado"
				End If
				
			End If

	End If
	
	RSNumeroDoPedido.Close
	Set RSNumeroDoPedido = Nothing
	BDNumeroDoPedido.Close
	Set BDNumeroDoPedido = Nothing

End Function


'__________________________________________________________________________________________________

Function TabelaPedido(PedidoID)
	'Procura o ID do pedido e mostra seu n�mero

	If PedidoID = 0 then
		TabelaPedido = "zero"
		exit function
	End If

	Set BDTabelaPedido = CreateObject("ADODB.Connection")
	Set RSTabelaPedido = CreateObject("ADODB.Recordset")
	BDTabelaPedido.Open Application("bd-logped")
	
	RSTabelaPedido.Open "SELECT n_pedido From andamento Where id = " & PedidoID & ";", BDTabelaPedido
	
	If Not RSTabelaPedido.EOF then
		TabelaPedido = "andamento"
	Else
		'N�o est� em andamento, procurar nos carregados
		RSTabelaPedido.Close
		RSTabelaPedido.Open "SELECT n_pedido From carregado Where id = " & PedidoID & ";", BDTabelaPedido
		
			If Not RSTabelaPedido.EOF then
				TabelaPedido = "carregado"
			Else
				'N�o est� em carregado, procurar em carregado_bkp
				RSTabelaPedido.Close
				RSTabelaPedido.Open "SELECT n_pedido From carregado_bkp Where id = " & PedidoID & ";", BDTabelaPedido
				
				If Not RSTabelaPedido.EOF then
					TabelaPedido = "carregado_bkp"
				Else
					'N�o est� em nenhum lugar
					TabelaPedido = "zero"
				End If
				
			End If

	End If
	
	RSTabelaPedido.Close
	Set RSTabelaPedido = Nothing
	BDTabelaPedido.Close
	Set BDTabelaPedido = Nothing

End Function


'__________________________________________________________________________________________________


Function RazaoSocialUsuario(UsuarioID)
	'Procura o ID do usu�rio e mostra seu nome

	If UsuarioID = 0 then 'ID usado na importa��o autom�tica de pedidos
		RazaoSocialUsuario = Empty
		exit function
	End If

	Set BDRazaoSocialUsuario = CreateObject("ADODB.Connection")
	Set RSRazaoSocialUsuario = CreateObject("ADODB.Recordset")
	BDRazaoSocialUsuario.Open Application("bd-logped")
	
	RSRazaoSocialUsuario.Open "SELECT razao_social From usuario Where id = " & UsuarioID & ";", BDRazaoSocialUsuario
	
	If Not RSRazaoSocialUsuario.EOF then
		RazaoSocialUsuario = RSRazaoSocialUsuario("razao_social")
	Else
		RazaoSocialUsuario = Empty
	End If
	
	RSRazaoSocialUsuario.Close
	Set RSRazaoSocialUsuario = Nothing
	BDRazaoSocialUsuario.Close
	Set BDRazaoSocialUsuario = Nothing

End Function

'__________________________________________________________________________________________________


Function NomeCliente(ClienteCNPJ)
	'Procura o CNPJ do Cliente e, se existir, retorna a Raz�o Social
	
	numClienteCNPJ = SoNumeros(ClienteCNPJ)

	'If Not IsCNPJ(numClienteCNPJ) AND numClienteCNPJ <> 8 then
	'	NomeCliente = ClienteCNPJ
	'	exit function
	'End If

	Set BDNomeCliente = CreateObject("ADODB.Connection")
	Set RSNomeCliente = CreateObject("ADODB.Recordset")
	BDNomeCliente.Open Application("bd-logped")
	
	RSNomeCliente.Open "SELECT descricao From cnpj Where cnpj LIKE '" & CortaCNPJ(numClienteCNPJ) & "%';", BDNomeCliente
	'RSNomeCliente.Open "SELECT Descricao From CNPJ Where CNPJ LIKE '" & CortaCNPJ(numClienteCNPJ) & "*';", BDNomeCliente
	
	If Not RSNomeCliente.EOF then
		NomeCliente = RSNomeCliente("descricao")
	Else
		NomeCliente = CNPJ(numClienteCNPJ)
	End If
	
	RSNomeCliente.Close
	Set RSNomeCliente = Nothing
	BDNomeCliente.Close
	Set BDNomeCliente = Nothing

End Function

'__________________________________________________________________________________________________

Sub InserirTopoTabelaPedPend()
	'Insere a parte superior da tabela de Pedidos Pendentes

%>

						<tr>
						<td valign='top' colspan='2' class='dlight'>
						<table cellpadding='4' cellspacing='1' align='center' width='100%' style='border:1px solid #37833E' ID="tblTopoAcomp">
						<tr>
						<td  align='left' width='5%' class='titlemedium'>&nbsp;</td>
						<td  align='left' width='5%' class='titlemedium'>&nbsp;</td>
						<td  align='left' width='45%' class='titlemedium'><b><a href="<%=Link("acesso.asp?menu=volcar_ped")%>">Pedido</a> e <a href="<%=Link("acesso.asp?menu=volcar_produtor")%>">Unidade Produtora</a></b></td>
						<td  align='center' width='30%' class='titlemedium'><b><a href="<%=Link("acesso.asp?menu=volcar_carregado")%>">Carregado (m�)</a></b></td>
						<td  align='center' width='15%' class='titlemedium'><b><a href="<%=Link("acesso.asp?menu=volcar_data")%>">Emitido em</a></b></td>
						</tr>

<%
End Sub 'InserirTopoTabelaPedPend()

'__________________________________________________________________________________________________

Sub InserirTopoTabelaPedCarregado()
	'Insere a parte superior da tabela de Pedidos Carregados (Hist�rico)

%>

						<tr>
						<td valign='top' colspan='2' class='dlight'>
						<table cellpadding='4' cellspacing='1' align='center' width='100%' style='border:1px solid #37833E' ID="Table4">
						<tr>
						<td  align='left' width='5%' class='titlemedium'>&nbsp;</td>
						<td  align='left' width='50%' class='titlemedium'><b>Pedido e Unidade Produtora</b></td>
						<td  align='center' width='30%' class='titlemedium'><span title="Volume Carregado (m�)"><b>Carregado (m�)</b></span></td>
						<td  align='center' width='15%' class='titlemedium'><span title="Data de emiss�o"><B>Emitido em</b></span></td>
						<!--<td  align='center' width='15%' class='titlemedium'><span title="Fim da opera��o"><b>Fim da oper.</span></b></td>-->
						</tr>

<%
End Sub 'InserirTopoTabelaPedPend()

'__________________________________________________________________________________________________


Sub InserirTabelaCinza(Titulo)
	'Insere a tabela de Pedidos Pendentes ou Completos, qdo � h� desses pedidos
%>
<!-- Sem pedidos -->
			<tr>
			<td valign='top' colspan='2' class='dlight'>
			<table cellpadding='4' cellspacing='1' align='center' width='100%' style='border:1px solid #37833E' ID="Table1">
			<tr>
			<td  align='left' width='5%' class='titlemedium'>&nbsp;</td>
			<td  align='left' width='5%' class='titlemedium'>&nbsp;</td>
			<td  align='left' width='55%' class='titlemedium'><b>Pedido e Unidade Produtora</b></td>
			<td  align='center' width='20%' class='titlemedium'><b>Carregado (m�)</b></td>
			<td  align='center' width='15%' class='titlemedium'><B>Emitido em</b></td>
			</tr>
				
				<tr>
				<td class='row1' colspan='5' align='center'><b><%=Titulo%></b></td>                
				</tr>				  <tr>
							<td align='right' nowrap class='titlemedium' colspan='5'>
								&nbsp;</td>
							</tr>
							</table>
							</td>
							</tr>
			


<!-- Fim Sem pedidos -->

<%
End Sub 'InserirTabelaCinza(Titulo)


'__________________________________________________________________________________________________

Private Sub MostrarSubTitulo(Nome)
	'T�tulo da se��o do Menu

	'Response.Write "<tr><td class='titlemedium' width='100%'>" & Nome & "</td></tr>"
	Response.Write "<tr><td valign='top' colspan='2'><span class='pagetitle'>" & Nome & "</span></td></tr>" & vbNewLine

End Sub

'__________________________________________________________________________________________________

Private Sub MostrarTituloSecao(Nome)
	'Sub-t�tulo da se��o do menu
	
	Response.Write "<td colspan='2' class='category'><b>" & Nome & "</b></td>" & vbNewLine
	
End Sub

'__________________________________________________________________________________________________


Sub MostrarConteudoSecao(Esquerda, Direita)
	'Cria tabela para disposi��o de dados
	
	Response.Write "<tr><td class='row1' align='left' valign='top' nowrap>" & Esquerda & "</td>" & vbNewLine
    Response.Write "<td class='row1' align='left'  width='100%'>" & Direita & "</td></tr>" & vbNewLine
	
End Sub


'__________________________________________________________________________________________________

Function MostrarData(Data)
	'Formata a data para exibi��o do dia da semana: "sex 26/06/2003"
	
	If IsEmpty(Data) OR IsNull(Data) then exit function
	MostrarData = WeekDayName(WeekDay(Data), true) & " " & Data

End Function

'__________________________________________________________________________________________________

Sub MostrarTabelaPedidosPendentes(Argumento, Ordem)
	'Completa a tabela com os Pedidos Pendentes de todos os CNPJs cadastrados para o usu�rio
		'Argumento: ser� passado pelo link (Hist�rico ou Pendentes)
		'Ordem: ordena��o dos pedidos = N_Pedido

		'MostrarSubTitulo("Pedidos em carregamento")
	
				Set BDAnd = CreateObject("ADODB.Connection")
				Set RSCli = CreateObject("ADODB.Recordset") 'Verifica os clientes

				BDAnd.Open Application("bd-logped")
				
				RSCli.Open "Select cnpj From cliente WHERE usuarioid = " & Session("ID") & " ORDER BY cnpj;", BDAnd

				if RSCli.EOF = false then 'existem clientes
				
					'Criar string com os CNPJs do cliente
					Dim strSQL_WHERE_CNPJsDoCliente
					strSQL_WHERE_CNPJsDoCliente = Empty
					
					While Not RSCli.EOF
						strSQL_WHERE_CNPJsDoCliente = strSQL_WHERE_CNPJsDoCliente & "cnpj LIKE '" & CortaCNPJ(RSCli("cnpj")) & "%' OR "
						RSCli.MoveNext 'pr�x. cliente
					Wend 'pr�x. cliente
					
					'Tirar ' OR '
					strSQL_WHERE_CNPJsDoCliente = mid(strSQL_WHERE_CNPJsDoCliente, 1, len(strSQL_WHERE_CNPJsDoCliente) - 4)
					
				
				
					Set RSAnd = CreateObject("ADODB.Recordset")
					
					Dim bolPrimeiroReg, txtImgStatusPed, txtImgPedAtualizado
					Dim txtAltStatusPed, txtAltPedAtualizado, intControleCorLinha
					Dim qtdVolCarregado
					Dim strAtualCNPJCli
					bolPrimeiroReg = true
					
											
						RSAnd.Open "SELECT * FROM andamento WHERE " & strSQL_WHERE_CNPJsDoCliente & " ORDER BY " & Ordem & ";", BDAnd
						'response.Write "SELECT * FROM andamento WHERE " & strSQL_WHERE_CNPJsDoCliente & " ORDER BY " & Ordem & ";"
						
						If RSAnd.EOF = false then 'h� pedidos deste cliente
						
							while Not RSAnd.EOF
							
							if bolPrimeiroReg then
								InserirTopoTabelaPedPend() 'inserir t�tulo e cabe�alho da tabela
								bolPrimeiroReg = false
							end if
							
							'if strAtualCNPJCli <> CortaCNPJ(RSCli("cnpj")) then
							'	strAtualCNPJCli = CortaCNPJ(RSCli("cnpj"))
							'	'Sub-se��o para mostrar os acompanhamentos desta unidade
							'	
							'	
							'	<!-- T�tulo Cliente = server.HTMLEncode(RSCli("cnpj"))  -->
							'				<TR>
							'					<TD align='center' class='category'><IMG src='<%=Link("img/gif_ico_cliente_flecha.gif")' border='0'></TD>
							'					<TD colspan='4' class='category'><B>= NomeCliente(RSCli("cnpj"))  (= CNPJ(RSCli("cnpj")) )</B></TD>
							'				</TR>
							'	<!-- T�tulo Cliente = RSCli("cnpj")  -->
							'	
							'	
							'end if
							
							if RSAnd("volume_carregado") = RSAnd("volume") then 'pedido carregado totalmente
								txtImgStatusPed = "img/gif_ico_aco_finalizado.gif"
								txtAltStatusPed = "Acompanhamento finalizado"
							elseif RSAnd("novo") = 1 then 'pedido novo (1 = true)
								txtImgStatusPed = "img/gif_ico_aco_novo.gif"
								txtAltStatusPed = "Novo acompanhamento"
							else
								txtImgStatusPed = "img/gif_ico_aco_andamento.gif"
								txtAltStatusPed = "Acompanhamento em andamento"
							end if
							
							If RSAnd("Atualizado") = 1 then 'pedido atualizado (1 = true)
								txtImgPedAtualizado = "img/gif_ico_aco_atualizado.gif"
								txtAltPedAtualizado = "Acompanhamento atualizado"
							else
								txtImgPedAtualizado = "img/gif_trans.gif"
								txtAltPedAtualizado = Empty
							end If
							
							If intControleCorLinha = 1 then intControleCorLinha = 2 Else intControleCorLinha = 1
							
							qtdVolCarregado = RSAnd("volume_carregado") - RSAnd("volume_devolvido") 'VolumeCarregado(RSAnd("id"))
							
								%>

										<!-- In�cio do pedido <%=RSAnd("n_pedido") & "-" & RSAnd("modalidade")%> -->
										<tr>
											<td align='center' class='forum<%=intControleCorLinha%>'><img src='<%=Link(txtImgStatusPed)%>' border='0' alt='<%=txtAltStatusPed%>'></td>
											<td align='center' class='forum<%=intControleCorLinha%>'><img src='<%=Link(txtImgPedAtualizado)%>' border='0' alt='<%=txtAltPedAtualizado%>'></td>
											<td class='forum<%=intControleCorLinha%>'>
												<table width='100%' border='0' cellspacing='0' cellpadding='0' ID="Table2">
													<tr>
														<td valign='middle'></td>
														<td width='100%'><span class='linkthru'>
																<a href='<%=Link("ped_info.asp?id=" & RSAnd("id") & "&acao=" & Argumento)%>' class='linkthru'>
																	<b><%=RSAnd("n_pedido") & "-" & RSAnd("modalidade")%></b></a></span>
														</td>
													</tr>
												</table>
												<span class='desc'><%=RSAnd("nome_usina")%></span>
											</td>
											<td align='center' class='forum<%=intControleCorLinha%>'><a href='<%=Link("ped_hist.asp?id=" & RSAnd("id") & "&acao=" & Argumento)%>'><%=FormatNumber(Round(qtdVolCarregado, 3), 3)%> (<%=PorcentagemCarregada(RSAnd("id"))%>)</a></td>
											<td align='center' class='forum<%=intControleCorLinha%>'><%=RSAnd("data_emissao")%></td>
										</tr>
										<!-- Fim do pedido <%=RSAnd("n_pedido") & "-" & RSAnd("modalidade")%> -->
								<%
								
								RSAnd.MoveNext 'pr�x. pedido do cliente
							Wend 'pr�x. pedido do cliente
						End If 'fim dos pedidos do cliente
					
						RSAnd.Close

					
					if bolPrimeiroReg = false then 'houveram pedidos, deve-se fechar a tabela
					'Fim da tabela de pedidos
					%>
								<tr>
											<td align='right' nowrap class='titlemedium' colspan='5'>
												&nbsp;</td>
											</tr>
											</table>
											</td>
											</tr>
											<tr>
											<td class='row1' align='left' valign='middle'>
												<img src='<%=Link("img/gif_ico_aco_novo.gif")%>' border='0'  alt=''>&nbsp;Novo acompanhamento<br>
												<img src='<%=Link("img/gif_ico_aco_andamento.gif")%>' border='0'  alt=''>&nbsp;Acompanhamento em andamento<br>
												<img src='<%=Link("img/gif_ico_aco_atualizado.gif")%>' border='0'  alt=''>&nbsp;Acompanhamento atualizado</td>
											<td class='row1mini' align='right' valign='top' nowrap>&nbsp;<%'=Controle()%></td>
											</tr>
							        
								</table>
								</td>
								</tr>
								</table>
								</td>
							</tr>
							</table>
					<%
					else 'n�o h� clientes
					
						Response.Write "<!-- N�o h� clientes1 -->"
						InserirTabelaCinza("N�o h� pedidos em carregamento")
					
					end if
					
				Set RSAnd = Nothing
				RSCli.Close
				Set RSCli = nothing

				else 'n�o h� pedidos
					Response.Write "<!-- N�o h� clientes2 -->"
					InserirTabelaCinza("N�o h� pedidos em carregamento")
				end if
				
				BDAnd.Close

				Set BDAnd = Nothing
End Sub
'__________________________________________________________________________________________________

Function Form(Campo)
	'Retorna o conte�do de um campo enviado via POST "tratado"

	Form = Trim(Replace(Request.Form(Campo), "'", "''"))

End Function

'__________________________________________________________________________________________________

Function NumeroSQL(Numero)
    'Retorna o n�mero na forma 22222.666

    'NumeroSQL = Replace(Trim(Replace(Numero, ".", "")), ",", ".")
    
    Dim Valor, pos, strEsq, strDis
    
    Valor = Numero
    pos = InStr(Valor, ",") 'captura a posi��o da v�rgula na string
    If pos <> 0 Then 'se o numero n�o tem v�rgula, InStr=0
        strEsq = Left(Valor, (pos - 1)) 'captura a parte inteira
        strdir = Right(Valor, (Len(Valor) - pos)) 'captura a parte decimal
        Valor = strEsq & "." & strdir 'concatena com um ponto
    Else
        Valor = Numero 'no caso do n�mero n�o ter v�rgulas
    End If

    NumeroSQL = Valor
    

End Function

'__________________________________________________________________________________________________

Sub MostrarTabelaPedidosCarregados()
	'Completa a tabela com os Pedidos Carregados (Hist�rico) de todos os CNPJs cadastrados para o usu�rio

		MostrarSubTitulo("Hist�rico de pedidos carregados")
	
				Set BDAnd = CreateObject("ADODB.Connection")
				Set RSCli = CreateObject("ADODB.Recordset") 'Verifica os clientes

				BDAnd.Open Application("bd-logped")
				
				RSCli.Open "Select cnpj From cliente WHERE usuarioid = " & Session("ID") & " ORDER BY cnpj;", BDAnd

				if RSCli.EOF = false then 'existem clientes
				
				
					'Criar string com os CNPJs do cliente
					Dim strSQL_WHERE_CNPJsDoCliente
					strSQL_WHERE_CNPJsDoCliente = Empty
					
					While Not RSCli.EOF
						strSQL_WHERE_CNPJsDoCliente = strSQL_WHERE_CNPJsDoCliente & "cnpj LIKE '" & CortaCNPJ(RSCli("cnpj")) & "%' OR "
						RSCli.MoveNext 'pr�x. cliente
					Wend 'pr�x. cliente
					
					'Tirar ' OR '
					strSQL_WHERE_CNPJsDoCliente = mid(strSQL_WHERE_CNPJsDoCliente, 1, len(strSQL_WHERE_CNPJsDoCliente) - 4)
					
					
					Set RSAnd = CreateObject("ADODB.Recordset")
					
					Dim bolPrimeiroReg, intControleCorLinha
					Dim qtdVolCarregado, qtdVolDevolvido
					bolPrimeiroReg = true
					
						
						RSAnd.Open "SELECT * FROM carregado WHERE "& strSQL_WHERE_CNPJsDoCliente &" ORDER BY n_pedido;", BDAnd
						'response.Write "SELECT * FROM carregado WHERE "& strSQL_WHERE_CNPJsDoCliente &" ORDER BY n_pedido;"
						
						If RSAnd.EOF = false then 'h� pedidos deste cliente
							
							while Not RSAnd.EOF
							
							if bolPrimeiroReg then
								InserirTopoTabelaPedCarregado() 'inserir t�tulo e cabe�alho da tabela
								bolPrimeiroReg = false
							end if
							
							If intControleCorLinha = 1 then intControleCorLinha = 2 Else intControleCorLinha = 1
							
							qtdVolCarregado = RSAnd("volume_carregado") - RSAnd("volume_devolvido") 'VolumeCarregado(RSAnd("id"))
							'qtdVolDevolvido = RSAnd("volume_devolvido") + 0 'VolumeDevolvido(RSAnd("id"))
							
								%>

										<!-- In�cio do pedido carregado <%=RSAnd("id") & ") " & RSAnd("n_pedido") & "-" & RSAnd("modalidade")%> -->
										<tr>
											<td align='center' class='forum<%=intControleCorLinha%>'><img src='<%=Link("img/gif_ico_aco_finalizado.gif")%>' border='0' alt=''></td>
											<td class='forum<%=intControleCorLinha%>'>
												<table width='100%' border='0' cellspacing='0' cellpadding='0' ID="tblSubTabelaInfo<%=RSAnd("id")%>">
													<tr>
														<td valign='middle'></td>
														<td width='100%'><span class='linkthru'>
																<a href='<%=Link("ped_info.asp?acao=fim&id=" & RSAnd("id"))%>' class='linkthru'>
																	<b><%=RSAnd("n_pedido") & "-" & RSAnd("modalidade")%></b></a></span>
														</td>
													</tr>
												</table>
												<span class='desc'><%=RSAnd("nome_usina")%></span>
											</td>
											<td align='center' class='forum<%=intControleCorLinha%>'><a href='<%=Link("ped_hist.asp?acao=fim&id=" & RSAnd("id"))%>'><%=FormatNumber(Round((qtdVolCarregado), 3), 3)%></a></td>
											<td align='center' class='forum<%=intControleCorLinha%>'><%=RSAnd("data_emissao")%></td>
											<!--<td align='center' class='forum<%'=intControleCorLinha%>'><%'=MostrarData(RSAnd("encerradoem"))%></td>-->
										</tr>
										<!-- Fim do pedido <%=RSAnd("n_pedido") & "-" & RSAnd("modalidade")%> -->
								<%
								
								RSAnd.MoveNext 'pr�x. pedido do cliente
							Wend 'pr�x. pedido do cliente
						End If 'fim dos pedidos do cliente
					
						RSAnd.Close

					
					if bolPrimeiroReg = false then 'houveram pedidos, deve-se fechar a tabela
					'Fim da tabela de pedidos
					%>
								<tr>
											<td align='right' nowrap class='titlemedium' colspan='5'>
												&nbsp;</td>
											</tr>
											
											</table>
											
											<tr>
											<td class='row1' align='left' valign='middle'>
												<img src='<%=Link("img/gif_ico_aco_finalizado.gif")%>' border='0'  alt=''>&nbsp;Acompanhamento finalizado<br>
											</td>
											<td class='row1mini' align='right' valign='top' nowrap><%=Controle()%></td>
											</tr>
											
											</td>
											</tr>
							        

					<%
					else 'n�o h� clientes
					Response.Write "<!-- N�o h� clientes3 -->"
					InserirTabelaCinza("N�o h� pedidos carregados")

					end if
					
				Set RSAnd = Nothing
				RSCli.Close
				Set RSCli = nothing

				else 'n�o h� clientes
					Response.Write "<!-- N�o h� clientes4 -->"
					InserirTabelaCinza("N�o h� pedidos carregados")
				end if
				
				BDAnd.Close

				Set BDAnd = Nothing
End Sub

'__________________________________________________________________________________________________

Sub MostrarInputCNPJ(ClienteOuAdmin)

Dim TipoDoc

	Select Case lcase(ClienteOuAdmin)
		Case "cliente"
			If Session("CNPJCli") = "zero" then
				MostrarInputSemCNPJ(ClienteOuAdmin)
				Exit Sub
			Else
				TipoDoc = "CNPJCli"
			End If
		Case "cli"
			If Session("CNPJCli") = "zero" then
				MostrarInputSemCNPJ(ClienteOuAdmin)
				Exit Sub
			Else
				TipoDoc = "CNPJCli"
			End If
		Case "admin"
			If Session("AdminCNPJ") = "zero" then
				MostrarInputSemCNPJ(ClienteOuAdmin)
				Exit Sub
			Else
				TipoDoc = "AdminCNPJ"
			End If
		Case "adm"
			If Session("AdminCNPJ") = "zero" then
				MostrarInputSemCNPJ(ClienteOuAdmin)
				Exit Sub
			Else
				TipoDoc = "AdminCNPJ"
			End If
		Case Else
			MostrarInputSemCNPJ(ClienteOuAdmin)
			Exit Sub
	End Select

'If InStr(1, Session(TipoDoc), " ") > 1 then 'Tem mais de um CNPJ, logo cria-se uma combobox

	'Iniciar Combo
	Response.Write "<select name='" & TipoDoc & "' class='forminput'>"
	
	'N�mero de CNPJS
	'Dim ContDoc
	'N�mero de CNPJS na Sess�o, come�a no 0 (zero)
	'ContDoc = UBound(split(Session(TipoDoc)))
	
	'Mostrar op��es
	For Each NmrCNPJ In split(Session(TipoDoc))
		Response.Write "<option value='" & NmrCNPJ & "'>" & CNPJ(NmrCNPJ) & "</option>"
    next
	
	'Fechar Combo
	Response.Write "</select>"

End Sub

'__________________________________________________________________________________________________

Sub MostrarInputSemCNPJ(ClienteOuAdmin)

Dim TipoDoc

	Select Case lcase(ClienteOuAdmin)
		Case "cliente"
			TipoDoc = "CNPJCli"
		Case "cli"
			TipoDoc = "CNPJCli"
		Case "admin"
			TipoDoc = "AdminCNPJ"
		Case "adm"
			TipoDoc = "AdminCNPJ"
	End Select

	Response.Write "<select name='" & TipoDoc & "' class='forminput'><option value='zero'>N�o h� CNPJ cadastrado</option></select>"

End Sub

'__________________________________________________________________________________________________

'Converter data para SQL
Function DataSQL(ByVal DataBR)
	Dim mview,dview
	
	if Len(Day(DataBR)) = 1 Then
		dview = "0" & Day(DataBR)
	Else
		dview = Day(DataBR)
	End if
	
	if Len(Month(DataBR)) = 1 Then
		mview = "0" & Month(DataBR)
	Else
		mview = Month(DataBR)
	End if
	DateConvert = Year(DataBR) & "/" & mview & "/" & dview
	DataSQL = DateConvert
End Function

'__________________________________________________________________________________________________

Function Controle()
	'Retorna o c�digo da sess�o sem as {chaves}
	
	Controle = Replace(Replace(Session("Controle"), "{", Empty), "}", Empty)
	
End Function

'__________________________________________________________________________________________________

Sub InserirTopoTabelaListarUsuarios()
	'Insere a parte superior da tabela para Listar os usu�rios

%>

				<tr>
				<td valign='top' colspan='2' class='dlight'>
				<table cellpadding='4' cellspacing='1' align='center' width='100%' style='border:1px solid #37833E' ID="Table5">
				<tr>
				<td  align='left' width='3%' class='titlemedium'>&nbsp;</td>
				<td  align='center' width='20%' class='titlemedium'><b><a href="<%=Link("acesso.asp?menu=ad_usu_listar&class=usua")%>">Usu�rio</b></td>
				<td  align='center' width='37%' class='titlemedium'><a href="<%=Link("acesso.asp?menu=ad_usu_listar&class=nome")%>">Nome do usu�rio</a></td>
				<td  align='center' width='40%' class='titlemedium'><b><a href="<%=Link("acesso.asp?menu=ad_usu_listar&class=distr")%>">Distribuidor</a></b></td>
				</tr>
				

<%
End Sub 'InserirTopoTabelaListarUsuarios()

'__________________________________________________________________________________________________

Sub MostrarTabelaListarUsuarios(Ordem)
	'Completa a tabela com os Usu�rios

		'MostrarSubTitulo("Usu�rios cadastrados")
	
				Set BD = CreateObject("ADODB.Connection")
				Set RS = CreateObject("ADODB.Recordset")
				
				BD.Open Application("bd-logped")
				
				Dim strOrdemLista
				Select Case lcase(Ordem)
					Case "nome"
						strOrdemLista = "nome"
					Case "distr"
						strOrdemLista = "razao_social"
					Case Else
						strOrdemLista = "usuario"
				End Select
				
				RS.Open "Select id, usuario, nome, habilitado, razao_social From usuario ORDER BY " & strOrdemLista & ";", BD

				if RS.EOF = false then 'existem usu�rios
					
					InserirTopoTabelaListarUsuarios()
					Dim imgUsuarioHabilitado, intControleCorLinha
					
					intControleCorLinha = 1
					
					While Not RS.EOF 'Verificar os usu�rios e mostra suas informa��es
						
						If RS("habilitado") = 1 then '1 = true
							imgUsuarioHabilitado = "img/gif_ico_usu_habilitado.gif"
						Else
							imgUsuarioHabilitado = "img/gif_ico_usu_bloqueado.gif"
						End If
						
						If intControleCorLinha = 1 then intControleCorLinha = 2 Else intControleCorLinha = 1
						
						%>
						<!-- In�cio do usu�rio <%=RS("id")%> -->
						<tr>
							<td align='center' class='row<%=intControleCorLinha%>'><img src='<%=Link(imgUsuarioHabilitado)%>' border='0' alt=''></td>
							<td align='left' class='row<%=intControleCorLinha%>'><a href='<%=Link("usuario_info.asp?id=" & RS("id"))%>'><%=RS("usuario")%></a></td>
							<td align='left' class='row<%=intControleCorLinha%>'><%=RS("nome")%></td>
							<td align='left' class='row<%=intControleCorLinha%>'><%=RS("razao_social")%></td>
						</tr>
						<!-- Fim do usu�rio <%=RS("id")%> -->
						<%
							
						RS.MoveNext 'pr�x. pedido do cliente
					Wend 'pr�x. pedido do cliente
			
				'Fim da tabela de pedidos
				%>
							<tr>
										<td align='right' nowrap class='titlemedium' colspan='5'>
											&nbsp;</td>
										</tr>
										
										</table>
										
											<tr>
											<td class='row1' align='left' valign='middle'>
												<img src='<%=Link("img/gif_ico_usu_habilitado.gif")%>' border='0'  alt=''>&nbsp;Usu�rio habilitado<br>
												<img src='<%=Link("img/gif_ico_usu_bloqueado.gif")%>' border='0'  alt=''>&nbsp;Usu�rio bloqueado
											</td>
											<td class='row1mini' align='right' valign='top' nowrap>&nbsp;</td>
											</tr>
										
										</td>
										</tr>
							    

				<%
				else 'n�o h� clientes
				Response.Write "<!-- N�o h� usu�rios -->"
				InserirTabelaCinza("N�o h� usu�rios")

				end if
				
			RS.Close
			Set RS = Nothing
			BD.Close
			Set BD = Nothing
End Sub

'__________________________________________________________________________________________________

Sub InserirTopoTabelaEditarUsuarios()
	'Insere a parte superior da tabela para Editar os usu�rios

%>

				<tr>
				<td valign='top' colspan='2' class='dlight'>
				<table cellpadding='4' cellspacing='1' align='center' width='100%' style='border:1px solid #37833E' ID="Table3">
				<tr>
				<td  align='left' width='3%' class='titlemedium'>&nbsp;</td>
				<td  align='center' width='20%' class='titlemedium'><b><a href="<%=Link("acesso.asp?menu=ad_usu_editar&ordlista=usua")%>">Usu�rio</b></td>
				<td  align='center' width='37%' class='titlemedium'><a href="<%=Link("acesso.asp?menu=ad_usu_editar&ordlista=nome")%>">Nome do usu�rio</a></td>
				<td  align='center' width='40%' class='titlemedium'><b><a href="<%=Link("acesso.asp?menu=ad_usu_editar&ordlista=razsoc")%>">Distribuidor</a></b></td>
				</tr>

<%
End Sub 'InserirTopoTabelaListarUsuarios()

'__________________________________________________________________________________________________

Sub MostrarTabelaEditarUsuarios(Ordem)
	'Completa a tabela com os Usu�rios e usa url para edi��o

		MostrarSubTitulo("Editar usu�rio")
	
				Set BD = CreateObject("ADODB.Connection")
				Set RS = CreateObject("ADODB.Recordset")
				
				BD.Open Application("bd-logped")
				
				Dim strOrdemLista
				Select Case lcase(Ordem)
					Case "nome"
						strOrdemLista = "nome"
					Case "razsoc"
						strOrdemLista = "razao_social"
					Case Else
						strOrdemLista = "usuario"
				End Select
				
				RS.Open "Select id, usuario, nome, habilitado, razao_social From usuario WHERE id <> " & Session("ID") & " ORDER BY " & strOrdemLista & ";", BD
				'Pegar todos os us�rios com exce��o do atual

				if RS.EOF = false then 'existem usu�rios
					
					InserirTopoTabelaEditarUsuarios()
					Dim imgUsuarioHabilitado, intControleCorLinha
					
					intControleCorLinha = 1
					
					While Not RS.EOF 'Verificar os usu�rios e mostra suas informa��es
						
						If RS("habilitado") = 1 then '1 = true
							imgUsuarioHabilitado = "img/gif_ico_usu_habilitado.gif"
						Else
							imgUsuarioHabilitado = "img/gif_ico_usu_bloqueado.gif"
						End If
						
						If intControleCorLinha = 1 then intControleCorLinha = 2 Else intControleCorLinha = 1
						
						%>
						<!-- In�cio do usu�rio <%=RS("id")%> -->
						<tr>
							<td align='center' class='forum<%=intControleCorLinha%>'><img src='<%=Link(imgUsuarioHabilitado)%>' border='0' alt=''></td>
							<td align='left' class='forum<%=intControleCorLinha%>'><a href='<%=Link("acesso.asp?menu=ad_usu_edinfo&UsuarioID=" & RS("id"))%>'><%=RS("usuario")%></a></td>
							<td align='left' class='forum<%=intControleCorLinha%>'><%=RS("nome")%></td>
							<td align='left' class='forum<%=intControleCorLinha%>'><%=RS("razao_social")%></td>
						</tr>
						<!-- Fim do usu�rio <%=RS("id")%> -->
						<%
							
						RS.MoveNext 'pr�x. pedido do cliente
					Wend 'pr�x. pedido do cliente
			
				'Fim da tabela de pedidos
				%>
							<tr>
										<td align='right' nowrap class='titlemedium' colspan='5'>
											&nbsp;</td>
										</tr>
										
										</table>
										
											<tr>
											<td class='row1' align='left' valign='middle'>
												<img src='<%=Link("img/gif_ico_usu_habilitado.gif")%>' border='0'  alt=''>&nbsp;Usu�rio habilitado<br>
												<img src='<%=Link("img/gif_ico_usu_bloqueado.gif")%>' border='0'  alt=''>&nbsp;Usu�rio bloqueado
											</td>
											<td class='row1mini' align='right' valign='top' nowrap><%=Controle()%></td>
											</tr>
										
										</td>
										</tr>
							    

				<%
				else 'n�o h� clientes
				Response.Write "<!-- N�o h� usu�rios -->"
				InserirTabelaCinza("N�o h� usu�rios")

				end if
				
			RS.Close
			Set RS = Nothing
			BD.Close
			Set BD = Nothing
End Sub

'__________________________________________________________________________________________________

    ' for :Total Email Validation
    '**************************************
    ' Name: Total Email Validation
    ' Description:Validates email addresses.
    '     Makes sure the email addresses with IP a
    '     ddresses are not private network address
    '     es. Allows multiple sub-domain levels. v
    '     erifies characters within domain names. 
    '     only allows standard length 26 character
    '     s for each domain name level, except the
    '     top (3 max)
    ' By: Lewis Moten
    '
    '
    ' Inputs:asString - Email address to be 
    '     validated.
    '
    ' Returns:Boolean (true/false) value ind
    '     icating if the string presented was a va
    '     lid email address.
    '
    '**************************************
    
    Function IsEmail(ByRef asString)
    	Dim lsDomain
    	Dim lsSubDomain
    	Dim lsSubDomainArray
    	Dim lbIsIPdomain
    	Dim lnStart
    	Dim lsUserName
    	Dim lnOctect
    	Dim lnOctect2
    	Dim lnIndex
    	Const lsDOMAIN_CHARACTERS = ".ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-"
    	' Must have at least 6 characters "a@a.ru"
    	If Len(asString) < 6 Then
    		IsEmail = False
    		Exit Function
    	End If
    	
    	' Look for "@" delimiter
    	If Not InStr(asString, "@") > 1 Then
    		IsEmail = False
    		Exit Function
    	End If
    	
    	' Make sure characters exist after the "@"
    	If Len(asString) = InStr(asString, "@") Then
    		IsEmail = False
    		Exit Function
    	End If
    	
    	' Grab domain information "a.ru"
    	lsDomain = UCase(Mid(asString, InStr(asString, "@") + 1))
    	
    	' Grab username information
    	lsUserName = UCase(Left(asString, InStr(asString, "@") - 1))
    	
    	' Make sure at least 1 "." exists
    	If InStr(lsDomain, ".") = 0 Then
    		IsEmail = False
    		Exit Function
    	End If
    	
    	' Check for valid domain characters
    	lnStart = 1
    	Do While lnStart <= Len(lsDomain)
    		If InStr(lsDOMAIN_CHARACTERS, Mid(lsDomain, lnStart, 1)) Then
    			lnStart = lnStart + 1
    		Else
    			IsEmail = False
    			Exit Function
    		End If
    	Loop
    	
    	' Split domains
    	lsSubDomainArray = Split(lsDomain, ".")
    	lbIsIPdomain = False
    		
    	' Loop through each domain
    	For lnIndex = 0 To UBound(lsSubDomainArray, 1)
    		lsSubDomain = lsSubDomainArray(lnIndex)
    		If Len(lsSubDomain) = 0 Then
    			IsEmail = False
    			Exit Function
    		End If
    		
    		' Check to see if the domain is an IP Address
    		If lnIndex = 0 Then
    			If IsNumeric(lsSubDomain) Then
    				
    				' Only IP Addresses can have only numbers in subdomain area
    				lbIsIPDomain = True
    				
    				' Make sure 4 subdomains are present
    				If Not UBound(lsSubDomainArray, 1) = 3 Then
    					IsEmail = False
    					Exit Function
    				End If
    			
    			End If
    		End If
    		
    		If lbIsIPDomain Then
    			If Len(lsSubDomain) > 3 Then
    				IsEmail = False
    				Exit Function
    			ElseIf Not InStr(lsSubDomain, "-") = 0 Then
    				IsEmail = False
    				Exit Function
    			ElseIf Not IsNumeric(lsSubDomain)Then
    				IsEmail = False
    				Exit Function
    			End If
    			
    			lnOctect = CInt(lsSubDomain)
    			
    			If lnOctect > 255 Then
    				IsEmail = False
    				Exit Function
    			ElseIf lnOctect < 0 Then
    				IsEmail = False
    				Exit Function
    			End If
    			
    			' Look for private network settings
    			If lnIndex = 0 Then
    			
    				' Grab 2nd IP value
    				lnOctect2 = lsSubDomainArray(1)
    				
    				If Len(lnOctect2) > 3 Then
    					IsEmail = False
    					Exit Function
    				ElseIf Not IsNumeric(lnOctect2)Then
    					IsEmail = False
    					Exit Function
    				End If
    				
    				lnOctect2 = CInt(lnOctect2)
    				'	TCP/IP addresses reserved for 'private' networks are:
    				'				
    				'	10.0.0.0to 10.255.255.255				
    				'	172.16.0.0 to 172.31.255.255				
    				'	192.168.0.0to 192.168.255.255				
    				
    				Select Case lnOctect
    					
    					Case 10 ' Private Network
    						IsEmail = False
    						Exit Function
    					
    					Case 172
    						If lnOctect2 => 16 And lnOctect2 =< 31 Then
    							IsEmail = False
    							Exit Function
    						End If
    					
    					Case 192 ' Local Network
    						If lnOctect2 = 168 Then
    							IsEmail = False
    							Exit Function
    						End If
    					
    					Case 127 ' Local Machine
    						IsEmail = False
    						Exit Function				
    				
    				End Select
    			
    			End If
    			' End 'private' network check					
    		Else
    			
    			If lnIndex = UBound(lsSubDomainArray, 1) Then
    				
    				' Last domain can have 3 characters max
    				If Len(lsSubDomain) > 3 Then
    					IsEmail = False
    					Exit Function
    				ElseIf Not InStr(lsSubDomain, "-") = 0 Then
    					IsEmail = False
    					Exit Function
    				End If
    			
    			Else
    				' Domain, sub domain can only have 22 characters max
    				If Len(lsSubDomain) > 22 Then
    					IsEmail = False
    					Exit Function
    				End If
    			End If
    		End If
    	Next
    	
    	' Check for valid characters in username
    	lnStart = 1
    	Do While lnStart <= Len(lsUserName)
    		If InStr(lsDOMAIN_CHARACTERS, Mid(lsUserName, lnStart, 1)) Then
    			lnStart = lnStart + 1
    		Else
    			IsEmail = False
    			Exit Function
    		End If
    	Loop
    	
    	IsEmail = True
    End Function

'__________________________________________________________________________________________________

Function FormataSenha(txtSenha)

	FormataSenha = lcase(trim(txtSenha))

End Function

'__________________________________________________________________________________________________

Function isSenha(Senha)

    txtSenha = Trim(LCase(Senha))

    If Trim(txtSenha) = Empty Then
            isSenha = False
            Exit Function
        Else
            isSenhaTemp = True
    End If
    
    If Len(txtSenha) <> 5 Then
        isSenha = False
        Exit Function
    Else
        isSenhaTemp = True
    End If
    
    Numeros = "0123456789"
    Letras = "abcdefghijklmnopqrstuvwxyz"

    For i = 1 To Len(txtSenha)
        bolNumeros = bolNumeros + InStr(Numeros, Mid(txtSenha, i, 1))
        bolLetras = bolLetras + InStr(Letras, Mid(txtSenha, i, 1))
        If InStr(Numeros, Mid(txtSenha, i, 1)) = 0 And InStr(Letras, Mid(txtSenha, i, 1)) = 0 Then
            'bolOutros = True
            isSenha = False
            Exit Function
        End If
    Next
    
    If bolNumeros > 0 And bolLetras > 0 Then
        isSenhaTemp = True
    Else
        isSenha = False
        Exit Function
    End If
    
    isSenha = isSenhaTemp
End Function


'__________________________________________________________________________________________________

Function isCNPJ(pCNPJ)
  
    Dim Conta, Soma, Passo
    Dim Digito1, Digito2, Flag
    
    pCNPJ = SoNumeros(Trim(pCNPJ))
    
    'If Len(pCNPJ) = 8 Then
	'	'adapta��o para CNPJs at� a barra
	'	isCNPJ = True
    '    Exit Function
    'End If
    
    If Len(pCNPJ) <> 14 Then
		isCNPJ = False
        Exit Function
    End If
    
    For Passo = 5 To 6
        Soma = 0
        Flag = Passo
        
        For Conta = 1 To Passo + 7
            Soma = Soma + (Mid(pCNPJ, Conta, 1) * Flag)
            If Flag > 2 Then
				Flag = Flag - 1
			Else
				Flag = 9
			End If
        Next
        
        Soma = Soma Mod 11
        
        If Passo = 5 Then 
			IF Soma > 1 then
				Digito1 = 11 - Soma
			Else
				Digito1 = 0
			End IF
        End If
        
        If Passo = 6 Then
			IF Soma > 1 then
				Digito2 = 11 - Soma
			Else
				Digito2 = 0
			End IF
		End If
    Next
    
    If Cstr(Digito1) = Left(Right(pCNPJ, 2), 1) And Cstr(Digito2) = Right(pCNPJ, 1) Then
        isCNPJ = True
	Else
		isCNPJ = False
    End If
End Function

'__________________________________________________________________________________________________

Function CompactaDB(CaminhoDB,Access97)
on error resume next
	Dim fso, Engine, strCaminhoDB
	strCaminhoDB = left(CaminhoDB,instrrev(CaminhoDB,"\"))
	Set fso = CreateObject("Scripting.FileSystemObject")

	If fso.FileExists(CaminhoDB) Then
		Set Engine = CreateObject("JRO.JetEngine")
		If Access97 = "True" Then
			Engine.CompactDatabase "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & CaminhoDB, _
			"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strCaminhoDB & "LogPedTemp_" & Session.SessionID & ".mdb;" _
			& "Jet OLEDB:Engine Type=4"
		Else
			Engine.CompactDatabase "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & CaminhoDB, _
			"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strCaminhoDB & "LogPedTemp_" & Session.SessionID & ".mdb"
		End If
		fso.CopyFile strCaminhoDB & "LogPedTemp_" & Session.SessionID & ".mdb", CaminhoDB
		fso.DeleteFile strCaminhoDB & "LogPedTemp_" & Session.SessionID & ".mdb"
		Set fso = nothing
		Set Engine = nothing
		CompactaDB = CaminhoDB & ": compactado."
	Else
	      
		CompactaDB = CaminhoDB & ": n�o foi localizado."
	End If

End Function

'__________________________________________________________________________________________________

Function VolumeCarregado(PedidoID)
	on error resume next

	Set BDVolumeCarregado = CreateObject("ADODB.Connection")
	BDVolumeCarregado.Open Application("bd-logped")

	Set RS_SomarVolCarregado = CreateObject("ADODB.Recordset") 'Verifica o qto foi carregado
	Dim numSomarVolumeCarregado

	'Verifica��o do volume carregado
	RS_SomarVolCarregado.Open "SELECT ROUND(SUM(volume), 3) FROM carregar WHERE pedidoid = " & PedidoID & ";", BDVolumeCarregado
	numSomarVolumeCarregado = RS_SomarVolCarregado.GetString()
		'response.Write "volcarr:" & numSomarVolumeCarregado
	VolumeCarregado = replace(numSomarVolumeCarregado, ".", ",")

	RS_SomarVolCarregado.Close
	Set RS_SomarVolCarregado = nothing
	BDVolumeCarregado.Close
	Set BDVolumeCarregado = nothing

End Function

'__________________________________________________________________________________________________

Function VolumeDevolvido(PedidoID)
	on error resume next

	Set BDVolumeDevolvido = CreateObject("ADODB.Connection")
	BDVolumeDevolvido.Open Application("bd-logped")

	Set RS_SomarVolDevolvido = CreateObject("ADODB.Recordset") 'Verifica o qto foi Devolvido
	Dim numSomarVolumeDevolvido
	
	'Verifica��o do volume Devolvido
	RS_SomarVolDevolvido.Open "SELECT ROUND(SUM(volume_devolvido), 3) FROM carregar WHERE pedidoid = " & PedidoID & ";", BDVolumeDevolvido
	numSomarVolumeDevolvido = RS_SomarVolDevolvido.GetString()
	
	VolumeDevolvido = replace(numSomarVolumeDevolvido, ".", ",")

	RS_SomarVolDevolvido.Close
	Set RS_SomarVolDevolvido = nothing
	BDVolumeDevolvido.Close
	Set BDVolumeDevolvido = nothing

End Function

'__________________________________________________________________________________________________

Function ComboRazoesSociaisDosUsuarios()

	Set BDCombo = CreateObject("ADODB.Connection")
	BDCombo.Open Application("bd-logped")

	Set RSCombo = CreateObject("ADODB.Recordset")
	RSCombo.Open "SELECT DISTINCT razao_social FROM usuario ORDER BY razao_social ASC", BDCombo

	response.write "<select size=""1"" name=""descrcnpj"" ID=""descrcnpj"" class=""forminput"">" & vbcrlf
	
	If RSCombo.EOF then 'nenhuma raz�o social
		response.write "<option value="""">- Nenhuma Unidade Produtora cadastrada -</option>" & vbcrlf
	Else
	
		'Insist�ncia da Carol: uma linha em branco
		response.write "<option value=""""> </option>" & vbcrlf
		
		While not RSCombo.EOF	
			response.write "<option value=""" & RSCombo("razao_social") & """>" & RSCombo("razao_social") & "</option>" & vbcrlf
			RSCombo.MoveNext
		Wend
	
	End If	
	
	Response.Write "</select>" & vbcrlf
	
	RSCombo.Close
	Set RSCombo = nothing
	BDCombo.Close
	set BDCombo = nothing

End Function

'__________________________________________________________________________________________________

Function PorcentagemCarregada(PedidoID)

'	dim TempPC
'	TempPC = calcularCarregamento(PedidoID) / calcularVolumeTotal(PedidoID)
'	
'			If TempPC <= 0 then
'				PorcentagemCarregada = "0%"
'			elseif TempPC >= 1 then
'				PorcentagemCarregada = "100%"
'			else
'				PorcentagemCarregada = Round(TempPC * 100, 2) & "%"
'			End If
'
'			If TempPC <= 0 then
'				PorcentagemCarregada = "0%"
'			elseif TempPC >= 1 then
'				PorcentagemCarregada = "100%"
'			else
'				PorcentagemCarregada = Round(TempPC * 100, 2) & "%"
'			End If

	Set BDPC = CreateObject("ADODB.Connection")
	BDPC.Open Application("bd-logped")

	Set RSPC = CreateObject("ADODB.Recordset")
	RSPC.Open "SELECT volume_carregado, volume_devolvido, volume FROM andamento where id = " & PedidoID & ";", BDPC
	
	iF RSPC.EOF then
		RSPC.Close
		RSPC.Open "SELECT volume_carregado, volume_devolvido, volume FROM carregado where id = " & PedidoID & ";", BDPC
			if RSPC.BOF then
				PorcentagemCarregada = "--%"
			else
				TempPC = (RSPC("volume_carregado") - RSPC("volume_devolvido"))/RSPC("volume")
					If TempPC <= 0 then
						PorcentagemCarregada = "0%"
					elseif TempPC >= 1 then
						PorcentagemCarregada = "100%"
					else
						PorcentagemCarregada = Round(TempPC * 100, 2) & "%"
					End If
				
			end if
	elsE
		TempPC = (RSPC("volume_carregado") - RSPC("volume_devolvido"))/RSPC("volume")
			If TempPC <= 0 then
				PorcentagemCarregada = "0%"
			elseif TempPC >= 1 then
				PorcentagemCarregada = "100%"
			else
				PorcentagemCarregada = Round(TempPC * 100, 2) & "%"
			End If
	end iF
	
	RSPC.Close
	BDPC.Close
	Set BDPC = nothing
	set RSPC = nothing

End Function

'__________________________________________________________________________________________________

Function calcularCarregamento(PedidoID)

	'Carregamento = Notas de Volumes Carregados (Acumuladas) � Notas de Volumes Devolvidos (Acumuladas)
	 calcularCarregamento = VolumeCarregado(PedidoID) - VolumeDevolvido(PedidoID)

End Function

'__________________________________________________________________________________________________


Function calcularSaldo(PedidoID)

		'Saldo do Pedido = Volume Original � Carregamento
		'calcularCarregamento / calcularVolumeTotal
		'carregamento = volume_original 
		'	Saldo = 0
		'carregamento < volume_original 
		'	Saldo = Saldo
		'carregamento > volume_original 
		'	Saldo = 0
						
		dim decVolumeTotal
		dim decCarregamento
		dim decSaldoTemp
		
		decVolumeTotal  = calcularVolumeTotal(PedidoID)
		decCarregamento = calcularCarregamento(PedidoID)
		
		if decCarregamento < decVolumeTotal then
			decSaldoTemp = decVolumeTotal - decCarregamento
		else
			decSaldoTemp = 0
		end if
		
		if decSaldoTemp < 0 then
			calcularSaldo = 0
		else
			calcularSaldo = decSaldoTemp
		end if

			
End Function


'__________________________________________________________________________________________________

Function calcularVolumeTotal(PedidoID)

		Set BDcalcularVolTot = CreateObject("ADODB.Connection")
		Set RScalcularVolTot = CreateObject("ADODB.Recordset")
		
		BDcalcularVolTot.Open Application("bd-logped")
		RScalcularVolTot.Open "SELECT ROUND(volume, 3) FROM " & TabelaPedido(PedidoID) & " WHERE id = " & PedidoID & ";", BDcalcularVolTot
		
		
		if NOT RScalcularVolTot.EOF then
			calcularVolumeTotal = RScalcularVolTot.GetString()
		else
			RScalcularVolTot = 0
		end if
			
		RScalcularVolTot.Close
		set RScalcularVolTot = nothing
		BDcalcularVolTot.Close
		set BDcalcularVolTot = nothing

End Function

'__________________________________________________________________________________________________

    function CriarChaveBloqueio()
    	dim counter
    	dim subconter
    	Dim TempLongID
    	For Counter = 1 to 1
    	 for subcoonter = 1 to 3
    		Randomize Timer / RND
    		TempLongID = TempLongID & Chr((ASC("z") - ASC("a")) * RND + ASC("a"))
    	  next
    	  
    	  TempLongID = TempLongID & RND
    	  
    	Next

		TempLongID = Replace(TempLongID, "<", "")
		TempLongID = Replace(TempLongID, ">", "")
		TempLongID = Replace(TempLongID, """", "")
		TempLongID = Replace(TempLongID, """", "")
		TempLongID = Replace(TempLongID, """", "")
		TempLongID = Replace(TempLongID, "'", "")
		TempLongID = Replace(TempLongID, "'", "")
		TempLongID = Replace(TempLongID, "'", "")
		TempLongID = Replace(TempLongID, ",", "")
		TempLongID = UCASE(TempLongID)
		TempLongID = server.URLEncode(TempLongID)
		

    	CriarChaveBloqueio = TempLongID
    end function


'__________________________________________________________________________________________________
'__________________________________________________________________________________________________
'__________________________________________________________________________________________________
'__________________________________________________________________________________________________
'__________________________________________________________________________________________________
%>