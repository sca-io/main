<%
' clsDebug 2.0
' Debug Information Class
' Original: Microsoft - http://support.microsoft.com/default.aspx?scid=KB;EN-US;q288965&
' Formatting: Mike - http://www.planet-source-code.com/vb/scripts/ShowCode.asp?txtCodeId=7475&lngWId=4
' Added many features incl Mozilla, database, editing and repost: Hunter Beanland - http://www.geocities.com/hbeanland/

Class clsDebug
  Private dbg_ProcTime_Start
  Private dbg_ProcTime_End
  Private dbg_RequestTime
  Private dbg_FinishTime
  Private dbg_Data
  Private dbg_DB_Data
  Private DivSetStatus
  Private DivSets()
  Public Show
  Public AllVars
  Public Editing
  Public ClassFile
  
  Private Sub Class_Initialize()
    dbg_RequestTime = Now()
	dbg_ProcTime_Start = Timer()
    AllVars = false
    Editing = true
    ClassFile = "clsDebug.asp"
	Set dbg_Data = Server.CreateObject("Scripting.Dictionary") 
	redim DivSets(2)
	DivSets(0) = "<TR><TD><DIV id=sect#sectname# class='dbgDivSetsHeader' onclick=""javascript:togglePanel('data#sectname#');"">|#title#<DIV id=data#sectname# class='dbgDivSetsBody' style='display:none;' onclick=""window.event.cancelBubble = true;"">#data#|</DIV></DIV>|"
	DivSets(1) = "<TR><TD><DIV id=sect#sectname# class='dbgDivSetsHeader' onclick=""javascript:togglePanel('data#sectname#');"">|#title#<DIV id=data#sectname# class='dbgDivSetsBody' style='display:block;' onclick=""window.event.cancelBubble = true;"">#data#|</DIV></DIV>|"
	DivSets(2) = "<TR><TD><DIV id=sect#sectname# class='dbgDivSetsHeader' style='font-weight:normal;'>|#title#<DIV id=data#sectname# class='dbgDivSetsBody'>|#data#</DIV></DIV>|"
	DivSetStatus = "0,0,1,1,0,0,0,0,0,0,0"
  End Sub

  Public Sub Print(label, output)
      if err.number > 0 then 
        call dbg_Data.Add(ValidLabel(label), "!!! Error: " & err.number & " " &  err.Description)
        err.Clear 
      else 
        call dbg_Data.Add(ValidLabel(label), output)
      end if
  End Sub
  
  Private Function ValidLabel(byval label)
    dim i, lbl
    i=0
    lbl = label
    do
      if not dbg_Data.Exists(lbl) then exit do
      i = i+1
      lbl=label & "(" & i & ")"
    loop until i=100
    ValidLabel = lbl
  End Function
 
  Private Sub PrintCookiesInfo(byval DivSetNo)
    dim tbl, cookie, key, tmp
    tbl = "<form name=dbgCookies action='" & ClassFile & "' method=post><input type=hidden name=DebugInfoCookies value=true>"
    For Each cookie in Request.Cookies
      If Not Request.Cookies(cookie).HasKeys Then
		if Editing then
			tbl = AddRowEdit(tbl, cookie, Request.Cookies(cookie))
		else
			tbl = AddRow(tbl, cookie, server.HTMLEncode(Request.Cookies(cookie)))
		end if	     
      Else
        For Each key in Request.Cookies(cookie)
 			if Editing then
				tbl = AddRowEdit(tbl, cookie & "(" & key & ")", Request.Cookies(cookie)(key))
			else
				tbl = AddRow(tbl, cookie & "(" & key & ")", server.HTMLEncode(Request.Cookies(cookie)(key)))    
			end if	     
        Next 
      End If
    Next 
	if Editing and Request.Cookies.count > 0 then tbl = tbl & "<TR><TD colspan='2'><a href='javascript:document.dbgCookies.submit();'>Save Updates</a></TR>"
	tbl = tbl & "</form>"
    tbl = MakeTable(tbl)
    if Request.Cookies.count <= 0 then DivSetNo = 2
    tmp = replace(replace(replace(DivSets(DivSetNo),"#sectname#","COOKIES"),"#title#","COOKIES"),"#data#",tbl)
	Response.Write replace(tmp,"|", vblf)
  end sub
  
  Private Sub PrintSummaryInfo(byval DivSetNo)
    dim tmp, tbl
    tbl = AddRow(tbl, "Time of Request",dbg_RequestTime)
    tbl = AddRow(tbl, "Elapsed Time",formatnumber(dbg_ProcTime_End - dbg_ProcTime_Start, 2) & " seconds")
    tbl = AddRow(tbl, "Request Type",Request.ServerVariables("REQUEST_METHOD"))
    tbl = AddRow(tbl, "Status Code",Response.Status)
  	tbl = AddRow(tbl, "Script Engine",ScriptEngine & " " & ScriptEngineMajorVersion & "." & ScriptEngineMinorVersion & "." & ScriptEngineBuildVersion)
    tbl = MakeTable(tbl)
    tmp = replace(replace(replace(DivSets(DivSetNo),"#sectname#","SUMMARY"),"#title#","SUMMARY INFO"),"#data#",tbl)
	Response.Write replace(tmp,"|", vblf)
  End Sub

  Public Sub GrabDatabaseInfo(byval oSQLDB)
    dbg_DB_Data = AddRow(dbg_DB_Data, "ADO Ver",oSQLDB.Version)
    dbg_DB_Data = AddRow(dbg_DB_Data, "OLEDB Ver",oSQLDB.Properties("OLE DB Version"))
    dbg_DB_Data = AddRow(dbg_DB_Data, "DBMS",oSQLDB.Properties("DBMS Name") & " Ver: " & oSQLDB.Properties("DBMS Version"))
    dbg_DB_Data = AddRow(dbg_DB_Data, "Provider",oSQLDB.Properties("Provider Name") & " Ver: " & oSQLDB.Properties("Provider Version"))
  End Sub

  Private Sub PrintDatabaseInfo(byval DivSetNo)
    dim tbl
    tbl = MakeTable(dbg_DB_Data)
    tbl = replace(replace(replace(DivSets(DivSetNo),"#sectname#","DATABASE"),"#title#","DATABASE INFO"),"#data#",tbl)
	Response.Write replace(tbl,"|", vbcrlf)
  End Sub

  Private Sub PrintCollection(Byval Name, ByVal Collection, ByVal CollectType, ByVal ShowInputBox)
	Dim vItem, tbl, Temp, DivSetNo
	if CollectType = 2  then
		tbl = "<form name=dbgCollection" & CollectType & " method=get><input type=hidden name=DebugInfo" & CollectType & " value=true>"
	elseif CollectType = 3 then
		tbl = "<form name=dbgCollection" & CollectType & " method=post><input type=hidden name=DebugInfo" & CollectType & " value=true>"
    else
		tbl = "<form name=dbgCollection" & CollectType & " method=post><input type=hidden name=DebugInfo" & CollectType & " value=true>"
    end if
    For Each vItem In Collection
      if isobject(Collection(vItem)) and CollectType <> 2 and CollectType <> 3 and CollectType <> 7 then
        tbl = AddRow(tbl, vItem, "{object}")
      elseif isnull(Collection(vItem)) then
        tbl = AddRow(tbl, vItem, "{null}")
      elseif isarray(Collection(vItem)) then
        tbl = AddRow(tbl, vItem, "{array}")
      elseif (CollectType = 2 and vItem <> "DebugInfo2") or (CollectType = 3 and vItem <> "DebugInfo3") or (CollectType <> 2 and CollectType <> 3) then
		if AllVars or ((CollectType = 7 and vItem <> "ALL_HTTP" and vItem <> "ALL_RAW") or CollectType <> 7) then
			if ShowInputBox then
				if Collection(vItem) <> "" then tbl = AddRowEdit(tbl, vItem, Collection(vItem))
			else
				if Collection(vItem) <> "" then tbl = AddRow(tbl, vItem, server.HTMLEncode(Collection(vItem)))
			end if	     
			' & " {" & TypeName(Collection(vItem)) & "}")
		else
			tbl = AddRow(tbl, vItem, "...")
		end if
	  end if
    Next
    select case CollectType
		case 5 	if ShowInputBox then
					tbl = tbl & "<TR><TD colspan='2'><HR></TR>"
					tbl = AddRowEdit(tbl,"Locale ID",Hex(Session.LCID))
					tbl = AddRowEdit(tbl,"Code Page",Session.CodePage)
					tbl = AddRow(tbl,"Session ID",Session.SessionID)
				else
					tbl = tbl & "<TR><TD colspan='2'><HR></TR>"
					tbl = AddRow(tbl,"Locale ID",Session.LCID & " (&H" & Hex(Session.LCID) & ")")
					tbl = AddRow(tbl,"Code Page",Session.CodePage)
					tbl = AddRow(tbl,"Session ID",Session.SessionID)
				end if	
		case 7 if Editing then
					tbl = tbl & "<TR><TD colspan='2'><HR></TR>"
					tbl = AddRowEdit(tbl,"Timeout",Server.ScriptTimeout)
				else
					tbl = tbl & "<TR><TD colspan='2'><HR></TR>"
					tbl = AddRow(tbl,"Timeout",Server.ScriptTimeout)
				end if	
	end select
	if (ShowInputBox or CollectType = 7) and Collection.count > 0 then 
		if CollectType = 2 or CollectType = 3 then
			tbl = tbl & "<TR><TD colspan='2'><a href='javascript:document.dbgCollection" & CollectType & ".submit();'>Re-post</a></TR>"
		else
			tbl = tbl & "<TR><TD colspan='2'><a href='javascript:document.dbgCollection" & CollectType & ".submit();'>Save Updates</a></TR>"
		end if
	end if	
	tbl = tbl & "</form>"
    tbl = MakeTable(tbl)
    DivSetNo = DivSetStatus(CollectType)
    if Collection.count <= 0 then DivSetNo =2
    tbl = replace(replace(DivSets(DivSetNo),"#title#",Name),"#data#",tbl)
    tbl = replace(tbl,"#sectname#",replace(Name," ",""))
	Response.Write replace(tbl,"|", vbcrlf)
  End Sub
  
  Private Function AddRow(byval t, byval var, byval val)
    t = t & "<TR valign=top><TD>" & var & "<TD>= " & val & "</TR>|"
    AddRow = t
  End Function

  Private Function AddRowEdit(byval t, byval var, byval val)
    t = t & "<TR valign=top><TD>" & var & "<TD>= <input type=text name=" & var & " value='" & val & "' size=" & len(val)+(len(val)\3)+1 & " class=dbgEditField></TR>|"
    AddRowEdit = t
  End Function

  Private Function MakeTable(byval tdata)
    tdata = "<table border=0 style=""font-size:10pt;font-weight:normal;"">" + tdata + "</Table>|"
    MakeTable = tdata
  End Function

  Public Sub Display
		dbg_FinishTime = Now
		dbg_ProcTime_End = Timer()
		Dim x
		DivSetStatus = split(DivSetStatus,",")
		Show = split(Show,",")
		For x = 0 to ubound(Show)
			DivSetStatus(x) = Show(x)
		Next
		Response.Write("<style>.dbgDivSetsHeader{font-weight:bold;border:1;outset;silver;cursor:hand;background:RoyalBlue;color:white;padding-left:4;padding-right:4;padding-bottom:2;}" & vbLF)
		Response.Write(".dbgDivSetsBody{border:1;inset;silver;cursor:text;display:none;background:lightsteelblue;padding-left:8;}" & vbLF)
		Response.Write(".dbgEditField{background:lightsteelblue;border:RoyalBlue 1px solid;}</style>" & vbLF)
		Response.Write("<script language='javascript'>function togglePanel(panelName) {if (document.getElementById(panelName).style.display=='none'){document.getElementById(panelName).style.display='block';}else{document.getElementById(panelName).style.display='none';}} </script>")
		Response.Write("<BR><Table id=dbgInfo width=100% cellspacing=0 border=0 style=""font-family:arial;font-size:9pt;font-weight:normal;""><TR><TD><DIV style=""border:1 outset buttonhighlight;background:Navy;color:white;padding:4;font-size:12pt;font-weight:bold;"">")
		Response.Write("<span onclick=""javascript:document.getElementById('dbgInfo').style.display='none';"" style='cursor:hand;'><font face='wingdings' color=white>x</font></span> Debug Information:</DIV>" & vbLF)
		Call PrintSummaryInfo(DivSetStatus(0))
		Call PrintCollection("VARIABLES", dbg_Data, 1, false)
		Call PrintCollection("QUERYSTRING", Request.QueryString(), 2, Editing)
		Call PrintCollection("FORM", Request.Form(), 3, Editing)
		Call PrintCookiesInfo(DivSetStatus(4))
		Call PrintCollection("SESSION", Session.Contents(), 5, Editing)
		Call PrintCollection("APPLICATION", Application.Contents(), 6, Editing)
		Call PrintCollection("SERVER VARIABLES", Request.ServerVariables(), 7,false)
		Call PrintDatabaseInfo(DivSetStatus(8))
        Call PrintCollection("SESSION STATIC OBJECTS", Session.StaticObjects(), 9,false)
        Call PrintCollection("APPLICATION STATIC OBJECTS", Application.StaticObjects(), 10, false)
  		Response.Write "</Table>"
  End Sub

  Private Sub Class_Terminate()
	'Display
    Set dbg_Data = Nothing
  End Sub

End Class

'Collection Editing routines - called from self
Sub dbgGoBack
	response.Write("<script language='javascript'>history.back();</script>")
End Sub

Sub dbgCheckUpdateCollections
	Dim cItem
	'Cookies
	if request.Form("DebugInfoCookies") = "true" then
		for each cItem in request.Form
			if instr(1,cItem,"!") > 0 then
				response.Cookies(left(cItem,instr(1,cItem,"!")-1)(right(cItem,len(cItem)-instr(1,cItem,"!")))) = request.Form(cItem)
			elseif cItem <> "DebugInfoCookies" then 
				response.Cookies(cItem) = request.Form(cItem)
			end if	
		next
		dbgGoBack
	end if
	'Session
	if request.Form("DebugInfo5") = "true" then
		for each cItem in request.Form
			if request.Form("Locale ID") <> "" then 
				if isnumeric(request.Form("Locale ID")) then
					session.LCID = clng(request.Form("Locale ID"))
				else
					session.LCID = clng("&H" & request.Form("Locale ID"))
				end if	
			elseif 	request.Form("Code Page") <> "" then
				if isnumeric(request.Form("Code Page")) then session.CodePage = clng(request.Form("Code Page"))
			elseif cItem <> "DebugInfo5" then
				session(cItem) = request.Form(cItem)
			end if	
		next
		dbgGoBack
	end if
	'Application
	if request.Form("DebugInfo6") = "true" then
		for each cItem in request.Form
			if cItem <> "DebugInfo6" then
				application(cItem) = request.Form(cItem)
			end if
		next
		dbgGoBack
	end if
	'Server Variables
	if request.Form("DebugInfo7") = "true" then
		if request.Form("Timeout") <> "" and isnumeric(request.Form("Timeout")) then server.ScriptTimeout = clng(request.Form("Timeout"))
		dbgGoBack
	end if
End Sub

dbgCheckUpdateCollections
%>
