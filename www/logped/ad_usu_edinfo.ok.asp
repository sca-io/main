<%@ Language=VBScript %>

<!-- #include file = "script/bloqueio.asp" -->

<!-- #include file = "script/logado.asp" -->

<!-- #include file = "script/funcao.asp" -->

<!-- #include file = "script/seguir.asp" -->

<!-- #include file = "script/topo.asp" -->

<% 'VerificarNovosDados %>

<table width='95%' bgcolor='#37833E' align='center' border="0" cellspacing="1" cellpadding="0" ID="tblCentral">
		<tr>
			<td class='maintitle'>
				<table width='100%' border='0' cellspacing='0' cellpadding='3' id="tblSubCentral">
					<tr>
						<td><IMG src='<%=Link("img/gif_ico_nav_titulo.gif")%>' alt='' width="8" height="8" border="0"></td>
						<td width='100%' class='maintitle'>Central de controle</td>
						<td align='right' class='maintitle' nowrap></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
		<td>
			<table class='mainbg' width="100%" border="0" cellspacing="1" cellpadding="4" ID="tblCeont">
				<tr>
					<td nowrap class='titlemedium'>&nbsp;</td>
					<td width="100%" nowrap class='titlemedium'>&nbsp;</td>
				</tr>
				<tr>
					<td class='row1' valign="top">
						<!-- #include file = "script/montarMenu.asp" -->
					</td>
					<td valign='top' class='row1'>
						<table cellpadding='4' cellspacing='3' width='100%' align='left' border='0' ID="tblMostrar">
							
							<!-- In�cio do conte�do -->
							
								<%
									
								'Verifica��es para altera��o das informa��es:
									Dim Rejeitar, Nome, Razao, Contato, email, Cod_cli
									
									Nome = Form("txtNome")
									Razao = Form("txtRazao")
									'Contato = Form("txtContato")
									Email = Form("txtEmail")
									Cod_cli = Form("txtCod_cli")
								
									Rejeitar = Empty
									
									'Erros:
									
									If Nome = Empty OR Razao = Empty OR Email = Empty then
										Rejeitar = "|Voc� deve completar todos os campos antes de prosseguir"
									End If

									If Not IsEmail(Email) then
										Rejeitar = Rejeitar & "|O e-mail informado � inv�lido"
									End If
									
									If Nome = Form("NomeBkp") AND Razao = Form("RazaoBkp") AND Contato = Form("ContatoBkp") AND Email = Form("EmailBkp") AND Cod_cli = Form("Cod_cliBkp") then
										Rejeitar = "|Voc� precisa atualizar pelo menos um campo"
									End If

									
									If Rejeitar <> Empty then
									'Ocorreu um erro
									
									Acao "Erro em ad edinfo: " & Rejeitar
									MostrarSubTitulo "Erro na edi��o de informa��es"
								%>
								
									<tr> 
									<td class='row1' valign='top'>
										Nenhuma informa��o foi alterada.<br><br>
									<b>Segue o relat�rio do(s) erro(s):</b>
										<br>
										<span class='highlight'>
										<%=Replace(Rejeitar, "|", "<br>&#149; ")%><br><br>Por favor, retorne e confirme os dados digitados.</span>
										<br><br>
										<br><br>
										<b>Para evitar outros erros, siga as seguintes recomenda��es:</b>
										<br><br>
										&#149; Preencha todos os campos<br>
										&#149; Informe um e-mail v�lido<br>
										<br><br>
										<br><br>
									</td>
									</tr>
									<tr> 
									<td class='titlefoot' align='center'><A href="javascript:history.go(-1);"><IMG src="<%=Link("img/gif_bot_voltar.gif")%>" alt="Voltar" border="0" title=""></A></td>
									</tr>
								<%
								else 'info alterada
								
									Set BD = CreateObject("ADODB.Connection")

									BD.Open Application("bd-logped")

								Dim strSQL_UPDATE
								strSQL_UPDATE = Empty
								
								If Nome <> Form("NomeBkp") Then
									strSQL_UPDATE = "nome = '" & ucase(Nome) & "'"
									Acao "EdInfoAd Nome:" & Form("NomeBkp")
								End If
								
								If Email <> Form("EmailBkp") Then
									If strSQL_UPDATE <> Empty Then strSQL_UPDATE = strSQL_UPDATE & ", "
									strSQL_UPDATE = strSQL_UPDATE & "email = '" & lcase(Email) & "'"
									Acao "EdInfoAd Email:" & Form("EmailBkp")
								End If

								'If Contato <> Form("ContatoBkp") Then
								'	If strSQL_UPDATE <> Empty Then strSQL_UPDATE = strSQL_UPDATE & ", "
								'	strSQL_UPDATE = strSQL_UPDATE & "contato = '" & Contato & "'"
								'	Acao "EdInfoAd Contato:" & Form("ContatoBkp")
								'End If

								If Razao <> Form("RazaoBkp") Then
									If strSQL_UPDATE <> Empty Then strSQL_UPDATE = strSQL_UPDATE & ", "
									strSQL_UPDATE = strSQL_UPDATE & "razao_social = '" & ucase(Razao) & "'"
									Acao "EdInfoAd Razao:" & Form("RazaoBkp")
								End If
								
								If Cod_cli <> Form("Cod_cliBkp") Then
									If strSQL_UPDATE <> Empty Then strSQL_UPDATE = strSQL_UPDATE & ", "
									strSQL_UPDATE = strSQL_UPDATE & "cod_cli = '" & Cod_cli & "'"
									Acao "EdInfoAd Cod_cli:" & Form("Cod_cliBkp")
								End If

									IF strSQL_UPDATE <> Empty Then
										BD.Execute "UPDATE usuario SET " & strSQL_UPDATE & " WHERE id = " & Request.QueryString("UsuarioID") & ";"
										
										BD.Close
										set bd=nothing
										
										MostrarSubTitulo "Informa��es atualizadas"
										%>
										<tr> 
										<td class='row1' valign='top'>
											<img src="<%=Link("img/gif_ico_aviso.gif")%>">&nbsp;&nbsp;As informa��es alteradas foram atualizadas.<br><br>
											<br><br>
											<!--<b>Por motivos de seguran�a, voc� receber� um e-mail com as novas informa��es.</b>-->
											<br><br></p>
										</td>
										</tr>
										<tr> 
										<td class='titlefoot' align='center'><A href="<%=Link("acesso.asp")%>"><IMG src="<%=Link("img/gif_bot_voltar.gif")%>" alt="Voltar" border="0" title=""></A></td>
										</tr>								
										<%
									Else
										MostrarSubTitulo "Erro na atualiza��o"
										%>
										<tr> 
										<td class='row1' valign='top'>
											<img src="<%=Link("img/gif_ico_aviso.gif")%>">&nbsp;&nbsp;As informa��es n�o foram alteradas.<br><br>
											<br><br>
											<br><br></p>
										</td>
										</tr>
										<tr> 
										<td class='titlefoot' align='center'><A href="<%=Link("acesso.asp")%>"><IMG src="<%=Link("img/gif_bot_voltar.gif")%>" alt="Voltar" border="0" title=""></A></td>
										</tr>
										<%
									End IF 'strSQL_UPDATE <> Empty
								end if 'teve erro?
								
								%>
									
							<!-- Fim do conte�do -->
								
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!-- #include file = "script/fim.asp" -->