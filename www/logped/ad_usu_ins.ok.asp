<%@ Language=VBScript %>

<!-- #include file = "script/bloqueio.asp" -->

<!-- #include file = "script/logado.asp" -->

<!-- #include file = "script/funcao.asp" -->

<!-- #include file = "script/seguir.asp" -->

<!-- #include file = "script/topo.asp" -->

<% 'VerificarNovosDados %>

<table width='95%' bgcolor='#37833E' align='center' border="0" cellspacing="1" cellpadding="0" ID="tblCentral">
		<tr>
			<td class='maintitle'>
				<table width='100%' border='0' cellspacing='0' cellpadding='3' id="tblSubCentral">
					<tr>
						<td><IMG src='<%=Link("img/gif_ico_nav_titulo.gif")%>' alt='' width="8" height="8" border="0"></td>
						<td width='100%' class='maintitle'>Central de controle</td>
						<td align='right' class='maintitle' nowrap></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
		<td>
			<table class='mainbg' width="100%" border="0" cellspacing="1" cellpadding="4" ID="tblCeont">
				<tr>
					<td nowrap class='titlemedium'>&nbsp;</td>
					<td width="100%" nowrap class='titlemedium'>&nbsp;</td>
				</tr>
				<tr>
					<td class='row1' valign="top">
						<!-- #include file = "script/montarMenu.asp" -->
					</td>
					<td valign='top' class='row1'>
						<table cellpadding='4' cellspacing='3' width='100%' align='left' border='0' ID="tblMostrar">
							
							<!-- In�cio do conte�do -->
							
								<%
									
								'Verifica��es para altera��o das informa��es:
									Dim Rejeitar, Nome, Razao, Contato, email, Usuario
									
									Usuario = Form("txtUsuario")
									Nome = Form("txtNome")
									Razao = Form("txtRazao")
									Contato = NULL 'Form("txtContato") 'n�o h� necessidade
									Email = Form("txtEmail")
									Senha = FormataSenha(Form("txtSenha"))
									Cod_cli = NULL 'Form("txtCod_cli") 'retirado
									Habilitado = Form("chkHabilitado")
									NivelAcesso = Form("chkNivel")
								
									Rejeitar = Empty
									
									'Erros:
									If Nome = Empty OR Razao = Empty OR Email = Empty OR Usuario = Empty OR Senha = Empty OR Habilitado = Empty then
										Rejeitar = "|Voc� deve completar todos os campos antes de prosseguir"
									End If
									
									If isSenha(Senha) = false then
										Rejeitar = "|Senha deve conter 5 caracteres com letras e n�meros"
									End If

									If Not IsEmail(Email) then
										Rejeitar = Rejeitar & "|O e-mail informado � inv�lido"
									End If
									
									If Habilitado = "Liberado" then
										Habilitado = "1" '"TRUE"
									Elseif Habilitado = "Bloqueado" then
										Habilitado = "0" '"FALSE"
									Else
										Rejeitar = Rejeitar & "|O status de acesso � inv�lido"
									End If
									
									If NivelAcesso = "6" then
										NivelAcesso = "6"
									Elseif NivelAcesso = "7" then
										NivelAcesso = "7"
									Else
										Rejeitar = Rejeitar & "|O n�vel de acesso � inv�lido"
									End If
									
									Set BD = CreateObject("ADODB.Connection")
									Set RS = CreateObject("ADODB.Recordset")
									BD.Open Application("bd-logped")
									RS.Open "SELECT id FROM usuario Where usuario = '" & Usuario & "';", BD
									
									If RS.EOF <> true Then 'J� existe esse nome de usu�rio
										Rejeitar = Rejeitar & "|O usu�rio informado j� est� sendo usado"
									End If
									
									RS.Close
									BD.Close
									Set RS = Nothing
									Set BD = Nothing
									
									
									If Rejeitar <> Empty then
									'Ocorreu um erro
									
									Acao "Erro ad_usu_novo: " & Rejeitar
									MostrarSubTitulo "Erro na inclus�o de novo usu�rio"
								%>
								
									<tr> 
									<td class='row1' valign='top'>
										Nenhuma informa��o foi adicionada.<br><br>
									<b>Segue o relat�rio do(s) erro(s):</b>
										<br>
										<span class='highlight'>
										<%=Replace(Rejeitar, "|", "<br>&#149; ")%><br><br>Por favor, retorne e confirme os dados digitados.</span>
										<br><br>
										<br><br>
										<b>Para evitar outros erros, siga as seguintes recomenda��es:</b>
										<br><br>
										&#149; Preencha todos os campos<br>
										&#149; Informe um e-mail v�lido<br>
										<br><br>
										<br><br>
									</td>
									</tr>
									<tr> 
									<td class='titlefoot' align='center'><A href="javascript:history.go(-1);"><IMG src="<%=Link("img/gif_bot_voltar.gif")%>" alt="Voltar" border="0" title=""></A></td>
									</tr>
								<%
								else 'usu�rio adicionado
								
										Set BD = CreateObject("ADODB.Connection")

										BD.Open Application("bd-logped")

											BD.Execute "INSERT INTO usuario (usuario, senha, nome, nivelacesso, dataaltsenha, datainclusao, habilitado, cod_cli, razao_social, contato, email) VALUES ('" & ucase(Usuario) & "', '" & lcase(FormataSenha(Senha)) & "', '" & ucase(Nome) & "', " & NivelAcesso & ", '2001/12/30', '" & DataSql(Date) & " " & Time() & "', '" & Habilitado & "', '" & Cod_cli & "', '" & ucase(Razao) & "', '" & Contato & "', '" & lcase(Email) & "');"
											Acao "Inseriu usu�rio: '" & usuario & "','" & senha & "'"
										
										BD.Close
										set bd=nothing
										
										MostrarSubTitulo "Informa��es atualizadas"
										%>
										<tr> 
										<td class='row1' valign='top'>
											<img src="<%=Link("img/gif_ico_aviso.gif")%>">&nbsp;&nbsp;O novo usu�rio <b><%=lcase(Usuario)%></b> foi inserido com sucesso.<br><br>
											<br><br>
											<b>No primeiro <i>login</i> o usu�rio ser� alertado a alterar a senha.</b>
											<br><br></p>
										</td>
										</tr>
										<tr> 
										<td class='titlefoot' align='center'><A href="<%=Link("acesso.asp")%>"><IMG src="<%=Link("img/gif_bot_voltar.gif")%>" alt="Voltar" border="0" title=""></A></td>
										</tr>								
										<%
								end if 'teve erro?
								
								%>
									
							<!-- Fim do conte�do -->
								
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!-- #include file = "script/fim.asp" -->