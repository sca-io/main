<? require_once("conf.php");
Usuario::autenticarLogon();

$idSecao = FERRAMENTAS;
$idSubSecao = '';
$id = isset($id)? secureResponse($id) : '';

$urlRetorno = "edicao_sapo_controle_espelhamento.php?idSecao=$idSecao";

$indexAbaEdicao = 1;
$indexAba = 1;

$nrSE = SapoEspelhamento::countListar();
$nrSCE = SapoControleEspelhamento::countListar('','status = '.NAOAVALIADO);

$processando = '';
if($nrSE>=1 && $nrSCE==1){
    $processando = ' (Processando)';
}

$gpAbas = new AbaSuperior();
$gpAbas->setIndexAba($indexAba);
$gpAbas->addItem("Espelhamento $processando",'edicao_sapo_controle_espelhamento.php?idSecao='.$idSecao);
    
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? include "includes/head.php";?>
<link rel="stylesheet" type="text/css" href="css/edicao-conteudo.css"/>
<script type="text/javascript">
var ultimaAcao = '';

var hora = 0;
var segundo = 0
var minuto = 0
var timer = null;
function cronometro(){
    segundo++
    if (segundo == 60){
        segundo = 0
        minuto++
    }
    if (minuto == 60){
        minuto = 0
        hora++
    }
    var htmlM = minuto;
    var htmlS = segundo;
    if(htmlM <= 9) htmlM = "0" + htmlM;

    if(htmlS <= 9) htmlS = "0" + htmlS;

    $("#time").html(hora+":"+htmlM+":"+htmlS)
    timer = setTimeout('cronometro();',1000);
}

function setWidthProgressBar(w){    
    $(".barra-de-progresso-peq .bar").css('width',w+'px');
}

function paraCronometro(){
    clearTimeout(timer);
}

function funcEspelhar(){
    $('A').attr("href","javascript:alerta('Aguarde o termino do processo!');");
    $("#crtlAvast").fadeIn();
    $("#crtlEspelhamento").attr("src","crtlEspelhamento.php");
    ultimaAcao = 'Espelhar';
}
<? /*
function funcVerificar(idSce){
    $('A').attr("href","javascript:alerta('Aguarde o termino do processo!');");
    $("#crtlAvast").fadeIn();
    $("#crtlEspelhamento").attr("src","crtlEspelhamento.php?acao=Verificar&idSCE="+idSce);
    ultimaAcao = 'Verificar';
}

function pausar(){
    $("#crtlEspelhamento").attr("src","");
    $("#pcArq").html('Parado');
    $("#btPausar").replaceWith('<a href="javascript:continuar();" class="bt-padrao" title="Continuar" id="btContinuar">Continuar</a>');
    paraCronometro();
}

function continuar(){
    $("#crtlEspelhamento").attr("src","crtlEspelhamento.php?acao="+ultimaAcao);
    $("#btContinuar").replaceWith('<a href="javascript:pausar();" class="bt-padrao" title="Pausar" id="btPausar">Pausar</a>');
    cronometro();
}*/?>

function concluir(){
    window.location = '<?=$urlRetorno?>';
}
</script>
</head>
<body>
	<div id="sapo">
		<div class="container">
		  <? include "includes/topo.php";?>          
          <? include "includes/menu-lateral.php";?>          
          <div class="coluna-geral">
          	<? include "includes/topo_int.php";?>
            <div class="modulo-geral">
            	<form name="formulario" id="formulario" method="post" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="<?=$id?>" />
                    <input type="hidden" id="acao" name="acao" value="" />
                                                         
                    <input type="hidden" id="urlRetorno" name="urlRetorno" value="<?=$urlRetorno?>" />
                  <div class="esq">
                      
                      <h5>Espelhamento</h5>
                      <?
                      
                      
                      if($nrSE==0){
                          ?>
                      <p>Sistema ainda não espelhado!</p>
                          <?
                      }else{
                          ?>
                      <p><?=$nrSE?> Arquivo(s) cadastrados</p>
                          <?
                      }
                      ?>
                      <?
                      if($nrSE==0){
                      ?>                      
                      <a href="javascript:funcEspelhar();" class="bt-padrao" title="Espelhar" id="btEspelhar">Espelhar</a>
                      <?
                      }else{
                          if($nrSCE==1){
                      ?>
                      <p>Sistema sendo espelhado. Acesse esta página novamente mais tarde para maiores detalhes.
                      <?
                          }else{
                       ?>
                      <a href="javascript:funcEspelhar();" class="bt-padrao" title="Novo Espelhamento" id="btEspelhar">Novo Espelhamento</a>
                      <?  
                          }
                      }                      
                      ?>   
                      <div id="crtlAvast">
                          <table class="avast" border="0" cellpadding="5" cellspacing="5">
                              <tr> <td class="alignRight" id="prc">0%</td><td><div class="barra-de-progresso-peq"><div class="bar"></div><div class="end"></div></div></td> </tr>
                              <tr valign="top"> <td class="alignRight">Arquivo processado:</td><td id="pcArq"></td> </tr>
                              <tr valign="top"> <td class="alignRight">Tempo de execução:</td><td id="time">0:00:00</td> </tr>
                              <tr valign="top"> <td class="alignRight">Arquivos testados:</td><td id="nR">0</td> </tr>
                              <tr valign="top"> <td class="alignRight">Arquivos diferentes:</td><td id="diffArq">0</td> </tr>
                              <tr valign="top"> <td class="alignRight" rowspan="2"><iframe frameborder="0" width="150" height="50" src='' id="crtlEspelhamento"></iframe></td>
                                  <td id="retorno"></td> </tr>
                              <tr valign="top"> <td>
                                      <? /*<a href="javascript:pausar();" class="bt-padrao" title="Pausar" id="btPausar">Pausar</a>*/?>
                                      <a href="javascript:concluir();" class="bt-padrao" title="Concluir" id="btConcluir">Concluir</a>
                                  </td> </tr>                              
                          </table>                          
                          
                      </div>   
                      <br/><br/>
                      <?
                        $lstLR = SapoControleEspelhamento::listar('','','status = '.ATIVO,'data_vrf DESC');
                        if($lstLR){
                        ?>
                      <h5>Verificações Anteriores</h5>
                      <table class="tabela" cellspacing="0">
                        <thead>
                            <tr>
                                <th><a>&nbsp;Num&nbsp;</a></th>
                                <th><a>&nbsp;Data&nbsp;</a></th>
                                <th><a>&nbsp;Total Arq.&nbsp;</a></th>
                                <th><a>&nbsp;Arquivos OK&nbsp;</a></th>
                                <th><a>&nbsp;Arquivos Err.&nbsp;</a></th>
                                <th><a>&nbsp;Ação&nbsp;</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?
                                foreach($lstLR as $lr){
                                    $sID = secureResponse($lr->getId());
                                   ?>
                            <tr>
                                <td>#<?=$lr->getId()?></td>
                                <td><?=Util::dataDoBDcomHora($lr->getDataVrf())?></td>
                                <td><?=$lr->getNumArqVrf()?></td>
                                <td><?=$lr->getNumArqOk()?></td>
                                <td><?=$lr->getNumArqErr()?></td>
                                <td>
                                    <a href="javascript:;" onclick="openPop('pop-celog.php?parametros=<?=urlencode("&idSce=$sID")?>')" class="bt-padrao" title="Ver os logs desta verificação">Ver Logs</a>
                                    <? /*<a href="javascript:funcVerificar('<?=$sID?>');" class="bt-padrao" title="Refazer esta verificação" id="btRevalidar">Revalidar</a>*/?>
                                </td>
                            </tr>
                               <? 
                            }
                            ?>
                        </tbody>
                    </table>
                      <? 
                        }
                        ?>
                  </div>
                </form>
            </div>
          </div>          
        </div>
    </div>
</body>
</html>