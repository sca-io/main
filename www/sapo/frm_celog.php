<? require_once("conf.php");
Usuario::autenticarLogon();

$idSce = secureRequest('idSce');

$acao = request('acao');
$status = request('status')?request('status'):NAOAVALIADO;
$flgErro = request('flgErro');
$idSE = request('idSE');

?><!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
<meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE" />
<head>
<script src='js/jquery-1.3.2.min.js'></script>
<script src="js/jquery.min.js"type="text/javascript"></script>
<script src="js/Popup.js" language="JavaScript" type="text/javascript"></script>
<script src="js/scripts.js" language="JavaScript" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/estrutura.css"/>
<link rel="stylesheet" type="text/css" href="css/pop-up.css"/>
<script type="text/javascript">
var msg = Array();
var vld = Array();
var destino_frm = "frm_celog.php";
var target_frm = "popCELog";

function buscar(){
    var frm = document.formulario;
    fSucess = function(url){
        frm.acao.value = "Buscar";
        frm.action = destino_frm;
        frm.submit();        
        
    }
    fErro = function(strErro){
        var str = sapo_replace(",",";<br/>",strErro);
        alerta("<b>Por favor preencha os campos:</b><br/>"+str);
        enviando = 0;
    }
    validaFormulario('formulario',fSucess,fErro);
}

$(window).ready(function(){
    var iHeight = $(".pop-galeria-de-imagens").height();
    var iWidth = $(".pop-galeria-de-imagens").width();
    if(parseInt(iHeight) > 600){
        parent.$("#popCELog").attr("height",iHeight);
    }else{
        parent.$("#popCELog").attr("height",600);
    }
    if(parseInt(iWidth) > 597){
        parent.$("#popCELog").attr("height",iWidth);
    }else{
        parent.$("#popCELog").attr("height",597);
    }
});
</script>
<style type="text/css">
BODY{ background-color: #FFF; }

</style>
</head>
<body>
    <div class="pop-galeria-de-imagens">
        <h3>LOG Espelhamento<a onClick="parent.popup.close();" class="bt-fechar" title="Fechar">X</a></h3>
        <form name="formulario" id="formulario" action="" method="post" target="popCELog" enctype='multipart/form-data'>
            <input type="hidden" name="acao" value="" />
            <input type="hidden" name="idSce" value="<?=secureResponse($idSce)?>" />
            <div class="inserir">                
                <p>Verificaçao #<?=$idSce?></p>
                <p>Status:
                <select name="status" id="status">
                    <?
                    $std = Array('','','');
                    $std[$status] = 'selected';
                    ?>
                    <option value="<?=NAOAVALIADO?>" <?=$std[NAOAVALIADO]?>>Novos</option>
                    <option value="<?=ATIVO?>" <?=$std[ATIVO]?>>Ja vistos</option>
                </select>
                </p>
                <p>Erros:
                <select name="flgErro" id="status">
                    <?
                    $std = Array('','','','','','','','','');
                    $std[$flgErro] = 'selected';
                    ?>
                    <option value="">Todos</option>
                    <option value="<?=CE_FLAG_ERRO_FDATAM?>" <?=$std[CE_FLAG_ERRO_FDATAM]?>>Data de Modificação</option>
                    <option value="<?=CE_FLAG_ERRO_FOWNER?>" <?=$std[CE_FLAG_ERRO_FOWNER]?>>Dados de criação</option>
                    <option value="<?=CE_FLAG_ERRO_FEXT?>" <?=$std[CE_FLAG_ERRO_FEXT]?>>Extensão</option>
                    <option value="<?=CE_FLAG_ERRO_FPERM?>" <?=$std[CE_FLAG_ERRO_FPERM]?>>Permissão</option>
                    <option value="<?=CE_FLAG_ERRO_FSIZE?>" <?=$std[CE_FLAG_ERRO_FSIZE]?>>Tamanho</option>
                    <option value="<?=CE_FLAG_NOVO_ARQUIVO?>" <?=$std[CE_FLAG_NOVO_ARQUIVO]?>>Novo</option>
                </select>
                </p>
                <p>ID Arquivo:
                <input type="text" name="idSE" id="idSE" />
                </p>
                <p><a href="javascript:buscar();" class="bt-padrao">Buscar</a></p>        
            </div>
        </form>
        <table class="tabela" cellspacing="0">
            <thead>
                <tr>
                    <th><a>&nbsp;Mensagem&nbsp;</a></th>
                    <? /*<th class='acao'><a>&nbsp;Ação&nbsp;</a></th>*/?>
                </tr>
            </thead>
            <tbody>
                <?      
                $where = '';
                if($status != ''){
                    if($where != ''){
                        $where .= ' AND ';
                    }
                    $where .= "status = $status";
                }
                if($flgErro != ''){
                    if($where != ''){
                        $where .= ' AND ';
                    }
                    $where .= "flg_erro = $flgErro";
                }
                
                $ncadastro =  SapoCELog::countListar($idSce,$idSE,$where);

                $pagina = (isset($_REQUEST['pagina']))? $_REQUEST['pagina'] : 0;
                $total_reg_pag = 15;
                $init_reg_pag = ($pagina * $total_reg_pag);	 
                $aux_pagina = $pagina + 1;
                $total_paginas = $ncadastro/$total_reg_pag;

                $by = (isset($_REQUEST['by'])) ? $_REQUEST['by'] : "id";
                $ordem = (isset($_REQUEST['ordem'])) ? $_REQUEST['ordem'] : "DESC";

                if($ncadastro>$total_reg_pag){
                    $paginacao = new Paginacao($pagina,$ncadastro,$total_reg_pag);
                    $paginacao->setUrl("&idSce=".secureResponse($idSce)."&acao=$acao&status=$status&flgErro=$flgErro");
                }
                ?>
                <p>Registros: "<?=$ncadastro?>"</p>        
                <?
                $lsLogs = SapoCELog::listar($idSce,$idSE,'',$where," $by $ordem","$init_reg_pag, $total_reg_pag");
                if($lsLogs){
                    foreach($lsLogs as $lr){
                        $sID = secureResponse($lr->getId());
                        if($lr->getStatus() == NAOAVALIADO){
                            $lr->setStatus(ATIVO);
                            $lr->salvar();
                        }
                       ?>
                <tr>
                    <td><?=$lr->getMsg()?></td>
                </tr>
                   <? 
                    }
                }else{
                    ?>
                <tr>
                    <td>Nenhum log encontrado com estes parâmentros.</td>
                </tr>
                   <?
                }
                ?>
            </tbody>
        </table>
        <? if($ncadastro>$total_reg_pag){ ?>
        <div class="paginacao"><?=$paginacao->printHtml()?></div>    
        <? } ?>
    </div>
</body>
</html>