<?
require_once("conf.php");
header("Content-type: text/html; charset=UTF-8");

$controleAcesso->addWhiteListReferer(array(
    "sapo/edicao_home.php",
    "sapo/edicao-media.php",
    "sapo/edicao_chamadas.php",
    "sapo/frm_media_crop.php"
));
$controleAcesso->autenticarReferer();
Usuario::autenticarLogon();

set_time_limit(24*60*60);
ignore_user_abort(1);

function criarDir($dir){
    if(!file_exists($_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ.$dir)) mkdir($_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ.$dir, 0777);
}

$id = secureRequest('id');
$idSecao = request('idSecao');
$acao = request('acao');	
$indexAba = request('indexAba');
$urlRetorno = request('urlRetorno');
$callBack = request('callBack');
//print_r($_FILES);exit;

if($acao == 'salvarImg'){
    $tipoProp = request("tipoProp");
    $modoVis = request("modoVis");
    $tipo = request("tipo");
    $idConteudo = secureRequest("idConteudo");
    $idDestaque = request("idDestaque");
    $idioma = request('idioma');
    $assunto = request('assunto');
   // $template = request("template");
    if($idDestaque){ //NO caso das sub-homes         
        $idConteudo = $idDestaque;
    }
    $titulo = request("titulo");
    $credito = request("credito");
    $legenda = request("legenda");
    $descricao = request("descricao");
    $urlLink = request("urlLink");
    $targetLink = request("targetLink");
    $urlImg = request("urlMedia");
    $dir = request("dir");
    $arq = $_FILES['arquivo']; 
    
    $control = new ControleMedia(); 
    $errosRq = Upload::checkErrosRequest($arq);
    if($errosRq){
         print "<script>parent.alert('".$errosRq."');document.location='$urlRetorno';</script>";exit;
    }
    $media = null;    
    if($id){
        $media = Media::ler($id);
        $urlImg = $urlImg?$urlImg :$media->getUrl();
    }
    
    if(!$media){   
        $media = new Media();
        $media->setDataInsercao(date("Y-m-d H:i:s"));
        $media->setOrdem(Media::countListar($idConteudo,$idSecao,"status = ".ATIVO." AND lang = '$idioma'") + 1);
    }  
    //print_r($_REQUEST);exit;
    //print Media::getLogSql();
    if($urlImg){
        
        $media->setTipo($tipo);
        $media->setIdConteudo($idConteudo); 
        $media->setIdSecao($idSecao); 
        $media->setIdCategoria($assunto); 
        $media->setNome($titulo); 
        $media->setCredito($credito);
        $media->setLegenda($legenda);
        $media->setDescricao($descricao);
        $media->setDir($dir);  
        $media->setUrl($urlImg);    
        if($urlLink && $targetLink == "_blank") $urlLink = "http://".str_replace("http://","",$urlLink);
        $media->setUrlLink($urlLink);  
        $media->setTargetLink($targetLink);
        $media->setLang($idioma);
        $media->setModoVis($modoVis);
        $media->setStatus(ATIVO);        
        $media->salvar();
        
        //exit;
        if($idDestaque){
            print "<script>document.location='$urlRetorno&id=".secureResponse($media->getId())."&acao=crop';</script>";           
        }else{
            if($tipo == Media::IMAGEM){
                $urlRetorno = urlencode($urlRetorno);
                print "<script>document.location='frm_media_crop.php?id=".secureResponse($media->getId())."&idSecao=$idSecao&idConteudo=$idConteudo&tipoProp=$tipoProp&urlRetorno=$urlRetorno&teste=1';</script>";
            }else{
                print "<script>window.location='$urlRetorno';</script>";
            }            
        }        
    }else{
        if($msgErroFrmt){
            $control->msgErroFrmt = $msgErroFrmt;
        }else{
            if($tipo == Media::IMAGEM) $control->msgErroFrmt = ' \nUtilize apenas os formatos: .jpg, .gif ou .png';
            if($tipo == Media::DOCS) $control->msgErroFrmt = ' \nFormato de arquivo inválido';
        }
        print "<script>parent.alert('".$control->getMensagemError()."');document.location='$urlRetorno';</script>";
    }
}

if($acao == 'salvarOrdenacao'){
    $ids = request('ids');
    $ids = explode(";",$ids);
    $idxId = 1;
    foreach($ids as $id){
        $sID = secureRequest("",$id);        
        $obj = Media::ler($sID,"id,ordem");
        //print Media::getLogSql();
        if($obj){
            $obj->setOrdem($idxId);
            $obj->salvar();           
        }
        $idxId++;
    }
    print "OK";
}



if($acao == 'subirImagem'){
    $campo = request("campo");
    $idDestaque = request("idDestaque");
    $tipoProp = request("tipoProp");
    $arq = $_FILES[$campo]; 
    $tipo = request("tipoMedia");   
    if(!$tipo)$tipo = request("tipo");

    $control = new ControleMedia();
    $errosRq = Upload::checkErrosRequest($arq);
    if($errosRq){
         print "error[;]".$errosRq;  exit;
    }
    if(isset($arq["type"]) && $campo){  
        $apenasFormatos = array();
        if($idDestaque){
            if(isset($settModulosDeHome[$idSecao][$idDestaque]["filtro_exts"]))
                $apenasFormatos = $settModulosDeHome[$idSecao][$idDestaque]["filtro_exts"];
        }        
        $dirTipoDoc = "";
        switch($tipo){
            case Media::DOCS:                
                if(!$apenasFormatos) $apenasFormatos = Array('doc','docx','pptx','ppt','xls','xlsx','pdf');                
                $dirTipoDoc = PATH_DOCS;
            break;
            case Media::IMAGEM:
            default:    
                if(!$apenasFormatos) $apenasFormatos = Array('jpg','gif','png');
                $dirTipoDoc = PATH_IMGS;
            break;

        }        
        if(!file_exists($_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ.$dirTipoDoc.date("Y"))) mkdir($_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ.$dirTipoDoc.date("Y"), 0777);
        if(!file_exists($_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ.$dirTipoDoc.date("Y")."/".date("m"))) mkdir($_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ.$dirTipoDoc.date("Y")."/".date("m"), 0777);
        $diretorio = $dirTipoDoc.date("Y")."/".date("m")."/";
        $control->setDiretorioUpload(DIRETORIO_RAIZ.$diretorio);
        if(isset($porporcoes[$idSecao]) && isset($porporcoes[$idSecao][$tipoProp]))  $control->setPorporcao($porporcoes[$idSecao][$tipoProp]);
        elseif(isset($porporcoes["default"][$tipoProp]))  $control->setPorporcao($porporcoes["default"][$tipoProp]);
        $resUp = $control->fazerUpLoad($arq,"",$apenasFormatos);
        if($resUp){
            print "sucesso[;]".$resUp."[;]".$diretorio;
        }else{            
            print "error[;]".$control->getMensagemErrorUpload();
        }
    }else{
        print "error[;]Arquivo não selecionado".$arq["type"];
    }
}

if($acao == 'subirArquivo'){
    $campo = request("campo");
    $idDestaque = request("idDestaque");
    $arq = $_FILES[$campo]; 
    $apenasFormatos = array();
    $pathImportacao = "";

    $control = new ControleArquivo();
    $errosRq = Upload::checkErrosRequest($arq);
    if($errosRq){
         print "error[;]".$errosRq;  exit;
    }
    if($idDestaque){
        if(isset($settModulosDeHome[$idSecao][$idDestaque]["importacao"])) $settDest = $settModulosDeHome[$idSecao][$idDestaque]["importacao"];
        else $settDest = null;
        if($settDest){
            if(isset($settDest["exts"])) $apenasFormatos = $settDest["exts"];
            if(isset($settDest["path"])) $pathImportacao = $settDest["path"];
        }
    }
    if(isset($arq["type"]) && $campo){
        if(!$apenasFormatos) $apenasFormatos = Array('doc','docx','pptx','ppt','xls','xlsx','pdf');
        $diretorio = PATH_DOCS;         
        if($pathImportacao){
            criarDir(PATH_DOCS."importacao");
            criarDir(PATH_DOCS."importacao/".$pathImportacao);   
            $diretorio .= "importacao/$pathImportacao";   
        }
        
        $control->setDiretorioUpload(DIRETORIO_RAIZ.$diretorio);
        $resUp = $control->fazerUpLoad($arq,"",$apenasFormatos);        
        if($resUp){
            print "sucesso[;]".$resUp."[;]".$diretorio;
        }else{            
            print "error[;]".$control->getMensagemErrorUpload();
        }
    }else{
        print "error[;]Arquivo não selecionado".$arq["type"];
    }
}

if($acao == 'cortarImg'){
    $urlImg = request("src");
    $tipoProp = request("tipoProp");
    $idDestaque = request("idDestaque");
    $diretorio = request("diretorio");
    $x = request('x');
    $y = request('y');
    $w = request('w');
    $h = request('h');
    if($urlImg){
        $control = new ControleMedia();
        if(isset($porporcoes[$idSecao]))  $control->setPorporcao($porporcoes[$idSecao][$tipoProp]);
        else  $control->setPorporcao($porporcoes["default"][$tipoProp]);
        $control->setDiretorioUpload($diretorio);
        $retorno = $control->crop($urlImg,$w,$h,$x,$y,"","",false);
        if($retorno){
            /*print "<script>window.location='$urlRetorno';</script>";
            exit;*/
            if($callBack){
                print "<script>window.parent.popup.close();window.parent.$callBack()</script>";
            }else{				
                if($idDestaque){
                    print "<script>";
                    print "window.parent.location='$urlRetorno';//window.parent.popup.close();";				   
                    //else print "url = window.parent.window.location.toString().replace('&acao=cortarFoto','');parent.window.location.replace(url);";
                    print "</script>";
                }else{    
                    print "<script>window.location='$urlRetorno';</script>";            
                   // print "<script>novaImagem();parent.geraIframe('#cadastradas',true,true,'?idSecao=$idSecao');</script>";
                }
            }
        }else{
            print "<script>alert('Erro ao cortar a imagem');</script>";
        }
    }else{
        print "<script>alert('Imagem nao selecionada');</script>";
    }
}

if($acao == 'excluirImg'){
    $id = secureRequest("id");
    if($id){
        $arq = Media::ler($id);
        //print Media::getLogSql();
        $arq->setStatus(INATIVO);
        $arq->salvar();
        $msg = "";
        switch($arq->getTipo()){
            case Media::IMAGEM:
                $msg = "Imagem excluida com sucesso";
            break;    
            case Media::DOCS:
                $msg = "Arquivo excluido com sucesso";
            break;  
        }
        print "<script>alert('$msg');window.location='$urlRetorno';</script>";
    }else{
            print "<script>alert('Erro ao excluir');window.location= $urlRetorno;</script>";
    }
}



?>