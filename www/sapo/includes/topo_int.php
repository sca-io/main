<?
$idioma = isset($idioma)?$idioma:LNG_PT;
$nomePagina = end(explode("/", $_SERVER['PHP_SELF']));
$link2 = $link3 = $link1 = $nomeArea = "";
$isTopo = false;
if(!isset($secao)){
	$secao = null;
	if($idSecao)$secao = Secao::ler($idSecao);
}
$arrPref = explode('_',$nomePagina); // divide o nome da página pelo '_'
$idSubSecao = isset($idSubSecao)?$idSubSecao:0;
if($secao){    
    $nomeArea = $secao->getNome();	
    $isTopo = $secao->getTemTopo();
    $link1 = getUrl('frm_',$idSecao,$idSubSecao); //CAMINHO DA PÁGINA DE CONTEUDO
    if($idSecao == USUARIOS){
        $link2 = getUrl('frm_',$idSecao,$idSubSecao);
        $link3 = getUrl('listagem_',$idSecao,$idSubSecao);
    }else{
        /*$link2 = getUrl('edicao_',$idSecao,$idSubSecao); //CAMINHO DA PÁGINA DE CONTEUDO
        $link3 = getUrl('listagem_',$idSecao,$idSubSecao);*/
        $link2 = getUrlByTipo($secao->getTipo(),$idSecao,'edicao_'); //CAMINHO DA PÁGINA DE CONTEUDO
        $link3 = getUrlByTipo($secao->getTipo(),$idSecao,'listagem_');            
    }	
}else{
	//print "<script>document.location='index.php'</script>";
}
$indexAbaEdicao = isset($indexAbaEdicao)?$indexAbaEdicao:0;
if($indexAbaEdicao == 1){
    $classEdicao = "active";
    $classEdicao2 = "";
}elseif($indexAbaEdicao == 2  || $indexAbaEdicao || $arrPref[0] == 'listagem'){
    $classEdicao = "";
    $classEdicao2 = "active";
}

if(strstr($link2,"?")){
    $link2 .= "&acao=novo";
}else{
    $link2 .= "?acao=novo";
}   

$link2 .= "&idioma=$idioma";
$link3 .= "&idioma=$idioma";
?>
<div id="tp_int_geral">
    <h2><?=$nomeArea?></h2>
    <? if($isTopo):?>
    <div class="op-com-links">
    	<? /*if($idSecao == USUARIOS){
                $link2 .= (strpos($link2,"?") != 0)?"&":"?";
                $link3 .= (strpos($link3,"?") != 0)?"&":"?";
                $link2 .= "idSecao=$idSecao&acao=novo";
                $link3 .= "idSecao=$idSecao&indexAbaEdicao=2";
			}*/
		?> 
        <? if($secao->getTipo() == Secao::LISTA_DE_CONTEUDOS || $idSecao == USUARIOS || $secao->getTipo() == Secao::LISTA_DE_CHAMADAS):?>
        <a target="_self" href="<?=$link2?>" class="<?=$classEdicao?>">Criar um novo conteúdo</a>
        <a target="_self" href="<?=$link3?>" class="<?=$classEdicao2?>">&nbsp;Editar um conteúdo existente</a>   
        <? else:?>

        <? endif;?>
    </div> 
        <? if(isset($gpAbas)): //gpAbas é um objeto da classe AbaSuperior ?>
            <? $gpAbas->printHtml();?>            	
        <? else:?>		   

        <br>
        <? endif;?>
      
   
    <? endif;?>
</div>