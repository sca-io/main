<script type="text/javascript">
CK_HAS_LINK = true;
CK_HAS_STYLES = true;
CK_H = "300px";

function validarCoord(valor){
    valorEmNum = valor.replace(/[^0-9]/g, '');
    if(valor.indexOf(".") == -1) return false;
    if(valorEmNum.length == 0) return false; 
    arrValor = valor.split(".");
    if(!arrValor[0] && arrValor[0].length == 0) return false; 


    return true; 
}

function salvarCotacao(){  
    var frm = document.formulario;
    var destino_frm = "conteudo_controle.php";
    msg = Array();
    vld = Array();
    msg['estado'] = "Estado"; vld['estado'] = 1;  
    msg['nome'] = "Nome"; vld['nome'] = 1;    
    msg['link'] = "Link"; vld['link'] = 1;
    msg['posx'] = "Lat"; vld['posx'] = 1;
    msg['posy'] = "Long"; vld['posy'] = 1;
    msg['descricao'] = "Descrição"; vld['descricao'] = 1;


    if(frm.descricao.className == "ckeditor") frm.descricao.value = getTextEditorParent("descricao");
    <? if($max_len_texto):?>
    if(frm.descricao.value.length > <?=$max_len_texto?>) {
        alert("O texto do campo Condições ultrapassou o limite máximo de <?=$max_len_texto?> caracteres") ;return;      
    }   
    <? endif;?>     
    fSucess = function(url){
        if(!validarCoord(frm.posx.value)){
            alert("Preencha corretamente o campo de Latitude") ;return; 
        }
        if(!validarCoord(frm.posy.value)){
            alert("Preencha corretamente o campo de Longitude") ;return; 
        }  
       
        frm.acao.value = "salvarUnidadeProd";
        frm.action = destino_frm; 
        frm.target = "";
        frm.submit();
        $("#btSalvar").html("aguarde...").attr("href","#");
    }
    fErro = function(strErro){
        var str = sapo_replace(",",";<br/>",strErro);
        alerta("<b>Por favor preencha os campos:</b><br/>"+str);
        enviando = 0;
    }
    validaFormulario('formulario',fSucess,fErro)
}


</script>    
<? function getHtmlForm($id="",$nome="",$link="",$descricao="",$posx="",$posy=""){ ?>
    <? global $vetTpCot; global $idioma; global $idDestaque; ?>	

    <p>Nome: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="nome" id="nome" value="<?=$nome?>" /></p>
    <p>Link: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="link" id="link" value="<?=$link?>" /></p> 
    <p class="double-input">Posições: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lat: <input class="pequeno" type="text" name="posx" id="posx" value="<?=$posx?>" /> Long: <input class="pequeno" type="text" name="posy" id="posy" value="<?=$posy?>" /></p> 
    <p>Descrição: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<textarea class="ckeditor" name="descricao" id="descricao"><?=$descricao?></textarea></p>
    <? /*<p>Distância: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<textarea class="ckeditor" name="distancias" id="distancias"><?=$distancias?></textarea></p>   */?> 

    <a href="javascript:salvarCotacao();" class="bt-padrao" title="Salvar" id="btSalvar">Salvar</a>
<? }?>  
<? 
$estado = request("estado");
if($estado){
    $listar = true;
    $urlRetorno .= "&estado=$estado";
}else {$listar = false;}
?>
  

<p class="medio2"><br /><br />Estado: <select name="estado" id="estado" style="width:169px;" onChange="document.formulario.submit()">        
    <option value="">Selecione</option>
    <?=Estados::getHtmlOptions($estado); ?>
</select><br /><br /></p>



<blockquote <? if(!$id && $acao != "novo"){?>id="dd"<? }?>>
    <!-- <p><strong>Listagem:</strong></p> -->
<?
$nReg = 0;	
if( $listar ) {
    $whereTemp = "status = ".ATIVO." AND idioma = $idioma";
    if($estado) $whereTemp .= " AND estado = '$estado'";

    $listagem = UnidadeProdutora::listar("",$whereTemp,"ordem ASC");
    //print UnidadeProdutora::getLogSql();
    if($listagem){
        foreach($listagem as $l){
            $sID = secureResponse($l->getId());
            $nome = $l->getNome();                         
  
            $link = $l->getLink();     
            $descricao = $l->getDescricao();    

            $posMapa = $l->getPosMapa();
            $posx = $posy = "";
            if($posMapa){
                $posMapa = explode(",",$posMapa);
                if(count($posMapa) > 0){
                    $posx =  $posMapa[0];
                    $posy =  $posMapa[1];
                }            
            }

            ?>
            <div id="ch_<?=$sID?>">

                    <p>
                        <strong><?=$nome?></strong>
                    </p>
                    <div class="col">
                            <a href="<?=$urlRetorno?>&idioma=<?=$idioma?>&idDestaque=<?=$idDestaque?>&id=<?=$sID?>#ch_<?=$sID?>" title="Editar"></a>
                            <a href="javascript:excluirUnidadeProd('<?=$sID?>','<?=$urlRetorno?>');" title="Excluir"></a>
                    </div>
            <? if($id == $l->getId()){?>
                <div class="bloco">
                        <input type="hidden" name="id" id="id" value="<?=$sID?>"/>
                        <div>Editar<a href="<?=$urlRetorno?>&idioma=<?=$idioma?>&idDestaque=<?=$idDestaque?>">X</a></div>
                        <?=getHtmlForm($l->getId(),$nome,$link,$descricao,$posx,$posy)?> 
                </div>

            <? }?>
            </div>
            <?
            $nReg++;
        }
    }
}
?>
   <? if($acao == "novo"): ?>
    <div>
        <div class="bloco">
            <div>Novo<a id="form-novo" href="<?=$urlRetorno?>&idioma=<?=$idioma?>&idDestaque=<?=$idDestaque?>">X</a></div>              
            <?=getHtmlForm()?>    
        </div>
    </div>   
    <? endif; ?>
</blockquote>