<? function getHtmlFormMedia($id="",$titulo="",$credito="",$legenda="",$dir="",$urlMedia="",$urlLink="",$targetLink=""){ ?>    
    <? global $arr_campos;?>
	<? if(inArray('titulo', $arr_campos) || !count($arr_campos)):?>
		<p>Titulo: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="titulo" id="titulo" value="<?=$titulo?>" /></p> 
	<? endif;?>	
    <? /*<p>Crédito: &nbsp;&nbsp;<input type="text" name="credito" id="credito" value="<?=$credito?>" /></p>
    <p>Legenda: &nbsp;<input type="text" name="legenda" id="legenda" value="<?=$legenda?>" /></p> */?>
    <? if(inArray('link', $arr_campos) || !count($arr_campos)):?>
		<p>Link: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="urlLink" id="urlLink" value="<?=$urlLink?>" /></p>  
	<? endif;?>	
    <? if(inArray('target', $arr_campos) || !count($arr_campos)):?>
		<p>Abrir link em:<select name="targetLink" id="targetLink">
			<? $arrSeleced = array("_blank" => "","_self" => "") ; if(isset($arrSeleced[$targetLink])) $arrSeleced[$targetLink] = " selected=\"selected\""; ?>        
			<option value="_self"<?=$arrSeleced["_self"]?>>Mesma página</option>
			<option value="_blank"<?=$arrSeleced["_blank"]?>>Nova aba</option>   

		</select></p>
	<? endif;?>
    <? if(inArray('arquivo', $arr_campos) || !count($arr_campos)):?><p>Arquivo:&nbsp;&nbsp;&nbsp;<input type="file" name="arquivo" id="arquivo" /></p><? endif;?>


    <? if($urlMedia):?><p><img src="<?=DIRETORIO_RAIZ.$dir."".$urlMedia?>" width="270" id="srcImg" /></p><? endif;?>
    <input type="hidden" name="urlMedia" id="urlMedia" value="<?=$urlMedia?>"/>
    <input type="hidden" name="tipo" id="tipo" value="<?=Media::IMAGEM?>"/>
    <input type="hidden" name="dir" id="dir" value="<?=$dir?>"/>
    <a href="javascript:salvarMedia();" class="bt-padrao" title="Salvar" id="btSalvar">Salvar</a>
    <? if($urlMedia):?><a href="javascript:recortarImagem('<?=$urlMedia?>','<?=$dir?>');" class="bt-padrao" title="Recortar" id="btRecortar">Recortar Novamente</a><? endif;?>
<? }?>                      


<blockquote <? if(!$id && $acao != "novo"){?>id="dd"<? }?>>
    <?

    $nReg = 0;
    $lstMedias = Media::listar($idDestaque,$idSecao,"","status = ".ATIVO." AND lang = '$idioma'","ordem ASC");
    //print Media::getLogSql();
    if($lstMedias){
        foreach($lstMedias as $l){
            $titulo = $l->getNome();
            $credito = $l->getCredito();
            $legenda = $l->getLegenda();
            $urlMedia = $l->getUrl();
            $dir = $l->getDir();
            $urlLink = $l->getUrlLink();
            $targetLink = $l->getTargetLink();
            //$legenda = $objI->getDescricao();
            $sID = secureResponse($l->getId());
            $urlMediaAbs = DIRETORIO_RAIZ.$dir."".$urlMedia;
            ?>
    <div id="ch_<?=$sID?>">
		
        <p>
            <strong><?=$titulo?></strong>
			<strong><?=$urlLink?></strong>
            <?=$credito?>
        </p>
        <? $classExt = (!$titulo && !$urlLink && !$credito)?" extendido":""?>
        <div class="col<?=$classExt?>">
            <? $tamMedia = (!$titulo && !$urlLink && !$credito)?"270":"75"?>
            
            <? if($urlMedia):?><a class="foto"><img src="<?=$urlMediaAbs?>" width="<?=$tamMedia?>" /></a><? endif;?>
            <a href="<?=$urlRetorno?>&idioma=<?=$idioma?>&idDestaque=<?=$idDestaque?>&id=<?=$sID?>#ch_<?=$sID?>" title="Editar"></a>
            <a href="javascript:excluirMedia('<?=$sID?>','<?=$urlRetorno?>');" title="Excluir"></a>
        </div>
    <?
    if($id == $l->getId()){
        ?>
        <div class="bloco">
            <input type="hidden" name="id" id="id" value="<?=$sID?>"/>
            <div>Editar<a href="<?=$urlRetorno?>&idioma=<?=$idioma?>&idDestaque=<?=$idDestaque?>">X</a></div>
            <?=getHtmlFormMedia($l->getId(),$titulo,$credito,$legenda,$dir,$urlMedia,$urlLink,$targetLink)?> 
        </div>

        <?
    }
    ?>
    </div>
            <?
            $nReg++;
        }
    }
?>
   <? if($acao == "novo"){ ?>
    <div>
        <div class="bloco">
            <div>Novo<a id="form-novo" href="<?=$urlRetorno?>&idioma=<?=$idioma?>&idDestaque=<?=$idDestaque?>">X</a></div>              
            <?=getHtmlFormMedia()?>    
        </div>
    </div>   
    <? } ?>
</blockquote>
