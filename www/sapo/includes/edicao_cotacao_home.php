 <? function getHtmlFormCotacao($id="",$categoria="",$titulo="",$periodo="",$valor="",$perc=""){ ?>
    <? global $vetTpCot; global $idioma; global $idDestaque;?>
    <p>Titulo: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="titulo" id="titulo" value="<?=response_attr($titulo)?>" /></p> 
    <? if($idDestaque != 4):?><p>Período: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="periodo" id="periodo" value="<?=$periodo?>" /></p><? endif;?>
    
    
    <p>Valor: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="valor" id="valor" value="<?=$valor?>" /></p> 
    <p>Percentual: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="perc" id="perc" value="<?=$perc?>" /></p> 

    

    <a href="javascript:salvarCotacao();" class="bt-padrao" title="Salvar" id="btSalvar">Salvar</a>
<? }?>   
<?  $listar = true;?>
<?

 $dataGrupo = "";
 $tituloGrupo = "";
 $chapeuGrupo = "";
 $idChCotacao = "";
 $dadosExtra = null;
 if($idDestaque == 4) $listar = $categoria?true:false;
 $listaChams = Chamada::listar($idSecao, $idDestaque, "id,titulo,chapeu,data_publicacao", "status != ".Chamada::INATIVO." AND idioma = $idioma", "data_publicacao DESC", "1");

 if(isset($listaChams[0])){
     $dadosExtra = $listaChams[0];
 }
 if($dadosExtra){
     $idChCotacao = $dadosExtra->getId();
     $tituloGrupo = $dadosExtra->getTitulo();
     $dataGrupo = $dadosExtra->getDataPublicacao(); 
     if($dataGrupo == "0000-00-00 00:00:00") $dataGrupo = "";
     if($dataGrupo) $dataGrupo = Util::dataDoBD($dataGrupo);
     $chapeuGrupo = $dadosExtra->getChapeu(); 
 }
 
 ?> 
<script type="text/javascript">
function salvarCotacao(){  
    var frm = document.formulario;
    var destino_frm = "conteudo_controle.php";
    msg = Array();
    vld = Array();
    msg['titulo'] = "titulo"; vld['titulo'] = 1;
    if(frm.categoria){ msg['categoria'] = "Categoria"; vld['categoria'] = 1;}
    msg['valor'] = "Valor"; vld['valor'] = 1;
    msg['perc'] = "Percentual"; vld['perc'] = 1;

    
    fSucess = function(url){
        frm.acao.value = "salvarCotacao";
        frm.action = destino_frm; 
        frm.target = "";
        frm.submit();
        $("#btSalvar").html("aguarde...").attr("href","#");
    }
    fErro = function(strErro){
        var str = sapo_replace(",",";<br/>",strErro);
        alerta("<b>Por favor preencha os campos:</b><br/>"+str);
        enviando = 0;
    }
    validaFormulario('formulario',fSucess,fErro)
}
function salvarDadosExtraCotacao(){
    var frm = document.formulario;
    var destino_frm = "conteudo_controle.php";
    msg = Array();
    vld = Array();
    msg['tituloGrupo'] = "Titulo do Grupo"; vld['tituloGrupo'] = 1;  
    
    fSucess = function(url){
        frm.acao.value = "salvarDadosExtraCotacao";
        frm.action = destino_frm; 
        frm.target = "";
        frm.submit();
        $("#btSalvarDados").html("aguarde...").attr("href","#");
    }
    fErro = function(strErro){
        var str = sapo_replace(",",";<br/>",strErro);
        alerta("<b>Por favor preencha os campos:</b><br/>"+str);
        enviando = 0;
    }
    validaFormulario('formulario',fSucess,fErro)
} 

function subirArquivo(arquivo,calback){
    var frm = document.formulario;
    if(frm[arquivo].value){
        $("#ajaxLoad").css('opacity','0.7').fadeIn();
        frm.action = "media_controle.php?campo="+arquivo;
        frm.acao.value = 'subirArquivo';
        uploadMedia(frm,function(res){
            //alert(res);
            res = res.split("[;]");
             $("#ajaxLoad").fadeOut();
            if(res[0] == "sucesso"){
                frm.urlMedia.value = res[2]+res[1];
                if(calback) calback();                    
            }else{
                if(!res[1]){
                    alerta("Ocorreu um erro ao subir a imagem, verifique se a mesma não ultrapassa o limite máximo do servidor que é de <?=(LIMITE_MAX_UPLOAD)?>KB");
                }else{
                    alerta(res[1]);
                } 
            }
        })
    }else{
        alerta("Selecione um arquivo antes")
    }
}

function importarDadosCotacao(){
    var frm = document.formulario;
    var destino_frm = "importacao-dados.php";
    msg = Array();
    vld = Array();
    msg['arquivo'] = "Arquivo"; vld['arquivo'] = 1;     
    fSucess = function(url){
        subirArquivo('arquivo',function(){
            frm.acao.value = "importarDadosCotacao";
            frm.action = destino_frm; 
            frm.target = "";
            frm.submit();
            $("#btImportar").html("aguarde...").attr("href","#");                        
        })

    }
    fErro = function(strErro){
        var str = sapo_replace(",",";<br/>",strErro);
        alerta("<b>Por favor preencha os campos:</b><br/>"+str);
        enviando = 0;
    }
    validaFormulario('formulario',fSucess,fErro)
}  
</script>  

 <blockquote>
     <div class="bloco blocao-inicial">
        <h2>Dados do grupo:</h2>
        <input type="hidden" name="idChCotacao" id="idChCotacao" value="<?=secureResponse($idChCotacao)?>"/>
        <? if($idDestaque == 4):?>
            <p>Titulo: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="tituloGrupo" id="tituloGrupo" value="<?=$tituloGrupo?>" /></p> 
            <p>Data: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="dataGrupo" id="dataGrupo" value="<?=$dataGrupo?>" onclick="showInputCalendar(this)" /></p>
        <? else:?>
            <p>Data: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="dataGrupo" id="dataGrupo" value="<?=$dataGrupo?>" onclick="showInputCalendar(this)" /></p>
        <? endif;?> 
        <p><a href="javascript:salvarDadosExtraCotacao();" class="bt-padrao" title="Salvar" id="btSalvarDados">Salvar</a></p>
     </div>
     <div class="bloco blocao-inicial">

        <input type="hidden" name="urlMedia" id="urlMedia" value=""/>
        <input type="hidden" name="tipoMedia" id="tipoMedia" value="<?=Media::DOCS?>"/>
        
        <h2>Importação de dados:</h2> 
        <p>Arquivo: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" name="arquivo" id="arquivo" /></p>
        <p><a href="javascript:importarDadosCotacao();" class="bt-padrao" title="Importar" id="btImportar">Importar</a>
            <a href="dwld.php?<?="idSecao=".HOME."&idDestaque=".$idDestaque?>" class="bt-padrao" title="Importar" id="btImportar">Download Modelo</a></p>
        <? if(isset($settings["limite_chamadas"])):?>
        <p>*Limitado a <?=$settings["limite_chamadas"]?> linhas de registro</p>
        <? endif;?>
        
     </div>
 </blockquote>

  
<? if($idDestaque == 4):?>
<p class="medio2">Categoria:<select name="categoria" id="categoria" style="width:150px;" onChange="document.formulario.submit()">        
	<option value="">Selecione</option>
	<? $listaTipos = CategoriaCotacao::listar("id,nome","idioma = '$idioma' AND status = ".ATIVO);
            print CategoriaCotacao::getLogSql();
        ?>
	<? foreach($listaTipos as $l):?>
		<? $selectedTp = $categoria == $l->getId()?" selected=\"selected\"":""; ?>
		<option value="<?=$l->getId()?>" <?=$selectedTp?>><?=$l->getNome()?></option>
	<? endforeach;?>
</select>
<a href="javascript:openPop('pop-cat-cotacao.php?idioma=<?=$idioma?>');" class="bt-padrao margintop" title="Novo" id="btNovo">Novo</a>
</p>
<? endif;?>
  
<blockquote <? if(!$id && $acao != "novo"){?>id="dd"<? }?>>
    <!-- <p><strong>Listagem:</strong></p> -->
    <?
    $nReg = 0;
	
	if( $listar ) {
	$whereCot = "status = ".ATIVO." AND idioma = $idioma";
	if($categoria) $whereCot .= " AND categoria = $categoria";

    $listagem = Cotacao::listar($idDestaque,"",$whereCot,"ordem ASC");
    //print Chamada::getLogSql();
    if($listagem){
		foreach($listagem as $l){
			$sID = secureResponse($l->getId());
			$categoria = $l->getCategoria();
			$titulo = $l->getTitulo();
			$periodo = $l->getPeriodo();
			$valor = $l->getValor();
			$perc = $l->getPerc();

			?>
			<div id="ch_<?=$sID?>">
				
				<p>
					<strong><?=$titulo?></strong>
					<?=$periodo?>
				</p>
				<div class="col">
					<a href="<?=$urlRetorno?>&idioma=<?=$idioma?>&idDestaque=<?=$idDestaque?>&id=<?=$sID?>#ch_<?=$sID?>" title="Editar"></a>
					<a href="javascript:excluirCotacao('<?=$sID?>','<?=$urlRetorno?>');" title="Excluir"></a>
				</div>
			<?
			if($id == $l->getId()){
				?>
				<div class="bloco">
					<input type="hidden" name="id" id="id" value="<?=$sID?>"/>
					<div>Editar<a href="<?=$urlRetorno?>&idioma=<?=$idioma?>&idDestaque=<?=$idDestaque?>">X</a></div>
					<?=getHtmlFormCotacao($l->getId(),$categoria,$titulo,$periodo,$valor,$perc)?> 
				</div>

				<?
			}
			?>
			</div>
			<?
			$nReg++;
		}
    }
	}
?>
   <? if($acao == "novo"): ?>
    <div>
        <div class="bloco">
            <div>Novo<a id="form-novo" href="<?=$urlRetorno?>&idioma=<?=$idioma?>&idDestaque=<?=$idDestaque?>">X</a></div>              
            <?=getHtmlFormCotacao()?>    
        </div>
    </div>   
    <? endif; ?>
</blockquote>