<?

Usuario::autenticarLogon();
$user = Usuario::getUserLogado();
$secoes = Secao::listar("","id,id_secao,nome,tipo","tem_menu = ".ATIVO." AND status = ".ATIVO,"nome ASC");

$ordemSubMenu[EMPRESA] = array(TRAJETORIA,MENU_TIME_LINE,ACOES_SUSTENTAVEIS,MERCADO_DE_ATUACAO);

function getSubSecoes($idSecao,$idSec,$style){
	global $ordemSubMenu;
    $subSecoes = Secao::listar($idSecao,"id,id_secao,nome,tipo","tem_menu = ".ATIVO." AND status = ".ATIVO,"id ASC");
    $html = "";	
    if($subSecoes){
        /*$dsp = 'style="display:none"';
        if($style){
                $dsp = "";
        }*/
		$arrHtml = array();
        $html .= "<ul class=\"laranja\">\n";
        foreach($subSecoes as $l){
			$pref = "listagem_";
			$linkSecao = "";
			$nomeSecao = $l->getNome();
			$linkSecao = getUrlByTipo($l->getTipo(), $l->getId());
			if(!$linkSecao) $linkSecao = getUrl($pref,$l->getId());
			if($idSec == $l->getId()){
					$nomeSecao = "<b>$nomeSecao</b>";
			}	
			if(isset($ordemSubMenu[$idSecao])){
				$arrHtml[$l->getId()] =  "<li><a href=\"".$linkSecao."\" target=\"_self\">$nomeSecao</a></li>\n";
			}else{
				$html .=  "<li><a href=\"".$linkSecao."\" target=\"_self\">$nomeSecao</a></li>\n";			
			}	                
        }
		//print_r($ordemSubMenu[EMPRESA]);
		if(isset($ordemSubMenu[$idSecao]))
		foreach($ordemSubMenu[$idSecao] as $k=>$l){
			if(isset($arrHtml[$l])) $html .= $arrHtml[$l];
		}
        $html .=  "</ul>\n";
    }
    return $html;
}

if($secoes){	
    $arrPerm = $user->getPermissoes(0);
    foreach($secoes as $ls){
        $attrId = $class  = $linkSecao= '';
        $cont = 0;
        $stl = false;
        if($cont > 0){
            $cont = "($cont)"; 
        }else{
            $cont = "";
        }

        $nomeSecao = $ls->getNome()." ".$cont;
        $pref = "listagem_";
        $linkSecao = getUrlByTipo($ls->getTipo(), $ls->getId());    
        if(!$linkSecao) $linkSecao = getUrl($pref,$ls->getId());
        if(inArray($ls->getId(),$arrPerm)){	
            if($idSecao == $ls->getId()){
                //$nomeSecao = "<b>$nomeSecao</b>";
                $attrId = 'active';
                $stl = true;
            }
            $subs = getSubSecoes($ls->getId(),$idSecao,$stl);
            if($subs) $class = 'mn-abre';
            $arrMenuTemp[$ls->getId()] = "<li><a href=\"".$linkSecao."\" target=\"_self\" class='$attrId'>$nomeSecao</a>$subs</li>\n";
        }
    }
}
$strMenu = "";
for($i=0;$i<count($arrOrdemMenu);$i++){
	if(isset($arrOrdemMenu[$i]) && isset($arrMenuTemp[$arrOrdemMenu[$i]]))$strMenu .= $arrMenuTemp[$arrOrdemMenu[$i]];
}	


?>
<div class="menu-lateral">
  <div><a href="index.php"><img src="css/img/logo.png" width="174" /></a></div>
  <h3>MENU</h3>
  <ul>
      <li><a href="javascript:openPop('pop-alterar-senha.php')" class="bt-padrao" ><b>Alterar senha</b></a></li>
      <? /*<li><a href="javascript:openPop('pop-secao.php');"  class="bt-padrao"><b>Adicionar secao</b></a></li> */?>
  </ul>
  <ul class="preto">
	<?=$strMenu?>      
  </ul>
</div>