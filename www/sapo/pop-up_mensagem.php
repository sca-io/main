<? require_once("conf.php");
$semMoldura= request('semMoldura');
$acao = request('acao');
$msg = request('msg');
$idSecao = request('idSecao');
$idSubSecao = request('idSubSecao');
$id = request('id');
$urlRetorno = urldecode(request('urlRetorno'));
if(!$msg){
	switch($acao){
		case 'ERROR': $msg = $msg; break;
		case 'excluido': $msg = "Item excluído com sucesso"; break;
		case 'excluidoLixeira': $msg = "Item excluído com sucesso e enviado para a lixeira"; break;
		case 'deletado': $msg = "Item deletado com sucesso"; break;
		case 'restaurado': $msg = "Item restaurado com sucesso"; break;
		case 'restauradoTudo': $msg = "Itens restaurados com sucesso"; break;
		case 'excluidoTudo': $msg = "Itens deletados com sucesso"; break;
		default: $msg = "Dados ".$acao."s com sucesso"; break;
	}
}

?><!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>@import url(<?=DIRETORIO_RAIZ?>sapo/css/estrutura.css);</style>
<link href="css/pop-up.css" rel="stylesheet" type="text/css" >
</head>
<body>
<? if(!$semMoldura){?>
<div id="sapo">
	<div class="container">
		<? include "includes/topo.php";?>                    
        <? include "includes/menu-lateral.php";?>          
        <div class="coluna-geral">
<? }?>
                <div class="pop-sucesso">
                    <p><?=response_html($msg)?></p>
                    <a href="<?=response_attr($urlRetorno)?>" class="bt-padrao">Voltar</a>
                </div>
<? if(!$semMoldura){?>
        </div>
    </div>
</div>
<? }?>
</body>
</html>