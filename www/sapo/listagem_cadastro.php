<? require_once("conf.php");
Usuario::autenticarLogon();
$acao = request("acao");
$idSecao = request("idSecao");
$idSubSecao = request("idSubSecao");

$busca = request('busca');
$letra = request('letra');
$idioma = request('idioma');
$idioma = $idioma?$idioma:LNG_PT;
$status = request('status');
$dataIn = request('dataIn')?request('dataIn'):date("d/m/Y",mktime(0,0,0,date("m")-1,date("d"),date("Y")));
$dataOut = request('dataOut')?request('dataOut'):date("d/m/Y");
$di = explode("/",$dataIn);
$do = explode("/",$dataOut);
$tipo = request('tipo');

$urlRetorno = "listagem_cadastro.php?idSecao=$idSecao";

$secao = Secao::ler($idSecao);
$settings = null;
if($secao && $secao->getIdSecao()){
    $settings = getSettings($secao->getIdSecao(),$idSecao);
}else{
    $settings = getSettings($idSecao);
}

$url = "&status=$status";
/*if($idSecao != NEWSLETTER) $where = "idioma = $idioma";
else $where = "";*/
$where = "idioma = $idioma";

if($busca != ""){
    if($where != "")  $where .= " AND ";
    $where .= "nome like '%".strtr($busca,$ent)."%'";
    $url .= "&busca=$busca";
}
if($letra != ""){
    if($where != "")  $where .= " AND ";
    $where .= "nome like '$letra%'";
    $url .= "&letra=$letra";
}
if($status != ""){
    if($where != "")  $where .= " AND ";
    $where .= "status = $status";	
}else{
    if($where != "")  $where .= " AND ";
    $where .= "status != ".INATIVO;
}
if($dataIn != ""){
    if($where != "")  $where .= " AND ";
    $where .= "data_inclusao >= '".$di[2]."-".$di[1]."-".$di[0]." 00:00:00'";
    $url .= "&dataIn=$dataIn";
}
if($dataOut != ""){
    if($where != "")  $where .= " AND ";
    $where .= "data_inclusao <= '".$do[2]."-".$do[1]."-".$do[0]." ".date("H:i:s")."'";
    $url .= "&dataOut=$dataOut";
}
$ncadastro = 0;
if($idSecao == GESTAO_DE_PESSOAS){
    $ncadastro = CadGestaoPessoa::countListar($where);
}
if($idSecao == CONTATOS){
    $ncadastro = Contato::countListar($where);
}
if($idSecao == NEWSLETTER){
    $ncadastro = Newsletter::countListar($where);
}
if($idSecao == SEJA_NOSSO_CLIENTE){
     $ncadastro = Cliente::countListar($tipo,$where);
}

$pagina = isset($_REQUEST['pagina'])?$_REQUEST['pagina']:0;
$total_reg_pag = 15;
$init_reg_pag = ($pagina * $total_reg_pag);	 
$aux_pagina = $pagina + 1;
$total_paginas = $ncadastro/$total_reg_pag;

$by = (isset($_REQUEST['by'])) ? $_REQUEST['by'] : "data_inclusao";
$ordem = (isset($_REQUEST['ordem'])) ? $_REQUEST['ordem'] : "DESC";
$ordemArr = Array();


$ordemArr[$by] = $ordem;
$ordemArr['data_inclusao'] = (isset($ordemArr['data_inclusao']))? $ordemArr['data_inclusao'] : "DESC";
$ordemArr['nome'] = (isset($ordemArr['nome']))? $ordemArr['nome'] : "ASC";
$ordemArr['status'] = (isset($ordemArr['status']))? $ordemArr['status'] : "ASC";
$ordemArr['email'] = (isset($ordemArr['email']))? $ordemArr['email'] : "ASC";

if($ordemArr[$by] == "DESC") {
    $ordemArr[$by] = "ASC";
}else{
    $ordemArr[$by] = "DESC";
}

$paginacao = new Paginacao($pagina,$ncadastro,$total_reg_pag);
$paginacao->setUrl("&idSecao=$idSecao&idSubSecao=$idSubSecao".$url);

$lista = null;
if($idSecao == GESTAO_DE_PESSOAS){
    $lista = CadGestaoPessoa::listar("id,idioma,nome,email,data_inclusao,status",$where," $by $ordem","$init_reg_pag, $total_reg_pag");
}
if($idSecao == CONTATOS){
    $lista = Contato::listar("id,idioma,nome,email,data_inclusao,status",$where," $by $ordem","$init_reg_pag, $total_reg_pag");
}
if($idSecao == NEWSLETTER){
    $lista = Newsletter::listar("id,idioma,nome,email,data_inclusao,status",$where," $by $ordem","$init_reg_pag, $total_reg_pag");
}
if($idSecao == SEJA_NOSSO_CLIENTE){
    $lista = Cliente::listar($tipo,"id,idioma,nome,email,data_inclusao,status",$where," $by $ordem","$init_reg_pag, $total_reg_pag");
}
$listaGeral = $lstTitulo = $listacbk = $listaEmail ="";
//print Contato::getLogSql();
if($lista){
        /*<th class='checkbox'><input type='checkbox' id='todosCkb' onClick='selecionaTudo(this)'/></th>*/
	$lstTitulo = "            
            <th><a href='listagem_cadastro.php?idSecao=$idSecao&by=nome&ordem=".$ordemArr[$by]."$url'>&nbsp;Nome&nbsp;</a></th>
            <th><a href='listagem_cadastro.php?idSecao=$idSecao&by=email&ordem=".$ordemArr[$by]."$url'>&nbsp;E-mail&nbsp;</a></th>
            <th><a href='listagem_cadastro.php?idSecao=$idSecao&by=data_inclusao&ordem=".$ordemArr[$by]."$url'>&nbsp;Data&nbsp;</a></th>";
                
        if($idSecao != NEWSLETTER && $idSecao != SEJA_NOSSO_CLIENTE) $lstTitulo .= "<th><a href='listagem_cadastro.php?idSecao=$idSecao&by=status&ordem=".$ordemArr[$by]."$url'>&nbsp;Status&nbsp;</a></th>";
            
        if($idSecao != NEWSLETTER) $lstTitulo .= "<th class='acao'>Ação</th>";
	$listaGeral = "";
	foreach($lista as $l){	
            //$listacbk = "<td class='checkbox'><input type='checkbox' class='cbkEdicao' value='".secureResponse($l->getId())."' name='selecao".secureResponse($l->getId())."' /></td>";
            $listaTit = "<td>".$l->getNome()."</td>";
            $listaEmail = "<td>".$l->getEmail()."</td>";
            $listaData = "<td>".Util::dataDoBdcomHora($l->getDataInclusao())."</td>";
            $listaStatus = "<td>".$l->getStatus(true)."</td>";
            $listaAcao = "<td class='acao'><a href=\"javascript:deletar('".secureResponse($l->getId())."',".$idSecao.",'".urlencode($urlRetorno)."')\" class=\"bt-excluir\">fechar</a>
                <a href=\"visualizar_cadastro.php?id=".secureResponse($l->getId())."&idSecao=$idSecao\" class=\"bt-editar\">editar</a></td>";
            
            $linhaTable = "$listacbk $listaTit $listaEmail $listaData";
            if($idSecao != NEWSLETTER && $idSecao != SEJA_NOSSO_CLIENTE) $linhaTable .= "$listaStatus";
            if($idSecao != NEWSLETTER) $linhaTable .= "$listaAcao";
            $listaGeral .= "<tr id='eq_".secureResponse($l->getId())."'>$linhaTable</tr>";
	}
}

if($acao == "gerarExcel"){
   if($lista){
        $lstTitulo = "<tr>
            <th>Nome</th>
            <th>Email</th>
            <th>Data</th>
            <th>Idioma</th>
        </tr>";
        $listaGeral = "$lstTitulo";
        foreach($lista as $l){	                
                $listaNome = "<td>".$l->getNome()."</td>";
                $listaData = "<td>".Util::dataDoBdcomHora($l->getDataInclusao())."</td>";
                $listaEmail = "<td>".$l->getEmail()."</td>";
                $listaIdioma = "<td>".$vetIdioma[$l->getIdioma()]."</td>";                
                $listaGeral .= "<tr>$listaNome $listaEmail $listaData $listaIdioma</tr>";
        }
        Util::error_downLoadArquivoExcel("lista-cadastro-".date("d-m-Y-H-i-s").".xls", utf8_decode($listaGeral));
    }
}

if(isset($settings["abas_sapo"]) && $settings["abas_sapo"]) {
    $indexAbaEdicao = 2;
    $indexAba = request("indexAba")?request("indexAba"):1;
    $gpAbas = new AbaSuperior();
    $gpAbas->setIndexAba($indexAba);
    foreach($settings["abas_sapo"] as $txt=>$aba){
        $gpAbas->addItem($txt,"$aba?idSecao=$idSecao&idSubSecao=$idSubSecao");
    }    
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<? include "includes/head.php";?>
    <link rel="stylesheet" type="text/css" href="css/home.css"/>
    <script src="js/jquery-ui-1.7.1.custom.min.js" type="text/javascript"></script>
<script type="text/javascript">
function filtrar(){
    var frm = document.formulario;
    frm.acao.value = "";
    frm.submit();
}
function changeIdioma(){
   var frm = document.formulario;
   //frm.id.value = "";
   frm.submit()    
}
function gerarExcel(){
    var frm = document.formulario;
    frm.acao.value = "gerarExcel";
    frm.submit();
}
 </script>
</head>
<body>
	<div id="sapo">
		<div class="container">
		  <? include "includes/topo.php";?>                    
          <? include "includes/menu-lateral.php";?>
          
          <div class="coluna-geral">
            <? include "includes/topo_int.php";?>
            <div class="modulo-geral home3 lista">              
               <div class="dados">
                    <form class="interessados" id='formulario' name='formulario' method="get">
                        <input type="hidden" name="acao" id="acao" value=""/>
                        <input type="hidden" name="idSecao" id="idSecao" value="<?=$idSecao?>"/>
                        <p>
                            <strong>Idioma</strong><br />
                            <select id='idioma' name='idioma' onchange="changeIdioma()" style="width:150px">
                                <?	
                                    $std = Array("","","");
                                    $std[$idioma] = "selected";
                                ?>
                                <option value='<?=LNG_PT?>' <?=$std[LNG_PT]?>><?=$vetIdioma[LNG_PT]?></option>
                                <option value='<?=LNG_EN?>' <?=$std[LNG_EN]?>><?=$vetIdioma[LNG_EN]?></option>        
                            </select>
                        </p>
                        <p>Nome:<br /><input type="text"  name="busca" value="<?=$busca?>" class="grande"/></p><br /><br /><br />
                        <p>Data inicial:<br /><input type="text"  name="dataIn" value="<?=$dataIn?>"  readonly="readonly" onclick="showInputCalendar(this)"/></p>
                        <p>Data final:<br /><input type="text"  name="dataOut" value="<?=$dataOut?>"  readonly="readonly" onclick="showInputCalendar(this)"/></p>
                        <? if($idSecao != NEWSLETTER && $idSecao != SEJA_NOSSO_CLIENTE):?>
                        <p>Status:<br />
                        <select id='status' name='status'>
                            <option value=''>Todos</option>
                            <?	
                                $std = Array("","","");
                                $std[$status] = "selected";
                            ?>
                            <option value='<?=ATIVO?>' <?=$std[ATIVO]?>><?=$vetStatus[ATIVO]?></option>
                            <option value='<?=NAOAVALIADO?>' <?=$std[NAOAVALIADO]?>><?=$vetStatus[NAOAVALIADO]?></option>        
                        </select>
                        </p>
                        <? endif;?>
                        <? if($idSecao == SEJA_NOSSO_CLIENTE):?>
                        <p>Tipo:<br />
                        <select id='tipo' name='tipo'>
                            <option value=''>Todos</option>
                            <? $tiposCliente = TipoCliente::listar("id, nome","status = ".ATIVO." AND idioma = '$idioma'");?>
                            <? foreach($tiposCliente as $k=>$lt):?>
                            <? $strSelected = ($tipo == $lt->getId())?"selected":"";?>
                            <option value='<?=($lt->getId())?>' <?=$strSelected?>><?=$lt->getNome()?></option>
                            <? endforeach;?>                            
                        </select>                            
                        </p>
                        <? endif;?>                        
                        
                        
                        <div> 
                            <a href="javascript:filtrar();" class="bt-padrao">Filtrar</a> 
                            <a href="javascript:limpaform('formulario','idSecao');filtrar();" class="bt-padrao">Limpar Filtro</a> 
                            <a href="javascript:gerarExcel();" class="bt-padrao">Gerar Excel</a>
                        </div>
                    </form>	
                    </div>
                	<h3>Total de registros: <?=$ncadastro?></h3>
					<?
                    $alfabeto = new LinksAlfabeto("listagem_cadastro.php?idSecao=$idSecao&idSubSecao=$idSubSecao".$url);
                    $alfabeto->printHtml();
                    ?>              
                    <table class="tabela" cellspacing="0">
                        <thead>
                            <tr>
                                <?=$lstTitulo?>
                            </tr>
                        </thead>
                        <tbody>
                            <?=$listaGeral?>
                        </tbody>
                    </table>
                    
                    <div class="paginacao"><?=$paginacao->printHtml()?></div> 
				</div>
          </div>          
		</div>
    </div>
</body>
</html>