<?
include("conf.php");
header("Content-type: text/html; charset=UTF-8");

$controleAcesso->addWhiteListReferer(array(
"sapo/crtlEspelhamento.php",
"sapo/edicao_chamadas.php",
"sapo/edicao_conteudo.php",
"sapo/edicao_conteudo_com_subs.php",
"sapo/edicao_conteudo_inicial.php",
"sapo/edicao_home.php",
"sapo/edicao_sapo_controle_espelhamento.php",
"sapo/exclusao.php",
"sapo/frm_celog.php",
"sapo/frm_media_crop.php",
"sapo/frm_usuario.php",
"sapo/importacao-dados.php",
"sapo/index.php",
"sapo/listagem_arquivos.php",
"sapo/listagem_cadastro.php",
"sapo/listagem_chamadas.php",
"sapo/listagem_conteudo.php",
"sapo/listagem_ferramentas.php",
"sapo/listagem_historicos.php",
"sapo/listagem_lixeira.php",
"sapo/listagem_usuario.php",
"sapo/pop-alterar-senha.php",
"sapo/pop-cat-cotacao.php",
"sapo/pop-categoria.php",
"sapo/pop-celog.php",
"sapo/pop-media.php",
"sapo/pop-secao.php",
"sapo/pop-tags.php",
"sapo/pop-up_mensagem.php",
"sapo/popup_permissoes.php",    
"sapo/visualizar_cadastro.php"
));
$controleAcesso->autenticarReferer();
Usuario::autenticarLogon();

$idSecao = USUARIOS;
$acao = request('acao');	
$indexAba = request('indexAba');
$id = secureRequest('id');

function salvarPermissao($idUser,$idPai,$idPerm,$status){
	$obj = UsuarioPermissao::lerByPermissao($idUser,$idPerm,$idPai);
	if(!$obj) $obj = new UsuarioPermissao();		
	$obj->setIdUsuario($idUser);
	$obj->setIdPai($idPai);
	$obj->setIdPermissao($idPerm);
	$obj->setStatus($status);			
	$obj->salvar();
       // print UsuarioPermissao::getLogSql();
}

function salvarSubPermissao($idUser,$idSecao,$str,$status){
    $arr = explode(";",$str);	
    if($arr){
        foreach($arr as $ps){
            if($ps){
                salvarPermissao($idUser,$idSecao,$ps,$status);
            }
        }
    }
}

function salvarPermissoesUsuario($id){
	$permSecaoAtivas =  request('permissoes_secao_ativos');
	$permSecaoAtivas = explode(";",$permSecaoAtivas);
	$permSecaoInativas =  request('permissoes_secao_inativos');
	$permSecaoInativas = explode(";",$permSecaoInativas);

	if($permSecaoAtivas){
		foreach($permSecaoAtivas as $psa){
			if($psa){
				salvarPermissao($id,0,$psa,ATIVO);
				$permSubAtivas =  request("perm_sub_".$psa."_ativos");
				if($permSubAtivas){
					salvarSubPermissao($id,$psa,$permSubAtivas,ATIVO);							
				}
				$permSubInativas =  request("perm_sub_".$psa."_inativos");
				if($permSubInativas){
					salvarSubPermissao($id,$psa,$permSubInativas,INATIVO);							
				}
			}
		}
		foreach($permSecaoInativas as $psi){
			if($psi){
				salvarPermissao($id,0,$psi,INATIVO);
				$permSubAtivas =  request("perm_sub_".$psi."_ativos");
				if($permSubAtivas){
					salvarSubPermissao($id,$psi,$permSubAtivas,ATIVO);							
				}
				$permSubInativas =  request("perm_sub_".$psi."_inativos");
				if($permSubInativas){
					salvarSubPermissao($id,$psi,$permSubInativas,INATIVO);							
				}			
			}
		}		
	}
}	

if($acao == "salvar"){	
	$login = request('login');
	$email = request('email');	
	$tipoUsuario = request('tipoUsuario');
	$privilegios =  request('permissoes');
	$urlRetorno = request('urlRetorno');
	$nome = request('nome');
        $status = request('status');
        $status = $status?$status:ATIVO;
        $desbloqueioLogin = false;
	if($id){
            $user = Usuario::ler($id,"id,login,nome,email,id_tipo,status");
            $resposta = "alterado";
            $idAcaoHistorico = EDITADO;
            if($user->getStatus() == Usuario::BLOQUEADO && $status == Usuario::ATIVO){
                $desbloqueioLogin = true;
            }
	}else{
             $user = new Usuario();
             $resposta = "inserido";
             $senha = gs();
             $user->setSenha($senha);
             $idAcaoHistorico = INSERIDO;
	}
        
	$user->setLogin($login);
	$user->setNome($nome);
	$user->setEmail($email);
        $user->setStatus($status);
	$user->setIdTipo($tipoUsuario);
	if($user->salvar()){
            
        if(!$id){
            $mail = new PHPMailer();
            $mail->SetLanguage("br", "classes/PhpMailer/"); //Idioma
            $mail->From = "sapo@scetanol.com.br";    //nome de quem ta enviando, vai aparecer na coluna "De:"
            $mail->FromName = "SCA - Site";    //nome de quem ta enviando, vai aparecer na coluna "De:"	
            $mail->AddAddress($email); 
            $mail->IsHTML(true);
            $mail->CharSet = "UTF-8";
            $mail->Subject = "Cadastro no SAPO";
            
            $mail->SMTPDebug = false;
            $mail->IsSMTP();
            $mail->SMTPAuth = true;
            $mail->Port = 587;
            $mail->SMTPSecure = "tls";
            $mail->Host = "smtpauth.vcaremail.com.br";
            $mail->Username = "sapo@scetanol.com.br"; //Nome do usuario SMTP
            $mail->Password = "Sca@173";     //Senha do usuario SMTP	
            
            $link = "http://".$_SERVER['SERVER_NAME'].DIRETORIO_RAIZ."sapo/login.php";

            $mail->Body = "<html><head></head><body>Você foi cadastrado no Sistema Atômica de Publicação Online(SAPO)<br>$link<br>Login: ".$login."<br> Senha: ".$senha."</body></html>";
            if(!$mail->Send()){
                    print "<script>alert('Erro ao enviar email')</script>";
                    exit;
            }	
                /*print "<script>window.location='pre-inscricao.php?r=1'</script>";*/
        }       
        if($desbloqueioLogin){
            $controleAcesso->desBloquearLogin($user);
        }
        $user->inserirHistorico(getSession('idUsuarioSapo'),$ip,$idAcaoHistorico);
        $id = $user->getId();
        if(request('salvaPermissoes')){
            salvarPermissoesUsuario($id);
        }	
        print "<script>window.location='pop-up_mensagem.php?acao=salvo&urlRetorno=$urlRetorno';</script>";
		
	}else{
            if($user->getUsuarioExistente()){
                print "<script>alert('Usuário existente');</script>";
            }
	}

  
}
	

if($acao == "salvarPermissoes"){
	salvarPermissoesUsuario($id);
	/*print "<script>window.location='popup_permissoes.php?id=$id&msg=sucesso';</script>";*/
	print "<script>window.close();window.opener.location.reload();window.opener.alert('Permissões salvas com sucesso');</script>";
}		

if($acao == "alterarSenha"){
	$user = Usuario::ler(getSession('idUsuarioSapo'));
	if($user){
		$senha = request("senha");
		$confirmaSenha = request("confirmaSenha");
		$vld = Array(1,1);
		$cmp = Array($senha,$confirmaSenha);
		if(Util::validaFormulario($vld,$cmp)){	
			if($senha == $confirmaSenha){
				$user->setSenha($senha);				
				$user->alteraSenha();
				print "sucesso";
			}
		}
	}
}

if($acao == "verificarUsuario"){
	$user = Usuario::logar($login,$senha);
	if($user){				
		print "Usuário existente";
	}else{
		print "Sem usuario";
	}
}	

?>