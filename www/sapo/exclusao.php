<?
include("conf.php");

$controleAcesso->autenticarReferer();
Usuario::autenticarLogon();

$id = secureRequest('id');
$acao = request('acao');
$urlRetorno = request('urlRetorno');
$idSecaoTipo = request('idSecao');
$ids = request('ids'); 
$arrIds = secureArray('ids',',');

if($acao == 'enviarLixeiraSelecionados'){
	if($ids){		
		foreach($arrIds as $ai){
			if(strlen($ai)>0){
				$obj = getObj($ai,$idSecaoTipo);			
				if($obj){
					$obj->setStatus(INATIVO);
					$obj->salvar();					
					if(in_array($idSecaoTipo,$secoesInsereHistorico)){
						$idAcaoHistorico = ENVIADOLIXEIRA;
						$obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,$idAcaoHistorico);
					}
				}else{
					print "não há objetos";	
				}									
			}
		}
	}
	if(!$urlRetorno){
		$urlRetorno = getUrl('listagem_',$idSecaoTipo,$idSubSecao);	
	}else{
		$urlRetorno = urlencode($urlRetorno);
	}
	print "<script>window.location='pop-up_mensagem.php?acao=excluido&idSecao=$idSecaoTipo&urlRetorno=$urlRetorno';</script>";
}	 
	 
if($acao =='excluir'){
	if($idSecaoTipo == HISTORICOS){
		$obj = Historico::ler($id);
		$obj->setStatus(INATIVO);
		$obj->salvar();
	}else{
		$obj = getObj($id,$idSecaoTipo);
		$obj->excluir();
		if(in_array($idSecaoTipo,$secoesInsereHistorico)){
			$idAcaoHistorico = DELETADO;
			$obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,$idAcaoHistorico);
		}
	}

        $retorno = "listagem_lixeira.php?idSecaoSelecionada=$idSecaoTipo";
        print "<script>window.location='pop-up_mensagem.php?acao=deletado&idSecao=$idSecaoTipo&urlRetorno=$retorno';</script>";

}

if($acao =='excluirSelecionados'){	
	if($ids){
		foreach($arrIds as $ai){
			if($idSecaoTipo == HISTORICOS){
				$obj = Historico::ler($ai);
				$obj->setStatus(INATIVO);
				$obj->salvar();
			}else{
				$obj = getObj($ai,$idSecaoTipo);
				$obj->excluir();
				if(in_array($idSecaoTipo,$secoesInsereHistorico)){
					$idAcaoHistorico = DELETADO;
					$obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,$idAcaoHistorico);
				}
			}			
		}
	}
	$retorno = "listagem_lixeira.php?idSecaoSelecionada=$idSecaoTipo";
	print "<script>window.location='pop-up_mensagem.php?acao=excluidoTudo&idSecao=$idSecaoTipo&urlRetorno=$retorno';</script>";
}

if($acao =='restaurarSelecionados'){	
	if($ids){	
		if($arrIds){
			foreach($arrIds as $ai){
				$obj = getObj($ai,$idSecaoTipo);
				$obj->setStatus(ATIVO);
				$obj->salvar();		
				if(in_array($idSecaoTipo,$secoesInsereHistorico)){
					$idAcaoHistorico = RESTAURADO;
					$obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,$idAcaoHistorico);
				}
			}
		}
	}
	$retorno = "listagem_lixeira.php?idSecaoSelecionada=$idSecaoTipo";
	print "<script>window.location='pop-up_mensagem.php?acao=restauradoTudo&idSecao=$idSecaoTipo&urlRetorno=$retorno';</script>";
}

if($acao =='excluirTudo'){
	$where = "status = ".INATIVO;
	$listaExclusao = getListaBySecao($idSecaoTipo,$where);
	if($listaExclusao){
		foreach($listaExclusao as $le){
			if(in_array($idSecaoTipo,$secoesInsereHistorico)){
				$idAcaoHistorico = DELETADO;
				$le->inserirHistorico(getSession('idUsuarioSapo'),$ip,$idAcaoHistorico);
			}
			$le->excluir();
		}
	}	
	$retorno = "listagem_lixeira.php?idSecaoSelecionada=$idSecaoTipo";
	print "<script>window.location='pop-up_mensagem.php?acao=excluidoTudo&idSecao=$idSecaoTipo&urlRetorno=$retorno';</script>";    
}

if($acao =='restaurarTudo'){	
	$where = "status = ".INATIVO;
	$listaExclusao = getListaBySecao($idSecaoTipo,$where);
	if($listaExclusao){
		foreach($listaExclusao as $le){						
			$le->setStatus(ATIVO);
			$le->salvar();
		}
	}
	$retorno = "listagem_lixeira.php?idSecaoSelecionada=$idSecaoTipo";
	print "<script>window.location='pop-up_mensagem.php?acao=restauradoTudo&idSecao=$idSecaoTipo&urlRetorno=$retorno';</script>";
}

if($acao == 'enviarLixeira'){	
	$obj = getObj($id,$idSecaoTipo);
	$obj->setStatus(INATIVO);
	$obj->salvar();	
	if(in_array($idSecaoTipo,$secoesInsereHistorico)){
        $idAcaoHistorico = ENVIADOLIXEIRA;
        $obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,$idAcaoHistorico);
    }
    if(!$urlRetorno){
        $urlRetorno = getUrl('listagem_',$idSecaoTipo);	
    }else{
        $urlRetorno = urlencode($urlRetorno);
    }

    print "<script>window.location='pop-up_mensagem.php?acao=excluido&idSecao=$idSecaoTipo&urlRetorno=$urlRetorno';</script>";
}

if($acao == 'restaurar'){	 
	$obj = getObj($id,$idSecaoTipo);
	$obj->setStatus(ATIVO);
	$obj->salvar();
	if(in_array($idSecaoTipo,$secoesInsereHistorico)){
		$idAcaoHistorico = RESTAURADO;
		$obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,$idAcaoHistorico);
	}
	$retorno = "listagem_lixeira.php?idSecaoSelecionada=$idSecaoTipo";
	print "<script>window.location='pop-up_mensagem.php?acao=restaurado&idSecao=$idSecaoTipo&urlRetorno=$retorno';</script>";
}
?>