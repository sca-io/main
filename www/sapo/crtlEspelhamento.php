<? require_once("conf.php");

ini_set('memory_limit','512M');
ini_set('max_input_time','0');
set_time_limit(0);

$acao = request('acao');
$idSCE = secureRequest('idSCE');

$referer = $_SERVER['HTTP_REFERER'];

$mostrarJs = false;
$flg1 = (indexOf($referer,"edicao_sapo_controle_espelhamento.php")!=-1);
$flg2 = (indexOf($referer,"crtlEspelhamento.php")!=-1);
if($flg1 || $flg2){
    $mostrarJs = true;
}

if($idSCE!='' && $acao!=''){
    SapoControleEspelhamento::Controle_Verificar($mostrarJs, $acao, $idSCE);
}else{
    SapoControleEspelhamento::Controle_Espelhar($mostrarJs);
}
?>
