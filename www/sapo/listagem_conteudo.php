<? require_once("conf.php");
Usuario::autenticarLogon();

$idSecao = request("idSecao");
$idSecao = is_numeric($idSecao)?$idSecao:0;
$idCont = request("idCont");
$busca = request('busca');
$letra = request('letra');
$idioma = request('idioma');
$idioma = $idioma?$idioma:LNG_PT;
$status = request('status');
$dataIn = request('dataIn');
$dataOut = request('dataOut');
$di = explode("/",$dataIn);
$do = explode("/",$dataOut);

$secao = Secao::ler($idSecao);
$settings = null;
if($secao && $secao->getIdSecao()){
    $settings = getSettings($secao->getIdSecao(),$idSecao);
}else{
    $settings = getSettings($idSecao);
}


$url = "&idSecao=$idSecao&status=$status";
$urlRetorno = "listagem_conteudo.php?idSecao=$idSecao";
$where = "idioma = $idioma";
if($busca != ""){
    if($where != "")  $where .= " AND ";
    $where .= "titulo like '%".($busca)."%'";
    $url .= "&busca=$busca";
}
if($letra != ""){
    if($where != "")  $where .= " AND ";
    $where .= "titulo like '$letra%'";
    $url .= "&letra=$letra";
}
if($status != ""){
   if($where != "")  $where .= " AND ";     
   $where .= "status = $status";	
}else{
    if($where != "")  $where .= " AND ";
    $where .= "status != ".INATIVO;
}
if($dataIn != ""){
    if($where != "")  $where .= " AND ";
    $where .= "data_inclusao >= '".$di[2]."-".$di[1]."-".$di[0]." 00:00:00'";
    $url .= "&dataIn=$dataIn";
}
if($dataOut != ""){
    if($where != "")  $where .= " AND ";
    $where .= "data_inclusao <= '".$do[2]."-".$do[1]."-".$do[0]." ".date("H:i:s")."'";
    $url .= "&dataOut=$dataOut";
}
$ncadastro = VersaoConteudo::countListar($idSecao,$idCont,$where);
//print VersaoConteudo::getLogSql();
$pagina = isset($_REQUEST['pagina'])?$_REQUEST['pagina']:0;

$total_reg_pag = 15;
$init_reg_pag = ($pagina * $total_reg_pag);	 
$aux_pagina = $pagina + 1;
$total_paginas = $ncadastro/$total_reg_pag;

$by = (isset($_REQUEST['by'])) ? $_REQUEST['by'] : "data_inclusao";
$ordem = (isset($_REQUEST['ordem'])) ? $_REQUEST['ordem'] : "DESC";
$ordemArr = Array();
$ordemArr[$by] = $ordem;
$ordemArr['data_inclusao'] = (isset($ordemArr['data_inclusao']))? $ordemArr['data_inclusao'] : "DESC";
$ordemArr['titulo'] = (isset($ordemArr['titulo']))? $ordemArr['titulo'] : "DESC";
$ordemArr['status'] = (isset($ordemArr['status']))? $ordemArr['status'] : "DESC";

if($ordemArr[$by] == "DESC") {
    $ordemArr[$by] = "ASC";
}else{
    $ordemArr[$by] = "DESC";
}

$paginacao = new Paginacao($pagina,$ncadastro,$total_reg_pag);
$paginacao->setUrl($url);

$lista = VersaoConteudo::listar($idSecao,$idCont,"id,id_conteudo,titulo,data_inclusao,status",$where," $by $ordem","$init_reg_pag, $total_reg_pag");
//print NovosAssociados::getLogSql();
$listaGeral = $lstTitulo = $listacbk = "";
if($lista){
        /*<th class='checkbox'><input type='checkbox' id='todosCkb' onClick='selecionaTudo(this)'/></th>*/
	$lstTitulo = "            
            <th><a href='listagem_conteudo.php?&by=nome&ordem=".$ordemArr[$by]."$url'>&nbsp;Título&nbsp;</a></th>
            <th><a href='listagem_conteudo.php?&by=data_inclusao&ordem=".$ordemArr[$by]."$url'>&nbsp;Data&nbsp;</a></th>
            <th><a href='listagem_conteudo.php?&by=status&ordem=".$ordemArr[$by]."$url'>&nbsp;Status&nbsp;</a></th>
            <th class='acao'>Ação</th>";
	
	foreach($lista as $l){	
            //$listacbk = "<td class='checkbox'><input type='checkbox' class='cbkEdicao' value='".secureResponse($l->getId())."' name='selecao".secureResponse($l->getId())."' /></td>";
            $listaTit = "<td>".$l->getTitulo()."</td>";
            $listaData = "<td>".Util::dataDoBdcomHora($l->getDataInclusao())."</td>";
            $listaStatus = "<td>".$l->getStatus(true)."</td>";
            $listaAcao = "<td class='acao'><a href=\"javascript:deletar('".secureResponse($l->getId())."',".$idSecao.")\" class=\"bt-excluir\">fechar</a>
                <a href=\"edicao_conteudo.php?id=".secureResponse($l->getIdConteudo())."&idSecao=$idSecao&idioma=$idioma\" class=\"bt-editar\">editar</a></td>";
            $listaGeral .= "<tr id='eq_".secureResponse($l->getId())."'>$listacbk $listaTit $listaData $listaStatus $listaAcao</tr>";
	}
}

if(isset($settings["texto_inicial"]) && $settings["texto_inicial"]) {
    $indexAbaEdicao = 2;
    $indexAba = 1;
    $gpAbas = new AbaSuperior();
    $gpAbas->setIndexAba($indexAba);
    $gpAbas->addItem("Listagem","listagem_conteudo.php?idSecao=$idSecao&idioma=$idioma");
    $gpAbas->addItem("Texto Inicial","edicao_conteudo_inicial.php?idSecao=$idSecao&idioma=$idioma");
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<? include "includes/head.php";?>
    <link rel="stylesheet" type="text/css" href="css/home.css"/>
    <script src="js/jquery-ui-1.7.1.custom.min.js" type="text/javascript"></script>
<script type="text/javascript">
function filtrar(){
	var frm = document.formulario;
        frm.acao.value = "";
	frm.submit();
}

function changeIdioma(){
   var frm = document.formulario;
   //frm.id.value = "";
   frm.submit()    
}

 </script>
</head>
<body>
	<div id="sapo">
		<div class="container">
		  <? include "includes/topo.php";?>                    
          <? include "includes/menu-lateral.php";?>
          
          <div class="coluna-geral">
            <? include "includes/topo_int.php";?>
            <div class="modulo-geral home3 lista">              
               <div class="dados">
                    <form class="interessados" id='formulario' name='formulario' method="get">
                        <input type="hidden" name="urlRetorno" id="urlRetorno" value="<?=$urlRetorno?>"/>
                        <input type="hidden" name="idSecao" id="idSecao" value="<?=$idSecao?>"/>
                        <input type="hidden" name="acao" id="acao" value=""/>
						
                        <? /*<p class="radio-group">Português:<input type="radio" name="idioma" value="<?=LNG_PT?>" onclick="document.formulario.submit()" <?=($idioma==LNG_PT)?"checked":""?> />Inglês:<input type="radio" name="idioma" value="<?=LNG_EN?>" onclick="document.formulario.submit()" <?=($idioma==LNG_EN)?"checked":""?> /></p> */?>
                        <p>
                            <strong>Idioma</strong><br />
                            <select id='idioma' name='idioma' onchange="changeIdioma()" style="width:150px">
                                <?	
                                    $std = Array("","","");
                                    $std[$idioma] = "selected";
                                ?>
                                <option value='<?=LNG_PT?>' <?=$std[LNG_PT]?>><?=$vetIdioma[LNG_PT]?></option>
                                <option value='<?=LNG_EN?>' <?=$std[LNG_EN]?>><?=$vetIdioma[LNG_EN]?></option>        
                            </select>
                        </p>			
                        <p>Titulo:<br /><input type="text"  name="busca" value="<?=$busca?>" class="grande"/></p><br /><br /><br />  
                        <p>Data inicial:<br /><input type="text"  name="dataIn" value="<?=$dataIn?>"  readonly="readonly" onclick="showInputCalendar(this)"/></p>
                        <p>Data final:<br /><input type="text"  name="dataOut" value="<?=$dataOut?>"  readonly="readonly" onclick="showInputCalendar(this)"/></p>
                        <p>Status:<br />
                        <select id='status' name='status'>
                            <option value=''>Todos</option>
                            <?	$std = Array("","",""); $std[$status] = "selected";?>
                            <option value='<?=ATIVO?>' <?=$std[ATIVO]?>>Publicado</option>
                            <option value='<?=NAOAVALIADO?>' <?=$std[NAOAVALIADO]?>>Salvo</option>        
                        </select>
                        </p><br /><br /><br />
                        <div> <a href="javascript:filtrar();" class="bt-padrao">Filtrar</a> <a href="javascript:limpaform('formulario','urlRetorno,idSecao');filtrar();" class="bt-padrao">Limpar Filtro</a> </div>
                    
                    </form>	
                    </div>
                	<h3>Total de registros: <?=$ncadastro?></h3>
					<?
                    $alfabeto = new LinksAlfabeto("listagem_conteudo.php?idSecao=$idSecao&idSubSecao=$idSubSecao".$url);
                    $alfabeto->printHtml();
                    ?>              
                    <table class="tabela" cellspacing="0">
                        <thead>
                            <tr>
                                <?=$lstTitulo?>
                            </tr>
                        </thead>
                        <tbody>
                            <?=$listaGeral?>
                        </tbody>
                    </table>
                    
                    <div class="paginacao"><?=$paginacao->printHtml()?></div> 
				</div>
          </div>          
		</div>
    </div>
</body>
</html>