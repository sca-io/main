CKEDITOR.editorConfig = function( config )
{
    //i.toolbar_Full=[['Source','-','Save','NewPage','Preview','-','Templates'],['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print','SpellChecker','Scayt'],
    //['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],['Form','Checkbox','Radio','TextField','Textarea','Select','Button','ImageButton','HiddenField'],'/',
    //['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
    //['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],['Link','Unlink','Anchor'],['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],'/',
    //['Styles','Format','Font','FontSize'],['TextColor','BGColor'],['Maximize','ShowBlocks','-','About']];
    config.toolbar_Basic = [['RemoveFormat','Bold','Italic','Underline']];
    var bts_link = ['Link','Unlink'];
    var bt_styles = ['Styles'];
    var bt_image = ['Image'];
    var botoes = [['Maximize'],['Undo','Redo'],['Find'],['SelectAll'],['RemoveFormat','Bold','Italic','Underline']];
    if(!PHP_FUNCTION){
        PHP_FUNCTION = function(){}
    }
    if(CK_HAS_LINK){
        botoes.push(bts_link);
        config.toolbar_Basic.push(bts_link);
    }
    if(CK_HAS_IMAGE){
        botoes.push(bt_image);        
    }
    if(CK_HAS_STYLES){
        botoes.push(bt_styles);
        config.toolbar_Basic.push(bt_styles);
    }
    config.height = CK_H;
    config.width = CK_W;
    config.resize_enabled = true;    
    config.RemoveAttributes = 'class,style,lang,width,height,align,hspace,valign' ;
    config.toolbar_Sapo = botoes;

    config.toolbar = CK_TOOL_BAR;    
    config.enterMode = CK_ENTER_MODE;//2=<br>;1=<P>	
    if(CK_CSS) config["EditorAreaCSS"] = CK_CSS;
    config.ForcePasteAsPlainText = true ;
    config.pasteFromWordRemoveStyles = true;	
};

