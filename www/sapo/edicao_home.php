<? require_once("conf.php");
Usuario::autenticarLogon();
$acao = request("acao");
$id = secureRequest("id");
$idSecao = (int) request("idSecao");
$idioma = request("idioma")?request("idioma"):LNG_PT;
$nReg = 0;

$idDestaque = request("idDestaque")?request("idDestaque"):1;
$categoria = request("categoria");
 if($idDestaque != 4 && $idDestaque != 5) $categoria = 0;
$settings = null;
if(isset($settModulosDeHome[$idSecao]) && isset($settModulosDeHome[$idSecao][$idDestaque])){
    $settings = $settModulosDeHome[$idSecao][$idDestaque];
    $template = isset($settings["template"])?$settings["template"]:"";
    $max_len_texto = isset($settings["max_len_texto"])?$settings["max_len_texto"]:"";
    $campos = isset($settings["campos"])?$settings["campos"]:"";    
}else{
    $template = "";
    $max_len_texto = "";
    $campos = "";
}
$arr_campos = array();
if($campos) $arr_campos = explode(',',$campos);

$acao = request('acao');
$idSecaoCont = $idSecao;
$whereSecao = "status = ".Secao::ATIVO;
if($idSecao == HOME){
    $idSecaoCont = "";
}
$secoesComConteudo = Array(TRAJETORIA,ACOES_SUSTENTAVEIS,MERCADO_DE_ATUACAO,COMERCIALIZACAO,SCA_TRADING,INTELIGENCIA_DE_MERCADO,CERTIFICACOES,CERTIFICACOES,LEGISLACOES);

$subSecoes = Secao::listar($idSecaoCont,"id,id_secao,nome",$whereSecao , "nome ASC");
$htmlOptsSecoes = "";
foreach($subSecoes as $sub){
    //$nomeSecao = $sub->getNome();
    if(in_array($sub->getIdSecao(),$secoesComConteudo) || in_array($sub->getId(),$secoesComConteudo)){
        if($sub->getIdSecao()){
            $nomeSecao = getNomeSecao($sub->getIdSecao(),$sub->getId(), "", $idioma);
        }else{
            $nomeSecao = getNomeSecao($sub->getId(),"", "", $idioma); 
        }    

        $htmlOptsSecoes .= "<option value=\"".$sub->getId()."\">".$nomeSecao."</option>" ;
    }
   
}
//print Secao::getLogSql();
$idPreview = "";
switch($idSecao){
    case HOME:
            $idPreview = "bgHome";
    break;
    case EMPRESA:
            $idPreview = "bgHomeEmpresa";
    break;
    case SERVICOS:
            $idPreview = "bgHomeServicos";
    break;
    case ETANOL:
            $idPreview = "bgHomeEtanol";
    break;
}

$urlRetorno = "edicao_home.php?idSecao=$idSecao&idDestaque=$idDestaque&idioma=$idioma&categoria=$categoria";
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE" />
<head>
<? include "includes/head.php";?>
<link rel="stylesheet" type="text/css" href="css/home.css"/>

<? if ((strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 8') == true) || (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7') == true)):?>
    <script src="js/jquery-ui-1.7.1.custom.min.js" type="text/javascript"></script>
<? else:?>
    <script src="js/jquery-ui-1.8.17.custom.min.js" type="text/javascript"></script>
<? endif;?>
<script type="text/javascript">
CK_HAS_LINK = false;
CK_HAS_IMAGE = false;
CK_W = '280px';
CK_H = '130px';
CK_TOOL_BAR = "Basic";
CK_ENTER_MODE = 2;
</script>
<script type="text/javascript" src="Fckeditor/ckeditor.js"></script>
<script type="text/javascript" src="Fckeditor/config.js"></script>
<script type="text/javascript" src="js/jquery.priceFormat.js"></script>
<script src='js/UploadByFrame.js' type="text/javascript"></script>
<script type="text/javascript">
function salvarOrdenacao(array){
    <?
    $urlControle = "";
    if($template == "cotacoes"){
         $urlControle = "conteudo_controle.php?acao=salvarOrdenacaoCotacoes";      
    }
    if($template == "chamadas"){
         $urlControle = "conteudo_controle.php?acao=salvarOrdenacaoChamadas";      
    }
    if($template == "icones" || $template == "rotativo") {
        $urlControle = "media_controle.php?acao=salvarOrdenacao"; 
    }
    if($template == "unidades"){
         $urlControle = "conteudo_controle.php?acao=salvarOrdenacaoUnidadeProd";      
    }    
    ?>
    array = sapo_replace("ch[]=","",array)
    array = sapo_replace("&",";",array)
    $.post("<?=$urlControle?>&idSecao=<?=$idSecao?>&idioma=<?=$idioma?>&idDestaque=<?=$idDestaque?>&ids="+array,function(data){
       if(data=="ERRO"){
           alerta("Ocorreu um erro na ordenação!");
       }
    });/**/
}   
    
    
/*** Template Chamada ***/    
function salvarDadosChamada(){
    var frm = document.formulario;
    frm.target = "";
    frm.action = "conteudo_controle.php";
    frm.acao.value = "salvarChamada";        
    frm.submit();
    $("#btSalvar").html("aguarde...").attr("href","#");
    
}    
    
function salvar(){
    var frm = document.formulario;
    frm.target = "";
    if(frm.titulo){ msg['titulo'] = "Título"; vld['titulo'] = 1;}
    
    if($("#titulo").attr('type') != "text"){
        if(frm.cont_secao){
             $("#titulo").val($("#cont_secao option:selected").text());
        }else{
              $("#titulo").val($("#titulo option:selected").text()); 
        }        
                      
    }

    if(frm.texto.className == "ckeditor") frm.texto.value = getTextEditorParent("texto");
    msg['texto'] = "Texto"; vld['texto'] = 1;
   /* msg['cont_id'] = "Conteúdo"; vld['cont_id'] = 1;
    if(frm.cont_secao) {msg['cont_secao'] = "Seção"; vld['cont_secao'] = 1;}*/
    msg['idioma'] = "Idioma"; vld['idioma'] = 1;
    <? if($max_len_texto):?>
    if(frm.texto.value.length > <?=$max_len_texto?>) {
        //alert(frm.texto.value)
        alert("O texto ultrapassou o limite máximo de <?=$max_len_texto?> caracteres") ;return;      
    }      
    <? endif;?>        
    fSucess = function(url){
        if(frm.arquivo && frm.arquivo.value){
            subirImagem('arquivo',frm.tipoProp.value);
            onImgCrop = function(){
                var frm = document.formulario;
                frm.arquivo.value = '';
                statusImagem = '';
                salvarDadosChamada();
            }
        }else{
            salvarDadosChamada();
        }
    }
    fErro = function(strErro){
        var str = sapo_replace(",",";<br/>",strErro);
        alerta("<b>Por favor preencha os campos:</b><br/>"+str);
        enviando = 0;	
    }
    validaFormulario('formulario',fSucess,fErro);
}

/*** Template icones e rotativo ***/  
salvarDadosMedia = function(){
    var frm = document.formulario;
    var destino_frm = "media_controle.php";
    frm.target = "";
    frm.acao.value = "salvarImg";
    frm.action = destino_frm;    
    frm.submit();
}

function salvarMedia(){  
    var frm = document.formulario;    
    msg = Array();
    vld = Array();
    msg['titulo'] = "titulo"; vld['titulo'] = 1;
    <? if(!$id){?>msg['arquivo'] = "Arquivo"; vld['arquivo'] = 1;<? }?>
    fSucess = function(url){
        if(frm.arquivo.value){
                subirImagem('arquivo',frm.tipoProp.value)
        }else{
                salvarDadosMedia();
        }
    }
    fErro = function(strErro){
        var str = sapo_replace(",",";<br/>",strErro);
        alerta("<b>Por favor preencha os campos:</b><br/>"+str);
        enviando = 0;
    }
    validaFormulario('formulario',fSucess,fErro)
}

function recortarImagem(src,diretorio){
    openPop('pop-media.php?tp=2&parametros=<?=urlencode("&idDestaque=$idDestaque&idSecao=".$idSecao."&tipoProp=".$template."&urlRetorno=".urlencode($urlRetorno)."&callBack=onImgCrop&src=")?>'+src+'<?=urlencode("&diretorio=")?>'+diretorio);    
}

function onImgCrop(){
    var frm = document.formulario;
    frm.arquivo.value = '';
    statusImagem = '';
    salvarDadosMedia();
}

function subirImagem(arquivo,tipoProp){
    var frm = document.formulario;
    if(frm[arquivo].value){
         $("#ajaxLoad").css('opacity','0.7').fadeIn();
        frm.action = "media_controle.php?campo="+arquivo+"&tipoProp="+tipoProp;
        frm.acao.value = 'subirImagem';
        uploadMedia(frm,function(res){
            //alert(res);
            res = res.split("[;]");
             $("#ajaxLoad").fadeOut();
            if(res[0] == "sucesso"){
                recortarImagem(res[1],res[2]);
                
                <? if($template == "chamadas"): ?>
                    frm.urlMedia.value = res[2]+res[1];
                <? else:?>
                     frm.urlMedia.value = res[1];    
                     frm.dir.value = res[2];   
                <? endif;?> 
                    
                statusImagem = 'aguardando-crop';
            }else{
                if(!res[1]){
                    alerta("Ocorreu um erro ao subir a imagem, verifique se a mesma não ultrapassa o limite máximo do servidor que é de <?=(LIMITE_MAX_UPLOAD)?>KB");
                }else{
                    alerta(res[1]);
                }                
            }
        })
    }else{
        alerta("Selecione um arquivo antes")
    }
}

/*** script da template de unidades esta dentro do aquivo edicao_unidades_home.php ***/  

 $(window).ready(function(){    
<? if(isset($settings["ordenar"]) && $settings["ordenar"]):?>
     $(function() { $("#dd").sortable({ opacity: 0.6, cursor: 'move', update: function() { salvarOrdenacao($(this).sortable("serialize")); }}); });

<? endif; ?>
    
    $(".maskPriceFormat").priceFormat({	
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
    });
    
});
</script>
</head>
<body>
<div id="sapo">
    <div class="container">
      <? include "includes/topo.php";?>

      <? include "includes/menu-lateral.php";?>

      <div class="coluna-geral">
        <? include "includes/topo_int.php";?>
        <div class="modulo-geral">
            <div class="esq">
                <form action="" name="formulario" id="formulario" method="post" enctype='multipart/form-data'>
                    <input type="hidden" name="MAX_FILE_SIZE" value="<?=LIMITE_MAX_UPLOAD?>" />
                    <input type="hidden" name="idDestaque" id="idDestaque" value="<?=$idDestaque?>"/>                    
                    <input type="hidden" name="idSecao" id="idSecao" value="<?=$idSecao?>"/>
                    <input type="hidden" name="template" id="template" value="<?=$template?>"/>
                    <input type="hidden" name="acao" id="acao" value=""/>
                    <p><input type="radio" name="idioma" value="<?=LNG_PT?>" onclick="document.formulario.submit()" <?=($idioma==LNG_PT)?"checked":""?> />Português:</p>
                    <p><input type="radio" name="idioma" value="<?=LNG_EN?>" onclick="document.formulario.submit()" <?=($idioma==LNG_EN)?"checked":""?> />Inglês:</p>
                    <input type="hidden" name="tipoProp" id="tipoProp" value="<?=$template?>"/>
                                        
                    <? if($template == "unidades"):?>                        
                        <? include "includes/edicao_unidades_home.php";?>
                    <? endif;?>
                    <? if($template == "chamadas"):?>                        
                        <? include "includes/edicao_chamada_home.php";?>
                    <? endif;?>
                    <? if($template == "cotacoes"):?>                        
                        <? include "includes/edicao_cotacao_home.php";?>
                    <? endif;?>                    
                    <? if($template == "icones" || $template == "rotativo"):?>                       
                        <? include "includes/edicao_media_home.php";?>
                    <? endif;?>
                        
                    
                    <blockquote>
                        <? 
                        $btnInserirNovo = true;
                        if(isset($settModulosDeHome[$idSecao][$idDestaque]["limite_chamadas"])){
                                if($nReg < $settModulosDeHome[$idSecao][$idDestaque]["limite_chamadas"]){
                                    $btnInserirNovo = true;
                                }else{
                                    $btnInserirNovo = false;                                    
                                }
                        }
                        if(($idDestaque == 4 || $idDestaque == 5) && $idSecao == HOME) {
							$btnInserirNovo = false; 
							if($categoria){
								$nRegTot = Cotacao::countListar($idDestaque,"status = ".ATIVO." AND idioma = $idioma AND categoria = (SELECT id FROM tbl_categoria_cotacao WHERE id = tbl_cotacao.categoria LIMIT 1)");
								if($nRegTot < $settModulosDeHome[$idSecao][$idDestaque]["limite_chamadas"]){
									$btnInserirNovo = true;
								}
							}
						}
                        if($idSecao == UNIDADES_PRODUTORAS && !$estado)  $btnInserirNovo = false; 
                        ?>   
                        <? if($btnInserirNovo):?>          
                        <a href="<?=$urlRetorno?>&acao=novo#form-novo" class="bt-padrao" title="Inserir novo destaque">Inserir novo destaque</a>
                        <? endif;?>  
     
                    </blockquote>   
                    <input type="hidden" name="urlRetorno" id="urlRetorno" value="<?=$urlRetorno?>"/>
                </form>
            </div>
            <? if($idPreview):?>
            <div class="preview-do-site" id="<?=$idPreview?>">     
                <? /*<? if(isset($settModulosDeHome[$idSecao][1])):?><a href="<?=$urlRetorno?>&idioma=<?=$idioma?>&idDestaque=1" title="Editar destaque 1" class="dst1 <?=($idDestaque==1)?"active":""?>">Destaque 1</a><? endif;?>
                <? if(isset($settModulosDeHome[$idSecao][2])):?><a href="<?=$urlRetorno?>&idioma=<?=$idioma?>&idDestaque=2" title="Editar destaque 2" class="dst2 <?=($idDestaque==2)?"active":""?>">Destaque 2</a><? endif;?> */?>
                <? if(isset($settModulosDeHome[$idSecao])):?>
                    <? foreach($settModulosDeHome[$idSecao] as $k=>$set):?>
                        <? $urlAtual = "edicao_home.php?idSecao=$idSecao"; ?>
                        <a href="<?=$urlAtual?>&idioma=<?=$idioma?>&idDestaque=<?=$k?>" title="Editar destaque <?=$k?>" class="dst<?=$k?> <?=($idDestaque==$k)?"active":""?>">Destaque <?=$k?></a>
                    <? endforeach;?>                
                <? endif;?>
            </div>
            <? endif;?>
        </div>
    </div>
</div>
</div>
</body>
</html>