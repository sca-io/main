<? require_once("conf.php");
Usuario::autenticarLogon();


$busca = request('busca');
$acao = request('acao');
$id = secureRequest('id');
$idSecao = request("idSecao");
$idConteudo = secureRequest("idConteudo");
$idioma = request("idioma");
$tipoProp = request("tipoProp");
$tipo = request("tipo");
$tipo = $tipo?$tipo:Media::IMAGEM;
//print_r($_REQUEST);
$modoVis = request("modoVis");
//$modoVis = $modoVis?$modoVis:Media::PRIVADO;

if($id){
    $objI = Media::ler($id);
    if($objI){
        $titulo = $objI->getNome();
        $credito = $objI->getCredito();
        $legenda = $objI->getLegenda();
        $targetLink = $objI->getTargetLink();
        $urlLink = $objI->getUrlLink();
        $descricao = $objI->getDescricao();
        $idAssunto = $objI->getIdCategoria();
        $dir = $objI->getDir();
        $urlMedia = $objI->getUrl();
    }
}else{
    $titulo = "";
    $credito = "";
    $legenda = "";
    $targetLink = "";
    $urlLink = "";
    $descricao = "";
    $idAssunto = "";
    $urlMedia = "";
    $dir = "";
}

$secao = Secao::ler($idSecao);
$settings = null;
if($secao->getIdSecao()){
    $secaoPai = Secao::ler($secao->getIdSecao());
    if($secaoPai->getIdSecao()){
        $settings = getSettings($secaoPai->getIdSecao(),$secao->getIdSecao(),$idSecao);     
    }else{
        $settings = getSettings($secao->getIdSecao(),$idSecao);
    }
    
}else{
    $settings = getSettings($idSecao);
}

$camposForm = array("titulo"=>false,"credito"=>false,"legenda"=>false,"urlLink"=>false,"targetLink"=>false,"assunto"=>false,"descricao"=>false);
if($tipo == Media::IMAGEM){
    $camposForm['titulo'] = true;
    if($idSecao == ACOES_SUSTENTAVEIS){
        $camposForm['urlLink'] = true;
        $camposForm['targetLink'] = true;
    }else{
        //$camposForm['credito'] = true;
        if($idSecao == NOTICIAS) $camposForm['legenda'] = true;
    }
}
if($tipo == Media::DOCS){
    $camposForm['titulo'] = true;
    $camposForm['descricao'] = true;
    if(isset($settings["filtro_por_assunto"]) && $settings["filtro_por_assunto"] == true){
        $camposForm['assunto'] = true;
    }  
    if($idSecao == MERCADO_EXTERNO || $idSecao == MERCADO_INTERNO){
		$camposForm['assunto'] = true;
        $camposForm['descricao'] = false;
    }
}

$htmlOptsAs = "";
if(isset($camposForm['assunto']) && $camposForm['assunto']){
    $listaAs = Assunto::listar($idSecao,"id,nome,idioma", "status = ".ATIVO." AND idioma = $idioma", "data DESC");  
    
    foreach($listaAs as $as){
        $nomeAs = $as->getNome();
        $selectedAs = $idAssunto == $as->getId()?" selected=\"selected\"":"";
        $htmlOptsAs .= "<option value=\"".$as->getId()."\" $selectedAs>".$nomeAs."</option>" ;

    }
}
//$urlRetorno = "edicao-media.php?".$_SERVER['QUERY_STRING'];
$urlRetorno = "edicao-media.php?idSecao=$idSecao&idConteudo=".secureResponse($idConteudo)."&idioma=$idioma&tipo=$tipo&tipoProp=$tipoProp&modoVis=$modoVis";

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><? include "includes/head.php";?>
    <meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE" />
<link rel="stylesheet" type="text/css" href="css/pop-up.css"/>
<style type="text/css">
.pop-galeria-de-imagens .blk .body{ display: none;}
</style>
<script type="text/javascript" src="js/UploadByFrame.js"></script>
<script type="text/javascript">
function subirImagem(arquivo,tipoProp){
    var frm = document.formulario;
    if(frm[arquivo].value){
         $("#ajaxLoad").css('opacity','0.7').fadeIn();
        frm.action = "media_controle.php?campo="+arquivo+"&tipoProp="+tipoProp;
        frm.acao.value = 'subirImagem';
        uploadMedia(frm,function(res){
            //alerta(res);
            res = res.split("[;]");
             $("#ajaxLoad").fadeOut();
            if(res[0] == "sucesso"){                
                frm.urlMedia.value = res[1];    
                frm.dir.value = res[2];                    
                statusImagem = 'aguardando-crop';
                frm[arquivo].value = "";
                salvarDadosMedia();
            }else{
                if(!res[1]){
                    alerta("Ocorreu um erro ao subir o arquivo, verifique se o mesma não ultrapassa o limite máximo do servidor que é de <?=(LIMITE_MAX_UPLOAD)?>KB");
                }else{
                    alerta(res[1]);
                }                
            }
        })
    }else{
        alerta("Selecione um arquivo antes")
    }
}
var destino_frm = "media_controle.php";
salvarDadosMedia = function(){
    var frm = document.formulario;
    var destino_frm = "media_controle.php";
    frm.target = "";
    frm.acao.value = "salvarImg";
    frm.action = destino_frm;    
    frm.submit();
    if(frm.arquivo.value) alerta("Aguarde o carregamento do arquivo!");
}

function envia(){
    var frm = document.formulario;
    msg = Array();
    vld = Array();
    msg['titulo'] = "titulo"; vld['titulo'] = 1;
    <? if(!$id){?>msg['arquivo'] = "Arquivo"; vld['arquivo'] = 1;<? }?>
    if(frm.assunto){ msg['assunto'] = "Assunto"; vld['assunto'] = 1;}
    if(frm.descricao){ msg['descricao'] = "Descricão"; vld['descricao'] = 1;}
    fSucess = function(url){
        if(frm.arquivo.value){
            subirImagem('arquivo',frm.tipoProp.value)
        }else{
            salvarDadosMedia();
        } 
    }
    fErro = function(strErro){
        var str = sapo_replace(",",";<br/>",strErro);
        alerta("<b>Por favor preencha os campos:</b><br/>"+str);
        enviando = 0;
    }
    validaFormulario('formulario',fSucess,fErro)
}

$(window).ready(function(){
    var iHeight = $(".pop-galeria-de-imagens").height();
    var iWidth = $(".pop-galeria-de-imagens").width();
    if(parseInt(iHeight) < 600){
        parent.$("#popMedia").attr("height",iHeight);
    }else{
        parent.$("#popMedia").attr("height",600);
    }
    if(parseInt(iWidth) > 597){
        parent.$("#popMedia").attr("width",iWidth + 25);
    }else{
        parent.$("#popMedia").attr("width",597);
    }
});
</script>
</head>
<body>
    <div class="pop-galeria-de-imagens">
    	<h3>Galeria<a onClick="parent.popup.close();" class="bt-fechar" title="Fechar">X</a></h3>
        <form name="formulario" id="formulario" action="" method="post" target="popMedia" enctype='multipart/form-data'>
            <input type="hidden" name="MAX_FILE_SIZE" value="<?=LIMITE_MAX_UPLOAD?>" />
            <input type="hidden" name="acao" value="" />
            <input type="hidden" name="id" value="<?=secureResponse($id)?>" />
            <input type="hidden" name="idConteudo" value="<?=secureResponse($idConteudo)?>" />
            <input type="hidden" name="idSecao" value="<?=($idSecao)?>" />
            <input type="hidden" name="idioma" value="<?=($idioma)?>" />
            <input type="hidden" name="tipo" value="<?=($tipo)?>" />
            <input type="hidden" name="tipoProp" value="<?=($tipoProp)?>" />
            <input type="hidden" name="modoVis" value="<?=($modoVis)?>" />
            <input type="hidden" name="urlRetorno" value="<?=($urlRetorno)?>" />
            <input type="hidden" name="dir" value="<?=($dir)?>" />
            <input type="hidden" name="urlMedia" value="<?=($urlMedia)?>" />
            <div class="inserir">
                <h4><? if($id){?>Editar esta imagem<? }else{?>Inserir novo<? }?></h4>
                
                <? if($camposForm['assunto']):?>
                <p><strong>Assunto:</strong> <select name="assunto" id="assunto">
                    <option value="">Selecione</option>
                    <?=$htmlOptsAs?>
                </select>
                <a href="javascript:openPop('pop-categoria.php?idSecao=<?=$idSecao?>&idioma=<?=$idioma?>')" class="bt-padrao">Inserir nova</a>
                </p>
                <? endif;?>
                

               <? if($camposForm['titulo']):?>
               <p><strong>Titulo: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong><input type="text" name="titulo" id="titulo" value="<?=$titulo?>" /></p>
               <? endif;?>

               <? if($camposForm['credito']):?>
               <p><strong>Crédito: &nbsp;&nbsp;</strong><input type="text" name="credito" id="credito" value="<?=$credito?>" /></p>
               <? endif;?>
                                          
                


                <? if($camposForm['urlLink']):?>
                <p><strong>Link: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong><input type="text" name="urlLink" id="urlLink" value="<?=$urlLink?>" /></p>
                <? endif;?>
                
                <? if($camposForm['targetLink']):?>
                <p>
                <strong>Abrir link em:</strong><select name="targetLink" id="targetLink">
                <? $arrSeleced = array("_blank" => "","_self" => "") ; if(isset($arrSeleced[$targetLink])) $arrSeleced[$targetLink] = " selected=\"selected\""; ?>        
                    <option value="_self"<?=$arrSeleced["_self"]?>>Mesma página</option>
                    <option value="_blank"<?=$arrSeleced["_blank"]?>>Nova aba</option>                    
                </select>
                </p>
                <? endif;?>

 
                <? if($camposForm['legenda']):?>
                    <p><strong>Legenda: &nbsp;</strong><input type="text" name="legenda" id="legenda" value="<?=$legenda?>" /></p> 
                <? endif;?>
                    
                <? if($camposForm['descricao']):?>
                    <p><strong>Descrição: &nbsp;</strong><br /><textarea name="descricao" id="descricao" ><?=$descricao?></textarea></p>  
                <? endif;?>
                
                <p><strong>Arquivo:&nbsp;&nbsp;&nbsp;</strong><input type="file" name="arquivo" id="arquivo" /></p>
                <a href="javascript:envia();" class="bt-padrao"><? if($id){?>SALVAR<? }else{?>INSERIR<? }?></a>       
            </div>
            <?
            
            
$url = "&idSecao=$idSecao&idConteudo=".secureResponse($idConteudo)."&idioma=$idioma&tipo=$tipo&tipoProp=$tipoProp&modoVis=$modoVis";           
$where = "status = ".ATIVO." AND tipo = '$tipo'";
if($busca != ""){
    if($where != ""){
        $where .= " AND ";
    }
    $where .= "nome like '%$busca%'";
    $url .= "&busca=$busca";
}
if($idioma) $where .= " AND lang = ".$idioma;    
if($modoVis) $where .= " AND modo_vis = ".$modoVis;    
/*** Permite que todos os conteudos tenham acesso as mesmas imagens ***/
$parmIdConteudo = $idConteudo; 
$parmIdSecao = $idSecao;
if($modoVis == Media::PUBLICO){
    $parmIdConteudo = ""; 
    $parmIdSecao = "";
}
/************/
$ncadastro =  Media::countListar($parmIdConteudo,$parmIdSecao,$where);
//print Media::getLogSql();
$pagina = isset($_REQUEST['pagina'])?$_REQUEST['pagina']:0;
$total_reg_pag = 5;
$init_reg_pag = ($pagina * $total_reg_pag);	 
$aux_pagina = $pagina + 1;
$total_paginas = $ncadastro/$total_reg_pag;

$by = (isset($_REQUEST['by'])) ? $_REQUEST['by'] : "data_insercao";
$ordem = (isset($_REQUEST['ordem'])) ? $_REQUEST['ordem'] : "DESC";
$ordemArr = Array();


$ordemArr[$by] = $ordem;
$ordemArr['data_insercao'] = (isset($ordemArr['data_insercao']))? $ordemArr['data_insercao'] : "ASC";
if($ordemArr[$by] == "DESC") {
    $ordemArr[$by] = "ASC";
}else{
    $ordemArr[$by] = "DESC";
}

$paginacao = new Paginacao($pagina,$ncadastro,$total_reg_pag);
$paginacao->setUrl($url);




$listaImagens = Media::listar($parmIdConteudo,$parmIdSecao,"",$where," $by $ordem","$init_reg_pag, $total_reg_pag");
//print Media::getLogSql();
if($listaImagens){
    ?>
    <p><strong>Registros:</strong><em><?=$ncadastro?></em><strong>Buscar:</strong><input type="text" name="busca" class="medio1" id="busca" value="<?=$busca?>" /><a href="javascript:atualizaFormulario()" class="bt-padrao">OK</a></p>        
    <?
    foreach($listaImagens as $li){
        $sID = secureResponse($li->getId());
        $icone = getIconeArquivo($li->getUrl());
        $extFile = strtoupper(getExtArquivo($li->getUrl()));
        $isImgFile = false;
        $isImgFile = $extFile == "JPG" || $extFile == "GIF" || $extFile == "PNG";
            ?>
            <div class="blk" id="blk_<?=$sID?>">
                <p class="head">
                    <? if(!$isImgFile && $icone):?>
                        <?=$icone?>
                    <? endif;?>
                    <? if($isImgFile && !$icone):?>
                        <img src="<?=DIRETORIO_RAIZ.$li->getDir()?><?=$li->getUrl()?>" width="50" />
                    <? endif;?>
                    <strong>Título:</strong><em><?=$li->getNome()?></em><a class="bt-padrao" id="btM_1" href="javascript:slideGal('<?=$sID?>','m')">Mostrar</a>
                </p>
                <div class="body">
                    <p><strong>Título:</strong><em><?=$li->getNome()?></em><a class="bt-padrao" id="btE_1" href="javascript:slideGal('<?=$sID?>','e')">Esconder</a></p>
                    <div>
                        <? if(!$isImgFile && $icone):?>
                            <?=$icone?>
                        <? endif;?>
                        <? if($isImgFile && !$icone):?>
                            <img src="<?=DIRETORIO_RAIZ.$li->getDir()?>/<?=$li->getUrl()?>" id="imgIns_<?=$sID?>" width="280" />
                        <? endif;?>
                        <a href="javascript:excluirMedia('<?=$sID?>','<?=$urlRetorno?>');" class="bt-padrao">Excluir</a>
                        <a href="javascript:editarMedia('<?=$sID?>');" class="bt-padrao">Editar</a>
                        <? if($li->getTipo() == Media::IMAGEM && $tipoProp != "icones"):?>
                            <? $funcInserirJs = ($idSecao == NOTICIAS)? "inserirFCKComAling":"inserirFCK"?>
                        <a href="javascript:<?=$funcInserirJs."('".$sID."','".$li->getNome()."','".$li->getLegenda()."')"?>;" class="bt-padrao">Inserir</a>
                        <? endif;?>                       
                    </div>
                    <blockquote>
                        <? if($li->getIdCategoria()){
                            $cat = Assunto::ler($li->getIdCategoria())  ;
                            $nomeAs = $cat->getNome();
                        ?><p><b>Assunto:</b> <?=$nomeAs?> </p><? }?>
                        <? if($li->getLegenda()){?><p><b>Legenda:</b> <?=$li->getLegenda()?> </p><? }?>
                        <? if($li->getCredito()){?><p><b>Crédito:</b> <?=$li->getCredito()?> </p><? }?>
                        <? if($li->getDescricao()){?><p><b>Descrição:</b> <?=$li->getDescricao()?> </p><? }?>
                        <? if($li->getDataInsercao()){?><p><b>Data:</b> <?=Util::dataDoBDcomHora($li->getDataInsercao())?> </p><? }?>
                    </blockquote>
                </div>
            </div>
            <?
    }
}
            ?>
        </form>
        <div class="paginacao"><?=$paginacao->printHtml()?></div>         
    </div>     
</body>
</html>