<? require_once("conf.php");
Usuario::autenticarLogon();

$acao = request("acao");
$id = secureRequest("id");
$versao = secureRequest("versao");
$idSecao = request("idSecao");
$idSubSecao = request("idSubSecao");
$idDestaque = request("idDestaque");
$idioma = request("idioma");

$urlRetorno = "edicao_conteudo_inicial.php?idSecao=$idSecao&id=$id&idioma=$idioma";
$indexAbaEdicao = 1;

$secao = Secao::ler($idSecao);
$settings = null;
if($secao->getIdSecao()){
    $settings = getSettings($secao->getIdSecao(),$idSecao);
}else{
    $settings = getSettings($idSecao);
}


$idioma = request("idioma")?request("idioma"):LNG_PT;
$where = "idioma = $idioma";

$obj = null;
if($acao != "novo"){
    $where = "status != ".INATIVO." AND $where";  
    $camposTable = "id,titulo,idioma,texto,url_media,legenda_media,status";
    $listaConts = Chamada::listar($idSecao,$idDestaque,$camposTable,$where,"","1");
    //print VersaoConteudo::getLogSql();
    if($listaConts){
        $obj = $listaConts[0];
    }
}

if($obj){  
	$id = $obj->getId();
    $titulo = $obj->getTitulo();
   /* $descricao = $obj->getDescricao();
    $keywords = $obj->getKeywords();*/
    $texto = $obj->getTexto();
    $idioma = $obj->getIdioma();
    $status = $obj->getStatus();
}else{
    $titulo = "";
    $descricao = "";
    $keywords = "";
    $texto = "";    
    $urlEmbed = "";
    $status = ""; 
}

$urlCancelar = $_SERVER["HTTP_REFERER"];
$matchStr = explode("/",$urlCancelar);
if(indexOf($matchStr[count($matchStr)-1],"pop-up_mensagem.php")!=-1){
    $urlCancelar = "listagem_conteudo.php";
}

//if($secao->getTipo() == Secao::CADASTRO){
if(isset($settings["texto_inicial"]) && $settings["texto_inicial"]) {
    $indexAbaEdicao = 2;
    $indexAba = request("indexAba");
    $indexAba = $indexAba?$indexAba:2;
    $gpAbas = new AbaSuperior();
    $gpAbas->setIndexAba($indexAba);
    //$gpAbas->addItem("Listagem","listagem_cadastro.php?idSecao=$idSecao");
    $gpAbas->addItem("Listagem",getUrlByTipo($secao->getTipo(),$idSecao)."&idioma=$idioma");
    $gpAbas->addItem("Texto Inicial","edicao_conteudo_inicial.php?idSecao=$idSecao&idSubSecao=$idSubSecao&idioma=$idioma");
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? include "includes/head.php";?>
<link rel="stylesheet" type="text/css" href="css/edicao-conteudo.css"/>
<script type="text/javascript">
<? /*PHP_FUNCTION = function(){
        openPop('pop-media.php?parametros=<?=urlencode("&urlRetorno=".urlencode($urlRetorno))?>');
} */?>
PHP_FUNCTION = function(){
    openPop('pop-media.php?parametros=<?=urlencode("&modoVis=".Media::PUBLICO."&idSecao=$idSecao&urlRetorno=".urlencode($urlRetorno))?>');
}

CK_HAS_LINK = true;
CK_HAS_IMAGE = true;
CK_W = '640px';
CK_H = '300px';
</script>
<script type="text/javascript" src="Fckeditor/ckeditor.js"></script>
<script type="text/javascript" src="Fckeditor/config.js"></script>
<script type="text/javascript">
function salvar(){
    var frm = document.formulario;
    msg = [];
    vld = [];

    /*msg['titulo'] = "Título"; vld['titulo'] = 1;	
    msg['descricao'] = "Descrição"; vld['descricao'] = 1;
    msg['keywords'] = "Keywords"; vld['keywords'] = 1;*/
    frm.texto.value = getTextEditorParent("texto");
    msg['idioma'] = "Idioma"; vld['idioma'] = 1;
    msg['texto'] = "texto"; vld['Texto'] = 1;
    msg['status'] = "Publicar"; vld['status'] = 1;       
    
    fSucess = function(url){
        frm.action = "conteudo_controle.php";
        frm.acao.value = "salvarChamada";        
        frm.submit();
        $("#btSalvar").html("aguarde...").attr("href","#");
    }
    fErro = function(strErro){
        var str = sapo_replace(",",";<br/>",strErro);
        alerta("<b>Por favor preencha os campos:</b><br/>"+str);
        enviando = 0;	
    }
    validaFormulario('formulario',fSucess,fErro);
}

function changeIdioma(){
   var frm = document.formulario;
   //frm.id.value = "";
   frm.submit()    
}


</script>
</head>
<body>
	<div id="sapo">
		<div class="container">
		  <? include "includes/topo.php";?>          
          <? include "includes/menu-lateral.php";?>          
          <div class="coluna-geral">
          	<? include "includes/topo_int.php";?>
            <div class="modulo-geral">
            	<form name="formulario" id="formulario" method="post" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="<?=secureResponse($id)?>" />
                    <input type="hidden" id="acao" name="acao" value="" />
                    <input type="hidden" id="idSecao" name="idSecao" value="<?=$idSecao?>" />
                    <input type="hidden" id="urlRetorno" name="urlRetorno" value="<?=$urlRetorno?>" />
                    <input type="hidden" id="categorias" name="categorias" value="" />
                    <input type="hidden" id="versao" name="versao" value="<?=secureResponse($versao)?>" />
                  <div class="esq">
                      <p>
                      	<strong>Idioma</strong>
                        <select id='idioma' name='idioma' onchange="changeIdioma()">
                            <option value=''>Selecione</option>
                            <?	
                                $std = Array("","","");
				$std[$idioma] = "selected";
                            ?>
                            <option value='<?=LNG_PT?>' <?=$std[LNG_PT]?>><?=$vetIdioma[LNG_PT]?></option>
                            <option value='<?=LNG_EN?>' <?=$std[LNG_EN]?>><?=$vetIdioma[LNG_EN]?></option>        
                        </select>
                      </p>                     
                      <? /*<p><strong>Título</strong><textarea class="inputText" id="titulo" name="titulo" ><?=$titulo?></textarea></p>
                      <p><strong>Descrição</strong><textarea class="inputText" id="descricao" name="descricao" ><?=$descricao?></textarea></p>
                      <p><strong>Keywords</strong><textarea class="inputText" id="keywords" name="keywords" ><?=$keywords?></textarea></p> */?>
                      <p><strong>Texto</strong><textarea class="ckeditor" id="texto" name="texto" ><?=stripslashes($texto)?></textarea></p>                      
                      <p>
                      	<strong>Publicar</strong>
                        <select id='status' name='status'>
                            <option value=''>Selecione</option>
                            <?	$std = Array("","","");$std[$status] = "selected";?>
                            <option value='<?=VersaoConteudo::PUBLICADO?>' <?=$std[VersaoConteudo::PUBLICADO]?>>Sim</option>
                            <option value='<?=VersaoConteudo::SALVO?>' <?=$std[VersaoConteudo::SALVO]?>>Não</option>        
                        </select>
                      </p>
                      <a href="<?=$urlCancelar?>" class="bt-padrao" title="Cancelar">Cancelar</a>
                      <a href="javascript:salvar();" class="bt-padrao" title="Salvar" id="btSalvar">Salvar</a>
                  </div>
                </form>

            </div>
          </div>          
        </div>
    </div>
  </div>
</div>
</body>
</html>