<? require_once("conf.php");
Usuario::autenticarLogon();

$idSecaoSelecionada = request('idSecaoSelecionada');
$strIn = "";
foreach($secoesLixeira as $sl){
	$strIn .= ",$sl";
}
$strIn = substr($strIn,1);
$listaSecoes  = Secao::listar("","*",'id in ('.$strIn.')','nome ASC',"");
$lstSecoes = $lstTitulo = $listaGeral = "";
if($listaSecoes){
	foreach($listaSecoes as $ls){
		$selected = ($idSecaoSelecionada == $ls->getId())?"selected='selected'":"";	
		$lstSecoes .= "<option $selected value=\"".$ls->getId()."\">".$ls->getNome()."</option>";
	}
}

if($idSecaoSelecionada == USUARIOS){
	$lista = Usuario::listar("","id,login,status","status = ".INATIVO,"nome ASC","");//esta classe trabalha diferente das outras 
	if($lista){
		$listaGeral = "";
		$lstTitulo = "<th class='checkbox'><input type='checkbox' id='todosCkb' onClick='selecionaTudo(this)'/></th><th>Login</th><th class='acao'>Ação</th>";
		foreach($lista as $l){
			$listacbk = "<td class='checkbox'><input type='checkbox' class='cbkEdicao' value='".secureResponse($l->getId())."' name='selecao".secureResponse($l->getId())."' /></td>";
			$lstConteudo = "<td>".$l->getLogin()."</td>";
			$lstAcao = "<td class='acao'><a href=\"javascript:excluir('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-excluir\" title=\"Excluir\">Excluir</a><a href=\"javascript:restaurar('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-restaurar\">historico</a></td>";
			$listaGeral .= "<tr>$listacbk $lstConteudo $lstAcao</tr>";
		}
	}	
}

if($idSecaoSelecionada == NOTICIAS){
	$lista = VersaoConteudo::listar($idSecaoSelecionada,"","id,titulo,status","status = ".INATIVO,"titulo ASC","");//esta classe trabalha diferente das outras 
	if($lista){
		$listaGeral = "";
		$lstTitulo = "<th class='checkbox'><input type='checkbox' id='todosCkb' onClick='selecionaTudo(this)'/></th><th>Login</th><th class='acao'>Ação</th>";
		foreach($lista as $l){
			$listacbk = "<td class='checkbox'><input type='checkbox' class='cbkEdicao' value='".secureResponse($l->getId())."' name='selecao".secureResponse($l->getId())."' /></td>";
			$lstConteudo = "<td>".$l->getTitulo()."</td>";
			$lstAcao = "<td class='acao'><a href=\"javascript:excluir('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-excluir\" title=\"Excluir\">Excluir</a><a href=\"javascript:restaurar('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-restaurar\">historico</a></td>";
			$listaGeral .= "<tr>$listacbk $lstConteudo $lstAcao</tr>";
		}
	}	
}
if($idSecaoSelecionada == SEJA_NOSSO_CLIENTE){
	$lista = Cliente::listar("","id,nome,status","status = ".INATIVO,"nome ASC","");//esta classe trabalha diferente das outras 
        //print Cliente::getLogSql();
	if($lista){
		$listaGeral = "";
		$lstTitulo = "<th class='checkbox'><input type='checkbox' id='todosCkb' onClick='selecionaTudo(this)'/></th><th>Login</th><th class='acao'>Ação</th>";
		foreach($lista as $l){
			$listacbk = "<td class='checkbox'><input type='checkbox' class='cbkEdicao' value='".secureResponse($l->getId())."' name='selecao".secureResponse($l->getId())."' /></td>";
			$lstConteudo = "<td>".$l->getNome()."</td>";
			$lstAcao = "<td class='acao'><a href=\"javascript:excluir('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-excluir\" title=\"Excluir\">Excluir</a><a href=\"javascript:restaurar('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-restaurar\">historico</a></td>";
			$listaGeral .= "<tr>$listacbk $lstConteudo $lstAcao</tr>";
		}
	}	
}
if($idSecaoSelecionada == GESTAO_DE_PESSOAS){
	$lista = CadGestaoPessoa::listar("id,nome,status","status = ".INATIVO,"nome ASC","");//esta classe trabalha diferente das outras 
	if($lista){
		$listaGeral = "";
		$lstTitulo = "<th class='checkbox'><input type='checkbox' id='todosCkb' onClick='selecionaTudo(this)'/></th><th>Login</th><th class='acao'>Ação</th>";
		foreach($lista as $l){
			$listacbk = "<td class='checkbox'><input type='checkbox' class='cbkEdicao' value='".secureResponse($l->getId())."' name='selecao".secureResponse($l->getId())."' /></td>";
			$lstConteudo = "<td>".$l->getNome()."</td>";
			$lstAcao = "<td class='acao'><a href=\"javascript:excluir('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-excluir\" title=\"Excluir\">Excluir</a><a href=\"javascript:restaurar('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-restaurar\">historico</a></td>";
			$listaGeral .= "<tr>$listacbk $lstConteudo $lstAcao</tr>";
		}
	}	
}
if($idSecaoSelecionada == CONTATOS){
	$lista = Contato::listar("id,nome,status","status = ".INATIVO,"nome ASC","");//esta classe trabalha diferente das outras 
	if($lista){
		$listaGeral = "";
		$lstTitulo = "<th class='checkbox'><input type='checkbox' id='todosCkb' onClick='selecionaTudo(this)'/></th><th>Login</th><th class='acao'>Ação</th>";
		foreach($lista as $l){
			$listacbk = "<td class='checkbox'><input type='checkbox' class='cbkEdicao' value='".secureResponse($l->getId())."' name='selecao".secureResponse($l->getId())."' /></td>";
			$lstConteudo = "<td>".$l->getNome()."</td>";
			$lstAcao = "<td class='acao'><a href=\"javascript:excluir('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-excluir\" title=\"Excluir\">Excluir</a><a href=\"javascript:restaurar('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-restaurar\">historico</a></td>";
			$listaGeral .= "<tr>$listacbk $lstConteudo $lstAcao</tr>";
		}
	}	
}
if($idSecaoSelecionada == UNIDADES_PRODUTORAS){
	$lista = UnidadeProdutora::listar("id,nome,status","status = ".INATIVO,"nome ASC","");//esta classe trabalha diferente das outras 
	if($lista){
		$listaGeral = "";
		$lstTitulo = "<th class='checkbox'><input type='checkbox' id='todosCkb' onClick='selecionaTudo(this)'/></th><th>Unidade</th><th class='acao'>Ação</th>";
		foreach($lista as $l){
			$listacbk = "<td class='checkbox'><input type='checkbox' class='cbkEdicao' value='".secureResponse($l->getId())."' name='selecao".secureResponse($l->getId())."' /></td>";
			$lstConteudo = "<td>".$l->getNome()."</td>";
			$lstAcao = "<td class='acao'><a href=\"javascript:excluir('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-excluir\" title=\"Excluir\">Excluir</a><a href=\"javascript:restaurar('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-restaurar\">historico</a></td>";
			$listaGeral .= "<tr>$listacbk $lstConteudo $lstAcao</tr>";
		}
	}	
}

if($idSecaoSelecionada == ACOES_SUSTENTAVEIS || $idSecaoSelecionada == MENU_TIME_LINE){
    $lista = Chamada::listar($idSecaoSelecionada,"","id,titulo,status","status = ".INATIVO,"titulo ASC","");//esta classe trabalha diferente das outras 
    if($lista){
        $listaGeral = "";
        $lstTitulo = "<th class='checkbox'><input type='checkbox' id='todosCkb' onClick='selecionaTudo(this)'/></th><th>Unidade</th><th class='acao'>Ação</th>";
        foreach($lista as $l){
            $listacbk = "<td class='checkbox'><input type='checkbox' class='cbkEdicao' value='".secureResponse($l->getId())."' name='selecao".secureResponse($l->getId())."' /></td>";
            $lstConteudo = "<td>".$l->getTitulo()."</td>";
            $lstAcao = "<td class='acao'><a href=\"javascript:excluir('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-excluir\" title=\"Excluir\">Excluir</a><a href=\"javascript:restaurar('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-restaurar\">historico</a></td>";
            $listaGeral .= "<tr>$listacbk $lstConteudo $lstAcao</tr>";
        }
    }	
}


if($idSecaoSelecionada == CERTIFICACOES){
    $lista = VersaoConteudo::listar($idSecaoSelecionada,"","id,titulo,status","status = ".INATIVO,"titulo ASC","");//esta classe trabalha diferente das outras 
    if($lista){
        $listaGeral = "";
        $lstTitulo = "<th class='checkbox'><input type='checkbox' id='todosCkb' onClick='selecionaTudo(this)'/></th><th>Unidade</th><th class='acao'>Ação</th>";
        foreach($lista as $l){
            $listacbk = "<td class='checkbox'><input type='checkbox' class='cbkEdicao' value='".secureResponse($l->getId())."' name='selecao".secureResponse($l->getId())."' /></td>";
            $lstConteudo = "<td>".$l->getTitulo()."</td>";
            $lstAcao = "<td class='acao'><a href=\"javascript:excluir('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-excluir\" title=\"Excluir\">Excluir</a><a href=\"javascript:restaurar('".secureResponse($l->getId())."',$idSecaoSelecionada)\" class=\"bt-restaurar\">historico</a></td>";
            $listaGeral .= "<tr>$listacbk $lstConteudo $lstAcao</tr>";
        }
    }	
}


$idSecao = LIXEIRA;
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<? include "includes/head.php";?>
    <link rel="stylesheet" type="text/css" href="css/home.css"/>
</head>
<body>
	<div id="sapo">
		<div class="container">
		  <? include "includes/topo.php";?>                    
          <? include "includes/menu-lateral.php";?>
          
          <div class="coluna-geral">
            <? include "includes/topo_int.php";?>
            <div class="modulo-geral home3 lista">              
            	<div class="dados">
					<form  name="formulario" id="formulario" method="post">
                            <p>&Aacute;rea:<br/>
                            <select name="idSecaoSelecionada" onChange='atualizaFormulario()'>
                                <option value=''>Selecione</option>
                                <?=$lstSecoes?>
                            </select>
                            </p>
                    </form>
                </div>
                <table class="tabela" cellspacing="0">
                	<thead>
                    	<tr>
                        	<?=$lstTitulo?>
                        </tr>
                    </thead>
                    <tbody>
                    	<?=$listaGeral?>
                    </tbody>
                </table>        
                <div class="botoes">
                	<a href='javascript:restaurarSelecionados(<?=$idSecaoSelecionada?>)' class="bt-padrao">Restaurar Selecionados</a>
                    <a href='javascript: restaurarTudo(document.formulario.idSecaoSelecionada.value)' class="bt-padrao">Restaurar tudo</a>
                    <a href='javascript:excluirSelecionados(<?=$idSecaoSelecionada?>)' class="bt-padrao">Excluir Selecionados</a>
                    <a href='javascript:excluirTudo(document.formulario.idSecaoSelecionada.value)' class="bt-padrao">Excluir tudo</a>
                </div>
			</div>
          </div>          
		</div>
    </div>
</body>
</html>