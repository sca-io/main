<?
include("conf.php");

$idSecao = USUARIOS;
$id = secureRequest('id');
$acao = request('acao');
$indexAbaEdicao = 1;

$nome = "";
$login = "";
$email = "";
$status = "";


if($id){
    $user = Usuario::ler($id);
    $nome = $user->getNome();
    $login = $user->getLogin();
    $email = $user->getEmail();
    $status = $user->getStatus();
}




$permissoes = request('permissoes');
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<? include "includes/head.php";?>
    <link rel="stylesheet" type="text/css" href="css/edicao-conteudo.css"/>
	<script>


	function envia(){
            var destino_frm = "usuario_controle.php";
            var target_frm = "";
            msg = [];
            vld = [];

            msg['nome'] = "Nome"; vld['nome'] = 1;
            msg['login'] = "Login"; vld['login'] = 1;
            msg['email'] = "E-mail"; vld['email'] = 2;
		var frm = document.formulario; 	
		/*if(frm.id.value.length ==0){
			msg['senha'] = "Senha"; vld['senha'] = 1;
		}	*/
		fSucess = function(url){					
			frm.urlRetorno.value = 'listagem_usuario.php';
			frm.action = destino_frm;
			frm.submit();
		}
		fErro = function(strErro){
			var str = sapo_replace(",",";\n",strErro);
			alert("Por favor preencha os campos:\n"+str);
			enviando = 0;	
		}
		validaFormulario('formulario',fSucess,fErro)
	}
</script>
<body>
    <div id="sapo">
		<div class="container">
		  <? include "includes/topo.php";?>          
          <? include "includes/menu-lateral.php";?>          
          <div class="coluna-geral">
			<? include("includes/topo_int.php"); ?>
    <div class="modulo-geral">
        <form name="formulario" id="formulario" method="post">
	    <input type='hidden' name='urlRetorno' value='listagem_usuario.php' />
            <input type='hidden' name='id' value='<?=secureResponse($id)?>' />
            <input type='hidden' name='acao' value="salvar" />
            <input type='hidden' name='tipoUsuario' value="1" />
            <div class="esq">                
                <p><strong>Nome:</strong><input name="nome" id="nome" value="<?=$nome ?>" /></p>
                <p><strong>Login:</strong><input name="login" id="login" value="<?=$login ?>" /></p>
                <p><strong>E-mail:</strong>	<input name="email" id="email" value="<?=$email ?>" /></p>
                <? if($status == Usuario::BLOQUEADO):?><p>
                    <strong>Status:</strong>
                    <select id='status' name='status'>
                        <?	 $std = Array("","");	$std[$status] = "selected";?>
                        <option value='<?=Usuario::ATIVO?>' <?=$std[Usuario::ATIVO]?>>Ativo</option>
                        <option value='<?=Usuario::BLOQUEADO?>' <?=$std[Usuario::BLOQUEADO]?>>Bloqueado</option>        
                    </select>
                </p><? endif;?>
                <a class="bt-padrao" href="javascript:wopen('popup_permissoes.php?id=<?=secureResponse($id)?>','pop',300,600,'auto');">Permissões</a>
                <a href="javascript:envia()" class="bt-padrao">Salvar</a> 
            </div>            
        </form>

</div>

<script>
	<? if(!$id):?>
	limpaform('formulario','id,urlRetorno,acao')
	<? endif;?>
</script>
</div>
</div>
</div>
</body>
</html>
