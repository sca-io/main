<? require_once("conf.php");
Usuario::autenticarLogon();

$id = secureRequest("id");
$idSecao = request("idSecao");
$idDestaque = request("idDestaque");
$tipoProp = request("tipoProp");
$callBack = request("callBack");
$src = request("src");
$diretorio = request("diretorio");

$urlRetorno = request("urlRetorno")?request("urlRetorno"):"edicao-media.php";

//print $id."////";exit;
if($id){
    $obj = Media::ler($id);
    $diretorio = $obj->getDir();
    $src = $obj->getUrl();
}



$diretorio = DIRETORIO_RAIZ.$diretorio;

if(isset($porporcoes[$idSecao]) && isset($porporcoes[$idSecao][$tipoProp])) $aspectRatio = $porporcoes[$idSecao][$tipoProp]->aspect;
else  $aspectRatio = $porporcoes["default"][$tipoProp]->aspect;

?><!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"><html>
<head>
<meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE" />
<head>
<script src='js/jquery-1.3.2.min.js'></script>
<script src="js/jquery.min.js"type="text/javascript"></script>
<script src="js/jquery.Jcrop.js"type="text/javascript"></script>
<script src="js/scripts.js" language="JavaScript" type="text/javascript"></script>
<link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />
<script type="text/javascript">
$(function(){
    $('#cropbox').Jcrop({
        <? if($aspectRatio):?>
            aspectRatio: <?=$aspectRatio?>,
        <?endif;?>
        onSelect: updateCoords
    });
});     

function checkCoords(){
    if(parseInt($('#w').val())){
        return true;
    }else{
        alert('Selecione alguma área da imagem para corta-la');
        return false;
    }
};

function updateCoords(c){
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
};      
</script>
<script type="text/javascript">
var msg = Array();
var vld = Array();
var destino_frm = "media_controle.php";
var target_frm = "clapa";

function cortar(){
    var frm = document.formulario;
    fSucess = function(url){
        frm.acao.value = "cortarImg";
        frm.action = destino_frm;
        frm.submit();        
        
    }
    fErro = function(strErro){
        var str = sapo_replace(",",";<br/>",strErro);
        alerta("<b>Por favor preencha os campos:</b><br/>"+str);
        enviando = 0;
    }
    if(checkCoords()){
        validaFormulario('formulario',fSucess,fErro)
    }
}

$(window).ready(function(){
    var iHeight = $(".pop-galeria-de-imagens").height();
    var iWidth = $(".pop-galeria-de-imagens").width();
    if(parseInt(iHeight) > 600){
        parent.$("#popMedia").attr("height",iHeight);
    }else{
        parent.$("#popMedia").attr("height",600);
    }
    if(parseInt(iWidth) > 597){
        parent.$("#popMedia").attr("height",iWidth);
    }else{
        parent.$("#popMedia").attr("height",597);
    }
});
</script>
<style type="text/css">
    *{padding: 0; margin: 0;}
    BODY{ background-color: #FFF; }
    DIV H3{ background-color:#f98823; padding:3px 5px 5px 15px; font:normal bold 11pt/14px verdana; color:#FFF;  position:relative; }
    DIV H3 A{ position:absolute; right:5px; top:2px; width:15px; height:21px;  font:normal bold 14pt/17px verdana; color:#FFF; cursor:pointer; }    
    P{ padding:5px 15px;}
    .bt-padrao{ border-top:1px solid #a9a8a8; border-right:1px solid #908f90; border-bottom:1px solid #838283; background-color:#383738; padding:3px 8px 4px 8px; font:normal bold 9pt/13px verdana; text-decoration: none;color:#FFF; }
</style>
</head>
<body>
    <div class="pop-galeria-de-imagens">
        <h3>Recortar<a onClick="parent.popup.close();" class="bt-fechar" title="Fechar">X</a></h3>
        <p>
            <img src="<?=str_replace("//","/",$diretorio)?><?=$src?>" id="cropbox" />
        </p>
        <form name="formulario" method="post" enctype='multipart/form-data' target="popMedia">
            <input type="hidden" id="x" name="x" />
            <input type="hidden" id="y" name="y" />
            <input type="hidden" id="w" name="w" />
            <input type="hidden" id="h" name="h" />
            
            <input type="hidden" id="src" name="src" value="<?=$src?>"/>
            <input type="hidden" id="diretorio" name="diretorio" value="<?=$diretorio?>"/>
            <input type="hidden" name="idSecao" value="<?=$idSecao?>" />
            <input type="hidden" name="idDestaque" value="<?=$idDestaque?>" />
            <input type="hidden" name="urlRetorno" value="<?=$urlRetorno?>" />
            <input type="hidden" name="acao" value="" />
            <input type="hidden" name="tipoProp" value="<?=$tipoProp?>" />
            <input type="hidden" name="callBack" value="<?=$callBack?>" />
            <input type="hidden" name="id" value="<?=secureResponse($id)?>" />

            <p>
                <a class="bt-padrao" href="javascript:cortar()" title="Recortar">Recortar</a>                
            </p>
        </form>
    </div>
</body>
</html>