in_array = function (value,arr){
	for (var i=0; i < arr.length; i++) {
		if (arr[i] === value) {
			return true;
		}
	}
	return false;
}

showInputCalendar = function(elem){
    var me = this;
    if(me.elem){
        $("#container_calendario").remove();
    }
    if(me.elem != elem){
        me.elem = elem;
        $(elem).after("<span id='container_calendario' style='display:none' ></span>");	
        cal = new Calendario({
            instanceName:'cal',
            closeable:true,
            destino:'#container_calendario'
        });	
        cal.onClose = function(){
            $("#container_calendario").remove();
            me.elem = null;
        }
        if(elem.value != ''){
            var data = elem.value.split('/');
            cal.atualizar(data[0],data[1],data[2]);
        }		
        $("#container_calendario").css("display","block");
        cal.aoSelecionarData = function(){		
            $(elem).val(this.getData('d/m/y'));
            cal.close();
        }
    }else{
        me.elem = null;
    }	
}


function Calendario(args){
	var me = this;
	//this.classes = args.classes;	
	this.destino = (args.destino)?args.destino:'calendario';
	this.instanceName = args.instanceName;
	this.aoSelecionarData = args.aoSelecionarData;
	this.datasAgendadas = [];
	this.onClose = args.onClose;
	var datas = new Array(42);
	var dataSel = [];	
	var strMes=new Array('Janeiro','Fervereiro','Mar&ccedil;o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');	
	var strLmDias=new Array('31','28','31','30','31','30','31','31','30','31','30','31');
	var strLmDiasB=new Array('31','29','31','30','31','30','31','31','30','31','30','31');	
	if(args.closeable){
		var btX = '<td class="fechar"><a href="javascript:void(0)" onclick="'+this.instanceName+'.close()">X</a></td>';
		var num = '6';
	}else{
		var btX = '';
		var num = '7';
	}
	var mesNavegador = '<tr><td colspan="'+num+'" class="mes"><a href="javascript:void(0)" id="seta-ant" class="bt-seta-ant">&lt;&lt;</a><b id="letreiroMesAno"></b><a href="javascript:void(0)" id="seta-prox" class="bt-seta-prx">&gt;&gt;</a></td>'+btX+'</tr>';	
	var htmlDiasSemana = '<tr><th>D</th><th>S</th><th>T</th><th>Q</th><th>Q</th><th>S</th><th>S</th></tr>';
	var htmlDias = '';
	for(var linha=1;linha<=6;linha++) {
		htmlDias += '<tr>';
		for(var col=1;col<=7;col++) {		
			num=7 * (linha-1) - (-col);
			htmlDias += '<td class="dias"><a id="dia_' + num + '">&nbsp;</a></td>';
		}
		htmlDias += '</tr>';
	}
	var htmlCalendar = '<table class="calendario" align="center" id="calendario">'+mesNavegador+htmlDiasSemana+htmlDias+'</table>';	
	///document.all?document.attachEvent('onclick',checkClick):document.addEventListener('click',checkClick,false);
	var agora = new Date;
	var numMes=agora.getMonth();
	var numAno=agora.getFullYear();
	this.mes=agora.getMonth();
	this.ano=agora.getFullYear();	

	this.selecionarData = function(data){
		dataSel = datas[data];
		if(me.aoSelecionarData)
			me.aoSelecionarData()
	}	
	this.setDataAgendada = function(data){
		me.datasAgendadas.push(data);
	}
	this.limparDatasAgendadas = function(){
		me.datasAgendadas = [];
	}
	this.getDatasAgendadas = function(){
		return me.datasAgendadas;
	}
	this.getAno = function(){
		return me.ano;
	}
	this.getMes = function(){
		return me.mes;
	}
	this.getData = function(formato){	
		if(dataSel.length > 0){
			if(formato == 'd-m-y'){
				return dataSel[0]+"-"+dataSel[1]+"-"+dataSel[2];
			}else if(formato == 'y-m-d'){
				return dataSel[2]+"-"+dataSel[1]+"-"+dataSel[0];
			}else if(formato == 'd/m/y'){
				return dataSel[0]+"/"+dataSel[1]+"/"+dataSel[2];
			}else if(formato == 'y/m/d'){
				return dataSel[2]+"/"+dataSel[1]+"/"+dataSel[0];
			}
		}else{
			return '';	
		}
	}
	this.close = function(){
		$("#calendario").remove();
		if(this.onClose)
		this.onClose();
	}
	this.atualizar = function(dia,mes,ano){
		if(mes)
		me.mes = mes-1;		
		if(ano)
		me.ano = ano;
		
		agora = new Date();
		dia_atual = agora.getDate();
		data_aux=new Date();
		data_aux.setDate(1);
		data_aux.setFullYear(me.ano);
		data_aux.setMonth(me.mes);
		dia_semana = data_aux.getDay();
		$('#letreiroMesAno').html(strMes[Math.abs(me.mes)]+ ' ' + me.ano);		
		me.limiteDias=((me.ano%4)==0)?strLmDiasB:strLmDias;
		for(var d=1;d<=42;d++){			
			$('#dia_'+d).parent().attr('class','vazio');
			if ((d >= (dia_semana -(-1))) && (d<=dia_semana-(-me.limiteDias[me.mes]))) {
				//dip=((d-dia_semana < dia_atual)&&(mes==numMes)&&(me.ano==numAno));
				dip=((d-dia_semana <= 0)&&(me.mes==numMes)&&(me.ano==numAno));
				
				//none=((dia!='')&&(d-dia_semana==dia));
				isToday=((d-dia_semana)==dia_atual&&(me.mes==numMes)&&(me.ano==numAno));
				//alert(isToday)
				var iDia = parseInt((d-dia_semana) / 10);
				var iDia = (iDia == 0)?'0'+(d-dia_semana):(d-dia_semana);
				var iMes = ((parseInt((me.mes-(-1)) / 10)) == 0)?'0'+(me.mes-(-1)):me.mes-(-1);
				classExtra = '';
				if(iDia == dia){
					classExtra = " selected"//dia selecionado
					dataSel = [iDia,iMes,me.ano];
				}
				if (dip){//alert('inativo')
					$('#dia_'+d).parent().attr('class',"vazio"+classExtra);//inativo
				}else if (isToday){//alert('atual')
					$('#dia_'+d).parent().attr('class',"hoje"+classExtra);//dia atual
				}else{//alert('ativo')
					$('#dia_'+d).parent().attr('class',"dias"+classExtra);
				}
				if(me.datasAgendadas){
					if(in_array(me.ano+'-'+iMes+'-'+iDia,me.datasAgendadas) && !isToday){
						$('#dia_'+d).parent().attr('class',"ativo"+classExtra); //contem compromisso agendado
					}
				}

				$('#dia_'+d).attr('href','javascript:'+me.instanceName+".selecionarData("+d+")");				
				$('#dia_'+d).html(d-dia_semana);				

	
				datas[d]=[iDia,iMes,me.ano];
				//alert(me.datas[d])
			}
			else {
				$('#dia_'+d).html('');
			}
		}
	}		
	this.proximo = function() {
		me.mes+=1;
		if (me.mes>=12) {
			me.mes=0;
			me.ano++;
		}
		me.atualizar();
		if(me.aoMudarMes)
			me.aoMudarMes();
		//alert(me.datas[1])
	}	
	this.anterior = function() {
		me.mes-=1;
		if (me.mes<0) {
			me.mes=11;
			me.ano--;
		}
		me.atualizar();
		if(me.aoMudarMes)
			me.aoMudarMes();
	}
	$(me.destino).html(htmlCalendar);
	this.atualizar();
	$("#seta-ant").click(this.anterior);
	$("#seta-prox").click(this.proximo);
	/*function cdayf() {
		if ((me.ano>numAno)|((me.ano==numAno)&&(me.mes>=numMes)))
			return ;
		else {
			me.ano=numAno;
			me.mes=numMes;
		}
	}*/
}