﻿/*********************************** -- CONSTANTES DO PHP -- ******************************/
var USUARIOS = 1;
var LIXEIRA = 2;
var HISTORICOS = 3;
var CONTEUDO = 12;


/*****************************************************************/
function display(idObj,valor){
    obj = document.getElementById(idObj); 
    if(obj){
        if(valor){
            obj.style.display = valor;
        }else{
            var dsp = obj;
            if(dsp.style.display == 'none' || dsp.style.display.length == 0){
                dsp.style.display = 'block';
            }else{
                dsp.style.display = 'none';
            }
        }
    }
}

popupAlerta = false;
function alerta(texto,onclose,botoes){
    popupAlerta = new Popup({
        idPopup:"alertAtomica",			  
        bt_close:'.bt-ok',
        mask:true,
        maskColor:'#000',
        Fixed:false,
        posRelScroll:true,
        fade:false
    });
	if(!botoes) botoes = '<a class="bt-ok">Ok</a>';
    popupAlerta.open('<div id="popAlerta"><p>'+texto+'</p>'+botoes+'</div>');
    if(onclose){popupAlerta.onClose = onclose;}
}

popupConf = false;
var popConfConfirm = function(){};
var popConfCancel = function(){};
function confirma(texto,onConfirm,onCancel){
    popupConf = new Popup({
        idPopup:"alertAtomica",			  
        bt_close:'.close',
        mask:true,
        maskColor:'#000',
        Fixed:false,
        posRelScroll:true,
        fade:false
    });
    botoes = '<a href="javascript:popConfConfirm()" class="close bt-padrao">Ok</a><a href="javascript:popConfCancel()" class="close bt-padrao">Cancelar</a>';
    popupConf.open('<div id="popAlerta"><p>'+texto+'</p>'+botoes+'</div>');
    if(onConfirm){popConfConfirm = onConfirm;}
    if(onCancel){popConfCancel = onCancel;}
}

var popup = false;
function openPop(pagina){
    pagina = (pagina.indexOf('?')!=-1)?pagina+"&nocache="+Math.random():pagina+"?nocache="+Math.random();
    /*if(popup)
        popup.close();*/
    if(!popup){
        popup = true;
        $.post(pagina, function(data){
            popup = new Popup({
                bt_close:'.bt-fechar',
                mask:true,
                Fixed:false,
                posRelScroll:true,//posicao relacionada ao Scroll 
                closeToEsc:true,
                fade:true
            });
            popup.open(data);
            popup.onClose = function(){
                popup = false;
            }
        });	
    }
}

/******************************** FUNCOES DE AJAX ***************************************/ 
var http_request = false;
function getXmlHttp(){
	 if(window.XMLHttpRequest){
		 return new XMLHttpRequest();
	 } else if (window.ActiveXObject) {			 
		 var axO=['Microsoft.XMLHTTP','Msxml2.XMLHTTP','Msxml2.XMLHTTP.6.0','Msxml2.XMLHTTP.4.0','Msxml2.XMLHTTP.3.0'];
		 for(var i=0;i<axO.length;i++){ 
			try{ 
				return new ActiveXObject(axO[i]);
				}catch(e){
					
				} 
		}
	}
 }  
 
function makeRequest(url,funcao,isAssincrono,isPost,campos) {	//MakeREQUEST
	if(!isAssincrono){
		isAssincrono = true;
	}
	http_request=getXmlHttp();

	if (!http_request) {			
		return false;
	}
	if(!funcao){
		funcao = defaultRequest;
	}		
	http_request.onreadystatechange = function(){
		//alerta(http_request.responseText)
		if (http_request.readyState == 4 && http_request.status == 200){
			funcao();
		}
	};
	
	if(isPost){
		http_request.open('POST', url, isAssincrono);			
		http_request.setRequestHeader("Content-type","application/x-www-form-urlencoded");	
		http_request.setRequestHeader("Content-length",campos.length);				
		http_request.send(campos);
	}else{
		http_request.open('GET', url, isAssincrono);
		http_request.send(null);
	}
}
	
function getDadosXml(xml){
	var obj = new Object();

	for(var i=0;i<xml.childNodes.length;i++){
		if(xml.childNodes[i].nodeType == 1){
			if(xml.childNodes[i].childNodes[0]){
				var node = xml.childNodes[i].childNodes[0].data;
			}else{
				var node = "";
			}
			node = node.replace(new RegExp("[']",'gi'), "&quot;");
			node = node.replace(new RegExp('["]','gi'), "&quot;");
			var tagNome = xml.childNodes[i].nodeName;
			if(tagNome == "link"){
				tagNome = "Link";
			}
			obj[tagNome] = node;			
		}
	}

	return obj;
}	


/*******************************************************************************************************************/	
 function geraQueryString(frm){
	var query = '';
	var cont = 0;
	for (var i = 0; i < frm.elements.length; i++) {
		if(frm.elements[i].type){
			if(frm.elements[i].type.indexOf('text') == 0){
				if (cont==0) {
					query = '?' + frm.elements[i].name + '=' + frm.elements[i].value;
				} else {
					query = query + '&' + frm.elements[i].name + '=' + frm.elements[i].value;
				}
				cont++;
			}else if(frm.elements[i].type.indexOf('checkbox')== 0){			
				if(frm.elements[i].checked){
					query = query + '&' + frm.elements[i].name + '=' + frm.elements[i].value;
					cont++;
				}			
			}else{
				query = query + '&' + frm.elements[i].name + '=' + frm.elements[i].value;
				cont++;		
			}
		}else{
			
		}
		
	}
	return (query);
}


function sapo_replace(strFind,newValue,str){
	while(str.indexOf(strFind)!=-1){
			str = str.replace(strFind,newValue); 
		} 
	return str;	
}
/*** VARIAVEIS DE VALIDACAO DE FORMULARIO ***/
var msg = Array();
var vld = Array();
var destino_frm = "";
var target_frm = "adm_miolo";
/*** ************************************ ***/

function validaFormulario(idForm,funcSucesso,funcErro){	
	var frm = document.forms[idForm];
	var checagem = 1;
	var count = frm.elements.length;
	var str_campo = "";
	for(i=0; i<frm.elements.length; i++){
		if(msg[frm.elements[i].name]){
			if(vld[frm.elements[i].name] == 1){
				checagem = validaCampo(frm.elements[i]);
			}
			else if(vld[frm.elements[i].name] == 2){
				checagem = validaEmail(frm.elements[i]);
			}else if(vld[frm.elements[i].name] == 3){
				 checagem = validaData("d-m-y");
			}else if(vld[frm.elements[i].name] == 4){
				 checagem = validaData("d-m");
			}else if(vld[frm.elements[i].name] == 5){
				 checagem = validaData("m-y");
			}else if(vld[frm.elements[i].name] == 6){
				 checagem = validaData("y");
			}else if(vld[frm.elements[i].name] == 7){
				 checagem = validaDataFinal("d-m-y");
			}else if(vld[frm.elements[i].name] == 8){
				 checagem = validaDataFinal("d-m");
			}else if(vld[frm.elements[i].name] == 9){
				 checagem = validaDataFinal("m-y");
			}else if(vld[frm.elements[i].name] == 10){
				 checagem = validaDataFinal("y");
			}
			
			if(!checagem){
				str_campo += msg[frm.elements[i].name] + ",";
				count --;
			}
		}
	}
	if(count == frm.elements.length){
		if(funcSucesso){
			funcSucesso(destino_frm);
		}
	}else{	
		if(funcErro){
			funcErro(str_campo);
		}
		return false;
	}
}

function validaCampo(campo){
	return campo.value != "";
}

function validaEmail(campo){
	return (campo.value != "" && campo.value.indexOf("@") > 0 && campo.value.indexOf(".") > 0);
}

function apenasNum(campo){
	campo.value = campo.value.replace(/[^0-9]/g, '');
}

function setCookie(name, value, expiresdays, path, domain, secure) {
	var today = new Date();
	var expires = new Date();
	expires.setTime(today.getTime() + 3600000*24*expiresdays);
    document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires.toGMTString() : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    } else {
        begin += 2;
    }
    var end = document.cookie.indexOf(";", begin);
    if (end == -1) {
        end = dc.length;
    }
    return unescape(dc.substring(begin + prefix.length, end));
}

function deleteCookie(name, path, domain) {
    if (getCookie(name)) {
        document.cookie = name + "=" +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") +
            "; expires=Thu, 01-Jan-70 00:00:01 GMT";
    }
}


function atualizaForm(idFormulario){
	document.forms['formulario'+idFormulario].submit();
}

var Ids = '';
function getSelecao(){
	Ids = '';
	$('.cbkEdicao').each(function(i){
		if($(this).attr('checked')){
			Ids +=','+$(this).attr('value');
		}		
	});
	Ids =Ids .substring(1);
	return Ids;
}
function selecionaTudo(obj){
	$('.cbkEdicao').each(function(i){
		$(this).attr('checked',$("#todosCkb").attr('checked'));		
	});
}

function wopen(page,nwin,larg,altu, scroll){
    window.open(page,nwin,'width='+larg+',height='+altu+',toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars='+scroll+',resizable=no,menubar=no,top=80,left=100');
}

function deletar(id,idSecao,urlRetorno){	
	var strIdioma = "";
	confirma('Deseja realmente excluir este conteúdo?',function(){
		//if(idioma) strIdioma = "&idioma="+idioma;
                if(urlRetorno) urlRetorno = "&urlRetorno="+urlRetorno;
                else  urlRetorno = "";
		window.location = 'exclusao.php?acao=enviarLixeira&id='+id+'&idSecao='+idSecao+urlRetorno;
	})
}

function addInputHidden(nome,value){
	var frm = document.formulario;
	var input = document.createElement("input");
	input.setAttribute("type","hidden");
	input.setAttribute("name",nome);
	input.setAttribute("id",nome);
	input.value = value;
	frm.appendChild(input);	
}

function getPermissoes(form,arrIdSub){
	addInputHidden('permissoes_secao_ativos',form.permissoes_secao_ativos.value);
	addInputHidden('permissoes_secao_inativos',form.permissoes_secao_inativos.value);
	for(i=0;i<arrIdSub.length;i++){
		addInputHidden("perm_sub_"+arrIdSub[i]+"_ativos",form.elements["perm_sub_"+arrIdSub[i]+"_ativos"].value);	
		addInputHidden("perm_sub_"+arrIdSub[i]+"_inativos",form.elements["perm_sub_"+arrIdSub[i]+"_inativos"].value);	
	}
	addInputHidden('salvaPermissoes',"true");    
	
}

function atualizaFormulario(){
    var frm = document.formulario;
    frm.submit();
}

function excluir(id,idSecao){
	confirma('Deseja realmente excluir este conteúdo?',function(){
		window.location = 'exclusao.php?acao=excluir&id='+id+'&idSecao='+idSecao;
	})
}

function excluirTudo(idSecao){
    confirma('Deseja realmente excluir tudo?',function(){
        window.location = 'exclusao.php?acao=excluirTudo&idSecao='+idSecao;
    })
}

function restaurar(id,idSecao){
    confirma('Deseja realmente restaurar este conteúdo?',function(){
        window.location = 'exclusao.php?acao=restaurar&id='+id+'&idSecao='+idSecao;
    })
}

function restaurarTudo(idSecao){
    confirma('Deseja realmente restaurar tudo?',function(){
        window.location = 'exclusao.php?acao=restaurarTudo&idSecao='+idSecao;
    })
}

function atualizaComentario(idComentario,idColirio,obj){
	document.location = '?acao=alterarComentario&idComentario='+idComentario+'&statusComentario='+obj.value+'&id='+idColirio+'&indexAba=2';
}

function excluirComentario(idComentario,idColirio){
	confirma('Deseja realmente excluir este comentário ?',function(){
		document.location = '?acao=excluirComentario&idComentario='+idComentario+'&id='+idColirio+'&indexAba=2';
	})
}

function enviaLixeiraSelecionados(idSecao){
	arrIDs = getSelecao();
	if(arrIDs.length > 0){
		confirma('Deseja realmente excluir os itens selecionados ?',function(){
			window.location = 'exclusao.php?acao=enviarLixeiraSelecionados&idSecao='+idSecao+'&ids='+arrIDs;
		})
	}else{
		alerta('Selecione algum item');
	}
}

function excluirSelecionados(idSecao){
	arrIDs = getSelecao();
	if(arrIDs.length > 0){
		confirma('Deseja realmente excluir os itens selecionados?',function(){
			window.location = 'exclusao.php?acao=excluirSelecionados&idSecao='+idSecao+'&ids='+arrIDs;
		})
	}else{
		alerta('Selecione algum item');
	}
}

function restaurarSelecionados(idSecao){
	arrIDs = getSelecao();
	if(arrIDs.length > 0){
		confirma('Deseja realmente restaurar os itens selecionados ?',function(){
			window.location = 'exclusao.php?acao=restaurarSelecionados&idSecao='+idSecao+'&ids='+arrIDs;
		})
	}else{
		alerta('Selecione algum item');
	}
}

function limpaCampos(idForm,strCamposAux){
	var frm = document.forms[idForm];
	if(strCamposAux){
		var arr = strCamposAux.split(',');
		for(i = 0; i < arr.length; i++){
			for (var j = 0; j < frm.elements.length; j++) {
				if(arr[i] == frm.elements[j].id){
					frm.elements[j].value='';							
				}
			}
		}		
	}
	
	for (var i = 0; i < frm.elements.length; i++) {
		if(frm.elements[i].type){
			if(frm.elements[i].type.indexOf('text') == 0){
				frm.elements[i].value='';
			}else if(frm.elements[i].type.indexOf('checkbox')== 0){			
				frm.elements[i].checked = false;			
			}else if(frm.elements[i].type.indexOf('select')== 0){	
				var sele = frm.elements[i];	
				if(sele[0]){
					sele[0].selected = true;
				}
			}
		}		
	}	
}

function getCoordsMouse(ev){    
    if(typeof(ev.pageX)!=="undefined"){
      return {x:ev.pageX, y:ev.pageY};
    }else{
        return {
          x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
          y:ev.clientY + document.body.scrollTop  - document.body.clientTop
        };
    }
}

function inArray(obj,valor){
	for(var i=0;i<obj.length;i++) {
		if(obj[i] == valor) {
			return true;
		}
	}
	return false;
}

function limpaform(idformulario,excessoes){
    if(excessoes)
    excessoes = excessoes.split(',')
    var frm =document.forms[idformulario];
    for (i=0;i<frm.elements.length;i++){
        var nome = frm.elements[i].getAttribute("id") || frm.elements[i].getAttribute("name");
        if(excessoes && !inArray(excessoes,nome))
        frm.elements[i].value = '';
    }	
}

function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable){
      return pair[1];
    }
  } 
  return "";
}

function strip_tags(str, allowed_tags){
    var key = '', allowed = false;
    var matches = [];    
	var allowed_array = [];
    var allowed_tag = '';
    var i = 0;
    var k = '';
    var html = ''; 
    var replacer = function (search, replace, str){return str.split(search).join(replace);};
    if(allowed_tags){allowed_array = allowed_tags.match(/([a-zA-Z0-9]+)/gi);}
    str += ''; 
    matches = str.match(/(<\/?[\S][^>]*>)/gi);
    for(key in matches){
        if(isNaN(key)){continue;}
        html = matches[key].toString();
        allowed = false; 
        for(k in allowed_array){
            allowed_tag = allowed_array[k];
            i = -1; 
            if(i != 0){i = html.toLowerCase().indexOf('<'+allowed_tag+'>');}
			if(i != 0){i = html.toLowerCase().indexOf('<'+allowed_tag+' ');}
            if(i != 0){i = html.toLowerCase().indexOf('</'+allowed_tag);} 
            if(i == 0){allowed = true;break;}
        }
         if(!allowed){str = replacer(html,"",str);}
    }
     return str;
}

function Trim(str){
	return str.replace(/^\s+|\s+$/g,"");
}

function isLocation(nome){
	str = window.location.toString();
	if(str.indexOf(nome) > -1){
		return true;	
	}else{
		return false;	
	}	
}

function pressedEnter(event){
	var keynum;       
	if(window.event) { //IE  
		keynum = event.keyCode  
	} else if(event.which) { // Netscape/Firefox/Opera AQUI ESTAVA O PEQUENINO ERRO ao invés de "e." é "event."  
		keynum = event.which  
	}  
	if( keynum==13 ) { <!-- 13 é o código do Enter --> AQUI TAMBEM  
		return true;		
	}else{
		return false;
	} 
}

function checarTecla(event,funcao){
	if(pressedEnter(event)){		
		funcao();
	}
}

function toggleQuadro(id,callBack){
	if($(id).css("display") == "none"){
		$(id).slideDown("normal",callBack);
	}else{
		$(id).slideUp("normal",callBack);
	}
}

function getTextEditorParent(campo){
    var editor = parent.CKEDITOR.instances[campo];
    return editor.getData()		
}

function setTextEditorParent(campo,valor){
    var editor = parent.CKEDITOR.instances[campo];
    editor.setData(valor)
}

function getTextEditor(campo){
    var editor = CKEDITOR.instances[campo];
    return editor.getData()		
}

function setTextEditor(campo,valor){
    var editor = CKEDITOR.instances[campo];
    editor.setData(valor)
}

function onSelectAlign(ftClass){}
function showOpcoesDeAlinImg(){
    alerta('Em qual lado deseja inserir a imagem:<br /><br />',function(){},'<a href="javascript:onSelectAlign(\'ft-esq\')" class="bt-padrao">Esquerda</a><a href="javascript:onSelectAlign(\'ft-dir\')" class="bt-padrao">Direita</a>')
}

function inserirFCKComAling(idImg,titulo,legenda){
    onSelectAlign = function(ftClass){
        popupAlerta.onClose = function(){inserirFCK(idImg,titulo,legenda,ftClass)}
        popupAlerta.close();

    }
    showOpcoesDeAlinImg()
}

function inserirFCK(idImg,titulo,legenda,alinhamnt){
    var txt = getTextEditorParent("texto");
    var src = $("#imgIns_"+idImg).attr("src");
    var titImg = titulo?titulo:legenda;
    htmlImg = "<img src='"+src+"' title='"+titImg+"' alt='"+titImg+"' />";
    if(legenda && alinhamnt) htmlImg = "<a class=\""+alinhamnt+"\">"+htmlImg+"<b>"+legenda+"</b></a>";
    //alert(htmlImg)
    setTextEditorParent("texto",htmlImg+txt);
    alerta("Imagem adicionada com sucesso!");
}

function contaCaracteres(campo,limit){
	var text = campo.value;
	if(text){
		if(text.length > limit) {
			campo.value = text.substring(0, limit);
		}
	}
}

function addOption(txt,value,input,selected){
	var nova_opcao = document.createElement("option");
	var texto = document.createTextNode(txt); 
	nova_opcao.setAttribute("value",value); 
	if(selected)
		nova_opcao.setAttribute("selected","selected"); 
	nova_opcao.appendChild(texto); 
	input.appendChild(nova_opcao);
}

function SlideUpDown(id){
	var dsp = $(id).css("display");	
	if(dsp == "block"){
		$(id).slideUp();
	}else{
		$(id).slideDown();
	}
}

/*** INPUT COM CALENDARIO ***/
showInputCalendar = function(elem){
	var me = this;
	if(me.elem){
		$("#container_calendario").remove();
	}
	if(me.elem != elem){
		me.elem = elem;
		$(elem).after("<span id='container_calendario' style='display:none' ></span>");	
		cal = new Calendario({
			instanceName:'cal',
			destino:'#container_calendario',
			closeable: true
		});	
		if(elem.value != ''){
			var data = elem.value.split('/');
			cal.atualizar(data[0],data[1],data[2]);
		}		
		$("#container_calendario").show("normal");
		cal.aoSelecionarData = function(){		
			$(elem).val(this.getData('m/d/y'));
			cal.close();
		}
		cal.onClose = function(){
			$("#container_calendario").remove();
			me.elem = null;
		}
	}else{
		me.elem = null;
	}
}

function closeMensagem(idForm){
	var form = document.forms[idForm]
	var boxMsg = document.getElementById("mensagem");
	form.style.display = "block";
	boxMsg.style.display = "none";	
	boxMsg.innerHTML = "";		
}
	
function alterarSenha(){
    msg['senha'] = "Senha";vld['senha'] = 1;
    msg['confirmaSenha'] = "Confirma senha";vld['confirmaSenha'] = 1;

	var frm = document.formAlterarSenha;
	var boxMsg = document.getElementById("mensagem");
	
	if(frm.senha.value != frm.confirmaSenha.value){
		//alert("Os campos senha e confirma senha devem ser iguais.");
		frm.style.display = "none";
		boxMsg.innerHTML = "Os campos senha e confirmar senha devem ser iguais.<br /><br /><a class='bt-padrao' href='javascript:closeMensagem(\"formAlterarSenha\")'>Voltar</a>";
		boxMsg.style.display = "block";	
		return
	}

	fSucess = function(url){	
		var url = 'usuario_controle.php?acao=alterarSenha&senha='+frm.senha.value+'&confirmaSenha='+frm.confirmaSenha.value;
		makeRequest(url,function(){
			var retorno = http_request.responseText;
			if(retorno == "sucesso"){
				frm.style.display = "none";
				boxMsg.innerHTML = "Senha alterada com sucesso.<br /><br /><a class='bt-padrao bt-fechar' href='javascript:popup.close()'>OK</a>";
				boxMsg.style.display = "block";	
				//popup.close();
			}
		});
	}
	fErro = function(strErro){
		var str = sapo_replace(",",";<br>",strErro);
		//alert("Por favor preencha os campos:\n"+str);
		frm.style.display = "none";
		boxMsg.innerHTML = "Por favor preencha os campos:<br /><br />"+str+"<br /><a class='bt-padrao' href='javascript:closeMensagem(\"formAlterarSenha\")'>Voltar</a>";
		boxMsg.style.display = "block";	
	}
	validaFormulario('formAlterarSenha',fSucess,fErro)

}

var blkAtual = 0;
function slideGal(inId,acao){
    if(blkAtual != inId){
        $("#blk_"+blkAtual+" .body").slideUp();
        $("#blk_"+blkAtual+" .head").slideDown();
    }
    //alert(inId+" * "+blkAtual);
    blkAtual = inId;
    switch(acao){
        case 'm':
            $("#blk_"+inId+" .head").slideUp();
            $("#blk_"+inId+" .body").slideDown();
        break;
        case 'e':
            $("#blk_"+inId+" .body").slideUp();
            $("#blk_"+inId+" .head").slideDown();
        break;
    }    
}

function buscaConteudoHome(contSecao,idioma,idSubSecao){
     var idSecao = document.formulario.idSecao.value;
    var url = "conteudo_controle.php?acao=buscaConteudoHome&idioma="+idioma+"&cont_secao="+contSecao+"&cont_subSecao="+idSubSecao+"&idSecao="+idSecao;
    $.getJSON(url,function(data,status){
        if(data.error != "0"){
            alerta(data.msg);
        }else{
            $("#cont_id").html('<option value="">Selecione</option>');
            $.each(data.ids, function(i,obj)
            {
                $("#cont_id").append('<option value="'+obj.id+'">'+obj.titulo+'</option>');
            });
        }
    });   
}


function selecionaConteudoHome(contId,idioma){
    var idSecao = document.formulario.idSecao.value;
    var idDestaque = document.formulario.idDestaque.value;
    var contSecao = document.formulario.cont_secao.value;
    var url = "conteudo_controle.php?acao=selecionaConteudoHome&idSecao="+idSecao+"&cont_secao="+contSecao+"&cont_id="+contId+"&idDestaque="+idDestaque+"&idioma="+idioma;
    
    $.getJSON(url,function(data,status){
        if(data.error != "0"){
            alerta(data.msg);
        }else{
            if($("#titulo").attr('type') == "text"){
                $("#titulo").val(data.titulo);                
            }else{
                $("#titulo").val($("#cont_secao option:selected").text());                
            }
            //alert($("#cont_secao").text())
           // alert($("#texto").attr('class'))
            if($("#texto").attr('class') == "ckeditor"){
                setTextEditorParent("texto",decodeURIComponent(data.texto));
                //CKEDITOR.instances.texto.setData(data.texto);
            }else{
               $("#texto").val(data.texto); 
            }
            
            $("#urlImg").val(data.urlImg);
            $("#dirImg").val(data.dirImg);
            $("#srcImg").attr("src",data.srcImg);
            $("#urlLink").val(data.urlLink);
            //$("#targetLink").val("_self");            
        }
    });    
}

function excluirChamadaHome(id){
    var url = "conteudo_controle.php?acao=excluirChamadasHome&id="+id;
    confirma("Deseja realmente excluir este conteúdo?",function(){
        $.getJSON(url,function(data,status){        
            if(data.error != "0"){
                alerta(data.msg);
            }else{
                $("#ch_"+id).remove();
            }
        });  
    })
}

function excluirMedia(id,urlRetorno){
    if(id){
	if(!urlRetorno)	urlRetorno = "edicao-media.php";
	confirma("Deseja realmente excluir esta imagem?",function(){
            document.location = "media_controle.php?acao=excluirImg&id="+id+"&urlRetorno="+encodeURIComponent(urlRetorno);
	})                    
    }
}

function excluirCotacao(id,urlRetorno){
    if(id){
	if(!urlRetorno)	urlRetorno = "edicao-media.php";
	confirma("Deseja realmente excluir este item?",function(){
            var url = "conteudo_controle.php?acao=excluirCotacao&id="+id+"&urlRetorno="+encodeURIComponent(urlRetorno);
            $.getJSON(url,function(data,status){        
                if(data.error != "0"){
                    alerta(data.msg);
                }else{
                    $("#ch_"+id).remove();
                }
            });  
	})                         
    }
}
function excluirUnidadeProd(id,urlRetorno){
    if(id){
	confirma("Deseja realmente excluir este item?",function(){
            var url = "conteudo_controle.php?acao=excluirUnidadeProd&id="+id+"&urlRetorno="+encodeURIComponent(urlRetorno);
            $.getJSON(url,function(data,status){        
                if(data.error != "0"){
                    alerta(data.msg);
                }else{
                    $("#ch_"+id).remove();
                }
            });  
	})                         
    }
}

function uploadMedia(frm,onUpload){
    if(UploadByFrame.status != UploadByFrame.EM_ANDAMENTO){
        UploadByFrame.status = UploadByFrame.EM_ANDAMENTO;
        //alert(destino_frm);
        var onLoad = function(res){
            //alert(res)
            UploadByFrame.res = res;
            UploadByFrame.status = UploadByFrame.CONCLUIDO;
            //UploadByFrame.campo = campo;
            if(onUpload) onUpload(res);
        }
        UploadByFrame.upLoad(frm, {'onStart' : function(){},'onComplete' : onLoad})
    }
}

function editarMedia(id){
    if(id){
        var frm = document.formulario
        frm.id.value = id;
        frm.action = "edicao-media.php";
        frm.submit();
    }
}

function salvarAssunto(){
    var nome = $("#nome").val();
    var idioma = $("#idioma").val();
    var tipo = $("#tipo").val();
    var idSecao = $("#idSecao").val();
	var idCat = $("#idCat").val();
    if(!nome){ alerta("Preencha corretamente o campo nome!"); return;}
    var url = "conteudo_controle.php?acao=salvarAssunto&idioma="+idioma+"&nome="+encodeURIComponent(nome)+"&tipo="+tipo+"&idSecao="+idSecao+"&idCat="+idCat;
    $.getJSON(url,function(data){     
        if(data.erro){
            alerta(data.msg);
        }else{
            //pop.close();
            //document.location.reload();
            popup.close();
            if(document.formulario.assunto){
                var objSelect = document.formulario.assunto;
                if(idCat){                    
                    for(var i=0;i<objSelect.options.length;i++){
                        if(objSelect.options[i].value == idCat) objSelect.options[i].text = nome;
                    }
                }else{
                    addOption(nome,data.id,objSelect)
                }                 
            }               
        }
    });      
}

function salvarTag(){
    var nome = $("#nome").val();
    var idioma = $("#idioma").val();
    var idSecao = $("#idSecao").val();
    var idCat = $("#idCat").val();
    if(!nome){ alerta("Preencha corretamente o campo nome!"); return;}
    var url = "conteudo_controle.php?acao=salvarTag&idioma="+idioma+"&nome="+encodeURIComponent(nome)+"&idSecao="+idSecao+"&idCat="+idCat;
    $.getJSON(url,function(data){        
        if(data.erro){
            alerta(data.msg);
        }else{
            popup.close();
            //if(!idCat && document.formulario.tags) addOption(nome,data.id,document.formulario.tags)
            if(document.formulario.tags){
                var objSelect = document.formulario.tags;
                var objSelect2 = document.formulario.tags_novas;
                if(idCat){                    
                    for(var i=0;i<objSelect.options.length;i++){
                        if(objSelect.options[i].value == idCat) objSelect.options[i].text = nome;
                    }
                    if(objSelect2)
                    for(var i=0;i<objSelect2.options.length;i++){
                        if(objSelect2.options[i].value == idCat) objSelect2.options[i].text = nome;
                    }
                }else{
                    addOption(nome,data.id,objSelect)
                }
            }     
        }
    });      
}
function salvarCatCotacao(){
    var nome = $("#nome").val();
    var idioma = $("#idioma").val();
    var idCat = $("#idCat").val();
    if(!nome){ alerta("Preencha corretamente o campo nome!"); return;}
    var url = "conteudo_controle.php?acao=salvarCatCotacao&idCat="+idCat+"&nome="+encodeURIComponent(nome)+"&idioma="+idioma;
    $.get(url,function(data){        
        if(data != "ok"){
            alerta(data);
        }else{
            //pop.close();
            document.location.reload();
        }
    });      
}

function editarCategoria(id,nome){
    if(id){
        $("#idCat").val(id)
        $("#nome").val(nome);
    }
}

function excluirCategoria(id,tipo){
    if(id){
        switch(tipo){
            case "cotacao":
                var url = "conteudo_controle.php?acao=excluirCategoria&id="+id;
            break;
            case "tags":
                var url = "conteudo_controle.php?acao=excluirTag&id="+id;
            break;
            case "assunto":
                var url = "conteudo_controle.php?acao=excluirAssunto&id="+id;
            break;
        }
       
        $.get(url,function(data){        
            if(data != "ok"){
                alerta(data);
            }else{
                var objSelect = null;
                var objSelect2 = null;
                if(tipo == "tags"){
                    objSelect = document.formulario.tags;
                    objSelect2 = document.formulario.tags_novas;
                } 
                if(tipo == "assunto"){
                    objSelect = document.formulario.assunto;
                }
                if(objSelect || objSelect2){
                    for(var i=0;i<objSelect.options.length;i++){
                        if(objSelect.options[i].value == id) objSelect.remove(i)
                    }
                    if(objSelect2)
                    for(var i=0;i<objSelect2.options.length;i++){
                        if(objSelect2.options[i].value == id) objSelect2.remove(i)
                    }
                    $("#item_pop_cat_"+id).remove();
                    popup.close();
                }else{
                    document.location.reload();
                } 
            }
        });  
    }
}

function adicionar(idCont){
    var tags = document.formulario.tags;
    var tagsNovas = document.formulario.tags_novas;
	var idSecao = document.formulario.idSecao.value;
    if(tags.selectedIndex != -1){
		var keywords = "";
        for(i=0;i<tags.length;i++){
            var repetido = false;
            for(j=0;j<tagsNovas.length;j++){
                    if(tagsNovas[j].value == tags[i].value){
                            repetido = true;
                            break;
                    }
            }
            if(repetido){
                    continue;
            }
            if(tags[i].selected){
                var nova_opcao = document.createElement("option");
                var texto = document.createTextNode(tags[i].text); 
                nova_opcao.setAttribute("value",tags[i].value); 
                nova_opcao.appendChild(texto); 
                tagsNovas.appendChild(nova_opcao);
                /*keywords += keywords?",":"";
                keywords += tags[i].value;*/
                //listarContRelacionado(tags[i].value,idCont);
            }
        }
        listarContRelacionado(idCont,idSecao);
    }
}

function remover(id){
	var tagsNovas = document.formulario.tags_novas;
	//var id = document.formulario.id.value;
	var idSecao = document.formulario.idSecao.value;
	if(tagsNovas.selectedIndex != -1){
            tagsNovas.remove(tagsNovas.selectedIndex);     
				
	}
        listarContRelacionado(id,idSecao);
}
var statusListaContRel = "Concluido";
function listarContRelacionado(idCont,idSecao){
    if(statusListaContRel != "Concluido") return;
    statusListaContRel = "Processando";
    var frm = document.formulario;
    var keywords = "";
    for(var j=0;j < frm.tags_novas.length;j++){
        if(j > 0) keywords += ",";
        keywords += frm.tags_novas[j].text;
    }
    if(keywords){
        var url = "conteudo_controle.php?acao=listarContRelacionado&keywords="+keywords+"&idSecao="+idSecao+"&idioma="+frm.idioma.value;
        var arr = new Array();
        var matsRelAnt = [];
        $.get(url,function(data){
            //alert(data)
            if(data){
                arr = data.split("[;]");
                $(".fieldset INPUT").each(function(){
                    //alert(this.value)
                    if(this.checked){
                        matsRelAnt.push(this.value);
                    }
                });
                var showLista = false;
                $(".fieldset").html("<legend><strong>Noticias Relacionadas:</strong></legend>")
                for(i=0;i<arr.length;i++){                    
                    if(arr[i]){	
                        var check = arr[i].split("[,]");
                        //alert($(".fieldset").val())
                        var checkado = false;
                        for(var j=0;j<matsRelAnt.length;j++){
                            //alert(check[0] +" != "+ matsRelAnt[j])
                            if(check[0] == matsRelAnt[j]) checkado = true;
                        }
                        if(checkado) htmlChec = "checked='checked'";
                        else htmlChec = ""; 
                        if(idCont != check[0]){
                            var texto = "<b><input type='checkbox' "+htmlChec+" name='nr"+check[0]+"' id='nr"+check[0]+"' value='"+check[0]+"'>"+check[1]+"</b>";                                                     
                            $(".fieldset").append(texto);
                            showLista = true;
                        }                        
                    }
                }
                if(showLista){
                    $(".fieldset").css("display","block");
                }
                statusListaContRel = "Concluido";
            }
        })
    }else{
        $(".fieldset").html("").css("display","none")
        statusListaContRel = "Concluido";
        
    }
    
}