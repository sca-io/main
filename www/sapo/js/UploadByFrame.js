UploadByFrame = {
	/*idFrame : 'frame_upload',*/
        EM_ANDAMENTO:'2',
        CONCLUIDO : '1',
        ERROR : '0',
	frame : function(funcs) {
		if(document.getElementById('frame_upload')){
			
		}else{
			var div = document.createElement('DIV');
			div.innerHTML = '<iframe style="display:none" src="about:blank" id="frame_upload" name="frame_upload" onload="UploadByFrame.onLoad(\'frame_upload\')"></iframe>';
			document.body.appendChild(div);
		}
		var frame = document.getElementById('frame_upload');
		if (funcs && typeof(funcs.onComplete) == 'function') {
			frame.onComplete = funcs.onComplete;
		}
		return 'frame_upload';
	},

	upLoad : function(frm, funcs) {
		frm.target = UploadByFrame.frame(funcs);
		frm.submit();
		if (funcs && typeof(funcs.onStart) == 'function') {
			return funcs.onStart();
		} else {
			return true;
		}
	},

	onLoad : function(id) {
		var frame = document.getElementById(id);
		if (frame.contentDocument) {
			var doc = frame.contentDocument;
		} else if (frame.contentWindow) {
			var doc = frame.contentWindow.document;
		} else {
			var doc = window.frames[id].document;
		}
		if (doc.location.href == "about:blank") {
			return;
		}
		if (typeof(frame.onComplete) == 'function') {
			frame.onComplete(doc.body.innerHTML);			
		}
		/*var pai = frame.parentNode;		
		pai.removeChild(frame);*/
	}

}