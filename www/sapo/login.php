<? require_once('conf.php');

$url = request('url')?request('url'):'index.php';
$acao = request('acao');
if($acao == 'logof'){
    $controleAcesso->logoutUser();  
}elseif($acao == 'logar'){
    $usuario = request('usuario');
    $senha = request('senha');
    $controleAcesso->logonUser($usuario,$senha,$url);
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? include "includes/head.php";?>
<script>
    var msg = Array();
    var vld = Array();
    var destino_frm = "?acao=logar";
    var target_frm = "";
    msg['usuario'] = "Login";
    vld['usuario'] = 1;

    msg['senha'] = "Senha";
    vld['senha'] = 1;
	
    function enviaLogin(idForm){
        funcS = function(){
                document.forms[idForm].submit();
        }

        funcE = function(str){
                var resp = "Por favor preencha os seguintes campos:<br /><br />"+sapo_replace(",","<br />",str);
                alerta(resp);
        }
        validaFormulario(idForm,funcS,funcE);
    }
</script>
</head>
<body>
	<div id="sapo">
		<div class="container">
		  <? include "includes/topo.php";?>
          <div class="login">
              <div>
                  <div>
                      <p>ÁREA DE AUTENTICAÇÃO DE USUÁRIO PARA ÁREA ADMINISTRATIVA</p>
                      <img src="css/img/logo-atomica-login.gif" width="87" height="86" />
                      <span>
                          <form name='formulario' id='formulario' method="post" action="?acao=logar">
                              <p>login:<br /><input type="text" name="usuario" onkeypress="checarTecla(event,function(){enviaLogin('formulario');})"/></p>
                              <p>Senha:<br /><input name="senha" type="password" autocomplete="off" onkeypress="checarTecla(event,function(){enviaLogin('formulario');})"/></p>
                          </form>
                          <a href="javascript:enviaLogin('formulario')">Login</a>
                      </span>
                  </div>
              </div>
          </div>          
		</div>
    </div>
</body>
</html>