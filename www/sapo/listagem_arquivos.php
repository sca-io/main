<? require_once("conf.php");
Usuario::autenticarLogon();

$id = secureRequest("id");
$versao = secureRequest("versao");
$idSecao = request("idSecao");

$urlRetorno = "listagem_arquivos.php?idSecao=$idSecao&id=$id";
$indexAbaEdicao = 1;

$secao = Secao::ler($idSecao);
$settings = null;
if($secao && $secao->getIdSecao()){
    $settings = getSettings($secao->getIdSecao(),$idSecao);
}else{
    $settings = getSettings($idSecao);
}

$idioma = request("idioma")?request("idioma"):LNG_PT;
$where = "idioma = $idioma";

$obj = null;
$idVcont = "";

$camposTable = "id,id_conteudo,titulo,idioma,descricao,keywords,autor,texto,img_destaque,url_embed,status";
$where = "status != ".VersaoConteudo::INATIVO." AND $where";
$listaConts = VersaoConteudo::listar($idSecao,$id,$camposTable,$where,"","1");
//print VersaoConteudo::getLogSql();
if($listaConts){
    $obj = $listaConts[0];
    $idVcont = $obj->getId();
    $id = $obj->getIdConteudo();
}

if($obj){  
    $titulo = $obj->getTitulo();
    $descricao = $obj->getDescricao();
    $keywords = $obj->getKeywords();
    $texto = $obj->getTexto();
    $idioma = $obj->getIdioma();
    $urlEmbed = $obj->getUrlEmbed();
    $status = $obj->getStatus();

}else{
    $titulo = "";
    $descricao = "";
    $keywords = "";
    $texto = "";    
    $urlEmbed = "";
    $status = ""; 
}

$urlCancelar = isset($_SERVER["HTTP_REFERER"])?$_SERVER["HTTP_REFERER"]:"";
$matchStr = explode("/",$urlCancelar);
if(indexOf($matchStr[count($matchStr)-1],"pop-up_mensagem.php")!=-1){
    $urlCancelar = "listagem_conteudo.php";
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? include "includes/head.php";?>
<link rel="stylesheet" type="text/css" href="css/edicao-conteudo.css"/>
<script type="text/javascript">
PHP_FUNCTION = function(){
    <? /*openPop('pop-media.php?parametros=<?=urlencode("&idConteudo=".secureResponse($id)."&idSecao=".$idSecao)?>'); */?>
    openPop('pop-media.php?parametros=<?=urlencode("&urlRetorno=".urlencode($urlRetorno))?>');
}
CK_HAS_LINK = true;
CK_HAS_IMAGE = false;
CK_W = '640px';
CK_H = '200px';
</script>
<script type="text/javascript" src="Fckeditor/ckeditor.js"></script>
<script type="text/javascript" src="Fckeditor/config.js"></script>
<script type="text/javascript">
function salvar(){
    var frm = document.formulario;
    msg = [];
    vld = [];

    msg['texto'] = "Texto"; vld['texto'] = 1;
    msg['idioma'] = "Idioma"; vld['idioma'] = 1;
    msg['status'] = "Publicar"; vld['status'] = 1;       
    frm.texto.value = getTextEditorParent("texto");
    fSucess = function(url){
        frm.action = "conteudo_controle.php";
        frm.acao.value = "salvarConteudo";        
        frm.submit();
        $("#btSalvar").html("aguarde...").attr("href","#");
    }
    fErro = function(strErro){
        var str = sapo_replace(",",";<br/>",strErro);
        alerta("<b>Por favor preencha os campos:</b><br/>"+str);
        enviando = 0;	
    }
    validaFormulario('formulario',fSucess,fErro);
}

function changeIdioma(){
   var frm = document.formulario;
   //frm.id.value = "";
   frm.submit()    
}

function openPopGaleria(idCont,idSecao){
    openPop('pop-media.php?parametros=<?=urlencode("&tipo=".Media::DOCS."&idioma=$idioma&urlRetorno=".urlencode($urlRetorno)."&idConteudo=")?>'+idCont+'<?=urlencode("&idSecao=")?>'+idSecao);    
}

/*function openPopGaleria(idCont){
    openPop('pop-media.php?parametros=<?=urlencode("&tipo=".Media::DOCS."&idioma=$idioma&idSecao=".$idSecao."&urlRetorno=".urlencode($urlRetorno)."&idConteudo=")?>'+idCont);    
}*/


</script>
</head>
<body>
    <div id="sapo">
        <div class="container">
          <? include "includes/topo.php";?>          
          <? include "includes/menu-lateral.php";?>          
          <div class="coluna-geral">
          	<? include "includes/topo_int.php";?>
            <div class="modulo-geral">
            	<form name="formulario" id="formulario" method="post" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="<?=secureResponse($id)?>" />
                    <input type="hidden" id="acao" name="acao" value="" />
                    <input type="hidden" id="idSecao" name="idSecao" value="<?=$idSecao?>" />
                    <input type="hidden" id="urlRetorno" name="urlRetorno" value="<?=$urlRetorno?>" />
                    <input type="hidden" id="tipo" name="status" value="<?=Chamada::PUBLICADO?>" />
                  <div class="esq">
                      <p>
                      	<strong>Idioma</strong>
                        <select id='idioma' name='idioma' onchange="changeIdioma()">
                            <option value=''>Selecione</option>
                            <?	
                                $std = Array("","","");
				$std[$idioma] = "selected";
                            ?>
                            <option value='<?=LNG_PT?>' <?=$std[LNG_PT]?>><?=$vetIdioma[LNG_PT]?></option>
                            <option value='<?=LNG_EN?>' <?=$std[LNG_EN]?>><?=$vetIdioma[LNG_EN]?></option>        
                        </select>
                      </p>

                      <p><strong>Texto</strong><textarea class="ckeditor" id="texto" name="texto" ><?=stripslashes($texto)?></textarea></p>                      
                   
                      <a href="javascript:salvar();" class="bt-padrao" title="Salvar" id="btSalvar">Salvar</a>
					  
                      <? if(isset($settings["sub_secoes"])):?>
                      <p>&nbsp;</p>    
                      <a href="javascript:openPopGaleria('','<?=(MERCADO_INTERNO)?>');" class="bt-padrao" title="Lista" id="btSalvar">Arquivos de Mercado Interno</a><br /><br />
                      <a href="javascript:openPopGaleria('','<?=(MERCADO_EXTERNO)?>');" class="bt-padrao" title="Lista" id="btSalvar">Arquivos de Mercado Externo</a>

                      <? else:?>
                        <? if($idVcont):?><a href="javascript:openPopGaleria('<?=secureResponse($idVcont)?>','<?=$idSecao?>');" class="bt-padrao" title="Lista" id="btSalvar">Lista de Arquivos</a><? endif;?>
                      <? endif; ?>
                       
                  </div>
                </form>
            </div>
          </div>          
        </div>
    </div>
  </div>
</div>
</body>
</html>