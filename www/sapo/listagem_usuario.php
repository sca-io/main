<?
include("conf.php");
$idSecao = USUARIOS;
$acao = request('acao');

$url = "listagem_usuario.php?1=1";

if($acao == 'excluir'){
    $id = request('id');
   Usuario::deletar($id);
   print "<script>alert('Usuário excluído com sucesso');document.location = 'listagem_usuario.php';</script>";
}

$where = "status != ".INATIVO;

$busca = request('busca');
$letra = request('letra');
if($busca != ""){
	if($where != ""){
		$where .= " AND ";
	}
	$where .= "login like '%$busca%'";
	$url .= "&busca=$busca";
}
$qLetra = "";
if($letra != ""){
	if($where != ""){
		$where .= " AND ";
	}
	$where .= "login like '$letra%'";
	$qLetra .= "&letra=$letra";
}

$ncadastro = Usuario::countListar("",$where);
//print Usuario::getLogSql();

$pagina = isset($_REQUEST['pagina'])?$_REQUEST['pagina']:0;

$total_reg_pag = 10;
$init_reg_pag = ($pagina * $total_reg_pag);	 
$aux_pagina = $pagina + 1;
$total_paginas = $ncadastro/$total_reg_pag;

$by = (isset($_REQUEST['by'])) ? $_REQUEST['by'] : "login";
$ordem = (isset($_REQUEST['ordem'])) ? $_REQUEST['ordem'] : "ASC";
$ordemArr = Array();


$ordemArr[$by] = $ordem;
$ordemArr['login'] = (isset($ordemArr['login']))? $ordemArr['login'] : "ASC";
if($ordemArr[$by] == "DESC") {
    $ordemArr[$by] = "ASC";
}else{
    $ordemArr[$by] = "DESC";
}

$paginacao = new Paginacao($pagina,$ncadastro,$total_reg_pag);
$paginacao->setUrl($url.$qLetra);
$lista = Usuario::listar("","id,nome,login,status",$where," $by $ordem", "$init_reg_pag, $total_reg_pag");
//print Usuario::getLogSql();
$listacbk = $lstLogin = $lstAcao = $listaGeral = $lstTitulo = "";
 if($lista){
	
	 $listaGeral = "";
	foreach($lista as $l){
		//$listacbk = "<td class='checkbox'><input type='checkbox' class='cbkEdicao' value='".secureResponse($l->getId())."' name='selecao".secureResponse($l->getId())."' /></td>";
		$lstLogin = "<td>".$l->getLogin()."</td>";
                $lstStatus = "<td class='acao'>".$l->getStatus(true)."</td>";
		$lstAcao = "<td class='acao'><a href=\"javascript:deletar('".secureResponse($l->getId())."',".USUARIOS.")\" class=\"bt-excluir\">fechar</a><a href=\"frm_usuario.php?id=".secureResponse($l->getId())."\" class=\"bt-editar\">editar</a></td>";
		$listaGeral .= "<tr>$listacbk $lstLogin $lstStatus $lstAcao</tr>";
	}
       if($listacbk) $lstTitulo .= "<th class='checkbox'><input type='checkbox' id='todosCkb' onClick='selecionaTudo(this)'/></th>";
       $lstTitulo .= "<th>Login</th>";
       $lstTitulo .= "<th>Status</th>";
       $lstTitulo .= "<th class='acao'>Ação</th>";
 }
	 
$indexAbaEdicao = 2;
	 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<? include "includes/head.php";?>
    <link rel="stylesheet" type="text/css" href="css/home.css"/>
</head>
<body>
	<div id="sapo">
		<div class="container">
		  <? include "includes/topo.php";?>                    
          <? include "includes/menu-lateral.php";?>
          
          <div class="coluna-geral">
            <? include "includes/topo_int.php";?>
            <div class="modulo-geral home3 lista">              
               <div class="dados">
                	<form name="formulario" id="formulario" method="post">
                		<p>Login<br /><input type="text"  name="busca" value="<?=$busca?>"/></p>
                    </form>
                    <div>
                    <a href="javascript:document.formulario.submit()" class="bt-padrao">Buscar</a>
                    </div>
                </div>
                <h3>Total de registros: <?=$ncadastro?></h3>
                <?
                $alfabeto = new LinksAlfabeto($url);
				$alfabeto->printHtml();
				?>              
                <table class="tabela" cellspacing="0">
                	<thead>
                    	<tr>
                        	<?=$lstTitulo?>
                        </tr>
                    </thead>
                    <tbody>
                    	<?=$listaGeral?>
                    </tbody>
                </table>
                
                <div class="paginacao"><?=$paginacao->printHtml()?></div> 
            </div>
          </div>          
		</div>
    </div>
</body>
</html>