<? require_once("conf.php");
Usuario::autenticarLogon();

$acao = request("acao");
$id = secureRequest("id");
$versao = secureRequest("versao");
$idSecao = request("idSecao");

$idSubSecao = request("idSubSecao");

$urlRetorno = "edicao_conteudo.php?idSecao=$idSecao&id=$id";
$indexAbaEdicao = 1;

$secao = Secao::ler($idSecao);
$settings = null;
if($secao && $secao->getIdSecao()){
    $settings = getSettings($secao->getIdSecao(),$idSecao);
}else{
    $settings = getSettings($idSecao);
}

$idioma = request("idioma")?request("idioma"):LNG_PT;
$where = "idioma = $idioma";
$idVcont = "";
$obj = null;
if($acao != "novo"){
    $where = "status != ".VersaoConteudo::INATIVO." AND $where";  
    $camposTable = "id,id_conteudo,assunto,titulo,idioma,descricao,keywords,autor,texto,img_destaque,url_embed,ids_relacionados,status,data_publicacao";
    $listaConts = VersaoConteudo::listar($idSecao,$id,$camposTable,$where,"","1");
    //print VersaoConteudo::getLogSql();
    if($listaConts){
        $obj = $listaConts[0];
        $idVcont = $obj->getId();
        $id = $obj->getIdConteudo();
    }
}

if($obj){  
    $titulo = $obj->getTitulo();
    $assunto = $obj->getAssunto();
    $descricao = $obj->getDescricao();
    $keywords = $obj->getKeywords();
    $texto = $obj->getTexto();
    $idioma = $obj->getIdioma();
    $urlEmbed = $obj->getUrlEmbed();
    $ids_nr = $obj->getIdsRelacionados();
    $status = $obj->getStatus();
    $dataPublicacao = Util::dataDoBD($obj->getDataPublicacao());
}else{
    $titulo = "";
    $assunto = "";
    $descricao = "";
    $keywords = "";
    $texto = "";    
    $urlEmbed = "";
    $status = ""; 
    $dataPublicacao = "";
    $ids_nr = "";
}

/*** TAGS ***/
$listaNotRel = "";
$arrTags = Array();
if($keywords != "")  $arrTags = explode(", ",$keywords);
$tags = "";
$tagsNovas = "";
$whereNotRel = "";
$listaNotRel = "";
$lstTag = Tags::listar("*","status = ".ATIVO." AND idioma = $idioma");
if($lstTag){
    foreach($lstTag as $lt){
        $nomeTag = $lt->getNome();
        $tags .= "<option value='".$lt->getId()."'>".$nomeTag."</option>";
        if($arrTags && in_array($nomeTag,$arrTags)){
            $tagsNovas .= "<option value='".$lt->getId()."'>".$nomeTag."</option>";
            $whereNotRel .= $whereNotRel?" OR ":"";
            $whereNotRel .= ' keywords LIKE "%'.$nomeTag.'%" ';	
        }
    }
    $whereNotRel = $whereNotRel?"($whereNotRel) ":"";
	
	if($whereNotRel){
		/*** Noticias relacionadas ***/
		
		//if($ids_nr){
		$arrNr = explode(",",$ids_nr);
		$whereNotRel .= $whereNotRel?" AND ":"";
		if($idVcont) $whereNotRel .= ' id != '.$idVcont." AND ";
		$whereNotRel .= 'status = '.ATIVO.' AND idioma = '.$idioma;
		$listaNR = VersaoConteudo::listar($idSecao,'','',$whereNotRel,'data_publicacao DESC');
		//print VersaoConteudo::getLogSql();    


		if($listaNR){
			foreach($listaNR as $lnr){
				if($id != $lnr->getId()){
					if($arrNr){
						if(in_array($lnr->getId(),$arrNr)){
							$checkedNr = "checked='checked'";					
						}else{
							$checkedNr = "";
						}
					}
					$listaNotRel .= "
							<b><input type='checkbox' $checkedNr name='nr".$lnr->getId()."' id='nr".$lnr->getId()."' value='".$lnr->getId()."'>".stripslashes($lnr->getTitulo())."</b>
					";
				}
			}
		}
	}
	//}
}
if(isset($_SERVER["HTTP_REFERER"])){
    $urlCancelar = $_SERVER["HTTP_REFERER"];   
}else{
    $urlCancelar = "listagem_conteudo.php?idSecao=$idSecao";
}
$matchStr = explode("/",$urlCancelar);
if(indexOf($matchStr[count($matchStr)-1],"pop-up_mensagem.php")!=-1){
    $urlCancelar = "listagem_conteudo.php?idSecao=$idSecao";
}

if(isset($settings["texto_inicial"]) && $settings["texto_inicial"]) {    
    $indexAba = request("indexAba");
    $indexAba = $indexAba?$indexAba:2;
    $gpAbas = new AbaSuperior();
    $gpAbas->setIndexAba($indexAba);
    $gpAbas->addItem("Listagem",getUrlByTipo($secao->getTipo()."&idioma=$idioma",$idSecao));
    if($titulo) $titAbaTexto = "Texto: $titulo";
    elseif($acao == "novo")  $titAbaTexto = "Novo Texto";
    else $titAbaTexto = "Texto";
    $gpAbas->addItem($titAbaTexto,"edicao_conteudo.php?idSecao=$idSecao&idSubSecao=$idSubSecao&idioma=$idioma&id=".secureResponse($id));
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? include "includes/head.php";?>
<link rel="stylesheet" type="text/css" href="css/edicao-conteudo.css"/>
<script type="text/javascript">
PHP_FUNCTION = function(){
    <? /*openPop('pop-media.php?parametros=<?=urlencode("&idConteudo=".secureResponse($id)."&idSecao=".$idSecao)?>'); */?>
    openPop('pop-media.php?parametros=<?=urlencode("&modoVis=".Media::PUBLICO."&idSecao=$idSecao&urlRetorno=".urlencode($urlRetorno))?>');
}
CK_HAS_LINK = true;
CK_HAS_IMAGE = true;
CK_W = '640px';
CK_H = '500px';
<? if($idSecao == NOTICIAS):?>
CK_CSS = "contents-2.css?v=10";
<? endif;?>    
</script>
<script type="text/javascript" src="Fckeditor/ckeditor.js"></script>
<script type="text/javascript" src="Fckeditor/config.js"></script>
<script type="text/javascript">
function salvar(){
    var frm = document.formulario;
    msg = [];
    vld = [];
    frm.ids_nr.value  = "";
    $(".fieldset INPUT").each(function(){
        if(this.checked){
            if(frm.ids_nr.value.length) frm.ids_nr.value += ",";
            frm.ids_nr.value += this.value;
        }
    });
    if(frm.tags_novas){
        frm.keywords.value = "";
        for(j=0;j < frm.tags_novas.length;j++){
            if(j > 0) frm.keywords.value += ", ";
            frm.keywords.value += frm.tags_novas[j].text;
        }
    }
    if(frm.dataPublicacao){ msg['dataPublicacao'] = "Data"; vld['dataPublicacao'] = 1;}	
    if(frm.assunto){ msg['assunto'] = "Assunto"; vld['assunto'] = 1;}	
    <? if($idSecao == NOTICIAS):?>msg['titulo'] = "Título"; vld['titulo'] = 1;<? endif;?>
    /*msg['descricao'] = "Descrição"; vld['descricao'] = 1;
    msg['keywords'] = "Keywords"; vld['keywords'] = 1;*/
    if(frm.texto.className == "ckeditor") frm.texto.value = getTextEditorParent("texto");
    msg['texto'] = "Texto"; vld['texto'] = 1;
    msg['idioma'] = "Idioma"; vld['idioma'] = 1;
    msg['status'] = "Publicar"; vld['status'] = 1;       
    
    fSucess = function(url){
        frm.action = "conteudo_controle.php";
        frm.acao.value = "salvarConteudo";        
        frm.submit();
        $("#btSalvar").html("aguarde...").attr("href","#");
    }
    fErro = function(strErro){
        var str = sapo_replace(",",";<br/>",strErro);
        alerta("<b>Por favor preencha os campos:</b><br/>"+str);
        enviando = 0;	
    }
    validaFormulario('formulario',fSucess,fErro);
}

function changeIdioma(){
	<? $paramAcao = ($acao)?"&acao=$acao":"";?>
	var frm = document.formulario;
	document.location = '<?=$urlRetorno.$paramAcao."&idioma="?>'+frm.idioma.value
   //var frm = document.formulario;
   //frm.id.value = "";
   //frm.submit()    
}
function openPopGaleria(idCont){
    openPop('pop-media.php?parametros=<?=urlencode("&tipo=".Media::DOCS."&idioma=$idioma&idSecao=".$idSecao."&urlRetorno=".urlencode($urlRetorno)."&idConteudo=")?>'+idCont);    
}


</script>
</head>
<body>
   
    
	<div id="sapo">
		<div class="container">
		  <? include "includes/topo.php";?>          
          <? include "includes/menu-lateral.php";?>          
          <div class="coluna-geral">
          	<? include "includes/topo_int.php";?>
            <div class="modulo-geral">
            	<form name="formulario" id="formulario" method="post" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="<?=secureResponse($id)?>" />
                    <input type="hidden" id="acao" name="acao" value="" />
                    <input type="hidden" id="idSecao" name="idSecao" value="<?=$idSecao?>" />
                    <input type="hidden" id="urlRetorno" name="urlRetorno" value="<?=$urlRetorno?>" />
                    <input type="hidden" id="ids_nr" name="ids_nr" value="<?=$ids_nr?>" />
                    <input type="hidden" id="versao" name="versao" value="<?=secureResponse($versao)?>" />
                    <? if($idSecao == NOTICIAS):?><input type="hidden" id="keywords" name="keywords" value="<?=$keywords?>" /><? endif;?>
                  <div class="esq">
                      <p>
                      	<strong>Idioma</strong>
                        <select id='idioma' name='idioma' onchange="changeIdioma()">
                            <option value=''>Selecione</option>
                            <?	
                                $std = Array("","","");
				$std[$idioma] = "selected";
                            ?>
                            <option value='<?=LNG_PT?>' <?=$std[LNG_PT]?>><?=$vetIdioma[LNG_PT]?></option>
                            <option value='<?=LNG_EN?>' <?=$std[LNG_EN]?>><?=$vetIdioma[LNG_EN]?></option>        
                        </select>
                      </p>

                      <? if(isset($settings["filtro_por_assunto"]) && $settings["filtro_por_assunto"]):?>
                          <p class="select">
                            <strong>Assunto</strong>
                            <select id='assunto' name='assunto'>
                                <option value=''>Selecione</option>
                                <?
                                $listaAs = Assunto::listar($idSecao,"id,nome", "status = ".ATIVO." AND idioma = $idioma", "data DESC");  
                                foreach($listaAs as $as):
                                    $nomeAs = $as->getNome();
                                    $selectedAs = $assunto == $as->getId()?" selected=\"selected\"":"";                           
                                ?>
                                <option value="<?=$as->getId()?>" <?=$selectedAs?>><?=$nomeAs?></option>
                                <? endforeach;?>
                            </select>
                            <a href="javascript:openPop('pop-categoria.php?idSecao=<?=$idSecao?>&idioma=<?=$idioma?>')" class="bt-padrao">Inserir nova</a>
                          </p>
                      <? endif;?>    
                      <? if($idSecao == NOTICIAS):?><p><strong>Data de publicação</strong><input type="text"  name="dataPublicacao" value="<?=$dataPublicacao?>" readonly="readonly" onclick="showInputCalendar(this)" class="peq"/></p><? endif;?>
                      <p><strong>Título</strong><textarea class="inputText" id="titulo" name="titulo" ><?=$titulo?></textarea></p>
                      <? if(!isset($settings["filtro_por_assunto"]) || (isset($settings["filtro_por_assunto"]) && !$settings["filtro_por_assunto"])):?>
                          <p><strong>Descrição(Para efeito de SEO)</strong><textarea class="inputText" id="descricao" name="descricao" ><?=$descricao?></textarea></p>
                          <? if($idSecao != NOTICIAS):?><p><strong>Keywords(Para efeito de SEO)</strong><textarea class="inputText" id="keywords" name="keywords" ><?=$keywords?></textarea></p><? endif;?>
                      <? endif;?>
                      <p><strong>Texto</strong><textarea class="ckeditor" id="texto" name="texto" ><?=stripslashes($texto)?></textarea></p>                      
                      <? /*<p><strong>Embed de flash</strong><textarea class="inputText" id="urlEmbed" name="urlEmbed" ><?=$urlEmbed?></textarea></p>*/ ?>

                      <? if($idSecao == NOTICIAS):?>
                        <p class="listbox">
                           <strong>Palavras-Chave</strong>
                            <select multiple="multiple" name="tags" id="tags"><?=$tags?></select>
                            <i>
                                <a href="javascript:adicionar(<?=$idVcont?>)" class="bt-padrao">»</a>
                                <a href="javascript:remover(<?=$idVcont?>)" class="bt-padrao">«</a>
                            </i>
                            <select multiple="multiple" name="tags_novas"  id="tags_novas"><?=$tagsNovas?></select>                            
                        </p> 
                        <p><a class="bt-padrao" href="javascript:openPop('pop-tags.php?idSecao=<?=$idSecao?>&idioma=<?=$idioma?>')" title="Adicionar nova Palavras-Chave">Nova Palavra-chave</a></p>
                        <p>
                            <fieldset class="fieldset" id="cont_relacionado" <? if(!$listaNotRel):?> style="display: none"<? endif;?>>                                
                                <legend><strong>Noticias Relacionadas:</strong></legend>
                                <?=$listaNotRel?>                                
                            </fieldset>
                        </p>
                        
                      <? endif;?>
                      
                      <p>
                      	<strong>Publicar</strong>
                        <select id='status' name='status'>
                            <option value=''>Selecione</option>
                            <?	 $std = Array("","","");	$std[$status] = "selected";?>
                            <option value='<?=VersaoConteudo::PUBLICADO?>' <?=$std[VersaoConteudo::PUBLICADO]?>>Sim</option>
                            <option value='<?=VersaoConteudo::SALVO?>' <?=$std[VersaoConteudo::SALVO]?>>Não</option>        
                        </select>
                      </p>
                      
                      <a href="<?=$urlCancelar?>" class="bt-padrao" title="Cancelar">Cancelar</a>
                      <a href="javascript:salvar();" class="bt-padrao" title="Salvar" id="btSalvar">Salvar</a>

                  </div>
                </form>

            </div>
          </div>          
        </div>
    </div>
  </div>
</div>
</body>
</html>