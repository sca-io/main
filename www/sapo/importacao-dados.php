<?
require_once("conf.php");
header("Content-type: text/html; charset=UTF-8");

/*$controleAcesso->addWhiteListReferer(array(
    "sapo/edicao_home.php"
));
$controleAcesso->autenticarReferer();*/
Usuario::autenticarLogon();

$id = secureRequest('id');
$idSecao = request('idSecao');
$idSecao = $idSecao?$idSecao:HOME;
$acao = request('acao');
$indexAba = request('indexAba');
$urlRetorno = request('urlRetorno');
$idioma = request("idioma");
$idioma = $idioma?$idioma:LNG_PT;
$idDestaque = request('idDestaque');


$urlArqModelo = DIRETORIO_RAIZ.PATH_DOCS."importacao/cotacoes/modelo-simples.xls";
if($idDestaque == 4) $urlArqModelo = DIRETORIO_RAIZ.PATH_DOCS."importacao/cotacoes/modelo-com-categoria.xls";
$callback = "";

$vld = array();
$val = array();
$msg = array();
$minLen = array();
$maxlen = array();
$settings = null;
if(isset($settModulosDeHome[$idSecao][$idDestaque])){
    $settings = $settModulosDeHome[$idSecao][$idDestaque];
}

function esc_valor_excel($valor){ 
    $valor = limpaSQLInjection($valor);
    $valor = limpaXSS($valor);	
    $valor = strip_tags($valor,"<p><b><strong><em><i><a><img><br><h1><h2><h3>");
    $valor = addslashes($valor);
    return $valor;
}
function addErroVal($texto){
    global $errosVal;
    if(!inArray($texto,$errosVal)) $errosVal[] = $texto;
}
$strResposta = "";
$errosVal = array();
$mostrarBotaoModelo = true;
if($acao == 'importarDadosCotacao'){    
    $urlMedia = request('urlMedia');
    if($urlMedia){
        //print $urlMedia;
        $dadosExcel = ControleArquivo::getDadosDeExcel($urlMedia);
        if(!$dadosExcel["error"]){
            //print_r($dadosExcel["dados"]);
            /*** VALIDACAO Headers ***/
            if($idDestaque == 4){
                $headerValidos = array("A"=>"titulo","B"=>"valor","C"=>"percentual","D"=>"categoria");
            }else{
                $headerValidos = array("A"=>"titulo","B"=>"periodo","C"=>"valor","D"=>"percentual");
            }
            
           
            $headersIneEx = array();
            $headersOrdemOK = true;
            $headersExsValidos = array();
            foreach($headerValidos as $key1=>$head1){
                foreach($dadosExcel["headers"] as $key2=>$head2){
                    if(strtolower($head2) == $head1){
                        $headersExsValidos[] = $head1;
                        if($key1 != $key2) $headersOrdemOK = false;
                    }
                }                
            }
            
            foreach($headerValidos as $key1=>$head1){
                if(!in_array($head1,$headersExsValidos)) $headersIneEx[] = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&bull;&nbsp;".$head1;

            }

            if(count($headersIneEx)){
                addErroVal("- Os seguintes campos estão ausentes na planilha:<br />".implode("<br />",$headersIneEx));
            }else if(!$headersOrdemOK){
                addErroVal("- A ordem do elementos do cabeçalho esta diferente da configurada no modelo!");
            }
            
            if(isset($settings["limite_chamadas"])){
                if(count($dadosExcel["dados"]) > $settings["limite_chamadas"]){
                    addErroVal("- O numero de registro a ser importado supera o limite, que é de :".$settings["limite_chamadas"]);
                }
            }

            
            /*** VALIDACAO valores ***/            
            if(count($errosVal) == 0){               
                
                foreach($dadosExcel["dados"] as $dado){
                     if(count($dado)>0){
                         $camposValidos = array();

                         /*** CATEGORIA ***/
                         if($idDestaque == 4){
                             $camposValidos["categoria"] = true;
                             if(!isset($dado["categoria"]) || $dado["categoria"] == "" ){
                                 $camposValidos["categoria"] = false;
                                 addErroVal("- Não há valor para o campo <strong>categoria</strong>!");
                             }
                             if($dado["categoria"] == ""){
                                 addErroVal("- A coluna <strong>categoria</strong> possui valores em branco!");
                             }
                             
                         }

                         /*** PERIODO ***/    
                         if(isset($dado["periodo"])){
                             $camposValidos["periodo"] = true;
                             $varAux = trim($dado["periodo"]);
                             if($varAux != ""){
                                 $arrAux = array();
                                 $varAux = str_replace(" ","",$varAux);
                                 if(indexOf($varAux,"-") != -1)   $arrAux = explode("-",$varAux);
                                 elseif(indexOf($varAux,"/") != -1)   $arrAux = explode("/",$varAux);
                                 if(count($arrAux) == 3){ 
								 	// dd/mm/yyyy
                                     $arrAux[0] = (int)$arrAux[0];
                                     //$arrAux[1] = (int)$arrAux[1];
                                     $arrAux[2] = (int)$arrAux[2];                              
                                     if(is_int($arrAux[0]) && $arrAux[1] && is_int($arrAux[2])){
                                         if(!($arrAux[0] > 0 && $arrAux[0] < 32)) $camposValidos["periodo"] = false;
                                         if(!in_array($arrAux[1],$arr_mes_pq_ing) && !in_array($arrAux[1],$arr_mes_pq)){
											 $arrAux[1] = (int)$arrAux[1];
											 $valMes = ($arrAux[1] > 0 && $arrAux[1] < 13);
											 if(!$valMes){
												 $camposValidos["periodo"] = false; 
											 }
										 }
                                     }else{
                                         $camposValidos["periodo"] = false;
                                     }  
									 /*if(!$camposValidos["periodo"]) {
										 print_r($arrAux) ;        
									 	exit;
									 }*/
                                 }elseif(count($arrAux) == 2){									 
                                     if($arrAux[0] != "" && (in_array($arrAux[0],$arr_mes_pq_ing) || in_array($arrAux[0],$arr_mes_pq))){
										 // mm/yyyy
										 if($arrAux[1] == "" || !is_int((int) $arrAux[1])){
											 $camposValidos["periodo"] = false;
										 }
									 }elseif($arrAux[1] != "" && (in_array($arrAux[1],$arr_mes_pq_ing) || in_array($arrAux[1],$arr_mes_pq))){
										 // dd/mm
										 if($arrAux[0] == "" || !is_int((int) $arrAux[0])){
											 $camposValidos["periodo"] = false;
										 }										 
                                     }else{
										$camposValidos["periodo"] = false;	 
									 }  
								 	 
                       

                                 }else{
                                     $camposValidos["periodo"] = false;
                                 }
                                 if(!$camposValidos["periodo"])  addErroVal("- A coluna <strong>período</strong> possui um valor incorreto!");
                             }else{
                                 addErroVal("- A coluna <strong>período</strong> possui valores em branco!");                             
                             }
                         }

                         /*** VALOR ***/
                         $camposValidos["valor"] = true;
                         $varAux = trim($dado["valor"]);
                         if($varAux != ""){
                             
                             $varAux = str_replace(" ","",$varAux);
                             $varAux = str_replace("R$","",$varAux);
                             if(indexOf($varAux,",") != -1)   $varAux = moeda($varAux,"U$");
                             if(!(filter_var($varAux,FILTER_VALIDATE_FLOAT) !== false)){    
                                 $camposValidos["valor"] = false;
                             }
                             if(!$camposValidos["valor"])  addErroVal("- A coluna <strong>valor</strong> possui um valor incorreto!");
                         }else{
                             addErroVal("- A coluna <strong>valor</strong> possui valores em branco!");                             
                         }

                         /*** PERCENTUAL ***/
                         $camposValidos["percentual"] = true;
                         $varAux = trim($dado["percentual"]);
                         if($varAux != ""){
                             $varAux = str_replace(" ","",$varAux);
                             $varAux = str_replace("%","",$varAux);
                             if(indexOf($varAux,",") != -1)  $varAux = moeda($varAux,"U$");
                             if(!(filter_var($varAux,FILTER_VALIDATE_FLOAT) !== false)){
                                 $camposValidos["percentual"] = false;
                             }
                             if(!$camposValidos["percentual"])  addErroVal("- A coluna <strong>percentual</strong> possui um valor incorreto!");
                         }else{
                             addErroVal("- A coluna <strong>percentual</strong> possui valores em branco!");                             
                         }
                     }
                }  
            }
            /*****************/
            
            /*** GRAVA NA BASE ***/
            $obsHist = "";
            if(!$strResposta && count($errosVal) == 0){
                /*** DESATIVAR ITENS ANTIGOS ***/
                if(count($dadosExcel["dados"]) > 0){
                    $db=new DB('tbl_cotacao');
                    $db->executaQuery("UPDATE tbl_cotacao SET status = ".INATIVO." WHERE id_destaque = ".$idDestaque);                     
                }
                /******************************/
				
                foreach($dadosExcel["dados"] as $dado){
                     if(count($dado)>0){
                        $dado["valor"] = trim(str_replace("U$","",$dado["valor"])); 
                        $dado["valor"] = str_replace("R$","",$dado["valor"]);
                        $dado["valor"] = str_replace(" ","",$dado["valor"]);
                        if($idioma == LNG_PT) $dado["valor"] = moeda($dado["valor"]);
                        else                  $dado["valor"] = moeda($dado["valor"],"U$");  
						
                        $dado["percentual"] = trim(str_replace("%","",$dado["percentual"]));
                        $dado["percentual"] = str_replace(" ","",$dado["percentual"]);
                        if($idioma == LNG_PT) $dado["percentual"] = moeda($dado["percentual"]);
                        else                  $dado["percentual"] = moeda($dado["percentual"],"U$");  
						
                        if(isset($dado["periodo"])){
                            /*** Formatando a data ***/
                            $dado["periodo"] = trim(str_replace("-","/",$dado["periodo"])); //quebrar pelo '-' não da um resultado com com o formato 'nov/11' 
                            $arrAux = explode("/",$dado["periodo"]);                        
                            if(count($arrAux) == 3){
                                /*$arrAux[1] = (int)$arrAux[1];
                                if(!in_array($arrAux[1],$arr_mes_pq) && !in_array($arrAux[1],$arr_mes_pq_ing) ){
                                    $arr_mes_aux = ($idioma == LNG_PT)?$arr_mes_pq:$arr_mes_pq_ing;
                                    if(isset($arr_mes_aux[$arrAux[1]])) $dado["periodo"] = $arr_mes_aux[$arrAux[1]]."/".$arrAux[2];                                
                                }*/
								$dado["periodo"] = $arrAux[0]."/".$arrAux[1]."/".$arrAux[2];								
                            }elseif(count($arrAux) == 2){
								$arr_mes_aux = ($idioma == LNG_PT)?$arr_mes_pq:$arr_mes_pq_ing;
                                if(in_array($arrAux[0],$arr_mes_aux)){
									$dado["periodo"] = $arrAux[0]."/".$arrAux[1];  
								}
								if(in_array($arrAux[1],$arr_mes_aux)){ 
									$arrAux[0] = (int)$arrAux[0];  
								    $arrAux[0] = ($arrAux[0] < 10)?"0".$arrAux[0]:$arrAux[0];                        
                                    $dado["periodo"] = $arrAux[0]."/".$arrAux[1];                                
                                }
                            }
                            /*****************************/
                        }

                        $whereCount = "status = ".ATIVO." AND idioma = $idioma";
                        $obsHist = "Cotações simples";
                        $nomeCategoria = isset($dado["categoria"])?$dado["categoria"]:"";
                                       
                        
                        $idCat = 0;
                        if($nomeCategoria && $idDestaque == 4){
                            /*** CATEGORIA ****/  
                            $nomeCategoria = trim($nomeCategoria);                      
                            $listCat = CategoriaCotacao::listar("id,nome,idioma,status","nome = '".$nomeCategoria."' AND status = ".ATIVO);
                            if($listCat){
                                $objCat = $listCat[0];
                            }else{
                                $objCat = new CategoriaCotacao();
                                $objCat->setNome($nomeCategoria);
                                $objCat->setData(date("Y-m-d H:i:s")); 
                                $objCat->setIdioma($idioma);                            
                            }                           
                            $objCat->setStatus(ATIVO);   
                            $objCat->salvar();
                            $idCat = $objCat->getId();                        
                            $whereCount .= " AND categoria = $idCat";
                            $obsHist = "Cotações com categoria";
                            /******************/
                        }
                        
                        $obj = new Cotacao();
                        $obj->setDataInclusao(date("Y-m-d H:i:s"));
                        $obj->setOrdem(Cotacao::countListar($idDestaque,$whereCount) + 1);
                        $obj->setIdDestaque($idDestaque);
                        $obj->setCategoria($idCat);
                        $obj->setTitulo(esc_valor_excel($dado["titulo"])); 
                        if(isset($dado["periodo"]))
                        $obj->setPeriodo(esc_valor_excel($dado["periodo"]));
                        $obj->setValor(esc_valor_excel($dado["valor"]));                    
                        $obj->setPerc(esc_valor_excel($dado["percentual"]));   
                        $obj->setIdioma($idioma);       
                        $obj->setStatus(ATIVO);    
                        $obj->salvar();
                       // print Cotacao::getLogSql();
                     }               
                }    
                
                $obsHist .= " - <a href=\"".DIRETORIO_RAIZ.$urlMedia."\">Arquivo</a>";
                $obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,IMPORTACAO_DE_DADOS,$obsHist);
                //print $dadosExcel["error"];exit;
                $strResposta="<h2>Dados importados com sucesso</h2>";
                $mostrarBotaoModelo = false;
            }
            if(count($errosVal)){
                $strResposta = "<h2>O arquivo selecionado contém os seguintes erros:</h2>";
                $strResposta .= "<p class='peq'>".implode("<br>",$errosVal)."</p>";
            }
            /*********************/

        }else{
            $strResposta = "Ocorreu um erro no momento da importação dos dados.<br />Tente novamente!";
        }
    }else{
        $strResposta = "Ocorreu um erro ao tentar subir o arquivo.<br />Tente novamente!";        
    }    
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <? include "includes/head.php";?>
    <link href="css/pop-up.css" rel="stylesheet" type="text/css" >
</head>
    <body>
        <div id="sapo">
            <div class="container">
                <? include "includes/topo.php";?>                    
                <? include "includes/menu-lateral.php";?>          
                <div class="coluna-geral">
                    <div class="pop-sucesso pop-msg-importacao">
                        <?=$strResposta?>
                        <?php if($mostrarBotaoModelo):?>
                        <a href="<?=$urlArqModelo?>" class="bt-padrao">Download Modelo</a>
                        <? endif;?>
                        <a href="<?=$urlRetorno?>" class="bt-padrao">Voltar</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
