<?php
include("conf.php");

$id = secureRequest('id');
$user = null;
if($id){
    $user = Usuario::ler($id);
}

$in = "id in (".implode(",",$arrOrdemMenu).")";
$where = "$in AND status = ".ATIVO;
$lstSecoes = Secao::listar("","id,nome",$where,'id ASC');

$checked = '';
$subChecked = '';
$lista = "";
$permissoes = "";
$cont = 0;
$arrMenuTemp = array();
if($lstSecoes){
        $arrPerm = null;
	if($user){
		$arrPerm = $user->getPermissoes(0);
	}
	foreach($lstSecoes as $ls){
		if($user){	
			$checked = (inArray($ls->getId(),$arrPerm))?"checked":"";
		}
                if(!isset($arrMenuTemp[$ls->getId()])) $arrMenuTemp[$ls->getId()] = "";
		$arrMenuTemp[$ls->getId()] .= "<p><input $checked name='permissao_secao' id='permissao_secao_".$ls->getId()."' value='".$ls->getId()."' type=\"Checkbox\"><label for='permissao_secao_".$ls->getId()."'><b>".$ls->getNome()."</b></label></p>\n";
		$cont++;
	}
}	

for($i=0;$i<count($arrOrdemMenu);$i++){
	$lista .= $arrMenuTemp[$arrOrdemMenu[$i]];
}	

	
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
<style>@import url(<?=DIRETORIO_RAIZ?>/sapo/css/estrutura.css);</style>
<link rel="stylesheet" type="text/css" href="css/pop-up.css"/>
<link rel="icon" type="image/gif" href="css/img/favIcon.gif">
<script src='js/jquery-1.3.2.min.js'></script>
<script src="js/scripts.js" language="JavaScript" type="text/javascript"></script>
<script>
	function diplaySubPermissao(id){
		var obj = document.getElementById('boxSub'+id);
		var Link = document.getElementById('link'+id);
		if(obj.style.display == "block"){
			obj.style.display = "none";
			Link.className = "comSub seta-baixo";
		}else{
			obj.style.display = "block";
			Link.className = "comSub seta-cima";
		}
	}

	function alteraSubs(id){
		var check = document.getElementById('check_'+id);		
		var checks = document.getElementById('boxSub'+id).getElementsByTagName('input');
		for(var i = 0; i < checks.length; i++){
			if(check.checked){
				checks[i].checked = true;
			}else{
				checks[i].checked = false;
			}
		}		
	}	
	function salvarPermissoes(){
		var frm = document.formulario;
		var idSub = [];
		for(var i = 0; i < frm.permissao_secao.length; i++){
			objSub = false;
			idSecao = frm.permissao_secao[i].value;
			if(frm.permissao_secao[i].checked){				
				frm.permissoes_secao_ativos.value +=  frm.permissao_secao[i].value+";";
			}else{
				frm.permissoes_secao_inativos.value += frm.permissao_secao[i].value+";";
			}
			objSub = document.getElementById("boxSub"+idSecao);				
			if(objSub){
				inputs = objSub.getElementsByTagName("input");
				str1 = str2 = "";
				for(var j = 0; j < inputs.length; j++){
					if(inputs[j].checked){
						str1 += inputs[j].value+";";
					}else{
						str2 += inputs[j].value+";";
					}
				}
				addInputHidden("perm_sub_"+idSecao+"_ativos",str1);
				addInputHidden("perm_sub_"+idSecao+"_inativos",str2);
				idSub.push(idSecao);
			}	
		}		
		if(frm.id.value.length > 0){
			frm.acao.value = 'salvarPermissoes';
			frm.action = "usuario_controle.php";
			frm.submit();
		}else{
			alert('Permissões salvas com sucesso');
			window.opener.getPermissoes(frm,idSub);
			window.close();
		}
	}
	var checksStatus;
	function selectAll(){
		var elements = document.formulario.elements;
		if(checksStatus == "marcados"){
			for(var i = 0; i < elements.length; i++){
				if(elements[i].type == "checkbox")
					elements[i].checked = false;
			}	
			document.getElementById('marcar-todos').innerHTML = "Marcar todos";
			checksStatus = "desmarcados";
		}else{
			for(var i = 0; i < elements.length; i++){
				if(elements[i].type == "checkbox")
					elements[i].checked = true;
			}
			document.getElementById('marcar-todos').innerHTML = "Desmarcar todos";
			checksStatus = "marcados";
		}
	
	}

</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
    <div class="pop-permissoes">
        <form name='formulario' id='formlario' methood='post'>
        	<input type=hidden name='acao'>
            <input type=hidden name='permissoes_secao_ativos' value='<?=$permissoes?>'>
            <input type=hidden name='permissoes_secao_inativos' value='<?=$permissoes?>'>
            <input type=hidden name='total' value='<?=$cont?>'>
            <input type=hidden name='id' value='<?=secureResponse($id)?>'>
            
            <h3>SEÇÕES</h3>
            <div id="box-items">
				<?=$lista?>
            </div>
            <div>
            <a href="javascript:selectAll()" id="marcar-todos" class="bt-padrao">Marcar todos</a>
            <a href="javascript:salvarPermissoes()" class="bt-padrao">Salvar</a>
            </div>
        </form>            
    </div>
</body>
</html>
