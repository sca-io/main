<? require_once("conf.php");
Usuario::autenticarLogon();

$idSecao = request("idSecao");
$idioma = request("idioma");
$idSubSecao = request("idSubSecao");
$id = secureRequest('id');

if($id){
    if($idSecao == SEJA_NOSSO_CLIENTE){
        $obj = Cliente::ler($id);
        if($obj){       
            $nome = $obj->getNome();
            $email = $obj->getEmail();    
            $mensagem = $obj->getMensagem();
            $tipoCliente = $obj->getTipoCliente("nome");    
            if($tipoCliente) $nomeTipoCliente = $tipoCliente->getNome();
            $mensagem = str_replace("\n","<br />",$mensagem);
            //$endereco = $obj->getEndereco();

            if($obj->getStatus() == Cliente::NAOAVALIADO){
                $obj->setStatus(Cliente::ATIVO);  
                $obj->salvar();
            }
            //$status = $obj->getStatus(true);
            $dataInclusao = $obj->getDataInclusao();
        }
    }
    if($idSecao == CONTATOS){
        $obj = Contato::ler($id);
        if($obj){       
            $nome = $obj->getNome();
            $email = $obj->getEmail(); 
            $uf = $obj->getUf();        
            $cidade = $obj->getCidade();
            $telefone = $obj->getTelefone();
            $assunto = $obj->getAssunto();
            $mensagem = $obj->getMensagem();
            $mensagem = str_replace("\n","<br />",$mensagem);
            //$endereco = $obj->getEndereco();

            if($obj->getStatus() == Contato::NAOAVALIADO){
                $obj->setStatus(Contato::LIDO);  
                $obj->salvar();
            }
            $status = $obj->getStatus(true);
            $dataInclusao = $obj->getDataInclusao();
        }
    }
    if($idSecao == GESTAO_DE_PESSOAS){
        $obj = CadGestaoPessoa::ler($id);
        if($obj){    
            $idioma = $obj->getIdioma();
            $nome = $obj->getNome();
            $endereco = $obj->getEndereco();
            $num = $obj->getNum();
            $comp = $obj->getComp();             
            $cidade = $obj->getCidade();
            $estado = $obj->getEstado();  
            $telefone = $obj->getTelefone();
            $email = $obj->getEmail();           
            $area = $obj->getArea(); 
            $outros = $obj->getOutros();
            $formacaoAcad= $obj->getFormacaoAcad();
            if($formacaoAcad) $formacaoAcad = $arrFormAcad[$idioma][$formacaoAcad];
            $curso = $obj->getCurso();
            $cursoAtual = $obj->getCursoAtual();
            $idiomas = $obj->getIdiomas();            
            $dadosProfiss = $obj->getDadosProfiss();       
            $dadosProfiss = str_replace("\n","<br />",$dadosProfiss);           
            
            if($obj->getStatus()  == CadGestaoPessoa::NAOAVALIADO){
                $obj->setStatus(CadGestaoPessoa::LIDO);  
                $obj->salvar();
            }
            $dataInclusao = $obj->getDataInclusao();
            $status = $obj->getStatus(true);
        } 
    }
    $mailto = "$email"; //&body=
    //$mensagemPadrao = $assunto;
    
    
    if(isset($assunto) && $assunto) $mailto .= "&subject=$assunto";
    if(isset($mensagemPadrao) && $mensagemPadrao) $mailto .= "&body=$mensagemPadrao";



}



?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <? include "includes/head.php";?>
    <link rel="stylesheet" type="text/css" href="css/home.css"/>
    <script>
        function enviarEmail(){
            wopen('pop-enviar-email.php?idSecao=<?=$idSecao?>&id=<?=$id?>','pop','620','300','no');
        }
    </script>    
</head>
<body>
    <div id="sapo">
        <div class="container">
          <? include "includes/topo.php";?>
          
          <!-- EXEMPLO DE MÓDULO COM MENU LATERAL -->
          
          <? include "includes/menu-lateral.php";?>
          
          <div class="coluna-geral">
            <? include "includes/topo_int.php";?>
            <div class="modulo-geral">
            	<div class="lista-dados">
            	<table class="lista-dados" cellspacing="0">
                    <tbody>                      
                      <? if(isset($nome)){?><tr><td>Nome</td><td class="branco"><?=$nome?></td></tr><? }?>                      
                      <? if(isset($endereco)){?><tr><td>Endereço</td><td class="branco"><?=$endereco?></td></tr><? }?>
                      <? if(isset($num)){?><tr><td>Número</td><td class="branco"><?=$num?></td></tr><? }?>
                      <? if(isset($comp)){?><tr><td>Complemento</td><td class="branco"><?=$comp?></td></tr><? }?>                      
                      <? if(isset($cidade)){?><tr><td>Cidade</td><td class="branco"><?=$cidade?></td></tr><? }?>
                      <? if(isset($uf)){?><tr><td>UF</td><td class="branco"><?=$uf?></td></tr><? }?>
                      <? if(isset($estado)){?><tr><td>Estado</td><td class="branco"><?=$estado?></td></tr><? }?>    
                      <? if(isset($telefone)){?><tr><td>Telefone</td><td class="branco"><?=$telefone?></td></tr><? }?>
                      <? if(isset($email)){?><tr><td>E-mail</td><td class="branco"><?=$email?></td></tr><? }?> 
                      <? if(isset($area)){?><tr><td>Área</td><td class="branco"><?=$area?></td></tr><? }?>
                      <? if(isset($outros)){?><tr><td>Outros</td><td class="branco"><?=$outros?></td></tr><? }?>                      
                      <? if(isset($formacaoAcad)){?><tr><td>Formação Acadêmica</td><td class="branco"><?=$formacaoAcad?></td></tr><? }?>
                      <? if(isset($curso)){?><tr><td>Curso</td><td class="branco"><?=$curso?></td></tr><? }?>
                      <? if(isset($cursoAtual)){?><tr><td>Curso de Atualização</td><td class="branco"><?=$cursoAtual?></td></tr><? }?>
                      <? if(isset($idiomas)){?><tr><td>Idiomas</td><td class="branco"><?=$idiomas?></td></tr><? }?>    
                      
                      <? if(isset($dadosProfiss)){?><tr><td>Dados Profissionais</td><td class="branco"><?=$dadosProfiss?></td></tr><? }?>
                      <? if(isset($assunto)){?><tr><td>Assunto</td><td class="branco"><?=$assunto?></td></tr><? }?>
                      <? if(isset($nomeTipoCliente)){?><tr><td>Tipo</td><td class="branco"><?=$nomeTipoCliente?></td></tr><? }?>
                      <? if(isset($mensagem)){?><tr><td>Mensagem</td><td class="branco"><?=$mensagem?></td></tr><? }?>  
                      
                      <? if(isset($status)){?><tr><td>Status</td><td class="branco"><?=$status?></td></tr><? }?>
                      <? if(isset($dataInclusao)){?><tr><td>Data de cadastro</td><td class="branco"><?=Util::dataDoBdcomHora($dataInclusao)?></td></tr><? }?> 
                      
                      
                      <tr><td>Ação</td><td class="branco"><a href="mailto:<?=$mailto?>" class="bt-padrao" style="margin-top: 0">Enviar e-mail</a> </td></tr>
                      
                      
                      
                    </tbody>
                 </table>
                    <a href="<?=$_SERVER['HTTP_REFERER']?>" class="bt-padrao">Voltar</a>
                    <? /*if(!$lstRes):?><a class="bt-padrao" href="javascript:enviarEmail()">responder</a><? endif;*/ ?>
                 </div>
                </div>	                
            </div>
          </div>
    </div>
</body>
</html>