<?
require_once("conf.php");
header("Content-type: text/html; charset=UTF-8");

$controleAcesso->addWhiteListReferer(array(
    "sapo/edicao_home.php",
    "sapo/edicao-media.php",
    "sapo/pop-media.php",
    "sapo/frm_media_crop.php",
    "sapo/edicao_chamadas.php",
    "sapo/edicao_conteudo.php",
    "sapo/listagem_arquivos.php",
    "sapo/edicao_conteudo_inicial.php",
    "sapo/edicao_conteudo_com_subs.php",
    "sapo/listagem_arquivos.php"
));
$controleAcesso->autenticarReferer();
Usuario::autenticarLogon();

$id = secureRequest('id');
$idSecao = request('idSecao');
$acao = request('acao');
$indexAba = request('indexAba');
$urlRetorno = request('urlRetorno');
$idsSemana = request('idsSemana');
$tipoBackUp = request('tipoBackUp');
//$callback = request('callback');
$callback = "";

$vld = array();
$val = array();
$msg = array();
$minLen = array();
$maxlen = array();

if($acao=='salvarConteudo'){
    $id = secureRequest('id');
    $assunto = request('assunto');
    $descricao = request('descricao');
    $titulo = request('titulo');
    $texto = request('texto');
    $imgDestaque = request('imgDestaque');
    $publicar = request('status');
    $keywords = request('keywords');
    $autor = request('autor');
    $urlArquivo = request('urlArquivo');
    $urlEmbed = request('urlEmbed');
    $ids_nr = request('ids_nr');
    $idioma = request('idioma');
    $idioma = $idioma?$idioma:LNG_PT;
    $dataPublicacao = request('dataPublicacao');

    if($id){ $val["id"] = $id; $msg['id'] = "Id"; $vld['id'] = 3;$maxlen['id'] = 100;$minlen['id'] = 1;}
    if($assunto){ $val["assunto"] = $assunto; $msg['assunto'] = "Assunto"; $vld['assunto'] = 3;$maxlen['assunto'] = 11;$minlen['assunto'] = 1;}
    $val["idSecao"] = $idSecao; $msg['idSecao'] = "idSecao"; $vld['idSecao'] = 3;$maxlen['idSecao'] = 100;$minlen['idSecao'] = 1;    
    if($titulo){ $val["titulo"] = $titulo; $msg['titulo'] = "Titulo"; $vld['titulo'] = 1;$maxlen['titulo'] = 255;$minlen['titulo'] = 1;}
    if($texto){ $val["texto"] = $texto; $msg['texto'] = "Texto"; $vld['texto'] = 1;$minlen['texto'] = 1;}
    if($imgDestaque){ $val["imgDestaque"] = $imgDestaque; $msg['imgDestaque'] = "Imagem de Destaque"; $vld['imgDestaque'] = 1;$maxlen['imgDestaque'] = 255;$minlen['imgDestaque'] = 1;}
    $val["publicar"] = $publicar; $msg['publicar'] = "Publicar"; $vld['publicar'] = 3;$maxlen['publicar'] = 11;$minlen['publicar'] = 1;
    if($descricao){ $val["descricao"] = $descricao; $msg['descricao'] = "Descricao"; $vld['descricao'] = 1;$maxlen['descricao'] = 255;$minlen['descricao'] = 1;}
    if($keywords){ $val["keywords"] = $keywords; $msg['keywords'] = "Keywords"; $vld['keywords'] = 1;$maxlen['keywords'] = 255;$minlen['keywords'] = 1;}
    if($autor){ $val["autor"] = $autor; $msg['autor'] = "autor"; $vld['autor'] = 1;$maxlen['autor'] = 255;$minlen['autor'] = 1;}
    if($ids_nr){ $val["ids_nr"] = $ids_nr; $msg['ids_nr'] = "ids_nr"; $vld['ids_nr'] = 1;$maxlen['ids_nr'] = 255;$minlen['ids_nr'] = 1;}
    if($urlArquivo){ $val["urlArquivo"] = $urlArquivo; $msg['urlArquivo'] = "urlArquivo"; $vld['urlArquivo'] = 1;$maxlen['urlArquivo'] = 255;$minlen['urlArquivo'] = 1;}
    if($urlEmbed){ $val["urlEmbed"] = $urlEmbed; $msg['urlEmbed'] = "urlEmbed"; $vld['urlEmbed'] = 1;$maxlen['urlEmbed'] = 255;$minlen['urlEmbed'] = 1;}
    if($dataPublicacao){ $val["dataPublicacao"] = $dataPublicacao; $msg['dataPublicacao'] = "Data Publicação"; $vld['dataPublicacao'] = 1;$maxlen['dataPublicacao'] = 255;$minlen['dataPublicacao'] = 1;}
    
    $res = Util::validaFormulario($vld,$val,$msg,$maxlen,$minLen);
    if($res == 1){
        if($id){
            $obj = Conteudo::ler($id);
        }else{
            $obj = new Conteudo();
            $obj->setIdSecao($idSecao);
            $obj->setDataInclusao(date('Y-m-d H:i:s'));           
            $obj->setStatus(Conteudo::SALVO);
            $obj->salvar();
        }

        if($obj){
            if($obj->getId()){                
                $versaoCont = VersaoConteudo::listar($idSecao,$obj->getId(), "*", "idioma = $idioma", "", "1");
                if($versaoCont){
                    $versaoCont = $versaoCont[0];
                    $idAcaoHistorico = EDITADO;
                }else{
                    $versaoCont = new VersaoConteudo();
                    $versaoCont->setDataInclusao(date('Y-m-d H:i:s'));
                    $idAcaoHistorico = INSERIDO;
                }
                $versaoCont->setIdConteudo($obj->getId());
                $versaoCont->setIdSecao($idSecao);
                $versaoCont->setAssunto($assunto);
                $versaoCont->setIdioma($idioma);
                $versaoCont->setDescricao($descricao);
                $versaoCont->setKeywords($keywords);
                $versaoCont->setAutor($autor);
                $versaoCont->setUrlEmbed($urlEmbed);
                $versaoCont->setIdsRelacionados($ids_nr);
                
                /*** PARA CONTEUDOS FILTRADOS POR ASSUNTO ***/
                if(!$titulo && $assunto){
                    $objAss = Assunto::ler($assunto);
                    $titulo = $objAss->getNome();
                }
                /****************************************/
                
                $versaoCont->setTitulo($titulo);
                //$versaoCont->setTexto(html_entity_decode($texto,ENT_QUOTES,"UTF-8"));
                $versaoCont->setTexto(($texto));
                $versaoCont->setImgDestaque($imgDestaque);
                $tituloUrlAmig = $titulo;
                if(!$tituloUrlAmig) $tituloUrlAmig = getNomeSecao($idSecao);
                $versaoCont->setUrlAmigavel(setUrlAmigavel($tituloUrlAmig));
                if($tituloUrlAmig){
                    $tituloUrlAmig = setUrlAmigavel($tituloUrlAmig);
                    $whereCount = "titulo = '$titulo'";
                    if($versaoCont->getId()) $whereCount .= " AND id != '".$versaoCont->getId()."'";
                    $qntVConts = (int)VersaoConteudo::countListar("","",$whereCount);
                    if($qntVConts > 0) $tituloUrlAmig .= "-".($qntVConts+1);
                    $versaoCont->setUrlAmigavel($tituloUrlAmig);
                }
                $versaoCont->setStatus($publicar);
                $contChanged = false;
                if($idSecao == NOTICIAS){
                    $versaoCont->setDataPublicacao(Util::dataParaBD($dataPublicacao));
                }else{                  
                    if($publicar == VersaoConteudo::PUBLICADO && (!$versaoCont->getDataPublicacao() || $versaoCont->getDataPublicacao() == "0000-00-00 00:00:00")){
                        $versaoCont->setDataPublicacao(date('Y-m-d H:i:s'));
                    }
                }
                if($publicar == VersaoConteudo::PUBLICADO){
                    if((!$obj->getDataPublicacao() || $obj->getDataPublicacao() == "0000-00-00 00:00:00")){
                        $obj->setDataPublicacao(date('Y-m-d H:i:s'));
                        $contChanged = true;
                    }
                    if($obj->getStatus() == Conteudo::SALVO){
                        $obj->setStatus($publicar);
                        $contChanged = true;
                    }
                }else{
                    $contConts= VersaoConteudo::countListar($idSecao,$obj->getId(),"status = ".VersaoConteudo::PUBLICADO);
                    if($contConts == 0){
                        $obj->setStatus($publicar);
                        $contChanged = true;
                    }
                }
                $versaoCont->salvar(); 
                if($versaoCont->getId()){                    
                    if($contChanged) $obj->salvar();
                    $versaoCont->inserirHistorico(getSession('idUsuarioSapo'),$ip,$idAcaoHistorico);
                    $urlRetorno = urlencode($urlRetorno."&idioma=$idioma&id=".secureResponse($obj->getId()));
                    //exit;
                    print "<script>document.location='pop-up_mensagem.php?acao=Salvo&urlRetorno=$urlRetorno';</script>";
                }else{
                    $msgError = "Ocorreu um erro ao salvar o conteudo, tente novamente";
                    print "<script>window.location = 'pop-up_mensagem.php?acao=ERROR&msg=$msgError&urlRetorno=".urlencode($urlRetorno)."';</script>";
                }
            }else{
                print "Erro: conteudo nao possui identificador";
            }
        }else{
            print "Erro ao recuperar/salvar o conteudo";
        }
    }else{
        Util::showErroValidacao($res,"javascript:history.back()");
    }

}

if($acao=='enviarVersaoContPraLixeira'){
    if($id && $idSecao){
    $obj = VersaoConteudo::ler($id);
        if($obj){
            $obj->setStatus(VersaoConteudo::INATIVO);
            $obj->salvar();
            $obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,DELETADO,$idSecao);
            if($urlRetorno){
                $urlRetorno = urlencode($urlRetorno);
                print "<script>document.location='pop-up_mensagem.php?acao=excluido&urlRetorno=$urlRetorno';</script>";
            }else{
                print "$msg";
            }
        }


    }
}

/**********************************************************************************  HOME */
if($acao == "buscaConteudoHome"){
    $cont_secao = request("cont_secao");
    $idioma = request("idioma");
    if($cont_secao){       
        $retorno = "";
        if($cont_secao == ACOES_SUSTENTAVEIS || $cont_secao == CERTIFICACOES){
            $where = "status != ".Chamada::INATIVO." AND idioma = $idioma";
            $listaConts = Chamada::listar($cont_secao,"","id,titulo",$where,"data_insercao DESC"); 
        }else{
            $where = "status != ".VersaoConteudo::INATIVO." AND idioma = $idioma";
            $listaConts = VersaoConteudo::listar($cont_secao,"","id,titulo",$where,"data_inclusao DESC");
        }
        
        if($listaConts){
            foreach($listaConts as $l){
                if($retorno != ""){
                    $retorno .= ",";
                }
                $id = secureResponse($l->getId());
                $titulo = $l->getTitulo();
                if(!$titulo) $titulo = getNomeSecao (getIdSecaoPai($cont_secao),$cont_secao,$idioma);
                //$nome = getNomeSecao($l->getId(),"","",$idioma);
                $retorno .= '{"id":"'.$id.'","titulo":"'.response_attr($titulo).'"}';
            }
        }
        print $callback.'({"error":0,"ids":['.$retorno.']})';
    }else{
        print $callback."({error:1,msg:'Seção não selecionada!'})";        
    }

}

if($acao == "selecionaConteudoHome"){
    $cont_secao = request("cont_secao");
    $cont_id = secureRequest("cont_id");
    $idDestaque = request('idDestaque');
    $idioma = request("idioma");
    $max_len_texto = "";
    if(isset($settModulosDeHome[$idSecao][$idDestaque]["max_len_texto"]))
    $max_len_texto = $settModulosDeHome[$idSecao][$idDestaque]["max_len_texto"];
    if($cont_id){
        if($cont_secao == ACOES_SUSTENTAVEIS || $cont_secao == CERTIFICACOES){
            $obj = Chamada::ler($cont_id,"id,id_secao,titulo,texto");
        }else{
            $obj = VersaoConteudo::ler($cont_id,"id,id_secao,titulo,texto,img_destaque");
        }
        
        if($obj){
            $settings = getSettings(getIdSecaoPai($cont_secao),$cont_secao);
            $sID = secureResponse($obj->getId());
            $sIDSecao = secureResponse($idSecao);
            $sIDSubSecao = secureResponse($cont_secao);
            $idSecaCont = $obj->getIdSecao();            
            $titulo = $obj->getTitulo();  
            $tituloUrlAmig = $titulo;
            if(!$titulo){
                $titulo = getNomeSecao (getIdSecaoPai($cont_secao),$cont_secao,$idioma);
                //$tituloUrlAmig = getNomeSecao ($idSecao,$cont_secao,LNG_PT);                
            }
            //$titulo = htmlentities($titulo, ENT_QUOTES,"UTF-8");
            $texto = $obj->getTexto();           
            $texto = str_replace("\t","",$texto); 
            $texto = strip_tags($texto);
            $texto = str_replace("\n","",$texto);
            $texto = str_replace("\r","",$texto);
            $texto = strip_tags($texto);
            if($max_len_texto) $texto = Util::truncarTexto2($texto,$max_len_texto);
            
            $urlImg = "";
            $urlLink = $settings["url_secao"];
            
            //if($cont_secao != ACOES_SUSTENTAVEIS && $cont_secao != CERTIFICACOES) $urlLink .= setUrlAmigavel($titulo); //ACOES_SUSTENTAVEIS é a unica secao de chamadas sem intena
            //if($tituloUrlAmig) $urlLink .= setUrlAmigavel($tituloUrlAmig);            
            //$urlLink = ("conteudo.php?idSecao=$sIDSecao&idSubSecao=$sIDSubSecao&id=".$sID);
            $retorno = '"titulo":"'.response_attr($titulo).'","texto":"'.response_attr($texto,false).'","urlImg":"'.response_attr($urlImg).'","urlLink":"'.response_attr($urlLink,false).'"';
            print $callback.'({"error":"0",'.$retorno.'})';
        }else{
            print $callback."({error:1,msg:'Erro inesperado!'})";
        }     
    }else{
        print $callback."({error:1,msg:'Seção não selecionada!'})";        
    } 

}

function salvarSubConteudos($idioma){
    $subsItens = TipoCliente::listar("id,descricao","idioma = '$idioma'");
    foreach($subsItens as $sub){
        $id = request('id_sub_'.$sub->getId());
        $texto = request('texto_sub_'.$sub->getId());
        
        $vld = Array(1);
        $frm = Array($texto);
        $msg = Array("Texto");
        $retorno = Util::validaFormulario($vld,$frm,$msg);
        if($retorno==true){
             $sub->setDescricao($texto);
             $sub->salvar();
        }else{
            print "<script>window.location = 'pop-up_mensagem.php?acao=ERROR&msg=$retorno&urlRetorno=".urlencode($urlRetorno)."';</script>";
        }        
    }
}

if($acao == "salvarChamada"){
    $cont_id = secureRequest('cont_id');
    $cont_secao = request('cont_secao');
    $idioma = request('idioma');
    $idDestaque = request('idDestaque');
    $titulo = request('titulo');
    $texto = request('texto');
    $urlMedia = request('urlMedia');
    $legendaMedia = request("legendaMedia");
    $urlLink = request('urlLink');
    $targetLink = request('targetLink');   
    $status = request("status")?request("status"):ATIVO;
    $tipos_subs = request('tipos_subs');  
    
    $vld = Array(1,1,1,1,1);
    $frm = Array($titulo,$texto,$cont_id,$cont_secao,$idioma);
    $msg = Array("Título","Texto","Conteúdo","Seção","Idioma");
    $retorno = Util::validaFormulario($vld,$frm,$msg);
    if($retorno==true){
        $obj = null;
        if($id){
            $obj = Chamada::ler($id);
            $idAcaoHistorico = EDITADO;

        }
        if(!$obj){
            $obj = new Chamada();            
            $obj->setDataInsercao(date("Y-m-d H:i:s"));
            $obj->setOrdem(Chamada::countListar($idSecao,$idDestaque,"status = ".Chamada::PUBLICADO." AND idioma = $idioma") + 1);
            $idAcaoHistorico = INSERIDO;
        }
       
        $obj->setIdSecao($idSecao);
        $obj->setContId($cont_id);        
        $obj->setContSecao($cont_secao);
        $obj->setIdDestaque($idDestaque);
        $obj->setTitulo($titulo);
        $obj->setTexto($texto);
        $obj->setUrlMedia($urlMedia);
        $obj->setLegendaMedia($legendaMedia);
        if($urlLink && $targetLink == "_blank") $urlLink = "http://".str_replace("http://","",$urlLink);
        $obj->setLink($urlLink);
        $obj->setTarget($targetLink);
        $obj->setIdioma($idioma);       
        $obj->setStatus($status);  
        if($status == Chamada::PUBLICADO && $obj->getDataPublicacao() == "0000-00-00 00:00:00")  $obj->setDataPublicacao(date("Y-m-d H:i:s"));
        $obj->salvar();
        if($obj->getId()){
            if($tipos_subs){
                salvarSubConteudos($idioma);
            }
            $obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,$idAcaoHistorico);
            print "<script>window.location = 'pop-up_mensagem.php?acao=salvo&msg=".urlencode("Dados salvos com sucesso")."&urlRetorno=".urlencode($urlRetorno."&id=".secureResponse($obj->getId())."&idDestaque=$idDestaque#ch_".secureResponse($obj->getId()))."';</script>";
        }else{
            $msgError = "Ocorreu um erro ao salvar o conteudo, tente novamente";
            //print "<script>window.location = 'pop-up_mensagem.php?acao=ERROR&msg=$msgError&urlRetorno=".urlencode($urlRetorno)."';</script>";
            print $obj->getLogSql();
        }
    }else{
        print "<script>window.location = 'pop-up_mensagem.php?acao=ERROR&msg=$retorno&urlRetorno=".urlencode($urlRetorno)."';</script>";
    }
}


if($acao == 'salvarOrdenacaoChamadas'){
    $ids = request('ids');
    $ids = explode(";",$ids);
    $idxId = 1;
    foreach($ids as $id){
        $sID = secureRequest("",$id);        
        $obj = Chamada::ler($sID,"id,ordem");
        if($obj){
            $obj->setOrdem($idxId);
            $obj->salvar();        }
        $idxId++;
    }
    print "OK";
}


if($acao == 'excluirChamadasHome'){
    if($id){        
        $obj = Chamada::ler($id,"id,id_secao,status,titulo");
        if($obj){
            $obj->setStatus(INATIVO);   
            $obj->salvar();
            $obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,DELETADO);
            print $callback."({error:0,msg:'Conteúdo excluído com sucesso!'})";
        }else{
             print $callback ."({error:1,msg:'Conteúdo não encontrado!'})";
        }        
    }else{
         print $callback."({error:1,msg:'Conteúdo não selecionado!'})";
    }
}

if($acao == "fazerBackUp"){
    $obj = new Backup();
    $obj->setNome("backup-".date('d-m-Y-H-i-s'));   
    $obj->setData(date('Y-m-d H:i:s')); 
    $obj->setStatus(ATIVO); 
    $obj->salvar(); 
    $obj->fazerBackUp("/sandbox/sapo/backUp/"); 
    print "<script>document.location='pop-up_mensagem.php?acao=Salvo&urlRetorno=$urlRetorno';</script>";
}

if($acao == 'ziparBackUp'){
    $obj = Backup::ler($id);
    Util::zipar("backUp/".$obj->getNome().".zip","backUp/".$obj->getNome().".sql");
    print "<script>window.location='$urlRetorno?zip=1&nomeArq=".("backUp/".$obj->getNome().".zip")."';</script>";
}

if($acao == 'salvarAgendamento'){
    $db = new DB();
    $db->executaQuery("TRUNCATE TABLE tbl_agenda_backup");

    $obj = new AgendaBackup();
    $obj->setTipoBackUp($tipoBackUp);   
    $obj->setIdsSemana(substr($idsSemana,1));
    $obj->setData(date('Y-m-d H:i:s')); 
    $obj->setStatus(NAOAVALIADO);   
    $obj->salvar(); 
    
    print "<script>document.location='pop-up_mensagem.php?acao=Salvo&urlRetorno=$urlRetorno';</script>";
}

if($acao == 'salvarSecao'){
    $id = request("id");
    $idSecao = request("idSecao");
    $nome = request("nome");
    $nomeIng = request("nome_ing");
    $tipo = request("tipo");
    $publicar = request("publicar_secao");
    //$temMenu = request("temMenu");

    if($id){ $val["id"] = $id; $msg['id'] = "Id"; $vld['id'] = 3;$maxlen['id'] = 100;$minlen['id'] = 1;}
    $val["idSecao"] = $idSecao; $msg['idSecao'] = "idSecao"; $vld['idSecao'] = 3;$maxlen['idSecao'] = 100;$minlen['idSecao'] = 1;
    $val["tipo"] = $tipo; $msg['tipo'] = "Tipo"; $vld['tipo'] = 3;$maxlen['tipo'] = 100;$minlen['tipo'] = 1;
    $val["nome"] = $nome; $msg['nome'] = "Nome"; $vld['nome'] = 1;$maxlen['nome'] = 255;$minlen['nome'] = 1;
    if($nomeIng){ $val["nome_ing"] = $nomeIng; $msg['nome_ing'] = "Nome em ingles"; $vld['nome_ing'] = 1;$maxlen['nome_ing'] = 255;$minlen['nome_ing'] = 1;}
    $res = Util::validaFormulario($vld,$val,$msg,$maxlen,$minLen);
    if($res == 1){
        if($id){
            $obj = Secao::ler($id);
        }else{
            $obj = new Secao();
            $count = Secao::countListar($idSecao,'status = '.Secao::PUBLICADO,Array());
            $obj->setOrdem($count+1);
        }
        $obj->setIdSecao($idSecao);
        $obj->setNome(htmlentities($nome, ENT_QUOTES, 'UTF-8'));
        $obj->setNomeIng(htmlentities($nomeIng, ENT_QUOTES, 'UTF-8'));
        $obj->setTipo($tipo);
        $obj->setStatus($publicar);
        $obj->setTemMenu(0);
        $obj->setTemTopo(1);
        $obj->salvar();
        if($obj->getId()){
            $msg = "ok";
        }else{
            $msg = "error";
        }
        if($urlRetorno){
            print "<script>document.location='$urlRetorno&id=".($obj->getId())."';</script>";
        }else{
            print "$msg";
        }       
    }else{
        Util::showErroValidacao($res,$urlRetorno);
    }
}
if($acao=='excluirSecao'){
    if($id && $idSecao){
    $obj = Secao::ler($id);
    $obj->setStatus(Secao::INATIVO);
    $obj->salvar();
    Secao::reordenar($idSecao);
    $obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,DELETADO);
    print "<script>document.location='$urlRetorno';</script>";
    }
}
if($acao=='ordernarSecao'){
    $idSecao = request("idSecao");
    $direcao = request("direcao");
    if($direcao && $id){
    Secao::ordenar($direcao,$id,$idSecao);
    print "<script>document.location='$urlRetorno';</script>";
    }
}


if($acao=='apagarArquivo'){
    $idCont = request("idCont");
    if($idCont){
    $obj = VersaoConteudo::ler($idCont);
        $obj->setUrlEmbed('');
        $obj->salvar();
    print "ok";
    }
}


/*** ASSUNTOS ****/
if($acao == 'salvarAssunto'){
    $idCat = request("idCat");
    $nome = request("nome");
    $idioma = request("idioma");
    $tipo = request("tipo");

    if($idCat){ $val["idCat"] = $idCat; $msg['idCat'] = "Id"; $vld['idCat'] = 3;$maxlen['idCat'] = 100;$minlen['idCat'] = 1;}
    if($idSecao){$val["idSecao"] = $idSecao; $msg['idSecao'] = "idSecao"; $vld['idSecao'] = 3;$maxlen['idSecao'] = 100;$minlen['idSecao'] = 1;}
    $val["nome"] = $nome; $msg['nome'] = "Nome"; $vld['nome'] = 1;$maxlen['nome'] = 255;$minlen['nome'] = 1;
    if($idioma){ $val["idioma"] = $idioma; $msg['idioma'] = "idioma"; $vld['idioma'] = 1;$maxlen['idioma'] = 100;$minlen['idioma'] = 1;}
    if($tipo){ $val["tipo"] = $tipo; $msg['tipo'] = "Tipo"; $vld['tipo'] = 3;$maxlen['tipo'] = 11;$minlen['tipo'] = 1;}
    
    $res = Util::validaFormulario($vld,$val,$msg,$maxlen,$minLen);
    if($res == 1){
        if($idCat){
            $obj = Assunto::ler($idCat);
        }else{
            $obj = new Assunto();
            $obj->setData(date('Y-m-d H:i:s'));
        }
        $obj->setIdSecao($idSecao);
        $obj->setNome($nome);
        $obj->setIdioma($idioma);        
        $obj->setTipo($tipo);
        $obj->setStatus(ATIVO);
        $obj->salvar();
        /*if($obj->getId()){$msg = "ok";
        }else{  $msg = "error";  }  print $msg; */
        if($obj->getId()){
            $jsonResp = "({\"error\":0,\"msg\":\"Dados salvos com sucesso\",\"id\":".$obj->getId()."})";
        }else{
            $jsonResp = "({\"error\":1,\"msg\":\"Erro ao salvar dados\"})";
        }   
        print $jsonResp;
    }else{
        Util::showErroValidacao($res);
    }
}
if($acao == 'excluirAssunto'){
    $id = request("id");
    if($id){
        $obj = Assunto::ler($id); 
        if($obj){
            $obj->setStatus(INATIVO);   
            $obj->salvar();
            $msg = "ok";    
        }else{
            $msg = "Categoria não encontrada";
        }
    }else{
         $msg = "sem id";
    }
    print $msg;
}

/*** TAGS ****/
if($acao == 'salvarTag'){
    $idCat = request("idCat");
    $nome = request("nome");
    $idioma = request("idioma");

    if($idCat){ $val["idCat"] = $idCat; $msg['idCat'] = "Id"; $vld['idCat'] = 3;$maxlen['idCat'] = 100;$minlen['idCat'] = 1;}
    if($idSecao){$val["idSecao"] = $idSecao; $msg['idSecao'] = "idSecao"; $vld['idSecao'] = 3;$maxlen['idSecao'] = 100;$minlen['idSecao'] = 1;}
    $val["nome"] = $nome; $msg['nome'] = "Nome"; $vld['nome'] = 1;$maxlen['nome'] = 255;$minlen['nome'] = 1;
    if($idioma){ $val["idioma"] = $idioma; $msg['idioma'] = "idioma"; $vld['idioma'] = 1;$maxlen['idioma'] = 100;$minlen['idioma'] = 1;}
    
    $res = Util::validaFormulario($vld,$val,$msg,$maxlen,$minLen);
    if($res == 1){
        if($idCat){
            $obj = Tags::ler($idCat);
        }else{
            $obj = new Tags();
            $obj->setData(date('Y-m-d H:i:s'));
        }
        //$obj->setIdSecao($idSecao);
        $obj->setNome($nome);
        $obj->setIdioma($idioma);        
        $obj->setStatus(ATIVO);
        $obj->salvar();
        /*if($obj->getId()){
            $msg = "ok";
        }else{
            $msg = "error";
        }  
        print $msg;*/ 
        if($obj->getId()){
            $jsonResp = "({\"error\":0,\"msg\":\"Dados salvos com sucesso\",\"id\":".$obj->getId()."})";
        }else{
            $jsonResp = "({\"error\":1,\"msg\":\"Erro ao salvar dados\"})";
        }   
        print $jsonResp;
        
    }else{
        Util::showErroValidacao($res);
    }
}

if($acao == 'excluirTag'){
    $id = request("id");
    if($id){
        $obj = Tags::ler($id); 
        if($obj){
            $obj->setStatus(INATIVO);   
            $obj->salvar();
            $msg = "ok";    
        }else{
            $msg = "Categoria não encontrada";
        } 
    }else{
         $msg = "sem id";
    }
    print $msg;
}

if($acao == 'listarContRelacionado'){   
    $keywords = request("keywords");
    $idioma = request("idioma");
    if($keywords){
        $arrkeys = explode(",", $keywords);
        $whre = " status = ".VersaoConteudo::PUBLICADO." AND idioma = ".$idioma;
        $whreKeys = "";
        foreach($arrkeys as $k){
            $whreKeys .= $whreKeys ? " OR ":""; 
            $whreKeys .= "keywords LIKE '%".trim($k)."%'";
        }
        $whre .=  $whreKeys?" AND ($whreKeys)":"";
        $listaNot = VersaoConteudo::listar($idSecao,"","id,titulo",$whre,"data_publicacao DESC");
        $strRes = ""; 
        foreach($listaNot as $ln){
            $strRes .= $ln->getId()."[,]".$ln->getTitulo()."[;]";
        }
        print $strRes;
    }

}

/*** COTACOES ***/
if($acao == 'salvarCatCotacao'){
    $idCat = request("idCat");
    $nome = request("nome");
    $idioma = request("idioma");

    if($idCat){ $val["idCat"] = $idCat; $msg['idCat'] = "Id"; $vld['idCat'] = 3;$maxlen['idCat'] = 100;$minlen['idCat'] = 1;}
    if($idSecao){$val["idSecao"] = $idSecao; $msg['idSecao'] = "idSecao"; $vld['idSecao'] = 3;$maxlen['idSecao'] = 100;$minlen['idSecao'] = 1;}
    $val["nome"] = $nome; $msg['nome'] = "Nome"; $vld['nome'] = 1;$maxlen['nome'] = 255;$minlen['nome'] = 1;
    $val["idioma"] = $idioma; $msg['idioma'] = "Idioma"; $vld['idioma'] = 1;$maxlen['idioma'] = 100;$minlen['idioma'] = 1;
    
    $res = Util::validaFormulario($vld,$val,$msg,$maxlen,$minLen);
    if($res == 1){
        if($idCat){
            $obj = CategoriaCotacao::ler($idCat);
        }else{
            $obj = new CategoriaCotacao();
            $obj->setStatus(ATIVO); 
            $obj->setData(date("Y-m-d H:i:s"));
        }
        $obj->setNome($nome);
        $obj->setIdioma($idioma);        
        $obj->salvar();
        if($obj->getId()){
            $msg = "ok";
        }else{
            $msg = "error";
        }   
        print $msg;
    }else{
        Util::showErroValidacao($res);
    }
}

if($acao == 'excluirCategoria'){
    $id = request("id");
    if($id){
        /*CategoriaCotacao::delete($id);
        $msg = "ok";  */ 
        $obj = CategoriaCotacao::ler($id); 
        if($obj){
            $obj->setStatus(INATIVO);   
            $obj->salvar();
            $msg = "ok";    
        }else{
            $msg = "Categoria não encontrada";
        }
    }else{
         $msg = "sem id";
    }
    print $msg;
}

if($acao == "salvarCotacao"){
    $idioma = request('idioma');
    $idDestaque = request('idDestaque');
    $categoria = request('categoria');
    $titulo = request('titulo');   
    $periodo = request('periodo');
    $valor = request('valor');
    $perc = request('perc');
    $status = request("status")?request("status"):ATIVO;
    
    $vld = Array(1,1,1,1,1);
    $frm = Array($categoria,$titulo,$periodo,$valor,$perc);
    $msg = Array("Categoria","Título","Periodo","Valor","Percentual");
    $retorno = Util::validaFormulario($vld,$frm,$msg);
    if($retorno==true){
        $obj = null;
        if($id){
            $obj = Cotacao::ler($id);
            $idAcaoHistorico = EDITADO;
        }
        if(!$obj){            
            $obj = new Cotacao();            
            $obj->setDataInclusao(date("Y-m-d H:i:s"));
            $whereCount = "status = ".ATIVO." AND idioma = $idioma";
            if($categoria) $whereCount .= " AND categoria = $categoria";
            $obj->setOrdem(Cotacao::countListar($idDestaque,$whereCount) + 1);
            $idAcaoHistorico = INSERIDO;
        }
        $obj->setIdDestaque($idDestaque);
    $obj->setCategoria($categoria);
        $obj->setTitulo($titulo);
        $obj->setPeriodo($periodo);
        $obj->setValor($valor);
        $obj->setPerc($perc);   
        $obj->setIdioma($idioma);       
        $obj->setStatus($status);    
        $obj->salvar();
        if($obj->getId()){
            $obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,$idAcaoHistorico);
            print "<script>window.location = 'pop-up_mensagem.php?acao=salvo&msg=".urlencode("Dados salvos com sucesso")."&urlRetorno=".urlencode($urlRetorno."&id=".secureResponse($obj->getId())."&idDestaque=$idDestaque#ch_".secureResponse($obj->getId()))."';</script>";
        }else{
            $msgError = "Ocorreu um erro ao salvar o conteudo, tente novamente";
            print "<script>window.location = 'pop-up_mensagem.php?acao=ERROR&msg=$msgError&urlRetorno=".urlencode($urlRetorno)."';</script>";   
        }
    }else{
        print "<script>window.location = 'pop-up_mensagem.php?acao=ERROR&msg=$retorno&urlRetorno=".urlencode($urlRetorno)."';</script>";
    }
}

if($acao == 'salvarOrdenacaoCotacoes'){
    $ids = request('ids');
    $ids = explode(";",$ids);
    $idxId = 1;
    foreach($ids as $id){
        $sID = secureRequest("",$id);        
        $obj = Cotacao::ler($sID,"id,ordem");
        //print Chamada::getLogSql();
        if($obj){
            $obj->setOrdem($idxId);
            $obj->salvar();        }
        $idxId++;
    }
    print "OK";
}

if($acao == 'excluirCotacao'){
    if($id){        
        $obj = Cotacao::ler($id,"id,status,titulo");
        if($obj){
            //$obj->excluir();
            $obj->setStatus(INATIVO);   
            $obj->salvar();
            print $callback."({error:0,msg:'Conteúdo excluído com sucesso!'})";
        }else{
             print $callback."({error:1,msg:'Conteúdo não encontrado!'})";
        }        
    }else{
         print $callback."({error:1,msg:'Conteúdo não selecionado!'})";
    }
}

if($acao == 'salvarDadosExtraCotacao'){
    $idChCotacao = secureRequest('idChCotacao');
    $idioma = request('idioma');
    $idDestaque = request('idDestaque');
    $dataGrupo = request('dataGrupo'); 
    $tituloGrupo = request('tituloGrupo');
    $chapeuGrupo = request('chapeuGrupo');
    
    $vld = Array(1);
    $frm = Array($tituloGrupo);
    $msg = Array("Título do grupo");
    $retorno = Util::validaFormulario($vld,$frm,$msg);
    if($retorno==true){
        $obj = null;
        if($idChCotacao){
            $obj = Chamada::ler($idChCotacao);
            $idAcaoHistorico = EDITADO;
        }
        if(!$obj){
            $obj = new Chamada();            
            $obj->setDataInsercao(date("Y-m-d H:i:s"));
            $obj->setOrdem(Chamada::countListar($idSecao,$idDestaque,"status = ".Chamada::PUBLICADO." AND idioma = $idioma") + 1);
            $idAcaoHistorico = INSERIDO;
        }
        $obj->setIdSecao($idSecao);
        $obj->setIdDestaque($idDestaque);
        $obj->setTitulo($tituloGrupo);
        $obj->setChapeu($chapeuGrupo);
        if($dataGrupo) $dataGrupo = Util::dataParaBD($dataGrupo);
            
        $obj->setDataPublicacao($dataGrupo);
        $obj->setIdioma($idioma);       
        $obj->setStatus(ATIVO);    
        $obj->salvar();
        //$obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,$idAcaoHistorico);
        print "<script>window.location = 'pop-up_mensagem.php?acao=salvo&msg=".urlencode("Dados salvos com sucesso")."&urlRetorno=".urlencode($urlRetorno."&id=".secureResponse($obj->getId())."&idDestaque=$idDestaque#ch_".secureResponse($obj->getId()))."';</script>";

    }else{
        print "<script>window.location = 'pop-up_mensagem.php?acao=ERROR&msg=$retorno&urlRetorno=".urlencode($urlRetorno)."';</script>";
    }
}

if($acao == 'salvarUnidadeProd'){
    $id = secureRequest('id');
    $idDestaque = secureRequest('idDestaque');
    $idioma = request('idioma');
    $nome = request('nome');
    $estado = request('estado'); 
    $link = request('link');
    $descricao = request('descricao');
    $posx = request('posx');
    $posy = request('posy');

    if($id){ $val["id"] = $id; $msg['id'] = "Id"; $vld['id'] = 3;$maxlen['id'] = 100;$minlen['id'] = 1;}
    if($idSecao){$val["idSecao"] = $idSecao; $msg['idSecao'] = "idSecao"; $vld['idSecao'] = 3;$maxlen['idSecao'] = 100;$minlen['idSecao'] = 1;}    
    $val["idioma"] = $idioma; $msg['idioma'] = "Idioma"; $vld['idioma'] = 1;$maxlen['idioma'] = 100;$minlen['idioma'] = 1;    
    $val["nome"] = $nome; $msg['nome'] = "Nome"; $vld['nome'] = 1;$maxlen['nome'] = 255;$minlen['nome'] = 1;    
    $val["estado"] = $estado; $msg['estado'] = "estado"; $vld['estado'] = 1;$maxlen['estado'] = 100;$minlen['estado'] = 1;
    if($link){$val["link"] = $link; $msg['link'] = "link"; $vld['link'] = 1;$maxlen['link'] = 255;$minlen['link'] = 1;}
    if($descricao){$val["descricao"] = $descricao; $msg['descricao'] = "Descrição"; $vld['descricao'] = 1;$maxlen['descricao'] = 2000;$minlen['descricao'] = 1;}
    if($posx){$val["posx"] = $posx; $msg['posx'] = "posx"; $vld['posx'] = 1;$maxlen['posx'] = 10;$minlen['posx'] = 1;}
    if($posy){$val["posy"] = $posy; $msg['posy'] = "posy"; $vld['posy'] = 1;$maxlen['posy'] = 10;$minlen['posy'] = 1;}
    
    $res = Util::validaFormulario($vld,$val,$msg,$maxlen,$minLen);
    if($res==true){
        $obj = null;
        if($id){
            $obj = UnidadeProdutora::ler($id);
            $idAcaoHistorico = EDITADO;
        }
        if(!$obj){
            $obj = new UnidadeProdutora();            
            $obj->setDataInclusao(date("Y-m-d H:i:s"));
            $obj->setOrdem(UnidadeProdutora::countListar("status = ".ATIVO." AND idioma = $idioma AND estado = '$estado'") + 1);
            $idAcaoHistorico = INSERIDO;
        }
        $obj->setNome($nome);
        $obj->setEstado($estado);
        if($link) $link = "http://".str_replace("http://","",$link);
        $obj->setLink($link);
        $obj->setDescricao($descricao);
        $posMapa = ($posx && $posy)?$posx.",".$posy:"";
        $obj->setPosMapa($posMapa); 
        $obj->setIdioma($idioma);       
        $obj->setStatus(ATIVO);    
        $obj->salvar();
        if($obj->getId()){
            $obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,$idAcaoHistorico);
            print "<script>window.location = 'pop-up_mensagem.php?acao=salvo&msg=".urlencode("Dados salvos com sucesso")."&urlRetorno=".urlencode($urlRetorno."&id=".secureResponse($obj->getId())."&idDestaque=$idDestaque#ch_".secureResponse($obj->getId()))."';</script>";
        }else{
            $msgError = "Ocorreu um erro ao salvar o conteudo, tente novamente";
            print "<script>window.location = 'pop-up_mensagem.php?acao=ERROR&msg=$msgError&urlRetorno=".urlencode($urlRetorno)."';</script>";
        }
    }else{
        print "<script>window.location = 'pop-up_mensagem.php?acao=ERROR&msg=$res&urlRetorno=".urlencode($urlRetorno)."';</script>";
    }
}

if($acao == 'salvarOrdenacaoUnidadeProd'){
    $ids = request('ids');
    $ids = explode(";",$ids);
    $idxId = 1;
    foreach($ids as $id){
        $sID = secureRequest("",$id);        
        $obj = UnidadeProdutora::ler($sID,"id,ordem");
        //print UnidadeProdutora::getLogSql();
        if($obj){
            $obj->setOrdem($idxId);
            $obj->salvar();
        }
        $idxId++;
    }
    print "OK";
}

if($acao == 'excluirUnidadeProd'){
    if($id){        
        $obj = UnidadeProdutora::ler($id,"id,nome,status,ordem");
        if($obj){
            //$obj->excluir();
            $obj->setStatus(INATIVO);
            $obj->salvar();
            $obj->inserirHistorico(getSession('idUsuarioSapo'),$ip,DELETADO);
            print $callback."({error:0,msg:'Conteúdo excluído com sucesso!'})";
        }else{
             print $callback."({error:1,msg:'Conteúdo não encontrado!'})";
        }        
    }else{
         print $callback."({error:1,msg:'Conteúdo não selecionado!'})";
    }
}

?>