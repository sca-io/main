<? require_once("conf.php");
Usuario::autenticarLogon();

$id = secureRequest("id");
$versao = secureRequest("versao");
$idSecao = request("idSecao");
$idDestaque = request("idDestaque");

$urlRetorno = "edicao_chamadas.php?idSecao=$idSecao";
$indexAbaEdicao = 1;

$secao = Secao::ler($idSecao);
$idioma = request("idioma")?request("idioma"):LNG_PT;
$where = "idioma = $idioma";

$obj = null;

$camposTable = "id,titulo,idioma,texto,url_media,legenda_media,status";
$where = "status != ".INATIVO." AND $where";
if($id){
    $obj = Chamada::ler($id,$camposTable);
    //print Chamada::getLogSql();
}
if($obj){  
    $titulo = $obj->getTitulo();
    $texto = $obj->getTexto();
    $idioma = $obj->getIdioma();
    $urlMedia = $obj->getUrlMedia();
    $legendaMedia = $obj->getLegendaMedia();
    $status = $obj->getStatus();

}else{
    $legendaMedia = "";
    $titulo = "";
    $texto = "";    
    $urlMedia = "";
    $status = ""; 
}

$urlCancelar = $_SERVER["HTTP_REFERER"];
$matchStr = explode("/",$urlCancelar);
if(indexOf($matchStr[count($matchStr)-1],"pop-up_mensagem.php")!=-1){
    $urlCancelar = "listagem_chamadas.php";
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? include "includes/head.php";?>
<meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE" />
<link rel="stylesheet" type="text/css" href="css/edicao-conteudo.css"/>
<script type="text/javascript">
PHP_FUNCTION = function(){
    openPop('pop-media.php?parametros=<?=urlencode("&idConteudo=".secureResponse($id)."&idSecao=".$idSecao)?>');
}
CK_HAS_LINK = true;
CK_HAS_IMAGE = false;
CK_W = '400px';
CK_H = '200px';
</script>
<script type="text/javascript" src="Fckeditor/ckeditor.js"></script>
<script type="text/javascript" src="Fckeditor/config.js"></script>
<script src='js/UploadByFrame.js' type="text/javascript"></script>
<script type="text/javascript">
function salvar(){
    var frm = document.formulario;
    frm.target = "";
    msg = [];
    vld = [];
    msg['titulo'] = "Título"; vld['titulo'] = 1;
    msg['idioma'] = "Idioma"; vld['idioma'] = 1;
    msg['status'] = "Publicar"; vld['status'] = 1; 
    msg['urlMedia'] = "Imagem"; vld['urlMedia'] = 1;
    //msg['legendaMedia'] = "Legenda da Imagem"; vld['legendaMedia'] = 1;
    
    fSucess = function(url){
        frm.action = "conteudo_controle.php";
        frm.acao.value = "salvarChamada";        
        frm.submit();
        $("#btSalvar").html("aguarde...").attr("href","#");
    }
    fErro = function(strErro){
        var str = sapo_replace(",",";<br/>",strErro);
        alerta("<b>Por favor preencha os campos:</b><br/>"+str);
        enviando = 0;	
    }
    validaFormulario('formulario',fSucess,fErro);
}

function changeIdioma(){
   var frm = document.formulario;
   //frm.id.value = "";
   frm.submit()    
}

function openPopGaleria(){
    <? $idCham = $id?$id:'0';?>
    openPop('pop-media.php?parametros=<?=urlencode("&idioma=$idioma&idDestaque=$idDestaque&idConteudo=".secureResponse($idCham)."&idSecao=".$idSecao."&tipoProp=icones&urlRetorno=".urlencode($urlRetorno))?>');    
}

function recortarImagem(src,diretorio){
    openPop('pop-media.php?tp=2&parametros=<?=urlencode("&idDestaque=$idDestaque&idSecao=".$idSecao."&urlRetorno=".urlencode($urlRetorno)."&callBack=onImgCrop&src=")?>'+src+'<?=urlencode("&diretorio=")?>'+diretorio);    
}

function onImgCrop(){
    var frm = document.formulario;
    statusImagem = '';
    $("#tag-imagem").html("<img src='"+DIRETORIO_RAIZ+frm.urlMedia.value+"' />")
}

function subirImagem(arquivo,tipoProp){
    var frm = document.formulario;
    if(frm[arquivo].value){
         $("#ajaxLoad").css('opacity','0.7').fadeIn();
        frm.action = "media_controle.php?campo="+arquivo+"&tipoProp="+tipoProp;
        frm.acao.value = 'subirImagem';
        uploadMedia(frm,function(res){
            //alert(res);
            res = res.split("[;]");
             $("#ajaxLoad").fadeOut();
            if(res[0] == "sucesso"){
                recortarImagem(res[1],res[2]);
                frm.urlMedia.value = res[2]+"m_"+res[1]
                statusImagem = 'aguardando-crop';
            }else{
                if(!res[1]){
                    alert("Ocorreu um erro ao subir a imagem, verifique se a mesma não ultrapassa o limite máximo do servidor que é de <?=((int)LIMITE_MAX_UPLOAD+1)?>KB");
                }else{
                    alert(res[1]);
                }  
            }
        })
    }else{
        alert("Selecione um arquivo antes")
    }
}

</script>
</head>
<body>
	<div id="sapo">
		<div class="container">
		  <? include "includes/topo.php";?>          
          <? include "includes/menu-lateral.php";?>          
          <div class="coluna-geral">
          	<? include "includes/topo_int.php";?>
            <div class="modulo-geral">
            	<form name="formulario" id="formulario" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="MAX_FILE_SIZE" value="<?=LIMITE_MAX_UPLOAD?>" />
                    <input type="hidden" id="id" name="id" value="<?=secureResponse($id)?>" />
                    <input type="hidden" id="acao" name="acao" value="" />                    
                    <input type="hidden" id="idSecao" name="idSecao" value="<?=$idSecao?>" />
                    <input type="hidden" id="urlRetorno" name="urlRetorno" value="<?=$urlRetorno?>" />
                    <input type="hidden" id="urlMedia" name="urlMedia" value="<?=$urlMedia?>" />
                    <input type="hidden" id="versao" name="versao" value="<?=secureResponse($versao)?>" />
                    <input type="hidden" id="idioma" name="idioma" value="<?=$idioma?>" />
                  <div class="esq">
                      <p>
                      	<strong>Idioma</strong>
                        <select id='idioma' name='idioma' onchange="changeIdioma()">
                            <option value=''>Selecione</option>
                            <?	
                                $std = Array("","","");
				$std[$idioma] = "selected";
                            ?>
                            <option value='<?=LNG_PT?>' <?=$std[LNG_PT]?>><?=$vetIdioma[LNG_PT]?></option>
                            <option value='<?=LNG_EN?>' <?=$std[LNG_EN]?>><?=$vetIdioma[LNG_EN]?></option>        
                        </select>
                      </p>
                      <p id="campo-imagem"><strong>Imagem</strong><input class="medio" type="file" name="arquivo" id="arquivo" /><a href="javascript:subirImagem('arquivo','');" class="bt-padrao" title="Upload" id="btUpload">Upload</a></p>
                      <p id="tag-imagem"><? if($urlMedia):?><img src='<?=DIRETORIO_RAIZ.$urlMedia?>' /><? endif;?></p>
                      <p><strong>Legenda da imagem</strong><textarea class="inputText" id="legendaMedia" name="legendaMedia" style="width:410px" ><?=$legendaMedia?></textarea></p>
                      <p><strong>Título</strong><textarea class="inputText" id="titulo" name="titulo" style="width:410px" ><?=$titulo?></textarea></p>
                      <p><strong>Texto</strong><textarea class="ckeditor" id="texto" name="texto" ><?=stripslashes($texto)?></textarea></p>                      
                      <? /*<p><strong>Embed de flash</strong><textarea class="inputText" id="urlEmbed" name="urlEmbed" ><?=$urlEmbed?></textarea></p>*/ ?>
                      <p>
                      	<strong>Publicar</strong>
                        <select id='status' name='status'>
                            <option value=''>Selecione</option>
                            <?	
                                $std = Array("","","");
				$std[$status] = "selected";
                            ?>
                            <option value='<?=VersaoConteudo::PUBLICADO?>' <?=$std[VersaoConteudo::PUBLICADO]?>>Sim</option>
                            <option value='<?=VersaoConteudo::SALVO?>' <?=$std[VersaoConteudo::SALVO]?>>Não</option>        
                        </select>
                      </p>
                      <? if($id):?><a href="javascript:openPopGaleria();" class="bt-padrao" title="Lista de icones" id="btListaIcones">Lista de icones</a><p>&nbsp;</p><? endif;?>
                      <a href="<?=$urlCancelar?>" class="bt-padrao" title="Cancelar">Cancelar</a>
                      <a href="javascript:salvar();" class="bt-padrao" title="Salvar" id="btSalvar">Salvar</a>
                  </div>
                </form>
                

            </div>
          </div>          
        </div>
    </div>
  </div>
</div>
</body>
</html>