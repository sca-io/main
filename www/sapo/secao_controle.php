<?
require_once("conf.php");
header("Content-type: text/html; charset=UTF-8");
$controleAcesso->autenticarReferer();
Usuario::autenticarLogon();
$acao = request("acao");
if($acao == 'showConstantesSecao'){
    $tipo = request("tipo");
    $secoes = Secao::listar("","id,nome","status = 1");
    if($secoes){
            foreach($secoes as $l){
                $nome = $l->getNome();                
                $nome = html_entity_decode($nome, ENT_QUOTES, 'UTF-8');
                $nome = strtoupper( ereg_replace("[^a-zA-Z0-9-]", " ", strtr(utf8_decode(trim($nome)), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-")) );

                $nome = str_replace(' ','_',$nome);
                $nome = str_replace('-','_',$nome);
                if($tipo == 1){
                    print "define(\"".$nome."\",".$l->getId().");<br />";                   
                }else{
                   print $nome.',';                    
                }                
            }
    }
}
?>