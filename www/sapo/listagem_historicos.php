<?
include("conf.php");
$idConteudo = $diaInicial = $idUsuario = $mesInicial = $anoInicial = $diaFinal = $mesFinal = $anoFinal = $url = $paginacao = $busca =  $lstTitulo = $listaGeral = "";

$idUsuario = request('idUsuario');

$idAcaoHistorico = request('idAcaoHistorico');
$idSecao = HISTORICOS;

$where = "status = ".ATIVO;

$ncadastro =  Historico::countListar('','',$idUsuario,$idAcaoHistorico,$where);
 
$pagina = isset($_REQUEST['pagina'])?$_REQUEST['pagina']:0;				//################################
$total_reg_pag = 20;						//	   ESTE BLOCO DEFINE AS
$init_reg_pag = ($pagina * $total_reg_pag);	//	  VARIAVEIS DE PAGINAÃ
$aux_pagina = $pagina + 1;					//
$total_paginas = ($ncadastro / $total_reg_pag); //################################

$by = (isset($_REQUEST['by'])) ? $_REQUEST['by'] : "data";
$ordem = (isset($_REQUEST['ordem'])) ? $_REQUEST['ordem'] : "DESC";	

$ordemArr = Array();
$ordemArr[$by] = $ordem;

$ordemArr['data'] = (isset($ordemArr['data']))? $ordemArr['data'] : "DESC";
////#########################################

$lista = Historico::listar('','',$idUsuario,$idAcaoHistorico,"*",$where," $by $ordem","$init_reg_pag, $total_reg_pag");
if($lista){
	//$lstTitulo = "<th class='checkbox'><input type='checkbox' id='todosCkb' onClick='selecionaTudo(this)'/></th><th>Conteudo</th><th>Usuário</th><th>Data</th><th>Tipo</th><th>Obs.</th><th class='acao'>Ação</th>";
	$lstTitulo = "<th>&nbsp;Usuário&nbsp;</th><th>Data</th><th>Ação executada</th><th>Obs.</th>";
	$listaGeral = "";
	foreach($lista as $l){
		$nomeSecao = $nomeU = $listacbk = $listaConteudo = $listaAcao = "";
		$objU = $l->getUsuario();
		if($objU){
                    $nomeU = $l->getUsuario()->getLogin();
		}
		$objS = null;
		if($l->getIdSecao()) $objS = Secao::ler($l->getIdSecao());
		if($objS){
             $nomeSecao = $objS->getNome();
		}	
			
		//$listacbk = "<td class='checkbox'><input type='checkbox' class='cbkEdicao' value='".$l->getId()."' name='selecao".$l->getId()."' /></td>";
		//$listaConteudo = "<td>$nomeC</td>";
		$str_acao = "";
		if($l->getObjAcao()) $str_acao = $l->getObjAcao()->getAcao();
		$listaUsuario = "<td>$nomeU</td>";
		$listaData = "<td>".Util::dataDoBdcomHora($l->getData())."</td>";
		$listaTipo = "<td>".$str_acao."</td>";
		$observacao = strip_tags($l->getObservacao(),"<br><a>");
		$nomeSecao .= $nomeSecao && $observacao?" - ":"";
		$observacao = $observacao?$nomeSecao.$observacao:$nomeSecao;
		$listaObs = "<td>".$observacao."</td>";
		//$listaAcao = "<td class='acao'><a href=\"javascript:deletar('".$l->getId()."',".$idSecao.")\" class=\"bt-excluir\">fechar</a></td>";
		$listaGeral .= "<tr>$listacbk $listaConteudo $listaUsuario $listaData $listaTipo $listaObs $listaAcao</tr>";
	}
}


$url .="&idUsuario=".$idUsuario."&idConteudo=".$idConteudo."&idAcaoHistorico=$idAcaoHistorico&diaInicial=$diaInicial&mesInicial=$mesInicial&anoInicial=$anoInicial&diaFinal=$diaFinal&mesFinal=$mesFinal&anoFinal=$anoFinal";
$aux_pagina = $pagina - 1;
$variacao = 4;
if($pagina != 0){
	$paginacao .= "<a href=\"?by=".$by."&ordem=$ordem&pagina=".$aux_pagina."&$url&#ancora\" class='prox-ant'> &lt;&lt Anterior </a>\n";
}
if($total_paginas > 1){
	if($pagina >= -$variacao ){
		for($i = 0; $i<$total_paginas; $i++){	
			if($i != $pagina && $i <= ($pagina + $variacao) && $i >= ($pagina - $variacao)){
					 $paginacao .= "<a href=\"?by=".$by."&ordem=$ordem&pagina=".$i."&$url&busca=$busca\">".($i+1)."</a>\n";
			}
			else if($i==$pagina){
					$paginacao .= "<a class='active'>".($i+1)."</a>";
			}
		}
	}
}

$aux_pagina = $pagina + 1;
if($pagina < ($total_paginas - 1)){
	   $paginacao .= "<a href=\"?by=".$by."&ordem=$ordem&pagina=".$aux_pagina."&$url&#ancora\" class='prox-ant'> Próximo &gt;&gt </a>\n";
}

$idSecao = HISTORICOS;
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<? include "includes/head.php";?>
    <link rel="stylesheet" type="text/css" href="css/home.css"/>
<script>
	function filtrar(){
		var frm = document.formulario;
		frm.submit();
	}
 $(window).ready(function(){ $("#ajaxLoad").fadeOut() });
 </script>
</head>
<body>
	<div id="sapo">
		<div class="container">
		  <? include "includes/topo.php";?>                    
          <? include "includes/menu-lateral.php";?>
          
          <div class="coluna-geral">
            <? include "includes/topo_int.php";?>
            <div class="modulo-geral home3 lista">              
               <div class="dados">
                    <form class="interessados" id='formulario' name='formulario' method="post">
                        <p>Usuário:<br />
                        <select id='idUsuario' name='idUsuario'>        
                            <option value=''>Selecione</option>
                            <?
                                $listaU = Usuario::listar("","*","status = ".ATIVO,"nome ASC");
                                if($listaU){
                                    foreach($listaU as $lu){
                                        $selected = ($idUsuario == $lu->getId())?"selected='selected'":'';
                                        print "<option $selected value='".$lu->getId()."'>".$lu->getLogin()."</option>\n";
                                    }
                                }
                            ?>            
                        </select> 
                        </p>
                        <p>Tipo Edição:<br />
                        <select id='idAcaoHistorico' name='idAcaoHistorico'>
                            <option value=''>Selecione</option>
                            <?	
                                $whr = ' id NOT IN ( SELECT id FROM tbl_acao_historico WHERE acao LIKE "%Imagem%")';
                                $listaA = AcaoHistorico::listar("*",$whr);
                                if($listaA){
                                    foreach($listaA as $la){
                                        $selected = ($idAcaoHistorico == $la->getId())?"selected='selected'":'';
                                        print "<option $selected value='".$la->getId()."'>".$la->getAcao()."</option>\n";
                                    }
                                }
                            ?>            
                        </select>
                        </p>
                        <div> <a href="javascript:filtrar();" class="bt-padrao">Filtrar</a> </div>
                    </form>	
                    </div>
                	<h3>Total de registros: <?=$ncadastro?></h3>
                    <table class="tabela" cellspacing="0">
                        <thead>
                            <tr>
                                <?=$lstTitulo?>
                            </tr>
                        </thead>
                        <tbody>
                            <?=$listaGeral?>
                        </tbody>
                    </table>
					<div class="paginacao"><?=$paginacao?></div>
				</div>
          </div>          
		</div>
    </div>
</body>
</html>