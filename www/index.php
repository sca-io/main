<? require_once ('conf.php');
$idSecao = HOME;
$idioma = getIdioma();

?><? include "includes/head.php"?>

<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="home">
        	<div class="esquerda">
            	<div class="fotos">
                	<div></div>
                        <? $lstMedias = Media::listar(1,$idSecao,"url_link,dir,url,target_link","status = ".ATIVO." AND lang = '$idioma'","ordem ASC"); ?>
                        <? if($lstMedias):?>
                                <? foreach($lstMedias as $lm):?>
                                        <? 
                                        $targetLink = $lm->getTargetLink() == "_blank"?"rel=\"external\"":"";
                                        $tagLink = $lm->getUrlLink()?"<a href=\"".response_attr($lm->getUrlLink())."\" title=\"\" $targetLink />":"";
                                        $tagFechaLink = $tagLink?"</a>":"";
                                        ?>

                                        <?=$tagLink?><img src="<?=DIRETORIO_RAIZ.$lm->getDir()."rt_".$lm->getUrl()?>" width="693" height="384" alt="" /><?=$tagFechaLink?>
                                <? endforeach;?>
                        <? endif;?>
                    
					
                </div>
                <div class="noticias-destaques">
                    <? $noticias = VersaoConteudo::listar(NOTICIAS,"","id,titulo,url_amigavel","status = ".VersaoConteudo::PUBLICADO." AND idioma = '$idioma'","data_publicacao DESC","3"); ?>
                    <? $destaques = Chamada::listar($idSecao,2,"id,titulo,texto,cont_secao,link","status = ".ATIVO." AND idioma = $idioma","ordem ASC");?>
                    <? if($noticias):?>
                            <? $url_noticias = DIRETORIO_RAIZ."noticias/";?>					
                            <div class="noticias<? if($destaques):?> noticias-fechado<? endif;?>"><!--se houver destaques use o noticia fechado-->
                                    <h2<?=getTextoByLang(""," class=\"eng\"");?>><?=getNomeSecao(NOTICIAS)?></h2>
                                    <? foreach($noticias as $not):?>
                                    <? 	$linkNot = $url_noticias.$not->getUrlAmigavel()."/";?>	
                                    <div>
                                            <p><a href="<?=response_attr($linkNot)?>"><?=response_html($not->getTitulo())?></a><a href="<?=response_attr($linkNot)?>" class="st-verde-home"> > </a></p>
                                    </div>
                                    <? endforeach;?>	
                                    <a href="<?=$url_noticias?>" class="bt-veja-a-lista-completa<?=getTextoByLang("","-eng");?>" title="<?=getTextoByLang("Veja a lista completa","View the complete list");?>"><?=getTextoByLang("Veja a lista completa","View the complete list");?></a>					
									<? if(!$destaques):?><a href="javascript:openPop('pop-news.php')" class="bt-assine-nossa-newsletter<?=getTextoByLang("","-eng");?>" title="<?=getTextoByLang("Assine nossa newsletter","Subscribe to our newsletter");?>"><?=getTextoByLang("Assine nossa newsletter","Subscribe to our newsletter");?></a><? endif;?>
							</div>

                    <? endif;?>
                    
                    <? if($destaques):?>
                        <? foreach($destaques as $dest):?>
                        <div class="noticias destaques">
							<? 
                            $idContSecaoPai = getIdSecaoPai($dest->getContSecao());
                            if($idContSecaoPai){
                                 $nomeSecaoDest = getNomeSecao($idContSecaoPai,$dest->getContSecao());                                  
                            }else{
                                 $nomeSecaoDest = getNomeSecao($dest->getContSecao());
                            }                               
                            $urlLinkDest = DIRETORIO_RAIZ.$dest->getLink();
                            $tituloDest = Util::truncarTexto(strip_tags($dest->getTitulo()),200);
                            $textoDest = Util::truncarTexto(strip_tags($dest->getTexto()),200);
                            ?>
                            <h2<?=getTextoByLang(""," class=\"eng\"");?>><?=getTextoByLang("Destaques","Highlights")?></h2>
                            <h3><strong><?=$nomeSecaoDest?></strong></h3>
                            <div>
                                    <p>
                                            <strong><a href="<?=response_attr($urlLinkDest)?>"><?=response_html($tituloDest)?></a></strong>
                                            <a href="<?=response_attr($urlLinkDest)?>"><?=response_html($textoDest)?></a><a href="<?=response_attr($urlLinkDest)?>" class="st-verde-home"> > </a>
                                    </p>
                            </div> 
                            <a href="javascript:openPop('pop-news.php')" class="bt-assine-nossa-newsletter<?=getTextoByLang("","-eng");?>" title="<?=getTextoByLang("Assine nossa newsletter","Subscribe to our newsletter");?>"><?=getTextoByLang("Assine nossa newsletter","Subscribe to our newsletter");?></a>                                            
                        </div>
                        <? endforeach;?>                                     

                    <? endif?>
                    
                    
            	</div>
                <div class="clima">
                	<script  type="text/javascript">
					$(window).ready(function(){
                    <? if(strpos($_SERVER['HTTP_USER_AGENT'],"MSIE 8") > -1 || strpos($_SERVER['HTTP_USER_AGENT'],"MSIE 7") > -1 || strpos($_SERVER['HTTP_USER_AGENT'],"MSIE 6") > -1): ?>
                        $(".clima").html('<iframe src="http://iframe.somarmeteorologia.com.br/sca/" width=235 height=175 frameborder="0"></iframe>');
                    <? else:?>
                        $(".clima").html('<object style=" border:0" type="text/html" data="http://iframe.somarmeteorologia.com.br/sca/" width="235" height="175"></object>');                  
                    <? endif; ?>
					});
					</script>
                </div>
            </div>
            
            <div class="direita">
                <?
                 $listaChamsCotas = Chamada::listar($idSecao,"", "id_destaque,titulo,data_publicacao", "status = ".Chamada::PUBLICADO." AND idioma = $idioma AND id_destaque IN(3,4)", "id_destaque ASC", "2");
                 $dadosDoGrupo = array();
                 
                 foreach($listaChamsCotas as $lst){
                     $dadosDoGrupo[$lst->getIdDestaque()] = $lst;
                 }
                ?>
                <h2<?=getTextoByLang(""," class=\"eng\"");?>><?=getTextoByLang("Cotações","Quotes");?></h2>
                <div class="cotacoes" id="cotacoes">
                    <?
                    $tituloGrupo = $dataGrupo = "";
                    if(isset($dadosDoGrupo[3])){
                        $tituloGrupo = $dadosDoGrupo[3]->getTitulo();
                        $dataGrupo = $dadosDoGrupo[3]->getDataPublicacao();
                        if($dataGrupo == "0000-00-00 00:00:00") $dataGrupo = "";
                        if($dataGrupo) $dataGrupo = Util::dataDoBD($dataGrupo);
                    }
                    ?>
                    <? if($dataGrupo):?><h5><?=getTextoByLang("Fechamento do dia ","Closing the day ")?><?=response_html($dataGrupo)?></h5><? endif;?>
                    <? $listaCotas = Cotacao::listar(3,"","status = ".ATIVO." AND idioma = $idioma","ordem ASC");?>
                    <? if($listaCotas):?>
                        <? foreach($listaCotas as $k=>$lc):?>
                            <? $classCota = ($k == 0 || $k%2==0)?"":" class=\"com-fundo\"";?>
                            <div<?=$classCota?>>
                                <h3><?=response_html($lc->getTitulo())?></h3>
                                <p class="primeiro"><?=$lc->getPeriodo()?response_html($lc->getPeriodo()):"&nbsp;"?></p>
                                <p><?=response_html($lc->getValor())?></p>
                                <p><?=response_html($lc->getPerc())?>%</p>
                            </div> 
                        <? endforeach;?>
                    <? endif;?>                    
                </div>
                
                <div class="envolve" id="box-mais-cotacoes">
                	<a href="javascript:ocultarBoxCotacoes()" class="bt-fecha-cotacoes" title="<?=getTextoByLang("Fechar","Close");?>"><?=getTextoByLang("Fechar","Close");?></a>
                	<div id="pane1" class="cotacoes scroll-pane" >
                      <?
                      $tituloGrupo = $dataGrupo = "";
                      if(isset($dadosDoGrupo[4])){
                          $tituloGrupo = $dadosDoGrupo[4]->getTitulo();
                          $dataGrupo = $dadosDoGrupo[4]->getDataPublicacao();
                          if($dataGrupo == "0000-00-00 00:00:00") $dataGrupo = "";
                          if($dataGrupo) $dataGrupo = Util::dataDoBD($dataGrupo);
                      }
                      ?>
                      
                      
                      
                      <h6><?=response_html($tituloGrupo)?></h6>
                      <? if($dataGrupo):?><h5><?=response_html($dataGrupo)?></h5><? endif;?>				
                      
                      <? $listaCotas2 = Cotacao::listar(4,"","status = ".ATIVO." AND idioma = $idioma AND categoria = (SELECT id FROM tbl_categoria_cotacao WHERE status = ".ATIVO." AND id = tbl_cotacao.categoria LIMIT 1)","categoria,ordem ASC");?>
                      <? if($listaCotas2):?>
                          <? $catAtual = 0;$count = 0;?>
                      
                          <? foreach($listaCotas2 as $k2=>$lc):?>                            
                              <? if($lc->getCategoria() != $catAtual):?>
                  <?                                
                                  $cat = CategoriaCotacao::ler($lc->getCategoria());
                                  if($cat) {
                                      $nomeCat = $cat->getNome();
                                      $catAtual = $lc->getCategoria();
                                  }else $nomeCat = "";
                                  $count = 0;
                                  ?>
                                  <? if($nomeCat):?><h4><?=response_html($nomeCat)?></h4><? endif;?>
                              <? endif;?>
                              <? if($catAtual):?>
                                  <? $classCota = ($count > 0 && ($count+1)%2==0)?" class=\"com-fundo\"":"";?>
                                  <div <?=$classCota?>>
                                      <h3><?=response_html($lc->getTitulo())?></h3>
                                      <p class="primeiro"><?=response_html($lc->getValor())?></p>
                                      <p><?=response_html($lc->getPerc())?>%</p>
                                  </div>
                                  <? $count++;?>
                              <? endif;?>    
                          <? endforeach;?>
                                  
                                  
                                  
                                  
                      <? endif;?>
                  </div>
                </div>
                
                <? if($listaCotas2):?><a href="javascript:mostrarBoxCotacoes()" class="bt-veja-outras-cotacoes-aqui<?=getTextoByLang(""," bt-veja-outras-cotacoes-aqui-eng");?>" title="<?=getTextoByLang("Veja outras cotações aqui","View other stock quotes here");?>"><?=getTextoByLang("Veja outras cotações aqui","View other stock quotes here");?></a><? endif;?>
                
            </div>
            
        </div>
      </div>
    </div>
    <? include "includes/rodape.php"?>
</div>
</body>
</html>