<?php

require_once("../conf.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');
$e = request("e");
$idEm = secureRequest("sid");

$db=new DB();

// Regitra visualizações
if($e == "view"){
    $src = request("src");    
    if($src == "spacer.gif"){
        $src = "http://www.scetanol.com.br/cartaovirtual/pt/images-v2/".$src;
        $evento = new EmailMktEvent();
        $evento->setIdEmail($idEm);
        $evento->setTipo(EmailMktTipoEvent::VISUALIZACAO);
        $evento->setData(date("Y-m-d H:i:s"));
        $evento->salvar();
        $query_upd = "UPDATE `tbl_email_mkt_disp` SET `num_views` = (SELECT count(*) FROM `tbl_email_mkt_event` WHERE `tipo` = ".EmailMktTipoEvent::VISUALIZACAO." AND id_email = ".$idEm.") WHERE `id` = ".$idEm.";";
        //$query_upd = "UPDATE `tbl_email_mkt_disp` SET `num_views` = (num_views+1) WHERE `id` = ".$idEm.";";
        $db->executaQuery($query_upd);
        $cont_img = file_get_contents($src);        
        if($cont_img){
            header("Content-Type: image/gif");
            print $cont_img;
        }
    }
}

// Regitra cliques
if($e == "click"){
    $rediret_to = "http://www.scetanol.com.br";
    $evento = new EmailMktEvent();
    $evento->setIdEmail($idEm);
    $evento->setTipo(EmailMktTipoEvent::CLICK);
    $evento->setData(date("Y-m-d H:i:s"));
    $evento->salvar();    
    $query_upd = "UPDATE `tbl_email_mkt_disp` SET `num_cliques` = (SELECT count(*) FROM `tbl_email_mkt_event` WHERE `tipo` = ".EmailMktTipoEvent::CLICK." AND id_email = ".$idEm.") WHERE `id` = ".$idEm.";";
    //$query_upd = "UPDATE `tbl_email_mkt_disp` SET `num_cliques` = (num_cliques+1) WHERE `id` = ".$idEm.";";
    $db->executaQuery($query_upd);    
    header("location: ".DIRETORIO_RAIZ."cartaovirtual/email.php?sid=".secureResponse($idEm));

}

?>