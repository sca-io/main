<?php
require_once("../conf.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');
$e = request("e");
$idEm = secureRequest("sid");
if($idEm){
    $email = EmailMktDisp::ler($idEm);
    if($email){
        if($email->getIdioma() == LNG_PT){
            $filename = "pt/template.html";
        }
        if($email->getIdioma() == LNG_EN){
            $filename = "in/template.html";
        }
        $body = file_get_contents($filename);
        $body = str_replace("##NOME##",$email->getPrimeiroNome(),$body);
        $body = str_replace("##SID##",secureResponse($idEm),$body);
        $body = str_replace("##LINK_ALT##","",$body);
        print $body;
    }
}


print htmlentities("’",ENT_QUOTES,"utf-8");
?>