<?php

require_once("../../conf.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');
$e = request("e");
$idEm = secureRequest("sid");

$db=new DB();
$tabela = "tbl_email_mkt_disp_2015";
EmailMktEvent::$tableName = "tbl_email_mkt_event_2015";

// Regitra visualizações
if($e == "view"){
    $src = request("src");    
    if($src == "spacer.gif"){
        $src = "http://www.scetanol.com.br/cartaovirtual/2015/v2/images/".$src;
        $evento = new EmailMktEvent();
        $evento->setIdEmail($idEm);
        $evento->setTipo(EmailMktTipoEvent::VISUALIZACAO);
        $evento->setData(date("Y-m-d H:i:s"));
        $evento->salvar();
        $query_upd = "UPDATE `$tabela` SET `num_views` = (SELECT count(*) FROM `".EmailMktEvent::$tableName."` WHERE `tipo` = ".EmailMktTipoEvent::VISUALIZACAO." AND id_email = ".$idEm.") WHERE `id` = ".$idEm.";";
        $db->executaQuery($query_upd);
        $cont_img = file_get_contents($src);        
        if($cont_img){
            header("Content-Type: image/gif");
            print $cont_img;
        }
    }
}

// Regitra cliques
if($e == "click_link_alt" || $e == "click"){
    if($e == "click_link_alt"){
        $tipo = EmailMktTipoEvent::CLICK_LINK_ALT;
        $link_redirect = DIRETORIO_RAIZ."cartaovirtual/2015/email.php?sid=".secureResponse($idEm);
        $campo = "num_cliques_alt_link";
    }else{
        $tipo = EmailMktTipoEvent::CLICK;  
        $link_redirect = "http://www.scalcool.com.br/cartaovirtual/2015/v2/";
        $campo = "num_cliques";
    }
    $rediret_to = "http://www.scetanol.com.br";
    $evento = new EmailMktEvent();
    $evento->setIdEmail($idEm);
    $evento->setTipo($tipo);
    $evento->setData(date("Y-m-d H:i:s"));
    $evento->salvar();    
    $query_upd = "UPDATE `$tabela` SET `$campo` = (SELECT count(*) FROM `".EmailMktEvent::$tableName."` WHERE `tipo` = ".$tipo." AND id_email = ".$idEm.") WHERE `id` = ".$idEm.";";
    $db->executaQuery($query_upd);    
    header("location: $link_redirect");    
}

?>