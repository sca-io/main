-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 11/12/2013 às 12h59min
-- Versão do Servidor: 5.5.16
-- Versão do PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `scalcool_db`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_email_mkt_disp_2013`
--

DROP TABLE IF EXISTS `tbl_email_mkt_disp_2013`;
CREATE TABLE IF NOT EXISTS `tbl_email_mkt_disp_2013` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma` tinyint(1) NOT NULL DEFAULT '0',
  `nome_completo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `primeiro_nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enviado` tinyint(1) NOT NULL DEFAULT '0',
  `num_views` int(11) NOT NULL DEFAULT '0',
  `num_cliques` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `enviado` (`enviado`),
  KEY `idioma` (`idioma`),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tbl_email_mkt_disp_2013`
--

INSERT INTO `tbl_email_mkt_disp_2013` (`id`, `idioma`, `nome_completo`, `primeiro_nome`, `email`, `enviado`, `num_views`, `num_cliques`, `status`) VALUES
(1, 1, 'Fabiano Alves', 'Fabiano', 'fabiano@atomica.com.br', 0, 0, 0, 1),
(2, 1, 'Fabiano Alves 2', 'Fabiano 2', 'fabianoalvs@gmail.com', 0, 0, 0, 1),
(3, 1, 'Fabiano Alves 3', 'Fabiano 3', 'fabianoalv@hotmail.com', 0, 0, 0, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
