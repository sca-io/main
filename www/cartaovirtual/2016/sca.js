(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.AsnossascrençassetransformamempensamentosNossospensa = function() {
	this.initialize(img.AsnossascrençassetransformamempensamentosNossospensa);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1050,983);


(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,933,2155);


(lib.Camada1 = function() {
	this.initialize(img.Camada1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1250,1934);


(lib.Camada1copy = function() {
	this.initialize(img.Camada1copy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,433,417);


(lib.Camada3 = function() {
	this.initialize(img.Camada3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,786,1014);


(lib.Camada4 = function() {
	this.initialize(img.Camada4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1299,585);


(lib.FelizNataleumprósperoanonovo = function() {
	this.initialize(img.FelizNataleumprósperoanonovo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,881,187);


(lib.Layer1 = function() {
	this.initialize(img.Layer1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,322,264);


(lib.Layer10 = function() {
	this.initialize(img.Layer10);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,88,75);


(lib.Layer11 = function() {
	this.initialize(img.Layer11);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,136,62);


(lib.Layer12 = function() {
	this.initialize(img.Layer12);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,174,78);


(lib.Layer13 = function() {
	this.initialize(img.Layer13);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,200,144);


(lib.Layer14 = function() {
	this.initialize(img.Layer14);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1090,835);


(lib.Layer15 = function() {
	this.initialize(img.Layer15);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,108,117);


(lib.Layer17 = function() {
	this.initialize(img.Layer17);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,756,961);


(lib.Layer18 = function() {
	this.initialize(img.Layer18);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,458,909);


(lib.Layer19 = function() {
	this.initialize(img.Layer19);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,511,90);


(lib.Layer2 = function() {
	this.initialize(img.Layer2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,74,87);


(lib.Layer20 = function() {
	this.initialize(img.Layer20);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,802,97);


(lib.Layer3 = function() {
	this.initialize(img.Layer3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,80,61);


(lib.Layer4 = function() {
	this.initialize(img.Layer4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,70,87);


(lib.Layer5 = function() {
	this.initialize(img.Layer5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,62,53);


(lib.Layer6 = function() {
	this.initialize(img.Layer6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,110,137);


(lib.Layer7 = function() {
	this.initialize(img.Layer7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,94,101);


(lib.Layer8 = function() {
	this.initialize(img.Layer8);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,86,69);


(lib.Layer9 = function() {
	this.initialize(img.Layer9);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,66,65);


(lib.SCAcopiar = function() {
	this.initialize(img.SCAcopiar);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,578,217);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol31 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer7();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol31, new cjs.Rectangle(0,0,37.8,40.6), null);


(lib.Symbol30 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer6();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol30, new cjs.Rectangle(0,0,44.2,55.1), null);


(lib.Symbol29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer5();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol29, new cjs.Rectangle(0,0,24.9,21.3), null);


(lib.Symbol28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer4();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol28, new cjs.Rectangle(0,0,28.2,35), null);


(lib.Symbol27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer3();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol27, new cjs.Rectangle(0,0,32.2,24.5), null);


(lib.Symbol26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer2();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol26, new cjs.Rectangle(0,0,29.8,35), null);


(lib.Symbol25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer12();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol25, new cjs.Rectangle(0,0,69.9,31.4), null);


(lib.Symbol24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer11();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol24, new cjs.Rectangle(0,0,54.7,24.9), null);


(lib.Symbol23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer10();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol23, new cjs.Rectangle(0,0,35.4,30.2), null);


(lib.Symbol22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer9();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol22, new cjs.Rectangle(0,0,26.5,26.1), null);


(lib.Symbol21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer8();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol21, new cjs.Rectangle(0,0,34.6,27.7), null);


(lib.Symbol20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer13();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol20, new cjs.Rectangle(0,0,80.4,57.9), null);


(lib.Symbol19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer14();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol19, new cjs.Rectangle(0,0,438,335.5), null);


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer15();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol18, new cjs.Rectangle(0,0,43.4,47), null);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer17();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol16, new cjs.Rectangle(0,0,303.8,386.2), null);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer18();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol15, new cjs.Rectangle(0,0,184.1,365.3), null);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer19();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(0,0,205.3,36.2), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer20();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(0,0,322.3,39), null);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Layer1();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(0,0,129.4,106.1), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.SCAcopiar();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,232.2,87.2), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.FelizNataleumprósperoanonovo();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,353.9,75.1), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.AsnossascrençassetransformamempensamentosNossospensa();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,421.8,394.9), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Camada4();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,521.8,235), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Camada3();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,315.8,407.4), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Camada1copy();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,174,167.5), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Camada1();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.402,0.402);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,502.2,776.9), null);


// stage content:
(lib.SCA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{LoopPoint:65});

	// timeline functions:
	this.frame_115 = function() {
		this.gotoAndPlay("LoopPoint");
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(115).call(this.frame_115).wait(8));

	// SCA copiar
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(603.1,722.5,1.02,1.02,0,0,0,116,43.6);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:116.1,x:603.2},0).wait(2).to({x:603.1},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(2).to({x:603},0).wait(3).to({x:602.9},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(3).to({x:602.8},0).wait(3).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:602.7},0).wait(3).to({x:602.6},0).wait(2).to({x:602.5},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(2).to({x:602.4},0).wait(4).to({scaleX:1.01,scaleY:1.01,x:602.3},0).wait(4).to({x:602.2},0).wait(3).to({scaleX:1.01,scaleY:1.01,x:602.1},0).wait(3).to({x:602},0).wait(3).to({scaleX:1.01,scaleY:1.01,x:601.9},0).wait(3).to({x:601.8},0).wait(3).to({scaleX:1.01,scaleY:1.01,alpha:0.077},0).wait(1).to({x:601.7,alpha:0.154},0).wait(1).to({alpha:0.231},0).wait(1).to({alpha:0.308},0).wait(1).to({alpha:0.385},0).wait(1).to({x:601.6,alpha:0.462},0).wait(1).to({alpha:0.538},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:601.5,alpha:0.615},0).wait(1).to({alpha:0.692},0).wait(1).to({alpha:0.769},0).wait(1).to({x:601.4,alpha:0.846},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:1},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:601.3},0).wait(48).to({regX:116,x:601.2,y:722.6},0).wait(1).to({regX:116.1,y:722.5},0).wait(5).to({_off:true},1).wait(7));

	// Feliz Natal e um próspero ano novo! 
	this.instance_1 = new lib.Symbol6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(606,566.4,1.02,1.02,0,0,0,176.7,37.6);
	this.instance_1.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({regX:177,x:606.2},0).wait(1).to({y:566.5},0).wait(2).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:606.1},0).wait(2).to({y:566.6},0).wait(1).to({x:606},0).wait(2).to({scaleX:1.02,scaleY:1.02,y:566.7},0).wait(2).to({x:605.9},0).wait(2).to({x:605.8},0).wait(1).to({y:566.8},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:605.7},0).wait(2).to({y:566.9},0).wait(2).to({x:605.6},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:605.5},0).wait(1).to({y:567},0).wait(2).to({x:605.4},0).wait(1).to({y:567.1},0).wait(2).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({x:605.3},0).wait(2).to({y:567.2},0).wait(2).to({x:605.2},0).wait(1).to({x:605.1,y:567.3},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(3).to({x:605},0).wait(1).to({y:567.4},0).wait(2).to({scaleX:1.01,scaleY:1.01,x:604.9},0).wait(2).to({y:567.5},0).wait(2).to({x:604.8},0).wait(1).to({y:567.6},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:604.7,y:567.5,alpha:0.077},0).wait(1).to({y:567.6,alpha:0.154},0).wait(1).to({alpha:0.231},0).wait(1).to({alpha:0.308},0).wait(1).to({x:604.6,y:567.7,alpha:0.385},0).wait(1).to({alpha:0.462},0).wait(1).to({alpha:0.538},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:604.5,y:567.8,alpha:0.615},0).wait(1).to({y:567.7,alpha:0.692},0).wait(1).to({x:604.4,y:567.8,alpha:0.769},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.923},0).wait(1).to({y:567.9,alpha:1},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:604.3},0).wait(3).to({x:604.2,y:568},0).wait(45).to({x:604},0).wait(1).to({x:603.9},0).wait(5).to({_off:true},1).wait(7));

	// "As nossas crenças se transformam em pensamentos.  Nossos pensa
	this.instance_2 = new lib.Symbol5();
	this.instance_2.parent = this;
	this.instance_2.setTransform(580.5,302.1,1.02,1.02,0,0,0,211,197.4);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({regX:210.9,x:580.4},0).wait(1).to({y:302.2},0).wait(1).to({x:580.3},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:302.3},0).wait(1).to({y:302.4},0).wait(1).to({x:580.2},0).wait(1).to({y:302.5},0).wait(1).to({y:302.6},0).wait(1).to({x:580.1},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:302.7},0).wait(2).to({y:302.8},0).wait(1).to({y:302.9},0).wait(1).to({x:580},0).wait(1).to({y:303},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:303.1},0).wait(1).to({x:579.9},0).wait(1).to({y:303.2},0).wait(2).to({x:579.8,y:303.3},0).wait(1).to({y:303.4},0).wait(2).to({scaleX:1.02,scaleY:1.02,y:303.5},0).wait(1).to({x:579.7,y:303.6},0).wait(2).to({y:303.7},0).wait(1).to({x:579.6},0).wait(1).to({y:303.8},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:303.9},0).wait(1).to({x:579.5},0).wait(1).to({y:304},0).wait(1).to({y:304.1},0).wait(1).to({y:304.2},0).wait(2).to({x:579.4},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:304.3},0).wait(1).to({y:304.4},0).wait(1).to({x:579.3},0).wait(1).to({y:304.5},0).wait(1).to({y:304.6},0).wait(1).to({x:579.2,y:304.7},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:579.3},0).wait(1).to({x:579.2,alpha:0.2},0).wait(1).to({y:304.8,alpha:0.4},0).wait(1).to({y:304.9,alpha:0.6},0).wait(1).to({x:579.1,y:305,alpha:0.8},0).wait(1).to({alpha:1},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:305.1},0).wait(1).to({x:579,y:305.2},0).wait(2).to({y:305.3},0).wait(2).to({x:578.9,y:305.4},0).wait(1).to({y:305.5},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({x:578.8,y:305.6},0).wait(1).to({y:305.7},0).wait(2).to({x:578.7,y:305.8},0).wait(2).to({scaleX:1.01,scaleY:1.01,y:305.9},0).wait(1).to({x:578.6,y:306},0).wait(1).to({x:578.7},0).wait(1).to({x:578.6,y:306.1},0).wait(45).to({regX:211.2,regY:197.5,x:578.7,y:306.2},0).wait(1).to({regX:210.9,regY:197.4,x:578.3,y:306},0).wait(5).to({_off:true},1).wait(7));

	// Camada 3
	this.instance_3 = new lib.Symbol3();
	this.instance_3.parent = this;
	this.instance_3.setTransform(156.1,193.2,1.02,1.02,0,0,0,157.8,203);
	this.instance_3.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1).to({regX:157.9,regY:203.7,x:156.3,y:194},0).wait(1).to({y:194.1},0).wait(1).to({y:194.2},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:194.3},0).wait(1).to({x:156.4},0).wait(1).to({y:194.4},0).wait(1).to({x:156.5,y:194.5},0).wait(1).to({y:194.6},0).wait(1).to({x:156.6,y:194.7},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({y:194.8},0).wait(1).to({y:194.9},0).wait(1).to({x:156.7,y:195},0).wait(1).to({y:195.1},0).wait(1).to({x:156.8,y:195.2},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:156.9,y:195.3},0).wait(1).to({y:195.4},0).wait(1).to({y:195.5},0).wait(1).to({y:195.6},0).wait(1).to({x:157,y:195.7},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:157.1,y:195.8},0).wait(1).to({y:195.9},0).wait(1).to({x:157.2,y:196},0).wait(1).to({y:196.1},0).wait(2).to({y:196.3},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:157.3},0).wait(1).to({y:196.4},0).wait(1).to({x:157.4,y:196.5},0).wait(1).to({y:196.6},0).wait(1).to({x:157.5,alpha:0.167},0).wait(1).to({y:196.7,alpha:0.333},0).wait(1).to({y:196.8,alpha:0.5},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:196.9,alpha:0.667},0).wait(1).to({x:157.6,y:197,alpha:0.833},0).wait(1).to({y:197.1,alpha:1},0).wait(1).to({x:157.7,y:197.2},0).wait(2).to({y:197.3},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:157.8,y:197.4},0).wait(1).to({y:197.5},0).wait(2).to({x:157.9,y:197.7},0).wait(2).to({x:158,y:197.8},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:197.9},0).wait(1).to({y:198},0).wait(1).to({x:158.1,y:198.1},0).wait(2).to({y:198.2},0).wait(1).to({x:158.2,y:198.3},0).wait(1).to({y:198.4},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:158.3},0).wait(1).to({y:198.6},0).wait(2).to({x:158.4,y:198.7},0).wait(1).to({y:198.8},0).wait(1).to({y:198.9},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:158.5,y:199},0).wait(1).to({x:158.6,y:199.1},0).wait(2).to({y:199.2},0).wait(45).to({regX:157.6,regY:202.7,x:158.5,y:198.5},0).wait(1).to({regX:157.9,regY:203.7,x:158.9,y:199.5},0).wait(5).to({_off:true},1).wait(7));

	// Camada 4
	this.instance_4 = new lib.Symbol4();
	this.instance_4.parent = this;
	this.instance_4.setTransform(541.6,752,1.02,1.02,0,0,0,260.9,117.3);
	this.instance_4.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1).to({regY:117.5,y:752.2},0).wait(3).to({scaleX:1.02,scaleY:1.02,x:541.5},0).wait(3).to({y:752.1},0).wait(1).to({x:541.4,y:752.2},0).wait(2).to({scaleX:1.02,scaleY:1.02,y:752.1},0).wait(1).to({x:541.3},0).wait(1).to({x:541.4,y:752.2},0).wait(1).to({x:541.3,y:752.1},0).wait(3).to({scaleX:1.02,scaleY:1.02,x:541.2},0).wait(5).to({x:541.1},0).wait(2).to({scaleX:1.02,scaleY:1.02},0).wait(3).to({x:541,y:752,alpha:0.143},0).wait(1).to({alpha:0.286},0).wait(1).to({y:752.1,alpha:0.429},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:752,alpha:0.571},0).wait(1).to({x:540.9,alpha:0.714},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:1},0).wait(1).to({x:540.8},0).wait(1).to({x:540.9},0).wait(1).to({x:540.8},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(2).to({x:540.7},0).wait(4).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({x:540.6},0).wait(2).to({y:751.9},0).wait(2).to({y:752},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:540.5,y:751.9},0).wait(4).to({x:540.4},0).wait(3).to({scaleX:1.01,scaleY:1.01,x:540.3},0).wait(1).to({x:540.4},0).wait(1).to({x:540.3},0).wait(3).to({x:540.2},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:540.3},0).wait(1).to({x:540.2,y:751.8},0).wait(1).to({y:751.9},0).wait(1).to({y:751.8},0).wait(45).to({regY:117.3,y:751.7},0).wait(1).to({regY:117.5,x:540.1,y:751.8},0).wait(5).to({_off:true},1).wait(7));

	// Camada 1
	this.instance_5 = new lib.Symbol1();
	this.instance_5.parent = this;
	this.instance_5.setTransform(244.1,1179.2,1.02,1.02,0,0,0,250.8,388.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1).to({regX:251.1,regY:388.4,x:244.7,y:1150.7},0).wait(1).to({x:245,y:1122.4},0).wait(1).to({x:245.3,y:1094},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:245.5,y:1065.7},0).wait(1).to({x:245.8,y:1037.3},0).wait(1).to({x:246.1,y:1009},0).wait(1).to({x:246.4,y:980.7},0).wait(1).to({x:246.7,y:952.3},0).wait(1).to({x:246.9,y:923.9},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:247.2,y:895.6},0).wait(1).to({x:247.5,y:867.3},0).wait(1).to({x:247.8,y:839},0).wait(1).to({x:248.1,y:810.6},0).wait(1).to({x:248.3,y:782.2},0).wait(1).to({x:248.7,y:753.9},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:248.9,y:725.6},0).wait(1).to({x:249.2,y:697.2},0).wait(1).to({x:249.5,y:668.9},0).wait(1).to({x:249.8,y:640.5},0).wait(1).to({x:250.1,y:612.2},0).wait(1).to({x:250.3,y:583.9},0).wait(1).to({x:250.6,y:555.5},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:250.9,y:527.1},0).wait(1).to({x:251.2,y:498.8},0).wait(1).to({x:251.5,y:470.5},0).wait(1).to({x:251.4},0).wait(1).to({y:470.4},0).wait(1).to({x:251.3},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:470.5},0).wait(2).to({x:251.2},0).wait(1).to({y:470.4},0).wait(1).to({x:251.1,y:470.5},0).wait(3).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({x:251,y:470.4},0).wait(1).to({y:470.5},0).wait(2).to({x:250.9},0).wait(1).to({y:470.4},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:250.8},0).wait(1).to({y:470.5},0).wait(2).to({x:250.7},0).wait(1).to({y:470.4},0).wait(1).to({x:250.6,y:470.5},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(2).to({x:250.5},0).wait(1).to({y:470.4},0).wait(1).to({y:470.5},0).wait(1).to({x:250.4},0).wait(2).to({scaleX:1.01,scaleY:1.01,x:250.3,y:470.4},0).wait(2).to({y:470.5},0).wait(1).to({x:250.2},0).wait(2).to({x:250.1,y:470.4},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:470.5},0).wait(3).to({x:250},0).wait(45).to({regX:250.7,x:249.6},0).wait(1).to({regX:251.1,x:250},0).wait(5).to({_off:true},1).wait(7));

	// Camada 1 copy
	this.instance_6 = new lib.Symbol2();
	this.instance_6.parent = this;
	this.instance_6.setTransform(157.2,369,1.02,1.02,0,0,0,86.7,83.8);
	this.instance_6.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1).to({regX:87,x:157.6,y:362},0).wait(1).to({x:157.7,y:355.1},0).wait(1).to({x:157.9,y:348.2},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:158,y:341.2},0).wait(1).to({x:158.1,y:334.3},0).wait(1).to({x:158.2,y:327.4},0).wait(1).to({x:158.4,y:320.5},0).wait(1).to({x:158.5,y:313.5},0).wait(1).to({x:158.6,y:306.6},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:158.7,y:299.7},0).wait(1).to({x:158.9,y:292.8},0).wait(1).to({x:159,y:285.8},0).wait(1).to({x:159.1,y:278.9},0).wait(1).to({x:159.3,y:272},0).wait(1).to({x:159.4,y:265.1},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:159.5,y:258.1},0).wait(1).to({x:159.6,y:251.2},0).wait(1).to({x:159.8,y:244.3},0).wait(1).to({x:159.9,y:237.4},0).wait(1).to({x:160,y:230.4},0).wait(1).to({x:160.1,y:223.5},0).wait(1).to({x:160.3,y:216.6},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:160.4,y:209.6},0).wait(1).to({x:160.5,y:202.7},0).wait(1).to({x:160.6,y:195.8},0).wait(1).to({x:160.8,y:188.9,alpha:0.077},0).wait(1).to({x:160.9,y:181.9,alpha:0.154},0).wait(1).to({x:161,y:175,alpha:0.231},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:161.2,y:168.1,alpha:0.308},0).wait(1).to({x:161.3,y:161.2,alpha:0.385},0).wait(1).to({x:161.4,y:154.2,alpha:0.462},0).wait(1).to({x:161.5,y:147.3,alpha:0.538},0).wait(1).to({x:161.7,y:140.4,alpha:0.615},0).wait(1).to({x:161.8,y:133.5,alpha:0.692},0).wait(1).to({x:161.9,y:126.5,alpha:0.769},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:162,y:119.6,alpha:0.846},0).wait(1).to({x:162.2,y:112.7,alpha:0.923},0).wait(1).to({x:162.3,y:105.8,alpha:1},0).wait(1).to({x:162.2,y:105.6},0).wait(1).to({x:162.1,y:105.5},0).wait(1).to({x:162,y:105.4},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:161.9,y:105.2},0).wait(1).to({x:161.8,y:105.1},0).wait(1).to({x:161.7,y:105},0).wait(1).to({y:104.9},0).wait(1).to({x:161.5,y:104.7},0).wait(1).to({x:161.4,y:104.6},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:104.5},0).wait(1).to({x:161.2,y:104.4},0).wait(1).to({x:161.1,y:104.2},0).wait(1).to({y:104.1},0).wait(1).to({x:161,y:104},0).wait(1).to({x:160.9,y:103.9},0).wait(1).to({x:160.8,y:103.7},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:160.7,y:103.6},0).wait(1).to({x:160.6,y:103.5},0).wait(1).to({x:160.5,y:103.3},0).wait(1).to({x:160.4,y:103.2},0).wait(1).to({x:160.3,y:103.1},0).wait(1).to({x:160.2,y:103},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:160.1,y:102.8},0).wait(1).to({x:160,y:102.7},0).wait(1).to({x:159.9,y:102.6},0).wait(1).to({x:159.8,y:102.4},0).wait(45).to({regX:86.7,x:159.6,y:102.5},0).wait(1).to({regX:87,x:159.8,y:102.4},0).wait(5).to({_off:true},1).wait(7));

	// Layer 1
	this.instance_7 = new lib.Symbol12();
	this.instance_7.parent = this;
	this.instance_7.setTransform(744.5,42.7,1.02,1.02,0,0,0,64.5,52.8);
	this.instance_7.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1).to({regX:64.7,regY:53,x:744.7,y:43},0).wait(1).to({x:744.6,y:43.1},0).wait(1).to({x:744.5,y:43.2},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:43.3},0).wait(1).to({y:43.4},0).wait(1).to({x:744.4,y:43.5},0).wait(1).to({y:43.6},0).wait(1).to({x:744.3,y:43.7},0).wait(1).to({x:744.2,y:43.8},0).wait(1).to({y:43.9},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:44},0).wait(1).to({x:744.1},0).wait(1).to({x:744,y:44.2},0).wait(1).to({y:44.3},0).wait(1).to({y:44.4},0).wait(1).to({x:743.9,y:44.5},0).wait(1).to({y:44.6},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:743.8},0).wait(1).to({x:743.7,y:44.7},0).wait(1).to({y:44.8},0).wait(1).to({y:44.9},0).wait(1).to({x:743.6,y:45},0).wait(1).to({y:45.1},0).wait(1).to({x:743.5,y:45.2},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:45.3},0).wait(1).to({x:743.4,y:45.4},0).wait(1).to({y:45.5},0).wait(1).to({x:743.3,y:45.6},0).wait(1).to({x:743.2,y:45.7},0).wait(1).to({y:45.8},0).wait(1).to({y:45.9},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:743.1,y:46},0).wait(1).to({y:46.1},0).wait(1).to({y:46.2},0).wait(1).to({x:743,y:46.3},0).wait(1).to({x:742.9,y:46.4},0).wait(1).to({y:46.5},0).wait(1).to({x:742.8,y:46.6},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:46.7},0).wait(1).to({x:742.7,y:46.8},0).wait(1).to({y:46.9},0).wait(1).to({x:742.6,y:47},0).wait(1).to({y:47.1},0).wait(2).to({x:742.5,y:47.2},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:742.4,y:47.3},0).wait(1).to({y:47.4},0).wait(1).to({x:742.3,y:47.5},0).wait(1).to({y:47.7},0).wait(1).to({y:47.8},0).wait(1).to({x:742.2},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:742.1,y:47.9},0).wait(1).to({y:48},0).wait(1).to({y:48.1},0).wait(1).to({x:742,y:48.2},0).wait(1).to({x:741.9,y:48.3},0).wait(1).to({y:48.4},0).wait(1).to({x:741.8,y:48.5},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:48.6},0).wait(1).to({y:48.7},0).wait(1).to({x:741.7,y:48.8},0).wait(1).to({x:741.6,y:48.9,alpha:0.125},0).wait(1).to({y:49,alpha:0.25},0).wait(1).to({y:49.1,alpha:0.375},0).wait(1).to({regX:64,regY:52.9,x:741.4,y:48.9,alpha:0.5},0).wait(1).to({regX:64.7,regY:53,x:742,y:49,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.771},0).wait(1).to({alpha:0.743},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.686},0).wait(1).to({alpha:0.657},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.588},0).wait(1).to({alpha:0.582},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.559},0).wait(1).to({alpha:0.553},0).wait(1).to({alpha:0.547},0).wait(1).to({alpha:0.541},0).wait(1).to({alpha:0.535},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.524},0).wait(1).to({alpha:0.518},0).wait(1).to({alpha:0.512},0).wait(1).to({alpha:0.506},0).wait(1).to({alpha:0.5},0).wait(1).to({regX:64,regY:52.9,x:741.4,y:48.9},0).wait(1).to({regX:64.7,regY:53,x:742,y:49},0).wait(5).to({_off:true},1).wait(7));

	// Layer 2
	this.instance_8 = new lib.Symbol26();
	this.instance_8.parent = this;
	this.instance_8.setTransform(794.6,221.5,1.02,1.02,0,0,0,14.6,17.2);
	this.instance_8.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(1).to({regX:14.9,regY:17.5,x:794.8,y:221.9},0).wait(2).to({x:794.7,y:222},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:222.1},0).wait(1).to({x:794.6,y:222.2},0).wait(2).to({x:794.5,y:222.3},0).wait(1).to({y:222.4},0).wait(1).to({x:794.4,y:222.5},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:794.3,y:222.6},0).wait(1).to({x:794.2},0).wait(1).to({y:222.7},0).wait(1).to({x:794.1,y:222.8},0).wait(1).to({x:794},0).wait(1).to({y:222.9},0).wait(1).to({x:793.9,y:223},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:223.1},0).wait(1).to({x:793.8},0).wait(1).to({y:223.2},0).wait(1).to({x:793.7,y:223.3},0).wait(1).to({y:223.4},0).wait(1).to({x:793.6},0).wait(1).to({y:223.5},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:793.5,y:223.6},0).wait(2).to({x:793.4,y:223.7},0).wait(1).to({y:223.8},0).wait(1).to({x:793.3,y:223.9},0).wait(1).to({x:793.2},0).wait(1).to({y:224},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:793.1},0).wait(1).to({y:224.1},0).wait(1).to({x:793,y:224.2},0).wait(1).to({x:792.9,y:224.3},0).wait(2).to({x:792.8,y:224.4},0).wait(1).to({y:224.5},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:792.7},0).wait(1).to({y:224.6},0).wait(1).to({x:792.6,y:224.7},0).wait(1).to({y:224.8},0).wait(1).to({x:792.5},0).wait(1).to({y:224.9},0).wait(1).to({x:792.4,y:225},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:792.3,y:225.1},0).wait(2).to({x:792.2,y:225.2},0).wait(1).to({y:225.3},0).wait(1).to({x:792.1,alpha:0.143},0).wait(1).to({y:225.4,alpha:0.286},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:792,alpha:0.429},0).wait(1).to({y:225.5,alpha:0.571},0).wait(1).to({x:791.9,y:225.6,alpha:0.714},0).wait(1).to({y:225.7,alpha:0.857},0).wait(1).to({x:791.8,alpha:1},0).wait(1).to({y:225.8,alpha:0.945},0).wait(1).to({x:791.7,y:225.9,alpha:0.891},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:791.6,y:226,alpha:0.836},0).wait(1).to({x:791.5,alpha:0.781},0).wait(1).to({y:226.1,alpha:0.727},0).wait(1).to({x:791.4,y:226.2,alpha:0.672},0).wait(1).to({y:226.3,alpha:0.617},0).wait(1).to({x:791.3,alpha:0.563},0).wait(1).to({regX:14.8,regY:17.3,x:791.1,y:226.1,alpha:0.5},0).wait(1).to({regX:14.9,regY:17.5,y:226.3,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(14).to({alpha:0.982},0).wait(1).to({alpha:0.964},0).wait(1).to({alpha:0.945},0).wait(1).to({alpha:0.927},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.891},0).wait(1).to({alpha:0.873},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.836},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.5},0).wait(1).to({regX:14.8,regY:17.3,y:226.1},0).wait(1).to({regX:14.9,regY:17.5,y:226.3},0).wait(5).to({_off:true},1).wait(7));

	// Layer 3
	this.instance_9 = new lib.Symbol27();
	this.instance_9.parent = this;
	this.instance_9.setTransform(794.8,435.5,1.02,1.02,0,0,0,16.1,12);
	this.instance_9.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(1).to({regY:12.3,x:794.7,y:435.8},0).wait(1).to({y:435.9},0).wait(1).to({x:794.6},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:436},0).wait(1).to({x:794.5},0).wait(2).to({x:794.4,y:436.1},0).wait(2).to({x:794.3,y:436.2},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:794.2,y:436.3},0).wait(1).to({y:436.2},0).wait(1).to({x:794.1,y:436.3},0).wait(2).to({x:794,y:436.4},0).wait(1).to({x:793.9},0).wait(1).to({y:436.5},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:793.8},0).wait(2).to({x:793.7,y:436.6},0).wait(1).to({x:793.6},0).wait(1).to({y:436.7},0).wait(1).to({x:793.5},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:793.4,y:436.8},0).wait(2).to({x:793.3,y:436.9},0).wait(2).to({x:793.2},0).wait(1).to({y:437},0).wait(1).to({x:793.1},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:793,y:437.1},0).wait(2).to({x:792.9,y:437.2},0).wait(2).to({x:792.8},0).wait(1).to({y:437.3},0).wait(1).to({x:792.7},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:437.4},0).wait(1).to({x:792.6},0).wait(2).to({x:792.5},0).wait(1).to({x:792.4,y:437.5},0).wait(2).to({x:792.3,y:437.6},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({x:792.2},0).wait(1).to({x:792.1,y:437.7,alpha:0.25},0).wait(1).to({alpha:0.5},0).wait(1).to({x:792,y:437.8,alpha:0.75},0).wait(1).to({alpha:1},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:791.9,alpha:0.964},0).wait(1).to({y:437.9,alpha:0.929},0).wait(1).to({x:791.8,alpha:0.893},0).wait(1).to({y:438,alpha:0.858},0).wait(1).to({x:791.7,alpha:0.822},0).wait(1).to({y:438.1,alpha:0.787},0).wait(1).to({x:791.6,alpha:0.751},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.716},0).wait(1).to({x:791.5,y:438.2,alpha:0.68},0).wait(1).to({alpha:0.645},0).wait(1).to({x:791.4,y:438.3,alpha:0.609},0).wait(1).to({alpha:0.574},0).wait(1).to({x:791.2,alpha:0.538},0).wait(1).to({regY:12.1,y:438.1,alpha:0.5},0).wait(1).to({regY:12.3,y:438.3,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(23).to({alpha:0.944},0).wait(1).to({alpha:0.889},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.722},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.611},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.5},0).wait(1).to({regY:12.1,y:438.1},0).wait(1).to({regY:12.3,y:438.3},0).wait(5).to({_off:true},1).wait(7));

	// Layer 4
	this.instance_10 = new lib.Symbol28();
	this.instance_10.parent = this;
	this.instance_10.setTransform(721.4,499,1.02,1.02,0,0,0,14.1,17.1);
	this.instance_10.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(1).to({regY:17.5,y:499.4},0).wait(1).to({x:721.3},0).wait(1).to({y:499.5},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:721.2},0).wait(1).to({x:721.1},0).wait(2).to({x:721,y:499.6},0).wait(2).to({x:720.9},0).wait(2).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:720.8},0).wait(1).to({y:499.7},0).wait(1).to({x:720.7},0).wait(2).to({x:720.6},0).wait(1).to({y:499.8},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:720.5},0).wait(3).to({x:720.4,y:499.9},0).wait(2).to({x:720.3},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:720.2,y:500},0).wait(2).to({x:720.1},0).wait(1).to({x:720},0).wait(3).to({x:719.9},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:500.1},0).wait(1).to({x:719.8},0).wait(2).to({x:719.7},0).wait(1).to({y:500.2},0).wait(1).to({x:719.6},0).wait(2).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({x:719.5,y:500.3},0).wait(2).to({x:719.4},0).wait(2).to({x:719.3,y:500.4,alpha:0.333},0).wait(1).to({alpha:0.667},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:719.2,alpha:1},0).wait(1).to({alpha:0.988},0).wait(1).to({y:500.5,alpha:0.975},0).wait(1).to({x:719.1,alpha:0.962},0).wait(1).to({alpha:0.95},0).wait(1).to({x:719,alpha:0.938},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.925},0).wait(1).to({x:718.9,alpha:0.913},0).wait(1).to({x:718.8,alpha:0.9},0).wait(1).to({y:500.6,alpha:0.887},0).wait(1).to({x:718.7,alpha:0.875},0).wait(1).to({alpha:0.863},0).wait(1).to({alpha:0.85},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:718.6,y:500.7,alpha:0.837},0).wait(1).to({alpha:0.825},0).wait(1).to({x:718.5,alpha:0.813},0).wait(1).to({alpha:0.8},0).wait(1).to({x:718.4,y:500.8,alpha:0.7},0).wait(1).to({alpha:0.6},0).wait(1).to({regX:13.8,regY:17.2,y:500.4,alpha:0.5},0).wait(1).to({regX:14.1,regY:17.5,x:718.7,y:500.7,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.982},0).wait(1).to({alpha:0.964},0).wait(1).to({alpha:0.945},0).wait(1).to({alpha:0.927},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.891},0).wait(1).to({alpha:0.873},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.836},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.815},0).wait(1).to({alpha:0.831},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.877},0).wait(1).to({alpha:0.892},0).wait(1).to({alpha:0.908},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.938},0).wait(1).to({alpha:0.954},0).wait(1).to({alpha:0.969},0).wait(1).to({alpha:0.985},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.5},0).wait(1).to({regX:13.6,regY:17.2,x:718.4,y:500.4},0).wait(1).to({regX:14.1,regY:17.5,x:718.8,y:500.7},0).wait(5).to({_off:true},1).wait(7));

	// Layer 5
	this.instance_11 = new lib.Symbol29();
	this.instance_11.parent = this;
	this.instance_11.setTransform(763.6,475.7,1.02,1.02,0,0,0,12.5,10.7);
	this.instance_11.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(1).to({regY:10.6,x:763.5,y:475.6},0).wait(1).to({y:475.7},0).wait(1).to({x:763.4},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:763.3,y:475.8},0).wait(2).to({x:763.2},0).wait(2).to({x:763.1,y:475.9},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:763},0).wait(1).to({y:476},0).wait(1).to({x:762.9},0).wait(1).to({x:762.8},0).wait(1).to({y:476.1},0).wait(1).to({x:762.7},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:762.6},0).wait(1).to({y:476.2},0).wait(1).to({x:762.5},0).wait(2).to({x:762.4,y:476.3},0).wait(2).to({x:762.3},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:762.2},0).wait(1).to({y:476.4},0).wait(1).to({x:762.1},0).wait(2).to({x:762,y:476.5},0).wait(2).to({scaleX:1.01,scaleY:1.01,x:761.9},0).wait(1).to({y:476.6},0).wait(1).to({x:761.8},0).wait(2).to({x:761.7},0).wait(1).to({y:476.7},0).wait(1).to({x:761.6},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({x:761.5,y:476.8,alpha:1},0).wait(1).to({alpha:0.99},0).wait(1).to({x:761.4,alpha:0.981},0).wait(1).to({x:761.3,y:476.9,alpha:0.971},0).wait(1).to({alpha:0.962},0).wait(1).to({x:761.2,alpha:0.952},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.943},0).wait(1).to({x:761.1,y:477,alpha:0.933},0).wait(1).to({alpha:0.924},0).wait(1).to({x:761,alpha:0.914},0).wait(1).to({y:477.1,alpha:0.905},0).wait(1).to({x:760.9,alpha:0.895},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.886},0).wait(1).to({x:760.8,y:477.2,alpha:0.876},0).wait(1).to({alpha:0.867},0).wait(1).to({x:760.7,alpha:0.857},0).wait(1).to({alpha:0.848},0).wait(1).to({x:760.6,alpha:0.838},0).wait(1).to({y:477.3,alpha:0.829},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:760.5,alpha:0.819},0).wait(1).to({alpha:0.81},0).wait(1).to({x:760.4,y:477.4,alpha:0.8},0).wait(1).to({alpha:0.725},0).wait(1).to({x:760.3,alpha:0.65},0).wait(1).to({alpha:0.575},0).wait(1).to({regX:12.1,regY:10.7,y:477.6,alpha:0.5},0).wait(1).to({regX:12.5,regY:10.6,x:760.6,y:477.4,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.843},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.782},0).wait(1).to({alpha:0.765},0).wait(1).to({alpha:0.747},0).wait(1).to({alpha:0.729},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.694},0).wait(1).to({alpha:0.676},0).wait(1).to({alpha:0.659},0).wait(1).to({alpha:0.641},0).wait(1).to({alpha:0.624},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.588},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.553},0).wait(1).to({alpha:0.535},0).wait(1).to({alpha:0.518},0).wait(1).to({alpha:0.5},0).wait(1).to({regX:12,regY:10.7,x:760.3,y:477.6},0).wait(1).to({regX:12.5,regY:10.6,x:760.7,y:477.4},0).wait(5).to({_off:true},1).wait(7));

	// Layer 6
	this.instance_12 = new lib.Symbol30();
	this.instance_12.parent = this;
	this.instance_12.setTransform(770.4,527.6,1.02,1.02,0,0,0,22.1,27.6);
	this.instance_12.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(1).to({regY:27.5,x:770.3,y:527.5},0).wait(1).to({x:770.2},0).wait(1).to({y:527.6},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:770.1},0).wait(2).to({x:770},0).wait(1).to({x:769.9},0).wait(2).to({x:769.8},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:769.7},0).wait(1).to({y:527.7},0).wait(1).to({x:769.6},0).wait(2).to({x:769.5},0).wait(2).to({x:769.4,y:527.8},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:769.3,y:527.7},0).wait(1).to({y:527.8},0).wait(1).to({x:769.2},0).wait(1).to({x:769.1},0).wait(2).to({x:769},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:768.9,y:527.9},0).wait(2).to({x:768.8},0).wait(2).to({x:768.7},0).wait(2).to({scaleX:1.01,scaleY:1.01,x:768.6},0).wait(2).to({x:768.5,y:528},0).wait(2).to({x:768.4},0).wait(1).to({x:768.3},0).wait(2).to({scaleX:1.01,scaleY:1.01,x:768.2},0).wait(1).to({y:528.1},0).wait(1).to({x:768.1},0).wait(2).to({x:768},0).wait(2).to({x:767.9,alpha:0.2},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.4},0).wait(1).to({x:767.8,alpha:0.6},0).wait(1).to({alpha:0.8},0).wait(1).to({x:767.7,y:528.2,alpha:1},0).wait(1).to({alpha:0.969},0).wait(1).to({x:767.6,alpha:0.938},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.907},0).wait(1).to({x:767.5,alpha:0.876},0).wait(1).to({x:767.4,y:528.3,alpha:0.844},0).wait(1).to({alpha:0.813},0).wait(1).to({x:767.3,alpha:0.782},0).wait(1).to({alpha:0.751},0).wait(1).to({x:767.2,alpha:0.72},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.689},0).wait(1).to({x:767.1,alpha:0.658},0).wait(1).to({x:767,alpha:0.627},0).wait(1).to({alpha:0.596},0).wait(1).to({x:766.9,y:528.4,alpha:0.564},0).wait(1).to({alpha:0.533},0).wait(1).to({regY:27.6,y:528.5,alpha:0.5},0).wait(1).to({regY:27.5,y:528.3,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(27).to({alpha:0.9},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.5},0).wait(1).to({regY:27.6,y:528.5},0).wait(1).to({regY:27.5,y:528.3},0).wait(5).to({_off:true},1).wait(7));

	// Layer 7
	this.instance_13 = new lib.Symbol31();
	this.instance_13.parent = this;
	this.instance_13.setTransform(761,719.2,1.02,1.02,0,0,0,18.9,20.4);
	this.instance_13.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(1).to({regY:20.3,x:760.9,y:719.1},0).wait(1).to({x:760.8},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:760.7},0).wait(2).to({x:760.6},0).wait(2).to({x:760.5},0).wait(2).to({x:760.4},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:719},0).wait(1).to({x:760.3},0).wait(2).to({x:760.2,y:719.1},0).wait(2).to({x:760.1},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:760},0).wait(2).to({x:759.9},0).wait(1).to({x:759.8},0).wait(2).to({x:759.7},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:759.6},0).wait(2).to({x:759.5},0).wait(2).to({x:759.4},0).wait(2).to({x:759.3},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(2).to({x:759.2},0).wait(2).to({x:759.1},0).wait(2).to({x:759},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:758.9},0).wait(2).to({x:758.8},0).wait(2).to({x:758.7},0).wait(2).to({x:758.6},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({x:758.5},0).wait(1).to({alpha:0.5},0).wait(1).to({x:758.4,alpha:1},0).wait(1).to({alpha:0.967},0).wait(1).to({x:758.3,alpha:0.933},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.9},0).wait(1).to({x:758.2,alpha:0.867},0).wait(1).to({alpha:0.833},0).wait(1).to({x:758.1,alpha:0.8},0).wait(1).to({alpha:0.767},0).wait(1).to({x:758,alpha:0.733},0).wait(1).to({x:757.9,alpha:0.7},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.667},0).wait(1).to({x:757.8,alpha:0.633},0).wait(1).to({alpha:0.6},0).wait(1).to({x:757.7,alpha:0.575},0).wait(1).to({alpha:0.55},0).wait(1).to({alpha:0.525},0).wait(1).to({regY:20.4,x:757.6,y:719.2,alpha:0.5},0).wait(1).to({regY:20.3,y:719,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.991},0).wait(1).to({alpha:0.982},0).wait(1).to({alpha:0.973},0).wait(1).to({alpha:0.964},0).wait(1).to({alpha:0.955},0).wait(1).to({alpha:0.945},0).wait(1).to({alpha:0.936},0).wait(1).to({alpha:0.927},0).wait(1).to({alpha:0.918},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.891},0).wait(1).to({alpha:0.882},0).wait(1).to({alpha:0.873},0).wait(1).to({alpha:0.864},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.836},0).wait(1).to({alpha:0.827},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.809},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.767},0).wait(1).to({alpha:0.733},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.633},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.567},0).wait(1).to({alpha:0.533},0).wait(1).to({alpha:0.5},0).wait(1).to({regY:20.4,y:719.2},0).wait(1).to({regY:20.3,y:719},0).wait(5).to({_off:true},1).wait(7));

	// Layer 8
	this.instance_14 = new lib.Symbol21();
	this.instance_14.parent = this;
	this.instance_14.setTransform(571.7,799.3,1.02,1.02,0,0,0,17.3,13.9);
	this.instance_14.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(1).to({x:571.6},0).wait(1).to({y:799.2},0).wait(2).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:571.5},0).wait(4).to({x:571.4},0).wait(2).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:571.3},0).wait(4).to({x:571.2},0).wait(2).to({scaleX:1.02,scaleY:1.02},0).wait(2).to({x:571.1},0).wait(3).to({y:799.1},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:571},0).wait(4).to({x:570.9},0).wait(2).to({x:570.8},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(2).to({y:799},0).wait(2).to({x:570.7},0).wait(3).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({x:570.6},0).wait(2).to({alpha:0.2},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.6},0).wait(1).to({x:570.5,alpha:0.8},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:1},0).wait(1).to({alpha:0.974},0).wait(1).to({alpha:0.948},0).wait(1).to({x:570.4,alpha:0.921},0).wait(1).to({alpha:0.895},0).wait(1).to({x:570.3,alpha:0.869},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:798.9,alpha:0.843},0).wait(1).to({alpha:0.816},0).wait(1).to({alpha:0.79},0).wait(1).to({alpha:0.764},0).wait(1).to({x:570.2,alpha:0.738},0).wait(1).to({alpha:0.711},0).wait(1).to({alpha:0.685},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.659},0).wait(1).to({x:570.1,alpha:0.633},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.554},0).wait(1).to({x:570,alpha:0.528},0).wait(1).to({regX:17.1,regY:13.6,x:570.1,alpha:0.5},0).wait(1).to({regX:17.3,regY:13.9,x:570.2,y:799.2,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(8).to({alpha:0.983},0).wait(1).to({alpha:0.967},0).wait(1).to({alpha:0.95},0).wait(1).to({alpha:0.933},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.883},0).wait(1).to({alpha:0.867},0).wait(1).to({alpha:0.85},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.817},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.775},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.675},0).wait(1).to({alpha:0.65},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.575},0).wait(1).to({alpha:0.55},0).wait(1).to({alpha:0.525},0).wait(1).to({alpha:0.5},0).wait(1).to({regX:17.1,regY:13.5,x:570.1,y:798.9},0).wait(1).to({regX:17.3,regY:13.9,x:570.2,y:799.3},0).wait(5).to({_off:true},1).wait(7));

	// Layer 9
	this.instance_15 = new lib.Symbol22();
	this.instance_15.parent = this;
	this.instance_15.setTransform(526.8,794.4,1.02,1.02,0,0,0,13.3,13.1);
	this.instance_15.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(1).to({x:526.7,y:794.3},0).wait(3).to({scaleX:1.02,scaleY:1.02},0).wait(2).to({x:526.6},0).wait(5).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:526.5},0).wait(6).to({scaleX:1.02,scaleY:1.02,x:526.4},0).wait(2).to({y:794.2},0).wait(2).to({x:526.3},0).wait(3).to({scaleX:1.02,scaleY:1.02},0).wait(2).to({x:526.2},0).wait(5).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({x:526.1,y:794.1},0).wait(6).to({scaleX:1.01,scaleY:1.01,x:526},0).wait(6).to({x:525.9},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:794},0).wait(2).to({x:525.8},0).wait(4).to({scaleX:1.01,scaleY:1.01},0).wait(2).to({x:525.7,alpha:0.143},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.714},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.857},0).wait(1).to({x:525.6,alpha:1},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.6},0).wait(1).to({regY:12.8,alpha:0.5},0).wait(1).to({regY:13.1,y:794.3,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(18).to({alpha:0.964},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.893},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.821},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.679},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.607},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.5},0).wait(1).to({regY:12.7,y:794},0).wait(1).to({regY:13.1,y:794.4},0).wait(5).to({_off:true},1).wait(7));

	// Layer 10
	this.instance_16 = new lib.Symbol23();
	this.instance_16.parent = this;
	this.instance_16.setTransform(625.1,855.6,1.02,1.02,0,0,0,17.7,15.1);
	this.instance_16.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(1).to({y:855.5},0).wait(1).to({x:625},0).wait(2).to({scaleX:1.02,scaleY:1.02},0).wait(2).to({x:624.9},0).wait(1).to({y:855.4},0).wait(2).to({x:624.8},0).wait(2).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:624.7},0).wait(1).to({y:855.3},0).wait(2).to({x:624.6},0).wait(1).to({y:855.2},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:624.5},0).wait(4).to({x:624.4},0).wait(1).to({y:855.1},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:624.3},0).wait(4).to({x:624.2,y:855},0).wait(2).to({x:624.1},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(2).to({x:624},0).wait(1).to({y:854.9},0).wait(2).to({x:623.9},0).wait(1).to({y:854.8},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:1},0).wait(1).to({alpha:0.98},0).wait(1).to({x:623.8,alpha:0.96},0).wait(1).to({alpha:0.94},0).wait(1).to({alpha:0.92},0).wait(1).to({x:623.7,alpha:0.9},0).wait(1).to({y:854.7,alpha:0.88},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.86},0).wait(1).to({alpha:0.84},0).wait(1).to({x:623.6,alpha:0.82},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.813},0).wait(1).to({x:623.5,y:854.6,alpha:0.827},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.84},0).wait(1).to({x:623.4,alpha:0.853},0).wait(1).to({alpha:0.867},0).wait(1).to({alpha:0.88},0).wait(1).to({alpha:0.893},0).wait(1).to({x:623.3,y:854.5,alpha:0.907},0).wait(1).to({alpha:0.92},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.933},0).wait(1).to({x:623.2,alpha:0.947},0).wait(1).to({y:854.4,alpha:0.96},0).wait(1).to({alpha:0.973},0).wait(1).to({alpha:0.987},0).wait(1).to({x:623.1,alpha:1},0).wait(1).to({regY:15.4,alpha:0.5},0).wait(1).to({regY:15.1,y:854.1,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(20).to({alpha:0.958},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.542},0).wait(1).to({alpha:0.5},0).wait(1).to({regY:15.5,y:854.4},0).wait(1).to({regY:15.1,y:854},0).wait(5).to({_off:true},1).wait(7));

	// Layer 11
	this.instance_17 = new lib.Symbol24();
	this.instance_17.parent = this;
	this.instance_17.setTransform(723.7,860.1,1.02,1.02,0,0,0,27.1,12.5);
	this.instance_17.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(1).to({regX:27.3,x:723.9},0).wait(1).to({x:723.8},0).wait(1).to({y:860},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:723.7},0).wait(2).to({x:723.6},0).wait(2).to({x:723.5},0).wait(1).to({y:859.9},0).wait(1).to({x:723.4},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:723.3},0).wait(2).to({y:859.8},0).wait(1).to({x:723.2},0).wait(2).to({x:723.1,y:859.7},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:723},0).wait(3).to({x:722.9},0).wait(2).to({x:722.8,y:859.6},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:722.7},0).wait(3).to({x:722.6},0).wait(1).to({y:859.5},0).wait(1).to({x:722.5},0).wait(1).to({x:722.4},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(2).to({x:722.3,y:859.4},0).wait(2).to({x:722.2},0).wait(2).to({x:722.1},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({y:859.3},0).wait(1).to({x:722},0).wait(2).to({x:721.9,y:859.2},0).wait(1).to({x:721.8},0).wait(2).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({x:721.7},0).wait(2).to({x:721.6,y:859.1},0).wait(2).to({x:721.5,alpha:0.25},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.5},0).wait(1).to({alpha:0.75},0).wait(1).to({x:721.4,y:859,alpha:1},0).wait(1).to({alpha:0.955},0).wait(1).to({x:721.3,alpha:0.91},0).wait(1).to({x:721.2,alpha:0.865},0).wait(1).to({alpha:0.82},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.775},0).wait(1).to({x:721.1,y:858.9,alpha:0.73},0).wait(1).to({alpha:0.685},0).wait(1).to({x:721,alpha:0.64},0).wait(1).to({alpha:0.595},0).wait(1).to({x:720.9,alpha:0.55},0).wait(1).to({regX:27.1,regY:12.4,x:720.7,y:858.8,alpha:0.5},0).wait(1).to({regX:27.3,regY:12.5,x:720.8,y:858.9,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(22).to({alpha:0.95},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.85},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.65},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.55},0).wait(1).to({alpha:0.5},0).wait(1).to({regX:27.1,regY:12.4,x:720.7,y:858.8},0).wait(1).to({regX:27.3,regY:12.5,x:720.8,y:858.9},0).wait(5).to({_off:true},1).wait(7));

	// Layer 12
	this.instance_18 = new lib.Symbol25();
	this.instance_18.parent = this;
	this.instance_18.setTransform(422.4,856.2,1.02,1.02,0,0,0,34.7,15.7);
	this.instance_18.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(1).to({regX:35,x:422.7,y:856.1},0).wait(3).to({scaleX:1.02,scaleY:1.02},0).wait(3).to({y:856},0).wait(4).to({scaleX:1.02,scaleY:1.02},0).wait(2).to({y:855.9},0).wait(5).to({scaleX:1.02,scaleY:1.02,y:855.8},0).wait(5).to({y:855.7},0).wait(2).to({scaleX:1.02,scaleY:1.02},0).wait(4).to({y:855.6},0).wait(3).to({scaleX:1.01,scaleY:1.01},0).wait(3).to({y:855.5},0).wait(4).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({y:855.4},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.957},0).wait(1).to({y:855.3,alpha:0.943},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.929},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.871},0).wait(1).to({y:855.2,alpha:0.857},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.843},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.77},0).wait(1).to({alpha:0.741},0).wait(1).to({y:855.1,alpha:0.711},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.681},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.622},0).wait(1).to({y:855,alpha:0.593},0).wait(1).to({alpha:0.563},0).wait(1).to({alpha:0.533},0).wait(1).to({regX:34.8,x:422.4,alpha:0.5},0).wait(1).to({regX:35,x:422.6,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.984},0).wait(1).to({alpha:0.968},0).wait(1).to({alpha:0.952},0).wait(1).to({alpha:0.935},0).wait(1).to({alpha:0.919},0).wait(1).to({alpha:0.903},0).wait(1).to({alpha:0.887},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.839},0).wait(1).to({alpha:0.823},0).wait(1).to({alpha:0.806},0).wait(1).to({alpha:0.79},0).wait(1).to({alpha:0.774},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.742},0).wait(1).to({alpha:0.726},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.694},0).wait(1).to({alpha:0.677},0).wait(1).to({alpha:0.661},0).wait(1).to({alpha:0.645},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.613},0).wait(1).to({alpha:0.597},0).wait(1).to({alpha:0.581},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.548},0).wait(1).to({alpha:0.532},0).wait(1).to({alpha:0.516},0).wait(1).to({alpha:0.5},0).wait(1).to({regX:34.8,x:422.4},0).wait(1).to({regX:35,x:422.6},0).wait(5).to({_off:true},1).wait(7));

	// Layer 13
	this.instance_19 = new lib.Symbol20();
	this.instance_19.parent = this;
	this.instance_19.setTransform(33,842.2,1.02,1.02,0,0,0,39.7,28.9);
	this.instance_19.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(1).to({regX:40.2,x:33.6,y:842.1},0).wait(2).to({x:33.7},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(2).to({x:33.8},0).wait(1).to({x:33.9},0).wait(1).to({y:842},0).wait(1).to({x:34},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:34.1},0).wait(2).to({x:34.2,y:841.9},0).wait(2).to({x:34.3},0).wait(2).to({x:34.4},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:34.5,y:841.8},0).wait(1).to({x:34.6},0).wait(2).to({x:34.7},0).wait(3).to({scaleX:1.02,scaleY:1.02,x:34.8,y:841.7},0).wait(2).to({x:34.9},0).wait(1).to({x:35},0).wait(1).to({y:841.6},0).wait(1).to({x:35.1},0).wait(2).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({x:35.2},0).wait(1).to({x:35.3},0).wait(2).to({x:35.4},0).wait(1).to({y:841.5},0).wait(1).to({x:35.5},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({x:35.6,y:841.4},0).wait(1).to({alpha:0.25},0).wait(1).to({x:35.7,alpha:0.5},0).wait(1).to({alpha:0.75},0).wait(1).to({x:35.8,alpha:1},0).wait(1).to({alpha:0.983},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:35.9,alpha:0.967},0).wait(1).to({x:36,y:841.3,alpha:0.95},0).wait(1).to({alpha:0.933},0).wait(1).to({alpha:0.917},0).wait(1).to({x:36.1,alpha:0.9},0).wait(1).to({y:841.2,alpha:0.883},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:36.2,alpha:0.867},0).wait(1).to({x:36.3,alpha:0.85},0).wait(1).to({alpha:0.833},0).wait(1).to({x:36.4,alpha:0.817},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.767},0).wait(1).to({x:36.5,y:841.1,alpha:0.734},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.702},0).wait(1).to({x:36.6,alpha:0.669},0).wait(1).to({x:36.7,alpha:0.636},0).wait(1).to({y:841,alpha:0.603},0).wait(1).to({x:36.8,alpha:0.57},0).wait(1).to({alpha:0.538},0).wait(1).to({regX:39.6,x:36.3,alpha:0.5},0).wait(1).to({regX:40.2,x:37,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.982},0).wait(1).to({alpha:0.964},0).wait(1).to({alpha:0.945},0).wait(1).to({alpha:0.927},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.891},0).wait(1).to({alpha:0.873},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.836},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.785},0).wait(1).to({alpha:0.77},0).wait(1).to({alpha:0.755},0).wait(1).to({alpha:0.74},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.695},0).wait(1).to({alpha:0.68},0).wait(1).to({alpha:0.665},0).wait(1).to({alpha:0.65},0).wait(1).to({alpha:0.635},0).wait(1).to({alpha:0.62},0).wait(1).to({alpha:0.605},0).wait(1).to({alpha:0.59},0).wait(1).to({alpha:0.575},0).wait(1).to({alpha:0.56},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.53},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.5},0).wait(1).to({regX:39.5,x:36.4},0).wait(1).to({regX:40.2,x:37.1},0).wait(5).to({_off:true},1).wait(7));

	// Layer 19
	this.instance_20 = new lib.Symbol14();
	this.instance_20.parent = this;
	this.instance_20.setTransform(518,7,1.02,1.02,0,0,0,102.7,17.8);
	this.instance_20.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(1).to({regY:18.1,y:7.4},0).wait(1).to({x:517.9,y:7.5},0).wait(1).to({y:7.6},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:7.7},0).wait(1).to({y:7.8},0).wait(1).to({y:7.9},0).wait(1).to({y:8},0).wait(1).to({y:8.1},0).wait(1).to({x:517.8,y:8.2},0).wait(1).to({y:8.3},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:8.5},0).wait(1).to({y:8.6},0).wait(1).to({y:8.7},0).wait(1).to({y:8.8},0).wait(2).to({x:517.7,y:8.9},0).wait(1).to({y:9},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:9.1},0).wait(1).to({y:9.2},0).wait(1).to({y:9.3},0).wait(1).to({y:9.5},0).wait(1).to({y:9.6},0).wait(1).to({x:517.6,y:9.7},0).wait(1).to({y:9.8},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:9.9},0).wait(1).to({y:10},0).wait(1).to({y:10.1},0).wait(1).to({y:10.2},0).wait(1).to({x:517.5,y:10.3},0).wait(1).to({y:10.4},0).wait(1).to({y:10.5},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:10.6},0).wait(1).to({y:10.7},0).wait(1).to({y:10.8},0).wait(1).to({y:10.9},0).wait(1).to({x:517.4,y:11},0).wait(1).to({y:11.1},0).wait(1).to({y:11.2},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:11.3},0).wait(1).to({y:11.4},0).wait(1).to({y:11.5},0).wait(1).to({y:11.6},0).wait(1).to({x:517.3,y:11.7},0).wait(1).to({y:11.8},0).wait(1).to({y:11.9},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:12},0).wait(1).to({y:12.1},0).wait(1).to({x:517.2,y:12.2},0).wait(1).to({y:12.3},0).wait(1).to({y:12.4},0).wait(1).to({y:12.5},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:12.7},0).wait(1).to({x:517.1},0).wait(1).to({y:12.8},0).wait(1).to({y:12.9},0).wait(1).to({y:13,alpha:0.495},0).wait(1).to({y:13.1,alpha:0.99},0).wait(1).to({y:13.2,alpha:0.93},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:13.3,alpha:0.87},0).wait(1).to({x:517,y:13.4,alpha:0.81},0).wait(1).to({y:13.5,alpha:0.75},0).wait(1).to({y:13.6,alpha:0.69},0).wait(1).to({y:13.8,alpha:0.63},0).wait(1).to({y:13.9,alpha:0.57},0).wait(1).to({regX:102.3,regY:17.4,y:13.5,alpha:0.5},0).wait(1).to({regX:102.7,regY:18.1,x:517.4,y:14.2,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(19).to({alpha:0.985},0).wait(1).to({alpha:0.969},0).wait(1).to({alpha:0.954},0).wait(1).to({alpha:0.938},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.908},0).wait(1).to({alpha:0.892},0).wait(1).to({alpha:0.877},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.831},0).wait(1).to({alpha:0.815},0).wait(1).to({alpha:0.8},0).wait(1).to({regX:102.2,regY:17.4,x:517,y:13.6,alpha:0.801},0).wait(1).to({regX:102.7,regY:18.1,x:517.5,y:14.3,alpha:0.8},0).wait(5).to({_off:true},1).wait(7));

	// Layer 14
	this.instance_21 = new lib.Symbol19();
	this.instance_21.parent = this;
	this.instance_21.setTransform(215.4,160.6,1.02,1.02,0,0,0,218.7,167.3);
	this.instance_21.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(1).to({regX:219,regY:167.8,x:215.8,y:161.2},0).wait(1).to({y:161.3},0).wait(1).to({y:161.4},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({y:161.5},0).wait(1).to({x:215.9,y:161.6},0).wait(1).to({y:161.7},0).wait(2).to({x:216,y:161.9},0).wait(2).to({scaleX:1.02,scaleY:1.02,y:162},0).wait(1).to({y:162.1},0).wait(1).to({x:216.1,y:162.2},0).wait(2).to({y:162.3},0).wait(1).to({y:162.4},0).wait(1).to({x:216.2,y:162.5},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:162.6},0).wait(1).to({x:216.3,y:162.7},0).wait(2).to({y:162.8},0).wait(1).to({y:162.9},0).wait(1).to({y:163},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:216.4,y:163.2},0).wait(2).to({x:216.5,y:163.3},0).wait(1).to({y:163.4},0).wait(1).to({y:163.5},0).wait(1).to({y:163.6},0).wait(1).to({x:216.6},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:163.7},0).wait(1).to({y:163.8},0).wait(1).to({y:163.9},0).wait(1).to({y:164},0).wait(1).to({x:216.7,y:164.1},0).wait(2).to({x:216.8,y:164.2,alpha:0.028},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:164.3,alpha:0.057},0).wait(1).to({y:164.4,alpha:0.085},0).wait(1).to({y:164.5,alpha:0.114},0).wait(1).to({y:164.6,alpha:0.142},0).wait(1).to({x:216.9,alpha:0.171},0).wait(1).to({y:164.7,alpha:0.199},0).wait(1).to({y:164.8,alpha:0.228},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:164.9,alpha:0.256},0).wait(1).to({x:217,alpha:0.285},0).wait(1).to({y:165,alpha:0.313},0).wait(1).to({y:165.1,alpha:0.342},0).wait(1).to({x:217.1,y:165.2,alpha:0.37},0).wait(1).to({y:165.3,alpha:0.399},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:165.4,alpha:0.427},0).wait(1).to({alpha:0.456},0).wait(1).to({y:165.5,alpha:0.484},0).wait(1).to({x:217.2,y:165.6,alpha:0.513},0).wait(1).to({y:165.7,alpha:0.541},0).wait(1).to({x:217.3,y:165.8,alpha:0.57},0).wait(1).to({y:165.9,alpha:0.598},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:166,alpha:0.627},0).wait(1).to({alpha:0.655},0).wait(1).to({y:166.1,alpha:0.684},0).wait(1).to({x:217.4,y:166.2,alpha:0.712},0).wait(1).to({y:166.3,alpha:0.741},0).wait(1).to({alpha:0.769},0).wait(1).to({regX:218.4,regY:167.2,x:217,y:165.9,alpha:0.801},0).wait(1).to({regX:219,regY:167.8,x:217.6,y:166.6,alpha:0.817},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.85},0).wait(1).to({alpha:0.867},0).wait(1).to({alpha:0.883},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.933},0).wait(1).to({alpha:0.95},0).wait(1).to({alpha:0.967},0).wait(1).to({alpha:0.983},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.983},0).wait(1).to({alpha:0.967},0).wait(1).to({alpha:0.95},0).wait(1).to({alpha:0.933},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.883},0).wait(1).to({alpha:0.867},0).wait(1).to({alpha:0.85},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.817},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.836},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.873},0).wait(1).to({alpha:0.891},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.927},0).wait(1).to({alpha:0.945},0).wait(1).to({alpha:0.964},0).wait(1).to({alpha:0.982},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.975},0).wait(1).to({alpha:0.95},0).wait(1).to({alpha:0.925},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.85},0).wait(1).to({alpha:0.825},0).wait(1).to({alpha:0.8},0).wait(1).to({regX:218.3,regY:167.2,x:217,y:166,alpha:0.801},0).wait(1).to({regX:219,regY:167.8,x:217.7,y:166.7,alpha:0.8},0).wait(5).to({_off:true},1).wait(7));

	// Layer 15
	this.instance_22 = new lib.Symbol18();
	this.instance_22.parent = this;
	this.instance_22.setTransform(545.6,46.2,1.02,1.02,0,0,0,21.7,23.6);
	this.instance_22.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(1).to({regY:23.5,x:545.5},0).wait(1).to({y:46.3},0).wait(1).to({x:545.4,y:46.4},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:46.5},0).wait(1).to({y:46.6},0).wait(1).to({y:46.7},0).wait(1).to({y:46.8},0).wait(1).to({x:545.3,y:46.9},0).wait(1).to({y:47},0).wait(1).to({y:47.1},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:47.2},0).wait(1).to({y:47.3},0).wait(1).to({y:47.4},0).wait(1).to({x:545.2},0).wait(1).to({y:47.5},0).wait(1).to({y:47.6},0).wait(1).to({y:47.7},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:47.8},0).wait(1).to({x:545.1,y:47.9},0).wait(1).to({y:48},0).wait(1).to({x:545,y:48.1},0).wait(1).to({y:48.2},0).wait(1).to({y:48.3},0).wait(1).to({y:48.4},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:48.5},0).wait(1).to({y:48.6},0).wait(1).to({x:544.9,y:48.7},0).wait(1).to({y:48.8},0).wait(1).to({y:48.9},0).wait(1).to({y:49},0).wait(1).to({y:49.1},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:544.8,y:49.2},0).wait(1).to({y:49.3},0).wait(1).to({y:49.4},0).wait(1).to({x:544.7,y:49.5},0).wait(1).to({y:49.6},0).wait(1).to({y:49.7},0).wait(1).to({y:49.8},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:49.9},0).wait(1).to({x:544.6,y:50},0).wait(1).to({y:50.1},0).wait(1).to({y:50.2},0).wait(2).to({y:50.3},0).wait(1).to({x:544.5,y:50.4},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:50.5},0).wait(1).to({y:50.6},0).wait(1).to({y:50.7},0).wait(1).to({y:50.8},0).wait(1).to({x:544.4,y:50.9},0).wait(1).to({y:51},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:51.1},0).wait(1).to({x:544.3,y:51.2},0).wait(1).to({y:51.3},0).wait(1).to({y:51.4},0).wait(1).to({y:51.5},0).wait(1).to({y:51.6},0).wait(1).to({x:544.2,y:51.7},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:51.8},0).wait(1).to({y:51.9},0).wait(1).to({y:52},0).wait(1).to({alpha:0.125},0).wait(1).to({x:544.1,y:52.1,alpha:0.25},0).wait(1).to({y:52.2,alpha:0.375},0).wait(1).to({y:52.4,alpha:0.5},0).wait(1).to({y:52.3,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(24).to({alpha:0.938},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.813},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.688},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.563},0).wait(1).to({alpha:0.5},0).wait(1).to({y:52.4},0).wait(1).to({y:52.3},0).wait(5).to({_off:true},1).wait(7));

	// Layer 17
	this.instance_23 = new lib.Symbol16();
	this.instance_23.parent = this;
	this.instance_23.setTransform(332.5,578.1,1.02,1.02,0,0,0,151.8,193.1);
	this.instance_23.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(1).to({regX:151.9},0).wait(1).to({x:332.6},0).wait(2).to({scaleX:1.02,scaleY:1.02,y:578.2},0).wait(1).to({x:332.7},0).wait(1).to({x:332.6},0).wait(1).to({x:332.7},0).wait(1).to({y:578.3},0).wait(3).to({scaleX:1.02,scaleY:1.02,x:332.8},0).wait(1).to({x:332.7},0).wait(1).to({x:332.8,y:578.4},0).wait(3).to({x:332.9},0).wait(1).to({y:578.5},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(3).to({x:333},0).wait(1).to({y:578.6},0).wait(3).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({y:578.7},0).wait(1).to({x:333.1},0).wait(4).to({y:578.8},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:333.2},0).wait(3).to({y:578.9},0).wait(2).to({x:333.3},0).wait(1).to({alpha:0.2},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.4},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.8},0).wait(1).to({y:579,alpha:1},0).wait(1).to({x:333.4,alpha:0.985},0).wait(1).to({alpha:0.969},0).wait(1).to({alpha:0.954},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:333.5,y:579.1,alpha:0.938},0).wait(1).to({x:333.4,alpha:0.923},0).wait(1).to({x:333.5,alpha:0.908},0).wait(1).to({alpha:0.892},0).wait(1).to({alpha:0.877},0).wait(1).to({y:579.2,alpha:0.862},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.846},0).wait(1).to({x:333.6,alpha:0.831},0).wait(1).to({alpha:0.815},0).wait(1).to({y:579.3,alpha:0.8},0).wait(1).to({alpha:0.77},0).wait(1).to({x:333.7,alpha:0.741},0).wait(1).to({x:333.6,alpha:0.711},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:333.7,alpha:0.681},0).wait(1).to({y:579.4,alpha:0.652},0).wait(1).to({alpha:0.622},0).wait(1).to({x:333.8,alpha:0.593},0).wait(1).to({alpha:0.563},0).wait(1).to({y:579.5,alpha:0.533},0).wait(1).to({regY:192.8,alpha:0.5},0).wait(1).to({regY:193.1,x:333.7,y:579.8,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(20).to({alpha:0.958},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.542},0).wait(1).to({alpha:0.5},0).wait(1).to({regY:192.7,x:333.8,y:579.5},0).wait(1).to({regY:193.1,x:333.7,y:579.9},0).wait(5).to({_off:true},1).wait(7));

	// Layer 18
	this.instance_24 = new lib.Symbol15();
	this.instance_24.parent = this;
	this.instance_24.setTransform(85.9,567.5,1.02,1.02,0,0,0,91.5,182.7);
	this.instance_24.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(1).to({regX:92,regY:182.6,x:86.4,y:567.4},0).wait(1).to({x:86.5},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:86.6,y:567.5},0).wait(3).to({x:86.7},0).wait(1).to({x:86.8,y:567.6},0).wait(3).to({scaleX:1.02,scaleY:1.02,x:86.9},0).wait(1).to({y:567.7},0).wait(2).to({x:87},0).wait(1).to({x:87.1},0).wait(1).to({y:567.8},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:87.2},0).wait(3).to({x:87.3,y:567.9},0).wait(1).to({x:87.4},0).wait(3).to({scaleX:1.02,scaleY:1.02,x:87.5},0).wait(2).to({x:87.6,y:568},0).wait(2).to({x:87.7},0).wait(2).to({x:87.8,y:568.1},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(2).to({x:87.9},0).wait(1).to({y:568.2},0).wait(1).to({x:88},0).wait(2).to({x:88.1},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({y:568.3},0).wait(1).to({x:88.2},0).wait(2).to({x:88.3},0).wait(1).to({y:568.4},0).wait(1).to({x:88.4},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(2).to({x:88.5},0).wait(2).to({x:88.6,y:568.5},0).wait(2).to({scaleX:1.01,scaleY:1.01,x:88.7},0).wait(2).to({y:568.6},0).wait(1).to({x:88.8},0).wait(2).to({x:88.9},0).wait(1).to({alpha:0.25},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:89,y:568.7,alpha:0.5},0).wait(1).to({alpha:0.75},0).wait(1).to({x:89.1,alpha:1},0).wait(1).to({alpha:0.875},0).wait(1).to({y:568.8,alpha:0.75},0).wait(1).to({x:89.2,alpha:0.625},0).wait(1).to({regX:91.2,x:88.8,y:568.9,alpha:0.5},0).wait(1).to({regX:92,x:89.6,y:568.8,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(12).to({alpha:0.967},0).wait(1).to({alpha:0.933},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.867},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.779},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.736},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.693},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.65},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.607},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.564},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.521},0).wait(1).to({alpha:0.5},0).wait(1).to({regX:91,x:88.7,y:568.9},0).wait(1).to({regX:92,x:89.7,y:568.8},0).wait(5).to({_off:true},1).wait(7));

	// Layer 20
	this.instance_25 = new lib.Symbol13();
	this.instance_25.parent = this;
	this.instance_25.setTransform(209.4,354.2,1.02,1.02,0,0,0,161.2,19.5);
	this.instance_25.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(1).to({regX:161.1},0).wait(1).to({y:354.3},0).wait(2).to({scaleX:1.02,scaleY:1.02,x:209.5,y:354.4},0).wait(1).to({y:354.5},0).wait(2).to({y:354.6},0).wait(1).to({x:209.6},0).wait(1).to({y:354.7},0).wait(1).to({y:354.8},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({x:209.7,y:354.9},0).wait(1).to({y:355},0).wait(2).to({y:355.1},0).wait(1).to({x:209.8},0).wait(1).to({y:355.2},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:355.3},0).wait(1).to({x:209.9},0).wait(1).to({y:355.4},0).wait(1).to({y:355.5},0).wait(2).to({x:210,y:355.6},0).wait(2).to({scaleX:1.02,scaleY:1.02,y:355.7},0).wait(1).to({y:355.8},0).wait(1).to({x:210.1},0).wait(1).to({y:355.9},0).wait(2).to({y:356},0).wait(1).to({x:210.2,y:356.1},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({y:356.2},0).wait(1).to({x:210.3,y:356.3},0).wait(2).to({y:356.4},0).wait(1).to({y:356.5},0).wait(1).to({x:210.4},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:356.6},0).wait(1).to({y:356.7},0).wait(2).to({x:210.5},0).wait(1).to({y:356.8},0).wait(1).to({y:356.9,alpha:0.143},0).wait(1).to({x:210.6,alpha:0.286},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:357,alpha:0.429},0).wait(1).to({y:357.1,alpha:0.571},0).wait(1).to({alpha:0.714},0).wait(1).to({x:210.7,y:357.2,alpha:0.857},0).wait(1).to({y:357.3,alpha:1},0).wait(1).to({alpha:0.982},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:357.4,alpha:0.964},0).wait(1).to({x:210.8,y:357.5,alpha:0.945},0).wait(1).to({alpha:0.927},0).wait(1).to({y:357.6,alpha:0.909},0).wait(1).to({y:357.7,alpha:0.891},0).wait(1).to({x:210.9,alpha:0.873},0).wait(1).to({y:357.8,alpha:0.855},0).wait(1).to({scaleX:1.01,scaleY:1.01,alpha:0.836},0).wait(1).to({x:211,y:357.9,alpha:0.818},0).wait(1).to({alpha:0.8},0).wait(1).to({y:358,alpha:0.725},0).wait(1).to({y:358.1,alpha:0.65},0).wait(1).to({x:211.1,alpha:0.575},0).wait(1).to({regX:161.5,regY:19.7,x:211.2,y:358.2,alpha:0.5},0).wait(1).to({regX:161.1,regY:19.5,x:210.7,y:358,alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.978},0).wait(1).to({alpha:0.956},0).wait(1).to({alpha:0.933},0).wait(1).to({alpha:0.911},0).wait(1).to({alpha:0.889},0).wait(1).to({alpha:0.867},0).wait(1).to({alpha:0.844},0).wait(1).to({alpha:0.822},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.822},0).wait(1).to({alpha:0.844},0).wait(1).to({alpha:0.867},0).wait(1).to({alpha:0.889},0).wait(1).to({alpha:0.911},0).wait(1).to({alpha:0.933},0).wait(1).to({alpha:0.956},0).wait(1).to({alpha:0.978},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.962},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.885},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.808},0).wait(1).to({alpha:0.769},0).wait(1).to({alpha:0.731},0).wait(1).to({alpha:0.692},0).wait(1).to({alpha:0.654},0).wait(1).to({alpha:0.615},0).wait(1).to({alpha:0.577},0).wait(1).to({alpha:0.538},0).wait(1).to({alpha:0.5},0).wait(1).to({regX:161.6,regY:19.7,x:211.1,y:358.2},0).wait(1).to({regX:161.1,regY:19.5,x:210.6,y:358},0).wait(5).to({_off:true},1).wait(7));

	// Layer 1
	this.instance_26 = new lib.bg();
	this.instance_26.parent = this;
	this.instance_26.setTransform(-7,-10,0.408,0.408);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8B0304").s().p("Eg/5BEpMAAAiJRMB/zAAAMAAACJRg");
	this.shape.setTransform(409,429.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_26}]}).to({state:[{t:this.instance_26}]},115).to({state:[]},1).wait(7));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(388.2,417.1,829.8,1589.3);
// library properties:
lib.properties = {
	width: 800,
	height: 862,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"images/texto1.png", id:"AsnossascrençassetransformamempensamentosNossospensa"},
		{src:"images/bg.png", id:"bg"},
		{src:"images/Camada1.png", id:"Camada1"},
		{src:"images/Camada1copy.png", id:"Camada1copy"},
		{src:"images/Camada3.png", id:"Camada3"},
		{src:"images/Camada4.png", id:"Camada4"},
		{src:"images/texto2.png", id:"FelizNataleumprósperoanonovo"},
		{src:"images/Layer1.jpg", id:"Layer1"},
		{src:"images/Layer10.jpg", id:"Layer10"},
		{src:"images/Layer11.jpg", id:"Layer11"},
		{src:"images/Layer12.jpg", id:"Layer12"},
		{src:"images/Layer13.jpg", id:"Layer13"},
		{src:"images/Layer14.jpg", id:"Layer14"},
		{src:"images/Layer15.jpg", id:"Layer15"},
		{src:"images/Layer17.jpg", id:"Layer17"},
		{src:"images/Layer18.jpg", id:"Layer18"},
		{src:"images/Layer19.jpg", id:"Layer19"},
		{src:"images/Layer2.jpg", id:"Layer2"},
		{src:"images/Layer20.jpg", id:"Layer20"},
		{src:"images/Layer3.jpg", id:"Layer3"},
		{src:"images/Layer4.jpg", id:"Layer4"},
		{src:"images/Layer5.jpg", id:"Layer5"},
		{src:"images/Layer6.jpg", id:"Layer6"},
		{src:"images/Layer7.jpg", id:"Layer7"},
		{src:"images/Layer8.jpg", id:"Layer8"},
		{src:"images/Layer9.jpg", id:"Layer9"},
		{src:"images/texto3.png", id:"SCAcopiar"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;