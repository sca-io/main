<?
$debug = true;  //caso true não remove os arquivos
if($_SERVER['SERVER_NAME'] == "atomicaserv" || $_SERVER['SERVER_NAME'] == "localhost"){
	$dirSite = "/scalcool/";
	ini_set('display_errors', '1');
	error_reporting(E_ALL);
}else{
	$dirSite = "/";
	ini_set('display_errors', '0');
	error_reporting(0);
}
set_time_limit(0);
ignore_user_abort(1);

$arrDirVarredura = array("arquivos/"); //diretorio de varredura
$arquivosExcessao = array("data_ag.txt","teste.php");
$urlArqAgendamnt = $_SERVER['DOCUMENT_ROOT'].$dirSite."arquivos/data_ag.txt";
$intervalo = 24*60; //em minutos
$emailNotices = array("dvrusso@atomica.com.br");
$dirPhpMailer = $_SERVER['DOCUMENT_ROOT'].$dirSite."classes/PhpMailer/";
$endSite = $_SERVER['SERVER_NAME'].$dirSite;

$vMimeType = Array(
    '' => Array(''),
    'doc'  => Array('application/msword'),
    'docx' => Array('application/vnd.openxmlformats-officedocument.wordprocessingml.document',"application/zip"),
    'ppt'  => Array('application/vnd.ms-powerpoint','application/vnd.ms-office'),
    'pptx' => Array('application/vnd.openxmlformats-officedocument.presentationml.presentation',"application/zip"),    
    'xls'  => Array('application/vnd.ms-excel'),
    'xlsx' => Array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',"application/zip"),
    'pdf'  => Array('application/pdf'),
    'swf'  => Array('application/x-shockwave-flash'),
    'mp3'  => Array('audio/mpeg','audio/mp3','application/octet-stream'),
    'jpg'  => Array('image/pjpeg','image/jpeg'),
    'gif'  => Array('image/gif'),
    'png'  => Array('image/png','image/x-png')
);

$dispararEmail = false;
$arquivosCapturados = "";


function getExtArquivo($file){
    $arr = explode(".",$file);
    return end($arr);
}
function validarArquivo($ext,$mimetype){
    global $vMimeType;
    $showComp = 0;
    $ext = strtolower($ext);
    if(isset($vMimeType[$ext])){
        if($mimetype == "no_mime_function"){
            return true;
        }else{
            if($showComp) echo "<br />";
            foreach($vMimeType[$ext] as $mime){
                if($showComp) echo "<b>".$mime ."==". $mimetype."</b><br />";
                if($mime == $mimetype){
                    return true;	
                }
            }
        }
    }	
    return false;
}

if (!function_exists ('mime_content_type') ){	
   function mime_content_type ( $file ){
       if(function_exists ('finfo_open') && function_exists ('finfo_file') && defined("FILEINFO_MIME_TYPE")){
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $file);
            finfo_close($finfo);
            return 	$mime;
       }else{
            return "no_mime_function";
       }
   }
}


function eliminarArquivos($dir){
    global $extensoePermitidas,$dispararEmail,$arquivosCapturados,$debug,$finfo,$arquivosExcessao;
    if($debug) print "<br /><h2><i>".str_replace($_SERVER['DOCUMENT_ROOT'],"",$dir)."</i></h2><br />";
    $ponteiro  = opendir($dir);
    while ($filename = readdir($ponteiro)) {
        if ($filename == "." || $filename==".." || $filename=='.svn') continue;
        $style="";
        $tipo = filetype($dir.$filename);		
        if($tipo == "dir"){
                eliminarArquivos($dir.$filename."/");			
        }else{
            $excluir = true;
            if(validarArquivo(getExtArquivo($filename),mime_content_type($dir.$filename))) $excluir = false;
            //if(validarArquivo(getExtArquivo($filename),finfo_file($finfo, $dir.$filename))) $excluir = false;
            if(in_array($filename,$arquivosExcessao)) $excluir = false;							
            if($excluir) {
                $arquivosCapturados .= $filename."<br />";
                //if(!$debug) unlink($dir.$filename);
                $dispararEmail = true;
                $style = "style='color:red'";
            }
            if($debug) echo "<span $style>".$filename." - ".mime_content_type($dir.$filename)."</span><br />";
        }	
    }
    closedir($ponteiro);
}


function comparaDatas($dat1,$dat2){	
    $data = mktime(substr($dat1,11,2),substr($dat1,14,2),substr($dat1,17,2),substr($dat1,5,2),substr($dat1,8,2),substr($dat1,0,4));
    $data_atual = mktime(substr($dat2,11,2),substr($dat2,14,2),substr($dat2,17,2),substr($dat2,5,2),substr($dat2,8,2),substr($dat2,0,4));	
    return ceil (($data_atual - $data)/60);
}
$dif = 0;
if (file_exists($urlArqAgendamnt)){
    $data = file_get_contents($urlArqAgendamnt);
    $dif = (int)comparaDatas($data,date("Y-m-d H:i:s"));
}else{
    $dif = $intervalo+1;
}
//print $dif > $intervalo;
if($dif > $intervalo){
    foreach($arrDirVarredura as $dir){	
        eliminarArquivos($_SERVER['DOCUMENT_ROOT'].$dirSite.$dir);
    }
    if($_SERVER['SERVER_NAME'] == "atomicaserv") $dispararEmail = false;
    if($dispararEmail){
        $dadosSmtp = array();
        require_once($dirPhpMailer.'class.phpmailer.php');		
        if($_SERVER['SERVER_NAME'] == "atomicaserv") require_once($_SERVER['DOCUMENT_ROOT'].'/dados-smtp.php');       

        foreach($emailNotices as $email){
            $mail = new PHPMailer();
            if(count($dadosSmtp) > 0){
                $mail->IsSMTP();	
                $mail->Host = $dadosSmtp["Host"]; 			//Host do servidor SMTP
                $mail->SMTPAuth = true;
                $mail->Username = $dadosSmtp["Username"];   //Nome do usuario SMTP
                $mail->Password = $dadosSmtp["Password"];   //Senha do usuario SMTP	
            }			
            $mail->SetLanguage("br", $dirPhpMailer); 		//Idioma
            $mail->From = "sapo@atomica.com.br";    		//nome de quem ta enviando, vai aparecer na coluna "De:"
            $mail->FromName = "Monitor Atomica Studio";    //nome de quem ta enviando, vai aparecer na coluna "De:"
            $mail->AddAddress($email);
            $mail->IsHTML(true);
            $mail->CharSet = "UTF-8";
            $mail->Subject = "Notificação de checagem de arquivos";
            $mail->Body = "A checagem de arquivos detectou e excluiu os seguintes arquivos:<br />".$arquivosCapturados."<br /><br /> SITE:".$endSite." <br />DATA: ".date("Y-m-d H:i:s");
            $mail->Send();			
        }
    }
    print "Processo Finalizado!";
    $data = file_put_contents($urlArqAgendamnt,date("Y-m-d H:i:s"));	
}
?>