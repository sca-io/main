<? require_once ('conf.php');
$idSecao = SEJA_NOSSO_CLIENTE;
$idioma = getIdioma();
$nome_secao = $has_lista_icones = $nome_secao_atual = "";
if($settSecoes[$idSecao]){
    $nome_secao = getNomeSecao($idSecao);	
    $metaTitle = $nome_secao;
}
$res = request("res");

$idDestaque = 0;
$where = "idioma = '$idioma' AND status = ".Chamada::PUBLICADO."";
$listaChs = Chamada::listar($idSecao,$idDestaque,"id,titulo,texto",$where," data_publicacao DESC","1");
$chamada = null;
if($listaChs){
    $chamada = $listaChs[0];    
} 
$tituloInicial = $textoInicial = "";
if($chamada){
    $tituloInicial = $chamada->getTitulo();
    $textoInicial = $chamada->getTexto();
    
}
$tiposCliente = TipoCliente::listar("id, nome,descricao","status = ".ATIVO." AND idioma = '$idioma'");
$hasTextoTipoCliente = false;

?><? include "includes/head.php"?>
<body>
<div id="sca">
    <div class="container">
      <div class="conteudo">      
        <? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <a href="javascript:;"><strong><?=$nome_secao?></strong></a>
            </div>
        </div>        
        <div class="empresa gestao-de-pessoas seja-nosso-cliente">
            <form id="fomCliente" action="">
                <div class="esquerda">
                    <div class="txt-box">
                        <div>
                            <h2 class="tit-empresa"><img src="<?=DIRETORIO_RAIZ?>css/img/tit-seja-nosso-cliente<?=getTextoByLang("","-eng")?>.png" alt="<?=$nome_secao?>" title="<?=$nome_secao?>" /></h2>
                            <h3><?=response_html($tituloInicial)?></h3>
                            <?=response_html($textoInicial)?>
                            <ul class="tp-radio">                                 
                                <? foreach($tiposCliente as $k=>$tipo):?> 
                                <? if($tipo->getDescricao()) $hasTextoTipoCliente = true;?>
                                <li class="distancia"><input type="radio" id="tipo-cliente-<?=secureResponse($tipo->getId())?>" name="tipoCliente" onchange="changeTextoTipoCliente(this)" value="<?=secureResponse($tipo->getId())?>" <? if($k == 0):?>checked="checked"<? endif;?> /><label for="tipo-cliente-<?=secureResponse($tipo->getId())?>"><?=response_html($tipo->getNome())?></label></li>
                                <? endforeach;?>
                            </ul>
                        </div>
                    </div>
                    <div class="formulario">
                        <div> 
                            <? if($tiposCliente && $hasTextoTipoCliente):?>
                            <div class="cont-form" id="textos-tipo-cliente">
                                <? foreach($tiposCliente as $k=>$tipo):?> 
                                    <? if($tipo->getDescricao()):?>
                                    <div <?=($k>0)?'class="oculto"':""?> id="texto-tipo-<?=secureResponse($tipo->getId())?>">
                                        <h3><?=response_html($tipo->getNome())?></h3>
                                        <?=response_html($tipo->getDescricao())?>
                                    </div>
                                    <? endif;?>
                                <? endforeach;?>
                            </div>
                            <? endif;?>
                            <ul class="form" id="content-form">
                                <li><label id="lbl-nome"><?=getTextoByLang("Nome","Name")?></label><input type="text" id="nome" name="nome" /></li>
                                <li><label id="lbl-email">E-mail</label><input type="text" id="email" name="email" /></li>
                                <li><label id="lbl-mensagem"><?=getTextoByLang("Mensagem","Message")?></label><textarea rows="" cols="" id="mensagem" name="mensagem"></textarea></li>
                                <li><a href="javascript:salvarCliente()" class="bt-enviar<?=getTextoByLang("","-eng")?>" title="<?=getTextoByLang("Enviar","Submit")?>"><?=getTextoByLang("Enviar","Submit")?></a></li>
                            </ul>
                            <div id="box-msg" class="msg oculto"></div>                            
                        </div>
                    </div>

                </div>    
            </form>    
        </div>
        
      </div>
    </div>
    <? include "includes/rodape.php"?>
</div>
</body>
</html>