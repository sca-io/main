<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class comment
 */
class Comment extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('comment_model');
    }

    public function index()
    {
        $this->data['list'] = $this->comment_model->get()->result();

        $this->loadJs(array(
            array(
                'name' => 'jquery.dataTables.min',
                'path' => 'assets/dataTables/media/js/'
            ),
            array(
                'name' => 'dataTables.bootstrap.min',
                'path' => 'assets/dataTables/media/js/'
            ),
            array(
                'name' => 'dataTables.responsive.min',
                'path' => 'assets/dataTables/media/js/'
            ),
            array(
                'name' => 'responsive.bootstrap.min',
                'path' => 'assets/dataTables/media/js/'
            ),
            array(
                'name' => 'dataTables',
                'path' => 'assets/dataTables/'
            )
        ));

        $this->loadCss(array(
            array(
                'name' => 'dataTables.bootstrap.min',
                'path' => 'assets/dataTables/media/css/'
            ),
            array(
                'name' => 'responsive.bootstrap.min',
                'path' => 'assets/dataTables/media/css/'
            ),
            array(
                'name' => 'shadowbox',
                'path' => 'assets/shadowbox/'
            )
        ));

        parent::renderer();
    }

    public function edit($id = NULL)
    {

        if ($this->uri->segment(3) == 'editar' && (int)$id === 0) {
            redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/novo');
        } elseif ($id > 0) {
            $data = $this->comment_model->get(array('id' => $id))->result();
            if (count($data) > 0) {
                $data = current($data);
                $this->data['data'] = $data;
            }
        }
        $this->loadJs(array(
            'name' => 'ckeditor',
            'path' => 'assets/ckeditor/'
        ));


        parent::renderer();
    }

    public function save($id = NULL)
    {
        $id = (int)$id;
        if ($this->input->post()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('text', 'Texto', 'trim|required');

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
                if ($id === 0) {
                    $redirect = '/novo';
                } else {
                    $redirect = '/editar/' . $id;
                }
                redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . $redirect);
            } else {
                $data = array(
                    'text' => $this->input->post('text')
                );

                if ($id === 0) {
                    $id = $this->comment_model->insert($data, true);
                } else {
                    $this->comment_model->update(array('id' => $id), $data);
                }
                if ($id === 0) {
                    $this->setError('Falha na importação dos dados. Entre em contato com o administrador para análise dos logs.');
                } else {
                    $this->setMsg('Dados gravados com sucesso.');
                }
            }
        } else {
            $this->setError('Ocorreu um erro ao processar o formulario, tente novamente mais tarde.');
        }
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }

    public function delete($id)
    {
        $this->comment_model->delete(array('id' => $id));
        $this->setMsg('Registro excluido com sucesso.');
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
}