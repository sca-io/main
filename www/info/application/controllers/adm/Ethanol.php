<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Ethanol extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('ethanol_model');
    }

    private function get_city()
    {
        $this->load->model('city_model');
        $city = $this->city_model->get_related()->result();

        $this->data['city'] = array();
        $this->data['state'] = array();

        foreach ($city as $row) {
            if (!isset($this->data['state'][$row->state_id])) {
                $this->data['state'][$row->state_id] = new stdClass();
                $this->data['state'][$row->state_id]->id = $row->state_id;
                $this->data['state'][$row->state_id]->name = $row->state_name;
            }
            $this->data['city'][$row->id] = $row;
        }
    }

    public function index()
    {

        $this->get_city();

        $where = array();

        $this->data['end'] = $this->input->post('end') ? $this->input->post('end') : date('Y-m-d');
        $this->data['start'] = $this->input->post('start') ? $this->input->post('start') : date('Y-m-d', strtotime(date('Y-m-d') . ' -10 week'));

        $where['datetime <='] = $this->data['end'] . ' 00:00:00';
        $where['datetime >='] = $this->data['start'] . ' 23:59:59';


        $this->data['list'] = $this->ethanol_model->get($where)->result();

        $this->loadJs(array(
            array(
                'name' => 'jquery.dataTables.min',
                'path' => 'assets/dataTables/media/js/'
            ),
            array(
                'name' => 'dataTables.bootstrap.min',
                'path' => 'assets/dataTables/media/js/'
            ),
            array(
                'name' => 'dataTables.responsive.min',
                'path' => 'assets/dataTables/media/js/'
            ),
            array(
                'name' => 'responsive.bootstrap.min',
                'path' => 'assets/dataTables/media/js/'
            ),
            array(
                'name' => 'dataTables',
                'path' => 'assets/dataTables/'
            )
        ));

        $this->loadCss(array(
            array(
                'name' => 'dataTables.bootstrap.min',
                'path' => 'assets/dataTables/media/css/'
            ),
            array(
                'name' => 'responsive.bootstrap.min',
                'path' => 'assets/dataTables/media/css/'
            ),
            array(
                'name' => 'shadowbox',
                'path' => 'assets/shadowbox/'
            )
        ));

        foreach ($this->data['list'] as $key => $row) {
            $row->date = date('Y-m-d', strtotime($row->datetime));
            $this->data['list'][$key] = $row;
        }

        parent::renderer();
    }

    public function upload()
    {

        if ($_FILES['excel']['size'] > 0) {

            $uploadPath = './assets/upload/' . $this->router->class . '/';
            if (!is_dir($uploadPath)) {
                mkdir($uploadPath, DIR_WRITE_MODE, TRUE);
            }
            $config = array(
                'upload_path' => $uploadPath,
                'allowed_types' => '*',
                'overwrite' => 'true',
                'file_name' => $this->router->class,
            );

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('excel')) {
                $this->setError($this->upload->display_errors());
            } else {
                $file = $this->upload->data();
                $file = $uploadPath . $file['file_name'];
                $this->import($file);
            }

            redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
        }
    }

    public function import($inputFileName)
    {
        date_default_timezone_set('UTC');

        $this->load->file(APPPATH . 'third_party/phpexcel1.8/PHPExcel.php');

        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $excel = array();

        //  Loop through each row of the worksheet in turn
        for ($row = 5; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE);

            $rowData = current($rowData);

            if (PHPExcel_Shared_Date::isDateTime($sheet->getCell('A' . $row))) {
                $rowData[0] = date('Y-m-d H:i:s', PHPExcel_Shared_Date::ExcelToPHP($rowData[0]));
                foreach ($rowData as $i => $r) {
                    if (strpos($r, '/') !== false) {
                        $r = str_replace('.', '', $r);
                        $r = explode('/', $r);
                        $rowData[$i] = str_pad($r[0],4, '0') . '/' .  str_pad($r[1],4, '0');
                    }
                }

                $excel[] = $rowData;

            } else {
                break;
            }
        }

        $this->parse_data($excel);
    }


    private function parse_data($data)
    {
        $city = array();
        $ealq = array();
        $delivery_paulinia = array();

        foreach ($data as $row) {

            $city_id = 1;
            $col = 1;

            //
            $city[] = array(
                //'name' => 'Ribeirão Preto - R$/m³',
                'eh_outside' => null,
                'ea_outside' => null,
                'city_id' => $city_id++,
                'datetime' => $row[0],
                'eh_inside' => $row[$col++],
                'ea_inside' => $row[$col++],
            );
            //
            $row[$col++];
            $city[] = array(
                //'name' => 'Piracicaba - R$/m³',
                'eh_outside' => null,
                'ea_outside' => null,
                'city_id' => $city_id++,
                'datetime' => $row[0],
                'eh_inside' => $row[$col++],
                'ea_inside' => $row[$col++],
            );
            //
            $row[$col++];
            $city[] = array(
                //'name' => 'Araçatuba - R$/m³',
                'eh_outside' => null,
                'ea_outside' => null,
                'city_id' => $city_id++,
                'datetime' => $row[0],
                'eh_inside' => $row[$col++],
                'ea_inside' => $row[$col++],
            );
            //
            $row[$col++];
            $city[] = array(
                //'name' => 'Ourinhos - R$/m³',
                'eh_outside' => null,
                'ea_outside' => null,
                'city_id' => $city_id++,
                'datetime' => $row[0],
                'eh_inside' => $row[$col++],
                'ea_inside' => $row[$col++],
            );
            //
            $row[$col++];
            $city[] = array(
                //'name' => 'São J. Rio Preto - R$/m³',
                'eh_outside' => null,
                'ea_outside' => null,
                'city_id' => $city_id++,
                'datetime' => $row[0],
                'eh_inside' => $row[$col++],
                'ea_inside' => $row[$col++],
            );
            //
            $row[$col++];
            $city[] = array(
                //'name' => 'GO - R$/m³',
                'city_id' => $city_id++,
                'datetime' => $row[0],
                'eh_inside' => $row[$col++],
                'eh_outside' => $row[$col++],
                'ea_inside' => $row[$col++],
                'ea_outside' => $row[$col++],
            );
            //
            $row[$col++];
            $city[] = array(
                //'name' => 'MG - R$/m³',
                'city_id' => $city_id++,
                'datetime' => $row[0],
                'eh_inside' => $row[$col++],
                'eh_outside' => $row[$col++],
                'ea_inside' => $row[$col++],
                'ea_outside' => $row[$col++],
            );
            //
            $row[$col++];
            $city[] = array(
                //'name' => 'PR - R$/m³',
                'eh_outside' => null,
                'ea_outside' => null,
                'city_id' => $city_id++,
                'datetime' => $row[0],
                'eh_inside' => $row[$col++],
                'ea_inside' => $row[$col++],
            );
            //
            $row[$col++];
            $city[] = array(
                //'name' => 'MS - R$/m³',
                'eh_outside' => $row[$col++],
                'ea_outside' => $row[$col++],
                'city_id' => $city_id++,
                'datetime' => $row[0],
                'eh_inside' => $row[$col++],
                'ea_inside' => $row[$col++],
            );
            //
            $row[$col++];
            $city[] = array(
                //'name' => 'PE- R$/m³',
                'eh_outside' => null,
                'ea_outside' => null,
                'city_id' => $city_id++,
                'datetime' => $row[0],
                'eh_inside' => $row[$col++],
                'ea_inside' => $row[$col++],
            );
            //
            $row[$col++];
            $ealq[] = array(
                //'name' => 'Indicador Diário do Etanol Hidratado ESALQ/BM&FBovespa Posto Paulínia (SP)',
                'datetime' => $row[0],
                'value' => $row[$col++],
            );
            $row[$col++];
            $delivery_paulinia[] = array(
                //'name' => 'Entrega Paulínia',
                'datetime' => $row[0],
                'value' => $row[$col++],
            );
        }

        $this->array_clear($ealq, 'value', true);
        $this->array_clear($delivery_paulinia, 'value', true);


        $this->save_delivery_paulinia($delivery_paulinia);
        $this->save_ethanol($city);
        $this->save_ealq($ealq);

    }
    private function array_clear(array &$data, $key_check, $rekey = false) {

        foreach ($data as $key => $item) {

            if(isset($item[$key_check]) === false || strlen(trim($item[$key_check])) === 0) {
                unset($data[$key]);
            }
        }
        if ($rekey) {
            $data = array_values($data);
        }
    }
    private function save_ethanol($data = array())
    {
        if(isset($data[0]['datetime'])) {
            $this->load->model('ethanol_model');

            $this->ethanol_model->delete(array(
                'datetime >=' => $data[0]['datetime'],
                'id !=' => '0'
            ));

            $this->data['ethanol']['sent'] = count($data);
            $this->data['ethanol']['recorded'] = $this->ethanol_model->insert_batch($data);
        }
    }
    private function save_delivery_paulinia($data = array())
    {
        if(isset($data[0]['datetime'])) {
            $this->load->model('deliverypaulinia_model');

            $this->deliverypaulinia_model->delete(array(
                'datetime >=' => $data[0]['datetime'],
                'id !=' => '0'
            ));

            $this->data['deliverypaulinia']['sent'] = count($data);
            $this->data['deliverypaulinia']['recorded'] = $this->deliverypaulinia_model->insert_batch($data);
        }
    }
    private function save_ealq($data = array())
    {
        if(isset($data[0]['datetime'])) {
            $this->load->model('ealq_model');

            $this->ealq_model->delete(array(
                'datetime >=' => $data[0]['datetime'],
                'id !=' => '0'
            ));

            $this->data['ealq']['sent'] = count($data);
            $this->data['ealq']['recorded'] = $this->ealq_model->insert_batch($data);
        }
    }
}