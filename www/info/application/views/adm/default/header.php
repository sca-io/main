<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle; ?></title>
    <meta charset="utf-8">
    <base href="<?php echo base_url(); ?>">
    <meta name="controller" content="<?php echo $this->router->class ?>"/>
    <meta name="method" content="<?php echo $this->router->method ?>"/>
    <?php echo isset($assets) ? $assets : NULL; ?>
    <?php echo isset($css) ? $css : NULL; ?>
    <?php echo isset($js) ? $js : NULL; ?>

</head>
<body>
<div class="global-inf">
    <?php if ($error): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <strong>Erro:</strong> <?php echo $error; ?>
        </div>
    <?php endif; ?>
    <?php if ($msg): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <strong>Mensagem:</strong> <?php echo $msg; ?>
        </div>
    <?php endif; ?>
</div>
<div class="container-fluid wrapper">
    <div class="row header first-header">
        <div class="container">
            <div class="container-fluid row">
                <div class="navbar-header col-xs-12 row">
                    <a class="navbar-brand navbar-brand col-xs-4 col-sm-2 col-md-3" href="./adm"><img
                            src="./assets/adm/img/sca-logo.png" alt="<?php echo NAME_SITE; ?>"
                            class="img-responsive"></a>
                    <?php if (isset($adm->id)): ?>
                        <div class="user-logout pull-right">
                            <a href="./adm/login?sair=1" class="btn btn-lg logout pull-right">Sair</a>
                            <div class="user-logout-container pull-left">
                                <div>
                                    <span class="glyphicon glyphicon-user"></span> <?php echo $adm->name; ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div><!-- /.container-fluid -->
        </div>
    </div>

    <div class="row">
        <div class="container header-wrap">
            <?php if (isset($adm->id)): ?>
                <nav class="navbar navbar-default navbar-sca">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="dropdown admin">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false">Admin<span class="caret"></span></a>
                                    <ul class="dropdown-menu menu-principal-vertical" role="menu">
                                        <li class="admin_index"><a href="./adm/admin">Lista</a></li>
                                        <li class="admin_edit"><a href="./adm/admin/novo">Novo</a></li>
                                    </ul>
                                </li>
                                <li class="ethanol"><a href="./adm/ethanol">Etanol</a></li>
                                <li class="dropdown comment">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false">Comentários<span class="caret"></span></a>
                                    <ul class="dropdown-menu menu-principal-vertical" role="menu">
                                        <li class="comment_index"><a href="./adm/comentario">Lista</a></li>
                                        <li class="comment_edit"><a href="./adm/comentario/novo">Novo</a></li>
                                    </ul>
                                </li>
                                <li class="city"><a href="./adm/cidade">Localidade</a></li>
                                <li class="indicator"><a href="./adm/indicador">Indicadores</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            <?php endif; ?>
        </div><!-- /.header-wrap -->
    </div><!-- /.row .header -->