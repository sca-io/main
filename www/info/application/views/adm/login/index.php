<div class="container">
    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-7 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <h1 class="page-title">Autenticação</h1>
        <div class="form-base col-xs-12 col-sm-10">
            <form id="loginform" class="form" role="form" action="./adm/login/auth" method="post">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input id="login-email" required type="email" class="form-control" name="email"
                           value="<?php echo isset($email) ? $email : null; ?>" placeholder="Insira aqui seu e-mail">
                </div>

                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input id="login-password" required type="password"
                           value="<?php echo isset($password) ? $password : null; ?>" class="form-control"
                           name="password" placeholder="Insira aqui sua senha">
                </div>
                <div class="form-group form-action">
                    <button class="btn btn-success" type="submit">Salvar</button>
                </div>
            </form>
        </div>
    </div>
    <div class="clearfix"></div>
</div>