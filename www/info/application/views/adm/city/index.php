<div class="container">
    <h1 class="page-title">Localidades</h1>
    <div class="container-base col-xs-12 col-md-10">
        <table class="dataTable table table-bordered table-striped dt-responsive">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Descrição dentro</th>
                <th>Descrição fora</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($list as $row) : ?>
                <tr>
                    <td><?php echo $row->name; ?></td>
                    <td><?php echo $row->descriptionin; ?></td>
                    <td><?php echo $row->descriptionout; ?></td>
                    <td class="action">
                        <div class="btn-group">
                            <a class="btn btn-sm btn-primary"  href="./adm/<?php echo $this->uri->segment(2); ?>/editar/<?php echo $row->id; ?>">
                                <i class="fa fa-pencil" title="Editar"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            <?php endForeach; ?>
            </tbody>
        </table>
        <div class="clearfix"></div>
    </div>
</div>