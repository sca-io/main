<div class="container">
    <h1 class="page-title"><?php echo (isset($data->id) ? 'Editar' : 'Novo') . ' ' . 'Indicador' ?></h1>
    <div class="container-base col-xs-11 col-md-10">
        <form role="form" method="post" enctype="multipart/form-data"
              action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/salvar/<?php echo isset($data->id) ? $data->id : NULL ?>">
            <div class="form-group">
                <label for="description">Descrição: </label>
                <input class="form-control" type="text" name="description" id="description" value="<?php echo isset($data->description) ? $data->description : NULL ?>" />
            </div>
            <div class="clearfix"></div>
            <div class="form-group form-action text-right">
                <button class="btn btn-success" type="submit">Salvar</button>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
</div>
