<section>
    <div class="container">
        <h1 class="page-title">Utilize o filtro abaixo para customizar seu informativo</h1>
        <div class="container-base col-md-10">
            <form role="form" action="./home/save_filter" method="post">
                <div class="form-group">
                    <h3 class="sub-title">Quais localidades você tem interesse?</h3>
                    <?php foreach ($state as $row): ?>
                        <label class="checkbox h4"><input type="checkbox" class="check-all" data-state-class=".state-<?php echo $row->id; ?>"> <?php echo $row->name; ?>
                        </label>
                        <ul>
                            <?php foreach ($row->city as $city): ?>
                                <li class="col-md-4">
                                    <label class="checkbox">
                                        <input type="checkbox" name="city[]" value="<?php echo $city->id; ?>" class="state-<?php echo $row->id; ?> state-required" data-class=".state-<?php echo $row->id; ?>" required> <?php echo $city->name; ?>
                                        <div class="clearfix"></div>
                                    </label>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="clearfix"></div>
                    <?php endforeach; ?>
                </div>
                <div class="clearfix"></div>
                <h3> </h3>
                <div class="form-group">
                    <h3 class="sub-title">Qual tipo de Etanol prefere visualizar?</h3>
                    <label class="checkbox h4"><input type="checkbox" class="check-all" data-state-class=".ethanol_type"> Todos
                    </label>
                    <ul>
                        <li class="col-md-4">
                            <label class="checkbox">
                                <input type="checkbox" name="ethanol_type[]" value="eh" class="ethanol_type" data-class=".ethanol_type" required> Etanol Hidratado
                                <div class="clearfix"></div>
                            </label>
                        </li>
                        <li class="col-md-4">
                            <label class="checkbox">
                                <input type="checkbox" name="ethanol_type[]" value="ea" class="ethanol_type" data-class=".ethanol_type" required> Etanol Anidro
                                <div class="clearfix"></div>
                            </label>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <h3> </h3>
                <div class="form-group">
                    <h3 class="sub-title">Gosta de visualizar comentários sobre os dados?</h3>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-primary">
                            <input type="radio" name="comment" value="1" autocomplete="off" required> Sim
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="comment" value="0" autocomplete="off" required> Não
                        </label>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group form-action">
                    <button type="submit" class="btn btn-success">Filtrar</button>
                </div>
            </form>
        </div>
    </div>
</section>