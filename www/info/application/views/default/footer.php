<div class="footer-fix"></div>
<div class="footer">
    <div class="line-one"></div>
    <div class="line-two">
        <div class="container">
            <div class="phone">
                <h5>São Paulo</h5>
                <h4>Tel: 55 11 3709-4900</h4>
            </div>
            <div class="phone">
                <h5>Goiás</h5>
                <h4>Tel: 55 62 3878-4900</h4>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="line-three"></div>
    <div class="container">
        <img src="./assets/img/footer_leaf.png" alt="">
    </div>
</div>
</div>
</body>
</html>