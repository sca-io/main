<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class City_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_related(array $where = array(), $limit = null)
    {
        $this->db->join('state', $this->getAlias() . '.state_id = state.id');
        $this->db->select($this->getAlias() . '.*, state.name as state_name');
        return parent::get($where, $limit);
    }
}