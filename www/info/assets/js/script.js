$(function () {
    var content = $('meta[name=controller]').prop('content');
    var method = $('meta[name=method]').prop('content');
    $('.' + content + ', .' + content + '_' + method).addClass('active');

    $('[data-toggle="popover"]').popover();

    $('input.check-all').click(function () {
        var $this = $(this),
            class_state = $this.attr('data-state-class'),
            $input = $("form label.checkbox input" + class_state);

        if ($this.is(':checked')) {
            $input.prop('checked', true);
        } else {
            $input.prop('checked', false);
        }

        if($input.hasClass('state-required')) class_state = '.state-required';

        var $input_checked = $("form label.checkbox input" + class_state + ":checked");

        check_required($input_checked, $input)
    });

    $("form label.checkbox input").click(function () {
        var class_state = $(this).hasClass('state-required') ? '.state-required' : $(this).attr('data-class'),
            $input = $("form label.checkbox input" + class_state),
            $input_checked = $("form label.checkbox input" + class_state + ":checked");

        if ($input_checked.length < $input.length) {
            $('input.check-all[data-state-class="' + class_state + '"]').prop('checked', false);
        } else if ($input_checked.length == $input.length) {
            $('input.check-all[data-state-class="' + class_state + '"]').prop('checked', true);
        }

        check_required($input_checked, $input);
    });

});

function check_required($input_checked, $input) {
    if ($input_checked.length > 0) {
        $input.removeAttr('required');
    } else {
        $input.attr('required', 'required');
    }
}