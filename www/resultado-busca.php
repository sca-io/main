<? require_once ('conf.php');
$busca = request("busca");
$url_self = DIRETORIO_RAIZ."resultado-busca/?busca=".$busca;
$paramPage = true; //envia o parametro page não amigavel

function esc_busca_campo($campo,$busca){
     return " $campo LIKE '%$busca%' OR SOUNDEX($campo) = SOUNDEX('$busca') OR $campo LIKE '%".htmlentities($busca,ENT_QUOTES,"UTF-8")."%' ";
}

?><? include "includes/head.php"?>
<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <a href="javascript:;"><strong><?=getTextoByLang("Resultado da busca","Search Results");?></strong></a>
            </div>
        </div>
        
        <div class="busca">
            <? 
            $listagem = null;
            if($busca):?>
                <h3><?=getTextoByLang("Termo pesquisado","Search term:");?> <strong><?=response_html($busca)?></strong></h3>
                <?
                $idsNotIn = array(MENU_TIME_LINE,HOME,UNIDADES_PRODUTORAS);
                $where = " AND id_secao NOT IN(".implode(",",$idsNotIn).") AND idioma = '$idioma'";

                $whereLike1 = esc_busca_campo("titulo",$busca);
                $whereLike1 .= " OR ".esc_busca_campo("texto",$busca);
                $whereLike1 .= " OR ".esc_busca_campo("descricao",$busca);
                $whereLike1 .= " OR ".esc_busca_campo("keywords",$busca);
                $whereLike1 = " AND ($whereLike1)";
                $where1 = "status = ".VersaoConteudo::PUBLICADO." $where $whereLike1";
                $ncad1 =  (int) VersaoConteudo::countListar("","",$where1);
                
                $whereLike2 = esc_busca_campo("titulo",$busca);
                $whereLike2 .= " OR ".esc_busca_campo("texto",$busca);
                $whereLike2 = " AND ($whereLike2)";
                $where2 = "status = ".Chamada::PUBLICADO." $where $whereLike2";
                $ncad2 =  (int) Chamada::countListar("","",$where2);     
                
                $ncadastro = $ncad1+$ncad2;
                
                //print VersaoConteudo::getLogSql();    
                $pagina = isset($_REQUEST['page'])?$_REQUEST['page']:0;
                $pagina = $pagina > 0?(int) $pagina-1:$pagina;
                $total_reg_pag = 15;
                $init_reg_pag = ($pagina * $total_reg_pag);	 
                $aux_pagina = $pagina + 1;
                $total_paginas = $ncadastro/$total_reg_pag;				
                $lista1 = VersaoConteudo::listar("","","id,id_secao,titulo,texto,data_publicacao,url_amigavel",$where1,"data_publicacao DESC","$init_reg_pag, $total_reg_pag"); 
                $lista2 = Chamada::listar("","","id,id_secao,titulo,texto,data_publicacao,link",$where2,"data_publicacao DESC","$init_reg_pag, $total_reg_pag"); 
                $listagem = array_merge($lista1, $lista2);
                ?>
            <? endif;?>
            <? if($listagem):?>
                <? foreach($listagem as $l):?>
                <div class="bloco">
                    <blockquote>
                        <?
                        $idSecaPaiCont = getIdSecaoPai($l->getIdSecao());
                        if($idSecaPaiCont){
                            $settings = getSettings($idSecaPaiCont,$l->getIdSecao());
                        }else{
                            $settings = getSettings($l->getIdSecao());
                        }
                        
                        $titulo = $l->getTitulo();
                        $data = Util::dataDoBD($l->getDataPublicacao());
                        $texto = Util::truncarTexto(strip_tags($l->getTexto()),200);
                        $linkCont = DIRETORIO_RAIZ;
                        if(isset($settings["url_secao"])) $linkCont .= $settings["url_secao"];
                        
                        if($idSecaPaiCont == NOTICIAS || $l->getIdSecao() == NOTICIAS){
                            $urlAmigCont = $l->getUrlAmigavel();
                        }else{
                            //if(get_class($l) 
                            $urlAmigCont = "";
                        } 
                      
                        if($urlAmigCont) $linkCont .= response_attr($urlAmigCont)."/";
                        ?>
                        <!--<p><a href="<?=$linkCont?>" class="bt-leia-mais" title="<?=getTextoByLang("Leia mais","Read more");?> +"><?=getTextoByLang("Leia mais","Read more");?> <strong>+</strong></a></p>-->
                        <? /*<img src="img/ft-acoes-sustentaveis1.jpg" width="120" height="93" /><!--esta imagem não existe coloquei apenas por simulação ou seja não é para programar--> */?>
                        <h4><?=response_html($data)?></h4>
                        <h2><?=response_html($titulo)?></h2>
                        <p><?=response_html($texto)?></p>
                        <p><a href="<?=$linkCont?>" class="seta"  title="<?=getTextoByLang("Leia mais","Read more");?> +" ><?=getTextoByLang("Leia mais","Read more");?> </a></p>
                    </blockquote>
                </div>
                <? endforeach;?>
            <? else:?>
                <h3><?=getTextoByLang("Nenhum resultado encontrado!","No results found!");?></strong></h3>

            <? endif;?>
            
                  
        </div>
        
        
      </div>
      <? include "includes/paginacao.php"?>      
            
    </div>
    <? include "includes/rodape.php"?>
</div>
</body>
</html>