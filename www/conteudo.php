<?
require_once ('conf.php');
$idioma = getIdioma();
$id = secureRequest('id');
$idSecao = request('idSecao');
$idSubSecao = request('idSubSecao');
$url_amig = request('txt');
$where = "idioma = '$idioma' AND status = '".VersaoConteudo::PUBLICADO."'";
$conteudo = null;

if($id)  $where .= " AND id = $id";
if($url_amig)  $where .= " AND url_amigavel = '$url_amig'";


if($idSecao|| $idSubSecao){     
    $idSecaoCont = $idSubSecao?$idSubSecao:$idSecao;
    $listaConts = VersaoConteudo::listar($idSecaoCont,"","id,titulo,texto,keywords,descricao,id_secao,url_embed,img_destaque",$where,"data_publicacao DESC","1");
    if($listaConts){
        $conteudo = $listaConts[0];    
    }    
}

if($conteudo){
    $titulo = $conteudo->getTitulo();
    $descricao = $conteudo->getDescricao();
    $keywords = $conteudo->getKeywords();
    $texto = $conteudo->getTexto();
}

$nome_secao_atual = "";
$nome_secao = "";
$nome_img_tit = "";
$brad_crumb = "";
$url_secao = "";

if($idSecao){    
    $classe_principal = "empresa trajetoria";
    switch($idSecao){
        case EMPRESA:
            $classe_principal = "empresa trajetoria";
            $url_secao = DIRETORIO_RAIZ."empresa/";
        break;    
        case SERVICOS:
            $classe_principal = "servicos inteligencia-de-mercado";
            $url_secao = DIRETORIO_RAIZ."servicos/";            
        break;  
        case ETANOL:
            $classe_principal = "etanol";
            $url_secao = DIRETORIO_RAIZ."etanol/";
            switch($idSubSecao){
                case ESPECIFICACOES:                    
                break;              
                case CERTIFICACOES:
                    $classe_principal .= " legislacoes certificacoes";
                break;
                case LEGISLACOES:
                    $classe_principal = "empresa etanol legislacoes";
                break;
            }             
        break;  
        case GESTAO_DE_PESSOAS:
            $classe_principal = "empresa gestao-de-pessoas";
            $url_secao = DIRETORIO_RAIZ."gestao-de-pessoas/";
        break;   
    }  
    
    if(isset($settSecoes[$idSecao])) $nome_secao = getNomeSecao($idSecao);
    $brad_crumb = $nome_secao;
    if(!$idSubSecao) $brad_crumb = "<strong>".$brad_crumb."</strong>";	
    $brad_crumb = '<a href="'.$url_secao.'">'.$brad_crumb.'</a>';
    $nome_img_tit = getNomeSecao($idSecao,"","",LNG_PT);
    $nome_secao_atual = $nome_secao;

}


$subSecoes = array();

//$subSecao = Secao::ler($idSubSecao,"nome");
if($idSubSecao){
    switch($idSubSecao){
        case ESPECIFICACOES:
            $url_sub_secao = $url_secao."especificacoes/";
        break;
    }
    $nome_sub_secao = getNomeSecao($idSecao,$idSubSecao);
    $brad_crumb .= '&nbsp;&nbsp;/&nbsp;<a href="javascript:;"><strong>'.$nome_sub_secao.'</strong></a>'; 
    $nome_img_tit = getNomeSecao($idSecao,$idSubSecao,"",LNG_PT);
    $nome_secao_atual = $nome_sub_secao;
    if(isset($settSecoes[$idSecao]["sub_secoes"][$idSubSecao]["sub_secoes"]))$sub_secoes = $settSecoes[$idSecao]["sub_secoes"][$idSubSecao]["sub_secoes"];
}

?><? include "includes/head.php"?>
<body>
<div id="sca">
    <div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <?=response_html($brad_crumb)?>  
            </div>
        </div>
          <div class="<?=$classe_principal?>">
            <div class="esquerda">
                <div class="txt-box">
                    <? if(isset($sub_secoes[MERCADO_INTERNO]) || isset($sub_secoes[MERCADO_EXTERNO])):?><div><? endif;?>
                    <? if($conteudo):?>            	
                        <h2 class="tit-empresa"><img src="<?=DIRETORIO_RAIZ?>css/img/tit-<?=setUrlAmigavel($nome_img_tit).getTextoByLang("","-eng")?>.png"  height="35" alt="<?=$nome_secao_atual?>" /> </h2>
                        <? if($titulo):?><h3><?=response_html($titulo)?></h3><? endif;?>
                        <?=response_html($texto)?>		
                    <? else:?>                       
                        <p><br /><?=getTextoByLang("Conteúdo não encontrado!","Content not found!");?><br /><br /><br /></p>                
                    <? endif;?>
                    <p>
                    <? if(isset($sub_secoes[MERCADO_INTERNO])):?><a href="<?=$url_sub_secao?>mercado-interno/" class="bt-mercado-interno<?=getTextoByLang(""," bt-mercado-interno-eng")?>" title="<?=getNomeSecao(ETANOL,ESPECIFICACOES,MERCADO_INTERNO)?>"><?=getNomeSecao(ETANOL,ESPECIFICACOES,MERCADO_INTERNO)?></a><? endif;?>
                    <? if(isset($sub_secoes[MERCADO_EXTERNO])):?><a href="<?=$url_sub_secao?>mercado-externo/" class="bt-mercado-externo<?=getTextoByLang(""," bt-mercado-externo-eng")?>" title="<?=getNomeSecao(ETANOL,ESPECIFICACOES,MERCADO_EXTERNO)?>"><?=getNomeSecao(ETANOL,ESPECIFICACOES,MERCADO_EXTERNO)?></a><? endif;?>
                    </p>
                    <? if(isset($sub_secoes[MERCADO_INTERNO]) || isset($sub_secoes[MERCADO_EXTERNO])):?></div><? endif;?>
                </div>
                
                <? if($idSubSecao == TRAJETORIA):?><div id="flash"></div><? endif;?> 
                <a href="javascript:history.back()" class="bt-voltar" title="<?=getTextoByLang("Voltar","Return")?>"> &lt; <?=getTextoByLang("Voltar","Return")?></a> 
            </div>
            <? include "includes/direita.php";?>
        </div>
      </div>
    </div>
    <? include "includes/rodape.php"?>
</div> 
</body>
</html>