<? require_once ('./sapo/conf.php');?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? include "includes/head.php"?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <a href="#"><strong>Notícias</strong></a>
            </div>
        </div>
        <div class="busca noticias-listagem noticias-conteudo">
        	<h3><img src="css/img/tit-noticias.png" width="90" height="35" /></h3>
            <div class="esquerda">
            	<div class="texto">
                	<div class="bloco">
                        <blockquote>
                            <h4>10/05/2012</h4>
                            <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit fusceorci nec</h2>
                            <p><a href="#" class="ft-esq"><img src="img/ft-noticia-conteudo.jpg" width="344" height="257" /><b>Lorem ipsum dolor sit amet, consectetur adipis.</b></a>Lorem ipsum dolor sit amet, consectetur adipiscing elit fusce commodo orci nec nulla pretium volutpat sedkei feugiat semper tristique. Nunc dignissim tortor nonleo ipsum interdum sit amet tincidunt eros commodo. Donec ut lacinia justo. Etiam non orci velit. Lobortis lacus <strong>interdum sem ullamcorper fringilla</strong>. Proin a condimentum enim. <a href="#">Cras consectetur lorem nec tellus rhoncus</a> dignissim placerat nunc dignissim. Etiam accumsan dolor id nisl blandit convallis. Phasellus ac urna nec dui facilisis tristique. Ut vel est massa, id dignissim turpis. Suspendisse purus dui, aliquet rhoncus rhoncus eget, adipiscing imperdiet arcu. Aenean non neque et magna dapibus cursus eu nec mauris. Integer elit turpis, pretium placerat pulvinar condimentum, commodo sit amet lectus. Nam sagittis, nisi id lacinia malesuada, tortor odio malesuada mauris, sed iaculis tortor nisl vitae magna.<br /><br /></p>
                            <p><a href="#" class="ft-dir"><img src="img/ft-noticia-conteudo.jpg" width="344" height="257" /><b>Lorem ipsum dolor sit amet, consectetur adipis.</b></a>Lorem ipsum dolor sit amet, consectetur adipiscing elit fusce commodo orci nec nulla pretium volutpat sedkei feugiat semper tristique. Nunc dignissim tortor nonleo ipsum interdum sit amet tincidunt eros commodo. Donec ut lacinia justo. Etiam non orci velit. Lobortis lacus <strong>interdum sem ullamcorper fringilla</strong>. Proin a condimentum enim. <a href="#">Cras consectetur lorem nec tellus rhoncus</a> dignissim placerat nunc dignissim. Etiam accumsan dolor id nisl blandit convallis. Phasellus ac urna nec dui facilisis tristique. Ut vel est massa, id dignissim turpis. Suspendisse purus dui, aliquet rhoncus rhoncus eget, adipiscing imperdiet arcu. Aenean non neque et magna dapibus cursus eu nec mauris. Integer elit turpis, pretium placerat pulvinar condimentum, commodo sit amet lectus. Nam sagittis, nisi id lacinia malesuada, tortor odio malesuada mauris, sed iaculis tortor nisl vitae magna.<br /><br /></p>
                            <p><em>"Curabitur lectus eros, sodales scelerisque tempus vitae, tempus nec velit"</em>. Pellentesque lacinia volutpat purus, eu hendrerit urna commodo imperdiet. Vestibulum bibendum, nunc in semper blandit, risus ipsum venenatis purus, sed pharetra dolor nunc nec lacus. Mauris tempus ante ut nulla hendrerit consequat. Sed pretium viverra massa nec adipiscing. Vivamus at dolor at nulla tristique porta. Curabitur convallis bibendum elit nec tempor. Etiam facilisis tortor nec diam consectetur non commodo eros faucibus sed nisi turpis, lacinia quis malesuada ac, commodo a tellus.<br /><br /></p>
                            <p>Suspendisse condimentum augue et massa varius et luctus risus dapibus. Ut ut enim eros, sed venenatis elit. Integer condimentum mattis urna, vel adipiscing massa cursus dapibus.</p>
                        </blockquote>
                    </div>
            		<a href="noticias-listagem.php" class="bt-voltar"> < Voltar</a>
                </div>
                
                <h5><img src="css/img/tit-noticias-relacionadas.png" width="182" height="34" /></h5>    
                <div class="bloco">
                    <blockquote>
                        <a href="#" class="bt-leia-mais" title="Leia mais +">Leia mais <strong>+</strong></a>
                        <h4>10/05/2012</h4>
                        <h2>Título da notícia</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce commodo orci nec nulla pretium volutpat  sed feugiat semper tristique nunc dignissim tortor non ipsum interdum sit amet tincidunt eros commodo. </p>
                        <a href="#" class="seta"> > </a>
                    </blockquote>
                </div>
                
                <div class="bloco">
                    <blockquote>
                        <a href="#" class="bt-leia-mais" title="Leia mais +">Leia mais <strong>+</strong></a>
                        <img src="img/ft-acoes-sustentaveis1.jpg" width="120" height="93" /><!--esta imagem não existe coloquei apenas por simulação ou seja não é para programar-->
                        <h4>10/05/2012</h4>
                        <h2>Título da notícia</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce commodo orci nec nulla pretium volutpat  sed feugiat semper tristique nunc dignissim tortor non ipsum interdum sit amet tincidunt eros commodo. </p>
                        <a href="#" class="seta"> > </a>
                    </blockquote>
                </div>
                <a href="#" class="bt-veja-todas-as-noticias" title="Veja todas as notícias"> <strong>+</strong> Veja todas as notícias</a>                 
            </div>
            
            <div class="direita">
            	<div>
                	<h2>Últimas notícias</h2>
                    <blockquote>
                    	<h3>10-05-2012</h3>
                        <p><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce commodo orci nec nulla pretium volutpat.</a></p>
                    </blockquote>
                    
                    <blockquote>
                    	<h3>10-05-2012</h3>
                        <p><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce commodo orci nec nulla pretium volutpat.</a></p>
                    </blockquote>
                    
                    <blockquote>
                    	<h3>10-05-2012</h3>
                        <p><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce commodo orci nec nulla pretium volutpat.</a></p>
                    </blockquote>
                    
                    <blockquote>
                    	<h3>10-05-2012</h3>
                        <p><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce commodo orci nec nulla pretium volutpat.</a></p>
                    </blockquote>
                    
                    <blockquote>
                    	<h3>10-05-2012</h3>
                        <p><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce commodo orci nec nulla pretium volutpat.</a></p>
                    </blockquote>
                </div>
            </div>
                  
        </div>
        
      </div>
    </div>
    <? include "includes/rodape.php"?>
</div>
</body>
</html>