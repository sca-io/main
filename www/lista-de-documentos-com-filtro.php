<? require_once ('conf.php');
$idioma = getIdioma();
$id = secureRequest('id');
$assunto= secureRequest("assunto");
$url_amig = request("url_amig");
$order = request("order");

$idSecao = request('idSecao');
$idSubSecao = request('idSubSecao');
$subSecao2= request("subSecao2");

$conteudo = null;

if(!$subSecao2){
    $where = "idioma = '$idioma' AND status = '".VersaoConteudo::PUBLICADO."'";
    if($id)  $where .= " AND id = $id";
    if($idSecao|| $idSubSecao){     
        $idSecaoCont = $idSubSecao?$idSubSecao:$idSecao;
        $listaConts = VersaoConteudo::listar($idSecaoCont,"","id,titulo,texto,keywords,descricao,id_secao,url_embed,img_destaque",$where,"data_publicacao DESC","1");
        //print VersaoConteudo::getLogSql();
        if($listaConts){
            $conteudo = $listaConts[0];    
        }    
    }
    $id = "";
    if($conteudo){
        $id = $conteudo->getId();
        $titulo = $conteudo->getTitulo();
        $descricao = $conteudo->getDescricao();
        $keywords = $conteudo->getKeywords();
        $texto = $conteudo->getTexto();
    }
}else{
    
}

$nome_secao = "";
$nome_secao_atual = "";
$nome_img_tit = "";
$brad_crumb = "";
$url_secao = "";
$url_sub_secao = "";
$url_classe_doc = "";

$settings = null;
if($idSecao){
    $settings = getSettings($idSecao);
    if($settings){
        $url_secao = $settings["url_secao"];
    }   
    if(isset($settSecoes[$idSecao])) $nome_secao = getNomeSecao($idSecao);
    $brad_crumb = '<a href="'.$url_secao.'">'.$nome_secao.'</a>';
    $nome_img_tit = $nome_secao;
    $nome_secao_atual = $nome_secao;
    $url_self = $url_secao;
}

if($idSubSecao){
    $settings = null;
    $settings = getSettings($idSecao,$idSubSecao);
    if($settings){
        $url_sub_secao = $settings["url_secao"];
        if(isset($settings["classes_doc"])){
            $classes_doc = $settings["classes_doc"];
        }
    }   
    $nome_sub_secao = getNomeSecao($idSecao,$idSubSecao);
    if($subSecao2){
        $brad_crumb .= '&nbsp;&nbsp;/&nbsp;<a href="'.$url_sub_secao.'">'.$nome_sub_secao.'</a>'; 
    }else{
        $brad_crumb .= '&nbsp;&nbsp;/&nbsp;<a href="javascript:;"><strong>'.$nome_sub_secao.'</strong></a>'; 
    }    
    
    $nome_img_tit = getNomeSecao($idSecao,$idSubSecao,"",LNG_PT);
    $nome_secao_atual = $nome_sub_secao;   
    $idSecaoSel = $idSubSecao;
    $url_self = $url_sub_secao;
}

$classes_doc = array();
$nome_classe_doc = "";
$idSubSecao2 = "";
if($subSecao2){
    switch($subSecao2){
        case "mercado-interno":
            $nome_classe_doc = getNomeSecao($idSecao,$idSubSecao,MERCADO_INTERNO);
            $nome_img_tit = getNomeSecao($idSecao,$idSubSecao,MERCADO_INTERNO,LNG_PT);
            $idSubSecao2 = MERCADO_INTERNO;
        break;
        case "mercado-externo":
            $nome_classe_doc = getNomeSecao($idSecao,$idSubSecao,MERCADO_EXTERNO);
            $nome_img_tit = getNomeSecao($idSecao,$idSubSecao,MERCADO_EXTERNO,LNG_PT);
            $idSubSecao2 = MERCADO_EXTERNO;
        break;
    }
    $settings = null;
    $settings = getSettings($idSecao,$idSubSecao,$idSubSecao2);
    if($nome_classe_doc){
        $metaTitle = $nome_classe_doc;
        $brad_crumb .= '&nbsp;&nbsp;/&nbsp;<a href="javascript:;"><strong>'.$nome_classe_doc.'</strong></a>'; 
    }
    $url_classe_doc = $url_sub_secao.$subSecao2."/";
    $nome_secao_atual = $nome_classe_doc;
    $idSecaoSel = $idSubSecao2;
    $url_self = $url_classe_doc;	
}


$img_tit_secao = DIRETORIO_RAIZ."css/img/tit-".setUrlAmigavel($nome_img_tit).  getTextoByLang("", "-eng").".png";  

if($url_self) $url_self = DIRETORIO_RAIZ.$url_self;

$resulTemFiltro = isset($settings["filtro_de_resultado"]) && $settings["filtro_de_resultado"];

$where = "status = ".ATIVO." AND tipo = '".Media::DOCS."' AND lang = '$idioma'"; 
$categoriasComArq  = Media::getIdCategoriasComArqs($id,$idSecaoSel,$where);

?><? include "includes/head.php"?>
<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <?=$brad_crumb?>
            </div>
        </div>
        <div class="empresa etanol legislacoes">
            <div class="esquerda">
            	<div class="txt-box">
                    <div>                        
                        <h2 class="tit-empresa"><img src="<?=$img_tit_secao?>" alt="<?=$nome_secao_atual?>" /> </h2>
                        <? if($conteudo):?><?=response_html($texto) ?><? endif;?>
                        <? include "includes/filtro-assunto.php"?>
                    </div>
                </div>
                <? 
                $listaArqs = null;
                if($assunto || $url_amig){
                    /*** A variavel assunto é setada no include de filtro de assunto ***/
                    if($assunto) $where .= " AND id_categoria = '$assunto'";
                    $ncadastro =  Media::countListar($id,$idSecaoSel,$where);
                    $total_reg_pag = 10;                    
                    $pagina = isset($_REQUEST['pagina'])?$_REQUEST['pagina']:0;
                    $pagina = $pagina > 0?(int) $pagina-1:$pagina;                    
                    $init_reg_pag = ($pagina * $total_reg_pag);	 
                    $aux_pagina = $pagina + 1;
                    $total_paginas = $ncadastro/$total_reg_pag;                    
                    $orderBy = "data_insercao DESC";
                    if($order == "nome") $orderBy = "nome ASC";
                    $listaArqs = Media::listar($id,$idSecaoSel,"",$where,$orderBy,"$init_reg_pag, $total_reg_pag");  
                    //print Media::getLogSql();
                    $url_paginacao = $url_self.$url_amig."/";
                    if($order) $url_paginacao .= $order."/";        

                }       
                ?>
                <? if($listaArqs):?>
                <div class="resultados<? if(!$resulTemFiltro):?> result-estatistica<? endif;?>">
                    <? if($resulTemFiltro):?>
                    <form action="" id="formFiltroRes">
                        <p class="hidden">
                            <input type="hidden" name="urlSelf" value="<?=$url_self?>" />  
                            <input type="hidden" name="urlAmig" value="<?=$url_amig?>" />                                
                        </p>
                        	<p><label><?=getTextoByLang("Resultados","Results");?></label>
                            <select id="order" onChange="filtarResultado()">                                
                                <option value="">Classificar por</option>
                                <option value="nome" <?=($order == "nome")?"selected='selected'":""?>><?=getTextoByLang("Nome","Name");?></option>
                                <option value="data" <?=($order == "data")?"selected='selected'":""?>><?=getTextoByLang("Data","Date");?></option>
                            </select></p>
                        
                    </form>
                    <? endif;?>
                    <? foreach($listaArqs as $l):?>
                    <div>
                        <blockquote>

                            <? if(Util::comparaDatas($l->getDataInsercao(),date('Y-m-d H:i:s'),"dias") < 30):?>
                            <p><img src="<?=$icones["novo"]?>" width="42" height="23" alt="" /></p>
                            <? else:?>
                            <p><?=getIconeArquivo($l->getUrl())?></p>
                            <? endif;?>
                            <h5>
                            <?=response_html($l->getNome())?>
                            <a href="javascript:downloadDoc('<?=secureResponse($l->getId())?>')" class="bt-baixar" title="<?=getTextoByLang("Download","Download")?>"><?=getTextoByLang("Download","Download")?></a>
                            <a href="<?=DIRETORIO_RAIZ.$l->getDir().$l->getUrl()?>" rel="external" class="bt-ver" title="<?=getTextoByLang("Visualizar","View")?>"><?=getTextoByLang("Visualizar","View")?></a></h5>
                            <p><strong><?=getTextoByLang("Data de publicação: ","Date of publication: ");?></strong> <?=response_html(Util::dataDoBD($l->getDataInsercao()))?></p>
                            <? if($l->getDescricao()):?><p><strong><?=getTextoByLang("Referente: ","Relative: ");?></strong><?=response_html($l->getDescricao())?></p><? endif;?>          

                        </blockquote>
                    </div> 
                    <? endforeach;?>                       
                </div> 
                <? else:?>                    
                    <? if($assunto):?>
                    <div class="resultados<? if(!$resulTemFiltro):?> result-estatistica<? endif;?>">
                        <blockquote>
                            <p><br /><?=getTextoByLang("Nenhum documento encontrado!","No documents found!");?></p>
                        </blockquote>
                    </div>
                    <? endif;?>                    
                <? endif;?> 
                <? include "includes/paginacao.php"?>
                <a href="javascript:history.back()" class="bt-voltar" title="<?=getTextoByLang("Voltar","Return")?>"> &lt; <?=getTextoByLang("Voltar","Return")?></a> 
            </div>
            <? include "includes/direita.php"?>

                        
        </div>
      </div>
    </div>
    <? include "includes/rodape.php"?>
</div>
</body>
</html>