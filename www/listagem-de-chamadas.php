<? require_once ('conf.php');
$idioma = getIdioma();
$idSecao = request('idSecao');
$idSubSecao = request('idSubSecao');
$idDestaque = '';
$idSecaoCh = $idSubSecao?$idSubSecao:$idSecao;

$where = "idioma = $idioma AND status = ".Chamada::PUBLICADO."";
$lista = null;
$lista = Chamada::listar($idSecaoCh,$idDestaque,"id,titulo,idioma,texto,url_media,legenda_media",$where," data_publicacao DESC");
//print Chamada::getLogSql();
$idSecaoSel = 0;
$nome_secao = "";
$nome_secao_atual = "";
if($idSecao){
    $url_secao = DIRETORIO_RAIZ."empresa/";    
    if(isset($settSecoes[$idSecao])) $nome_secao = getNomeSecao($idSecao);
    $brad_crumb = '<a href="'.$url_secao.'">'.$nome_secao.'</a>';
    $nome_img_tit = $nome_secao;
    $idSecaoSel = $idSecao;
    $nome_secao_atual = $nome_secao;

}

if($idSubSecao){
    $nome_sub_secao = getNomeSecao($idSecao,$idSubSecao);
    $brad_crumb .= '&nbsp;&nbsp;/&nbsp;<a href="javascript:;"><strong>'.$nome_sub_secao.'</strong></a>'; 
    $nome_img_tit = getNomeSecao($idSecao,$idSubSecao,"",LNG_PT);
    $idSecaoSel = $idSubSecao;
    $nome_secao_atual = $nome_sub_secao;
}




?><? include "includes/head.php"?>
<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <?=$brad_crumb?>    
            </div>
        </div>
        <div class="empresa acoes-sustentaveis">
            <div class="esquerda">
                <? if($lista):?>
                    <? foreach($lista as $k=>$l):?>                    
                    <div class="modulo">
                        <? if($k == 0):?><h2><img src="<?=DIRETORIO_RAIZ?>css/img/tit-<?=setUrlAmigavel($nome_img_tit).getTextoByLang("","-eng")?>.png"  height="35" alt="<?=$nome_secao_atual?>" /> </h2><? endif;?>
                        <div class="foto<?=($k>0 && (($k+1)%2 == 0))?" dir":""?>">
                            <img src="<?=DIRETORIO_RAIZ.response_attr($l->getUrlMedia())?>" alt="<?=$l->getTitulo()?>" width="344" />
                            <div></div>
                            <? if($l->getLegendaMedia()):?><p><?=response_html($l->getLegendaMedia())?></p><? endif;?>
                        </div>
                        <div class="txt-box">

                            <h3><?=response_html($l->getTitulo())?></h3>
                            <?=response_html($l->getTexto())?>
                            <?
                            $listaIcones = Media::listar($l->getId(), $idSecaoSel,"","status = ".Media::ATIVO,"ordem ASC");
                            //print Media::getLogSql();
                            if($listaIcones){
                            ?>
                                <h4><?=getTextoByLang("A SCA colabora com","SCA is associated with")?>:</h4>
                                <p>
                                    <? foreach ($listaIcones as $key => $l):?>
                                        <?
                                        $targetLink = $l->getTargetLink() == "_blank"?" rel='external'":"";
                                        $attrsTagLink = $l->getUrlLink()?" title=\"".$l->getNome()."\" href=\"".$l->getUrlLink()."\"$targetLink":"";

                                        ?>
                                        <a <?=$attrsTagLink ?>><img src="<?=DIRETORIO_RAIZ.$l->getDir()."ico_".$l->getUrl()?>" alt="<?=$l->getNome()?>" /></a><? if($key < count($listaIcones)-1):?><b></b><? endif;?>
                                    
                                    <? endforeach;?>
                                </p>
                            <? }?>
                        </div>
                    </div>
                    <? endforeach;?>
                <? else:?>
                     <div class="modulo">
                         <h2><img src="<?=DIRETORIO_RAIZ?>css/img/tit-<?=setUrlAmigavel($nome_img_tit).getTextoByLang("","-eng")?>.png" alt="<?=$nome_secao_atual?>" height="35" /> </h2>
                         <p><?=getTextoByLang("Conteúdo não encontrado!","Content not found!");?></p>
                     </div>    
                <? endif;?>
                <a href="javascript:history.back()" class="bt-voltar" title="<?=getTextoByLang("Voltar","Return")?>"> &lt; <?=getTextoByLang("Voltar","Return")?></a> 
                
            </div>
            
            <? include "includes/direita.php"?>
                        
        </div>
      </div>
    </div>
    <? include "includes/rodape.php"?>
</div>
</body>
</html>