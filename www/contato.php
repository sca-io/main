<?
require_once ('conf.php');
$idioma = getIdioma();
$idSecao = CONTATOS;
$nome_secao = "";
if(isset($settSecoes[$idSecao])) $nome_secao = getNomeSecao($idSecao);
$metaTitle = $nome_secao;
?><? include "includes/head.php"?>
<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <a href="javascript:;"><strong><?=$nome_secao?></strong></a>
            </div>
        </div>
        <div class="contatos">
            <div class="esquerda">
            	<h2><img src="<?=DIRETORIO_RAIZ?>css/img/tit-contato<?=getTextoByLang("","-eng")?>.png" alt="<?=$nome_secao?>"/> </h2>
                
                <blockquote>
                    <address>
                        <strong>Sociedade Corretora de Álcool Ltda</strong><br />
                        Rua Joaquim Floriano, 72 – CJ 173 – 17º andar<br />
                        Itaim Bibi – São Paulo, SP <br />
                        Cep: 04534-000<br />
                        Tel: +55 11 3709-4900
                    </address>
                </blockquote>
                
                <blockquote>
                    <address>
                    	<strong>Sociedade Corretora de Álcool Ltda – Filial</strong><br />
                        Av. Dep. Jamel Cecílio nº 3.310, Salas 408 e 409<br />
                        Jardim Goiás –  Goiânia, GO<br />
                        Cep: 74810-100 <br />
                        Tel: +55 62 3878-4900
                    </address>
                </blockquote> 
                
                <blockquote>
                    <address>
                    	<strong>SCA Trading S/A</strong><br />
                        Rua Joaquim Floriano, 72 – CJ 174 – 17º andar<br />
                        Itaim Bibi – São Paulo, SP <br />
                        Cep: 04534-000<br />
                        Tel: +55 11 3709-4900
                    </address>
                </blockquote> 
                
                <div class="localizacao">
                    <h4><?=getTextoByLang("Localização","Location")?></h4>
                    <? if(strpos($_SERVER['HTTP_USER_AGENT'],"MSIE 8") > -1 || strpos($_SERVER['HTTP_USER_AGENT'],"MSIE 7") > -1 || strpos($_SERVER['HTTP_USER_AGENT'],"MSIE 6") > -1): ?>
                        <iframe width="272" height="187" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com.br/maps?hl=pt-BR&amp;q=rua+joaquim+floriano+72&amp;ie=UTF8&amp;sqi=2&amp;hq=&amp;hnear=R.+Joaquim+Floriano,+72+-+Itaim+Bibi,+S%C3%A3o+Paulo&amp;t=m&amp;z=14&amp;ll=-23.583559,-46.671049&amp;output=embed"></iframe>
                    <? else:?>                    
                        <object style="border:0;" type="text/html" width="272" height="187" data="http://maps.google.com.br/maps?hl=pt-BR&amp;q=rua+joaquim+floriano+72&amp;ie=UTF8&amp;sqi=2&amp;hq=&amp;hnear=R.+Joaquim+Floriano,+72+-+Itaim+Bibi,+S%C3%A3o+Paulo&amp;t=m&amp;z=14&amp;ll=-23.583559,-46.671049&amp;output=embed"></object>
                    <? endif;?>
                </div>        
            </div>
            
            <div class="direita">
            	<h3><?=getTextoByLang("Todos os campos são de preenchimento obrigatório:","All fields are required:");?></h3>
            	<form id="formulario" action="">
                    <div class="formulario">
                        <p class="hidden"><input type="hidden" id="idioma" name="idioma" value="<?=$idioma?>" /></p>
                    	<div>
                        	<label id="lbl-nome"><?=getTextoByLang("Nome completo","Full name")?></label>
                            <input class="tipo1" type="text"  name="nome"  id="nome" maxlength="255" />
                        </div>
                        <div>
                        	<label id="lbl-email"><?=getTextoByLang("E-mail","E-mail")?></label>
                            <input class="tipo2" type="text" name="email" id="email" maxlength="255" />
                        </div>
                        <div>
                        	<label id="lbl-cidade"><?=getTextoByLang("Cidade","City")?></label>
                            <input class="tipo3" type="text" name="cidade" id="cidade" maxlength="255" />
                        	<label id="lbl-uf" class="uf"><?=getTextoByLang("UF","UF")?></label>
                            <input type="text" class="mask-uf tipo4" name="uf" id="uf" maxlength="2" />
                        </div>
                        <div>
                        	<label id="lbl-telefone"><?=getTextoByLang("Telefone","Phone")?></label>
                            <input type="text" class="mask-ddd tipo5" name="ddd" id="ddd" maxlength="2" onKeyUp="onCompleteFocusTo(this,'telefone')" />
                            <input type="text" class="mask-tel tipo6" name="telefone" id="telefone" />
                        </div>
                        <div>
                        	<label id="lbl-assunto"><?=getTextoByLang("Assunto","Subject")?></label>
                            <input class="tipo1" type="text" name="assunto" id="assunto" maxlength="200" />
                        </div>
                        <div>
                        	<label class="area" id="lbl-mensagem"><?=getTextoByLang("Mensagem","Message")?></label>
                            <textarea name="mensagem" id="mensagem" rows="" cols=""></textarea>
                        </div>
                    </div>
                </form>
                <a href="javascript:salvarContato();" class="bt-enviar<?=getTextoByLang("","-eng")?>" title="<?=getTextoByLang("Enviar","Submit")?>"><?=getTextoByLang("Enviar","Submit")?></a>
            </div>
                  
        </div>
        
      </div>
    </div>
    <? include "includes/rodape.php"?>
    <script type="text/javascript">
        /*** Mascaras dos campos ***/
        $(".mask-uf").mask("aa");
        $(".mask-ddd").mask("99");
        $(".mask-tel").mask("9999-9999");
        $(".mask-data").mask("99/99/9999");
    </script>
</div>
</body>
</html>