    <%
    Server.ScriptTimeOut = 90
    
    if request.QueryString("bd") = empty then response.end
    
    ' This script exports data from a databa
    '     se and creates
    ' data to be placed into a Transact-SQL 
    '     Batch File.
    Dim Objconn
    Dim ObjRs
    Dim StrBatch
    Set ObjConn = createobject("ADODB.Connection")
    Set ObjRs = createobject("ADODB.Recordset")
    ' You must modify this line to reference
    '     the database that the tables are in
    
    
    select case request.QueryString("bd")
		case 1
			Objconn.Open Application("bd-sisweb")
			StrBatch = _
    			TableData("Usinas") & _
    			TableData("Pedidos") & _
    			TableData("Admin")
    	case 2 
			Objconn.Open Application("bd-sisweb-antigos")
			StrBatch = TableData("Pedidos")
		case 3
			Objconn.Open Application("bd-sisweb-Retorna_PAntigos")
			StrBatch = TableData("Retorna_PAntigos")
    
		case 4
			Objconn.Open Application("bd-sisweb-retorno")
			StrBatch = TableData("Pedidos")
			
		case 5
			Objconn.Open Application("bd-sisweb-envio")
			StrBatch = TableData("Pedidos")
		
    
    end select
    
    ObjConn.Close
    Response.Write "<PRE>" & StrBatch & "</PRE><hr>"
    Set ObjRs = Nothing
    Set ObjConn = Nothing
    
    
    
    
    
    
    
    
    Function TableData(ByRef pStrTable)
    	
    	Dim lStrSQL
    	Dim lLngMaxIndex
    	Dim lLngIndex
    	Dim lLngVarType
    	
    	lStrSQL = "SELECT * FROM " & pStrTable
    	Set ObjRs = objconn.Execute(lStrSQL)
    	
    	If objRs.EOF Then Exit Function
    	
    	lLngMaxIndex = ObjRs.Fields.Count - 1
    	
    	lStrSQL = "-- " & pStrTable & vbCrLf
    	
    	While Not ObjRs.EOF
    		'lStrSQL = lStrSQL & "INSERT INTO [" & pStrTable & "]"' &"("
    		
    		'nomes dos campos
    		'For lLngIndex = 0 To lLngMaxIndex
    		'	lStrSQL = lStrSQL & "[" & ObjRs.Fields(lLngIndex).Name & "]"
    		'	If Not lLngIndex = lLngMaxIndex Then lStrSQL = lStrSQL & ", "
    		'Next
    		
    		'lStrSQL = lStrSQL & ") VALUES ("
    		'lStrSQL = lStrSQL & " VALUES ("
    		
    		For lLngIndex = 0 To lLngMaxIndex
    			lVarValue = ObjRs.Fields(lLngIndex).Value
    			lLngVarType = VarType(lVarValue)
    			Select Case lLngVarType
    				Case vbLong, vbInteger, vbDouble, vbSingle, vbDecimal, vbCurrency
    					lStrSQL = lStrSQL & ObjRs.Fields(lLngIndex).Value
    				Case vbNull
    					lStrSQL = lStrSQL & "NULL"
    				Case vbString, vbDate
    					lStrSQL = lStrSQL & """" & Replace(Replace(lVarValue, "'", "''"), vbcrlf, "\n") & """"
    			End Select
    			If Not lLngIndex = lLngMaxIndex Then lStrSQL = lStrSQL & "|"
    		Next
    		
    		'lStrSQL = lStrSQL & ");" & vbCrLf
    		lStrSQL = lStrSQL & vbCrLf
    		ObjRs.MoveNext
    	Wend
    	'TableData = lStrSQL & "--" & vbCrLf & vbCrLf
    	TableData = lStrSQL & vbCrLf & vbCrLf
    End Function
    %>	
