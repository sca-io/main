<% SESSION.LCID = 1046

Set BD=CreateObject("ADODB.Connection")
Set TB=CreateObject("ADODB.Recordset")


dim strInfoAdicionalPedido

if request.QueryString("tipo") = "pend" then
	BD.Open Application("bd-sisweb")
	strInfoAdicionalPedido = empty
else
	BD.Open Application("bd-sisweb-antigos")	
end if

TB.Open "Select * From Pedidos Where N_Pedido=" & sonumeros(Server.URLEncode(request.QueryString("nped"))) &" And codsdr=" & _
request.QueryString("cod") &" And Modalidade='" & Server.URLEncode(request.QueryString("mped"))  & "'" , BD

If TB.EOF Then Response.Redirect "acesso.asp?menu=acesso_pedido_invalido"

''''''''''''''''''''''''''''''''
'Informa��o de ped alterado ou cancelado

if request.QueryString("tipo") = "pend" then
		IF TB.Fields("alterado") = -1 Then
				strInfoAdicionalPedido = "Pedido alterado em " & TB("UltimaAtualiza")
		ELSE
				strInfoAdicionalPedido = "Pedido Pendente"
		END IF
else
	IF TB.Fields("status").value = "C" THEN
			strInfoAdicionalPedido = "Pedido cancelado em " & TB("UltimaAtualiza")
		ELSEIF TB.Fields("status").value = "A" AND TB.Fields("alterado").value = -1 THEN
			strInfoAdicionalPedido = "Pedido alterado em " & TB("UltimaAtualiza")
		ELSEIF TB.Fields("status").value = "A" THEN
			strInfoAdicionalPedido = "Pedido aprovado em " & TB("UltimaAtualiza")
	END IF
	
END IF
''''''''''''''''''''''''''''''''


if request.QueryString("mped") = "C" then
	MODA = "CARBURANTE"
else
	MODA = "OUTROS FINS"
end if

If TB.RecordCount = 0 Then Response.Redirect "central.asp"

session("NOMEOP")=TB.Fields("Nome_op").value
session("email")=TB.Fields("email").value


If Err.number <> 0 Then
	If err.number <> 13 Then ' Se o e-mail estiver em branco
		Response.Redirect "login.asp?acao=erro&nmr=" & Err.description
	End If
End If
	  
''''''''''''''''''''''''''
' Formata��o do CNPJ
Function CNPJ(Numero)

	CNPJ = Left(Numero, 2) & "." & Mid(Numero, 3, 3) & "." & Mid(Numero, 6, 3) & "/" & Mid(Numero, 9, 4) & "-" & Mid(Numero, 13, 2)

End Function
'
''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''''
' M�SCARA PARA O VOLUME
VOLUMERESPOSTA = FormatNumber(TB.Fields("VOLUME").value, 3)
'''''''''''''''''

%>
<%IF Len(TB.Fields("Modalidade").value)=1 Then MODA="CARBURANTE" Else MODA="OUTROS FINS" 


''''''''''''''''''''''''''
' Identifica��o do pedido
''''''''''''''''''''''''''

carimbo	= trim(TB.Fields("N_pedido").value) & "&" & trim(TB.Fields("estado").value) & "&" & ucase(trim(TB.Fields("cod_cli").value)) & "&" & left(trim(TB.Fields("razao_social_us")),4) & "&" & TB.Fields("VOLUME").value & "&" & trim(TB.Fields("Nome_Produto").value) & "&" & TB.Fields("val_tot").value & "&" & trim(TB.Fields("Preco_Bruto").value)
'Criptografia
Err.Clear
     For r = 1 To Len(carimbo)
          letra = Mid(carimbo, r, 1)
          cripto = (Asc(letra) + 100) & ""
          ass = ass + cripto
     Next


''''''''''''''''''''''''''
' Fim da Identifica��o do pedido
''''''''''''''''''''''''''

%>
<style>
.info-pedido TABLE{border:1px solid #000;border-bottom:0 }
.info-pedido .sem-borda{border:0}
.info-pedido .ultimo{border-bottom:1px solid #000}
</style>

<div class="info-pedido">
<CENTER>
        <FONT face="Verdana" size="1"><B>SOCIEDADE CORRETORA DE �LCOOL LTDA.</B><BR>
            Fone : (0xx11) 3709-4900 - Fax (0xx11) 3709-4901 - www.scetanol.com.br<BR>
        </FONT>
        <HR size="1" color="#C0C0C0" noshade style="margin-top: 0">
    </CENTER>					
					
		<TABLE id="Table6" cellspacing="1" cellpadding="1" width="97%" border="0" align="center" class="sem-borda">
			<TR>
				<TD align="left"><FONT size="1" face="Tahoma"><B>Pedido de venda SCA -&nbsp;<%=MODA %></B></FONT></TD>
				<TD align="right"><FONT size="1" face="Tahoma">Impresso em <%=Date%> �s <%=Hour(Time) & ":" & Minute(Time)%></FONT></TD>
			</TR>
		</TABLE>
					
				<TABLE border="0" id="Table1" width="97%" align="center">
					<TR>
						<TD width="97%" colspan="4" bordercolor="#FFFFFF">
							<FONT face="Tahoma" size="1"><B><U>PEDIDO </U></B></FONT>
						</TD>
					</TR>
					<TR>
						<TD width="25%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">N�mero:</FONT></STRONG><FONT face="Tahoma" size="1">&nbsp;<%=TB.Fields("N_pedido").value %>
								-
								<%=TB("Tipo_pedido")%>
							</FONT>
						</TD>
						<TD width="25%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Data:</FONT></STRONG><FONT face="Tahoma" size="1">&nbsp;<%=TB.Fields("Data").value %></FONT></TD>
						<TD width="25%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">M�s base:</FONT></STRONG><FONT face="Tahoma" size="1">&nbsp;<%=TB.Fields("mes_ano").value %></FONT></TD>
						<TD width="25%" nowrap bordercolor="#FFFFFF"><STRONG> <FONT face="Verdana" size="1">Operador:</FONT></STRONG><FONT face="Tahoma" size="1"><B>&nbsp;</B><%=TB.Fields("nome_op").value %></FONT></TD>
					</TR>
				</TABLE>
		<DIV align="center">			
				<TABLE border="0" width="97%" id="Table2" style="text-align:left">
					<TR>
						<TD width="100%" colspan="6" bordercolor="#FFFFFF">
							<FONT face="Tahoma" size="1"><B><U>ADQUIRENTE</U>&nbsp;</B></FONT></TD>
					</TR>
					<TR>
						<TD width="40%" nowrap bordercolor="#FFFFFF" colspan="2">
							<FONT face="Verdana" size="1"><STRONG>C�digo</STRONG></FONT><STRONG><FONT face="Verdana" size="1"><% If MODA="CARBURANTE" THEN Response.Write(" (ANP)") %>:</FONT></STRONG><FONT face="Tahoma" size="1"><B>&nbsp;</B><% Response.Write(TB.Fields("cod_cli").value) %></FONT></TD>
						<TD width="30%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG> <FONT face="Verdana" size="1">CNPJ:</FONT></STRONG><FONT face="Tahoma" size="1"><B>&nbsp;</B>
								<% Response.Write(Left(TB.Fields("cgc").value,2) &"." & Mid(TB.Fields("cgc").value,3,3) _
    &"." & Mid(TB.Fields("cgc").value,6,3) &"/" & _
    Mid(TB.Fields("cgc").value,9,4) &"-" & Mid(TB.Fields("cgc").value,13,2)) %>
							</FONT>
						</TD>
						<TD width="30%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG> <FONT face="Verdana" size="1">Inscr. 
									estadual:</FONT></STRONG><B><FONT face="Tahoma" size="1">&nbsp;</FONT></B><FONT face="Tahoma" size="1"><%=TB.Fields("inscricao") %></FONT></TD>
					</TR>
					<TR>
						
        				<TD width="100%" bordercolor="#FFFFFF" colspan="6"><STRONG> <FONT face="Verdana" size="1">Raz�o 
          Social:</FONT></STRONG><FONT face="Tahoma" size="1"><B>&nbsp;</B><%=TB.Fields("nome_cli").value %></FONT></TD>
					</TR>
					<%
					' O Campo Base de faturamento n�o est� nos pedidos "Outros Fins"
					If request.QueryString("mped") <> "OF" Then
					%>
					<TR>
						<TD width="100%" bordercolor="#FFFFFF" colspan="6"><STRONG> <FONT face="Verdana" size="1">Base 
									de Faturamento:</FONT></STRONG><FONT face="Tahoma" size="1"><B>&nbsp;</B><%=TB.Fields("nome_centro").value %></FONT></TD>
					</TR>
					<%End If%>
					<TR>
						<%'>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>%>
						<FONT size="1"></FONT>
						<TD width="100%" bordercolor="#FFFFFF" colspan="6">
							<FONT face="Tahoma" size="1"><B><U>
										<CENTER>
            Endere�o de Faturamento 
          </CENTER>
									</U></B></FONT>
						</TD>
					</TR>
					<TR>
						<TD width="88%" colspan="5" bordercolor="#FFFFFF"><STRONG> <FONT face="Verdana" size="1">Endere�o:&nbsp;</FONT></STRONG><FONT face="Tahoma" size="1"><%=TB.Fields("endereco").value %></FONT></TD>
						<TD width="12%" bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Fone:</FONT></STRONG><FONT face="Tahoma" size="1"><B>&nbsp;</B><%=TB.Fields("telefone").value %></FONT></TD>
					</TR>
					<TR>
						<TD width="22%" bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">CEP:</FONT></STRONG><FONT face="Tahoma" size="1"><B>&nbsp;</B>
								<% POS=Instr(1,TB.Fields("cep").value,"-")
    
    if pos=0 then    
         Response.Write(Left(TB.Fields("cep").value,5) & "-" & Mid(TB.Fields("cep").value,6,3))
      else
         Response.Write(TB.Fields("cep").value)
    End if %>
							</FONT>
						</TD>
						<TD width="50%" bordercolor="#FFFFFF" colspan="2"><STRONG> <FONT face="Verdana" size="1">Cidade:</FONT></STRONG><FONT face="Tahoma" size="1"><B>&nbsp;</B><%=TB.Fields("cidade").value %></FONT></TD>
						<TD width="14%" bordercolor="#FFFFFF" colspan="2"><STRONG> <FONT face="Verdana" size="1">UF:</FONT></STRONG><FONT face="Tahoma" size="1"><B>&nbsp;</B><%=TB.Fields("estado").value %></FONT></TD>
						<TD width="31%" bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Bairro:</FONT></STRONG><FONT face="Tahoma" size="1"><B>&nbsp;</B><%=TB.Fields("bairro").value %></FONT></TD>
					</TR>
					<%'>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>%>
					<FONT size="1"></FONT>
					<TD width="100%" bordercolor="#FFFFFF" colspan="6">
						<FONT face="Tahoma" size="1"><B><U>
									<CENTER>
            Endere�o de Cobran�a 
          </CENTER>
								</U></B></FONT>
					</TD>
					</tr>
					<TR>
						<TD width="100%" colspan="6" bordercolor="#FFFFFF">
							<FONT face="Tahoma" size="1"><B>Endere�o:&nbsp;</B><%=TB.Fields("endereco_cob").value %></FONT></TD>
					</TR>
					<TR>
						<TD width="22%" bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">CEP:</FONT></STRONG><FONT face="Tahoma" size="1"><B>&nbsp;</B>
								<% POS=Instr(1,TB.Fields("cep_cob").value,"-")
    
    if pos=0 then    
         Response.Write(Left(TB.Fields("cep_cob").value,5) & "-" & Mid(TB.Fields("cep_cob").value,6,3))
      else
         Response.Write(TB.Fields("cep_cob").value)
    End if %>
							</FONT>
						</TD>
						<TD width="50%" bordercolor="#FFFFFF" colspan="2"><STRONG> <FONT face="Verdana" size="1">Cidade:</FONT></STRONG><FONT face="Tahoma" size="1"><B>&nbsp;</B><%=TB.Fields("cidade_cob").value %></FONT></TD>
						<TD width="14%" bordercolor="#FFFFFF" colspan="2"><STRONG> <FONT face="Verdana" size="1">UF:</FONT></STRONG><FONT face="Tahoma" size="1"><B>&nbsp;</B><%=TB.Fields("estado_cob").value %></FONT></TD>
						<TD width="31%" bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Bairro:</FONT></STRONG><FONT face="Tahoma" size="1"><B>&nbsp;</B><%=TB.Fields("bairro_cob").value %></FONT></TD>
				</TABLE>

		</DIV>
		<DIV align="center">

				<TABLE border="0" width="97%" id="Table3" style="text-align:left">
					<TR>
						<TD colspan="2" bordercolor="#FFFFFF"><FONT face="Tahoma" size="1"><B><U>PRODUTOR</U>&nbsp;</B></FONT></TD>
					</TR>
					<TR>
						<TD width="60%" bordercolor="#FFFFFF"><FONT size="1"><STRONG> <FONT face="Verdana">Raz�o Social</FONT></STRONG><B>:</B></FONT><B><FONT size="1">&nbsp;</FONT></B><FONT size="1">&nbsp;
							</FONT><FONT face="Tahoma" size="1">
								<%=TB.Fields("Razao_Social_Us") %>
							</FONT><FONT size="1">&nbsp; </FONT>
						</TD>
						<TD width="40%" bordercolor="#FFFFFF"><FONT size="1"><STRONG> <FONT face="Verdana">Local 
										Entrega</FONT></STRONG><B>:</B></FONT><B><FONT size="1">&nbsp;</FONT></B><FONT size="1">&nbsp;
							</FONT><FONT face="Tahoma" size="1">
								<%=TB.Fields("local").value %>
							</FONT><FONT size="1">&nbsp; </FONT>
						</TD>
					</TR>
					<TR>
						<TD width="60%" bordercolor="#FFFFFF"><FONT size="1"><STRONG> <FONT face="Verdana">CNPJ</FONT></STRONG><B>:</B></FONT><B><FONT size="1">&nbsp;</FONT></B><FONT size="1">&nbsp;
							</FONT><FONT face="Tahoma" size="1">
								<% Response.Write(Left(TB.Fields("cgc_us").value,2) &"." & Mid(TB.Fields("cgc_us").value,3,3) _
    &"." & Mid(TB.Fields("cgc_us").value,6,3) &"/" & _
    Mid(TB.Fields("cgc_us").value,9,4) &"-" & Mid(TB.Fields("cgc_us").value,13,2)) %>
							</FONT><FONT size="1">&nbsp; </FONT>
						</TD>
						<TD width="60%" bordercolor="#FFFFFF"><FONT size="1"><STRONG> <FONT face="Verdana">Cidade</FONT></STRONG><B>:</B></FONT><B><FONT size="1">&nbsp;</FONT></B><FONT size="1">&nbsp;
							</FONT><FONT face="Tahoma" size="1">
								<%=TB.Fields("Cidade_US") %>
							</FONT><FONT size="1">&nbsp; </FONT>
						</TD>
					</TR>
				</TABLE>

		</DIV>
		<DIV align="center">

				<!--  DADOS DO PEDIDO  -->
				<%Select Case TB("Modalidade")
				Case "C"
				%>
				<TABLE border="0" width="97%" id="Table4" style="text-align:left">
					<TR>
                    	<TD colspan="5" bordercolor="#FFFFFF" height="16"><FONT size="1" face="Tahoma"><B><U>DADOS DO PEDIDO</U></B></FONT></TD>
					</TR>
					<TR bordercolor="#FFFFFF">						
      					<TD width="17%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Tipo de &Aacute;lcool:</FONT></STRONG></TD>
						<TD colspan="4" bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=TB("Nome_produto")%></FONT>&nbsp;</TD>
					</TR>
					<TR bordercolor="#FFFFFF">
						<TD width="17%" nowrap bordercolor="#FFFFFF"><STRONG><FONT size="1"><FONT face="Verdana">C�digo NCM:</FONT></FONT></STRONG></TD>
						<TD width="34%" bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=TB("NCM")%></FONT>&nbsp;</TD>
						<TD width="26%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">Volume (m&sup3;):</FONT></STRONG></TD>
						<TD width="23%" bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=VolumeResposta%></FONT>&nbsp;</TD>
					</TR>
					<TR bordercolor="#FFFFFF">
						<TD width="17%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Tabela&nbsp;de Pre�o:</FONT></STRONG></TD>
						<TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=TB("Tabela_preco")%></FONT>&nbsp;</TD>						
      					<TD width="26%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">Pre&ccedil;o &agrave; Vista (R$/m&sup3;):</FONT></STRONG></TD>
      					<TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Val_Tot"),2)%></FONT></TD>
					</TR>
					<TR bordercolor="#FFFFFF">						
      					<TD width="17%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Pre�o da Tabela:</FONT></STRONG></TD>
						<TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Valor_Unitario"),2)%></FONT>&nbsp;</TD>
						
      					<TD width="26%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">Pre�o de Venda PVU (R$/m&sup3;):</FONT></STRONG></TD>
      					<TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Preco_Prazo"),2)%></FONT></TD>
					</TR>
					<TR bordercolor="#FFFFFF">
                    	<TD width="17%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Desconto Aplicado (%):</FONT></STRONG></TD>
						<TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Desc_aplicado"),2)%></FONT>&nbsp;</TD>						
      					<TD width="26%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">Subs�dio (incluso) (R$/m&sup3;):</FONT></STRONG></TD>
      					<TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Subsidio"),2)%></FONT></TD>
					</TR>
					<TR bordercolor="#FFFFFF">
						
      					<TD width="17%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Desconto 
        Aplicado (R$/m&sup3;):</FONT></STRONG></TD>
						<TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Valor_Desc_Aplicado"),2)%></FONT>&nbsp;</TD>
						
      <TD width="26%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">Pre�o 
        L�quido ao Produtor (R$/m&sup3;):</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=TB("PLP")%></FONT></TD>
					</TR>
					<TR bordercolor="#FFFFFF">
						<TD width="17%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Frete 
									(R$):</FONT></STRONG></TD>
						<TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Valor_Frete"),2)%></FONT>&nbsp;</TD>
						
      <TD width="26%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">CIDE 
        (R$/m&sup3;):</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=TB("CIDE")%></FONT></TD>
					</TR>
					<TR bordercolor="#FFFFFF">
						
      <TD width="17%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Modalidade:</FONT></STRONG></TD>
						
      <TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1">
        <%If TB("cif")=FALSE then Response.Write("PVU") Else Response.Write("CIF") %>
        </FONT>&nbsp;<FONT face="Verdana" size="1">&nbsp; </FONT></TD>
						
      <TD width="26%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">Complemento 
        do PIS (R$/m&sup3;):</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=TB("Complemento_PIS")%></FONT></TD>
					</TR>
					<TR bordercolor="#FFFFFF">
						
      <TD width="17%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">In�cio 
        Carregamento</FONT><FONT size="1" face="Verdana">:</FONT></STRONG></TD>
						
      <TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=TB("Inicio_op")%></FONT>&nbsp;</TD>
						
      <TD width="26%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">Complemento 
        da COFINS (R$/m&sup3;):</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=TB("Complemento_COFINS")%></FONT></TD>
					</TR>
					<TR bordercolor="#FFFFFF">
						
      <TD width="17%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">ICMS 
        (%):</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"> <%=TB("ICMS")%> 
        </FONT><FONT face="Verdana" size="1">&nbsp; </FONT>&nbsp;</TD>
						
      <TD width="26%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">ICMS 
        (R$/m&sup3;):</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=TB("Valor_ICMS")%></FONT></TD>
					</TR>
					<TR bordercolor="#FFFFFF">
						
      <TD width="17%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Encargos 
        Financeiros (%):</FONT></STRONG></TD>
						
      <TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=TB("Taxa_Juros")%>&nbsp;</FONT><FONT size="1">&nbsp;</FONT></TD>
						
      <TD bordercolor="#FFFFFF" width="26%" nowrap colspan="2"><FONT face="Verdana" size="1"><STRONG><FONT face="Verdana" size="1">Pre�o 
        Bruto (Produto) (R$/m&sup3;):</FONT></STRONG></FONT></TD>
      <TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Preco_bruto"),2)%></FONT></TD>
					</TR>
					<TR bordercolor="#FFFFFF">
						
      <TD width="17%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Forma 
        de Pagamento:</FONT></STRONG></TD>
      <TD colspan="2" bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=TB("Descricao_Pagamento")%></FONT></TD>
						
      <TD bordercolor="#FFFFFF" width="26%" nowrap><FONT face="Verdana" size="1"><STRONG>Pre�o 
        Bruto (Produto + Frete) (R$/m&sup3;):</STRONG></FONT></TD>
      <TD bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Preco_bruto_frete"),2)%></FONT></TD>
					</TR>
				</TABLE>
				<%
	Case Else 'outros fins
%>
				<TABLE border="0" width="97%" id="Table5" style="text-align:left">
					<TR>
						<TD colspan="5" bordercolor="#FFFFFF" height="16">
							<FONT size="1" face="Tahoma"><B><U>DADOS DO PEDIDO</U></B></FONT></TD>
					</TR>
					<TR>
						<TD width="21%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Tipo de 
									�lcool:</FONT></STRONG></TD>
						<TD bordercolor="#FFFFFF" height="18" colspan="4"><FONT face="Verdana" size="1"><%=TB("Nome_produto")%></FONT></TD>
					</TR>
					<TR>
						<TD width="21%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">C�digo 
									NCM:</FONT></STRONG></TD>
						<TD width="28%" height="18" bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=TB("NCM")%></FONT></TD>
						<TD width="25%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">Volume 
									(m&sup3;):</FONT></STRONG></TD>
						<TD width="25%" height="18" bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=VolumeResposta%></FONT></TD>
					</TR>
					<TR>
						
      <TD width="21%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Tabela 
        de Pre�o:</FONT></STRONG></TD>
						<TD bordercolor="#FFFFFF" height="18"><FONT face="Verdana" size="1"><%=TB("Tabela_preco")%></FONT></TD>
						
      <TD width="25%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">Pre&ccedil;o 
        &agrave; Vista (R$/m&sup3;):</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF" height="18"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Val_Tot"),2)%> 
        </FONT> </TD>
					</TR>
					<TR>
						
      <TD width="21%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Pre�o 
        da Tabela:</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF" height="18"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Valor_unitario"),2)%></FONT></TD>
						
      <TD width="25%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">Pre�o 
        de Venda PVU (R$/m&sup3;):</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF" height="18"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Preco_Prazo"),2)%> 
        </FONT> </TD>
					</TR>
					<TR>
						
      <TD width="21%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Desconto 
        Aplicado (%):</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF" height="18"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Desc_aplicado"),2)%> 
        </FONT></TD>
						
      <TD width="25%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">Subs�dio 
        (incluso) (R$/m&sup3;):</FONT></STRONG></TD>
      <TD height="18" bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Subsidio"),2)%> 
        </FONT></TD>
					</TR>
					<TR>
						
      <TD width="21%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Desconto 
        Aplicado (R$/m&sup3;):</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF" height="18"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Valor_desc_aplicado"),2)%> 
        </FONT></TD>
						
      <TD width="25%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">Pre�o 
        L&iacute;quido ao Produtor (R$/m&sup3;):</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF" height="18"><FONT face="Verdana" size="1"><%=TB("PLP")%> 
        </FONT></TD>
					</TR>
					<TR>
						
      <TD width="21%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">
		Frete (R$):</FONT></STRONG></TD>
						
      <TD bordercolor="#FFFFFF" height="18"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Valor_frete"),2)%> 
        </FONT></TD>
						
      <TD width="25%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">PIS 
        (R$/m&sup3;):</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF" height="18"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Complemento_PIS"),2)%> 
        </FONT></TD>
					</TR>
					<TR>
						
      <TD width="21%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Modalidade:</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF" height="18"><FONT face="Verdana" size="1"><FONT face="Verdana" size="1">
        <%If TB("cif")=FALSE then Response.Write("PVU") Else Response.Write("CIF")%>
        </FONT></FONT></TD>
						
      <TD width="25%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">COFINS 
        (R$/m&sup3;):</FONT></STRONG></TD>
      <TD bordercolor="#FFFFFF" height="18"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("Complemento_Cofins"),2)%></FONT> 
      </TD>
					</TR>
					<TR>
						
      <TD width="21%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">ICMS 
        (%):</FONT></STRONG></TD>
						
      <TD height="18" bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=FormatNumber(0+TB("ICMS"),0)%> 
        </FONT></TD>
						
      <TD width="25%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">ICMS 
        (R$/m&sup3;):</FONT></STRONG></TD>
						
      <TD height="18" bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=TB("Valor_ICMS")%> 
        </FONT></TD>
					</TR>
										<TR>
						
      <TD width="21%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">In&iacute;cio 
        do Carregamento:</FONT></STRONG></TD>
      <TD height="18" bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><FONT face="Verdana" size="1"> 
        <%=TB("Inicio_op")%> </FONT></FONT></TD>
						
      <TD width="25%" nowrap bordercolor="#FFFFFF" colspan="2"><STRONG><FONT face="Verdana" size="1">Pre&ccedil;o 
        Bruto (Produto) (R$/m&sup3;):</FONT></STRONG></TD>
						
      <TD height="18" bordercolor="#FFFFFF"><FONT face="Verdana" size="1">
        <%=FormatNumber(TB("preco_bruto"),2)%>
        </FONT></TD>
					</TR>
										<TR>
						
      <TD width="21%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Encargos 
        Financeiros (%):</FONT></STRONG></TD>
						
      <TD height="18" bordercolor="#FFFFFF" colspan="2"><FONT face="Verdana" size="1"><%=TB("Taxa_Juros")%></FONT></TD>
						
      <TD width="25%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Pre&ccedil;o 
        Bruto (Produto + Frete) (R$/m&sup3;):</FONT></STRONG></TD>
						
      <TD height="18" bordercolor="#FFFFFF"><FONT face="Verdana" size="1">
        <%=FormatNumber(TB("Preco_bruto_frete"),2)%>
        </FONT></TD>
					</TR>
					<TR bordercolor="#FFFFFF">
						
      <TD width="21%" nowrap bordercolor="#FFFFFF"><STRONG><FONT face="Verdana" size="1">Forma 
        de Pagamento:</FONT></STRONG></TD>
						
      <TD colspan="4" bordercolor="#FFFFFF"><FONT face="Verdana" size="1"><%=TB("Descricao_pagamento")%></FONT></TD>
					</TR>
				</TABLE>				
	<%End Select%>

			<!--  FIM DOS DADOS DO PEDIDO  -->
		</DIV>

			<TABLE border="0" width="97%" align="center" id="table7" class="ultimo">
			<TR>
			<TD width="7%" align="left" valign="top" bordercolor="#FFFFFF"><FONT size="1" face="tahoma"><B><U>
			OBS:</U> &nbsp;</B> </FONT> </TD>
			<TD width="93%" valign="top" bordercolor="#FFFFFF"><FONT size="1" face="tahoma"><B>            
                (1�) O n�o pagamento nas datas previamente combinadas, poder� determinar o CANCELAMENTO deste pedido.<br>
                (2�) A atribui��o de limite de cr�dito para pagamento a prazo � de exclusiva compet�ncia da UNIDADE PRODUTORA.<br>
                (3�) Revestindo-se a opera��o objeto deste pedido da condi��o:<br>
                a) FOB (PVU), os volumes de etanol vendidos ser�o entregues pela UNIDADE PRODUTORA, em sua sede, a transportador contratado pela ADQUIRENTE, o qual receber� o produto e o transportar� por ordem e conta e sob a exclusiva responsabilidade da ADQUIRENTE, at� o estabelecimento que esta indicar, assumindo todos os direitos e riscos do produto.<br>
                b) CIF, os volumes de etanol vendidos ser�o entregues pela UNIDADE PRODUTORA por sua ordem e conta e sob sua responsabilidade no estabelecimento que a ADQUIRENTE indicar. A ADQUIRINTE assume todos os direitos e  riscos do produto assim que o produto � descarregado no estabelecimento que a ADQUIRINTE indicou. Caso o local indicado n�o aceite o recebimento/descarregamento do produto, todos os custos de uma nova opera��o de entrega (frete, impostos, encargos, etc) dever�o ser pagos pela ADQUIRINTE;<br>
                (4�) A UNIDADE PRODUTORA e a ADQUIRENTE concordam que todos os termos e condi��es da compra e venda est�o explicitados presentes no Pedido de Venda SCA. A validade de qualquer termo ou condi��o que n�o esteja explicitada neste pedido emitida pela SCA, n�o ter� validade judicial ou extrajudicial;<br>
                (5�) A partir da efetiva entrega do produto ao transportador, a ADQUIRENTE assumir� a exclusiva responsabilidade tribut�ria, criminal,civil, ambiental e/ou administrativa resultante da utiliza��o e do destino que venha dar ao produto, e, ainda, no caso de tratar-se de etanol anidro carburante, a ADQUIRENTE responsabilizar-se-� dar-lhe a destina��o exclusivade mistura � gasolina "A", na produ��o da mistura carburante, nos termos da Lei Federal n� 9.478, de 06/06/1997.<br>
                (6�) Caso a cliente n�o preencha os requisitos legais para a aquisi��o do produto, o pre�o adiantado ser-lhe-� restitu�do no valor l�quido das despesas banc�rias incorridas e de tributos incidentes na restitui��o.<br>
                (7�) A ADQUIRENTE n�o poder� devolver/rejeitar o produto adquirido da VENDEDORA, exceto se o produto n�o estiver de acordo com as especifica��es valor l�quido das despesas banc�rias incorridas e de tributos incidentes na restitui��o.<br>
                (8�) O carregamento do etanol na UNIDADE PRODUTORA ocorrer� se e somente se o meio de transporte estiver de acordo com a Lei e Normas vigentes, bem como forem atendidos todos os requisitos de seguran�a.<br>
                (9�) No carregamento do etanol, ser� entregue ao respons�vel pelo transporte ou recebimento, a ficha de qualidade contendo as especifica��es do etanol.<br>
                (10�) A UNIDADE PRODUTORA � livre de qualquer responsabilidade sobre o atraso ou n�o entrega do produto em caso de situa��o enquadrada como caso fortuito ou for�a maior.           
            
         			
     			
		<%If ucase(trim(TB("nome_centro"))) = "SER� INFORMADA PELO CLIENTE" Then %>
            <BR>As bases para faturamento dever�o ser informadas no prazo m�ximo de 4 dias �teis.
        <%End If%>

			<%
			dim strerrobdobservacao
			strerrobdobservacao = TB.Fields("Observacao").value
			
			response.Write "</b><br>" & strerrobdobservacao & "" %>
			</FONT>
			</TD>
			</TR>
			<TR>
			<TD height="2" colspan="2" valign="middle" bordercolor="#FFFFFF"><HR align="center" size="1" noshade>
			</TD>
			</TR>
			<TR align="left" valign="top">
			<TD bordercolor="#FFFFFF"><FONT size="1" face="tahoma"><B><U>Destina��o:</U></B></FONT><FONT size="1">&nbsp;
			</FONT>
			</TD>
			<TD bordercolor="#FFFFFF"><FONT size="1" face="tahoma">
			<%
			dim strerrobdobsDestinacao
			strerrobdobsDestinacao = TB.Fields("Obs_Destinacao").value
			
			response.Write strerrobdobsDestinacao%>
			</FONT></TD>
			</TR>
			<TR align="left" valign="top">
			<TD bordercolor="#FFFFFF"><FONT size="1" face="tahoma"><B><U>ICMS:</U></B></FONT><FONT size="1">&nbsp;
			</FONT>
			</TD>
			<TD bordercolor="#FFFFFF"><FONT size="1" face="tahoma">
			<%
			dim strerrobdobsicms
			strerrobdobsicms = TB.Fields("Obs_ICMS").value
			
			response.Write strerrobdobsicms%>
			</FONT></TD>
			</TR>
			<TR align="left" valign="top">
			<TD bordercolor="#FFFFFF"><FONT size="1" face="tahoma"><B><U>IPI:</U></B></FONT><FONT size="1">&nbsp;
			</FONT>
			</TD>
			<TD bordercolor="#FFFFFF"><FONT size="1" face="tahoma">
			<%=TB.Fields("Obs_IPI").value%>
			</FONT></TD>
			</TR>
		</TABLE>
		</div>
</div> 

<%


'Rotina de e-mail para visualiza��o de pedidos cancelados
'RotinaPedidoCancelado(request.QueryString("nped"), request.QueryString("mped"), TB.Fields("email").value & "")
if request.QueryString("email") = "enviar" AND request.QueryString("tipo") = "canc" then
	RotinaPedidoCancelado nped, mped, session("email")
end if

%>

<%
if strInfoAdicionalPedido <> empty then

	Response.Write "<p align=""center"" style=""margin-top: 0; margin-bottom: 0""><b><font face=""Verdana"" size=""2"" color=""#800080"">"
	Response.Write strInfoAdicionalPedido
	Response.Write "</font></b></p>"

end if
%>
<FONT face="Tahoma" size="1" style="margin-top: 0; margin-bottom: 0">
        <P align="right">
        _______________________________________________________________<BR>
        <B>Assinatura do Cliente (Adquirente)</B>&nbsp;</P>

<FONT face="Tahoma" size="1">
<P align="left"><STRONG><U>Notas:</U></STRONG>
<OL style="font-family: Tahoma; font-size: 8 pt; margin-top: 0; margin-bottom: 0">
						<LI>
							<FONT face="Tahoma" size="1">Demais termos e condi��es, consultar o regulamento de 
								vendas da SCA, em vigor, de conhecimento das partes.</FONT></LI>
						<LI>
							<FONT face="Tahoma" size="1">Este pedido de venda deve ser assinado por pessoa(s) 
								que, consoante c�pia do estatuto ou do contrato social em poder da SCA, esteja(m) investida(s) 
								de poderes para representar a ADQUIRENTE.</FONT></LI>
						<LI>
							<FONT face="Tahoma" size="1">Este pedido de venda, devidamente assinado, 
								deve ser encaminhado via e-mail, � SCA, para formaliza��o da venda.</FONT></LI>
						<LI>
							<FONT face="Tahoma" size="1">Os pagamentos dever�o ser feitos diretamente � unidade 
								produtora.</FONT></LI>
						<LI>
							<FONT face="Tahoma" size="1">Os pagamentos dever�o ser efetuados por interm�dio de 
								ag�ncia banc�ria da localidade onde se encontra o estabelecimento do comprador, 
								com apresenta��o dos respectivos comprovantes.</FONT></LI>
						<LI>
							<FONT face="Tahoma" size="1">Os pagamentos em cheques ou DOC estar�o sujeitos aos 
								prazos de compensa��o determinados pelo Banco Central, sendo que somente ap�s 
								as libera��es o �lcool ser� carregado.</FONT></LI>
						<LI>
							<FONT face="Tahoma" size="1">A base para faturamento poder� ser alterada pelo 
								comprador, mediante descri��o na ordem de faturamento.</FONT></LI>
					</OL>

			<TABLE id="tblAssPedido" cellspacing="1" cellpadding="1" width="100%" border="0" class="sem-borda">
			<TR>
				<TD><FONT face="Tahoma" size="1"><%
				if request.QueryString("parte") = "pedido" then 'impress�o
					response.Write ass
				else
					response.Write left(ass, 120)
				end if
				%></FONT></TD>
			</TR>
		</TABLE>		

				</P>
			</FONT>


</div>

<%  TB.Close %>
<%  BD.Close %>
<% Set TB = Nothing %>
<% Set BD = Nothing %>