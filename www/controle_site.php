<? require_once("conf.php");

header("Content-type: text/html; charset=UTF-8");

$controleAcesso->addWhiteListReferer(array(
    "gestao-de-pessoas/trabalhe-conosco/",
    "contatos/",
	"contato/",
    "index.php",
    "seja-nosso-cliente/"
));
$controleAcesso->autenticarReferer();

$acao = request('acao');

//print_r($_POST);
if($acao =='salvarContato'){
    $idioma = request("idioma");
    $nome = request("nome");
    $email = request("email");	
    $cidade = request("cidade");
    $uf = request("uf");
    $ddd = Util::retirarChars(request("ddd"));
    $telefone = request("telefone");
    $assunto = request("assunto");
    $mensagem = request("mensagem");

    $val["idioma"] = $idioma; $msg['idioma'] = "idioma"; $vld['idioma'] = 1;$maxlen['idioma'] = 100;$minLen['idioma'] = 1;    
    $val["nome"] = $nome; $msg['nome'] = "Nome"; $vld['nome'] = 1;$maxlen['nome'] = 300;$minLen['nome'] = 1;
    $val["email"] = $email; $msg['email'] = "E-mail"; $vld['email'] = 2;$maxlen['email'] = 300;$minLen['email'] = 1;	
    $val["cidade"] = $cidade; $msg['cidade'] = "Cidade"; $vld['cidade'] = 1;$maxlen['cidade'] = 300;$minLen['cidade'] = 1;
    $val["uf"] = $uf; $msg['uf'] = "UF"; $vld['uf'] = 1;$maxlen['uf'] = 2;$minLen['uf'] = 1;
    $val["ddd"] = $ddd; $msg['ddd'] = "DDD"; $vld['ddd'] = 3;$maxlen['ddd'] = 5;$minLen['ddd'] = 1;
    $val["telefone"] = Util::retirarChars($telefone); $msg['telefone'] = "Telefone"; $vld['telefone'] = 3;$maxlen['telefone'] = 20;$minLen['telefone'] = 1;
    $val["assunto"] = $assunto; $msg['assunto'] = "Assunto"; $vld['assunto'] = 1;$maxlen['assunto'] = 300;
    $val["mensagem"] = $mensagem; $msg['mensagem'] = "Mensagem"; $vld['assunto'] = 1;$maxlen['mensagem'] = 2000;$minLen['mensagem'] = 1;

    $res = Util::validaFormulario($vld,$val,$msg,$maxlen,$minLen);
    if($res == 1){
        $cad = new Contato();
        $cad->setIdioma($idioma);
        $cad->setNome($nome);
        $cad->setEmail($email);		
        $cad->setCidade($cidade);
        $cad->setUf($uf);
        $telefone = $ddd."-".$telefone;
        $cad->setTelefone($telefone);		
        $cad->setAssunto($assunto);
        $cad->setMensagem($mensagem);
        $cad->setDataInclusao(date("Y-m-d H:i:s"));
        $cad->setStatus(Contato::NAOAVALIADO);
        if($cad->salvar()){
            print "sucesso";
            //redirect('fale-conosco.php?msg=sucesso');
        }
    }else{
        Util::showErroValidacao($res,'contatos.php');
    }
}


if($acao =='salvarCadGestaoPessoa'){
    $idioma = request("idioma");
    $nome = request("nome");
    $endereco = request("endereco");
    $num = Util::retirarChars(request("num"));
    $comp = request("comp");
    $cidade = request("cidade");
    $estado = request("estado");
    $ddd = Util::retirarChars(request("ddd"));
    $telefone = request("telefone");
    $email = request("email");
    $area = request("area");
    $outros = request("outros");
    $formacaoAcad = request("formacaoAcad");
    $curso = request("curso");
    $cursoAtual = request("cursoAtual");
    $idiomas = request("idiomas");
    $dadosProfss = request("dadosProfiss");
    
    $val["idioma"] = $idioma; $msg['idioma'] = "idioma"; $vld['idioma'] = 1;$maxlen['idioma'] = 100;$minLen['idioma'] = 1;
    $val["nome"] = $nome; $msg['nome'] = "Nome"; $vld['nome'] = 1;$maxlen['nome'] = 255;$minLen['nome'] = 1;	
    $val["endereco"] = $endereco; $msg['endereco'] = "Endereco"; $vld['endereco'] = 1;$maxlen['endereco'] = 255;$minLen['endereco'] = 1;
    $val["num"] = $num; $msg['num'] = "Número"; $vld['num'] = 3;$maxlen['num'] = 10;$minLen['num'] = 1;
    if($comp){$val["comp"] = $comp; $msg['comp'] = "Complemento"; $vld['comp'] = 1;$maxlen['comp'] = 255;$minLen['comp'] = 1;  }	
    $val["cidade"] = $cidade; $msg['cidade'] = "Cidade"; $vld['cidade'] = 1;$maxlen['cidade'] = 255;$minLen['cidade'] = 1;
    $val["estado"] = $estado; $msg['estado'] = "Estado"; $vld['estado'] = 1;$maxlen['estado'] = 100;$minLen['estado'] = 1;
    $val["ddd"] = $ddd; $msg['ddd'] = "DDD"; $vld['ddd'] = 3;$maxlen['ddd'] = 2;$minLen['ddd'] = 1;
    $val["telefone"] = Util::retirarChars($telefone); $msg['telefone'] = "Telefone"; $vld['telefone'] = 3;$maxlen['telefone'] = 15;$minLen['telefone'] = 1;
    $val["email"] = $email; $msg['email'] = "E-mail"; $vld['email'] = 2;$maxlen['email'] = 255;$minLen['email'] = 1;	
    $val["area"] = $area; $msg['area'] = "Área de Interesse"; $vld['area'] = 1;$maxlen['area'] = 255;$minLen['area'] = 1;
    if($outros){$val["outros"] = $outros; $msg['outros'] = "Outra Área"; $vld['outros'] = 1;$maxlen['outros'] = 255;$minLen['outros'] = 1; }	
    $val["formacaoAcad"] = $formacaoAcad; $msg['formacaoAcad'] = "Formação Acadêmica"; $vld['formacaoAcad'] = 1;$maxlen['formacaoAcad'] = 255;$minLen['formacaoAcad'] = 1;
    if($curso){$val["curso"] = $curso; $msg['curso'] = "Curso"; $vld['curso'] = 1;$maxlen['curso'] = 255;$minLen['curso'] = 1; }	
    if($cursoAtual){$val["cursoAtual"] = $cursoAtual; $msg['cursoAtual'] = "Curso de Atualzação"; $vld['cursoAtual'] = 1;$maxlen['cursoAtual'] = 255;$minLen['cursoAtual'] = 1; }	
    if($idiomas){$val["idiomas"] = $idiomas; $msg['idiomas'] = "Idiomas"; $vld['idiomas'] = 1;$maxlen['idiomas'] = 255;$minLen['idiomas'] = 1; }	
    if($dadosProfss){$val["dadosProfss"] = $dadosProfss; $msg['dadosProfss'] = "Dados Profissionais"; $vld['dadosProfss'] = 1;$maxlen['dadosProfss'] = 10000;$minLen['dadosProfss'] = 1; }	

    $res = Util::validaFormulario($vld,$val,$msg,$maxlen,$minLen);
    if($res == 1){
        $cad = new CadGestaoPessoa();
        $cad->setIdioma($idioma);
        $cad->setNome($nome);		
        $cad->setEndereco($endereco);
        $cad->setNum($num);
        $cad->setComp($comp);
        $cad->setCidade($cidade);
        $cad->setEstado($estado);		
        $telefone = $ddd."-".$telefone;
        $cad->setTelefone($telefone);		
        $cad->setEmail($email);	
        $cad->setArea($area);	
        $cad->setOutros($outros);	
        $cad->setFormacaoAcad($formacaoAcad);	
        $cad->setCurso($curso);	
        $cad->setCursoAtual($cursoAtual);	
        $cad->setIdiomas($idiomas);			
        $cad->setDadosProfiss($dadosProfss);        
        $cad->setDataInclusao(date("Y-m-d H:i:s"));
        $cad->setStatus(CadGestaoPessoa::NAOAVALIADO);
        if($cad->salvar()){
            print "sucesso";
            //redirect('fale-conosco.php?msg=sucesso');
        }
    }else{
        Util::showErroValidacao($res,'contatos.php');
    }
}

if($acao =='salvarNews'){
    $nome = request("nome");
    $email = request("email");
    $idioma = getIdioma();
    
    $val["idioma"] = $idioma; $msg['idioma'] = "idioma"; $vld['idioma'] = 1;$maxlen['idioma'] = 100;$minLen['idioma'] = 1;
    $val["nome"] = $nome; $msg['nome'] = "Nome"; $vld['nome'] = 1;$maxlen['nome'] = 255;$minLen['nome'] = 1;
    $val["email"] = $email; $msg['email'] = "E-mail"; $vld['email'] = 2;$maxlen['email'] = 255;$minLen['email'] = 1;    
	
    $res = Util::validaFormulario($vld,$val,$msg,$maxlen,$minLen);
    if($res == 1){
        $cad = new Newsletter();
        $cad->setIdioma($idioma);
        $cad->setNome($nome);	
        $cad->setEmail($email);     
        $cad->setDataInclusao(date("Y-m-d H:i:s"));
        $cad->setStatus(ATIVO);
        if($cad->salvar()){
            print "sucesso";
        }
    }else{
        Util::showErroValidacao($res,'contatos.php');
    }
}

if($acao =='salvarCliente'){
    $nome = request("nome");
    $email = request("email");
    $idioma = getIdioma();
    $mensagem = request("mensagem");
    $tipoCliente = secureRequest("tipoCliente");
    
    $val["idioma"] = $idioma; $msg['idioma'] = "idioma"; $vld['idioma'] = 1;$maxlen['idioma'] = 100;$minLen['idioma'] = 1;
    $val["nome"] = $nome; $msg['nome'] = "Nome"; $vld['nome'] = 1;$maxlen['nome'] = 255;$minLen['nome'] = 1;
    $val["email"] = $email; $msg['email'] = "E-mail"; $vld['email'] = 2;$maxlen['email'] = 255;$minLen['email'] = 1;    
    $val["mensagem"] = $mensagem; $msg['mensagem'] = "Mensagem"; $vld['mensagem'] = 1;$maxlen['mensagem'] = 1000;$minLen['mensagem'] = 1;
    $val["tipoCliente"] = $tipoCliente; $msg['tipoCliente'] = "Tipo Cliente"; $vld['tipoCliente'] = 1;$maxlen['tipoCliente'] = 10;$minLen['tipoCliente'] = 1;
	
    $res = Util::validaFormulario($vld,$val,$msg,$maxlen,$minLen);
    if($res == 1){
        $cad = new Cliente();
        $cad->setIdioma($idioma);
        $cad->setNome($nome);	
        $cad->setEmail($email);  
        $cad->setIdTipoCliente($tipoCliente);
        $cad->setMensagem($mensagem);  
        $cad->setDataInclusao(date("Y-m-d H:i:s"));
        $cad->setStatus(ATIVO);
        if($cad->salvar()){
            print "sucesso";
        }
    }else{
        Util::showErroValidacao($res,'contatos.php');
    }
}




?>