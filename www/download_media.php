<? require_once("conf.php");

/*function returnMIMEType($filename){
	preg_match("|\.([a-z0-9]{2,4})$|i", $filename, $fileSuffix);
	switch(strtolower($fileSuffix[1])){
		case "pdf" :
			return "application/pdf";
		default :
			if(function_exists("mime_content_type")){
				$fileSuffix = mime_content_type($filename);
			}
			return "unknown/" . trim($fileSuffix[0], ".");
	}
}*/
ini_set('display_errors', '0');
$idM = secureRequest("idM");
if($idM){
	$obj = Media::ler($idM);
	if($obj){
        switch($obj->getTipo()){  
            case Media::DOCS : 	$contentType="application/pdf"; break;
        }
           
            $arquivo = $_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ.$obj->getDir()."/".$obj->getUrl();			
            if(isset($arquivo) && file_exists($arquivo)){

			if($obj->getTipo()){
				$bufferSize = 512 * 1024; // 512Kb per second
				ob_end_clean();
				ignore_user_abort(true);
				set_time_limit(0);

				$filename = realpath($arquivo);
				if (file_exists($filename)) {
					header('Content-Type: application/octet-stream');
					header('Content-Disposition: attachment; filename=' . basename($filename));
					header('Content-Length: ' . filesize($filename));
					$resource = fopen($filename, 'rb');
					while (!feof($resource)) {
						echo fread($resource, $bufferSize);
						flush();
						sleep(1);
					}
					fclose($resource);
				}
				else {
					header('Location: /');
				}
			}else{
				header("Content-Type: ".$contentType);
				header("Content-Length: ".filesize($arquivo)); 
				header("Content-Disposition: attachment; filename=".basename($arquivo));
				readfile($arquivo);
			}
			
			
			
			exit;	  
		}	
	}
}
?>