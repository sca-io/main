<? require_once ('conf.php');
$idSecao = GESTAO_DE_PESSOAS;
$idioma = getIdioma();
$nome_secao = $has_lista_icones = $nome_secao_atual = "";
if($settSecoes[$idSecao]){
    $nome_secao = getNomeSecao($idSecao);	
    $metaTitle = $nome_secao;
}


$where = "idioma = '$idioma' AND status = '".VersaoConteudo::PUBLICADO."'";
$conteudo = null;

if($idSecao){     
    $listaConts = VersaoConteudo::listar($idSecao,"","id,titulo,texto,keywords,descricao,id_secao,url_embed,img_destaque",$where,"data_publicacao DESC","1");
    //print VersaoConteudo::getLogSql();
    if($listaConts){
        $conteudo = $listaConts[0];    
    }   
    $nome_secao_atual = $nome_secao;
}

if($conteudo){
    //$idSecao = $conteudo->getIdSecao();
    $titulo = $conteudo->getTitulo();
    $descricao = $conteudo->getDescricao();
    $keywords = $conteudo->getKeywords();
    $texto = $conteudo->getTexto();
}else{
    print "<script>document.location = '".DIRETORIO_RAIZ."gestao-de-pessoas/trabalhe-conosco/'</script>";
}

?><? include "includes/head.php"?>
<body>
<div id="sca">
    <div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <a href="javascript:;"><strong><?=$nome_secao?></strong></a>
            </div>
        </div>
        <div class="empresa gestao-de-pessoas">
            <div class="esquerda">
            	<div class="txt-box">
                    <div>
                        <h2 class="tit-empresa"><img src="<?=DIRETORIO_RAIZ?>css/img/tit-trabalhe-conosco<?=getTextoByLang("","-eng")?>.png" alt="<?=$nome_secao_atual?>" /> </h2>
                        <?=response_html($texto)?> 
                    </div>
                </div>
                <a href="<?=DIRETORIO_RAIZ?>gestao-de-pessoas/trabalhe-conosco/" class="bt-cadastre-se-aqui" title="<?=getTextoByLang("Cadastre-se aqui","Sign up here")?>"><?=getTextoByLang("Cadastre-se aqui","Sign up here")?> +</a>
            </div>
                        
        </div>
      </div>
    </div>
    <? include "includes/rodape.php"?>
</div>
</body>
</html>