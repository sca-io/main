<? require_once ('conf.php');
$idSecao = NOTICIAS;
$idioma = getIdioma();
$url_secao = DIRETORIO_RAIZ."noticias/";
$nome_secao = getNomeSecao($idSecao);
$metaTitle = $nome_secao;

$url_self = $url_secao;
?>
<? include "includes/head.php"?>
<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <a href="javascript:;"><strong><?=$nome_secao?></strong></a>
            </div>
        </div>
        <div class="busca noticias-listagem">
        	<h3><img src="<?=DIRETORIO_RAIZ?>css/img/tit-noticias<?=getTextoByLang("","-eng")?>.png" height="35" alt="<?=$nome_secao?>" /></h3>
            <div class="esquerda">
                <?
                $where = "status = ".VersaoConteudo::PUBLICADO." AND idioma = '$idioma'";
                $ncadastro =  VersaoConteudo::countListar($idSecao,"",$where);

                $pagina = isset($_REQUEST['pagina'])?$_REQUEST['pagina']:0;
                $pagina = $pagina > 0?(int) $pagina-1:$pagina;
                $total_reg_pag = 10;
                $init_reg_pag = ($pagina * $total_reg_pag);	 
                $aux_pagina = $pagina + 1;
                $total_paginas = $ncadastro/$total_reg_pag;				
                $noticias = VersaoConteudo::listar($idSecao,"","id,titulo,texto,data_publicacao,img_destaque,url_amigavel",$where,"data_publicacao DESC","$init_reg_pag, $total_reg_pag"); 
                ?>
                <? if($noticias):?>
                    <? foreach($noticias as $not):?>
                    <div class="bloco">
                            <blockquote>
                                    <? $texto = Util::truncarTexto(strip_tags($not->getTexto()),200);
                                    $linkNot = $url_secao.($not->getUrlAmigavel())."/";
                                    ?>
                                    
                                    <? /*<img src="img/ft-acoes-sustentaveis1.jpg" width="120" height="93" /><!--esta imagem não existe coloquei apenas por simulação ou seja não é para programar--> */ ?>
                                    <h4><?=Util::dataDoBD(response_html($not->getDataPublicacao()))?></h4>
                                    <h2><?=response_html($not->getTitulo())?></h2>

                                    <p><?=response_html($texto);?></p>	
									<p><a href="<?=response_attr($linkNot)?>" class="seta" title="<?=getTextoByLang("Leia mais","Read more");?> +"><?=getTextoByLang("Leia mais","Read more");?></a></p>
                            </blockquote>
                    </div>
                    <? endforeach;?>
                <? endif;?>
                
                <? include "includes/paginacao.php"?>
                <a href="javascript:history.back()" class="bt-voltar" title="<?=getTextoByLang("Voltar","Return")?>"> &lt; <?=getTextoByLang("Voltar","Return");?></a>              
            </div>

            <div class="direita">
				<? include "includes/direita-noticias.php"?>
            </div>
                  
        </div>
        
      </div>
    </div>
    <? include "includes/rodape.php"?>
</div>
</body>
</html>