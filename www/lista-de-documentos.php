<? require_once ('conf.php');
$idioma = getIdioma();
$id = secureRequest('id');
$idSecao = request('idSecao');
$idSubSecao = request('idSubSecao');

$where = "idioma = '$idioma' AND status = '".VersaoConteudo::PUBLICADO."'";
$conteudo = null;
if($id){
    $where .= " AND id = $id";
}

if($idSecao|| $idSubSecao){     
    $idSecaoCont = $idSubSecao?$idSubSecao:$idSecao;
    $listaConts = VersaoConteudo::listar($idSecaoCont,"","id,titulo,texto,keywords,descricao,id_secao,url_embed,img_destaque",$where,"data_publicacao DESC","1");
    //print VersaoConteudo::getLogSql();
    if($listaConts){
        $conteudo = $listaConts[0];    
    }    
}
$id = 0;
if($conteudo){
    $id = $conteudo->getId();
    $titulo = $conteudo->getTitulo();
    $descricao = $conteudo->getDescricao();
    $keywords = $conteudo->getKeywords();
    $texto = $conteudo->getTexto();
}

$nome_secao = "";
$nome_secao_atual = "";
$nome_img_tit = "";
$brad_crumb = "";
$url_secao = "";
$classe_principal = "";
$url_self = "";
if($idSecao){    
    switch($idSecao){
        case ESTATISTICAS:
            $classe_principal = "empresa etanol legislacoes";
            $url_secao = DIRETORIO_RAIZ."estatisticas/";
        break;  
    }   
    if(isset($settSecoes[$idSecao])) $nome_secao = getNomeSecao($idSecao);
    $brad_crumb = $nome_secao;
    if(!$idSubSecao) $brad_crumb = "<strong>".$brad_crumb."</strong>";	
    $brad_crumb = '<a href="'.$url_secao.'">'.$brad_crumb.'</a>';
    $nome_img_tit = getNomeSecao($idSecao,"","",LNG_PT);
    $metaTitle = $nome_secao;
    $url_self = $url_secao;
    $nome_secao_atual = $nome_secao;
}

//$subSecao = Secao::ler($idSubSecao,"nome");
if($idSubSecao){
	switch($idSubSecao){
		case ESPECIFICACOES:
			$url_sub_secao = $url_secao."especificacoes/";
		break;
	}
    $nome_sub_secao = getNomeSecao($idSecao,$idSubSecao);
    $brad_crumb .= '&nbsp;&nbsp;/&nbsp;<a href="javascript:;"><strong>'.$nome_sub_secao.'</strong></a>'; 
    $nome_img_tit = getNomeSecao($idSecao,$idSubSecao,"",LNG_PT);
    $url_self = $url_sub_secao;
    $nome_secao_atual = $nome_sub_secao;
}

?><? include "includes/head.php"?>
<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <?=$brad_crumb?>
            </div>
        </div>
        <div class="empresa etanol legislacoes">
            <div class="esquerda">
                <div class="resultados result-estatistica">
                	<h2 class="tit-empresa"><img src="<?=DIRETORIO_RAIZ?>css/img/tit-<?=setUrlAmigavel($nome_img_tit).getTextoByLang("","-eng")?>.png" alt="<?=$nome_secao_atual?>" /> </h2>
                        <? if($conteudo):?>  
                                <h3><?=response_html($titulo)?></h3>
                                <div class="introducao">
                                    <?=response_html($texto)?>
                                </div>

                        <? endif;?>
                        <?
                        $where = "status = ".ATIVO." AND tipo = '".Media::DOCS."' AND lang = '$idioma'";                                                
                        $ncadastro =  Media::countListar($id,$idSecao,$where);
                        $pagina = isset($_REQUEST['pagina'])?$_REQUEST['pagina']:0;
                        $pagina = $pagina > 0?(int) $pagina-1:$pagina;
                        $total_reg_pag = 10;
                        $init_reg_pag = ($pagina * $total_reg_pag);	 
                        $aux_pagina = $pagina + 1;
                        $total_paginas = $ncadastro/$total_reg_pag;
                        $listaArqs = Media::listar($id,"","",$where,"data_insercao DESC","$init_reg_pag, $total_reg_pag");
                        ?>
                        <? if($listaArqs):?>
                            <? foreach($listaArqs as $l):?>
                            <div>
                                <blockquote>
                                    <p><?=getIconeArquivo($l->getUrl())?></p>
                                    <h5>
                                        <?=response_html($l->getNome())?>
                                        <a href="javascript:downloadDoc('<?=secureResponse($l->getId())?>')" class="bt-baixar" title="<?=getTextoByLang("Download","Download")?>"><?=getTextoByLang("Download","Download")?></a>
                                        <a href="<?=DIRETORIO_RAIZ.$l->getDir().$l->getUrl()?>" rel="external" class="bt-ver" title="<?=getTextoByLang("Visualizar","View")?>"><?=getTextoByLang("Visualizar","View")?></a></h5>
                                    <p><strong><?=getTextoByLang("Data de publicação: ","Date of publication: ");?></strong><?=response_html(Util::dataDoBD($l->getDataInsercao()))?></p>
                                    <p><strong><?=getTextoByLang("Referente: ","Relative: ");?></strong><?=response_html($l->getDescricao())?></p>							
                                </blockquote>
                            </div>
                            <? endforeach;?>
                        <? endif;?>
                </div>
		<? include "includes/paginacao.php"?>		

                <a href="javascript:history.back()" class="bt-voltar" title="<?=getTextoByLang("Voltar","Return")?>"> &lt; <?=getTextoByLang("Voltar","Return")?></a> 
            </div>                        
        </div>
      </div>
    </div>
    <? include "includes/rodape.php"?>
</div>
</body>
</html>