<? $ultimasNoticias = VersaoConteudo::listar($idSecao,"","id,texto,url_amigavel,data_publicacao","status = ".VersaoConteudo::PUBLICADO." AND idioma = '$idioma'","data_publicacao DESC","5"); ?>
<? if($ultimasNoticias):?>
<div>
    <h2<?=getTextoByLang(""," class=\"eng\"");?>><?=getTextoByLang("Últimas notícias","Latest news");?></h2>
    <? foreach($ultimasNoticias as $not):?>
    <? $linkNot = $url_secao.$not->getUrlAmigavel()."/";
    $texto = Util::truncarTexto(strip_tags($not->getTexto()),200);
    ?>	
    <blockquote>
            <h3><?=Util::dataDoBD(response_html($not->getDataPublicacao()),"d-m-Y")?></h3>
            <p><a href="<?=response_attr($linkNot)?>"><?=response_html($texto)?></a></p>
    </blockquote>
    <? endforeach;?>
</div>
<? endif;?>