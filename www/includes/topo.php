<? $idSecao = isset($idSecao)?$idSecao:0;
$busca = isset($busca)?$busca:"";
?>
<div class="topo">
  <h1><a href="<?=DIRETORIO_RAIZ?>index.php" title="<?=getTextoByLang("SCA - ETANOL DO BRASIL","SCA - BRAZIL ETHANOL")?>" class="logo<?=getTextoByLang(""," logo-eng")?>"><?=getTextoByLang("SCA - ETANOL DO BRASIL","SCA - BRAZIL ETHANOL")?></a></h1>
  <div class="cont<?=getTextoByLang(""," cont-eng")?>">
      <a href="<?=DIRETORIO_RAIZ?>contato/" title="<?=getTextoByLang("Contato","Contact us")?>" class="contato"><?=getTextoByLang("Contato","Contact us")?></a>                
      <a href="<?=DIRETORIO_RAIZ?>?idioma=<?=LNG_EN?>" title="<?=getTextoByLang("Inglês","English")?>"><?=getTextoByLang("Inglês","English")?></a>
      <a href="<?=DIRETORIO_RAIZ?>?idioma=<?=LNG_PT?>" title="<?=getTextoByLang("Português","Portuguese")?>"><?=getTextoByLang("Português","Portuguese")?></a>
  </div>
  <form id="form_busca" action="<?=DIRETORIO_RAIZ?>resultado-busca/">
      <div class="busca-topo<?=getTextoByLang(""," busca-topo-eng")?>"><a href="javascript:;" onclick="buscar()" title="OK">OK</a><input type="text" name="busca" value="<?=response_attr($busca)?>" /></div>
  </form>
  <ul class="menu<?=getTextoByLang(""," menu-eng")?>">
  	<li class="empresa<? if($idSecao == EMPRESA){ print " active";}?>">
    	<a href="<?=DIRETORIO_RAIZ?>empresa/" title="<?=getNomeSecao(EMPRESA)?>"><?=getNomeSecao(EMPRESA)?></a>
        <ul>
            <li class="op1"><a href="<?=DIRETORIO_RAIZ?>empresa/trajetoria/" title="<?=getNomeSecao(EMPRESA,TRAJETORIA)?>"><?=getNomeSecao(EMPRESA,TRAJETORIA)?></a></li>
            <li class="op2"><a href="<?=DIRETORIO_RAIZ?>empresa/acoes-sustentaveis/" title="<?=getNomeSecao(EMPRESA,ACOES_SUSTENTAVEIS)?>"><?=getNomeSecao(EMPRESA,ACOES_SUSTENTAVEIS)?></a></li>
            <li class="op3"><a href="<?=DIRETORIO_RAIZ?>empresa/mercado-de-atuacao/" title="<?=getNomeSecao(EMPRESA,MERCADO_DE_ATUACAO)?>"><?=getNomeSecao(EMPRESA,MERCADO_DE_ATUACAO)?></a></li>
        </ul>
    </li>
    <li class="servicos<? if($idSecao == SERVICOS){ print " active";}?>">
    	<a href="<?=DIRETORIO_RAIZ?>servicos/" title="<?=getNomeSecao(SERVICOS)?>"><?=getNomeSecao(SERVICOS)?></a>
        <ul>
            <li class="op1"><a href="<?=DIRETORIO_RAIZ?>servicos/comercializacao/" title="<?=getNomeSecao(SERVICOS,COMERCIALIZACAO)?>"><?=getNomeSecao(SERVICOS,COMERCIALIZACAO)?></a></li>
            <li class="op2"><a href="<?=DIRETORIO_RAIZ?>servicos/sca-trading/" title="<?=getNomeSecao(SERVICOS,SCA_TRADING)?>"><?=getNomeSecao(SERVICOS,SCA_TRADING)?></a></li>
            <li class="op3"><a href="<?=DIRETORIO_RAIZ?>servicos/inteligencia-de-mercado/" title="<?=getNomeSecao(SERVICOS,INTELIGENCIA_DE_MERCADO)?>"><?=getNomeSecao(SERVICOS,INTELIGENCIA_DE_MERCADO)?></a></li>
        </ul>
    </li>
    <li class="etanol<? if($idSecao == ETANOL){ print " active";}?>">
    	<a href="<?=DIRETORIO_RAIZ?>etanol/" title="<?=getNomeSecao(ETANOL)?>"><?=getNomeSecao(ETANOL)?></a>
        <ul>
            <li class="op1">
            	<a href="<?=DIRETORIO_RAIZ?>etanol/especificacoes/" title="<?=getNomeSecao(ETANOL,ESPECIFICACOES)?>"><?=getNomeSecao(ETANOL,ESPECIFICACOES)?></a>
                <ul>
                    <li class="op1-1"><a href="<?=DIRETORIO_RAIZ?>etanol/especificacoes/mercado-interno/" title="<?=getNomeSecao(ETANOL,ESPECIFICACOES,MERCADO_INTERNO)?>"><?=getNomeSecao(ETANOL,ESPECIFICACOES,MERCADO_INTERNO)?></a></li>
                    <li class="op1-2"><a href="<?=DIRETORIO_RAIZ?>etanol/especificacoes/mercado-externo/" title="<?=getNomeSecao(ETANOL,ESPECIFICACOES,MERCADO_EXTERNO)?>"><?=getNomeSecao(ETANOL,ESPECIFICACOES,MERCADO_EXTERNO)?></a></li>
                </ul>
            </li>
            <li class="op2">
            	<a href="<?=DIRETORIO_RAIZ?>etanol/certificacoes/" title="<?=getNomeSecao(ETANOL,CERTIFICACOES)?>"><?=getNomeSecao(ETANOL,CERTIFICACOES)?></a>
            </li>
            <li class="op3">
            	<a href="<?=DIRETORIO_RAIZ?>etanol/legislacoes/" title="<?=getNomeSecao(ETANOL,LEGISLACOES)?>"><?=getNomeSecao(ETANOL,LEGISLACOES)?></a>
            </li>
        </ul>
    </li>
    <li class="unidade-produtora<? if($idSecao == UNIDADES_PRODUTORAS){ print " active";}?>"><a href="<?=DIRETORIO_RAIZ?>unidades-produtoras/" title="<?=getNomeSecao(UNIDADES_PRODUTORAS)?>"><?=getNomeSecao(UNIDADES_PRODUTORAS)?></a></li>
    <li class="estatisticas<? if($idSecao == ESTATISTICAS){ print " active";}?>"><a href="<?=DIRETORIO_RAIZ?>estatisticas/" title="<?=getNomeSecao(ESTATISTICAS)?>"><?=getNomeSecao(ESTATISTICAS)?></a></li>
    <li class="gestao-de-pessoas<? if($idSecao == GESTAO_DE_PESSOAS){ print " active";}?>"><a href="<?=DIRETORIO_RAIZ?>gestao-de-pessoas/" title="<?=getNomeSecao(GESTAO_DE_PESSOAS)?>"><?=getNomeSecao(GESTAO_DE_PESSOAS)?></a></li>
    <li class="seja-nosso-cliente<? if($idSecao == SEJA_NOSSO_CLIENTE){ print " active";}?>"><a title="<?=getNomeSecao(SEJA_NOSSO_CLIENTE)?>" href="<?=DIRETORIO_RAIZ?>seja-nosso-cliente/"><?=getNomeSecao(SEJA_NOSSO_CLIENTE)?></a></li>
    <li class="area-restrita<? if($idSecao == AREA_RESTRITA){ print " active";}?>"><a href="http://www.scalcool.com.br/sisweb/" onclick="return OpenOffSite(this);" title="<?=getNomeSecao(AREA_RESTRITA)?>"><?=getNomeSecao(AREA_RESTRITA)?></a></li>
  </ul>
</div>