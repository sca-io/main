<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? 
$idSecao = isset($idSecao)?$idSecao:0;
$idSubSecao = isset($idSubSecao)?$idSubSecao:0;
$idioma = isset($idioma)?$idioma:getIdioma();

$titulo = isset($titulo)?$titulo:"";
$metaTitle = isset($metaTitle)?$metaTitle:$titulo;
if(!$metaTitle && isset($nome_sub_secao)) $metaTitle = $nome_sub_secao;

if(isset($metaTitle)) $title =  getTextoByLang("SCA - ETANOL DO BRASIL","SCA - BRAZIL ETHANOL");  
else $title = getTextoByLang("SCA - ETANOL DO BRASIL","SCA - BRAZIL ETHANOL"); 

if(isset($title)) print '<title>'.response_attr($title).'</title>
<meta name="title" content="'.response_attr($title).'" />';

if(isset($descricao)){ 
    $txtDescp = strip_tags($descricao);   
    $txtDescp = str_replace("\t","",$txtDescp);
    $txtDescp = str_replace("\n","",$txtDescp);
    $txtDescp = str_replace("\r","",$txtDescp);
    print '<meta name="description" content="'.response_attr($txtDescp).'" />'."\n";
    
}
if(isset($keywords)) print '<meta name="keywords" content="'.response_attr($keywords).'" />';
if(isset($autor)) print '<meta name="author" content="'.response_attr(strip_tags($autor)).'" />';

/*** Fluxo de imagem para o facebook ***/
if(isset($imgDestaque)){
    $image_src = DIRETORIO_UPLOAD_IMG."/".$imgDestaque;
    if(!$image_src && $texto){
        if(eregi("<img",$texto)){			
            $textoBase = strip_tags($texto,"<img>");			
            preg_match('/src=\"([^\"]*)?+\"?/msi',$textoBase, $matches);
            $image_src = $matches[1];
        }              
    }
}
if(isset($image_src)) print '<link rel="image_src" href="'.response_attr($image_src).'" />';
/***************************************/
?>


<link rel="STYLESHEET" type="text/css" href="<?=DIRETORIO_RAIZ?>css/style.css" />
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="<?=DIRETORIO_RAIZ?>css/style-ie.css"/><![endif]-->
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="<?=DIRETORIO_RAIZ?>css/style-ie8.css"/><![endif]-->
<link href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:700' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" media="all" href="<?=DIRETORIO_RAIZ?>css/jScrollPane.css" />
<script type="text/javascript">
if (navigator.userAgent.toLowerCase().indexOf('chrome')!=-1){
    document.write('<link rel="stylesheet" type="text/css" href="<?=DIRETORIO_RAIZ?>css/style-chrome.css"/>');                    
}
</script>

<? /*
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script> 
<script>//if(!window.jQuery) document.write('<script src="<?=DIRETORIO_RAIZ?>js/jquery-1.7.1.min.js"><\/script>')</script>
*/?>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="<?=DIRETORIO_RAIZ?>js/Popup.js"></script>
<script type="text/javascript" src="<?=DIRETORIO_RAIZ?>js/scripts.js"></script>
<script type="text/javascript">
	function OpenOffSite(a)
{
window.open(a.href, "OffSite", "directories=yes,location=yes,menubar=yes,resizable=yes,scrollbars=yes,toolbar=yes").focus();
return false;
}
</script>

<?
$scriptsAdicionais = array();
$scriptsAdicionais[UNIDADES_PRODUTORAS] = array("swfobject");
$scriptsAdicionais[TRAJETORIA] = array("swfobject");
$scriptsAdicionais[HOME] = array("Rotativo","validacao","jquery.mousewheel","jScrollPane");
$scriptsAdicionais[CONTATOS] = array("jquery.maskedinput","validacao");
$scriptsAdicionais[GESTAO_DE_PESSOAS] = array("jquery.maskedinput","validacao");
$scriptsAdicionais[SEJA_NOSSO_CLIENTE] = array("jquery.uniform","validacao"); 
$arrScripts = array();
if(isset($scriptsAdicionais[$idSecao])) $arrScripts = $scriptsAdicionais[$idSecao];
if(!$arrScripts && isset($scriptsAdicionais[$idSubSecao])) $arrScripts = $scriptsAdicionais[$idSubSecao];
?>
<? foreach($arrScripts as $script):?>
    <script type="text/javascript" src="<?=DIRETORIO_RAIZ?>js/<?=$script?>.js"></script>
<? endforeach;?>

    <script  type="text/javascript">
        var DIRETORIO_RAIZ = '<?=DIRETORIO_RAIZ?>';
        var IDIOMA = parseInt('<?=$idioma?>');
        $(window).ready(function(){
            $('a[rel=external]').attr('target', '_blank');
            $(".bolinha").each(function(){
                var txt=$(this).children("DIV").html();
                var lng=txt.length;
                $(this).children("DIV").css({"width":txt.length*7});                    
            });

            <? if($idSecao == HOME):?>
                webdoor = new Rotativo({conteudos:".fotos IMG",intervalo:5000,fade:true})
                webdoor.init();	
                rottDests = new Rotativo({conteudos:".destaques",intervalo:8000})
                rottDests.init();
            <? endif;?>
                
            <? if($idSecao == SEJA_NOSSO_CLIENTE):?>
                $("input, textarea, select, button").uniform();
            <? endif;?> 
                
            <? if($idSubSecao == TRAJETORIA):?>
            var mv = new SWFObject("<?=DIRETORIO_RAIZ?>swf/trajetoria.swf", "menutrajetoria", "670", "480", "8", "");
            mv.addParam("wmode", "transparent");
            mv.write("flash");
            <? endif;?>   

        });
        $.get("<?=DIRETORIO_RAIZ?>check-files/verificar-arquivos.php",function(){});
    </script>
</head>