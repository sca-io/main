<?
$subSecao2 = isset($subSecao2)?$subSecao2:"";
?>
<ul class="mn-all">
    <? if($idSecao == EMPRESA): ?>
    <!-- <li class="emp-eng op1"><a href="#" title="Trajetórias">Trajetórias </a></li> -->
    <li class="<?=getTextoByLang("","emp-eng ")?>op1<? if($idSubSecao == TRAJETORIA){print " active";}?>"><a href="<?=DIRETORIO_RAIZ?>empresa/trajetoria/" title="<?=getNomeSecao(EMPRESA,TRAJETORIA)?>"><?=getNomeSecao(EMPRESA,TRAJETORIA)?> </a></li>
    <li class="<?=getTextoByLang("","emp-eng ")?>op2<? if($idSubSecao == ACOES_SUSTENTAVEIS){print " active";}?>"><a href="<?=DIRETORIO_RAIZ?>empresa/acoes-sustentaveis/" title="<?=getNomeSecao(EMPRESA,ACOES_SUSTENTAVEIS)?>"><?=getNomeSecao(EMPRESA,ACOES_SUSTENTAVEIS)?></a></li>
    <li class="<?=getTextoByLang("","emp-eng ")?>op3<? if($idSubSecao == MERCADO_DE_ATUACAO){print " active";}?>"><a href="<?=DIRETORIO_RAIZ?>empresa/mercado-de-atuacao/" title="<?=getNomeSecao(EMPRESA,MERCADO_DE_ATUACAO)?>"><?=getNomeSecao(EMPRESA,MERCADO_DE_ATUACAO)?></a></li>    
    <? endif;?>

    <? if($idSecao == SERVICOS): ?>
    <li class="<?=getTextoByLang("","serv-eng ")?>op1<? if($idSubSecao == COMERCIALIZACAO){print " active";}?>"><a href="<?=DIRETORIO_RAIZ?>servicos/comercializacao/" title="<?=getNomeSecao(SERVICOS,COMERCIALIZACAO)?>"><?=getNomeSecao(SERVICOS,COMERCIALIZACAO)?></a></li>
    <li class="<?=getTextoByLang("","serv-eng ")?>op2<? if($idSubSecao == SCA_TRADING){print " active";}?>"><a href="<?=DIRETORIO_RAIZ?>servicos/sca-trading/" title="<?=getNomeSecao(SERVICOS,SCA_TRADING)?>"><?=getNomeSecao(SERVICOS,SCA_TRADING)?></a></li>
    <li class="<?=getTextoByLang("","serv-eng ")?>op3<? if($idSubSecao == INTELIGENCIA_DE_MERCADO){print " active";}?>"><a href="<?=DIRETORIO_RAIZ?>servicos/inteligencia-de-mercado/" title="<?=getNomeSecao(SERVICOS,INTELIGENCIA_DE_MERCADO)?>"><?=getNomeSecao(SERVICOS,INTELIGENCIA_DE_MERCADO)?></a></li> 
    <? endif;?>

    <? if($idSecao == ETANOL): ?>
    <li class="op1">
        <p><a class="<?=getTextoByLang("","eth-eng ")?><? if($idSubSecao == ESPECIFICACOES){print " active";}?>" href="<?=DIRETORIO_RAIZ?>etanol/especificacoes/" title="<?=getNomeSecao(ETANOL,ESPECIFICACOES)?>"><?=getNomeSecao(ETANOL,ESPECIFICACOES)?></a></p>
        <ul>
            <li class="<?=getTextoByLang("","eth-eng ")?> esp1<? if($subSecao2 == "mercado-interno"){print " active";}?>"><a href="<?=DIRETORIO_RAIZ?>etanol/especificacoes/mercado-interno/" title="<?=getNomeSecao(ETANOL,ESPECIFICACOES,MERCADO_INTERNO)?>"><?=getNomeSecao(ETANOL,ESPECIFICACOES,MERCADO_INTERNO)?></a></li>
            <li class="<?=getTextoByLang("","eth-eng ")?> esp2<? if($subSecao2 == "mercado-externo"){print " active";}?>"><a href="<?=DIRETORIO_RAIZ?>etanol/especificacoes/mercado-externo/" title="<?=getNomeSecao(ETANOL,ESPECIFICACOES,MERCADO_EXTERNO)?>"><?=getNomeSecao(ETANOL,ESPECIFICACOES,MERCADO_EXTERNO)?></a></li>
        </ul>
    </li>
    <li class="<?=getTextoByLang("","eth-eng ")?>op2<? if($idSubSecao == CERTIFICACOES){print " active";}?>"><a href="<?=DIRETORIO_RAIZ?>etanol/certificacoes/" title="<?=getNomeSecao(ETANOL,CERTIFICACOES)?>"><?=getNomeSecao(ETANOL,CERTIFICACOES)?></a></li>
    <li class="<?=getTextoByLang("","eth-eng ")?>op3<? if($idSubSecao == LEGISLACOES){print " active";}?>"><a href="<?=DIRETORIO_RAIZ?>etanol/legislacoes/" title="<?=getNomeSecao(ETANOL,LEGISLACOES)?>"><?=getNomeSecao(ETANOL,LEGISLACOES)?></a></li>
    <? endif;?>
</ul>