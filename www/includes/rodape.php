<div class="rodape">
    <div>
        <img src="<?=DIRETORIO_RAIZ?>css/img/bg-rodape.gif" width="123" height="90" alt="" />
        <ul>
            <li><a href="<?=DIRETORIO_RAIZ?>empresa/" title="<?=getNomeSecao(EMPRESA)?>"><?=getNomeSecao(EMPRESA)?></a><strong>&bull;</strong></li>
            <li><a href="<?=DIRETORIO_RAIZ?>servicos/" title="<?=getNomeSecao(SERVICOS)?>"><?=getNomeSecao(SERVICOS)?></a><strong>&bull;</strong></li>
            <li><a href="<?=DIRETORIO_RAIZ?>etanol/" title="<?=getNomeSecao(ETANOL)?>"><?=getNomeSecao(ETANOL)?></a><strong>&bull;</strong></li>
            <li><a href="<?=DIRETORIO_RAIZ?>unidades-produtoras/" title="<?=getNomeSecao(UNIDADES_PRODUTORAS)?>"><?=getNomeSecao(UNIDADES_PRODUTORAS)?></a><strong>&bull;</strong></li>
            <li><a href="<?=DIRETORIO_RAIZ?>estatisticas/" title="<?=getNomeSecao(ESTATISTICAS)?>"><?=getNomeSecao(ESTATISTICAS)?></a><strong>&bull;</strong></li>
            <li><a href="<?=DIRETORIO_RAIZ?>gestao-de-pessoas/" title="<?=getNomeSecao(GESTAO_DE_PESSOAS)?>"><?=getNomeSecao(GESTAO_DE_PESSOAS)?></a><strong>&bull;</strong></li>
            <li><a href="http://www.scalcool.com.br/sisweb/" onclick="return OpenOffSite(this);" title="<?=getNomeSecao(AREA_RESTRITA)?>"><?=getNomeSecao(AREA_RESTRITA)?></a></li>
        </ul>
        <address>
        	S&atilde;o Paulo Tel:  55 11 3709-4900
            Goi&aacute;s Tel:  55 62 3878-4900
        </address>
    </div>
</div>

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34367631-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
