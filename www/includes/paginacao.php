<?
$htmlPaginacao = "";
$variacao = 9;
$url_paginacao = isset($url_paginacao)?$url_paginacao:$url_self;
$paramPage = isset($paramPage)?$paramPage:false;

if(isset($total_paginas) && $total_paginas):
    if($total_paginas > 1):
        if($pagina >= -$variacao ){
            for($i = 0; $i<$total_paginas; $i++){
                $htmlItem = "";                
                if($i==$pagina){
                    $htmlItem .= "<a class='active'>".($i+1)."</a>";
                }else if($i != $pagina && $i <= ($pagina + $variacao) && $i >= ($pagina - $variacao)){
                    if($paramPage) $strParmPag = "&page=".($i+1);
                    else  $strParmPag = "page/".($i+1)."/"; //amigavel
                    $htmlItem .= "<a href=\"".$url_paginacao.$strParmPag."\">".($i+1)."</a>";								
                }
                if($i < $total_paginas - 1) $htmlItem .= "&nbsp;<strong>&bull;</strong>";
                $htmlPaginacao .= "<li>$htmlItem</li>";
            }
        }
?>

    <div class="paginacao">
        <ol>
            <?=$htmlPaginacao?>
        </ol>
    </div>
    <?  endif;?>
<?  endif;?>