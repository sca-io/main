<form id="formFiltro" action="">    
    <p class="hidden"><input type="hidden" id="urlSelf" value="<?=$url_self?>" /></p>
    <p>
        <label><?=getTextoByLang("Selecione o assunto para a consulta:","Select the topic for consultation:")?></label>
        <label>
            <select id="assunto" name="assunto" onchange="filtarAssunto()">
                <option value=""><?=getTextoByLang("Assunto","Subject")?></option>
                <? 
                $categoriasComArq = isset($categoriasComArq)?$categoriasComArq:array();
                $inCat = "";
                if(count($categoriasComArq) > 0):
                    $inCat = " AND id IN(".implode(",",$categoriasComArq).")";
                    $listaAs = Assunto::listar($idSecaoSel,"id,nome", "status = ".ATIVO." AND idioma = $idioma $inCat", "data DESC");
                ?> 
                    <? foreach($listaAs as $as):?>                
                    <?  $nomeAs = $as->getNome();
                        $selectedAs = "";  
                        if(setUrlAmigavel($nomeAs) == $url_amig){
                            $assunto = 	$as->getId();
                            $selectedAs = " selected=\"selected\""; 
                        }
                    ?>
                    <option value="<?=setUrlAmigavel($nomeAs)?>" <?=$selectedAs?>><?=$nomeAs?></option>
                    <? endforeach;?>
                <? endif;?>
            </select>
            <? /*<a href="javascript:filtarAssunto()" class="bt-consultar<?=getTextoByLang(""," bt-consultar-eng")?>" title="<?=getTextoByLang("Consultar","Consult")?>"><?=getTextoByLang("Consultar","Consult")?></a>
            <a href="javascript:limparFiltroAssunto()" class="bt-limpar<?=getTextoByLang(""," bt-limpar-eng")?>" title="<?=getTextoByLang("Limpar","Clear")?>"><?=getTextoByLang("Limpar","Clear")?></a> */?>
        </label>                            
    </p>
</form>
