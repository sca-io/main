<? 
require_once ('conf.php');
$idSecao = request('idSecao');
$idSubSecao = "";
$idioma = getIdioma();

$classe_principal = "";

switch($idSecao){
    case EMPRESA:
        $classe_principal = "empresa";
    break;    
    case SERVICOS:
        $classe_principal = "servicos";
    break;    
    case ETANOL:
        $classe_principal = "etanol";
    break;  
}
$nome_secao = $has_lista_icones = "";
if($settSecoes[$idSecao]){
    $nome_secao = getNomeSecao($idSecao);
    if(isset($settSecoes[$idSecao]["lista_icones"])) $has_lista_icones = $settSecoes[$idSecao]["lista_icones"];   
	
    $metaTitle = $nome_secao;    
}

?><? include "includes/head.php"?>
<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <a href="javascript:;"><strong><?=$nome_secao?></strong></a>
            </div>
        </div>
          <div class="<?=$classe_principal?>">
            <div class="esquerda">
                
                <? 
                $idDestaque = 1;
                $lstChamadas = Chamada::listar($idSecao,$idDestaque,"","status = ".ATIVO." AND idioma = $idioma","ordem ASC");
                //print Chamada::getLogSql();
                if($lstChamadas):
                    foreach($lstChamadas as $l):
                        $nome_img_tit = getNomeSecao($idSecao,$l->getContSecao(),"",LNG_PT);
                        $settSubSecao = getSettings($idSecao,$l->getContSecao());    
                        $url_secao = $settSubSecao['url_secao'];
                ?>
                    <div class="txt-box">
                        <div>    
                            <h2 class="tit-empresa"><img src="<?=DIRETORIO_RAIZ."css/img/tit-".setUrlAmigavel($nome_img_tit).getTextoByLang("","-eng")?>.png" height="35" alt="<?=$nome_secao?>" /> </h2>
                            <p><?=response_html($l->getTexto())?>
                            <a href="<?=DIRETORIO_RAIZ.$url_secao ?>" class="bt-saiba-mais<?=getTextoByLang(""," bt-saiba-mais-eng")?>" title="<?=getTextoByLang("Saiba Mais","Read More")?>"><?=getTextoByLang("Saiba Mais","Read More")?></a></p>
                        </div>
                    </div>                
                    
                <?
                    endforeach;
                endif;    
                ?>
                
                <? if($has_lista_icones):
                        $lstIcones = Media::listar(2,$idSecao,"","status = ".ATIVO." AND lang = '$idioma'","ordem ASC");
                        if($lstIcones):    
                        //print Media::getLogSql();
                ?>
                    <div class="associadas">                    
                        <h3><?=getTextoByLang("A SCA é associada a","SCA is associated with")?>:</h3>
                        <? foreach($lstIcones as $k=>$l):
                                $classe = $tagFechaLink = "";
                                if(count($lstIcones) > 1){
                                    if($k == 0) $classe = " class=\"sem-borda1\"";
                                    if($k == count($lstIcones)-1) $classe = " class=\"sem-borda2\"";
                                }else{
                                    $classe = " class=\"sem-borda\"";
                                }
                                $tituloMedia = $l->getNome();
                                $targetLink = $l->getTargetLink() == "_blank"?" rel=\"external\"":"";
                                $tagLink = $l->getUrlLink()?"<a href=\"".$l->getUrlLink()."\"$targetLink>":"";
                                if($l->getUrlLink()) $tagFechaLink = "</a>";
                            ?>
                        <p<?=$classe?>><?=$tagLink?><img src="<?=DIRETORIO_RAIZ.$l->getDir()."ico_".$l->getUrl()?>" alt="<?=$tituloMedia?>" /><?=$tagFechaLink?></p>
                        <? endforeach;?>
                    </div>
                     <? endif;?>
                <? endif;?>
            </div>
            
            <? include "includes/direita.php";?> 
                        
        </div>
      </div>
    </div>
    <? include "includes/rodape.php"?>
</div>
</body>
</html>