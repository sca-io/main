<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Home extends MY_Controller
{
    public function index()
    {
        $filter = $this->session->userdata('filter');

        if ($filter && count($filter['city'])) {

            $this->get_ethanol($filter);
            $this->get_delivery_paulinia();
            $this->get_ealq();
            $this->get_indicator();

            if(isset($filter) && isset($filter['comment']) && $filter['comment'] == 1) {
                $this->get_comment();
            }
            $this->renderer();
        } else {
            redirect('./home/filter');
        }
    }
    
    private function get_comment() {
        $this->load->model('comment_model');
        $this->data['comment'] = $this->comment_model->get()->result();
    }

    private function get_ethanol($filter)
    {
        $this->get_city($filter['city']);

        $this->load->model('ethanol_model');
        $this->load->helper('language');
        $this->lang->load('calendar');


        foreach ($this->data['state'] as $i => $item) {

            foreach ($item->city as $key => $row) {
                $this->data['state'][$i]->city[$key]->ethanol = $this->ethanol_model->get(array(
                    'city_id' => $row->id
                ), 6)->result();


                $eh_inside = 1;
                $eh_outside = 1;
                $ea_inside = 1;
                $ea_outside = 1;
                $eh_inside2 = 1;
                $eh_outside2 = 1;
                $ea_inside2 = 1;
                $ea_outside2 = 1;

                $this->data['state'][$i]->city[$key]->ethanol = array_reverse($this->data['state'][$i]->city[$key]->ethanol);

                foreach ($this->data['state'][$i]->city[$key]->ethanol as $e => $ethanol) {
                    $ethanol->datetime = date('d', strtotime($ethanol->datetime)) . '-' . lang('cal_' . strtolower(date('M', strtotime($ethanol->datetime))));


                    if($ethanol->eh_inside) {
                        $ethanol->eh_inside_diff_percent = round(( $ethanol->eh_inside / $eh_inside - 1)  * 100, 2);
                        $ethanol->eh_inside_diff = $ethanol->eh_inside - $eh_inside;
                        $ethanol->eh_inside_diff = round($ethanol->eh_inside_diff, 3);
                        $eh_inside = $ethanol->eh_inside;
                        $ethanol->eh_inside = number_format($ethanol->eh_inside, 0, '', '.');
                    }

                    if($ethanol->eh_outside) {
                        if (strpos($ethanol->eh_outside, '/') !== false) {
                            $eh_outside_average = explode('/', $ethanol->eh_outside);
                            $ethanol->eh_outside_average = ($eh_outside_average[0] + $eh_outside_average[1]) / 2;

                        }else{
                            $ethanol->eh_outside_average = $ethanol->eh_outside;
                        }

                        $ethanol->eh_outside_diff_percent = round(($ethanol->eh_outside_average / $eh_outside - 1)  * 100, 2);
                        $ethanol->eh_outside_diff = $ethanol->eh_outside_average - $eh_outside;
                        $ethanol->eh_outside_diff = round($ethanol->eh_outside_diff, 3);
                        $eh_outside = $ethanol->eh_outside_average;


                        if (strpos($ethanol->eh_outside, '/') !== false) {
                            $eh_outside_average[0] = number_format($eh_outside_average[0], 0, '', '.');
                            $eh_outside_average[1] = number_format($eh_outside_average[1], 0, '', '.');
                            $ethanol->eh_outside = $eh_outside_average[0] . '/' . $eh_outside_average[1];
                        }else{
                            $ethanol->eh_outside = number_format($ethanol->eh_outside, 0, '', '.');
                        }
                    }

                    if($ethanol->ea_inside) {
                        $ethanol->ea_inside_diff_percent = round(($ethanol->ea_inside / $ea_inside - 1)  * 100, 2);
                        $ethanol->ea_inside_diff = $ethanol->ea_inside - $ea_inside;
                        $ethanol->ea_inside_diff = round($ethanol->ea_inside_diff, 3);
                        $ea_inside = $ethanol->ea_inside;
                        $ethanol->ea_inside = number_format($ethanol->ea_inside, 0, '', '.');
                    }

                    if($ethanol->ea_outside) {
                        $ethanol->ea_outside_diff_percent = round(($ethanol->ea_outside / $ea_outside - 1)  * 100, 2);
                        $ethanol->ea_outside_diff = $ethanol->ea_outside - $ea_outside;
                        $ethanol->ea_outside_diff = round($ethanol->ea_outside_diff, 3);
                        $ea_outside = $ethanol->ea_outside;
                        $ethanol->ea_outside = number_format($ethanol->ea_outside, 0, '', '.');
                    }

                    if(in_array('ea', $filter['ethanol_type']) === false) {
                        unset($ethanol->ea_inside);
                        unset($ethanol->ea_outside);
                        unset($ethanol->ea_inside_diff);
                        unset($ethanol->ea_inside_diff_percent);
                        unset($ethanol->ea_outside_diff);
                        unset($ethanol->ea_outside_diff_percent);

                    }
                    if(in_array('eh', $filter['ethanol_type']) === false) {
                        unset($ethanol->eh_inside);
                        unset($ethanol->eh_outside);
                        unset($ethanol->eh_inside_diff);
                        unset($ethanol->eh_inside_diff_percent);
                        unset($ethanol->eh_outside_diff);
                        unset($ethanol->eh_outside_diff_percent);
                    }

                    $this->data['state'][$i]->city[$key]->ethanol[$e] = $ethanol;
                }
                array_shift($this->data['state'][$i]->city[$key]->ethanol);
            }
        }

    }

    public function save_filter()
    {
        if ($this->input->post()) {
            $this->session->set_userdata('filter', array(
                'city' => $this->input->post('city'),
                'ethanol_type' => $this->input->post('ethanol_type'),
                'comment' => $this->input->post('comment'),
            ));
        }

        redirect('./');
    }

    public function filter()
    {
        $this->get_city();

        $this->renderer();
    }

    private function get_delivery_paulinia ()
    {
        $this->load->model('deliverypaulinia_model');
        $this->data['deliverypaulinia'] = $this->deliverypaulinia_model->get(array(), 6)->result();

        $value = 1;
        $this->data['deliverypaulinia'] = array_reverse($this->data['deliverypaulinia']);

        foreach ($this->data['deliverypaulinia'] as $key => $item) {
            $item->datetime = date('d', strtotime($item->datetime)) . '-' . lang('cal_' . strtolower(date('M', strtotime($item->datetime))));

            if($item->value) {
                $item->value_diff_percent = round(( $item->value / $value - 1)  * 100, 2);
                $item->value_diff = $item->value - $value;
                $item->value_diff = round($item->value_diff, 3);
                $value = $item->value;
                $item->value = number_format($item->value, 0, '', '.');
            }
            $this->data['deliverypaulinia'][$key] = $item;
        }
        array_shift($this->data['deliverypaulinia']);
    }

    private function get_ealq() {
        $this->load->model('ealq_model');
        $this->data['ealq'] = $this->ealq_model->get(array(), 6)->result();

        $value = 1;

        $this->data['ealq'] = array_reverse($this->data['ealq']);

        foreach ($this->data['ealq'] as $key => $item) {
            $item->datetime = date('d', strtotime($item->datetime)) . '-' . lang('cal_' . strtolower(date('M', strtotime($item->datetime))));


            if($item->value) {
                $item->value_diff_percent = round(( $item->value / $value - 1)  * 100, 2);
                $item->value_diff = $item->value - $value;
                $item->value_diff = round($item->value_diff, 3);
                $value = $item->value;
                $item->value = number_format($item->value, 0, '', '.');
            }

            $this->data['ealq'][$key] = $item;
        }
        array_shift($this->data['ealq']);

    }

    private function get_indicator() {
        $this->load->model('indicator_model');
        $this->data['indicator'] = $this->indicator_model->get()->result();

    }

    private function get_city(array $id = array())
    {
        $where = array();

        if(count($id) > 0) {
            $where['id'] = $id;
        }

        $this->load->model('city_model');
        $city = $this->city_model->get_related($where)->result();

        $this->data['city'] = array();
        $this->data['state'] = array();

        foreach ($city as $row) {
            if (!isset($this->data['state'][$row->state_id])) {
                $this->data['state'][$row->state_id] = new stdClass();
                $this->data['state'][$row->state_id]->id = $row->state_id;
                $this->data['state'][$row->state_id]->name = $row->state_name;
                $this->data['state'][$row->state_id]->city = array();
            }
            $this->data['state'][$row->state_id]->city[] = $row;
            $this->data['city'][$row->id] = $row;
        }
    }
}