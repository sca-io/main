<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ethanol_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get(array $where = array(), $limit = null)
    {
        $this->db->order_by($this->getAlias() . '.datetime', 'DESC');
        return parent::get($where, $limit);
    }

}