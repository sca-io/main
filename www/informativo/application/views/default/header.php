<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle; ?></title>
    <base href="<?php echo base_url(); ?>">
    <meta name="controller" content="<?php echo $this->router->class ?>"/>
    <meta name="method" content="<?php echo $this->router->method ?>"/>
    <?php echo isset($assets) ? $assets : NULL; ?>
    <?php echo isset($css) ? $css : NULL; ?>
    <?php echo isset($js) ? $js : NULL; ?>
</head>
<body>
<div id="loader">
    <i class="fa-li fa fa-spinner fa-spin"></i>
</div>
<div class="container-fluid wrapper">
    <header>
        <div class="banner">
            <img src="./assets/img/banner.jpg?v=2" alt="">
        </div>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <div class="update_values">
                        <a href="./" class="hidden-sm hidden-md hidden-lg">
                            <img src="./assets/img/update_values.png" alt="" class="pull-left">
                            Atualizar valores
                            <img src="./assets/img/update_values.png" alt="" class="pull-right">
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav main-menu">
                        <li><a href="./home/filter" class="home_filter show-load">Filtro</a></li>
                        <li><a href="./home" class="home_index show-load">Preços</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </header>