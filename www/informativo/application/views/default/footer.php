<div class="footer-fix"></div>
<div class="footer">
    <div class="line-one"></div>
    <div class="line-two">
        <div class="container">
            <div class="phone">
                <h5>E-mail</h5>
                <h4><a href="mailto:info.sca@scetanol.com.br">info.sca@scetanol.com.br</a></h4>
            </div>
            <div class="phone">
                <h5>Site</h5>
                <h4>www.scetanol.com.br</h4>
            </div>
            <div class="pull-left">
                <div class="phone">
                    <h5>São Paulo</h5>
                    <h4>Tel: 11 3709-4900</h4>
                </div>
                <div class="phone">
                    <h5>Goiás</h5>
                    <h4>Tel: 62 3878-4900</h4>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="line-three"></div>
</div>
</div>
</body>
</html>