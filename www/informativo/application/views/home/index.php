<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-title">Regiões produtoras</h1>
            </div>
            <div class="col-md-10">
                <div class="ethanol-list container-base">
                    <div class="row">
                        <?php foreach ($state as $item): ?>
                            <?php foreach ($item->city as $city): ?>
                                <?php if (isset($city->ethanol[0]->ea_inside_diff_percent) || isset($city->ethanol[0]->ea_inside_diff) || isset($city->ethanol[0]->ea_inside) || isset($city->ethanol[0]->eh_inside_diff_percent) || isset($city->ethanol[0]->eh_inside_diff) || isset($city->ethanol[0]->eh_inside)): ?>
                                    <div class="col-sm-12 col-md-6 city item in">
                                        <?php
                                        $fa_info = (isset($city->descriptionin) && $city->descriptionin) ? " <i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i>" : "";
                                        $popover = (isset($city->descriptionin) && $city->descriptionin) ? "data-toggle=\"popover\" data-html=\"true\" data-trigger=\"click\" data-placement=\"bottom\" data-content=\"" . $city->descriptionin . "\"" : "";?>
                                        <h4 class="text-center" <?php echo $popover;?>><?php echo $city->name; ?><?php echo $fa_info;?></h4>
                                        <div class="item-title text-center">
                            <span class="date">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </span>
                                            <?php if (isset($city->ethanol[0]->eh_inside)) : ?>
                                                <span data-text="<?php echo $city->ethanol[0]->eh_inside; ?>" class="eh_inside">
                                    EH <i class="fa fa-sign-in" aria-hidden="true"></i>
                                </span>
                                            <?php endif; ?>
                                            <?php if (isset($city->ethanol[0]->eh_inside_diff)) : ?>
                                                <span data-text="<?php echo $city->ethanol[0]->eh_inside_diff; ?>"
                                                      class="eh_inside_diff diff">
                                    var
                                </span>
                                            <?php endif; ?>
                                            <?php if (isset($city->ethanol[0]->eh_inside_diff_percent)) : ?>
                                                <span data-text="<?php echo $city->ethanol[0]->eh_inside_diff_percent; ?>"
                                                      class="eh_inside_diff_percent diff_percent hidden-xs">
                                    <i class="fa fa-percent" aria-hidden="true"></i>
                                </span>
                                            <?php endif; ?>
                                            <?php if (isset($city->ethanol[0]->ea_inside)) : ?>
                                                <span data-text="<?php echo $city->ethanol[0]->ea_inside; ?>" class="ea_inside">
                                    EA  <i class="fa fa-sign-in" aria-hidden="true"></i>
                                </span>
                                            <?php endif; ?>
                                            <?php if (isset($city->ethanol[0]->ea_inside_diff)) : ?>
                                                <span data-text="<?php echo $city->ethanol[0]->ea_inside_diff; ?>"
                                                      class="ea_inside_diff diff">
                                    var
                                </span>
                                            <?php endif; ?>
                                            <?php if (isset($city->ethanol[0]->ea_inside_diff_percent)) : ?>
                                                <span data-text="<?php echo $city->ethanol[0]->ea_inside_diff_percent; ?>"
                                                      class="ea_inside_diff_percent diff_percent hidden-xs">
                                    <i class="fa fa-percent" aria-hidden="true"></i>
                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <?php foreach ($city->ethanol as $ethanol): ?>
                                            <div class="item-data text-center">
                                                <span class="date"
                                                      data-text="<?php echo $ethanol->datetime; ?>">
                                                    <?php echo $ethanol->datetime; ?>
                                                </span>
                                                <?php if (isset($ethanol->eh_inside)) : ?>
                                                    <span class="eh_inside"
                                                          data-text="<?php echo $ethanol->eh_inside; ?>"><?php echo $ethanol->eh_inside; ?></span>
                                                <?php endif; ?>
                                                <?php if (isset($ethanol->eh_inside_diff)) : ?>
                                                    <span class="eh_inside_diff diff"
                                                          data-text="<?php echo $ethanol->eh_inside_diff; ?>"><?php echo $ethanol->eh_inside_diff; ?></span>
                                                <?php endif; ?>
                                                <?php if (isset($ethanol->eh_inside_diff_percent)) : ?>
                                                    <span class="eh_inside_diff_percent diff_percent hidden-xs"
                                                          data-text="<?php echo $ethanol->eh_inside_diff_percent; ?>"><?php echo $ethanol->eh_inside_diff_percent; ?>
                                                        %</span>
                                                <?php endif; ?>
                                                <?php if (isset($ethanol->ea_inside)) : ?>
                                                    <span class="ea_inside"
                                                          data-text="<?php echo $ethanol->ea_inside; ?>"><?php echo $ethanol->ea_inside; ?></span>
                                                <?php endif; ?>
                                                <?php if (isset($ethanol->ea_inside_diff)) : ?>
                                                    <span class="ea_inside_diff diff"
                                                          data-text="<?php echo $ethanol->ea_inside_diff; ?>"><?php echo $ethanol->ea_inside_diff; ?></span>
                                                <?php endif; ?>
                                                <?php if (isset($ethanol->ea_inside_diff_percent)) : ?>
                                                    <span class="ea_inside_diff_percent diff_percent hidden-xs"
                                                          data-text="<?php echo $ethanol->ea_inside_diff_percent; ?>"><?php echo $ethanol->ea_inside_diff_percent; ?>
                                                        %</span>
                                                <?php endif; ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (isset($city->ethanol[0]->ea_outside_diff_percent) || isset($city->ethanol[0]->ea_outside_diff) || isset($city->ethanol[0]->ea_outside) || isset($city->ethanol[0]->eh_outside_diff_percent) || isset($city->ethanol[0]->eh_outside_diff) || isset($city->ethanol[0]->eh_outside)): ?>
                                    <div class="col-sm-12 col-md-6 city item out">
                                        <?php
                                        $fa_info = (isset($city->descriptionout) && $city->descriptionout) ? " <i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i>" : "";
                                        $popover = (isset($city->descriptionout) && $city->descriptionout) ? "data-toggle=\"popover\" data-html=\"true\" data-trigger=\"click\" data-placement=\"bottom\" data-content=\"" . $city->descriptionout . "\"" : "";?>
                                        <h4 class="text-center" <?php echo $popover;?>><?php echo $city->name; ?> (Interestadual)<?php echo $fa_info;?></h4>
                                        <div class="item-title text-center">
                                <span class="date">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </span>
                                            <?php if (isset($city->ethanol[0]->eh_outside)) : ?>
                                                <span data-text="<?php echo $city->ethanol[0]->eh_outside; ?>" class="eh_outside">
                                        EH <i class="fa fa-external-link" aria-hidden="true"></i>
                                    </span>
                                            <?php endif; ?>
                                            <?php if (isset($city->ethanol[0]->eh_outside_diff)) : ?>
                                                <span data-text="<?php echo $city->ethanol[0]->eh_outside_diff; ?>"
                                                      class="eh_outside_diff diff">
                                        var
                                    </span>
                                            <?php endif; ?>
                                            <?php if (isset($city->ethanol[0]->eh_outside_diff_percent)) : ?>
                                                <span data-text="<?php echo $city->ethanol[0]->eh_outside_diff_percent; ?>"
                                                      class="eh_outside_diff_percent diff_percent hidden-xs">
                                        <i class="fa fa-percent" aria-hidden="true"></i>
                                    </span>
                                            <?php endif; ?>
                                            <?php if (isset($city->ethanol[0]->ea_outside)) : ?>
                                                <span data-text="<?php echo $city->ethanol[0]->ea_outside; ?>" class="ea_outside">
                                        EA  <i class="fa fa-external-link" aria-hidden="true"></i>
                                    </span>
                                            <?php endif; ?>
                                            <?php if (isset($city->ethanol[0]->ea_outside_diff)) : ?>
                                                <span data-text="<?php echo $city->ethanol[0]->ea_outside_diff; ?>"
                                                      class="ea_outside_diff diff">
                                        var
                                    </span>
                                            <?php endif; ?>
                                            <?php if (isset($city->ethanol[0]->ea_outside_diff_percent)) : ?>
                                                <span data-text="<?php echo $city->ethanol[0]->ea_outside_diff_percent; ?>"
                                                      class="ea_outside_diff_percent diff_percent hidden-xs">
                                        <i class="fa fa-percent" aria-hidden="true"></i>
                                    </span>
                                            <?php endif; ?>
                                        </div>
                                        <?php foreach ($city->ethanol as $ethanol): ?>
                                            <div class="item-data text-center">
                                    <span class="date"
                                          data-text="<?php echo $ethanol->datetime; ?>"><?php echo $ethanol->datetime; ?></span>
                                                <?php if (isset($ethanol->eh_outside)) : ?>
                                                    <span class="eh_outside"
                                                          data-text="<?php echo $ethanol->eh_outside; ?>"><?php echo $ethanol->eh_outside; ?></span>
                                                <?php endif; ?>
                                                <?php if (isset($ethanol->eh_outside_diff)) : ?>
                                                    <span class="eh_outside_diff diff"
                                                          data-text="<?php echo $ethanol->eh_outside_diff; ?>"><?php echo $ethanol->eh_outside_diff; ?></span>
                                                <?php endif; ?>
                                                <?php if (isset($ethanol->eh_outside_diff_percent)) : ?>
                                                    <span class="eh_outside_diff_percent diff_percent hidden-xs"
                                                          data-text="<?php echo $ethanol->eh_outside_diff_percent; ?>"><?php echo $ethanol->eh_outside_diff_percent; ?>
                                                        %</span>
                                                <?php endif; ?>
                                                <?php if (isset($ethanol->ea_outside)) : ?>
                                                    <span class="ea_outside"
                                                          data-text="<?php echo $ethanol->ea_outside; ?>"><?php echo $ethanol->ea_outside; ?></span>
                                                <?php endif; ?>
                                                <?php if (isset($ethanol->ea_outside_diff)) : ?>
                                                    <span class="ea_outside_diff diff"
                                                          data-text="<?php echo $ethanol->ea_outside_diff; ?>"><?php echo $ethanol->ea_outside_diff; ?></span>
                                                <?php endif; ?>
                                                <?php if (isset($ethanol->ea_outside_diff_percent)) : ?>
                                                    <span class="ea_outside_diff_percent diff_percent hidden-xs"
                                                          data-text="<?php echo $ethanol->ea_outside_diff_percent; ?>"><?php echo $ethanol->ea_outside_diff_percent; ?>
                                                        %</span>
                                                <?php endif; ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <?php if (isset($comment) && count($comment) > 0) : ?>
            <div class="clearfix"></div>
            <div>&nbsp;</div>
            <div class="comment">
                <div class="row">
                    <div class="col-md-10">
                        <div class="text-center">
                            <a href="#collapse-comment" class="btn btn-info" data-toggle="collapse">
                                <i class="fa fa-1x fa-commenting-o" aria-hidden="true"></i> Comentários
                            </a>
                        </div>
                        <div class="collapse container-base" id="collapse-comment">
                            <?php foreach ($comment as $row): ?>
                                <p><?php echo $row->text; ?></p>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php if (count($ealq) > 0 || count($deliverypaulinia) > 0): ?>
        <div class="indicator">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-title">Indicadores</h1>
                </div>
                <div class="col-md-10">
                    <div class="ethanol-list container-base">
                        <div class="row">
                            <?php if (count($ealq) > 0): ?>
                                <div class="col-sm-12 col-md-6 city item">
                                    <?php
                                    $fa_info = (isset($indicator[0]->description) && $indicator[0]->description) ? " <i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i>" : "";
                                    $popover = (isset($indicator[0]->description) && $indicator[0]->description) ? "data-toggle=\"popover\" data-html=\"true\" data-trigger=\"click\" data-placement=\"bottom\" data-content=\"" . $indicator[0]->description . "\"" : "";?>
                                    <h4 class="text-center" <?php echo $popover;?>>Paulínia Diário (BM&F)<?php echo $fa_info;?></h4>
                                    <div class="item-title text-center">
                                        <span class="date">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </span>
                                        <?php if (isset($ealq[0]->value)) : ?>
                                            <span data-text="<?php echo $ealq[0]->value; ?>" class="eh_inside">
                                                EH <i class="fa fa-sign-in" aria-hidden="true"></i>
                                            </span>
                                        <?php endif; ?>
                                        <?php if (isset($ealq[0]->value)) : ?>
                                            <span
                                                data-text="<?php echo $ealq[0]->value; ?>"
                                                class="eh_inside_diff diff">
                                                var
                                            </span>
                                        <?php endif; ?>
                                        <?php if (isset($ealq[0]->value_diff_percent)) : ?>
                                            <span data-text="<?php echo $ealq[0]->value_diff_percent; ?>"
                                                  class="eh_inside_diff_percent diff_percent hidden-xs">
                                                <i class="fa fa-percent" aria-hidden="true"></i>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                    <?php foreach ($ealq as $item): ?>
                                        <div class="item-data text-center">
                                    <span class="date"
                                          data-text="<?php echo $item->datetime; ?>">
                                        <?php echo $item->datetime; ?>
                                    </span>
                                            <?php if (isset($item->value)) : ?>
                                                <span class="value"
                                                      data-text="<?php echo $item->value; ?>">
                                            <?php echo $item->value; ?>
                                        </span>
                                            <?php endif; ?>
                                            <?php if (isset($item->value_diff)) : ?>
                                                <span class="value_diff diff"
                                                      data-text="<?php echo $item->value_diff; ?>">
                                            <?php echo $item->value_diff; ?>
                                        </span>
                                            <?php endif; ?>
                                            <?php if (isset($item->value_diff_percent)) : ?>
                                                <span class="value_diff_percent hidden-xs"
                                                      data-text="<?php echo $item->value_diff_percent; ?>"><?php echo $item->value_diff_percent; ?>
                                                    %</span>
                                            <?php endif; ?>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            <?php endif; ?>
                            <?php if (count($deliverypaulinia) > 0): ?>
                                <div class="col-sm-12 col-md-6 city item">
                                    <?php
                                    $fa_info = (isset($indicator[1]->description) && $indicator[1]->description) ? " <i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i>" : "";
                                    $popover = (isset($indicator[1]->description) && $indicator[1]->description) ? "data-toggle=\"popover\" data-html=\"true\" data-trigger=\"click\" data-placement=\"bottom\" data-content=\"" . $indicator[1]->description . "\"" : "";?>
                                    <h4 class="text-center" <?php echo $popover;?>>Paulínia Diário (SCA)<?php echo $fa_info;?></h4>
                                    <div class="item-title text-center">
                                        <span class="date">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </span>
                                        <?php if (isset($deliverypaulinia[0]->value)) : ?>
                                            <span data-text="<?php echo $deliverypaulinia[0]->value; ?>" class="eh_inside">
                                                EH <i class="fa fa-sign-in" aria-hidden="true"></i>
                                            </span>
                                        <?php endif; ?>
                                        <?php if (isset($deliverypaulinia[0]->value)) : ?>
                                            <span
                                                data-text="<?php echo $deliverypaulinia[0]->value; ?>"
                                                class="eh_inside_diff diff">
                                                var
                                            </span>
                                        <?php endif; ?>
                                        <?php if (isset($deliverypaulinia[0]->value_diff_percent)) : ?>
                                            <span data-text="<?php echo $deliverypaulinia[0]->value_diff_percent; ?>"
                                                  class="eh_inside_diff_percent diff_percent hidden-xs">
                                                <i class="fa fa-percent" aria-hidden="true"></i>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                    <?php foreach ($deliverypaulinia as $item): ?>
                                        <div class="item-data text-center">
                                    <span class="date"
                                          data-text="<?php echo $item->datetime; ?>">
                                        <?php echo $item->datetime; ?>
                                    </span>
                                            <?php if (isset($item->value)) : ?>
                                                <span class="value"
                                                      data-text="<?php echo $item->value; ?>">
                                            <?php echo $item->value; ?>
                                        </span>
                                            <?php endif; ?>
                                            <?php if (isset($item->value_diff)) : ?>
                                                <span class="value_diff diff"
                                                      data-text="<?php echo $item->value_diff; ?>">
                                            <?php echo $item->value_diff; ?>
                                        </span>
                                            <?php endif; ?>
                                            <?php if (isset($item->value_diff_percent)) : ?>
                                                <span class="value_diff_percent hidden-xs"
                                                      data-text="<?php echo $item->value_diff_percent; ?>"><?php echo $item->value_diff_percent; ?>
                                                    %</span>
                                            <?php endif; ?>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    </div>
</section>