<div class="container">
    <h1 class="page-title">Comentários</h1>
    <div class="container-base col-xs-12 col-md-10">
        <table class="dataTable table table-bordered table-striped dt-responsive">
            <thead>
            <tr>
                <th>Texto</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($list as $row) : ?>
                <tr>
                    <td><?php echo $row->text; ?></td>
                    <td class="action">
                        <div class="btn-group">
                            <a class="btn btn-sm btn-primary"  href="./adm/<?php echo $this->uri->segment(2); ?>/editar/<?php echo $row->id; ?>">
                                <i class="fa fa-pencil" title="Editar"></i>
                            </a>
                            <a class="btn btn-sm btn-danger" href="./adm/<?php echo $this->uri->segment(2); ?>/excluir/<?php echo $row->id; ?>">
                                <i class="fa fa-trash-o fa-lg" title="Excluir"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            <?php endForeach; ?>
            </tbody>
        </table>
        <div class="clearfix"></div>
    </div>
</div>