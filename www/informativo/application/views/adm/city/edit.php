<div class="container">
    <h1 class="page-title"><?php echo (isset($data->id) ? 'Editar' : 'Novo') . ' ' . 'localidade' ?></h1>
    <div class="container-base col-xs-11 col-md-10">
        <form role="form" method="post" enctype="multipart/form-data"
              action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/salvar/<?php echo isset($data->id) ? $data->id : NULL ?>">
            <div class="form-group">
                <label for="name">Nome: </label>
                <input class="form-control" type="text" name="name" id="name" value="<?php echo isset($data->name) ? $data->name : NULL ?>" required />
            </div>
            <div class="form-group">
                <label for="descriptionin">Descrição: </label>
                <input class="form-control" type="text" name="descriptionin" id="descriptionin" value="<?php echo isset($data->descriptionin) ? $data->descriptionin : NULL ?>" />
            </div>
            <div class="form-group">
                <label for="descriptionout">Descrição (Fora): </label>
                <input class="form-control" type="text" name="descriptionout" id="descriptionout" value="<?php echo isset($data->descriptionout) ? $data->descriptionout : NULL ?>" />
            </div>
            <div class="clearfix"></div>
            <div class="form-group form-action text-right">
                <button class="btn btn-success" type="submit">Salvar</button>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
</div>
