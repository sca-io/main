<div class="container">
    <h1 class="page-title">Etanol</h1>
    <div class="container-base col-md-10">
        <div class="row">
            <div class="col-md-9">
                <form class="form-inline" role="form" method="post" enctype="multipart/form-data"
                      action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/upload">
                    <div class="form-group">
                        <label for="excel" class="control-label">Excel:</label>
                        <div class="input-group">
                            <input type="file" class="form-control" id="excel" name="excel"
                                   accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default">Enviar</button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-3 text-right">
                <a href="./assets/download/ethanol_import_exemple.xlsx" download="Preço Spot Etanol - Site.xlsx">
                    Arquivo de exemplo
                </a>
            </div>
        </div>
        <hr>
        <form action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/" class="form-inline"
              method="post">
            <legend>Filtro</legend>
            <div class="form-group">
                <label for="start" class="control-label">Intervalo: </label>
                <div class="input-group">
                    <input type="text" class="form-control" name="start" id="start"
                           value="<?php echo isset($start) && $start ? $start : null; ?>">
                    <span class="input-group-addon" id="basic-addon1">até</span>
                    <input type="text" class="form-control" name="end" id="end" value="<?php echo isset($end) && $end ? $end : null; ?>">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default">Filtrar</button>
                    </span>
                </div>
            </div>
        </form>
        <table class="dataTable table table-bordered table-striped dt-responsive">
            <thead>
            <tr>
                <th style="width: 88px">Data/Hora</th>
                <th>Localidade</th>
                <th>EH dentro</th>
                <th>EH fora</th>
                <th>EA dentro</th>
                <th>EA fora</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($list as $row) : ?>
                <tr>
                    <td><?php echo $row->date; ?></td>
                    <td><?php echo $city[$row->city_id]->state_name; ?> - <?php echo $city[$row->city_id]->name; ?></td>
                    <td><?php echo $row->eh_inside; ?></td>
                    <td><?php echo $row->eh_outside; ?></td>
                    <td><?php echo $row->ea_inside; ?></td>
                    <td><?php echo $row->ea_outside; ?></td>
                </tr>
            <?php endForeach; ?>
            </tbody>
        </table>
        <div class="clearfix"></div>
    </div>
</div>