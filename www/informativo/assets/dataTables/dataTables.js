$(function() {
	var oTable = $('.dataTable').DataTable({
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sZeroRecords': 'Nada encontrado - desculpe',
			'sInfo': 'Mostrando _START_ até _END_ de _TOTAL_ registros',
			'sSearch': 'Procurar:',
			'sInfoEmpty': 'Mostrando 0 até 0 de 0 registros',
			'sInfoFiltered': '(Filtrado de _MAX_ registros)',
			'oPaginate': {
				'sFirst': '<span class="glyphicon glyphicon-menu-left"></span><span class="glyphicon glyphicon-menu-left"></span>',
				'sPrevious': '<span class="glyphicon glyphicon-menu-left"></span>',
				'sNext': '<span class="glyphicon glyphicon-menu-right"></span>',
				'sLast': '<span class="glyphicon glyphicon-menu-right"></span><span class="glyphicon glyphicon-menu-right"></span>'
			}
		},
		'bRetrieve' : true,
		responsive: true,
		"autoWidth": false,
		"iDisplayLength" : 15
	});

	$('.dataTable').data('oTable', oTable);
	$('.dataTables_length').css('display','none');

});