<?php
session_start();
header("Content-type: text/html; charset=UTF-8");

/*ini_set("log_errors", 1);
ini_set("error_log", "/scalcool/logs/php-error.log");*/
define("NOME_PROJETO","SCA - ETNOL DO BRASIL");
if($_SERVER['SERVER_NAME'] == "atomicaserv" || $_SERVER['SERVER_NAME'] == "localhost" || $_SERVER['SERVER_NAME'] == "local.sites" ){
    define("DB_HOST","atomicaserv");
    define("DATABASE","scalcool_db");
    define("DB_USER","root");
    define("DB_PASS","");
    define("DIRETORIO_RAIZ","/scalcool/");
    ini_set('display_errors', '1');
    error_reporting(E_ALL);
}else{
    define("DB_HOST","mysql.shared.acl.com.br");
    define("DATABASE","scetanol_com_br_db");
    define("DB_USER","scetanol_db");
    define("DB_PASS","@UJDMsalm45");
    define("DIRETORIO_RAIZ","/");
    
    ini_set('display_errors', '1');
    error_reporting(E_ALL);
}/*else{
    define("DB_HOST","localhost");
    define("DATABASE","scalcool_adm_scalcool");
    define("DB_USER","scalc_user");
    define("DB_PASS","UJDMsalm45");
    define("DIRETORIO_RAIZ","/");
    
    if(!isset($_SERVER['DOCUMENT_ROOT'])) $_SERVER['DOCUMENT_ROOT'] = "e:/inetpub/vhosts/scalcool.com.br/httpdocs";
    ini_set('display_errors', '0');
    error_reporting(0);
}*/

define("ROOT_ABS",$_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ);
define("LIMITE_MAX_UPLOAD",7*1024*1024); //7MB
define("GERAR_COPIA_DE_IMG_ORIGINAL",true); 


$icones = Array();
$icones["pdf"] = DIRETORIO_RAIZ."css/img/ico-acrobat.png";
$icones["novo"] = DIRETORIO_RAIZ."css/img/ico-novo.png";

function getExtArquivo($file){
    $arr = explode(".",$file);
    return $arr[count($arr)-1];
}
function getIconeArquivo($file, $comTag = true){
    global $icones;
    $ext = getExtArquivo($file);
    if($ext && isset($icones[$ext])){
        if($comTag) return "<img src=\"".$icones[$ext]."\" alt=\"\" />";
        else $icones[$ext];
    }
}


function getEnderecoSite(){   return "http://".$_SERVER['SERVER_NAME'].DIRETORIO_RAIZ;}
define("ENDERECO_SITE","http://".$_SERVER['SERVER_NAME'].DIRETORIO_RAIZ);
define("EMAIL_FROM_DEFAULT","sapo@atomica.com.br");


function redirect($url){
    print "<script>document.location='$url'</script>";exit;
}


require_once(ROOT_ABS.'classes/rsa.php');
require_once(ROOT_ABS.'classes/PhpMailer/class.phpmailer.php');

function __autoload($class_name){
    $directorys = array('classes/','classes/sapo/');
    $pastaAbs = ROOT_ABS.'/'; 
    //print_r($_SERVER);
    foreach ($directorys as $directory){     
        //print $pastaAbs.$directory.$class_name.'.php <br>';
	$caminhoClasse = $pastaAbs.$directory.$class_name.'.php';
        if (file_exists($caminhoClasse)){
            //print $caminhoClasse."<br>";
            require_once ($caminhoClasse);
            break;
        }
    }
}

$ip = $_SERVER['REMOTE_ADDR'];

$keys = generate_keys(0);//USAR ESTE METODO PARA GERAR NOVAS CHAVES DE CRIPTOGRAFIA RSA
@define("KEYENCRYPT",'2S4P0C0L1R105!');
@define("PUBLICKEY",'44735783');
@define("PRIVATEKEYCRIPT",'4759');
@define("PRIVATEKEYDECRIPT",'41808559');
@define("ENCODEKEY", '&%2'+end(explode(" ",rsa_encrypt ($keySapo,  PRIVATEKEYCRIPT,  PUBLICKEY)))+'9203');

function secureResponse($var){
    $encoded = rsa_encrypt ($var,  PRIVATEKEYCRIPT,  PUBLICKEY);
    return str_replace(" ",ENCODEKEY,$encoded);
}

function secureRequest($var,$valor=''){
    if($valor){
            $var = str_replace(ENCODEKEY, ' ',$valor);
    }else{
            $var = str_replace(ENCODEKEY, ' ',request($var));
    }
    $decoded = rsa_decrypt($var,  PRIVATEKEYDECRIPT,  PUBLICKEY);
    return $decoded ;
}

function secureArray($var,$separador){
    $text = request($var);
    if($text){
        $arrText = explode($separador,$text);
        for($i = 0; $i < count($arrText); $i++){
            $var = str_replace(ENCODEKEY, ' ',$arrText[$i]);
            $arrText[$i] = rsa_decrypt($var,  PRIVATEKEYDECRIPT,  PUBLICKEY);
        }
        return $arrText;
    }
}

function getSession($var){
    if(isset($_SESSION[$var])) return $_SESSION[$var];
}

function setSession($var,$valor){
    return $_SESSION[$var] = $valor;
}

$controleAcesso = new ControleAcesso();
$controleAcesso->dominiosPermitidos = array("atomicaserv","localhost","scalcool.com.br","www.scalcool.com.br","local.sites:8080","scetanol.com.br","www.scetanol.com.br","scetanol1.dominiotemporario.com");
$controleAcesso->emailsNotificacoes = array("dvrusso@atomica.com.br");
$controleAcesso->checarAcesso();

function limpaSQLInjection($txtOrig){
    global $controleAcesso;
    $a = array(
        "/(.*)select (.*) from(.*)/msi",
        "/(.*)update (.*) set(.*)/msi",
        "/(.*)insert (.*) (set|values ?\()(.*)/msi",
        "/(.*)delete from (.*)/msi",	
        "/(.*)drop (database|table)(.*)?/msi",
        "/(.*)create (table|database) (.*)?/msi",
        "/(.*)union (all )?select (.*)?/msi",
        "/\'\+(.*)/msi",
        "/(convert|char)\((.*)/msi",
        "/NSFTW/msi",
        "/\"\+\(/msi"
    );
    $b = array("","","","","","","","","","","");
    $txt = preg_replace($a,$b,$txtOrig);
    /*** Verifica se houve tentativa de injection ***/
    if(($txtOrig != $txt)){
        $controleAcesso->barrarAcesso(ControleAcesso::TENTAVIVA_SQL_INJECTION,$txtOrig);        
    }
    /************************************************/
    while(indexOf($txt,"\\") != -1){
        $txt = str_replace("\\","",$txt);
    }
    $txt = str_replace("'","",$txt);
    $txt = str_replace("%27","",$txt);
    return $txt;
}

function limpaXSS($txtOrig){
    $a = array(
        "/<\/(.*) style(.*)/i",
        "/\"\-\-\>(.*)/i",
        "/\'\-\-\>(.*)/i",
        "/stYle\=x\:expre\/\*\*\/ssion\(/i",
        "/\" ns\=(.*)/i"
    );
    $b = array("","","","","","","","","","","","","");
    $txt = preg_replace($a,$b,$txtOrig);
    /*** Verifica se houve tentativa de Xss ***/
    
    if(($txtOrig != $txt)){
        $controleAcesso->barrarAcesso(ControleAcesso::TENTAVIVA_XSS,$txtOrig);        
    }
    /************************************************/
    while(indexOf($txt,"\\") != -1){
        $txt = str_replace("\\","",$txt);
    }
    return $txt;
}


function request($var){
    if($var && isset($_REQUEST[$var])){
	$str = str_replace('\\"','\"',$_REQUEST[$var]);
	$str = str_replace("\\'","\'",$str);		
	$str = limpaSQLInjection($str);
	$str = limpaXSS($str);	
	$str = strip_tags($str,"<p><b><strong><em><i><a><img><br><h1><h2><h3>");
        /*if(function_exists("mysql_real_escape_string")){
            $str = mysql_real_escape_string($str);
        }else{*/
            $str = addslashes($str);
        /*}    */    
	return $str;
    }
}

function response_html($val){
    $val = limpaXSS($val);
    $val = stripcslashes($val);
    $val = strip_tags($val,"<p><b><strong><em><i><a><img><br><h1><h2><h3>");
    return $val;
}
function response_attr($val,$encode=true){
    $val = limpaXSS($val);
    $val = stripcslashes($val);
    $val = strip_tags($val,"<p><b><strong><em><i><a><img><br><h1><h2><h3>");
    if($encode) $val = htmlentities($val,ENT_QUOTES,"UTF-8");
    return $val;
}



function getListaBySecao($idSecao,$where,$campos='id,status'){
    if($idSecao == USUARIOS){
        $campos .= ",nome"; 
        $obj = Usuario::listar($campos,$where);
    }elseif($idSecao == HISTORICOS){
        $obj = Historico::listar('','','','',$where);
    }elseif($idSecao == ACOES_SUSTENTAVEIS || $idSecao == MENU_TIME_LINE){
        $campos .= ",titulo,id_secao"; 
        $obj = Chamada::listar($idSecao,'',$campos,$where);
    }elseif($idSecao == CONTATOS){
        $campos .= ",nome"; 
        $obj = Contato::listar($campos,$where);
    }elseif($idSecao == GESTAO_DE_PESSOAS){
        $campos .= ",nome"; 
        $obj = CadGestaoPessoa::listar($campos,$where);
    }elseif($idSecao == NOTICIAS || $idSecao == CERTIFICACOES){
        $campos .= ",id_secao,titulo"; 
        $obj = VersaoConteudo::listar($idSecao,"",$campos,$where);
    }elseif($idSecao == UNIDADES_PRODUTORAS){
        $campos .= ",nome"; 
        $obj = UnidadeProdutora::listar($campos,$where);
    }elseif($idSecao == SEJA_NOSSO_CLIENTE){
        $campos .= ",nome"; 
        $obj = Cliente::listar("",$campos,$where);
    }
    return $obj;
}

function getObj($id,$idSecao,$campos='id,status'){
    if($idSecao == USUARIOS){
        $campos .= ",nome"; 
        $obj = Usuario::ler($id,$campos);
    }elseif($idSecao == HISTORICOS){
        $obj = Historico::ler($id,$campos);
    }elseif($idSecao == ACOES_SUSTENTAVEIS || $idSecao == MENU_TIME_LINE){
        $campos .= ",titulo,id_secao";  
        $obj = Chamada::ler($id,$campos);
    }elseif($idSecao == CONTATOS){
        $campos .= ",nome";    
        $obj = Contato::ler($id,$campos);
    }elseif($idSecao == GESTAO_DE_PESSOAS){
        $campos .= ",nome"; 
        $obj = CadGestaoPessoa::ler($id,$campos);
    }elseif($idSecao == NOTICIAS || $idSecao == CERTIFICACOES){
        $campos .= ",id_secao,titulo";     
        $obj = VersaoConteudo::ler($id,$campos);
    }elseif($idSecao == UNIDADES_PRODUTORAS){
        $campos .= ",nome"; 
        $obj = UnidadeProdutora::ler($id,$campos);
    }elseif($idSecao == SEJA_NOSSO_CLIENTE){
        $campos .= ",nome"; 
        $obj = Cliente::ler($id,$campos);
    }
    return $obj;
}

//////////////---SEÇÕES---////////////////////// LEMBRAR DE ATUALIZAR OS VALORES NO util.js, POIS LÁ EXISTEM AS MESMAS CONSTANTES
define("USUARIOS",1);
define("LIXEIRA",2);
define("HISTORICOS",3);
define("HOME",4);
define("GESTAO_DE_PESSOAS",26);
define("EMPRESA",16);
define("SERVICOS",15);
define("ESTATISTICAS",25);
define("UNIDADES_PRODUTORAS",24);
define("ETANOL",23);
define("AREA_RESTRITA",27);



//SUB-SECOES
define("TRAJETORIA",28);
define("ACOES_SUSTENTAVEIS",29);
define("MERCADO_DE_ATUACAO",30);

define("COMERCIALIZACAO",31);
define("SCA_TRADING",32);
define("INTELIGENCIA_DE_MERCADO",33);

define("ESPECIFICACOES",34);
define("CERTIFICACOES",35);
define("LEGISLACOES",36);


define("NOTICIAS",37);
define("CONTATOS",38);
define("NEWSLETTER",39);
define("MENU_TIME_LINE",40);

define("MERCADO_INTERNO",41);
define("MERCADO_EXTERNO",42);
define("FERRAMENTAS",43);
define("SEJA_NOSSO_CLIENTE",44);

/*** TIPOS DE CATEGORIAS */
define("TP_CAT_ARQUIVO",1);
define("TP_CAT_CONTEUDO",2);

/*** TIPOS DE MEDIDA DE COTAÇÂO */
define("TP_COT_RS",1);
define("TP_COT_US",2);
$vetTpCot[TP_COT_RS] = "R$/m³";
$vetTpCot[TP_COT_US] = "U$/m³";

$arrOrdemMenu = Array(HOME,NOTICIAS,EMPRESA,SERVICOS,ETANOL,UNIDADES_PRODUTORAS,ESTATISTICAS,GESTAO_DE_PESSOAS,CONTATOS,NEWSLETTER,SEJA_NOSSO_CLIENTE,USUARIOS,HISTORICOS,LIXEIRA,FERRAMENTAS);
$secoesLixeira = Array(USUARIOS,NOTICIAS,MENU_TIME_LINE,ACOES_SUSTENTAVEIS,UNIDADES_PRODUTORAS,CERTIFICACOES,GESTAO_DE_PESSOAS,CONTATOS,SEJA_NOSSO_CLIENTE);
$secoesInsereHistorico = Array(USUARIOS,NOTICIAS,MENU_TIME_LINE,ACOES_SUSTENTAVEIS,UNIDADES_PRODUTORAS,CERTIFICACOES,GESTAO_DE_PESSOAS,CONTATOS,SEJA_NOSSO_CLIENTE);
$secoesSubHome = Array(HOME,EMPRESA,SERVICOS,ETANOL);
$GLOBALS['secoesMultiConteudo'] = Array();

define("TEXTO_INICIAL",1);

$settImpCotacoes = array("exts"=>Array('xls','xlsx'),"path"=>"cotacoes/");

$settModulosDeHome = Array(
    HOME => Array( 
        1 => Array("template" => "rotativo", "limite_chamadas" => 6,"campos" => "arquivo" ,"ordenar" => true,"excluir"=>true),  //Webdoor link,target,arquivo
        2 => Array("template" => "chamadas", "limite_chamadas" => 6,"max_len_texto" => 130,"campos" => "lista_conteudos,conteudo,titulo,ckeditor" ,"ordenar" => true,"excluir"=>true),  //DESTAQUES
        3 => Array("template" => "cotacoes", "limite_chamadas" => 14,"campos" => "" ,"ordenar" => true,"excluir"=>true,"importacao"=>$settImpCotacoes),  //COTACOES NORMAIS
        4 => Array("template" => "cotacoes", "limite_chamadas" => 11,"campos" => "" ,"ordenar" => true,"excluir"=>true,"importacao"=>$settImpCotacoes)  //COTACOES ESALQ
    ),
    EMPRESA => Array(
        1 => Array("template" => "chamadas", "limite_chamadas" => 3,"max_len_texto" => 550 ,"campos" => 'lista_conteudos,conteudo,titulo_hidden,ckeditor',"ordenar" => false,"excluir"=>false),  //Destaques
        2 => Array("template" => "icones", "limite_chamadas" => 5,"ordenar" => true,"excluir"=>true)  //ICONES
    ), 
    SERVICOS => Array(
        1 => Array("template" => "chamadas", "limite_chamadas" => 3,"max_len_texto" => 550  ,"campos" => 'lista_conteudos,conteudo,titulo_hidden,ckeditor',"ordenar" => false,"excluir"=>false)  //Destaques
    ),
    ETANOL => Array(
        1 => Array("template" => "chamadas", "limite_chamadas" => 3,"max_len_texto" => 550  ,"campos" => 'lista_conteudos,conteudo,titulo_hidden,ckeditor',"ordenar" => false,"excluir"=>false)  //Destaques
    ), 
    MENU_TIME_LINE => Array(
        1 => Array("template" => "chamadas", "max_len_texto" => 1000  ,"campos" => 'arquivo,ano_titulo,texto',"ordenar" => true,"excluir"=>true)  //Destaques
    ), 
    UNIDADES_PRODUTORAS => Array(
        1 => Array("template" => "unidades", "max_len_texto" => 5000 ,"campos" => '',"ordenar" => true,"excluir"=>true)  //Destaques
    ) 
);

$settSecoes = array(
    EMPRESA => Array(
        "nome_pt" => "Empresa",
        "nome_en" => "Firm",
        "url_secao" => "empresa/",
        "lista_icones" => true,
        "sub_secoes" => Array(
            MENU_TIME_LINE => Array(
                "nome_pt" => "Menu Timeline",
                "nome_en" => "Timeline menu"
            ),
            TRAJETORIA => Array(
                "nome_pt" => "Trajetória",
                "nome_en" => "History",
                "url_secao" => "empresa/trajetoria/"
            ),
            ACOES_SUSTENTAVEIS => Array(
                "nome_pt" => "Ações Sustentáveis",
                "nome_en" => "Sustainable Actions",
                "url_secao" => "empresa/acoes-sustentaveis/"
            ),
            MERCADO_DE_ATUACAO => Array(
                "nome_pt" => "Mercado de Atuação",
                "nome_en" => "Market of Operation",
                "url_secao" => "empresa/mercado-de-atuacao/"
            )
        )
    ),
    SERVICOS => Array(
        "nome_pt" => "Serviços",
        "nome_en" => "Services",
        "url_secao" => "servicos/",
        "lista_icones" => false,
        "sub_secoes" => Array(
            COMERCIALIZACAO => Array(
                "nome_pt" => "Comercialização",
                "nome_en" => "Marketing",
                "url_secao" => "servicos/comercializacao/"
            ),
            SCA_TRADING => Array(
                "nome_pt" => "SCA Trading",
                "nome_en" => "Trading",
                "url_secao" => "servicos/sca-trading/"
            ),
            INTELIGENCIA_DE_MERCADO => Array(
                "nome_pt" => "Inteligência de Mercado",
                "nome_en" => "Market Intelligence",
                "url_secao" => "servicos/inteligencia-de-mercado/"
            )
        )        
    ),
    ETANOL => Array(
        "nome_pt" => "Etanol",
        "nome_en" => "Ethanol",
        "url_secao" => "etanol/",
        "lista_icones" => false,
        "sub_secoes" => Array(
            ESPECIFICACOES => Array(
                "nome_pt" => "Especificações",
                "nome_en" => "Specifications",
                "url_secao" => "etanol/especificacoes/",                
                "filtro_por_assunto" => true,
                "sub_secoes" => Array(
                    MERCADO_EXTERNO => Array(
                        "nome_pt" => "Mercado Externo",
                        "nome_en" => "External Market",
                        "url_secao" => "etanol/especificacao/mercado-externo/",
                        "filtro_por_assunto" => true
                    ),
                    MERCADO_INTERNO => Array(
                        "nome_pt" => "Mercado Interno",
                        "nome_en" => "Internal Market",
                        "url_secao" => "etanol/especificacao/mercado-interno/",
                        "filtro_por_assunto" => true
                    )
                )
            ),
            CERTIFICACOES => Array(
                "nome_pt" => "Certificações",
                "nome_en" => "Certifications",
                "url_secao" => "etanol/certificacoes/",
                "texto_inicial" => true,
                "filtro_por_assunto" => true 
            ),
            LEGISLACOES => Array(
                "nome_pt" => "Legislações",
                "nome_en" => "Legislation",
                "url_secao" => "etanol/legislacoes/",
                "filtro_por_assunto" => true ,
                "filtro_de_resultado" => true
            )
        )
    ),    
    UNIDADES_PRODUTORAS => Array(
        "nome_pt" => "Unidades Produtoras",
        "nome_en" => "Production Units",
        "url_secao" => "unidades-produtoras/"
    ),
    ESTATISTICAS => Array(
        "nome_pt" => "Estatísticas",
        "nome_en" => "Statistics",
        "url_secao" => "estatisticas/",
	"lista_docs" => true
    ),
    GESTAO_DE_PESSOAS => Array(
        "nome_pt" => "Gestão de Pessoas",
        "nome_en" => "People Management",
        "url_secao" => "gestao-de-pessoas/",
        "abas_sapo" => array("Listagem"=>"listagem_cadastro.php","Texto"=>"edicao_conteudo.php")
    ),
    CONTATOS => Array(
        "nome_pt" => "Contato",
        "nome_en" => "Contact Us",
        "url_secao" => "contatos/"
    ),   
    AREA_RESTRITA => Array(
        "nome_pt" => "Área Restrita",
        "nome_en" => "Restricted Area",
        "url_secao" => "area-restrita/"
    ),
    NOTICIAS => Array(
        "nome_pt" => "Notícias",
        "nome_en" => "News",
        "url_secao" => "noticias/"
    ),
    SEJA_NOSSO_CLIENTE => Array(
        "nome_pt" => "Seja Nosso Cliente",
        "nome_en" => "Our Client Is",
        "url_secao" => "seja-nosso-cliente/",
        "abas_sapo" => array("Listagem"=>"listagem_cadastro.php","Texto"=>"edicao_conteudo_com_subs.php")
    )    
    
);

function getIdSecaoPai($idSecao){
    global $settSecoes;
    $secaoSelc = null;
    foreach($settSecoes as $key=>$set){
        if(isset($set["sub_secoes"])){
            foreach($set["sub_secoes"] as $key2=>$set2){
                if($key2 == $idSecao) $secaoSelc = $key;
            }
        }       
    }
    return $secaoSelc;
} 

function getNomeSecao($idSecao,$idSubSecao="",$idSubSecao2="",$lang=""){
    global $settSecoes;
    $nome = "";    
    $sett = null;
    if($idSecao){
         if(isset($settSecoes[$idSecao])) $sett = $settSecoes[$idSecao];      
    }
    if($idSubSecao && $sett){
        if(isset($sett["sub_secoes"][$idSubSecao])) $sett = $sett["sub_secoes"][$idSubSecao];        
    }
    if($idSubSecao2 && $sett){
        if(isset($sett["sub_secoes"][$idSubSecao2])) $sett = $sett["sub_secoes"][$idSubSecao2];        
    }
    if($lang){
       if($lang == LNG_PT) $nome = $sett["nome_pt"];
       if($lang == LNG_EN) $nome = $sett["nome_en"];
    }else{
        if($sett) $nome = getTextoByLang($sett["nome_pt"],$sett["nome_en"]); 
    }
    
    return $nome;
}

function getSettings($idSecao,$idSubSecao="",$idSubSecao2=""){
    global $settSecoes;
    $sett = null;
    if($idSecao){
         if(isset($settSecoes[$idSecao])) $sett = $settSecoes[$idSecao];      
    }
    if($idSubSecao && $sett){        
        if(isset($sett["sub_secoes"][$idSubSecao])) $sett = $sett["sub_secoes"][$idSubSecao];
        else $sett = null;
    }    
    if($idSubSecao2 && $sett){
        if(isset($sett["sub_secoes"][$idSubSecao])) $sett = $sett["sub_secoes"][$idSubSecao];  
        else $sett = null;
    }  
    return $sett;
}

//////////////---TIPOS DE USUARIO---//////////////////////
define("ADMINISTRADOR",1);
define("USUARIO",3);

//////////////---STATUS---//////////////////////
define("INATIVO",0);
define("ATIVO",1);
define("NAOAVALIADO",2);


$vetStatus[INATIVO] = "Exclu&iacute;do";
$vetStatus[ATIVO] = "Ativo";
$vetStatus[NAOAVALIADO] = "N&atilde;o Avaliado";


///////////////////////HISTORICO///////////////
define("DELETADO",1);
define("ENVIADOLIXEIRA",2);
define("EDITADO",3);
define("IMAGEMDELETADA",4);
define("IMAGEMPRINCIPAL",5);
define("RESTAURADO",6);
define("INSERIDO",7);
define("LOGIN_NO_SAPO",8);
define("ERRO_AO_LOGAR_NO_SAPO",9);
define("LOGOUT_NO_SAPO",10);
define("BLOQUEIO_DE_ACESSO_AO_SAPO",11);
define("DESBLOQUEIO_DE_ACESSO_AO_SAPO",12);
define("IMPORTACAO_DE_DADOS",13);

/////////////////////IDIOMAS //////////////////
define("LNG_PT",1);
define("LNG_EN",2);
$vetIdioma[LNG_PT] = "Portugu&ecirc;s";
$vetIdioma[LNG_EN] = "Ingl&ecirc;s";

/////////////////// CAMINHO DAS IMAGENS///////////////

define("PATH_IMGS","arquivos/imagens/");
define("PATH_DOCS","arquivos/docs/");

class PorporcaoImagem{
    public $aspect;
    public $minSizeCrop;
    public $maxSizeCrop;
    public $dimensoes;//Array onde o key é o prefixo
}

$porporcoes["default"][""] = new PorporcaoImagem();
$porporcoes["default"][""]->aspect = "";//1.500;
$porporcoes["default"][""]->dimensoes[""] = "800x800";
//$porporcoes["default"][""]->dimensoes["rt_"] = "693x384";

$porporcoes["default"]["LstCham"] = new PorporcaoImagem();
$porporcoes["default"]["LstCham"]->aspect = 1.339;
$porporcoes["default"]["LstCham"]->dimensoes["m_"] = "344x257";

$porporcoes[HOME]["rotativo"] = new PorporcaoImagem();
$porporcoes[HOME]["rotativo"]->aspect = 1.805;
$porporcoes[HOME]["rotativo"]->dimensoes[""] = "800x800";
$porporcoes[HOME]["rotativo"]->dimensoes["rt_"] = "693x384";

$porporcoes[EMPRESA]["icones"] = new PorporcaoImagem();
$porporcoes[EMPRESA]["icones"]->aspect = "";
$porporcoes[EMPRESA]["icones"]->dimensoes[""] = "300x300";
$porporcoes[EMPRESA]["icones"]->dimensoes["ico_"] = "70x70";

$porporcoes[ACOES_SUSTENTAVEIS][""] = new PorporcaoImagem();
$porporcoes[ACOES_SUSTENTAVEIS][""]->aspect = 1.339;
$porporcoes[ACOES_SUSTENTAVEIS][""]->dimensoes[""] = "600x600";
$porporcoes[ACOES_SUSTENTAVEIS][""]->dimensoes["m_"] = "344x257";

$porporcoes[ACOES_SUSTENTAVEIS]["icones"] = new PorporcaoImagem();
$porporcoes[ACOES_SUSTENTAVEIS]["icones"]->aspect = "";
$porporcoes[ACOES_SUSTENTAVEIS]["icones"]->dimensoes[""] = "500x500";
$porporcoes[ACOES_SUSTENTAVEIS]["icones"]->dimensoes["ico_"] = "70x70";

$porporcoes[MENU_TIME_LINE]["chamadas"] = new PorporcaoImagem();
$porporcoes[MENU_TIME_LINE]["chamadas"]->aspect = 2.042;
$porporcoes[MENU_TIME_LINE]["chamadas"]->dimensoes[""] = "500x500";
$porporcoes[MENU_TIME_LINE]["chamadas"]->dimensoes["m_"] = "196x96"; //168x78

///////////////////////TIPOS DE FORMACAO ACADEMICA(PT)///////////////

define('FA_1_GRAU_INCOM',1);
define('FA_1_GRAU_COM',2);    
define('FA_2_GRAU_INCOM',3);
define('FA_2_GRAU_COM',4); 
define('FA_SUPERIOR_INCOM',5);
define('FA_SUPERIOR_COM',6);
define('FA_POS_GRAD',7);

$arrFormAcadPtIncompletas = array(FA_1_GRAU_INCOM,FA_2_GRAU_INCOM,FA_SUPERIOR_INCOM) ;
$arrFormAcad[LNG_PT][FA_1_GRAU_INCOM] =  "1° grau incompleto";
$arrFormAcad[LNG_PT][FA_1_GRAU_COM] = "1° grau completo";
$arrFormAcad[LNG_PT][FA_2_GRAU_INCOM] = "2° grau incompleto";
$arrFormAcad[LNG_PT][FA_2_GRAU_COM] = "2° grau completo";
$arrFormAcad[LNG_PT][FA_SUPERIOR_INCOM] = "Superior incompleto";
$arrFormAcad[LNG_PT][FA_SUPERIOR_COM] = "Superior completo";
$arrFormAcad[LNG_PT][FA_POS_GRAD] = "Pós graduação";

///////////////////////TIPOS DE FORMACAO ACADEMICA(EN)///////////////

define('ELEMENTARY_SCHOOL_INCOM',1);
define('ELEMENTARY_SCHOOL_COM',2);
define('MIDDLE_SCHOOL_INCOM',3);    
define('MIDDLE_SCHOOL_COM',4);   
define('HIGH_SCHOOL_INCOM',5);
define('HIGH_SCHOOL_COM',6);
define('HIGHER_EDUCATION_INCOM',7); 
define('HIGHER_EDUCATION_COM',8); 
define('GRADUATE_STUDIES',9);

$arrFormAcadEnIncompletas = array(ELEMENTARY_SCHOOL_INCOM,MIDDLE_SCHOOL_INCOM,HIGH_SCHOOL_INCOM,HIGHER_EDUCATION_INCOM) ;

$arrFormAcad[LNG_EN][ELEMENTARY_SCHOOL_INCOM] = "Elementary school Incomplete";
$arrFormAcad[LNG_EN][ELEMENTARY_SCHOOL_COM] = "Elementary school Complete";
$arrFormAcad[LNG_EN][MIDDLE_SCHOOL_INCOM] = "Middle school Incomplete";
$arrFormAcad[LNG_EN][MIDDLE_SCHOOL_COM] = "Middle school Complete";
$arrFormAcad[LNG_EN][HIGH_SCHOOL_INCOM] = "High school Incomplete";
$arrFormAcad[LNG_EN][HIGH_SCHOOL_COM] = "High school Complete";
$arrFormAcad[LNG_EN][HIGHER_EDUCATION_INCOM] = "Higher education Incomplete";
$arrFormAcad[LNG_EN][HIGHER_EDUCATION_COM] = "Higher education Complete";
$arrFormAcad[LNG_EN][GRADUATE_STUDIES] = "Graduate studies";


function geraOptionsFormAcad2($idx="",$idioma=LNG_PT){
	global $arrFormAcad;
	foreach($arrFormAcad[$idioma] as $i=>$e){
		$selected = "";
		if($i == $idx) $selected = "selected='selected'";
		print "<option value=\"".$i."\" $selected>".$e."</option>\n";
	}
}



////////////////////// OUTROS /////////////////////////
$arrDiaSemana = Array("Domingo","Segunda","Terça","Quarta","Quinta","Sexta","Sábado");
define("NUMDIASSEMANA",7);

define("MASCULINO",1);
define("FEMININO",2);
$vetSexo[MASCULINO] = "Masculino";
$vetSexo[FEMININO] = "Feminino";




$arrAcao = Array("", "Deletado", "Enviado para lixeira", "Editado", "Imagem deletada","Imagem definida como principal", "Restaurado", "Inserido");
$arr_mes = Array("", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
$arr_mes_pq = Array("", "Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez");
$arr_mes_pq_ing = Array("", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

function getUrl($prefixo,$idSecaoTipo,$idSubSecao=''){
    global $secoesMultiConteudo;
    $str_ = '';
    if($idSecaoTipo == USUARIOS){
        $str_ = $prefixo."usuario.php?idSecao=$idSecaoTipo";
    }elseif($idSecaoTipo == HISTORICOS){
        $str_ = $prefixo."historicos.php?idSecao=$idSecaoTipo";
    }elseif($idSecaoTipo == LIXEIRA){
        $str_ = $prefixo."lixeira.php?idSecao=$idSecaoTipo";
    }elseif($idSecaoTipo == EMPRESA || $idSecaoTipo == NOTICIAS || $idSecaoTipo == CERTIFICACOES){
        $str_ = $prefixo."conteudo.php?idSecao=".$idSecaoTipo;
    }elseif($idSecaoTipo == ACOES_SUSTENTAVEIS){
        $str_ = $prefixo."chamadas.php?idSecao=".$idSecaoTipo;
    }elseif($idSecaoTipo == FERRAMENTAS){
        $str_ = $prefixo."ferramentas.php?idSecao=".FERRAMENTAS;
    }
    return $str_;
}

function getUrlByTipo($tipo,$idSecao,$pref=""){
    $url = "";
    switch($tipo){
        case Secao::CONTEUDO_UNICO:
            $pref = $pref?$pref:"edicao_";
            $url = $pref."conteudo.php?idSecao=$idSecao";
        break;  
        case Secao::LISTA_DE_CONTEUDOS:
            $pref = $pref?$pref:"listagem_";
            $url = $pref."conteudo.php?idSecao=$idSecao";
        break; 
        case Secao::LISTA_DE_CHAMADAS:
            $pref = $pref?$pref:"listagem_";
            $url = $pref."chamadas.php?idSecao=$idSecao";
        break; 
        case Secao::HOME:
            $pref = $pref?$pref:"edicao_";
            $url = $pref."home.php?idSecao=$idSecao";
        break; 
        case Secao::CADASTRO:
            $pref = $pref?$pref:"listagem_";
            $url = $pref."cadastro.php?idSecao=$idSecao";
        break;  
        case Secao::LISTA_ARQUIVOS:
            $pref = $pref?$pref:"listagem_";
            $url = $pref."arquivos.php?idSecao=$idSecao";
        break;        
    }
    return $url;

}






function inArray($str,$array){ //Verifica primeiro se o elemento tem valores
    if(count($array)>0)
        return in_array($str,$array);
    else
        return false;
}

function tiraAcento($txt){
    $a = array('À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','Œ','œ','?','?','?','?','?','?','?','?','?','?','?','?','Š','š','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','Ÿ','?','?','?','?','Ž','ž','?','ƒ','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?');
    $b = array('A','A','A','A','A','A','AE','C','E','E','E','E','I','I','I','I','D','N','O','O','O','O','O','O','U','U','U','U','Y','s','a','a','a','a','a','a','ae','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','o','u','u','u','u','y','y','A','a','A','a','A','a','C','c','C','c','C','c','C','c','D','d','D','d','E','e','E','e','E','e','E','e','E','e','G','g','G','g','G','g','G','g','H','h','H','h','I','i','I','i','I','i','I','i','I','i','IJ','ij','J','j','K','k','L','l','L','l','L','l','L','l','l','l','N','n','N','n','N','n','n','O','o','O','o','O','o','OE','oe','R','r','R','r','R','r','S','s','S','s','S','s','S','s','T','t','T','t','T','t','U','u','U','u','U','u','U','u','U','u','U','u','W','w','Y','y','Y','Z','z','Z','z','Z','z','s','f','O','o','U','u','A','a','I','i','O','o','U','u','U','u','U','u','U','u','U','u','A','a','AE','ae','O','o');
    return str_replace($a,$b,$txt);
}

function setUrlAmigavel($str){
    $str = stripslashes($str);
    $str = tiraAcento($str);
    $str = strtolower($str);
    $str = str_replace("|","",$str);
    $str = urlencode($str);
    $str = str_replace("+","-",$str);
    $str = str_replace('.',"",$str);
	$str = str_replace("%E2%80","",$str);//travessao
	$str = str_replace('%C2%A0',"-",$str); //&nbsp;
    $str = str_replace("%BA","",$str);//º
    $str = str_replace('%2C',"",$str);//,
    $str = str_replace('%3F',"",$str);//?
    $str = str_replace('%22',"",$str);//"
    $str = str_replace("%27","",$str);//'
    $str = str_replace("%3A","",$str);//:
    $str = str_replace('%0D',"",$str);//
    $str = str_replace('%0A',"",$str);//
    $str = str_replace("%25","-por-cento",$str);//%
    $str = str_replace("%2F","-",$str);// /
    $str = str_replace("%26lsquo%3B","",$str);// ‘
    $str = str_replace("%93","",$str);// “  &ldquo;
    $str = str_replace("%94","",$str);// ”  &rdquo;
    $str = str_replace("%24","S",$str);// $
    return $str;
}

function gs($numcarats=10, $lmin=true, $lmai=true, $numeros=true, $simb=false){
    $lemin="abcdefghijklmnopqrstuvwxyz";
    $lemai="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $nmeros="0123456789";
    $simbolos="*@!&%";
    $senha="";
    $caracteres="";
    if($lmin) $caracteres.=$lemin;
    if($lmai) $caracteres.=$lemai;
    if($numeros) $caracteres.=$nmeros;
    if($simb) $caracteres.=$simbolos;
    $length=strlen($caracteres);
    for($i=1; $i<=$numcarats; $i++)
    {
        $rand=mt_rand(1, $length);
        $senha.=$caracteres[$rand-1];
    }
    return $senha;
}

function enviarEmail($paramns){    
    $mail = new PHPMailer();
    $mail->SetLanguage("br", "sapo/classes/PhpMailer/"); //Idioma
    $dadosSmtp = array();
    //if($_SERVER['SERVER_NAME'] == "atomicaserv") require_once($_SERVER['DOCUMENT_ROOT'].'/dados-smtp.php');   
    
    if($_SERVER['SERVER_NAME'] == "atomicaserv" || $_SERVER['SERVER_NAME'] == "localhost"){
        $dadosSmtp["Host"] = "smtp.atomica.com.br";
	$dadosSmtp["Username"] = "sapo@atomica.com.br"; //Nome do usuario SMTP
	$dadosSmtp["Password"] = "zxqw2580";     //Senha do usuario SMTP	 
    }   
    
    
    if(count($dadosSmtp) > 0){
        $mail->IsSMTP();	
        $mail->Host = $dadosSmtp["Host"]; 			//Host do servidor SMTP
        $mail->SMTPDebug = 1;
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Port = 587;
        $mail->Username = $dadosSmtp["Username"];   //Nome do usuario SMTP
        $mail->Password = $dadosSmtp["Password"];   //Senha do usuario SMTP	
    }
    $mail->From = $paramns['emailFrom'];    //nome de quem ta enviando, vai aparecer na coluna "De:"
    $mail->FromName = $paramns['fromName'];    //nome de quem ta enviando, vai aparecer na coluna "De:"
    $mail->AddAddress($paramns['email']);
    $mail->AddReplyTo($paramns['emailFrom']);
    $mail->IsHTML(true);
    $mail->Subject = $paramns['subject'];
    $mail->Body = $paramns['mensagem'];
    
   
    if($mail->Send()){
        return true;
    }else{
        echo "Erro do envio: " . $mail->ErrorInfo;
        return false;
    }
}

function getIdioma($inverte=false){

    if(request("idioma")){
        $idioma =  request("idioma");
    }else{
        $idioma = getSession("idioma");
    }
	$idioma =  (int) $idioma;
	if($idioma != LNG_EN) $idioma = LNG_PT;
    setSession("idioma",$idioma);
    if($inverte){
        if($idioma == LNG_PT) $idioma = LNG_EN;
        else $idioma = LNG_PT;
    }
    return $idioma;
}

function getTextoByLang($txtPort,$txtIng){
    $text = "";
    $idioma = getIdioma();
    switch($idioma){
        case LNG_PT: $text = $txtPort; break;
        case LNG_EN: $text = $txtIng; break;
    }
    return $text;
}

function moeda($valor,$tipo="R$"){
    if($tipo == "R$")   return number_format($valor, 2, ',', '.');
    if($tipo == "U$")   return number_format($valor, 2, '.', ',');
}

function indexOf($mystring, $findme){
    if($mystring && $findme){
        $pos = strpos($mystring, $findme);
        if($pos === false){
            return -1;
        }else{
            return $pos;
        }
    }else{
        return -1;
    }
}

/**
 * @var array $ent Variavel que troca ex: á por &aacute;
 */
$ent = array( 'Ã'=>'&Atilde;', 'ã'=>'&atilde;','Ç'=>'&Ccedil;', 'ç'=>'&ccedil;', 'Â'=>'&Acirc;', 'â'=>'&acirc;', 'Ô'=>'&Ocirc;', 'Ó'=>'&Oacute;', 'ô'=>'&ocirc;', 'É'=>'&Eacute;', 'é'=>'&eacute;', 'Ú'=>'&Uacute;', 'ú'=>'&uacute;', 'Á'=>'&Aacute;', 'á'=>'&aacute;', 'Í'=>'&Iacute;', 'í'=>'&iacute;', 'Ê'=>'&Ecirc;', 'ê'=>'&ecirc;','"'=>'&quot;'); 
/**
 * @var array $ent2 Variavel que troca ex: á por &aacute;
 */
$ent2 = array('&Atilde;'=>'Ã', '&atilde;'=>'ã','&Ccedil;'=>'Ç', '&ccedil;'=>'ç', '&Acirc;'=>'Â', '&acirc;'=>'â', '&Ocirc;'=>'Ô', '&Oacute;'=>'Ó', '&ocirc;'=>'ô', '&Eacute;'=>'É', '&eacute;'=>'é', '&Uacute;'=>'Ú', '&uacute;'=>'ú', '&Aacute;'=>'Á', '&aacute;'=>'á', '&Iacute;'=>'Í', '&iacute;'=>'í', '&Ecirc;'=>'Ê', '&ecirc;'=>'ê', '&quot;'=>'"');

$vMimeType = Array(
    '' => Array(''),
    'doc'  => Array('application/msword'),
    'docx' => Array('application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/save'),
    'rtf'  => Array('application/msword'),	
    'dot'  => Array('application/msword'),
    'mht'  => Array('multipart/related','message/rfc822'),
    'odt'  => Array('application/vnd.oasis.opendocument.text'),
    'txt'  => Array('text/plain'),	
    'ppt'  => Array('application/vnd.ms-powerpoint'),
    'pptx' => Array('application/vnd.openxmlformats-officedocument.presentationml.presentation'),    
    'xls'  => Array('application/vnd.ms-excel','application/download'),
    'xlsx' => Array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
    'pdf'  => Array('application/pdf'),
    'swf'  => Array('application/x-shockwave-flash'),
    'mp3'  => Array('audio/mpeg','audio/mp3'),
    'jpg'  => Array('image/pjpeg','image/jpeg'),
    'gif'  => Array('image/gif'),
    'png'  => Array('image/png','image/x-png')
);
function validaMimeType($arquivo,$extensions){
    global $vMimeType;
    $ok = false;
    if(isset($arquivo)){
        if(count($extensions)>0){
            $ext = explode(".",$arquivo["name"]);
            $ext = end($ext);
            $ext = strtolower($ext);
            $mime =  $arquivo["type"];
            foreach($extensions as $e){
                if($e == $ext){
                    foreach($vMimeType[$e] as $m){
                        if($m == $mime){
                            $ok = true;
                            break;
                        }
                    }            
                }
            }
        }
        return $ok;        
    }else{
        return false;
    }
}

function glob_recursive($pattern, $flags = 0)
 {
     $files = glob($pattern, $flags);

    foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir)
     {
         $files = array_merge($files, glob_recursive($dir.'/'.basename($pattern), $flags));
     }

    return $files;
 }
 
define("CE_FLAG_ERRO_FDATAM",1);
define("CE_FLAG_ERRO_FOWNER",2);
define("CE_FLAG_ERRO_FEXT",3);
define("CE_FLAG_ERRO_FPERM",4);
define("CE_FLAG_ERRO_FSIZE",5);
define("CE_FLAG_NOVO_ARQUIVO",6);
  
function GetUsernameFromUid($uid) 
{ 
  if (function_exists('posix_getpwuid')) 
  { 
    $a = posix_getpwuid($uid); 
    return $a['name']; 
  } 
  # This works on BSD but not with GNU 
  elseif (strstr(php_uname('s'), 'BSD')) 
  { 
    exec('id -u ' . (int) $uid, $o, $r); 

    if ($r == 0) 
      return trim($o['0']); 
    else 
      return $uid; 
  } 
  elseif (is_readable('/etc/passwd')) 
  { 
    exec(sprintf('grep :%s: /etc/passwd | cut -d: -f1', (int) $uid), $o, $r); 
    if ($r == 0) 
      return trim($o['0']); 
    else 
      return $uid; 
  } 
  else 
    return $uid; 
} 

?>