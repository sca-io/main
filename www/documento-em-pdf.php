<? require_once ('conf.php');
$idioma = getIdioma();
$assunto= secureRequest("assunto");
$url_amig = request("url_amig");

$idSecao = request('idSecao');
$idSubSecao = request('idSubSecao');
$subSecao2= request("subSecao2");

$nome_secao = "";
$nome_secao_atual = "";
$nome_img_tit = "";
$brad_crumb = "";
$url_secao = "";
$url_sub_secao = "";
$url_classe_doc = "";

if($idSecao){
    switch($idSecao){
        case EMPRESA:
            $url_secao = DIRETORIO_RAIZ."empresa/";
        break;    
        case SERVICOS:
            $url_secao = DIRETORIO_RAIZ."servicos/";
        break;  
        case ETANOL:
            $url_secao = DIRETORIO_RAIZ."etanol/"; 
        break;    
    }    
    
    if(isset($settSecoes[$idSecao])) $nome_secao = getNomeSecao($idSecao);
    $brad_crumb = '<a href="'.$url_secao.'">'.$nome_secao.'</a>';
    $nome_img_tit = $nome_secao;
    $nome_secao_atual = $nome_secao;

}

if($idSubSecao){
    switch($idSubSecao){
        case ESPECIFICACOES:
            $url_sub_secao = $url_secao."especificacoes/";
        break;
    }
    $nome_sub_secao = getNomeSecao($idSecao,$idSubSecao);
    $brad_crumb .= '&nbsp;&nbsp;/&nbsp;<a href="'.$url_sub_secao.'">'.$nome_sub_secao.'</a>'; 
    $nome_img_tit = getNomeSecao($idSecao,$idSubSecao,"",LNG_PT);
    $nome_secao_atual = $nome_sub_secao;	
    if(isset($settSecoes[$idSecao]["sub_secoes"][$idSubSecao]["classes_doc"]))$classes_doc = $settSecoes[$idSecao]["sub_secoes"][$idSubSecao]["classes_doc"];
}
$classes_doc = array();
$nome_classe_doc = "";
$idSubSecao2 = "";
if($subSecao2){
    switch($subSecao2){
        case "mercado-interno":
            $nome_classe_doc = getNomeSecao($idSecao,$idSubSecao,MERCADO_INTERNO);
            $nome_img_tit = getNomeSecao($idSecao,$idSubSecao,MERCADO_INTERNO,LNG_PT);
            $idSubSecao2 = MERCADO_INTERNO;
        break;
        case "mercado-externo":
            $nome_classe_doc = getNomeSecao($idSecao,$idSubSecao,MERCADO_EXTERNO);
            $nome_img_tit = getNomeSecao($idSecao,$idSubSecao,MERCADO_EXTERNO,LNG_PT);
            $idSubSecao2 = MERCADO_EXTERNO;
        break;
    }
    if($nome_classe_doc){
        $metaTitle = $nome_classe_doc;
        $brad_crumb .= '&nbsp;&nbsp;/&nbsp;<a href="javascript:;"><strong>'.$nome_classe_doc.'</strong></a>'; 
    }
    $url_classe_doc = $url_sub_secao.$subSecao2."/";
    $nome_secao_atual = $nome_classe_doc;
	
}
$idSecaoSel = $idSubSecao;
if($idSubSecao2) $idSecaoSel = $idSubSecao2;
$url_self = $url_classe_doc;

?><? include "includes/head.php"?>
<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
		<?=response_html($brad_crumb)?>  	
            </div>
        </div>
        <div class="etanol legislacoes">
            <div class="esquerda">
            	<div class="txt-box">
                    <div>
                        <h2><img src="<?=DIRETORIO_RAIZ?>css/img/tit-<?=setUrlAmigavel($nome_img_tit).getTextoByLang("","-eng")?>.png" alt="<?=$nome_secao_atual?>" /> </h2>
                        <? include "includes/filtro-assunto.php"?>
                    </div>
                </div>
                
                <?
                /*** LISTAGEM DE DOCS ***/
                /*** A variavel assunto é setada no include de filtro de assunto ***/
                $listaArqs = null;
                if($assunto){
                    $where = "status = ".ATIVO." AND tipo = '".Media::DOCS."' AND lang = '$idioma'";
                    if($assunto) $where .= " AND id_categoria = '$assunto'";
                    $idSecaoMd = $idSubSecao;
                    if($idSubSecao2) $idSecaoMd = $idSubSecao2;
                    $listaArqs = Media::listar("",$idSecaoMd,"",$where,"data_insercao DESC");
                }
                ?>
                <div class="resultados">
                    <? if($listaArqs):?>					
                        <? foreach($listaArqs as $l):?>
                        <p><iframe src="<?=DIRETORIO_RAIZ.$l->getDir().$l->getUrl()?>" width="592" height="365" class="iframe" ></iframe> </p>
                        <? endforeach;?>					
                    <? else:?>
                        <? if($assunto):?><p><br /><?=getTextoByLang("Nenhum documento encontrado!","No documents found!");?></p> <? endif;?>
                    <? endif;?>	
                </div>
                <a href="javascript:history.back();" class="bt-voltar" title="<?=getTextoByLang("Voltar","Return")?>"> &lt; <?=getTextoByLang("Voltar","Return")?></a> 
            </div>            
            <? include "includes/direita.php"?>                        
        </div>
      </div>
    </div>
    <? include "includes/rodape.php"?>
</div>
</body>
</html>