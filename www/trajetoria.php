<?php 
require_once("conf.php");
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header ("Cache-Control: no-cache, must-revalidate"); 
header ("Pragma: no-cache"); 	
header("Content-type: text/xml; charset=UTF-8");
print '<?xml version="1.0" encoding="ISO-8859-1"?>';
print '<root>';

$idioma = getIdioma();
$idSecao = MENU_TIME_LINE;
$idDestaque = 1;
$lstChamadas = Chamada::listar($idSecao,$idDestaque,"id,titulo,texto,url_media","status = ".ATIVO." AND idioma = $idioma","ordem ASC");
//print Chamada::getLogSql();
if($lstChamadas){
    foreach($lstChamadas as $l){
        $urlMedia = $l->getUrlMedia();
        $arrUrlMedia = explode("/",$urlMedia);
        $nomeImg = $arrUrlMedia[count($arrUrlMedia)-1];
        $urlMediaAbs = str_replace($nomeImg,"m_".$nomeImg,$urlMedia);
        $texto = (response_attr($l->getTexto(),false));
        $texto = str_replace('"', "&quot;", $texto);
        print '<ano imagem="'.DIRETORIO_RAIZ.response_attr($urlMediaAbs).'" titulo="'.response_attr($l->getTitulo(),false).'" texto="'.$texto.'" idTrabalho="'.$l->getId().'" />';
    }    
}
print '</root>';
?>

<?/*
<root>
	<ano imagem="" titulo="2000" texto="Criação da SCA com 31 Unidades Produtoras clientes, sendo 28 em S�o Paulo, 02 em Goi�s e 01 em Minas Gerais." idTrabalho="0" />

	<ano imagem="" titulo="2001" texto="Implanta��o do Sistema de Comercializa��o via Web.
� SCA inaugura escrit�rio no itaim Bibi-SP." idTrabalho="0" />

	<ano imagem="" titulo="2002" texto="As Unidades Produtoras clientes SCA comercializam 2.680 mil m de etanol, representando 23,9% do total produzido no Centro-Sul." idTrabalho="0" />

	<ano imagem="" titulo="2003" texto="Lan�amentos dos carros flex fuel no Brasil.
� SCA � membro da Bolsa Brasileira de Mercadorias (BBM)." idTrabalho="0" />

	<ano imagem="" titulo="2004" texto="As 41 Unidades Produtoras clientes SCA moem 84.4 milh�es de toneladas de cana representando 25,3% da Regi�o Centro-Sul" idTrabalho="0" />

	<ano imagem="" titulo="2005" texto="Com perspectiva de exporta��es � criada a SCA Trading S.A" idTrabalho="0" />

	<ano imagem="" titulo="2006" texto="Abertura da Filial Goi�s.
� SCA Trading � membro da IETHA (Internacional Ethanol Trade Association)." idTrabalho="0" />

	<ano imagem="" titulo="2007" texto="Lan�amento da nova 
marca SCA. 
� Implanta��o de princ�pios 
de Responsabilidade Social e Ambiental.
� A SCA Trading recebe o pr�mio �Exporta S�o Paulo�." idTrabalho="0" />

	<ano imagem="" titulo="2008" texto="A regi�o Centro-Sul, exporta 4,2 milh�es de m3 e a SCA � a 
maior exportadora de etanol com 24% do volume exportado." idTrabalho="0" />

	<ano imagem="" titulo="2009" texto="Primeira Unidade Produtora
no MS.

� SCA alcan�a a marca de 64 Unidades Produtoras clientes 
consolidando posi��o de corretora l�der.
� Com a expressiva venda de ve�culos flex fuel o volume de 
etanol supera as vendas de gasolina no Brasil." idTrabalho="0" />

	<ano imagem="" titulo="2010" texto="SCA completa 10 anos de exist�ncia, com excel�ncia na 
presta��o de servi�os." idTrabalho="0" />
</root>*/ ?>