<? require_once ('conf.php');
$idSecao = GESTAO_DE_PESSOAS;
$idioma = getIdioma();

$nome_secao = "";
$nome_secao_atual = "";
$brad_crumb = "";
$url_secao = DIRETORIO_RAIZ."gestao-de-pessoas/";
if($idSecao){   
    if(isset($settSecoes[$idSecao])) $nome_secao = getNomeSecao($idSecao);
    $brad_crumb = $nome_secao;
    $brad_crumb = "<strong>".$brad_crumb."</strong>";	
    $brad_crumb = '<a href="'.$url_secao.'">'.$brad_crumb.'</a>';
    $nome_secao_atual = $nome_secao;

}
$metaTitle = $nome_secao;

?><? include "includes/head.php"?>
<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <?=$brad_crumb?>
            </div>
        </div>
        <div class="empresa gestao-de-pessoas">
            <div class="esquerda">
            	<h2><img src="<?=DIRETORIO_RAIZ?>css/img/tit-trabalhe-conosco<?=getTextoByLang("","-eng")?>.png"  alt="<?=$nome_secao_atual?>" /> </h2>
                <h4><?=getTextoByLang("Formulário enviado com sucesso!","Form submitted successfully!")?></h4>      
                
                <a href="javascript:history.back();" class="bt-voltar" title="<?=getTextoByLang("Voltar","Return")?>"> &lt; <?=getTextoByLang("Voltar","Return")?></a>
                
            </div>
                        
        </div>
      </div>
    </div>
    <? include "includes/rodape.php"?>
</div>
</body>
</html>