<? require_once ('conf.php');
$idSecao = NOTICIAS;
$idSubSecao = request("idSubSecao");
$idioma = getIdioma();
$tit = request('tit');


$conteudo = null;
$where = "idioma = '$idioma' AND status = '".VersaoConteudo::PUBLICADO."'";
if($tit)  $where .= " AND url_amigavel = '$tit'";

$listaConts = VersaoConteudo::listar($idSecao,"","id,titulo,texto,keywords,descricao,id_secao,url_embed,img_destaque,ids_relacionados,data_publicacao",$where,"data_publicacao DESC","1");
//print VersaoConteudo::getLogSql();
if($listaConts){
	$conteudo = $listaConts[0];    
}    


if($conteudo){
    //$idSecao = $conteudo->getIdSecao();
    $titulo = $conteudo->getTitulo();
    $descricao = $conteudo->getDescricao();
    $keywords = $conteudo->getKeywords();
    $texto = $conteudo->getTexto();
    $dataPublicacao = $conteudo->getDataPublicacao();
    $idsRelacionados = $conteudo->getIdsRelacionados();
}



$nome_secao = "";
$nome_secao_atual = "";
$nome_img_tit = "";
$brad_crumb = "";
$url_secao = "";

if($idSecao){
    $url_secao = DIRETORIO_RAIZ."noticias/";
    if(isset($settSecoes[$idSecao])) $nome_secao = getNomeSecao($idSecao);
    $brad_crumb = $nome_secao;
    if(!$conteudo) $brad_crumb = "<strong>".$brad_crumb."</strong>";	
    $brad_crumb = '<a href="'.$url_secao.'">'.$brad_crumb.'</a>';
    $nome_img_tit = setUrlAmigavel(getNomeSecao($idSecao,"","",LNG_PT));
    $nome_secao_atual = $nome_secao;
}

if($conteudo){
	$brad_crumb .= '&nbsp;&nbsp;/&nbsp;<a href="javascript:;"><strong>'.$titulo.'</strong></a>';
}


?><? include "includes/head.php"?>
<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <?=$brad_crumb?>
            </div>
        </div>
        <div class="busca noticias-listagem noticias-conteudo">
        	<h3><img src="<?=DIRETORIO_RAIZ?>css/img/tit-<?=$nome_img_tit.getTextoByLang("","-eng")?>.png" height="35" alt="<?=$nome_secao_atual?>" /></h3>
            <div class="esquerda">
            	<div class="texto">
                	<div class="bloco">
                        <blockquote>
                                <? if($conteudo):?>
                                        <h4><?=Util::dataDoBD(response_html($dataPublicacao))?></h4>
                                        <h2><?=response_html($titulo)?></h2>
                                        <?=response_html($texto)?>
                                <? else:?>
                                        <p><br /><?=getTextoByLang("Conteúdo não encontrado!","Content not found!");?><br /><br /><br /></p>                
                                <? endif;?>
                        </blockquote>
                    </div>
            		<a href="javascript:history.back()" class="bt-voltar" title="<?=getTextoByLang("Voltar","Return")?>"> &lt; <?=getTextoByLang("Voltar","Return");?></a>
                </div>
                
                <? if(isset($idsRelacionados) && $idsRelacionados):?>	
                    <h5><img src="<?=DIRETORIO_RAIZ?>css/img/tit-noticias-relacionadas<?=getTextoByLang("","-eng");?>.png" height="34" /></h5>   							
                    <? $listaNot = VersaoConteudo::listar($idSecao,"","id,titulo,texto,data_publicacao,url_amigavel"," status = ".VersaoConteudo::PUBLICADO." AND id IN($idsRelacionados)"," data_publicacao DESC");?>
                    <? foreach($listaNot as $ln):?>
                    <div class="bloco">
                        <blockquote>                            
                            <h4><?=Util::dataDoBD(response_html($ln->getDataPublicacao()),"d-m-Y")?></h4>
                            <h2><?=response_html($ln->getTitulo())?></h2>
                            <p><?=Util::truncarTexto(response_html(strip_tags($ln->getTexto())),200)?></p>
							<a href="<?=$url_secao.response_attr($ln->getUrlAmigavel())?>" class="seta" title="<?=getTextoByLang("Leia mais","Read more");?> +"><?=getTextoByLang("Leia mais","Read more");?></a>
                        </blockquote>
                    </div>  
                    <? endforeach;?>                     
                    <a href="<?=$url_secao?>" class="bt-veja-todas-as-noticias" title="<?=getTextoByLang("Veja todas as notícias","See all news");?>"> <strong>+</strong> <?=getTextoByLang("Veja todas as notícias","See all news");?></a>
                <? endif;?>	               
            </div>
            
            <div class="direita">			
                <? include "includes/direita-noticias.php"?>
            </div>
                  
        </div>
        
      </div>
    </div>
    <? include "includes/rodape.php"?>
</div>
</body>
</html>