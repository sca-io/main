
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SCA - ETANOL DO BRASIL</title>
<meta name="title" content="SCA - ETANOL DO BRASIL" />

<link rel="STYLESHEET" type="text/css" href="/css/style.css" />
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="/css/style-ie.css"/><![endif]-->
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="/css/style-ie8.css"/><![endif]-->
<link href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:700' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" media="all" href="/css/jScrollPane.css" />
<script type="text/javascript">
if (navigator.userAgent.toLowerCase().indexOf('chrome')!=-1){
    document.write('<link rel="stylesheet" type="text/css" href="/css/style-chrome.css"/>');                    
}
</script>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="/js/Popup.js"></script>
<script type="text/javascript" src="/js/scripts.js"></script>
<script type="text/javascript">
	function OpenOffSite(a)
{
window.open(a.href, "OffSite", "directories=yes,location=yes,menubar=yes,resizable=yes,scrollbars=yes,toolbar=yes").focus();
return false;
}
</script>

    <script type="text/javascript" src="/js/Rotativo.js"></script>
    <script type="text/javascript" src="/js/validacao.js"></script>
    <script type="text/javascript" src="/js/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="/js/jScrollPane.js"></script>

    <script  type="text/javascript">
        var DIRETORIO_RAIZ = '/';
        var IDIOMA = parseInt('1');
        $(window).ready(function(){
            $('a[rel=external]').attr('target', '_blank');
            $(".bolinha").each(function(){
                var txt=$(this).children("DIV").html();
                var lng=txt.length;
                $(this).children("DIV").css({"width":txt.length*7});                    
            });

                            webdoor = new Rotativo({conteudos:".fotos IMG",intervalo:5000,fade:true})
                webdoor.init();	
                rottDests = new Rotativo({conteudos:".destaques",intervalo:8000})
                rottDests.init();
                            
             
                
               

        });
        $.get("/check-files/verificar-arquivos.php",function(){});
    </script>
</head>
<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<div class="topo">
  <h1><a href="/index.php" title="SCA - ETANOL DO BRASIL" class="logo">SCA - ETANOL DO BRASIL</a></h1>
  <div class="cont">
      <a href="/contato/" title="Contato" class="contato">Contato</a>                
      <a href="/?idioma=2" title="Inglês">Inglês</a>
      <a href="/?idioma=1" title="Português">Português</a>
  </div>
  <form id="form_busca" action="/resultado-busca/">
      <div class="busca-topo"><a href="javascript:;" onclick="buscar()" title="OK">OK</a><input type="text" name="busca" value="" /></div>
  </form>
  <ul class="menu">
  	<li class="empresa">
    	<a href="/empresa/" title="Empresa">Empresa</a>
        <ul>
            <li class="op1"><a href="/empresa/trajetoria/" title="Trajetória">Trajetória</a></li>
            <li class="op2"><a href="/empresa/acoes-sustentaveis/" title="Ações Sustentáveis">Ações Sustentáveis</a></li>
            <li class="op3"><a href="/empresa/mercado-de-atuacao/" title="Mercado de Atuação">Mercado de Atuação</a></li>
        </ul>
    </li>
    <li class="servicos">
    	<a href="/servicos/" title="Serviços">Serviços</a>
        <ul>
            <li class="op1"><a href="/servicos/comercializacao/" title="Comercialização">Comercialização</a></li>
            <li class="op2"><a href="/servicos/sca-trading/" title="SCA Trading">SCA Trading</a></li>
            <li class="op3"><a href="/servicos/inteligencia-de-mercado/" title="Inteligência de Mercado">Inteligência de Mercado</a></li>
        </ul>
    </li>
    <li class="etanol">
    	<a href="/etanol/" title="Etanol">Etanol</a>
        <ul>
            <li class="op1">
            	<a href="/etanol/especificacoes/" title="Especificações">Especificações</a>
                <ul>
                    <li class="op1-1"><a href="/etanol/especificacoes/mercado-interno/" title="Mercado Interno">Mercado Interno</a></li>
                    <li class="op1-2"><a href="/etanol/especificacoes/mercado-externo/" title="Mercado Externo">Mercado Externo</a></li>
                </ul>
            </li>
            <li class="op2">
            	<a href="/etanol/certificacoes/" title="Certificações">Certificações</a>
            </li>
            <li class="op3">
            	<a href="/etanol/legislacoes/" title="Legislações">Legislações</a>
            </li>
        </ul>
    </li>
    <li class="unidade-produtora"><a href="/unidades-produtoras/" title="Unidades Produtoras">Unidades Produtoras</a></li>
    <li class="estatisticas"><a href="/estatisticas/" title="Estatísticas">Estatísticas</a></li>
    <li class="gestao-de-pessoas"><a href="/gestao-de-pessoas/" title="Gestão de Pessoas">Gestão de Pessoas</a></li>
    <li class="seja-nosso-cliente"><a title="Seja Nosso Cliente" href="/seja-nosso-cliente/">Seja Nosso Cliente</a></li>
    <li class="area-restrita"><a href="http://www.scalcool.com.br/sisweb/" onclick="return OpenOffSite(this);" title="Área Restrita">Área Restrita</a></li>
  </ul>
</div>        <div class="home">
        	<div class="esquerda" style="
    margin-top: 145px;
">
            
                <div class="noticias-destaques">
                <?php 
				$dir=@opendir(".");
				if ($dir):
				$esconde = array(".", "..", ".htaccess", "index.php", ".htuh");
				while ($lista = readdir($dir)) {
					if (!in_array($lista, $esconde))
						echo '<a style="COLOR: #000;" download="" href="http://scetanol.com.br/web/alianca_sca/' . $lista . '">' . $lista . '</a><br>';
				}
				endif;
				closedir($dir);
				?>
                    
            	</div>
            </div>
            
        </div>
      </div>
    </div>
    <div class="rodape">
    <div>
        <img src="/css/img/bg-rodape.gif" width="123" height="90" alt="" />
        <ul>
            <li><a href="/empresa/" title="Empresa">Empresa</a><strong>&bull;</strong></li>
            <li><a href="/servicos/" title="Serviços">Serviços</a><strong>&bull;</strong></li>
            <li><a href="/etanol/" title="Etanol">Etanol</a><strong>&bull;</strong></li>
            <li><a href="/unidades-produtoras/" title="Unidades Produtoras">Unidades Produtoras</a><strong>&bull;</strong></li>
            <li><a href="/estatisticas/" title="Estatísticas">Estatísticas</a><strong>&bull;</strong></li>
            <li><a href="/gestao-de-pessoas/" title="Gestão de Pessoas">Gestão de Pessoas</a><strong>&bull;</strong></li>
            <li><a href="http://www.scalcool.com.br/sisweb/" onclick="return OpenOffSite(this);" title="Área Restrita">Área Restrita</a></li>
        </ul>
        <address>
        	S&atilde;o Paulo Tel:  55 11 3709-4900
            Goi&aacute;s Tel:  55 62 3878-4900
        </address>
    </div>
</div>

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34367631-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</div>
</body>
</html>