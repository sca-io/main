<?
class Cliente{
	private $id;
	private $idTipoCliente;
	private $idioma;
	private $nome;
	private $email;
	private $mensagem;
	private $dataInclusao;
	private $status;
	public static $logSql;
	private $oldValues = Array('id'=>'','id_tipo_cliente'=>'','idioma'=>'','nome'=>'','email'=>'','mensagem'=>'','data_inclusao'=>'','status'=>'');
        
	const NAOAVALIADO = 2;
	const ATIVO = 1;
	const INATIVO = 0;

	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdTipoCliente($inIdTipoCliente){
		$this->idTipoCliente = $inIdTipoCliente;
	}

	public function getIdTipoCliente(){
		return $this->idTipoCliente;
	}
	public function getTipoCliente($campos=""){
            return TipoCliente::ler($this->idTipoCliente,$campos);
	}

	public function setIdioma($inIdioma){
		$this->idioma = $inIdioma;
	}

	public function getIdioma(){
		return $this->idioma;
	}

	public function setNome($inNome){
		$this->nome = $inNome;
	}

	public function getNome(){
		return $this->nome;
	}

	public function setEmail($inEmail){
		$this->email = $inEmail;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setMensagem($inMensagem){
		$this->mensagem = $inMensagem;
	}

	public function getMensagem(){
		return $this->mensagem;
	}

	public function setDataInclusao($inDataInclusao){
		$this->dataInclusao = $inDataInclusao;
	}

	public function getDataInclusao(){
		return $this->dataInclusao;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus($string=false){
		if($string){
			switch($this->status){
                            case self::ATIVO:
                                return "Ativo";
                            break;
                            case self::NAOAVALIADO:
                                return "Não Avaliado";
                            break;
                            default:
                                return "Inativo";
                        }

		}else{
			return $this->status;
		}
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_cliente');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['id_tipo_cliente'])) $this->idTipoCliente=$conteudo['id_tipo_cliente'];
		if(isset($conteudo['idioma'])) $this->idioma=$conteudo['idioma'];
		if(isset($conteudo['nome'])) $this->nome=$conteudo['nome'];
		if(isset($conteudo['email'])) $this->email=$conteudo['email'];
		if(isset($conteudo['mensagem'])) $this->mensagem=$conteudo['mensagem'];
		if(isset($conteudo['data_inclusao'])) $this->dataInclusao=$conteudo['data_inclusao'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		
		foreach($conteudo as $k=>$news){ $this->oldValues[$k] = $news;} 
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || $this->oldValues['id_tipo_cliente'] != $this->idTipoCliente){ $campo[] = 'id_tipo_cliente';  $valor[] = "'$this->idTipoCliente'"; }
		if(!$this->id || $this->oldValues['idioma'] != $this->idioma){ $campo[] = 'idioma';  $valor[] = "'$this->idioma'"; }
		if(!$this->id || $this->oldValues['nome'] != $this->nome){ $campo[] = 'nome';  $valor[] = "'$this->nome'"; }
		if(!$this->id || $this->oldValues['email'] != $this->email){ $campo[] = 'email';  $valor[] = "'$this->email'"; }
		if(!$this->id || $this->oldValues['mensagem'] != $this->mensagem){ $campo[] = 'mensagem';  $valor[] = "'$this->mensagem'"; }
		if(!$this->id || $this->oldValues['data_inclusao'] != $this->dataInclusao){ $campo[] = 'data_inclusao';  $valor[] = "'$this->dataInclusao'"; }
		if(!$this->id || $this->oldValues['status'] != $this->status){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		
		
		$db=new DB('tbl_cliente');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return Cliente::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_cliente');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($idTipoCliente='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_cliente');
						
		if(strlen($idTipoCliente)>0){
			if($where) $where .= " AND ";
			$where .= "id_tipo_cliente = '$idTipoCliente'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new Cliente($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = Cliente::listar("",$campos,"id = '$id'",'','1');
		return $obj[0];
	}

	public static function listarXML($idTipoCliente='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_cliente');
						
		if(strlen($idTipoCliente)>0){
			if($where) $where .= " AND ";
			$where .= "id_tipo_cliente = '$idTipoCliente'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<Cliente id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</Cliente>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($idTipoCliente='',$where=''){
		$db=new DB('tbl_cliente');
						
		if(strlen($idTipoCliente)>0){
			if($where) $where .= " AND ";
			$where .= "id_tipo_cliente = '$idTipoCliente'";	
		}
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao(SEJA_NOSSO_CLIENTE);//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao($this->nome);//observação pertinente à classe
		$hst->salvar();
	}

}
?>