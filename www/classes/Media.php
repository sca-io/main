<?
class Media{
	private $id;
	private $idConteudo;
	private $idSecao;
	private $idCategoria;
	private $nome;
	private $tipo;
	private $dir;
	private $url;
	private $legenda;
	private $credito;
	private $descricao;
	private $lang;
	private $urlLink;
	private $targetLink;
	private $modoVis;
	private $ordem;
	private $dataInsercao;
	private $dataPublicacao;
	private $status;
	public static $logSql;
	private $oldValues = Array('id'=>'','id_conteudo'=>'','id_secao'=>'','id_categoria'=>'','nome'=>'','tipo'=>'','dir'=>'','url'=>'','legenda'=>'','credito'=>'','descricao'=>'','lang'=>'','url_link'=>'','target_link'=>'','modo_vis'=>'','ordem'=>'','data_insercao'=>'','data_publicacao'=>'','status'=>'');

        //status
	const ATIVO = 1;
	const INATIVO = 0;

        //tipos
	const IMAGEM = 1;
	const VIDEO = 2;
        const DOCS = 3;
        
        //modo visualizacao 
        const PRIVADO = 0;
	const PUBLICO = 1;
	
        
	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdConteudo($inIdConteudo){
		$this->idConteudo = $inIdConteudo;
	}

	public function getIdConteudo(){
		return $this->idConteudo;
	}

	public function setIdSecao($inIdSecao){
		$this->idSecao = $inIdSecao;
	}

	public function getIdSecao(){
		return $this->idSecao;
	}

	public function setIdCategoria($inIdCategoria){
		$this->idCategoria = $inIdCategoria;
	}

	public function getIdCategoria(){
		return $this->idCategoria;
	}

	public function setNome($inNome){
		$this->nome = $inNome;
	}

	public function getNome(){
		return $this->nome;
	}

	public function setTipo($inTipo){
		$this->tipo = $inTipo;
	}

	public function getTipo(){
		return $this->tipo;
	}

	public function setDir($inDir){
		$this->dir = $inDir;
	}

	public function getDir(){
		return $this->dir;
	}

	public function setUrl($inUrl){
		$this->url = $inUrl;
	}

	public function getUrl(){
		return $this->url;
	}

	public function setLegenda($inLegenda){
		$this->legenda = $inLegenda;
	}

	public function getLegenda(){
		return $this->legenda;
	}

	public function setCredito($inCredito){
		$this->credito = $inCredito;
	}

	public function getCredito(){
		return $this->credito;
	}

	public function setDescricao($inDescricao){
		$this->descricao = $inDescricao;
	}

	public function getDescricao(){
		return $this->descricao;
	}

	public function setLang($inLang){
		$this->lang = $inLang;
	}

	public function getLang(){
		return $this->lang;
	}

	public function setUrlLink($inUrlLink){
		$this->urlLink = $inUrlLink;
	}

	public function getUrlLink(){
		return $this->urlLink;
	}

	public function setTargetLink($inTargetLink){
		$this->targetLink = $inTargetLink;
	}

	public function getTargetLink(){
		return $this->targetLink;
	}

	public function setModoVis($inModoVis){
		$this->modoVis = $inModoVis;
	}

	public function getModoVis(){
		return $this->modoVis;
	}

	public function setOrdem($inOrdem){
		$this->ordem = $inOrdem;
	}

	public function getOrdem(){
		return $this->ordem;
	}

	public function setDataInsercao($inDataInsercao){
		$this->dataInsercao = $inDataInsercao;
	}

	public function getDataInsercao(){
		return $this->dataInsercao;
	}

	public function setDataPublicacao($inDataPublicacao){
		$this->dataPublicacao = $inDataPublicacao;
	}

	public function getDataPublicacao(){
		return $this->dataPublicacao;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus(){
		return $this->status;
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_media');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['id_conteudo'])) $this->idConteudo=$conteudo['id_conteudo'];
		if(isset($conteudo['id_secao'])) $this->idSecao=$conteudo['id_secao'];
		if(isset($conteudo['id_categoria'])) $this->idCategoria=$conteudo['id_categoria'];
		if(isset($conteudo['nome'])) $this->nome=$conteudo['nome'];
		if(isset($conteudo['tipo'])) $this->tipo=$conteudo['tipo'];
		if(isset($conteudo['dir'])) $this->dir=$conteudo['dir'];
		if(isset($conteudo['url'])) $this->url=$conteudo['url'];
		if(isset($conteudo['legenda'])) $this->legenda=$conteudo['legenda'];
		if(isset($conteudo['credito'])) $this->credito=$conteudo['credito'];
		if(isset($conteudo['descricao'])) $this->descricao=$conteudo['descricao'];
		if(isset($conteudo['lang'])) $this->lang=$conteudo['lang'];
		if(isset($conteudo['url_link'])) $this->urlLink=$conteudo['url_link'];
		if(isset($conteudo['target_link'])) $this->targetLink=$conteudo['target_link'];
		if(isset($conteudo['modo_vis'])) $this->modoVis=$conteudo['modo_vis'];
		if(isset($conteudo['ordem'])) $this->ordem=$conteudo['ordem'];
		if(isset($conteudo['data_insercao'])) $this->dataInsercao=$conteudo['data_insercao'];
		if(isset($conteudo['data_publicacao'])) $this->dataPublicacao=$conteudo['data_publicacao'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		
		foreach($conteudo as $k=>$news){ $this->oldValues[$k] = $news;} 
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || $this->oldValues['id_conteudo'] != $this->idConteudo){ $campo[] = 'id_conteudo';  $valor[] = "'$this->idConteudo'"; }
		if(!$this->id || $this->oldValues['id_secao'] != $this->idSecao){ $campo[] = 'id_secao';  $valor[] = "'$this->idSecao'"; }
		if(!$this->id || $this->oldValues['id_categoria'] != $this->idCategoria){ $campo[] = 'id_categoria';  $valor[] = "'$this->idCategoria'"; }
		if(!$this->id || $this->oldValues['nome'] != $this->nome){ $campo[] = 'nome';  $valor[] = "'$this->nome'"; }
		if(!$this->id || $this->oldValues['tipo'] != $this->tipo){ $campo[] = 'tipo';  $valor[] = "'$this->tipo'"; }
		if(!$this->id || $this->oldValues['dir'] != $this->dir){ $campo[] = 'dir';  $valor[] = "'$this->dir'"; }
		if(!$this->id || $this->oldValues['url'] != $this->url){ $campo[] = 'url';  $valor[] = "'$this->url'"; }
		if(!$this->id || $this->oldValues['legenda'] != $this->legenda){ $campo[] = 'legenda';  $valor[] = "'$this->legenda'"; }
		if(!$this->id || $this->oldValues['credito'] != $this->credito){ $campo[] = 'credito';  $valor[] = "'$this->credito'"; }
		if(!$this->id || $this->oldValues['descricao'] != $this->descricao){ $campo[] = 'descricao';  $valor[] = "'$this->descricao'"; }
		if(!$this->id || $this->oldValues['lang'] != $this->lang){ $campo[] = 'lang';  $valor[] = "'$this->lang'"; }
		if(!$this->id || $this->oldValues['url_link'] != $this->urlLink){ $campo[] = 'url_link';  $valor[] = "'$this->urlLink'"; }
		if(!$this->id || $this->oldValues['target_link'] != $this->targetLink){ $campo[] = 'target_link';  $valor[] = "'$this->targetLink'"; }
		if(!$this->id || $this->oldValues['modo_vis'] != $this->modoVis){ $campo[] = 'modo_vis';  $valor[] = "'$this->modoVis'"; }
		if(!$this->id || $this->oldValues['ordem'] != $this->ordem){ $campo[] = 'ordem';  $valor[] = "'$this->ordem'"; }
		if(!$this->id || $this->oldValues['data_insercao'] != $this->dataInsercao){ $campo[] = 'data_insercao';  $valor[] = "'$this->dataInsercao'"; }
		if(!$this->id || $this->oldValues['data_publicacao'] != $this->dataPublicacao){ $campo[] = 'data_publicacao';  $valor[] = "'$this->dataPublicacao'"; }
		if(!$this->id || $this->oldValues['status'] != $this->status){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		
		
		$db=new DB('tbl_media');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return Media::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_media');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($idConteudo='',$idSecao='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_media');
						
		if(strlen($idConteudo)>0){
			if($where) $where .= " AND ";
			$where .= "id_conteudo = '$idConteudo'";	
		}				
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new Media($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = Media::listar("","",$campos,"id = '$id'",'','1');
		return isset($obj[0])?$obj[0]:null;
	}

	public static function listarXML($idConteudo='',$idSecao='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_media');
						
		if(strlen($idConteudo)>0){
			if($where) $where .= " AND ";
			$where .= "id_conteudo = '$idConteudo'";	
		}				
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<Media id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</Media>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}
        
	public static function getIdCategoriasComArqs($idConteudo='',$idSecao='',$where=''){
		$db=new DB('tbl_media');
						
		if(strlen($idConteudo)>0){
                    if($where) $where .= " AND ";
                    $where .= "id_conteudo = '$idConteudo'";	
		}				
		if(strlen($idSecao)>0){
                    if($where) $where .= " AND ";
                    $where .= "id_secao = '$idSecao'";	
		}	
                $strWhere = ($where!='')?"WHERE $where":'';

                $db->executaQuery("SELECT id_categoria , count(id_categoria) qnt FROM tbl_media $strWhere GROUP BY id_categoria");                
                self::setLogSql($db->log);
                
		$lista = array();	
		while($row = $db->fetchArray()){
                    if($row["qnt"] > 0) $lista[] = $row["id_categoria"];
		}		
		return $lista;
	}

	public static function countListar($idConteudo='',$idSecao='',$where=''){
		$db=new DB('tbl_media');
						
		if(strlen($idConteudo)>0){
			if($where) $where .= " AND ";
			$where .= "id_conteudo = '$idConteudo'";	
		}				
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao();//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao();//observação pertinente à classe
		$hst->salvar();
	}

}
?>