<?
class Cotacao{
	private $id;
	private $idDestaque;
	private $categoria;
	private $idioma;
	private $titulo;
	private $periodo;
	private $valor;
	private $perc;
	private $ordem;
	private $dataInclusao;
	private $status;
	public static $logSql;
	private $oldValues = Array('id'=>'','id_destaque'=>'','categoria'=>'','idioma'=>'','titulo'=>'','periodo'=>'','valor'=>'','perc'=>'','ordem'=>'','data_inclusao'=>'','status'=>'');

	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdDestaque($inIdDestaque){
		$this->idDestaque = $inIdDestaque;
	}

	public function getIdDestaque(){
		return $this->idDestaque;
	}

	public function setCategoria($inCategoria){
		$this->categoria = $inCategoria;
	}

	public function getCategoria(){
		return $this->categoria;
	}

	public function setIdioma($inIdioma){
		$this->idioma = $inIdioma;
	}

	public function getIdioma(){
		return $this->idioma;
	}

	public function setTitulo($inTitulo){
		$this->titulo = $inTitulo;
	}

	public function getTitulo(){
		return $this->titulo;
	}

	public function setPeriodo($inPeriodo){
		$this->periodo = $inPeriodo;
	}

	public function getPeriodo(){
		return $this->periodo;
	}

	public function setValor($inValor){
		$this->valor = $inValor;
	}

	public function getValor(){
		return $this->valor;
	}

	public function setPerc($inPerc){
		$this->perc = $inPerc;
	}

	public function getPerc(){
		return $this->perc;
	}

	public function setOrdem($inOrdem){
		$this->ordem = $inOrdem;
	}

	public function getOrdem(){
		return $this->ordem;
	}

	public function setDataInclusao($inDataInclusao){
		$this->dataInclusao = $inDataInclusao;
	}

	public function getDataInclusao(){
		return $this->dataInclusao;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus(){
		return $this->status;
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_cotacao');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['id_destaque'])) $this->idDestaque=$conteudo['id_destaque'];
		if(isset($conteudo['categoria'])) $this->categoria=$conteudo['categoria'];
		if(isset($conteudo['idioma'])) $this->idioma=$conteudo['idioma'];
		if(isset($conteudo['titulo'])) $this->titulo=$conteudo['titulo'];
		if(isset($conteudo['periodo'])) $this->periodo=$conteudo['periodo'];
		if(isset($conteudo['valor'])) $this->valor=$conteudo['valor'];
		if(isset($conteudo['perc'])) $this->perc=$conteudo['perc'];
		if(isset($conteudo['ordem'])) $this->ordem=$conteudo['ordem'];
		if(isset($conteudo['data_inclusao'])) $this->dataInclusao=$conteudo['data_inclusao'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		
		foreach($conteudo as $k=>$news){ $this->oldValues[$k] = $news;} 
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || $this->oldValues['id_destaque'] != $this->idDestaque){ $campo[] = 'id_destaque';  $valor[] = "'$this->idDestaque'"; }
		if(!$this->id || $this->oldValues['categoria'] != $this->categoria){ $campo[] = 'categoria';  $valor[] = "'$this->categoria'"; }
		if(!$this->id || $this->oldValues['idioma'] != $this->idioma){ $campo[] = 'idioma';  $valor[] = "'$this->idioma'"; }
		if(!$this->id || $this->oldValues['titulo'] != $this->titulo){ $campo[] = 'titulo';  $valor[] = "'$this->titulo'"; }
		if(!$this->id || $this->oldValues['periodo'] != $this->periodo){ $campo[] = 'periodo';  $valor[] = "'$this->periodo'"; }
		if(!$this->id || $this->oldValues['valor'] != $this->valor){ $campo[] = 'valor';  $valor[] = "'$this->valor'"; }
		if(!$this->id || $this->oldValues['perc'] != $this->perc){ $campo[] = 'perc';  $valor[] = "'$this->perc'"; }
		if(!$this->id || $this->oldValues['ordem'] != $this->ordem){ $campo[] = 'ordem';  $valor[] = "'$this->ordem'"; }
		if(!$this->id || $this->oldValues['data_inclusao'] != $this->dataInclusao){ $campo[] = 'data_inclusao';  $valor[] = "'$this->dataInclusao'"; }
		if(!$this->id || $this->oldValues['status'] != $this->status){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		
		
		$db=new DB('tbl_cotacao');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return Cotacao::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_cotacao');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($idDestaque='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_cotacao');
						
		if(strlen($idDestaque)>0){
			if($where) $where .= " AND ";
			$where .= "id_destaque = '$idDestaque'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new Cotacao($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = Cotacao::listar("",$campos,"id = '$id'",'','1');
		return $obj[0];
	}

	public static function listarXML($idDestaque='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_cotacao');
						
		if(strlen($idDestaque)>0){
			if($where) $where .= " AND ";
			$where .= "id_destaque = '$idDestaque'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<Cotacao id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</Cotacao>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($idDestaque='',$where=''){
		$db=new DB('tbl_cotacao');
						
		if(strlen($idDestaque)>0){
			if($where) $where .= " AND ";
			$where .= "id_destaque = '$idDestaque'";	
		}
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico,$obs=""){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao(HOME);//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
                $obs = $obs?$obs:$this->titulo;
		$hst->setObservacao($obs);//observação pertinente à classe
		$hst->salvar();
	}

}
?>