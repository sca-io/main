<?
#DB.class.php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);

class DB{
    public  $connexao;
    public  $result;
    private $nRegistros;
    public $db;
    public $user;
    public $password;
    public $host;
    public $pass;
    public $tabela;
    public $log;
    public $insertId;

    function __construct($tabela = '',$host = DB_HOST,$user = DB_USER, $pass = DB_PASS,$db =DATABASE){	
        $this->user = $user;
        $this->pass = $pass;
        $this->host = $host;
        $this->db = $db;
        $this->tabela = $tabela;
        $this->insertId = -1;
    }

    function abreConexao(){

        $this->conexao = mysql_pconnect($this->host, $this->user, $this->pass) or die($this->setLog("Erro na conexao"));
        mysql_select_db($this->db, $this->conexao);
        if($this->conexao){

        }else{
             $this->setLog("Erro na abertura da conexão");
        }
    }

    function nRegistros($where = '',$tabela = ''){		
		$strTabela = ($tabela!='')? $tabela :$this->tabela;
        $where = ($where == '')?$where:" WHERE $where";
        $this->executaQuery("SELECT count(*) as c FROM $strTabela $where");
		if(isset($this->result) && $this->result){
			$this->nRegistros = mysql_fetch_assoc($this->result);
			return $this->nRegistros['c'];
		}else{
			return 0;
		}        
    }

    function select($campos = '*', $where ='', $orderBy = '',$limit = "",$tabela = ''){
        $limit = ($limit!="")?" LIMIT $limit ":"";
        $campos = ($campos!="")?" $campos ":"*";
        $strTabela = ($tabela!='')? $tabela :$this->tabela;
        if($strTabela!=''){
                $strWhere = ($where!='')?"WHERE $where":'';
                $strorderBy = ($orderBy!='')?"ORDER BY $orderBy":'';
                $this->executaQuery("SELECT $campos FROM $strTabela $strWhere $strorderBy $limit");
        }else{
                $this->setLog("Defina uma tabela a ser inserida");
        }
    }

    function update($campos, $valores, $where,$tabela =''){
        $strTabela = ($tabela!='')? $tabela :$this->tabela;
        if($strTabela!=''){

            if(!is_array($campos) && !is_array($valores)){                
                $arrCampos = explode(',',$campos);
                $arrValores = explode(',',$valores);
              }else{
                  $arrCampos = $campos;
                  $arrValores = $valores;
              }

            if(count($arrCampos) == count($arrValores)){
                    $strSet  = '';
                    for($i = 0 ;$i < count($arrCampos);$i++){
                             $arrValores[$i] = ($arrValores[$i] == "''")? 'DEFAULT' : $arrValores[$i];

                            if( $i == (count($arrCampos)-1 ) ){
                                    $strSet.=" $arrCampos[$i] = $arrValores[$i] ";
                                    break;
                            }else{
								//if(strlen($arrValores[$i]) > 2){
                                    $strSet.=" $arrCampos[$i] = $arrValores[$i], ";
								//}
                            }
                    }
                    $strWhere = ($where!='')?"":'';
                    $this->executaQuery("UPDATE $strTabela SET $strSet  WHERE $where ");
            }else{
                    $this->setLog("O nãmero de campos a serem alterados não são iguais aos declarados");
            }
        }else{
                $this->setLog("Defina uma tabela a ser inserida");
        }
    }

    function insert($campos, $valores, $tabela =''){
        $strTabela = ($tabela!='')? $tabela :$this->tabela;
        if($strTabela!=''){

            if(!is_array($campos) && !is_array($valores)){
                $arrCampos = explode(',',$campos);
                $arrValores = explode(',',$valores);
              }else{
                  $arrCampos = $campos;
                  $arrValores = $valores;
                  
              }

                if(count($arrCampos) == count($arrValores)){
                        $strSet  = '';
                        for($i = 0 ;$i < count($arrCampos);$i++){
                            
                            $arrValores[$i] = ($arrValores[$i] == "''")? 'DEFAULT' : $arrValores[$i];
                            
                            if( $i == (count($arrCampos)-1 ) ){
                                    $strSet.=" $arrCampos[$i] = $arrValores[$i] ";
                                    break;
                            }else{
                                    $strSet.=" $arrCampos[$i] = $arrValores[$i], ";
                            }
                        }

                        $this->abreConexao();
                        $this->executaQuery("INSERT $strTabela SET $strSet ",false);
                        $this->insertId = mysql_insert_id();
                        $this->fechaConexao();
                        return $this->insertId;
                }else{
                        $this->setLog("O numero de campos a serem alterados não são iguais aos declarados");
                }
        }else{
                $this->setLog("Defina uma tabela a ser inserida");
        }
    }

    function delete($where, $tabela =''){
            $strTabela = ($tabela!='')? $tabela :$this->tabela;
            if($strTabela!=''){
                    $sql = "DELETE FROM $strTabela WHERE $where ";
                    $this->executaQuery($sql);
            }else{
                $this->setLog("Defina uma tabela a ser inserida");
            }
			$this->executaQuery("REPAIR TABLE $strTabela");			
    }

    function setLog($_str){
            $this->log .= "<p>".date("d/m/Y H:i:s")." - $_str </p>";
    }
    function getLog($file = 'log',$opcao = 'a+'){
			Util::gravaConteudo($file.'.html',"<hr>".$this->log,$opcao);
    }
	function printLog(){
		print $this->log;
	}

    function executaQuery($_sql, $fecharConexao = true){
		$this->setLog($_sql);
		//print $_sql;
        if( $fecharConexao == true){
                $this->abreConexao();
                $this->result = mysql_query("SET NAMES 'utf8'");
                $this->result = mysql_query($_sql);
                $this->setLog(mysql_error());
                $this->fechaConexao();
                return $this->result;
        }else{
            $this->result = mysql_query("SET NAMES 'utf8'");
                $this->result = mysql_query($_sql);
                return $this->result;
        }
    }
	
	
	public function fetchArray($result=""){
		if(isset($result) && $result){
			return mysql_fetch_array($result);
		}else{
			if(isset($this->result) && $this->result){
				return mysql_fetch_array($this->result);
			}else{
				$this->setLog(mysql_error());
			}
		}
	}
	
	public function fetchAssoc($result=""){
		if(isset($result) && $result){
			return mysql_fetch_assoc($result);		
		}else{
			if(isset($this->result) && $this->result){
				return mysql_fetch_assoc($this->result);
			}else{
				$this->setLog(mysql_error());
			}
		}
	}
	
	public function join($tipoJoin, $tabela1, $tabela2, $strComparacao,$campos='*',$where='',$orderBy='',$limit=''){
		$strWhere = ($where!='')?"WHERE $where":'';
		$strorderBy = ($orderBy!='')?"ORDER BY $orderBy":'';
		$limit = ($limit!="")?" LIMIT $limit ":"";
		//$this->setLog("SELECT $campos from $tabela1 $tipoJoin join $tabela2 on $strComparacao $strWhere $strorderBy $limit");
		$this->executaQuery("SELECT $campos from $tabela1 $tipoJoin join $tabela2 on $strComparacao $strWhere $strorderBy $limit");	
	}
	
    function fechaConexao(){
       mysql_close($this->conexao);
       $this->setLog("Conexão fechada com sucesso !");
    }

}

?>
