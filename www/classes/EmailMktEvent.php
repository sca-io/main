<?
class EmailMktEvent{
	private $id;
	private $idEmail;
	private $tipo;
	private $data;
	public static $logSql;
	private $oldValues = Array('id'=>'','id_email'=>'','tipo'=>'','data'=>'');
	public static $tableName = "tbl_email_mkt_event";

	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdEmail($inIdEmail){
		$this->idEmail = $inIdEmail;
	}

	public function getIdEmail(){
		return $this->idEmail;
	}

	public function setTipo($inTipo){
		$this->tipo = $inTipo;
	}

	public function getTipo(){
		return $this->tipo;
	}

	public function setData($inData){
		$this->data = $inData;
	}

	public function getData(){
		return $this->data;
	}

	public function __construct($conteudo=''){
		$this->db=new DB(self::$tableName);
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['id_email'])) $this->idEmail=$conteudo['id_email'];
		if(isset($conteudo['tipo'])) $this->tipo=$conteudo['tipo'];
		if(isset($conteudo['data'])) $this->data=$conteudo['data'];
		
		foreach($conteudo as $k=>$news){ $this->oldValues[$k] = $news;} 
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || $this->oldValues['id_email'] != $this->idEmail){ $campo[] = 'id_email';  $valor[] = "'$this->idEmail'"; }
		if(!$this->id || $this->oldValues['tipo'] != $this->tipo){ $campo[] = 'tipo';  $valor[] = "'$this->tipo'"; }
		if(!$this->id || $this->oldValues['data'] != $this->data){ $campo[] = 'data';  $valor[] = "'$this->data'"; }
		
		
		$db=new DB(self::$tableName);
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return EmailMktEvent::delete($this->id);
	}

	public static function delete($id){
		$db=new DB(self::$tableName);
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($idEmail='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB(self::$tableName);
						
		if(strlen($idEmail)>0){
			if($where) $where .= " AND ";
			$where .= "id_email = '$idEmail'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new EmailMktEvent($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = EmailMktEvent::listar("",$campos,"id = '$id'",'','1');
		return ((isset($obj[0]))?$obj[0]:false);
	}

	public static function listarXML($idEmail='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB(self::$tableName);
						
		if(strlen($idEmail)>0){
			if($where) $where .= " AND ";
			$where .= "id_email = '$idEmail'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<EmailMktEvent id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</EmailMktEvent>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($idEmail='',$where=''){
		$db=new DB(self::$tableName);
						
		if(strlen($idEmail)>0){
			if($where) $where .= " AND ";
			$where .= "id_email = '$idEmail'";	
		}
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao();//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao();//observação pertinente à classe
		$hst->salvar();
	}

}
?>