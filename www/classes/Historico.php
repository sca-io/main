<?
class Historico{
	private $id;
	private $status;
	private $idSecao;
	private $idConteudo;
	private $idUsuario;
	private $idAcaoHistorico;
	private $data;
	private $ip;
	private $observacao;
	public static $logSql;
	private $oldValues = Array();

	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setIdSecao($inIdSecao){
		$this->idSecao = $inIdSecao;
	}

	public function getIdSecao(){
		return $this->idSecao;
	}

	public function setIdConteudo($inIdConteudo){
		$this->idConteudo = $inIdConteudo;
	}

	public function getIdConteudo(){
		return $this->idConteudo;
	}

	public function setIdUsuario($inIdUsuario){
		$this->idUsuario = $inIdUsuario;
	}

	public function getIdUsuario(){
		return $this->idUsuario;
	}

	public function setIdAcaoHistorico($inIdAcaoHistorico){
		$this->idAcaoHistorico = $inIdAcaoHistorico;
	}

	public function getIdAcaoHistorico(){
		return $this->idAcaoHistorico;
	}

	public function setData($inData){
		$this->data = $inData;
	}

	public function getData(){
		return $this->data;
	}

	public function setIp($inIp){
		$this->ip = $inIp;
	}

	public function getIp(){
		return $this->ip;
	}

	public function setObservacao($inObservacao){
		$this->observacao = $inObservacao;
	}

	public function getObservacao(){
		return $this->observacao;
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_historico');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		if(isset($conteudo['id_secao'])) $this->idSecao=$conteudo['id_secao'];
		if(isset($conteudo['id_conteudo'])) $this->idConteudo=$conteudo['id_conteudo'];
		if(isset($conteudo['id_usuario'])) $this->idUsuario=$conteudo['id_usuario'];
		if(isset($conteudo['id_acao_historico'])) $this->idAcaoHistorico=$conteudo['id_acao_historico'];
		if(isset($conteudo['data'])) $this->data=$conteudo['data'];
		if(isset($conteudo['ip'])) $this->ip=$conteudo['ip'];
		if(isset($conteudo['observacao'])) $this->observacao=$conteudo['observacao'];
		
		$this->oldValues = $conteudo;
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || (isset($this->oldValues['status']) && $this->oldValues['status'] != $this->status)){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		if(!$this->id || (isset($this->oldValues['id_secao']) && $this->oldValues['id_secao'] != $this->idSecao)){ $campo[] = 'id_secao';  $valor[] = "'$this->idSecao'"; }
		if(!$this->id || (isset($this->oldValues['id_conteudo']) && $this->oldValues['id_conteudo'] != $this->idConteudo)){ $campo[] = 'id_conteudo';  $valor[] = "'$this->idConteudo'"; }
		if(!$this->id || (isset($this->oldValues['id_usuario']) && $this->oldValues['id_usuario'] != $this->idUsuario)){ $campo[] = 'id_usuario';  $valor[] = "'$this->idUsuario'"; }
		if(!$this->id || (isset($this->oldValues['id_acao_historico']) && $this->oldValues['id_acao_historico'] != $this->idAcaoHistorico)){ $campo[] = 'id_acao_historico';  $valor[] = "'$this->idAcaoHistorico'"; }
		if(!$this->id || (isset($this->oldValues['data']) && $this->oldValues['data'] != $this->data)){ $campo[] = 'data';  $valor[] = "'$this->data'"; }
		if(!$this->id || (isset($this->oldValues['ip']) && $this->oldValues['ip'] != $this->ip)){ $campo[] = 'ip';  $valor[] = "'$this->ip'"; }
		if(!$this->id || (isset($this->oldValues['observacao']) && $this->oldValues['observacao'] != $this->observacao)){ $campo[] = 'observacao';  $valor[] = "'$this->observacao'"; }
		
		
		$db=new DB('tbl_historico');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return Historico::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_historico');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($idSecao='',$idConteudo='',$idUsuario='',$idAcaoHistorico='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_historico');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		if(strlen($idConteudo)>0){
			if($where) $where .= " AND ";
			$where .= "id_conteudo = '$idConteudo'";	
		}				
		if(strlen($idUsuario)>0){
			if($where) $where .= " AND ";
			$where .= "id_usuario = '$idUsuario'";	
		}				
		if(strlen($idAcaoHistorico)>0){
			if($where) $where .= " AND ";
			$where .= "id_acao_historico = '$idAcaoHistorico'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new Historico($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = Historico::listar("","","","",$campos,"id = '$id'",'','1');
		return isset($obj[0])?$obj[0]:null;
	}

	public static function listarXML($idSecao='',$idConteudo='',$idUsuario='',$idAcaoHistorico='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_historico');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		if(strlen($idConteudo)>0){
			if($where) $where .= " AND ";
			$where .= "id_conteudo = '$idConteudo'";	
		}				
		if(strlen($idUsuario)>0){
			if($where) $where .= " AND ";
			$where .= "id_usuario = '$idUsuario'";	
		}				
		if(strlen($idAcaoHistorico)>0){
			if($where) $where .= " AND ";
			$where .= "id_acao_historico = '$idAcaoHistorico'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<Historico id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</Historico>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($idSecao='',$idConteudo='',$idUsuario='',$idAcaoHistorico='',$where=''){
		$db=new DB('tbl_historico');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		if(strlen($idConteudo)>0){
			if($where) $where .= " AND ";
			$where .= "id_conteudo = '$idConteudo'";	
		}				
		if(strlen($idUsuario)>0){
			if($where) $where .= " AND ";
			$where .= "id_usuario = '$idUsuario'";	
		}				
		if(strlen($idAcaoHistorico)>0){
			if($where) $where .= " AND ";
			$where .= "id_acao_historico = '$idAcaoHistorico'";	
		}
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	function getUsuario(){
	   return Usuario::ler($this->idUsuario);
	}
	
	function getObjAcao(){
		 return AcaoHistorico::ler($this->idAcaoHistorico);
	}

}
?>