<?
class Secao{
	private $id;
	private $idSecao;
	private $nome;
	private $nomeIng;
	private $status;
	private $temTopo;
	private $tipo;
	private $temMenu;
	public static $logSql;
	private $oldValues = Array();
	
        ///Tipos ///
        const CONTEUDO_UNICO = 1;
        const LISTA_DE_CHAMADAS = 2;
        const HOME = 3;
        const LISTA_DE_CONTEUDOS = 4;
        const CADASTRO = 5;
        const LISTA_ARQUIVOS = 6;
        
        /// Status ///
        const SALVO = 2;
	const ATIVO = 1;
	const INATIVO = 0;

	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdSecao($inIdSecao){
		$this->idSecao = $inIdSecao;
	}

	public function getIdSecao(){
		return $this->idSecao;
	}

	public function setNome($inNome){
		$this->nome = $inNome;
	}

	public function getNome(){
		return $this->nome;
	}

	public function setNomeIng($inNomeIng){
		$this->nomeIng = $inNomeIng;
	}

	public function getNomeIng(){
		return $this->nomeIng;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setTemTopo($inTemTopo){
		$this->temTopo = $inTemTopo;
	}

	public function getTemTopo(){
		return $this->temTopo;
	}

	public function setTipo($inTipo){
		$this->tipo = $inTipo;
	}

	public function getTipo(){
		return $this->tipo;
	}

	public function setTemMenu($inTemMenu){
		$this->temMenu = $inTemMenu;
	}

	public function getTemMenu(){
		return $this->temMenu;
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_secao');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['id_secao'])) $this->idSecao=$conteudo['id_secao'];
		if(isset($conteudo['nome'])) $this->nome=$conteudo['nome'];
		if(isset($conteudo['nome_ing'])) $this->nomeIng=$conteudo['nome_ing'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		if(isset($conteudo['tem_topo'])) $this->temTopo=$conteudo['tem_topo'];
		if(isset($conteudo['tipo'])) $this->tipo=$conteudo['tipo'];
		if(isset($conteudo['tem_menu'])) $this->temMenu=$conteudo['tem_menu'];
		
		$this->oldValues = $conteudo;
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || (isset($this->oldValues['id_secao']) && $this->oldValues['id_secao'] != $this->idSecao)){ $campo[] = 'id_secao';  $valor[] = "'$this->idSecao'"; }
		if(!$this->id || (isset($this->oldValues['nome']) && $this->oldValues['nome'] != $this->nome)){ $campo[] = 'nome';  $valor[] = "'$this->nome'"; }
		if(!$this->id || (isset($this->oldValues['nome_ing']) && $this->oldValues['nome_ing'] != $this->nomeIng)){ $campo[] = 'nome_ing';  $valor[] = "'$this->nomeIng'"; }
		if(!$this->id || (isset($this->oldValues['status']) && $this->oldValues['status'] != $this->status)){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		if(!$this->id || (isset($this->oldValues['tem_topo']) && $this->oldValues['tem_topo'] != $this->temTopo)){ $campo[] = 'tem_topo';  $valor[] = "'$this->temTopo'"; }
		if(!$this->id || (isset($this->oldValues['tipo']) && $this->oldValues['tipo'] != $this->tipo)){ $campo[] = 'tipo';  $valor[] = "'$this->tipo'"; }
		if(!$this->id || (isset($this->oldValues['tem_menu']) && $this->oldValues['tem_menu'] != $this->temMenu)){ $campo[] = 'tem_menu';  $valor[] = "'$this->temMenu'"; }
		
		
		$db=new DB('tbl_secao');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return Secao::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_secao');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($idSecao='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_secao');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new Secao($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = Secao::listar("",$campos,"id = '$id'",'','1');
		return isset($obj[0])?$obj[0]:null;
	}

	public static function listarXML($idSecao='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_secao');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<Secao id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</Secao>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($idSecao='',$where=''){
		$db=new DB('tbl_secao');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		/*$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao();//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao();//observação pertinente à classe
		$hst->salvar();*/
	}

}
?>