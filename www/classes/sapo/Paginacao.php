<?
class Paginacao{
        public $html;
	public $totalRegistros;
	public $pagina;
	public $totalRegPag;
	public $totalPaginas;
	public $url;
	
	function __construct($pagina='',$total='',$totalRegPag=''){
		$this->pagina = ($pagina)?$pagina:0;
		$this->totalRegistros = $total;
		$this->totalRegPag = ($totalRegPag)?$totalRegPag:10;
		$this->totalPaginas = $this->totalRegistros/$this->totalRegPag;		
	}
	function setUrl($str){
		$this->url = $str;
	}	
	
	function definirHtml(){
		$aux_pagina = $this->pagina - 1;
		$variacao = 9;
		if($this->pagina != 0){
			$this->html .= "<a href=\"?pagina=".$aux_pagina.$this->url."\" class='prox-ant'> &lt;&lt; Anterior </a>\n";
		}	
		if($this->totalPaginas > 1){
			if($this->pagina >= -$variacao ){
				for($i = 0; $i<$this->totalPaginas; $i++){
					if($i==$this->pagina){
							$this->html .= "<a class='active'>".($i+1)."</a>\n";
					}else if($i != $this->pagina && $i <= ($this->pagina + $variacao) && $i >= ($this->pagina - $variacao)){
							 $this->html .= "<a href=\"?pagina=".$i.$this->url."\">".($i+1)."</a>\n";
				
					}
				}
			}
		}
		
		$aux_pagina = $this->pagina + 1;
		if($this->pagina < ($this->totalPaginas - 1)){
			$this->html .= "<a  href=\"?pagina=".$aux_pagina.$this->url."\"  class='prox-ant'> Pr&oacute;ximo &gt;&gt; </a>\n";
		
		}

	}
	
	function printHtml(){
		$this->definirHtml();
		print $this->html;
	}
}
?>