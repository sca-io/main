<?
class SapoControleEspelhamento{
	private $id;
	private $numArqVrf;
	private $numArqOk;
	private $numArqErr;
	private $dataVrf;
	private $acao;
	private $status;
	private $idUsuario;
	private $ip;
	public static $logSql;
	private $oldValues = Array('id'=>'','num_arq_vrf'=>'','num_arq_ok'=>'','num_arq_err'=>'','data_vrf'=>'','acao'=>'','status'=>'','id_usuario'=>'','ip'=>'');

	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setNumArqVrf($inNumArqVrf){
		$this->numArqVrf = $inNumArqVrf;
	}

	public function getNumArqVrf(){
		return $this->numArqVrf;
	}

	public function setNumArqOk($inNumArqOk){
		$this->numArqOk = $inNumArqOk;
	}

	public function getNumArqOk(){
		return $this->numArqOk;
	}

	public function setNumArqErr($inNumArqErr){
		$this->numArqErr = $inNumArqErr;
	}

	public function getNumArqErr(){
		return $this->numArqErr;
	}

	public function setDataVrf($inDataVrf){
		$this->dataVrf = $inDataVrf;
	}

	public function getDataVrf(){
		return $this->dataVrf;
	}

	public function setAcao($inAcao){
		$this->acao = $inAcao;
	}

	public function getAcao(){
		return $this->acao;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setIdUsuario($inIdUsuario){
		$this->idUsuario = $inIdUsuario;
	}

	public function getIdUsuario(){
		return $this->idUsuario;
	}

	public function setIp($inIp){
		$this->ip = $inIp;
	}

	public function getIp(){
		return $this->ip;
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_sapo_controle_espelhamento');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['num_arq_vrf'])) $this->numArqVrf=$conteudo['num_arq_vrf'];
		if(isset($conteudo['num_arq_ok'])) $this->numArqOk=$conteudo['num_arq_ok'];
		if(isset($conteudo['num_arq_err'])) $this->numArqErr=$conteudo['num_arq_err'];
		if(isset($conteudo['data_vrf'])) $this->dataVrf=$conteudo['data_vrf'];
		if(isset($conteudo['acao'])) $this->acao=$conteudo['acao'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		if(isset($conteudo['id_usuario'])) $this->idUsuario=$conteudo['id_usuario'];
		if(isset($conteudo['ip'])) $this->ip=$conteudo['ip'];
		
		foreach($conteudo as $k=>$news){ $this->oldValues[$k] = $news;} 
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || $this->oldValues['num_arq_vrf'] != $this->numArqVrf){ $campo[] = 'num_arq_vrf';  $valor[] = "'$this->numArqVrf'"; }
		if(!$this->id || $this->oldValues['num_arq_ok'] != $this->numArqOk){ $campo[] = 'num_arq_ok';  $valor[] = "'$this->numArqOk'"; }
		if(!$this->id || $this->oldValues['num_arq_err'] != $this->numArqErr){ $campo[] = 'num_arq_err';  $valor[] = "'$this->numArqErr'"; }
		if(!$this->id || $this->oldValues['data_vrf'] != $this->dataVrf){ $campo[] = 'data_vrf';  $valor[] = "'$this->dataVrf'"; }
		if(!$this->id || $this->oldValues['acao'] != $this->acao){ $campo[] = 'acao';  $valor[] = "'$this->acao'"; }
		if(!$this->id || $this->oldValues['status'] != $this->status){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		if(!$this->id || $this->oldValues['id_usuario'] != $this->idUsuario){ $campo[] = 'id_usuario';  $valor[] = "'$this->idUsuario'"; }
		if(!$this->id || $this->oldValues['ip'] != $this->ip){ $campo[] = 'ip';  $valor[] = "'$this->ip'"; }
		
		
		$db=new DB('tbl_sapo_controle_espelhamento');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return SapoControleEspelhamento::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_sapo_controle_espelhamento');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($idUsuario='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_sapo_controle_espelhamento');
						
		if(strlen($idUsuario)>0){
			if($where) $where .= " AND ";
			$where .= "id_usuario = '$idUsuario'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new SapoControleEspelhamento($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = SapoControleEspelhamento::listar("",$campos,"id = '$id'",'','1');
		return $obj[0];
	}

	public static function listarXML($idUsuario='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_sapo_controle_espelhamento');
						
		if(strlen($idUsuario)>0){
			if($where) $where .= " AND ";
			$where .= "id_usuario = '$idUsuario'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<SapoControleEspelhamento id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</SapoControleEspelhamento>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($idUsuario='',$where=''){
		$db=new DB('tbl_sapo_controle_espelhamento');
						
		if(strlen($idUsuario)>0){
			if($where) $where .= " AND ";
			$where .= "id_usuario = '$idUsuario'";	
		}
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao();//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao();//observação pertinente à classe
		$hst->salvar();
	}
        
        public static function Controle_Espelhar($mostrarJS=true){
            global $ip;
            
            $pasta = $_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ."*";
            
            $lstSCE = SapoControleEspelhamento::listar('','','status = '.NAOAVALIADO,'id DESC','1');
            if($lstSCE && $lstSCE[0]){
                $sce = $lstSCE[0];
                
                $NumArqErr = $sce->getNumArqErr();
                $NumArqOk = $sce->getNumArqOk();
                $NumArqVrf = $sce->getNumArqVrf();
                
                //$idSCE = secureResponse($sce->getId());
                $idSCE = $sce->getId();
       
                $nr = SapoEspelhamento::countFiles($pasta);
                $objSE = SapoEspelhamento::seekAndStore($idSCE,$pasta,$NumArqVrf);
                if($objSE!="EOF"){
                    $nomeArq = "...";

                    $porcentagem =  ($NumArqVrf * 100) / $nr;
                    $porcentagem = (int)$porcentagem;
                    if($porcentagem>100) $porcentagem = 100;

                    $prcBar = ($porcentagem * 439) / 100;
                    $prcBar = (int)$prcBar;
                    if($prcBar>439) $prcBar = 439;     

                    $NumArqVrf++;
                    $sce->setNumArqVrf($NumArqVrf);
                    
                    if(is_object($objSE)){
                        $nomeArq = $objSE->getFPath().".".$objSE->getFExt();
                        $NumArqOk++;
                    }elseif(is_string($objSE)){
                        $nomeArq = $objSE;
                        $NumArqErr++;                                 
                    }

                    $sce->setNumArqErr($NumArqErr);
                    $sce->setNumArqOk($NumArqOk);

                    $sce->salvar();
                    if($mostrarJS){
                        ?>
                        <script src='js/jquery-1.3.2.min.js'></script>
                        <script>
                            $(window).ready(function(){
                                parent.setWidthProgressBar(<?=$prcBar?>);                                
                                parent.$("#prc").html('<?=$porcentagem?>%');
                                parent.$("#pcArq").html('<?=$nomeArq?>');
                                parent.$("#nR").html('<?=$nr?>/<?=$NumArqVrf?>');
                                parent.$("#diffArq").html('<?=$NumArqErr?>');
                                window.location = "crtlEspelhamento.php";
                            });
                        </script>                    
                        <?
                    }
                }else{
                    $sce->setStatus(ATIVO);    
                    $sce->salvar();
                    if($mostrarJS){
                        ?>
                        <script src='js/jquery-1.3.2.min.js'></script>
                        <script>   
                            $(window).ready(function(){
                                parent.paraCronometro();
                                parent.setWidthProgressBar(439);                                
                                parent.$("#prc").html('100%');
                                parent.$("#pcArq").html('');
                                parent.$("#btConcluir").fadeIn().attr("href","javascript:concluir();");
                                parent.$("#retorno").html('Espelhamento concluído com sucesso. Ver logs para mais detalhes.');                                
                            });
                        </script>
                        <?
                    }
                }                
                    
            }else{  
                                   
                $sce = new SapoControleEspelhamento();  
                $sce->setNumArqVrf(0);//SapoEspelhamento::countFiles($pasta)
                $sce->setNumArqOk(0);
                $sce->setNumArqErr(0);
                $sce->setDataVrf(date('Y-m-d H:i:s'));
                $sce->setStatus(NAOAVALIADO);
                $sce->setIdUsuario(getSession('idUsuarioSapo'));
                $sce->setIp($ip);
                $sce->salvar();
                
                if($mostrarJS){
                    ?>
                    <script>
                        if(parent.timer==null){
                            parent.cronometro();
                        }
                        window.location = "crtlEspelhamento.php";
                    </script>
                    <? 
                }
            }
        }
        
        public static function Controle_Verificar($mostrarJS=true,$acao='',$idSCE=''){
            /*global $ip;
            
            $pasta = $_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ."*";
            switch($acao){
                
                case "Verificar":
                    if($idSCE){
                                                
                        $sce = SapoControleEspelhamento::ler($idSCE);
                        $sce->setNumArqVrf(0);
                        $sce->setNumArqErr(0);
                        $sce->setNumArqOk(0);
                        $sce->setStatus(NAOAVALIADO);   
                        $sce->setIdUsuario(getSession('idUsuarioSapo'));
                        $sce->setIp($ip);
                        $sce->salvar();
                            
                        $idSCE = secureResponse($sce->getId());
                        ?>
                        <script>
                            if(parent.timer==null){
                                parent.cronometro();
                            }
                            window.location = "crtlEspelhamento.php?acao=Verificando&idSCE=<?=$idSCE?>";
                        </script>
                        <?
                    }else{
                        ?>
                        <script>
                            parent.alerta('Erro inesperado!',function(){ parent.window.location = "index.php" });
                        </script>
                        <?
                    }
                    break;
                case "Verificando":
                    if($idSCE){
                        $nr = SapoEspelhamento::countListar();
                        
                        $sce = SapoControleEspelhamento::ler($idSCE);
                                                    
                        $NumArqErr = $sce->getNumArqErr();
                        $NumArqOk = $sce->getNumArqOk();
                        $NumArqVrf = $sce->getNumArqVrf();
                                               
                        $objSE = SapoEspelhamento::fileCompair($idSCE,$pasta,$NumArqVrf);
                        if($NumArqVrf<=$nr){
                            $nomeArq = "...";

                            $porcentagem =  ($NumArqVrf * 100) / $nr;
                            $porcentagem = (int)$porcentagem;
                            if($porcentagem>100) $porcentagem = 100;

                            $prcBar = ($porcentagem * 439) / 100;
                            $prcBar = (int)$prcBar;
                            if($prcBar>439) $prcBar = 439;     

                            $NumArqVrf++;
                            $sce->setNumArqVrf($NumArqVrf);
                            $idSCE = secureResponse($sce->getId());

                            if(is_object($objSE)){
                                $nomeArq = $objSE->getFPath().".".$objSE->getFExt();
                                $NumArqOk++;
                            }elseif(is_string($objSE)){
                                $nomeArq = $objSE;
                                $NumArqErr++;                                 
                            }

                            $sce->setNumArqErr($NumArqErr);
                            $sce->setNumArqOk($NumArqOk);

                            $sce->salvar();
                            ?>
                            <script src='js/jquery-1.3.2.min.js'></script>
                            <script>   
                                $(window).ready(function(){
                                    parent.setWidthProgressBar(<?=$prcBar?>);                                
                                    parent.$("#prc").html('<?=$porcentagem?>%');
                                    parent.$("#pcArq").html('<?=$nomeArq?>');
                                    parent.$("#nR").html('<?=$nr?>/<?=$NumArqVrf?>');
                                    parent.$("#diffArq").html('<?=$NumArqErr?>');
                                    window.location = "crtlEspelhamento.php?acao=Verificando&idSCE=<?=$idSCE?>";
                                });
                            </script>
                            <?
                        }else{
                            $sce->setStatus(ATIVO);    
                            $sce->salvar();
                            ?>
                            <script src='js/jquery-1.3.2.min.js'></script>
                            <script>   
                                $(window).ready(function(){       
                                    parent.paraCronometro();
                                    parent.$("#pcArq").html('');
                                    parent.$("#btConcluir").fadeIn().attr("href","javascript:concluir();");
                                    parent.$("#retorno").html('Verificação concluída com sucesso. Ver logs para mais detalhes.');                                
                                });
                            </script>
                            <?
                        }
                    }else{
                        ?><meta http-equiv="refresh" content="5; URL=http://<?=$_SERVER['SERVER_NAME']?><?=DIRETORIO_RAIZ?>sapo/crtlEspelhamento.php?acao=Verificar" /><?
                    }
                    break;
                default:
                    break;
            }*/
        }
}
?>