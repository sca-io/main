<?php 
class Util{
	public static function getAcoesLista($id,$idSecao,$excluir,$editar,$vizualizar,$extra=''){
		$acoes = "<li>";
		if($editar){
			$acoes .="<a href=\"javascript:deletar('".secureResponse($id)."',".$idSecao.")\" title='Excluir Item' class=\"bt_fechar\">Excluir</a>";
		}
		if($excluir){
			$acoes .= "<a href=\"".getUrl('edicao_',$idSecao)."?id=".$id.$extra."\" class=\"bt_editar\">editar</a>";
		}
		if($vizualizar){
			$acoes .= "<a href=\"".getUrl('vizualizar_',$idSecao)."?id=".$id.$extra."\" class=\"bt_visualizar2\">Vizualizar</a>";
		}
		$acoes .= "</li>";
		return $acoes;
	}
	
	
	public static function ordenar($direcao,$objT,$lista,$count=''){
		if($direcao == "cima"){
			if($objT->getOrdem()>=0){
				if($lista){
					foreach($lista as $l){
						$obj2 = $l->getOrdem();
						$objA = $objT->getOrdem();
						$objT->setOrdem($obj2);
						$l->setOrdem($objA);
						$objT->salvar();
						$l->salvar();
						return 'true';
					}
				}else{
					return 'false';
				}
			}else{
				return 'false';
			}
		}elseif($direcao == "baixo"){
			if($objT->getOrdem()<$count){
				if($lista){
					foreach($lista as $l){
						$obj2 = $l->getOrdem();
						$objA = $objT->getOrdem();
						$objT->setOrdem($obj2);
						$l->setOrdem($objA);
						$objT->salvar();
						$l->salvar();
						return 'true';
					}
				}else{
					return 'false';
				}
			}else{
				return 'false';
			}
		}
	}
	
	public static function dataParaBD($data){
		$dataNacs = explode("/",$data);	
		return $dataNacs[2]."-".$dataNacs[1]."-".$dataNacs[0];
	}
	public static function dataDoBD($data){
		$resultData = new DateTime($data);
		$strData = date_format($resultData,"d/m/Y");
		return $strData;
	}
	
	public static function dataDoBDcomHora($data,$format="d/m/Y \à\s H:i:s"){
		$resultData = new DateTime($data);
		$strData = date_format($resultData,$format);
		return $strData;
	}

		
	public static function geraAno($ano,$umAnoAmais=true){
		/*if($umAnoAmais)
			$anoF = date("Y")+1;
		else*/
		$anoF = date("Y");
		for($i=$anoF;$i>=1950;$i--){
			$selected = ($ano == $i)?'selected="selected"':'';
			print "<option $selected value='".$i."'>".$i."</option>";
		}
	}
	
	public static function geraMes($mes){
		for($i = 1; $i <= 12; $i++){
			$selected = ($i == $mes)?"selected=true":'';
			print "<option $selected value='$i'>".$i."</option>\n";
		}
	}
	
	public static function geraDia($dia){
		for($i = 1; $i <= 31; $i++){
			$selected = ($i == $dia)?"selected=true":'';
			print "<option $selected value='$i'>".$i."</option>\n";
		}
	}
	
	public static function geraAnoA($href,$umAnoAmais=true){
		/*if($umAnoAmais)
			$anoF = date("Y")+1;
		else*/
		$anoF = date("Y");
		for($i=$anoF;$i>=1950;$i--){
			print "<a ".str_replace("#ANO#",$i,$href).">$i</a>\n";
		}
	}
	
	public static function geraMesA($href){
		for($i = 1; $i <= 12; $i++){
			print "<a ".str_replace("#MES#",$i,$href).">$i</a>\n";
		}
	}
	
	public static function geraDiaA($href){
		for($i = 1; $i <= 31; $i++){
			print "<a ".str_replace("#DIA#",$i,$href).">$i</a>\n";
		}
	}
	
	public static function getIdade($data_nasc){
		if($data_nasc){
			$data = explode("-",$data_nasc);	
			$dia_atual = date("d");
			$mes_atual = date("m");
			$ano_atual = date("Y");
			$idade = $ano_atual - $data[0];
			if ($data[1] > $mes_atual) {
				$idade--;
			}
			if ($data[1] == $mes_atual and $dia_atual < $data[2]) {
				$idade--;
			}
			return $idade;
		}
		return false;
	} 
	public static function truncarTexto2($string, $limit, $break=" ", $pad="...") { 
            $limit = ($limit-strlen($pad));
            if(strlen($string) <= $limit) return $string;
            $string = substr($string, 0, $limit).$pad; 
            return $string; 
	}
	public static function truncarTexto($string, $limit, $break=" ", $pad="...") { 
            $limit = ($limit-strlen($pad));
            if(strlen($string) <= $limit) return $string;
            if(false !== ($breakpoint = strpos($string, $break,$limit-1))) {
                    if($breakpoint < strlen($string) - 1) { 
                            $string = substr($string, 0, $breakpoint).$pad; 
                    } 
            } 
            return $string; 
	}
	
	public static function gravaConteudo ($nomeArquivo, $conteudo,$opcao) {
		$fp = fopen($nomeArquivo,$opcao);
		fwrite($fp,$conteudo);
		fclose($fp);
	}
	
	public static function getComboStatus($objSel,$idSecao,$idBloco,$classAt='panel verde'){	
		$selectedAtivo = ($objSel->getStatus() == ATIVO)?'selected="selected"':'';
		$selectedNA = ($objSel->getStatus() == NAOAVALIADO)?'selected="selected"':'';	
	
		$str = '<select name="status_'.$objSel->getId().'" id="status_'.$objSel->getId().'" class="combo_status" onChange="alteraStatus2('.$objSel->getId().',this.value,'.$idSecao.',\''.$idBloco.'\',\''.$classAt.'\')">
			<option '.$selectedAtivo.' value="'.ATIVO.'">Ativo</option>
			<option '.$selectedNA.' value="'.NAOAVALIADO.'">Inativo</option>
		</select>';
		return $str;
	}

	public static function error_downLoadArquivoExcel($nome,$conteudo){
		header("Content-type: application/vnd.ms-excel");
		header("Content-type: application/force-download");
		header("Content-Disposition: attachment; filename=$nome");
		header("Pragma: no-cache");
		print "<table>$conteudo</table>";
		exit();
	}
	

	
	public static function validaData($dat,$separador = '-'){
		$data = explode($separador,"$dat");
		$d = $data[2];
		$m = $data[1];
		$y = $data[0];
		if(strlen($d)>0&&strlen($m)>0&&strlen($y)>0){
			$res = checkdate($y,$m,$d);	
		}
		if ($res == 1){
		   return true;
		} else {
		   return false;
		}
	}
	
	public static function comparaDatas($dat1,$dat2,$retorno){	
		$data = mktime(substr($dat1,11,2),substr($dat1,14,2),substr($dat1,17,2),substr($dat1,5,2),substr($dat1,8,2),substr($dat1,0,4));
		$data_atual = mktime(substr($dat2,11,2),substr($dat2,14,2),substr($dat2,17,2),substr($dat2,5,2),substr($dat2,8,2),substr($dat2,0,4));
		$dias = floor(($data_atual - $data)/86400);
		$horas = floor((($data_atual - $data)-($dias*86400))/3600);
		$mins = ceil ((($data_atual - $data)-($dias*86400)-($horas*3600))/60);	
		switch($retorno){
			case "dias":		
				return $dias;
			case "horas":
				return $horas;
			case "minutos":
				return $mins;
		}	
	}
	
	public static function intervaloDatas($data1,$data2){
		$dias = self::comparaDatas($data1,$data2,"dias");	
		if($dias <= 0){
			$horas = self::comparaDatas($data1,$data2,"horas");
			if($horas <= 0){
				if($horas == 0){
					$minutos = self::comparaDatas($data1,$data2,"minutos");
					$resp = $minutos." minuto(s)";
				}else{
					$resp = $horas." hora(s)";
				}
			}else{
				$resp = $horas." hora(s)";
			}
		}else{
			$horas = self::comparaDatas($data1,$data2,"horas");
			if($horas > 0)
				$dias++;	
			$resp = $dias." dia(s)";
		}
		return $resp;
	}
	
	public static function gerarXml($conteudo){
		if($conteudo){
			header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
			header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header ("Cache-Control: no-cache, must-revalidate"); 
			header ("Pragma: no-cache"); 	
			header("Content-type: text/xml; charset=UTF-8");
			print '<?xml version="1.0" encoding="UTF-8"?>';
			print "<root>$conteudo</root>";
		}
	}
	public static function limparTexto($txt){
		//$txt = preg_replace('/<(\/)?p>/msi','',$txt);
		//$txt = preg_replace('/<(\/)?strong>/msi','<$1b>',$txt);
		return $txt;
	}
	
	public static function convDataDoPayPal($data){
		return preg_replace('/([0-9]{4})-([0-9]{2})-([0-9]{2})\w([0-9]{2}:[0-9]{2}:[0-9]{2})\w/msi','$3/$2/$1 às $4',$data);;
	}
	
	
	public static function campararDatas($dat1,$dat2){
		$data = mktime(substr($dat1,11,2),substr($dat1,14,2),substr($dat1,17,2),substr($dat1,5,2),substr($dat1,8,2),substr($dat1,0,4));
		$data_atual = mktime(substr($dat2,11,2),substr($dat2,14,2),substr($dat2,17,2),substr($dat2,5,2),substr($dat2,8,2),substr($dat2,0,4));
		return $dat1<$dat2;
	}
	
	// Função que busca o cep e retorna array
	/*public static function buscaCep($cep,$tipoRetorno='string'){   
		$resultado = file_get_contents('http://www.buscarcep.com.br/?cep='.urlencode($cep).'&formato='.$tipoRetorno);
		if(!$resultado){
			$resultado = "&resultado=0&resultado_txt=erro+ao+buscar+cep";
			echo "<script>\n alert(\"Web service de busca de CEP temporariamente indisponível!\"); \n</script>";
		}
		if($tipoRetorno == 'string')
		parse_str($resultado, $retorno);
		return $retorno;
	}*/
	
	public static function zipar($nomeArquivo,$arqToAdd,$isPasta=false,$dir=""){
		$zip = new ZipArchive();
		$zip->open($nomeArquivo,ZIPARCHIVE::CREATE);
		if($isPasta){
			$nodes = glob($arqToAdd.$dir.'/*');
			foreach($nodes as $node){
				$add = str_replace($arqToAdd,"",$node);
				$zip->addFile($node,$add);
			}
		}else{
			$zip->addFile($arqToAdd);
		}
		$zip->close();
	}

        static function arrtourl($arr, $entity=true, $prefix='') {
                $params = array();
                foreach ($arr as $k => $v)
                   if ($v)
                   $params[] = is_array($v) ? arrtourl($v, $entity, $prefix ? $prefix.'['.$k.']' : $k) : sprintf($prefix ? $prefix.'[%s]' : '%s', urlencode($k)).'='.urlencode($v);
                return implode($entity ? '&amp;' : '&', $params);
        }

	
	public static function validaFormulario($vld,$formulario,$msg="",$maxLength="",$minLength=""){	
		$count = count($formulario);		
		$str_campo = "";
		foreach($formulario as $i => $frm){
			$checagem = 0;
			if(!isset($vld[$i])) $vld[$i] = 1;		
			switch($vld[$i]){
				case 1:	$checagem = self::validaCampo($frm); break;
				case 2: $checagem = self::validaEmail($frm); break;
				case 3: $checagem = self::apenasNum($frm); break;
			}			

			if($checagem && isset($minLength[$i]) && $minLength[$i]){
				$checagem = strlen($frm) >= $minLength[$i];
			}
			if($checagem && isset($maxLength[$i]) && $maxLength[$i]){
				$checagem = strlen($frm) <= $maxLength[$i];
			}
			//print "<br>campo:".$i." - tipo:$vld[$i] - minLen:".$minLength[$i]." - maxLen:".$maxLength[$i]." ; lenAt : ".strlen((string)$frm)." = ".$checagem;
			if(!$checagem){		
				if($msg[$i]){			
					$str_campo .= $msg[$i].",";
				}else{
					//$str_campo .= "";
				}
				$count--;
				
			}
			
		}
		if($count == count($formulario)){
			return true;
		}else{	
			return $str_campo;
		}
	}

	public static function showErroValidacao($str_campo,$urlVoltar='javascript:history.back();',$atomicaAlert=false){
            $str_campo = str_replace(",","<br>",$str_campo);
            if($atomicaAlert){
                //revisar
                print "
                <script src='js/jquery-1.3.2.min.js'></script>
                <script src='js/Popup.js' type='text/javascript'></script>
                <script src='js/scripts.js' language='JavaScript' type='text/javascript'></script>
                <script>setTimeout(function(){atomicaAlert('Preencha corretamente os campos:<br>$str_campo<br>')},500);document.location = '$urlVoltar';</script>";
            }else{
                print "Preencha corretamente os campos:<br>$str_campo<br><a href='$urlVoltar'>Voltar</a>";
            }
	}
	
	public static function validaCampo($campo){
            return $campo != "";
	}
	
	public static function validaEmail($campo){
            //filter_var($campo,FILTER_VALIDATE_EMAIL)
            return ($campo != "" && strpos($campo,"@") > 0 && strpos($campo,".") > 0);
	}

	public static function apenasNum($campo){
            return ($campo != "" && is_numeric($campo));
	}
	
	public static function indexOf($mystring, $findme){
            return strpos($mystring, $findme);
	}
	
	public static function retirarChars($campo){
            return preg_replace("/[^0-9]/i","",$campo);
	}
	public static function retirarNum($campo){
            return preg_replace("/[0-9]/i","",$campo);
	}

        
}



?>