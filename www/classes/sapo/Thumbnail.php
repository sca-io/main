<?

class Thumbnail{
    public $imgDst;
    public $imgOrigem;
    public $format;
    public $quality;
    public $width;
    public $height;
    public $x;
    public $y;
    public $path;
	
    function Thumbnail($imgfile){
	$this->imgOrigem["path"] = $imgfile;
        $this->format = preg_replace("/.*\.(.*)$/msi","\\1",$imgfile);
        $this->format = strtoupper($this->format);
        if ($this->format == "JPG" || $this->format == "JPEG") {
            $this->format="JPEG";
            $this->imgOrigem["src"] = ImageCreateFromJPEG ($imgfile);
        } elseif ($this->format == "PNG"){			
            $this->imgOrigem["src"] = ImageCreateFromPNG ($imgfile);
            imagealphablending($this->imgOrigem["src"], true);
        } elseif ($this->format == "GIF") {
            $this->imgOrigem["src"] = ImageCreateFromGIF ($imgfile);
        }
        @$this->imgOrigem["imagesx"] = imagesx($this->imgOrigem["src"]);
        @$this->imgOrigem["imagesy"] = imagesy($this->imgOrigem["src"]);
        //default quality jpeg
        $this->quality = 100;
    }
	
    function redimencionar($largura,$altura,$manterProporcao=true){
       // print "w : ". $largura." h : ". $altura."\n";
        if($manterProporcao){
                $X = $this->imgOrigem["imagesx"];
                $Y = $this->imgOrigem["imagesy"];
                if($X >= $Y && $largura < $X){
                    //print "paisagem";
                    $proporcao = $Y/$X;
                    $X = $largura;
                    $Y = (int)($largura*$proporcao);

                    if($altura < $Y){
                        $proporcao = $X/$Y;
                        $Y = $altura;
                        $X = (int)($altura*$proporcao);
                    }
                }elseif($X < $Y && $altura < $Y){
                    //print "retrato";
                    $proporcao = $X/$Y;
                    $Y = $altura;
                    $X = (int)($altura*$proporcao);
                }
                $this->width = $X;
                $this->height = $Y;
                /*print "porporcao : ". $proporcao."\n";
                print "w : ". $this->width."\n";
                print "h : ". $this->height."\n";*/

        }else{
                $this->width = $largura;
                $this->height = $altura;
        }

    }

    function autoCrop($w,$h){
       /* $X = $this->imgOrigem["imagesx"];
        $Y = $this->imgOrigem["imagesy"];

        $this->width = $w;
        $this->height = $h;
        $this->x = $x;
        $this->y = $y;*/
    }

    function crop($w,$h,$x,$y){
        $this->width = $w;
        $this->height = $h;
        $this->x = $x;
        $this->y = $y;
    }

    function getImageTemp($usarTamOriginal=true){
        $imagem = ImageCreateTrueColor($this->width, $this->height);
        if ($this->format == "PNG" || $this->format == "GIF") {
            imagealphablending($imagem, false);
            imagesavealpha($imagem, true);
            $transparent = imagecolorallocatealpha($imagem, 0, 0, 0, 127);
            imagefilledrectangle($imagem, 0, 0, $this->width, $this->height, $transparent);
            imagecolortransparent( $imagem, $transparent);
        }
        $width = ($usarTamOriginal)?$this->imgOrigem["imagesx"]: $this->width;
        $height = ($usarTamOriginal)?$this->imgOrigem["imagesy"]: $this->height;
        @imagecopyresampled($imagem, $this->imgOrigem["src"], 0, 0, $this->x, $this->y, $this->width, $this->height, $width, $height);
        return $imagem;
    }
	
    function exibir($usarTamOriginal=true){
        @header("Content-Type: image/".strtolower($this->format));
        $this->imgDst = $this->getImageTemp($usarTamOriginal);
        if ($this->format == "JPEG"){
            imageJPEG($this->imgDst,"",$this->quality);
        } elseif ($this->format == "PNG"){
            imagePNG($this->imgDst);
        } elseif ($this->format == "GIF"){
            imageGIF($this->imgDst);
        }
	$this->free();
    }

    function gerar($path="",$usarTamOriginal=true){
        if (empty($path)) $this->path=$this->imgOrigem["path"]; else $this->path = $path;
        $this->imgDst = $this->getImageTemp($usarTamOriginal);
        if ($this->format == "JPEG") {
            imageJPEG($this->imgDst,$this->path,$this->quality);
        } elseif ($this->format == "PNG") {
            imagePNG($this->imgDst,$this->path);
        } elseif ($this->format == "GIF") {
            imageGIF($this->imgDst,$this->path);
        }
        $this->free();
    }
	
    function free(){
        ImageDestroy ($this->imgDst);
        ImageDestroy ($this->imgOrigem["src"]);
    }
}
?>