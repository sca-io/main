<?
class Estados{
        public static $nomes = 	array("AC" => "Acre","AL" => "Alagoas",	"AP" => "Amapá","AM" => "Amazonas","BA" => "Bahia","CE" => "Ceará","DF" => "Distrito Federal","ES" => "Espírito Santo","GO" => "Goiás","MA" => "Maranhão","MT" => "Mato Grosso","MS" => "Mato Grosso do Sul","MG" => "Minas Gerais","PA" => "Pará","PB" => "Paraíba",	"PR" => "Paraná",	"PE" => "Pernambuco","PI" => "Piauí","RJ" => "Rio de Janeiro","RN" => "Rio Grande do Norte","RS" => "Rio Grande do Sul","RO" => "Rondônia","RR" => "Rorâima","SC" => "Santa Catarina",	"SP" => "São Paulo","SE" => "Sergipe","TO" => "Tocantins");
	public static function getHtmlOptions($estadoSel='',$apenasSigla=false){
		$strOpt = "";
		foreach(self::$nomes as $i=>$l){
			$selected = ($estadoSel == $i  || $estadoSel == $l)?" selected=\"selected\"":"";
			$text = ($apenasSigla)?$i:$l;
			$strOpt.="<option value=\"".$i."\" $selected>".$text."</option>\n";	
		}		
		return $strOpt;
	}
	
	public static function getEstado($sigla){
		self::$nomes[$sigla];
	}
	public static function getSigla($nome){		
		foreach(self::$nomes as $i=>$l){
			if($nome == $l){
				$sigla = $i;
				break;	
			}
			
		}
		return $sigla;
	}
}
	
?>