<?
class AbaSuperior{
    private $numAbas;
    private $indexAba;

    function setIndexAba($index){
        $this->indexAba = $index;
    }	
    function getIndexAba(){
        return $this->indexAba;
    }
    function addItem($descricao,$link=""){
        $this->numAbas++;
        $selecionado = ($this->numAbas == $this->indexAba)?'active':'';
        if($link){
            $link = $link."&indexAba=".$this->numAbas;
        }else{
            $link = "javascript:;";
        }
        $this->html.= '<li id="l'.$this->numAbas.'" class="'.$selecionado.'" ><a id="aba'.$this->numAbas.'" href="'.$link.'" >'.$descricao.'</a></li>';
    }	
    function printHtml(){
        print "<div class='abas'><ul>".$this->html."</ul></div>";
    }	
}
?>