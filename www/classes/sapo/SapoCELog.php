<?
class SapoCELog{
	private $id;
	private $idSce;
	private $idSe;
	private $msg;
	private $flgErro;
	private $data;
	private $status;
	public static $logSql;
	private $oldValues = Array('id'=>'','id_sce'=>'','id_se'=>'','msg'=>'','flg_erro'=>'','data'=>'','status'=>'');

	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdSce($inIdSce){
		$this->idSce = $inIdSce;
	}

	public function getIdSce(){
		return $this->idSce;
	}

	public function setIdSe($inIdSe){
		$this->idSe = $inIdSe;
	}

	public function getIdSe(){
		return $this->idSe;
	}

	public function setMsg($inMsg){
		$this->msg = $inMsg;
	}

	public function getMsg(){
		return $this->msg;
	}

	public function setFlgErro($inFlgErro){
		$this->flgErro = $inFlgErro;
	}

	public function getFlgErro(){
		return $this->flgErro;
	}

	public function setData($inData){
		$this->data = $inData;
	}

	public function getData(){
		return $this->data;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus(){
		return $this->status;
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_sapo_c_e_log');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['id_sce'])) $this->idSce=$conteudo['id_sce'];
		if(isset($conteudo['id_se'])) $this->idSe=$conteudo['id_se'];
		if(isset($conteudo['msg'])) $this->msg=$conteudo['msg'];
		if(isset($conteudo['flg_erro'])) $this->flgErro=$conteudo['flg_erro'];
		if(isset($conteudo['data'])) $this->data=$conteudo['data'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		
		foreach($conteudo as $k=>$news){ $this->oldValues[$k] = $news;} 
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || $this->oldValues['id_sce'] != $this->idSce){ $campo[] = 'id_sce';  $valor[] = "'$this->idSce'"; }
		if(!$this->id || $this->oldValues['id_se'] != $this->idSe){ $campo[] = 'id_se';  $valor[] = "'$this->idSe'"; }
		if(!$this->id || $this->oldValues['msg'] != $this->msg){ $campo[] = 'msg';  $valor[] = "'$this->msg'"; }
		if(!$this->id || $this->oldValues['flg_erro'] != $this->flgErro){ $campo[] = 'flg_erro';  $valor[] = "'$this->flgErro'"; }
		if(!$this->id || $this->oldValues['data'] != $this->data){ $campo[] = 'data';  $valor[] = "'$this->data'"; }
		if(!$this->id || $this->oldValues['status'] != $this->status){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		
		
		$db=new DB('tbl_sapo_c_e_log');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return SapoCELog::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_sapo_c_e_log');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($idSce='',$idSe='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_sapo_c_e_log');
						
		if(strlen($idSce)>0){
			if($where) $where .= " AND ";
			$where .= "id_sce = '$idSce'";	
		}				
		if(strlen($idSe)>0){
			if($where) $where .= " AND ";
			$where .= "id_se = '$idSe'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new SapoCELog($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = SapoCELog::listar("","",$campos,"id = '$id'",'','1');
		return $obj[0];
	}

	public static function listarXML($idSce='',$idSe='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_sapo_c_e_log');
						
		if(strlen($idSce)>0){
			if($where) $where .= " AND ";
			$where .= "id_sce = '$idSce'";	
		}				
		if(strlen($idSe)>0){
			if($where) $where .= " AND ";
			$where .= "id_se = '$idSe'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<SapoCELog id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</SapoCELog>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($idSce='',$idSe='',$where=''){
		$db=new DB('tbl_sapo_c_e_log');
						
		if(strlen($idSce)>0){
			if($where) $where .= " AND ";
			$where .= "id_sce = '$idSce'";	
		}				
		if(strlen($idSe)>0){
			if($where) $where .= " AND ";
			$where .= "id_se = '$idSe'";	
		}
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao();//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao();//observação pertinente à classe
		$hst->salvar();
	}

}
?>