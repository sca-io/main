<?

class Upload{    
    public $error;
    public $diretorio;
    public $msgErroFrmt;
    public $limitMaxTam = 7;
    const ARQUIVO_NAO_SETADO = 2;
    const FORMATO_INVALIDO = 3;
    const ULTRAPASSOU_LIMITE_PESO = 4;

    public function __construct($inDiretorio){
            $this->diretorio = $inDiretorio;
            if(LIMITE_MAX_UPLOAD) $this->limitMaxTam = (int)(LIMITE_MAX_UPLOAD/1024/1024); //EM MB *constante definida em /configs/upload.php
    }
    public static function checkErrosRequest($arquivo){
            if($arquivo && $arquivo["error"]){
                    switch($arquivo["error"]){
                            case UPLOAD_ERR_FORM_SIZE:
                            case UPLOAD_ERR_INI_SIZE:
                                    return "O tamanho de seu arquivo ultrapassa o limite de ".(LIMITE_MAX_UPLOAD/1024/1024)."MB! Envie outro arquivo";
                            break; 
                            case UPLOAD_ERR_OK:
                                    return "";
                                    //return "sem erros";
                            break;  
                            case UPLOAD_ERR_PARTIAL:
                                     return "O upload do arquivo foi feito parcialmente! tente novamente";   
                            break;
                            case UPLOAD_ERR_NO_FILE:
                                    return "";
                                    //return "Arquivo não indexado!";
                            break;                   
                    }
                    return "Ocorreu um erro ao subir o arquivo, tente novamente!";
            }            
    }
    function getMensagemError(){		
            switch($this->error){
                    case self::FORMATO_INVALIDO:
                            return "Arquivo com formato inválido!".$this->msgErroFrmt;
                    break;
                    case self::ARQUIVO_NAO_SETADO:
                            return "Arquivo não selecionado!";
                    break;
                    case self::ULTRAPASSOU_LIMITE_PESO:
                            return "Arquivo ultrapassou limite máximo de tamanho, que é de ".$this->limitMaxTam."MB!";
                    break;
            }
    }
    public function tranferir($file,$novoNome="",$filtroExt=""){            
        $arquivo = isset($file) ? $file : false;
        if($arquivo){               
            if($filtroExt){
                    if(!validaMimeType($arquivo,$filtroExt)){
                            $strExt = "";
                            foreach($filtroExt as $k=>$ext){
                                    if($k > 0 && $k < count($filtroExt)) $strExt .= ",";
                                    if($k == count($filtroExt)) $strExt .= " ou";
                                    $strExt .= " .".$ext;
                            }
                            $this->msgErroFrmt = "<br />Utilize apenas os formatos:$strExt";
                            $this->error = self::FORMATO_INVALIDO;
                            return false;
                    }
            }               
            $sizeMb = (int)(((int)$arquivo["size"] / 1024) / 1024); 
            //print $sizeMb;exit;
            if($sizeMb > $this->limitMaxTam){
                    $this->error = self::ULTRAPASSOU_LIMITE_PESO;
                    return false;
            }
            if($novoNome){
                    $nome_arq = $novoNome;
            }else{									
                    $nome_arq = md5(uniqid(time()));
            }
            $ext = explode(".",$arquivo["name"]);	
            if(count($ext)==1){
                    $nome_arq = $nome_arq . "." . $ext[1];
            }else{
                    $nome_arq = $nome_arq . "." . $ext[count($ext)-1];
            }	
            $url_media = $this->diretorio."/".$nome_arq;
            $url_abs_media = $_SERVER['DOCUMENT_ROOT'].$url_media;
            move_uploaded_file($arquivo["tmp_name"], $url_abs_media) or die("Erro ao subir imagem");
            return $nome_arq;	
        }else{
            $this->error = self::ARQUIVO_NAO_SETADO;
            return "";
        }
    }
}
?>