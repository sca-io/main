<?
class SapoEspelhamento{
	private $id;
	private $fPath;
	private $fExt;
	private $fSize;
	private $fDataC;
	private $fDataM;
	private $fOwner;
	private $fPerm;
	private $dataIns;
	private $dataAlt;
	private $status;
	public static $logSql;
	private $oldValues = Array('id'=>'','f_path'=>'','f_ext'=>'','f_size'=>'','f_data_c'=>'','f_data_m'=>'','f_owner'=>'','f_perm'=>'','data_ins'=>'','data_alt'=>'','status'=>'');

	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setFPath($inFPath){
		$this->fPath = $inFPath;
	}

	public function getFPath(){
		return $this->fPath;
	}

	public function setFExt($inFExt){
		$this->fExt = $inFExt;
	}

	public function getFExt(){
		return $this->fExt;
	}

	public function setFSize($inFSize){
		$this->fSize = $inFSize;
	}

	public function getFSize(){
		return $this->fSize;
	}

	public function setFDataC($inFDataC){
		$this->fDataC = $inFDataC;
	}

	public function getFDataC(){
		return $this->fDataC;
	}

	public function setFDataM($inFDataM){
		$this->fDataM = $inFDataM;
	}

	public function getFDataM(){
		return $this->fDataM;
	}

	public function setFOwner($inFOwner){
		$this->fOwner = $inFOwner;
	}

	public function getFOwner(){
		return $this->fOwner;
	}

	public function setFPerm($inFPerm){
		$this->fPerm = $inFPerm;
	}

	public function getFPerm(){
		return $this->fPerm;
	}

	public function setDataIns($inDataIns){
		$this->dataIns = $inDataIns;
	}

	public function getDataIns(){
		return $this->dataIns;
	}

	public function setDataAlt($inDataAlt){
		$this->dataAlt = $inDataAlt;
	}

	public function getDataAlt(){
		return $this->dataAlt;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus(){
		return $this->status;
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_sapo_espelhamento');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['f_path'])) $this->fPath=$conteudo['f_path'];
		if(isset($conteudo['f_ext'])) $this->fExt=$conteudo['f_ext'];
		if(isset($conteudo['f_size'])) $this->fSize=$conteudo['f_size'];
		if(isset($conteudo['f_data_c'])) $this->fDataC=$conteudo['f_data_c'];
		if(isset($conteudo['f_data_m'])) $this->fDataM=$conteudo['f_data_m'];
		if(isset($conteudo['f_owner'])) $this->fOwner=$conteudo['f_owner'];
		if(isset($conteudo['f_perm'])) $this->fPerm=$conteudo['f_perm'];
		if(isset($conteudo['data_ins'])) $this->dataIns=$conteudo['data_ins'];
		if(isset($conteudo['data_alt'])) $this->dataAlt=$conteudo['data_alt'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		
		foreach($conteudo as $k=>$news){ $this->oldValues[$k] = $news;} 
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || $this->oldValues['f_path'] != $this->fPath){ $campo[] = 'f_path';  $valor[] = "'$this->fPath'"; }
		if(!$this->id || $this->oldValues['f_ext'] != $this->fExt){ $campo[] = 'f_ext';  $valor[] = "'$this->fExt'"; }
		if(!$this->id || $this->oldValues['f_size'] != $this->fSize){ $campo[] = 'f_size';  $valor[] = "'$this->fSize'"; }
		if(!$this->id || $this->oldValues['f_data_c'] != $this->fDataC){ $campo[] = 'f_data_c';  $valor[] = "'$this->fDataC'"; }
		if(!$this->id || $this->oldValues['f_data_m'] != $this->fDataM){ $campo[] = 'f_data_m';  $valor[] = "'$this->fDataM'"; }
		if(!$this->id || $this->oldValues['f_owner'] != $this->fOwner){ $campo[] = 'f_owner';  $valor[] = "'$this->fOwner'"; }
		if(!$this->id || $this->oldValues['f_perm'] != $this->fPerm){ $campo[] = 'f_perm';  $valor[] = "'$this->fPerm'"; }
		if(!$this->id || $this->oldValues['data_ins'] != $this->dataIns){ $campo[] = 'data_ins';  $valor[] = "'$this->dataIns'"; }
		if(!$this->id || $this->oldValues['data_alt'] != $this->dataAlt){ $campo[] = 'data_alt';  $valor[] = "'$this->dataAlt'"; }
		if(!$this->id || $this->oldValues['status'] != $this->status){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		
		
		$db=new DB('tbl_sapo_espelhamento');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return SapoEspelhamento::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_sapo_espelhamento');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_sapo_espelhamento');
		
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new SapoEspelhamento($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = SapoEspelhamento::listar($campos,"id = '$id'",'','1');
		return $obj[0];
	}

	public static function listarXML($campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_sapo_espelhamento');
		
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<SapoEspelhamento id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</SapoEspelhamento>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($where=''){
		$db=new DB('tbl_sapo_espelhamento');
		
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao();//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao();//observação pertinente à classe
		$hst->salvar();
	}
        
        public static function salvarNovo($fDataM,$fExt,$fOwner,$fPath,$fPerm,$fSize,$idSCE){
            $objSE = new SapoEspelhamento();
            $objSE->setDataAlt(date("Y-m-d H:i:s"));
            $objSE->setDataIns(date("Y-m-d H:i:s"));
            $objSE->setFDataC(date("Y-m-d H:i:s"));
            $objSE->setFDataM($fDataM);
            $objSE->setFExt($fExt);
            $objSE->setFOwner($fOwner);
            $objSE->setFPath($fPath);
            $objSE->setFPerm($fPerm);
            $objSE->setFSize($fSize);
            $objSE->setStatus(ATIVO);
            $objSE->salvar();

            $objCELog = new SapoCELog();
            $objCELog->setIdSce($idSCE);
            $objCELog->setIdSe($objSE->getId());
            $objCELog->setFlgErro(CE_FLAG_NOVO_ARQUIVO);
            $objCELog->setMsg("<p>ID_".$objSE->getID()." = Novo: $fPath.$fExt</p>");
            $objCELog->setData(date("Y-m-d H:i:s"));
            $objCELog->setStatus(NAOAVALIADO);
            $objCELog->salvar();
            
            return $objSE;
        }
        
        public static function countFiles($pasta){            
            $root = glob_recursive($pasta,0);
            $count = count($root);
            return $count;
        }
        
        public static function seekAndStore($idSCE,$pasta,$idxF=-1)
        {
            if($idxF!=-1){
                $root = glob_recursive($pasta,0);            
                if(count($root)>$idxF){
                    $node = $root[$idxF];
                    
                    $fOwner = $fPerm = $fSize = $fDataM = $fPath = $fExt = "";
                    if(indexOf($node,"arquivos/")==-1){
                        if(!is_dir($node)){
                            
                            $fExt = explode(".",$node);
                            $fExt = end($fExt);

                            $fPath = str_replace($_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ,"",str_replace(".$fExt","",$node));

                            $fOwner = json_encode(GetUsernameFromUid(fileowner($node)));
                            $fPerm = substr(decoct(fileperms($node)),3);
                            $fSize = filesize($node);
                            $fDataM = date("Y-m-d H:i:s",filemtime($node));

                            $where = "f_path = '$fPath' AND f_ext = '$fExt'";
                            $nR = SapoEspelhamento::countListar($where);
                            if($nR<=0){
                                $objSE = SapoEspelhamento::salvarNovo($fDataM,$fExt,$fOwner,$fPath,$fPerm,$fSize,$idSCE);
                                return $objSE;
                            }else{
                                return SapoEspelhamento::fileCompair($idSCE,$pasta,$idxF);
                            }
                        }
                    }
                }else{
                    return "EOF";
                }
            }            
        }
        
        public static function fileCompair($idSCE,$pasta,$idx=-1){
            if($idx!=-1){
                $root = glob_recursive($pasta,0);
                $node = $root[$idx];
                
                $fOwner = $fPerm = $fSize = $fDataM = $fPath = $fExt = "";
                if(indexOf($node,"arquivos/")==-1){
                    if(!is_dir($node)){

                        $fExt = explode(".",$node);
                        $fExt = end($fExt);
                        $fPath = str_replace($_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ,"",str_replace(".$fExt","",$node));
                        $fOwner = json_encode(GetUsernameFromUid(fileowner($node)));
                        $fPerm = substr(decoct(fileperms($node)),3);
                        $fSize = filesize($node);
                        $fDataM = date("Y-m-d H:i:s",filemtime($node));

                        $where = "f_path = '$fPath' AND f_ext = '$fExt'";
                        $lst = SapoEspelhamento::listar("",$where,"id ASC",'1');                
                        if($lst){
                            foreach($lst as $objSE){              
                                $ok = 0;
                                $flgErro = 0;
                                $msgLog = "";

                                if($objSE->getFDataM() != $fDataM){
                                    $flgErro = CE_FLAG_ERRO_FDATAM;
                                    $msgLog .= "<p>ID_".$objSE->getID()." = ERRO_$flgErro => Data de modificação alterada : ".$objSE->getFDataM()." por $fDataM;</p>";  
                                    $ok++;
                                }
                                if(stripslashes($objSE->getFOwner()) != stripslashes($fOwner)){
                                    $flgErro = CE_FLAG_ERRO_FOWNER;
                                    $msgLog .= "<p>ID_".$objSE->getID()." = ERRO_$flgErro => Dados de criador alterado : ".stripslashes($objSE->getFOwner())." por ".stripslashes($fOwner).";</p>";  
                                    $ok++;
                                }
                                if($objSE->getFExt() != $fExt){
                                    $flgErro = CE_FLAG_ERRO_FEXT;
                                    $msgLog .= "<p>ID_".$objSE->getID()." = ERRO_$flgErro => Extensão alterada : ".$objSE->getFExt()." por $fExt;</p>";  
                                    $ok++; 
                                }
                                if($objSE->getFPerm() != $fPerm){
                                    $flgErro = CE_FLAG_ERRO_FPERM;
                                    $msgLog .= "<p>ID_".$objSE->getID()." = ERRO_$flgErro => Permissão alterada : ".$objSE->getFPerm()." por $fPerm;</p>";   
                                    $ok++;
                                }
                                if($objSE->getFSize() != $fSize){
                                    $flgErro = CE_FLAG_ERRO_FSIZE;
                                    $msgLog .= "<p>ID_".$objSE->getID()." = ERRO_$flgErro => Tamanho alterado : ".$objSE->getFSize()." por $fSize;</p>";   
                                    $ok++;
                                }

                                if($ok==0){
                                    return $objSE;
                                }else{

                                    $objCELog = new SapoCELog();
                                    $objCELog->setIdSce($idSCE);
                                    $objCELog->setIdSe($objSE->getId());
                                    $objCELog->setFlgErro($flgErro);
                                    $objCELog->setMsg($msgLog);
                                    $objCELog->setData(date("Y-m-d H:i:s"));
                                    $objCELog->setStatus(NAOAVALIADO);
                                    $objCELog->salvar();

                                    return str_replace($_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ,"",$node);
                                }
                            }
                        }else{
                            $objSE = SapoEspelhamento::salvarNovo($fDataM,$fExt,$fOwner,$fPath,$fPerm,$fSize,$idSCE);
                            /*return $objSE;*/
                            return str_replace($_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ,"",$node);
                        }
                    }
                }
            }
        }
}
?>