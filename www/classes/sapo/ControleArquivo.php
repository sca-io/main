<?

class ControleArquivo{    
    public $error;
    public $diretorio;
    private $upload;
    function setDiretorioUpload($inDiretorio){
            $this->diretorio = $inDiretorio;
    }	
    function getDiretorio(){
            return $this->diretorio;
    }
    function getDiretorioAbsoluto(){
            return $_SERVER['DOCUMENT_ROOT'].$this->diretorio;
    }
    function getMensagemErrorUpload(){
        return $this->upload->getMensagemError();
    }
    public function fazerUpLoad($file,$novoNome="",$regxTipos=""){    
        $this->upload = new Upload($this->diretorio);
        $resUp = $this->upload->tranferir($file,$novoNome,$regxTipos);
        return $resUp;
    }
    public function duplicarArquivo($nomeArquivo,$novoNome=""){
        $novoNome = $novoNome?$novoNome:$nomeArquivo."(2)";
        copy($_SERVER['DOCUMENT_ROOT']."/".$this->getDiretorio()."/".$nomeArquivo,$_SERVER['DOCUMENT_ROOT']."/".$this->getDiretorio()."/".$novoNome);
    }
    
    public static function download($arquivo){
        $arr = explode(".",$arquivo);
        if(end($arr) == "php") return false;
        $bufferSize = 512 * 1024; // 512Kb per second
        ob_end_clean();
        ignore_user_abort(true);
        set_time_limit(0);
        
        $filename = realpath($arquivo);
        if (file_exists($filename)) {
            
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($filename));
            header('Content-Length: ' . filesize($filename));
            $resource = fopen($filename, 'rb');
            while (!feof($resource)) {
                echo fread($resource, $bufferSize);
                flush();
                sleep(1);
            }
            fclose($resource);
            return true;
        }else{
            return false;
        }
    }
    public static function getDadosDeExcel($urlExcel,$tipoIndex="primeira-linha"){            
        $url_abs = ROOT_ABS.$urlExcel;
        $classe = ROOT_ABS.'/classes/PHPExcel-1.7.7/Classes/PHPExcel/IOFactory.php';
        $campos = array();
        $dadosExcel = array("error"=>"","dados"=>"");
        if(!file_exists($classe)){
            $dadosExcel["error"] = "Classe não encontrada!";
            return $dadosExcel;
        }
        if(!file_exists($url_abs)){
            $dadosExcel["error"] = "Arquivo não encontrado!";
            return $dadosExcel;
        }
        require_once($classe);
        set_time_limit(0);
        try {
            $objPHPExcel = PHPExcel_IOFactory::load($url_abs);               
            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            if($sheetData){
                $dadosExcel["dados"] = array();
                foreach($sheetData as $numLin=>$linha){
                    if($numLin == 1 && $tipoIndex == "primeira-linha"){
                        $campos = $linha;
                        $dadosExcel["headers"]  = $linha;
                        continue;
                    }
                    $dadosLinha = array();
                    $addLinha = false;
                    foreach($linha as $indxCol=>$dado){
                        //print $col." = ".strlen($col)."<br>";
                        if(isset($dado) && strlen($dado) > 0) $addLinha = true;
                        if($tipoIndex == "primeira-linha")
                            $indexArr = strtolower($campos[$indxCol]);
                        else
                            $indexArr = $numLin;
                        $dadosLinha[$indexArr] = $dado;  
                    }
                    if(count($dadosLinha) > 0 && $addLinha) $dadosExcel["dados"][] = $dadosLinha;

                    /* print " - linha $numLin<br />";*/
                }
            }
        } catch(Exception $e) {
            $dadosExcel["error"] = 'Error loading file "'.pathinfo($url_abs,PATHINFO_BASENAME).'": '.$e->getMessage();
        }             
        return $dadosExcel;
    }
}
?>