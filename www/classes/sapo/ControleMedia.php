<?

class ControleMedia{    
    public $error;
    public $diretorio;
    public $porporcao;
    public $porporcoes;
    private $upload;

    function setPorporcao($porporcao){
            $this->porporcao = $porporcao;
    }
    function getPorporcao(){
            return $this->porporcao;
    }
    function getPrefProporcao(){
        foreach($this->porporcao->dimensoes as $k=>$d){
           return $k;
        }
    }
    function setDiretorioUpload($inDiretorio){
            $this->diretorio = $inDiretorio;
    }	
    function getDiretorio(){
            return $this->diretorio;
    }
    function getDiretorioAbsoluto(){
            return $_SERVER['DOCUMENT_ROOT'].$this->diretorio;
    }
    function getMensagemErrorUpload(){
        return $this->upload->getMensagemError();
    }
    public function fazerUpLoad($file,$novoNome="",$regxTipos=""){    
        $this->upload = new Upload($this->diretorio);
        $resUp = $this->upload->tranferir($file,$novoNome,$regxTipos);      
        if($resUp){
            if(GERAR_COPIA_DE_IMG_ORIGINAL) $this->duplicarArquivo($resUp,"orig_".$resUp);
            //$this->gerarDimensao($nome_arq,'800x800','');
            if($this->porporcoes){
                foreach($this->porporcoes as $prop){
                    $this->gerarDimensoes($prop->dimensoes,$resUp);
                }
            }
        }
        return $resUp;
    }

    
    function gerarDimensoes($arrDimensoes,$nomeArquivo){
        if($arrDimensoes){
            foreach($arrDimensoes as $pref=>$d){
              // print  "$nomeArquivo, $d, $pref \n";
               $this->gerarDimensao($nomeArquivo,$d,$pref);
            }
            if(preg_match("/^temp_/msi",$nomeArquivo)) unlink($this->getDiretorioAbsoluto()."/".$nomeArquivo);
        }
    }
    function gerarDimensao($nomeArquivo,$dimensao,$pref=""){
        $val = explode("x",$dimensao);
        $img = new Thumbnail($this->getDiretorioAbsoluto()."/".$nomeArquivo);
        $img->redimencionar($val[0],$val[1]);
        if(preg_match("/^temp_/msi",$nomeArquivo)) $nomeArquivo = str_replace("temp_","",$nomeArquivo);
        $img->gerar($this->getDiretorioAbsoluto()."/".$pref.$nomeArquivo);
    }

    public function autoCrop($nomeArquivo,$dimensao,$qualid=100){
        $val = explode("x",$dimensao);
        $img = new Thumbnail($this->getDiretorioAbsoluto()."/".$pref.$nomeArquivo);
        $img->autoCrop($val[0],$val[1]);
        $img->gerar($this->getDiretorioAbsoluto()."/temp_".$pref.$nomeArquivo,false);
        $ret = $pref.$nomeArquivo;
        if($this->porporcao && $ret){
           $this->gerarDimensoes($this->porporcao->dimensoes,"temp_".$ret);
        }
        return $ret;
    }

    public function crop($nomeArquivo,$w,$h,$x,$y,$pref="",$qualid=100){
        $img = new Thumbnail($this->getDiretorioAbsoluto()."/".$pref.$nomeArquivo);
        $img->crop($w,$h,$x,$y);
        $img->gerar($this->getDiretorioAbsoluto()."/temp_".$pref.$nomeArquivo,false);
        $ret = $pref.$nomeArquivo;
        if($this->porporcao && $ret){
           $this->gerarDimensoes($this->porporcao->dimensoes,"temp_".$ret);
        }
        return $ret;
    }

    public function duplicarArquivo($nomeArquivo,$novoNome=""){
            $novoNome = $novoNome?$novoNome:$nomeArquivo."(2)";
            copy($_SERVER['DOCUMENT_ROOT']."/".$this->getDiretorio()."/".$nomeArquivo,$_SERVER['DOCUMENT_ROOT']."/".$this->getDiretorio()."/".$novoNome);
    }
}
?>