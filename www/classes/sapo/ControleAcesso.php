<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ControleAcesso
 *
 * @author Fabiano
 */
class ControleAcesso {
    public $maxErrosLogin = 3;
    public $sendEmail = true;
    public $idUser;
    public $numTentativasDeLogar;
    public $emailsNotificacoes;
    public $dominiosPermitidos;
    public $recursoBloqueio;
    public $whiteListReferer;
    
    /*** TIPOS DE BLOQUEIO ***/
    const TENTAVIVA_LOGIN_SAPO = 1;
    const TENTAVIVA_SQL_INJECTION = 2;
    const TENTAVIVA_XSS = 3;
    const TENTAVIVA_POST_PROIBIDO = 4;
    /*****************************/
    
    public function __construct(){
        $this->recursoBloqueio = "sessao"; //instante : somente no ato , sessao : enquanto durar a session;
        
    }
    public function logonUser($usuario,$senha,$urlRediret){
        $user = Usuario::logar($usuario, $senha);
        if($user){
            setSession("mostraMsgRdp","1");
            $this->gravarHistoricoSapo($user->getId(),LOGIN_NO_SAPO,"Login efetuado com sucesso");
            print "<script>window.location = '$urlRediret';</script>";   
        }else{                
            switch(Usuario::$erroLogin){
                case "Usuário bloqueado":
                    $this->gravarHistoricoSapo(getSession('idUsuarioSapoBloq'),ERRO_AO_LOGAR_NO_SAPO,"Usuário bloqueado");
                    print "<script>alert('Por motivos de segurança seu acesso ao sapo foi bloqueado.\\nPara desbloqueár-lo entre em contato com o reponsável pelo site!');window.location = '$urlRediret';</script>";
                    exit;
                break;
                default :
                    $this->registarErroDeLogin($usuario,$urlRediret);
                break;    
            } 
            
            /*print "<script>window.location = '$url';</script>";*/
        }
    }
    public function logoutUser(){
        $user = new Usuario(Array('id'=>getSession('idUsuarioSapo')));
        if($user){
            $user->logof(); 
            $this->gravarHistoricoSapo($user->getId(),LOGOUT_NO_SAPO,"Logout efetuado com sucesso");
        }
    }
    public function checarAcesso(){                
        print getSession("ver_acesso_barrado");
        if(getSession("ver_acesso_barrado")){
            $version = trim(file_get_contents($_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ."/versao-session.txt"));
            //print (secureRequest("",getSession("ver_acesso_barrado")) ."!=". $version ." = ".(secureRequest("",getSession("ver_acesso_barrado")) != $version));
            if(secureRequest("",getSession("ver_acesso_barrado")) != $version){                
                setSession("ver_acesso_barrado","");
            }else{
                print "Por motivos de segurança seu acesso ao site foi bloqueado, entre em contato com o reponsável pelo site!";
                exit;         
            }
        }        
    }
    public function addWhiteListReferer($array){
        $this->whiteListReferer = $array;
    }
    public function autenticarReferer(){
        //$script = str_replace(ENDERECO_SITE,"",$_SERVER['HTTP_REFERER']);print $script;exit;             
        $barrarAcesso = false;
        $log = "";
        if(count($_REQUEST)){		
            if(isset($_SERVER['HTTP_REFERER'])){
                $log .= "<br />REFERER: ".$_SERVER['HTTP_REFERER'];
                $arr_http = explode('?',$_SERVER['HTTP_REFERER']);
                $dr = explode('/',$arr_http[0]);
                if(!in_array($dr[2], $this->dominiosPermitidos)){
                    $barrarAcesso = true;
                }
                if(count($this->whiteListReferer) > 0 && !$barrarAcesso){
                    if(ENDERECO_SITE == $arr_http[0])
                        $script = "index.php";
                    else        
                        $script = str_replace(ENDERECO_SITE,"",$arr_http[0]);                    
                    if(!in_array($script, $this->whiteListReferer)){                        
                        $barrarAcesso = true;
                    }
                }
            }else{
                $barrarAcesso = true;
            }
            $log .= "<br />SCRIPT: ".$_SERVER['PHP_SELF'];
            $log .= "<br />REQUEST: ".var_export($_REQUEST, true);
        }else{
            print "Permissão negada!";
            exit; 
        }
        if($barrarAcesso){
             $this->barrarAcesso(self::TENTAVIVA_POST_PROIBIDO,$log);   
        }
    }
    
    public function barrarAcesso($acao="",$log="",$recursoBloqueio=""){        
        print "Permissão negada!!";        
        if($recursoBloqueio) $this->recursoBloqueio = $recursoBloqueio;        
        if($this->recursoBloqueio == "sessao"){            
            $version = trim(file_get_contents($_SERVER['DOCUMENT_ROOT'].DIRETORIO_RAIZ."/versao-session.txt"));
            //print "teste".getSession("ver_acesso_barrado");
            if($version) setSession("ver_acesso_barrado",secureResponse($version));
            if($acao == self::TENTAVIVA_SQL_INJECTION){
                $this->notificarDesenvolvedor($acao,"Tetativa de SQL Injection<br />LOG: ".$log);
            }
            if($acao == self::TENTAVIVA_XSS){
                $this->notificarDesenvolvedor($acao,"Tetativa de XSS<br />LOG: ".$log);
            }
            if($acao == self::TENTAVIVA_POST_PROIBIDO){
                $this->notificarDesenvolvedor($acao,"Tentaiva de acesso indevido a scripts de controle<br />".$log);
            }
        }
        exit;        
    }

    public function bloquearLogin($user){
        if($user->getStatus() == Usuario::ATIVO){
            $user->setStatus(Usuario::BLOQUEADO);
            $user->salvar();
            $texto = "Login bloqueado: \"".$user->getLogin()."\" <br /> MOTIVO: Usuário atingiu o máximo de tentativas de login no sapo";
            $this->notificarDesenvolvedor(self::TENTAVIVA_LOGIN_SAPO,$texto);
            $this->gravarHistoricoSapo($user->getId(),BLOQUEIO_DE_ACESSO_AO_SAPO,"Usuário atingiu o máximo de tentativas de login no sapo");            
        } 
    }
    public function desBloquearLogin($user){
        /*if($user->getStatus() == Usuario::BLOQUEADO){
            $user->setStatus(Usuario::ATIVO);
            $user->salvar();
        }*/
        $this->gravarHistoricoSapo(getSession('idUsuarioSapo'),DESBLOQUEIO_DE_ACESSO_AO_SAPO,"Desbloqueio do login do usuário: ".$user->getNome());           

    }
    public function setNumErrosDeLogin($idUser,$num){
        $num_erros_login = getSession("num_erros_login");
        $arr_erros_login = array();
        $exist = false;
        if($num_erros_login) {
            $arr_erros_login = explode(",",$num_erros_login);
            //print_r($arr_erros_login);
            if(count($arr_erros_login) > 0){
                foreach($arr_erros_login as $k=>$reg){
                    $dados = explode("=",$reg);
                    if($dados[0] == $idUser){
                        $exist = true;
                        $arr_erros_login[$k] = $dados[0]."=".$num;
                    }
                }
            }else{
                $dados = explode("=",$num_erros_login);
                if($dados[0] == $idUser){
                    $exist = true;
                    $num_erros_login = $dados[0]."=".$num;
                }
                $arr_erros_login = array($num_erros_login);
            }
        }
        if(!$exist) array_push ($arr_erros_login, $idUser."=".$num);
        setSession("num_erros_login",implode(",",$arr_erros_login));
    } 
    public function getNumErrosDeLogin($idUser){
        $num_erros_login = getSession("num_erros_login");
        $ret = null;
        if($num_erros_login) {
            $arr_erros_login = explode(",",$num_erros_login);
            foreach($arr_erros_login as $k=>$reg){
                $dados = explode("=",$reg);
                if($dados[0] == $idUser){
                     $ret = $dados[1];
                }
            }    
        }
        return $ret;
    } 
    public function registarErroDeLogin($login,$urlRedirect=""){
        $user = null;
        $arrUser = Usuario::listar('','id,login,status',"login = '$login' AND status = ".Usuario::ATIVO,'',1);    //print_r(Usuario::getLogSql());        
        if(count($arrUser) > 0)  $user = $arrUser[0];
        if($user){
            $numTent = (int)$this->getNumErrosDeLogin($user->getId());            
            $numTent++;
            if($numTent >= $this->maxErrosLogin){
                $numTent = 0;
                $this->bloquearLogin($user);
                if($urlRedirect) print "<script>window.location = '$urlRedirect';</script>";
                exit;
                //$this->registrarBloqueioDeIp("Usuário atingiu o máximo de tentativas de login no sapo","index.php");
            }else{
                $this->gravarHistoricoSapo($user->getId(),ERRO_AO_LOGAR_NO_SAPO,"Dados de usuário inválidos");
            }
            $this->setNumErrosDeLogin($user->getId(),$numTent);            
            
        } 
    }

    public function notificarDesenvolvedor($tipo,$mensagem){
        if(count($this->emailsNotificacoes) > 0 && $this->sendEmail){
            $dadosEmail = array();
            $dadosEmail["emailFrom"] = EMAIL_FROM_DEFAULT;
            $dadosEmail["fromName"] = "Monitor Atomica Studio";
            if($tipo == self::TENTAVIVA_LOGIN_SAPO){
                $dadosEmail["subject"] = "Bloqueio de login do sapo"; 
            }else{
                $dadosEmail["subject"] = "Bloqueio de acesso ao site"; 
            }
                       
            $mensagem .= "<br /><br />SITE: ".ENDERECO_SITE.""; 
            $mensagem .= "<br />DATA: ".date("Y-m-d H:i:s"); 
            $dadosEmail["mensagem"] = $mensagem;        
            foreach($this->emailsNotificacoes as $email){
                $dadosEmail["email"] = $email;
                enviarEmail($dadosEmail);
            }
        }        
    }
    public function gravarHistoricoSapo($idUsuario,$acao,$obs){
        $hst = new Historico();
        $hst->setIdAcaoHistorico($acao);
        $hst->setStatus(ATIVO);
        $hst->setIdConteudo($idUsuario);
        $hst->setIdSecao(USUARIOS);//constante pertinente à classe
        $hst->setIdUsuario($idUsuario);
        $hst->setData(date("Y-m-d H:i:s "));
        $hst->setIp($_SERVER['REMOTE_ADDR']);
        $hst->setObservacao($obs);
        $hst->salvar();
    }
}

?>
