<?

class ControleMedia{    
	private $error;
	private $diretorio;
	private $porporcao;
	public $msgErroFrmt;
	public $limitMaxTam = 7;
	//const UPLOAD_SUCESS = 1;
	const ARQUIVO_NAO_SETADO = 2;
	const FORMATO_INVALIDO = 3;
	const ULTRAPASSOU_LIMITE_PESO = 4;
	function setPorporcao($porporcao){
		$this->porporcao = $porporcao;
	}
	function getPorporcao(){
		return $this->porporcao;
	}
        function getPrefProporcao(){
            foreach($this->porporcao->dimensoes as $k=>$d){
               return $k;
            }
        }
	function setDiretorioUpload($inDiretorio){
		$this->diretorio = $inDiretorio;
	}	
	function getDiretorio(){
		return $this->diretorio;
	}
	function getDiretorioAbsoluto(){
		return $_SERVER['DOCUMENT_ROOT'].$this->diretorio;
	}
        function getErrosRequest($arquivo){
            if($arquivo && $arquivo["error"]){
                switch($arquivo["error"]){
                    case UPLOAD_ERR_FORM_SIZE:
                    case UPLOAD_ERR_INI_SIZE:
                        return "O tamanho de seu arquivo ultrapassa o limite de ".(LIMITE_MAX_UPLOAD/1024/1024)."MB! Envie outro arquivo";
                    break; 
                    case UPLOAD_ERR_OK:
                        return "";
                        //return "sem erros";
                    break;  
                    case UPLOAD_ERR_PARTIAL:
                         return "O upload do arquivo foi feito parcialmente! tente novamente";   
                    break;
                    case UPLOAD_ERR_NO_FILE:
                        return "";
                        //return "Arquivo não indexado!";
                    break;                   
                }
                return "Ocorreu um erro ao subir o arquivo, tente novamente!";
            }            
        }
        function getMensagemError(){
            
            switch($this->error){
                case self::FORMATO_INVALIDO:
                    return "Arquivo com formato inválido!".$this->msgErroFrmt;
                break;
                case self::ARQUIVO_NAO_SETADO:
                    return "Arquivo não selecionado!";
                break;
                case self::ULTRAPASSOU_LIMITE_PESO:
                    return "Arquivo ultrapassou limite máximo de tamanho, que é de ".$this->limitMaxTam."MB!";
                break;
            }
        }
	public function fazerUpLoad($file,$novoNome="",$filtroExt=""){            
            $arquivo = isset($file) ? $file : false;
            if($arquivo){               
                if($filtroExt){
                    if(!validaMimeType($arquivo,$filtroExt)){
                        $strExt = "";
                        foreach($filtroExt as $k=>$ext){
                            if($k > 0 && $k < count($filtroExt)) $strExt .= ",";
                            if($k == count($filtroExt)) $strExt .= " ou";
                            $strExt .= " .".$ext;
                        }
                        $this->msgErroFrmt = "<br />Utilize apenas os formatos:$strExt";
                        $this->error = self::FORMATO_INVALIDO;
                        return false;
                    }
                }
                /*$strVrf = file_get_contents($arquivo["tmp_name"]);  
                $this->msgErroFrmt = indexOf($strVrf,"<%");
                if(indexOf($strVrf,"<?") != -1 || indexOf($strVrf,"<%") != -1){
                    $this->error = self::FORMATO_INVALIDO;
                    //$this->msgErroFrmt = "<br />Arquivo com conteúdo inválido";
                    return false;
                }*/
                
                $sizeMb = (int)(((int)$arquivo["size"] / 1024) / 1024); 
                //print $sizeMb;exit;
                if($sizeMb > $this->limitMaxTam){
                    $this->error = self::ULTRAPASSOU_LIMITE_PESO;
                    return false;
                }
                if($novoNome){
                    $nome_arq = $novoNome;
                }else{									
                    $nome_arq = md5(uniqid(time()));
                }
                $ext = explode(".",$arquivo["name"]);	
                if(count($ext)==1){
                    $nome_arq = $nome_arq . "." . $ext[1];
                }else{
                    $nome_arq = $nome_arq . "." . $ext[count($ext)-1];
                }	
                $url_media = $this->diretorio."/".$nome_arq;
                $url_abs_media = $_SERVER['DOCUMENT_ROOT'].$url_media;
                move_uploaded_file($arquivo["tmp_name"], $url_abs_media) or die("Erro ao subir imagem");
                if(strstr($arquivo["type"],"image")){
                    $this->duplicarArquivo($nome_arq,"orig_".$nome_arq);
                    $this->gerarDimensao($nome_arq,'800x800','');
                    if($this->porporcao){
                       $this->gerarDimensoes($this->porporcao->dimensoes,$nome_arq);
                    }
                }
            return $nome_arq;	
        }else{
            $this->error = self::ARQUIVO_NAO_SETADO;
            return "";
        }
    }

    function gerarDimensoes($arrDimensoes,$nomeArquivo){
        if($arrDimensoes){
            foreach($arrDimensoes as $pref=>$d){
              // print  "$nomeArquivo, $d, $pref \n";
               $this->gerarDimensao($nomeArquivo,$d,$pref);
            }
            if(preg_match("/^temp_/msi",$nomeArquivo)) unlink($this->getDiretorioAbsoluto()."/".$nomeArquivo);
        }
    }
    function gerarDimensao($nomeArquivo,$dimensao,$pref=""){
        $val = explode("x",$dimensao);
        $img = new Thumbnail($this->getDiretorioAbsoluto()."/".$nomeArquivo);
        $img->redimencionar($val[0],$val[1]);
        if(preg_match("/^temp_/msi",$nomeArquivo)) $nomeArquivo = str_replace("temp_","",$nomeArquivo);
        $img->gerar($this->getDiretorioAbsoluto()."/".$pref.$nomeArquivo);
    }

    public function autoCrop($nomeArquivo,$dimensao,$qualid=100){
        $val = explode("x",$dimensao);
        $img = new Thumbnail($this->getDiretorioAbsoluto()."/".$pref.$nomeArquivo);
        $img->autoCrop($val[0],$val[1]);
        $img->gerar($this->getDiretorioAbsoluto()."/temp_".$pref.$nomeArquivo,false);
        $ret = $pref.$nomeArquivo;
        if($this->porporcao && $ret){
           $this->gerarDimensoes($this->porporcao->dimensoes,"temp_".$ret);
        }
        return $ret;
    }

    public function crop($nomeArquivo,$w,$h,$x,$y,$pref="",$qualid=100){
        $img = new Thumbnail($this->getDiretorioAbsoluto()."/".$pref.$nomeArquivo);
        $img->crop($w,$h,$x,$y);
        $img->gerar($this->getDiretorioAbsoluto()."/temp_".$pref.$nomeArquivo,false);
        $ret = $pref.$nomeArquivo;
        if($this->porporcao && $ret){
           $this->gerarDimensoes($this->porporcao->dimensoes,"temp_".$ret);
        }
        return $ret;
    }

    public function duplicarArquivo($nomeArquivo,$novoNome=""){
            $novoNome = $novoNome?$novoNome:$nomeArquivo."(2)";
            copy($_SERVER['DOCUMENT_ROOT']."/".$this->getDiretorio()."/".$nomeArquivo,$_SERVER['DOCUMENT_ROOT']."/".$this->getDiretorio()."/".$novoNome);
    }
	
}
?>