<?
class Assunto{
	private $id;
	private $idSecao;
	private $tipo;
	private $nome;
	private $idioma;
	private $status;
	private $data;
	public static $logSql;
	private $oldValues = Array('id'=>'','id_secao'=>'','tipo'=>'','nome'=>'','idioma'=>'','status'=>'','data'=>'');

	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdSecao($inIdSecao){
		$this->idSecao = $inIdSecao;
	}

	public function getIdSecao(){
		return $this->idSecao;
	}

	public function setTipo($inTipo){
		$this->tipo = $inTipo;
	}

	public function getTipo(){
		return $this->tipo;
	}

	public function setNome($inNomePt){
		$this->nome = $inNomePt;
	}

	public function getNome(){
		return $this->nome;
	}

	public function setIdioma($valor){
		$this->idioma = $valor;
	}

	public function getIdioma(){
		return $this->idioma;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setData($inData){
		$this->data = $inData;
	}

	public function getData(){
		return $this->data;
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_assunto');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['id_secao'])) $this->idSecao=$conteudo['id_secao'];
		if(isset($conteudo['tipo'])) $this->tipo=$conteudo['tipo'];
		if(isset($conteudo['nome'])) $this->nome=$conteudo['nome'];
		if(isset($conteudo['idioma'])) $this->idioma=$conteudo['idioma'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		if(isset($conteudo['data'])) $this->data=$conteudo['data'];
		
		foreach($conteudo as $k=>$news){ $this->oldValues[$k] = $news;} 
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || $this->oldValues['id_secao'] != $this->idSecao){ $campo[] = 'id_secao';  $valor[] = "'$this->idSecao'"; }
		if(!$this->id || $this->oldValues['tipo'] != $this->tipo){ $campo[] = 'tipo';  $valor[] = "'$this->tipo'"; }
		if(!$this->id || $this->oldValues['nome'] != $this->nome){ $campo[] = 'nome';  $valor[] = "'$this->nome'"; }
		if(!$this->id || $this->oldValues['idioma'] != $this->idioma){ $campo[] = 'idioma';  $valor[] = "'$this->idioma'"; }
		if(!$this->id || $this->oldValues['status'] != $this->status){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		if(!$this->id || $this->oldValues['data'] != $this->data){ $campo[] = 'data';  $valor[] = "'$this->data'"; }
		
		$db=new DB('tbl_assunto');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return Assunto::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_assunto');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($idSecao='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_assunto');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new Assunto($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = Assunto::listar("",$campos,"id = '$id'",'','1');
		return isset($obj[0])?$obj[0]:null;
	}

	public static function listarXML($idSecao='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_assunto');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<Assunto id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</Assunto>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($idSecao='',$where=''){
		$db=new DB('tbl_assunto');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao();//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao();//observação pertinente à classe
		$hst->salvar();
	}

}
?>