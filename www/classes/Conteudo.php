<?
class Conteudo{
	private $id;
	private $idSecao;
	private $dataInclusao;
	private $dataPublicacao;
	private $status;
	public static $logSql;
	private $oldValues = Array();
	
	const SALVO = 2;
	const PUBLICADO = 1;
	const INATIVO = 0;
	
	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdSecao($inIdSecao){
		$this->idSecao = $inIdSecao;
	}

	public function getIdSecao(){
		return $this->idSecao;
	}

	public function setDataInclusao($inDataInclusao){
		$this->dataInclusao = $inDataInclusao;
	}

	public function getDataInclusao(){
		return $this->dataInclusao;
	}

	public function setDataPublicacao($inDataPublicacao){
		$this->dataPublicacao = $inDataPublicacao;
	}

	public function getDataPublicacao(){
		return $this->dataPublicacao;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus(){
		return $this->status;
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_conteudo');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['id_secao'])) $this->idSecao=$conteudo['id_secao'];
		if(isset($conteudo['data_inclusao'])) $this->dataInclusao=$conteudo['data_inclusao'];
		if(isset($conteudo['data_publicacao'])) $this->dataPublicacao=$conteudo['data_publicacao'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		
		$this->oldValues = $conteudo;
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || (isset($this->oldValues['id_secao']) && $this->oldValues['id_secao'] != $this->idSecao)){ $campo[] = 'id_secao';  $valor[] = "'$this->idSecao'"; }
		if(!$this->id || (isset($this->oldValues['data_inclusao']) && $this->oldValues['data_inclusao'] != $this->dataInclusao)){ $campo[] = 'data_inclusao';  $valor[] = "'$this->dataInclusao'"; }
		if(!$this->id || (isset($this->oldValues['data_publicacao']) && $this->oldValues['data_publicacao'] != $this->dataPublicacao)){ $campo[] = 'data_publicacao';  $valor[] = "'$this->dataPublicacao'"; }
		if(!$this->id || (isset($this->oldValues['status']) && $this->oldValues['status'] != $this->status)){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		
		
		$db=new DB('tbl_conteudo');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return Conteudo::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_conteudo');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($idSecao='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_conteudo');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new Conteudo($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = Conteudo::listar("",$campos,"id = '$id'",'','1');
		return isset($obj[0])?$obj[0]:null;
	}

	public static function listarXML($idSecao='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_conteudo');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<Conteudo id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</Conteudo>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($idSecao='',$where=''){
		$db=new DB('tbl_conteudo');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao();//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao();//observação pertinente à classe
		$hst->salvar();
	}

}
?>