<?
class UsuarioPermissao{
	private $id;
	private $idUsuario;
	private $idPai;
	private $idPermissao;
	private $status;
	public static $logSql;
	private $oldValues = Array();

	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdUsuario($inIdUsuario){
		$this->idUsuario = $inIdUsuario;
	}

	public function getIdUsuario(){
		return $this->idUsuario;
	}

	public function setIdPai($inIdPai){
		$this->idPai = $inIdPai;
	}

	public function getIdPai(){
		return $this->idPai;
	}

	public function setIdPermissao($inIdPermissao){
		$this->idPermissao = $inIdPermissao;
	}

	public function getIdPermissao(){
		return $this->idPermissao;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus(){
		return $this->status;
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_usuario_permissao');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['id_usuario'])) $this->idUsuario=$conteudo['id_usuario'];
		if(isset($conteudo['id_pai'])) $this->idPai=$conteudo['id_pai'];
		if(isset($conteudo['id_permissao'])) $this->idPermissao=$conteudo['id_permissao'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		
		$this->oldValues = $conteudo;
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || (isset($this->oldValues['id_usuario']) && $this->oldValues['id_usuario'] != $this->idUsuario)){ $campo[] = 'id_usuario';  $valor[] = "'$this->idUsuario'"; }
		if(!$this->id || (isset($this->oldValues['id_pai']) && $this->oldValues['id_pai'] != $this->idPai)){ $campo[] = 'id_pai';  $valor[] = "'$this->idPai'"; }
		if(!$this->id || (isset($this->oldValues['id_permissao']) && $this->oldValues['id_permissao'] != $this->idPermissao)){ $campo[] = 'id_permissao';  $valor[] = "'$this->idPermissao'"; }
		if(!$this->id || (isset($this->oldValues['status']) && $this->oldValues['status'] != $this->status)){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		
		
		$db=new DB('tbl_usuario_permissao');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return UsuarioPermissao::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_usuario_permissao');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($idUsuario='',$idPai='',$idPermissao='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_usuario_permissao');
						
		if(strlen($idUsuario)>0){
			if($where) $where .= " AND ";
			$where .= "id_usuario = '$idUsuario'";	
		}				
		if(strlen($idPai)>0){
			if($where) $where .= " AND ";
			$where .= "id_pai = '$idPai'";	
		}				
		if(strlen($idPermissao)>0){
			if($where) $where .= " AND ";
			$where .= "id_permissao = '$idPermissao'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new UsuarioPermissao($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = UsuarioPermissao::listar("","","",$campos,"id = '$id'",'','1');
		return isset($obj[0])?$obj[0]:null;
	}

	public static function listarXML($idUsuario='',$idPai='',$idPermissao='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_usuario_permissao');
						
		if(strlen($idUsuario)>0){
			if($where) $where .= " AND ";
			$where .= "id_usuario = '$idUsuario'";	
		}				
		if(strlen($idPai)>0){
			if($where) $where .= " AND ";
			$where .= "id_pai = '$idPai'";	
		}				
		if(strlen($idPermissao)>0){
			if($where) $where .= " AND ";
			$where .= "id_permissao = '$idPermissao'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<UsuarioPermissao id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</UsuarioPermissao>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($idUsuario='',$idPai='',$idPermissao='',$where=''){
		$db=new DB('tbl_usuario_permissao');
						
		if(strlen($idUsuario)>0){
			if($where) $where .= " AND ";
			$where .= "id_usuario = '$idUsuario'";	
		}				
		if(strlen($idPai)>0){
			if($where) $where .= " AND ";
			$where .= "id_pai = '$idPai'";	
		}				
		if(strlen($idPermissao)>0){
			if($where) $where .= " AND ";
			$where .= "id_permissao = '$idPermissao'";	
		}
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao();//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao();//observação pertinente à classe
		$hst->salvar();
	}
	public static function lerByPermissao($idUser,$idPerm,$idPai){
            $db=new DB('tbl_usuario_permissao');
            $db->select("*","id_permissao = '$idPerm' AND id_pai = '$idPai' AND id_usuario = '$idUser'");
            $objeto = null;
            while($conteudo = $db->fetchArray()){
                $objeto = new UsuarioPermissao($conteudo); 
            }
           // print $db->log;
            //$db->printLog("log","w");
            return $objeto;	  
        }

}
?>