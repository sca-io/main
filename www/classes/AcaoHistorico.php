<?
class AcaoHistorico{
	private $id;
	private $acao;
	public static $logSql;
	private $oldValues = Array();

	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setAcao($inAcao){
		$this->acao = $inAcao;
	}

	public function getAcao(){
		return $this->acao;
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_acao_historico');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['acao'])) $this->acao=$conteudo['acao'];
		
		$this->oldValues = $conteudo;
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || (isset($this->oldValues['acao']) && $this->oldValues['acao'] != $this->acao)){ $campo[] = 'acao';  $valor[] = "'$this->acao'"; }
		
		
		$db=new DB('tbl_acao_historico');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return AcaoHistorico::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_acao_historico');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_acao_historico');
		
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new AcaoHistorico($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = AcaoHistorico::listar($campos,"id = '$id'",'','1');
		return isset($obj[0])?$obj[0]:null;
	}

	public static function listarXML($campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_acao_historico');
		
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<AcaoHistorico id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</AcaoHistorico>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($where=''){
		$db=new DB('tbl_acao_historico');
		
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao();//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao();//observação pertinente à classe
		$hst->salvar();
	}

}
?>