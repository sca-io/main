<?
class CadGestaoPessoa{
	private $id;
	private $idioma;
	private $nome;
	private $endereco;
	private $num;
	private $comp;
	private $cidade;
	private $estado;
	private $telefone;
	private $email;
	private $area;
	private $outros;
	private $formacaoAcad;
	private $curso;
	private $cursoAtual;
	private $idiomas;
	private $dadosProfiss;
	private $dataInclusao;
	private $status;
	public static $logSql;
	private $oldValues = Array('id'=>'','idioma'=>'','nome'=>'','endereco'=>'','num'=>'','comp'=>'','cidade'=>'','estado'=>'','telefone'=>'','email'=>'','area'=>'','outros'=>'','formacao_acad'=>'','curso'=>'','curso_atual'=>'','idiomas'=>'','dados_profiss'=>'','data_inclusao'=>'','status'=>'');
        const NAOAVALIADO = 2;
	const LIDO = 1;
	const INATIVO = 0;
	
	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdioma($inIdioma){
		$this->idioma = $inIdioma;
	}

	public function getIdioma(){
		return $this->idioma;
	}

	public function setNome($inNome){
		$this->nome = $inNome;
	}

	public function getNome(){
		return $this->nome;
	}

	public function setEndereco($inEndereco){
		$this->endereco = $inEndereco;
	}

	public function getEndereco(){
		return $this->endereco;
	}

	public function setNum($inNum){
		$this->num = $inNum;
	}

	public function getNum(){
		return $this->num;
	}

	public function setComp($inComp){
		$this->comp = $inComp;
	}

	public function getComp(){
		return $this->comp;
	}

	public function setCidade($inCidade){
		$this->cidade = $inCidade;
	}

	public function getCidade(){
		return $this->cidade;
	}

	public function setEstado($inEstado){
		$this->estado = $inEstado;
	}

	public function getEstado(){
		return $this->estado;
	}

	public function setTelefone($inTelefone){
		$this->telefone = $inTelefone;
	}

	public function getTelefone(){
		return $this->telefone;
	}

	public function setEmail($inEmail){
		$this->email = $inEmail;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setArea($inArea){
		$this->area = $inArea;
	}

	public function getArea(){
		return $this->area;
	}

	public function setOutros($inOutros){
		$this->outros = $inOutros;
	}

	public function getOutros(){
		return $this->outros;
	}

	public function setFormacaoAcad($inFormacaoAcad){
		$this->formacaoAcad = $inFormacaoAcad;
	}

	public function getFormacaoAcad(){
		return $this->formacaoAcad;
	}

	public function setCurso($inCurso){
		$this->curso = $inCurso;
	}

	public function getCurso(){
		return $this->curso;
	}

	public function setCursoAtual($inCursoAtual){
		$this->cursoAtual = $inCursoAtual;
	}

	public function getCursoAtual(){
		return $this->cursoAtual;
	}

	public function setIdiomas($inIdiomas){
		$this->idiomas = $inIdiomas;
	}

	public function getIdiomas(){
		return $this->idiomas;
	}

	public function setDadosProfiss($inDadosProfiss){
		$this->dadosProfiss = $inDadosProfiss;
	}

	public function getDadosProfiss(){
		return $this->dadosProfiss;
	}

	public function setDataInclusao($inDataInclusao){
		$this->dataInclusao = $inDataInclusao;
	}

	public function getDataInclusao(){
		return $this->dataInclusao;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus($string=false){
		if($string){
			switch($this->status){
                            case self::LIDO:
                                return "Lido";
                            break;
                            case self::NAOAVALIADO:
                                return "Não Avaliado";
                            break;
                            default:
                                return "Inativo";
                        }

		}else{
			return $this->status;
		}
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_cad_gestao_pessoa');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['idioma'])) $this->idioma=$conteudo['idioma'];
		if(isset($conteudo['nome'])) $this->nome=$conteudo['nome'];
		if(isset($conteudo['endereco'])) $this->endereco=$conteudo['endereco'];
		if(isset($conteudo['num'])) $this->num=$conteudo['num'];
		if(isset($conteudo['comp'])) $this->comp=$conteudo['comp'];
		if(isset($conteudo['cidade'])) $this->cidade=$conteudo['cidade'];
		if(isset($conteudo['estado'])) $this->estado=$conteudo['estado'];
		if(isset($conteudo['telefone'])) $this->telefone=$conteudo['telefone'];
		if(isset($conteudo['email'])) $this->email=$conteudo['email'];
		if(isset($conteudo['area'])) $this->area=$conteudo['area'];
		if(isset($conteudo['outros'])) $this->outros=$conteudo['outros'];
		if(isset($conteudo['formacao_acad'])) $this->formacaoAcad=$conteudo['formacao_acad'];
		if(isset($conteudo['curso'])) $this->curso=$conteudo['curso'];
		if(isset($conteudo['curso_atual'])) $this->cursoAtual=$conteudo['curso_atual'];
		if(isset($conteudo['idiomas'])) $this->idiomas=$conteudo['idiomas'];
		if(isset($conteudo['dados_profiss'])) $this->dadosProfiss=$conteudo['dados_profiss'];
		if(isset($conteudo['data_inclusao'])) $this->dataInclusao=$conteudo['data_inclusao'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		
		foreach($conteudo as $k=>$news){ $this->oldValues[$k] = $news;} 
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || $this->oldValues['idioma'] != $this->idioma){ $campo[] = 'idioma';  $valor[] = "'$this->idioma'"; }
		if(!$this->id || $this->oldValues['nome'] != $this->nome){ $campo[] = 'nome';  $valor[] = "'$this->nome'"; }
		if(!$this->id || $this->oldValues['endereco'] != $this->endereco){ $campo[] = 'endereco';  $valor[] = "'$this->endereco'"; }
		if(!$this->id || $this->oldValues['num'] != $this->num){ $campo[] = 'num';  $valor[] = "'$this->num'"; }
		if(!$this->id || $this->oldValues['comp'] != $this->comp){ $campo[] = 'comp';  $valor[] = "'$this->comp'"; }
		if(!$this->id || $this->oldValues['cidade'] != $this->cidade){ $campo[] = 'cidade';  $valor[] = "'$this->cidade'"; }
		if(!$this->id || $this->oldValues['estado'] != $this->estado){ $campo[] = 'estado';  $valor[] = "'$this->estado'"; }
		if(!$this->id || $this->oldValues['telefone'] != $this->telefone){ $campo[] = 'telefone';  $valor[] = "'$this->telefone'"; }
		if(!$this->id || $this->oldValues['email'] != $this->email){ $campo[] = 'email';  $valor[] = "'$this->email'"; }
		if(!$this->id || $this->oldValues['area'] != $this->area){ $campo[] = 'area';  $valor[] = "'$this->area'"; }
		if(!$this->id || $this->oldValues['outros'] != $this->outros){ $campo[] = 'outros';  $valor[] = "'$this->outros'"; }
		if(!$this->id || $this->oldValues['formacao_acad'] != $this->formacaoAcad){ $campo[] = 'formacao_acad';  $valor[] = "'$this->formacaoAcad'"; }
		if(!$this->id || $this->oldValues['curso'] != $this->curso){ $campo[] = 'curso';  $valor[] = "'$this->curso'"; }
		if(!$this->id || $this->oldValues['curso_atual'] != $this->cursoAtual){ $campo[] = 'curso_atual';  $valor[] = "'$this->cursoAtual'"; }
		if(!$this->id || $this->oldValues['idiomas'] != $this->idiomas){ $campo[] = 'idiomas';  $valor[] = "'$this->idiomas'"; }
		if(!$this->id || $this->oldValues['dados_profiss'] != $this->dadosProfiss){ $campo[] = 'dados_profiss';  $valor[] = "'$this->dadosProfiss'"; }
		if(!$this->id || $this->oldValues['data_inclusao'] != $this->dataInclusao){ $campo[] = 'data_inclusao';  $valor[] = "'$this->dataInclusao'"; }
		if(!$this->id || $this->oldValues['status'] != $this->status){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		
		
		$db=new DB('tbl_cad_gestao_pessoa');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return CadGestaoPessoa::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_cad_gestao_pessoa');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_cad_gestao_pessoa');
		
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new CadGestaoPessoa($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = CadGestaoPessoa::listar($campos,"id = '$id'",'','1');
		return isset($obj[0])?$obj[0]:null;
	}

	public static function listarXML($campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_cad_gestao_pessoa');
		
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<CadGestaoPessoa id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</CadGestaoPessoa>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($where=''){
		$db=new DB('tbl_cad_gestao_pessoa');
		
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		/*$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao();//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao();//observação pertinente à classe
		$hst->salvar();*/
	}

}
?>