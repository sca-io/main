<?
class Usuario{
    private $id;
    private $nome;
    private $senha;
    private $status;
    private $dtUltimoLogin;
    private $login;
    private $idTipo;
    private $email;
    public static $logSql;
    private $oldValues = Array();
    const BLOQUEADO = 2;
    const ATIVO = 1;
    const INATIVO = 0;
    public static $erroLogin;
    public static $userLogado;

    public function setId($inId){
            $this->id = $inId;
    }

    public function getId(){
            return $this->id;
    }

    public function setNome($inNome){
            $this->nome = $inNome;
    }

    public function getNome(){
            return $this->nome;
    }

    public function setSenha($inSenha){
            $this->senha = $inSenha;
    }

    public function getSenha(){
            return $this->senha;
    }

    public function setStatus($inStatus){
            $this->status = $inStatus;
    }

    public function getStatus($string=false){
        if($string){
            switch($this->status){
                case self::ATIVO:
                    return "Ativo";
                break;
                case self::BLOQUEADO:
                    return "Bloqueado";
                break;
                default:
                    return "Inativo";
            }
        }else{
            return $this->status;
        }
    }

    public function setDtUltimoLogin($inDtUltimoLogin){
            $this->dtUltimoLogin = $inDtUltimoLogin;
    }

    public function getDtUltimoLogin(){
            return $this->dtUltimoLogin;
    }

    public function setLogin($inLogin){
            $this->login = $inLogin;
    }

    public function getLogin(){
            return $this->login;
    }

    public function setIdTipo($inIdTipo){
            $this->idTipo = $inIdTipo;
    }

    public function getIdTipo(){
            return $this->idTipo;
    }

    public function setEmail($inEmail){
            $this->email = $inEmail;
    }

    public function getEmail(){
            return $this->email;
    }

    public function __construct($conteudo=''){
            $this->db=new DB('tbl_usuario');
            if(!is_array($conteudo)){
                    $conteudo = Array();
            }
            if(isset($conteudo['id'])) $this->id=$conteudo['id'];
            if(isset($conteudo['nome'])) $this->nome=$conteudo['nome'];
            if(isset($conteudo['senha'])) $this->senha=$conteudo['senha'];
            if(isset($conteudo['status'])) $this->status=$conteudo['status'];
            if(isset($conteudo['dt_ultimo_login'])) $this->dtUltimoLogin=$conteudo['dt_ultimo_login'];
            if(isset($conteudo['login'])) $this->login=$conteudo['login'];
            if(isset($conteudo['id_tipo'])) $this->idTipo=$conteudo['id_tipo'];
            if(isset($conteudo['email'])) $this->email=$conteudo['email'];

            $this->oldValues = $conteudo;
    }

    public static function setLogSql($valor){

                    self::$logSql = $valor;

    }

    public static function getLogSql(){
            return self::$logSql;
    }

    public function salvar(){

            $campo = array();
            $valor = array();
            if(!$this->id || (isset($this->oldValues['nome']) && $this->oldValues['nome'] != $this->nome)){ $campo[] = 'nome';  $valor[] = "'$this->nome'"; }

            if(!$this->id || (isset($this->oldValues['status']) && $this->oldValues['status'] != $this->status)){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
            if(!$this->id || (isset($this->oldValues['dt_ultimo_login']) && $this->oldValues['dt_ultimo_login'] != $this->dtUltimoLogin)){ $campo[] = 'dt_ultimo_login';  $valor[] = "'$this->dtUltimoLogin'"; }
            if(!$this->id || (isset($this->oldValues['login']) && $this->oldValues['login'] != $this->login)){ $campo[] = 'login';  $valor[] = "'$this->login'"; }
            if(!$this->id || (isset($this->oldValues['id_tipo']) && $this->oldValues['id_tipo'] != $this->idTipo)){ $campo[] = 'id_tipo';  $valor[] = "'$this->idTipo'"; }
            if(!$this->id || (isset($this->oldValues['email']) && $this->oldValues['email'] != $this->email)){ $campo[] = 'email';  $valor[] = "'$this->email'"; }


            $db=new DB('tbl_usuario');
            if(!$this->id){
                $campo[] = 'senha';  $valor[] = "PASSWORD('$this->senha')";
                $db->insert($campo,$valor);
                $this->id =  $db->insertId;
            }else{
                    $db->update($campo,$valor,"id = '$this->id'");
            }
            self::setLogSql($db->log);
            //print $db->log;exit;
            return true;
    }

    public function excluir(){
            return Usuario::delete($this->id);
    }

    public static function delete($id){
            $db=new DB('tbl_usuario');
            if(trim(strlen($id)>0)){
                    $db->delete("id = '$id'");
            }
            return true;
    }

    public static function listar($idTipo='',$campos='*',$where='',$ordem='',$paginacao=''){
            $db=new DB('tbl_usuario');

            if(strlen($idTipo)>0){
                    if($where) $where .= " AND ";
                    $where .= "id_tipo = '$idTipo'";	
            }
            $db->select($campos,$where,$ordem,$paginacao);	
            $lista = array();	
            while($conteudo = $db->fetchArray()){			
                    $lista[] = new Usuario($conteudo);
            }
            self::setLogSql($db->log);
            //$db->printLog();
            return $lista;
    }

    public static function ler($id='',$campos='*'){
            $obj = Usuario::listar("",$campos,"id = '$id'",'','1');
            return isset($obj[0])?$obj[0]:null;
    }

    public static function listarXML($idTipo='',$campos='*',$where='',$ordem='',$paginacao=''){
            $db=new DB('tbl_usuario');

            if(strlen($idTipo)>0){
                    if($where) $where .= " AND ";
                    $where .= "id_tipo = '$idTipo'";	
            }
            $db->select($campos,$where,$ordem,$paginacao);		
            while($conteudo = $db->fetchArray()){
                    $lista .= "<Usuario id='$conteudo[id]'>";
                    for($i = 0; $i < count($conteudo)/2; $i++ ){				
                            $n = mysql_field_name($db->result,$i);
                            $valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
                            $lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
                    }
                    $lista .= "</Usuario>";
            }
            self::setLogSql($db->log);
            //$db->printLog();
            return $lista;
    }

    public static function countListar($idTipo='',$where=''){
            $db=new DB('tbl_usuario');

            if(strlen($idTipo)>0){
                    if($where) $where .= " AND ";
                    $where .= "id_tipo = '$idTipo'";	
            }
            $res = $db->nRegistros($where); 
            self::setLogSql($db->log);
            //$db->printLog();
            return $res;
    }

    public function inserirHistorico($idUsuario,$ip,$acaoHistorico,$obsExtra=""){
            $hst = new Historico();
            $hst->setIdAcaoHistorico($acaoHistorico);
            $hst->setStatus(ATIVO);
            $hst->setIdConteudo($this->getId());
            $hst->setIdSecao(USUARIOS);//constante pertinente à classe
            $hst->setIdUsuario($idUsuario);
            $hst->setData(date("Y-m-d H:i:s "));
            $hst->setIp($ip);
            $hst->setObservacao($this->nome.$obsExtra);//observação pertinente à classe
            $hst->salvar();
    }

    function getPermissoes($idPai){
        $perm = array();
            $lista = UsuarioPermissao::listar($this->id,$idPai,"","","status = ".ATIVO);
            //print UsuarioPermissao::getLogSql();
            if($lista){			
                    foreach($lista as $l){
                            $perm[] = $l->getIdPermissao();
                    }	
            }		
            return $perm;	
    }
    function hasPermissaoInArray($idPermissao,$arrPerm){
            if(count($arrPerm)>0)		
                    return in_array($idPermissao,$arrPerm);
            else 
                    return false;
    }
    function hasPermissao($idPermissao,$idPai){
            $arrPerm = $this->getPermissoes($idPai);
            if(count($arrPerm)>0)		
                    return in_array($idPermissao,$arrPerm);
            else 
                    return false;
    }
    public function getTipo(){
        if($this->idTipo == USUARIO){
            return "Usuario";
        }elseif($this->idTipo == ADMINISTRADOR){
            return "Administrador";
        }
    }

    public function alteraSenha(){
        $this->db->update("senha",
            "password('$this->senha')","id = '$this->id'");
        if($this->db->result){
        }
    }
    public static function deletar($ids, $motivo=''){            
        $id = split(",",$ids);
        for($i = 0; $i < count($id);$i++){
           $entUsuario = Usuario::ler($id[$i]);
           $entUsuario->excluir($motivo);
         }
    }

    public function excluirLixeira(){
        $this->db->delete("id = '$this->id'");
        $this->db->getLog('log');
        unset($this);
        if($this->db->result){
            return true;
        }
    }

     public function restaurar(){
        $this->db->update("status",ATIVO,"id = '$this->id'");
        if($this->db->result){
            return true;
        }
    }

    public function logof(){
        setSession('idUsuarioSapo',"");
    }

    public static function logar($login, $senha){
        $login = str_replace("'","",$login);
        $senha = str_replace("'","",$senha);

        $user = Usuario::listar("","id,status","login = '$login' AND senha = password('$senha')","","1");
        if(count($user) > 0){  
            switch($user[0]->getStatus()){
                case Usuario::ATIVO:
                    setSession('idUsuarioSapo',$user[0]->getId());
                    return $user[0]; 
                break;
                case Usuario::BLOQUEADO:
                    setSession('idUsuarioSapoBloq',$user[0]->getId());
                    self::$erroLogin = "Usuário bloqueado";
                    return false;
                break;    
            }                                 
        }
        self::$erroLogin =  "Usuário não encontrado";
        return false;
    }
    
    public static function getUserLogado(){
        if(!self::$userLogado){
            if(getSession('idUsuarioSapo')){  
                self::$userLogado = Usuario::ler(getSession('idUsuarioSapo'));            
                if(self::$userLogado && self::$userLogado->getStatus() != Usuario::ATIVO) self::$userLogado = null;            
            }
        }
        return self::$userLogado;        
    }

    public static function autenticarLogon(){             
        if(!self::getUserLogado()){        
            print "<script>parent.location = 'login.php'</script>";
            exit;
        }
    }

}
?>