<?
class EmailMktDisp{
	private $id;
	private $idioma;
	private $nomeCompleto;
	private $primeiroNome;
	private $email;
	private $enviado;
	private $numViews;
	private $numCliques;
        private $numCliquesLinkAlt;
	public static $logSql;
	private $oldValues = Array('id'=>'','idioma'=>'','nome_completo'=>'','primeiro_nome'=>'','email'=>'','enviado'=>'','num_views'=>'','num_cliques'=>'','num_cliques_link_alt'=>'');
	public static $tableName = "tbl_email_mkt_disp";

	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdioma($inIdioma){
		$this->idioma = $inIdioma;
	}

	public function getIdioma(){
		return $this->idioma;
	}

	public function setNomeCompleto($inNomeCompleto){
		$this->nomeCompleto = $inNomeCompleto;
	}

	public function getNomeCompleto(){
		return $this->nomeCompleto;
	}

	public function setPrimeiroNome($inPrimeiroNome){
		$this->primeiroNome = $inPrimeiroNome;
	}

	public function getPrimeiroNome(){
		return $this->primeiroNome;
	}

	public function setEmail($inEmail){
		$this->email = $inEmail;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setEnviado($inEnviado){
		$this->enviado = $inEnviado;
	}

	public function getEnviado(){
		return $this->enviado;
	}

	public function setNumViews($inNumViews){
		$this->numViews = $inNumViews;
	}

	public function getNumViews(){
		return $this->numViews;
	}

	public function setNumCliques($inNumCliques){
		$this->numCliques = $inNumCliques;
	}

	public function getNumCliques(){
		return $this->numCliques;
	}
        
	public function setNumCliquesLinkAlt($inNumCliques){
		$this->numCliquesLinkAlt = $inNumCliques;
	}

	public function getNumCliquesLinkAlt(){
		return $this->numCliquesLinkAlt;
	}        

	public function __construct($conteudo=''){
		$this->db=new DB(self::$tableName);
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['idioma'])) $this->idioma=$conteudo['idioma'];
		if(isset($conteudo['nome_completo'])) $this->nomeCompleto=$conteudo['nome_completo'];
		if(isset($conteudo['primeiro_nome'])) $this->primeiroNome=$conteudo['primeiro_nome'];
		if(isset($conteudo['email'])) $this->email=$conteudo['email'];
		if(isset($conteudo['enviado'])) $this->enviado=$conteudo['enviado'];
		if(isset($conteudo['num_views'])) $this->numViews=$conteudo['num_views'];
		if(isset($conteudo['num_cliques'])) $this->numCliques=$conteudo['num_cliques'];
                if(isset($conteudo['num_cliques_link_alt'])) $this->numCliquesLinkAlt=$conteudo['num_cliques_link_alt'];
		
		foreach($conteudo as $k=>$news){ $this->oldValues[$k] = $news;} 
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || $this->oldValues['idioma'] != $this->idioma){ $campo[] = 'idioma';  $valor[] = "'$this->idioma'"; }
		if(!$this->id || $this->oldValues['nome_completo'] != $this->nomeCompleto){ $campo[] = 'nome_completo';  $valor[] = "'$this->nomeCompleto'"; }
		if(!$this->id || $this->oldValues['primeiro_nome'] != $this->primeiroNome){ $campo[] = 'primeiro_nome';  $valor[] = "'$this->primeiroNome'"; }
		if(!$this->id || $this->oldValues['email'] != $this->email){ $campo[] = 'email';  $valor[] = "'$this->email'"; }
		if(!$this->id || $this->oldValues['enviado'] != $this->enviado){ $campo[] = 'enviado';  $valor[] = "'$this->enviado'"; }
		if(!$this->id || $this->oldValues['num_views'] != $this->numViews){ $campo[] = 'num_views';  $valor[] = "'$this->numViews'"; }
		if(!$this->id || $this->oldValues['num_cliques'] != $this->numCliques){ $campo[] = 'num_cliques';  $valor[] = "'$this->numCliques'"; }
		if(!$this->id || $this->oldValues['num_cliques_link_alt'] != $this->numCliquesLinkAlt){ $campo[] = 'num_cliques_link_alt';  $valor[] = "'$this->numCliquesLinkAlt'"; }
		
		$db=new DB(self::$tableName);
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return EmailMktDisp::delete($this->id);
	}

	public static function delete($id){
		$db=new DB(self::$tableName);
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB(self::$tableName);
		
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new EmailMktDisp($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = EmailMktDisp::listar($campos,"id = '$id'",'','1');
		return ((isset($obj[0]))?$obj[0]:false);
	}

	public static function listarXML($campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB(self::$tableName);
		
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<EmailMktDisp id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</EmailMktDisp>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($where=''){
		$db=new DB(self::$tableName);
		
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao();//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao();//observação pertinente à classe
		$hst->salvar();
	}

}
?>