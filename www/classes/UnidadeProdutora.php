<?
class UnidadeProdutora{
	private $id;
	private $nome;
	private $estado;
	private $link;
	private $descricao;
	private $posMapa;
	private $idioma;
	private $ordem;
	private $dataInclusao;
	private $status;
	public static $logSql;
	private $oldValues = Array('id'=>'','nome'=>'','estado'=>'','link'=>'','descricao'=>'','pos_mapa'=>'','idioma'=>'','ordem'=>'','data_inclusao'=>'','status'=>'');

	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setNome($inNome){
		$this->nome = $inNome;
	}

	public function getNome(){
		return $this->nome;
	}

	public function setEstado($inEstado){
		$this->estado = $inEstado;
	}

	public function getEstado(){
		return $this->estado;
	}

	public function setLink($inLink){
		$this->link = $inLink;
	}

	public function getLink(){
		return $this->link;
	}

	public function setDescricao($inDescricao){
		$this->descricao = $inDescricao;
	}

	public function getDescricao(){
		return $this->descricao;
	}

	public function setPosMapa($inPosMapa){
		$this->posMapa = $inPosMapa;
	}

	public function getPosMapa(){
		return $this->posMapa;
	}

	public function setIdioma($inIdioma){
		$this->idioma = $inIdioma;
	}

	public function getIdioma(){
		return $this->idioma;
	}

	public function setOrdem($inOrdem){
		$this->ordem = $inOrdem;
	}

	public function getOrdem(){
		return $this->ordem;
	}

	public function setDataInclusao($inDataInclusao){
		$this->dataInclusao = $inDataInclusao;
	}

	public function getDataInclusao(){
		return $this->dataInclusao;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus(){
		return $this->status;
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_unidade_produtora');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['nome'])) $this->nome=$conteudo['nome'];
		if(isset($conteudo['estado'])) $this->estado=$conteudo['estado'];
		if(isset($conteudo['link'])) $this->link=$conteudo['link'];
		if(isset($conteudo['descricao'])) $this->descricao=$conteudo['descricao'];
		if(isset($conteudo['pos_mapa'])) $this->posMapa=$conteudo['pos_mapa'];
		if(isset($conteudo['idioma'])) $this->idioma=$conteudo['idioma'];
		if(isset($conteudo['ordem'])) $this->ordem=$conteudo['ordem'];
		if(isset($conteudo['data_inclusao'])) $this->dataInclusao=$conteudo['data_inclusao'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		
		foreach($conteudo as $k=>$news){ $this->oldValues[$k] = $news;} 
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || $this->oldValues['nome'] != $this->nome){ $campo[] = 'nome';  $valor[] = "'$this->nome'"; }
		if(!$this->id || $this->oldValues['estado'] != $this->estado){ $campo[] = 'estado';  $valor[] = "'$this->estado'"; }
		if(!$this->id || $this->oldValues['link'] != $this->link){ $campo[] = 'link';  $valor[] = "'$this->link'"; }
		if(!$this->id || $this->oldValues['descricao'] != $this->descricao){ $campo[] = 'descricao';  $valor[] = "'$this->descricao'"; }
		if(!$this->id || $this->oldValues['pos_mapa'] != $this->posMapa){ $campo[] = 'pos_mapa';  $valor[] = "'$this->posMapa'"; }
		if(!$this->id || $this->oldValues['idioma'] != $this->idioma){ $campo[] = 'idioma';  $valor[] = "'$this->idioma'"; }
		if(!$this->id || $this->oldValues['ordem'] != $this->ordem){ $campo[] = 'ordem';  $valor[] = "'$this->ordem'"; }
		if(!$this->id || $this->oldValues['data_inclusao'] != $this->dataInclusao){ $campo[] = 'data_inclusao';  $valor[] = "'$this->dataInclusao'"; }
		if(!$this->id || $this->oldValues['status'] != $this->status){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		
		
		$db=new DB('tbl_unidade_produtora');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return UnidadeProdutora::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_unidade_produtora');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_unidade_produtora');
		
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new UnidadeProdutora($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = UnidadeProdutora::listar($campos,"id = '$id'",'','1');
		return isset($obj[0])?$obj[0]:null;
	}

	public static function listarXML($campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_unidade_produtora');
		
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<UnidadeProdutora id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</UnidadeProdutora>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($where=''){
		$db=new DB('tbl_unidade_produtora');
		
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao(UNIDADES_PRODUTORAS);//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao($this->nome);//observação pertinente à classe
		$hst->salvar();
	}

}
?>