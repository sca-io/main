<?
class Chamada{
	private $id;
	private $idSecao;
	private $idDestaque;
	private $contSecao;
	private $contId;
	private $idioma;
	private $chapeu;
	private $titulo;
	private $texto;
	private $link;
	private $target;
	private $urlMedia;
	private $legendaMedia;
	private $ordem;
	private $dataInsercao;
	private $dataPublicacao;
	private $status;
	public static $logSql;
	private $oldValues = Array('id'=>'','id_secao'=>'','id_destaque'=>'','cont_secao'=>'','cont_id'=>'','idioma'=>'','chapeu'=>'','titulo'=>'','texto'=>'','link'=>'','target'=>'','url_media'=>'','legenda_media'=>'','ordem'=>'','data_insercao'=>'','data_publicacao'=>'','status'=>'');
	const SALVO = 2;
	const PUBLICADO = 1;
	const INATIVO = 0;
	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdSecao($inIdSecao){
		$this->idSecao = $inIdSecao;
	}

	public function getIdSecao(){
		return $this->idSecao;
	}

	public function setIdDestaque($inIdDestaque){
		$this->idDestaque = $inIdDestaque;
	}

	public function getIdDestaque(){
		return $this->idDestaque;
	}

	public function setContSecao($inContSecao){
		$this->contSecao = $inContSecao;
	}

	public function getContSecao(){
		return $this->contSecao;
	}

	public function setContId($inContId){
		$this->contId = $inContId;
	}

	public function getContId(){
		return $this->contId;
	}

	public function setIdioma($inIdioma){
		$this->idioma = $inIdioma;
	}

	public function getIdioma(){
		return $this->idioma;
	}

	public function setChapeu($inChapeu){
		$this->chapeu = $inChapeu;
	}

	public function getChapeu(){
		return $this->chapeu;
	}

	public function setTitulo($inTitulo){
		$this->titulo = $inTitulo;
	}

	public function getTitulo(){
		return $this->titulo;
	}

	public function setTexto($inTexto){
		$this->texto = $inTexto;
	}

	public function getTexto(){
		return $this->texto;
	}

	public function setLink($inLink){
		$this->link = $inLink;
	}

	public function getLink(){
		return $this->link;
	}

	public function setTarget($inTarget){
		$this->target = $inTarget;
	}

	public function getTarget(){
		return $this->target;
	}

	public function setUrlMedia($inUrlMedia){
		$this->urlMedia = $inUrlMedia;
	}

	public function getUrlMedia(){
		return $this->urlMedia;
	}

	public function setLegendaMedia($inLegendaMedia){
		$this->legendaMedia = $inLegendaMedia;
	}

	public function getLegendaMedia(){
		return $this->legendaMedia;
	}

	public function setOrdem($inOrdem){
		$this->ordem = $inOrdem;
	}

	public function getOrdem(){
		return $this->ordem;
	}

	public function setDataInsercao($inDataInsercao){
		$this->dataInsercao = $inDataInsercao;
	}

	public function getDataInsercao(){
		return $this->dataInsercao;
	}

	public function setDataPublicacao($inDataPublicacao){
		$this->dataPublicacao = $inDataPublicacao;
	}

	public function getDataPublicacao(){
		return $this->dataPublicacao;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus($string=false){
		if($string){
			switch($this->status){
                            case self::PUBLICADO:
                                return "Publicado";
                            break;
                            case self::SALVO:
                                return "Salvo";
                            break;
                            default:
                                return "Inativo";
                        }

		}else{
			return $this->status;
		}
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_chamada');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['id_secao'])) $this->idSecao=$conteudo['id_secao'];
		if(isset($conteudo['id_destaque'])) $this->idDestaque=$conteudo['id_destaque'];
		if(isset($conteudo['cont_secao'])) $this->contSecao=$conteudo['cont_secao'];
		if(isset($conteudo['cont_id'])) $this->contId=$conteudo['cont_id'];
		if(isset($conteudo['idioma'])) $this->idioma=$conteudo['idioma'];
		if(isset($conteudo['chapeu'])) $this->chapeu=$conteudo['chapeu'];
		if(isset($conteudo['titulo'])) $this->titulo=$conteudo['titulo'];
		if(isset($conteudo['texto'])) $this->texto=$conteudo['texto'];
		if(isset($conteudo['link'])) $this->link=$conteudo['link'];
		if(isset($conteudo['target'])) $this->target=$conteudo['target'];
		if(isset($conteudo['url_media'])) $this->urlMedia=$conteudo['url_media'];
		if(isset($conteudo['legenda_media'])) $this->legendaMedia=$conteudo['legenda_media'];
		if(isset($conteudo['ordem'])) $this->ordem=$conteudo['ordem'];
		if(isset($conteudo['data_insercao'])) $this->dataInsercao=$conteudo['data_insercao'];
		if(isset($conteudo['data_publicacao'])) $this->dataPublicacao=$conteudo['data_publicacao'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		
		foreach($conteudo as $k=>$news){ $this->oldValues[$k] = $news;} 
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || $this->oldValues['id_secao'] != $this->idSecao){ $campo[] = 'id_secao';  $valor[] = "'$this->idSecao'"; }
		if(!$this->id || $this->oldValues['id_destaque'] != $this->idDestaque){ $campo[] = 'id_destaque';  $valor[] = "'$this->idDestaque'"; }
		if(!$this->id || $this->oldValues['cont_secao'] != $this->contSecao){ $campo[] = 'cont_secao';  $valor[] = "'$this->contSecao'"; }
		if(!$this->id || $this->oldValues['cont_id'] != $this->contId){ $campo[] = 'cont_id';  $valor[] = "'$this->contId'"; }
		if(!$this->id || $this->oldValues['idioma'] != $this->idioma){ $campo[] = 'idioma';  $valor[] = "'$this->idioma'"; }
		if(!$this->id || $this->oldValues['chapeu'] != $this->chapeu){ $campo[] = 'chapeu';  $valor[] = "'$this->chapeu'"; }
		if(!$this->id || $this->oldValues['titulo'] != $this->titulo){ $campo[] = 'titulo';  $valor[] = "'$this->titulo'"; }
		if(!$this->id || $this->oldValues['texto'] != $this->texto){ $campo[] = 'texto';  $valor[] = "'$this->texto'"; }
		if(!$this->id || $this->oldValues['link'] != $this->link){ $campo[] = 'link';  $valor[] = "'$this->link'"; }
		if(!$this->id || $this->oldValues['target'] != $this->target){ $campo[] = 'target';  $valor[] = "'$this->target'"; }
		if(!$this->id || $this->oldValues['url_media'] != $this->urlMedia){ $campo[] = 'url_media';  $valor[] = "'$this->urlMedia'"; }
		if(!$this->id || $this->oldValues['legenda_media'] != $this->legendaMedia){ $campo[] = 'legenda_media';  $valor[] = "'$this->legendaMedia'"; }
		if(!$this->id || $this->oldValues['ordem'] != $this->ordem){ $campo[] = 'ordem';  $valor[] = "'$this->ordem'"; }
		if(!$this->id || $this->oldValues['data_insercao'] != $this->dataInsercao){ $campo[] = 'data_insercao';  $valor[] = "'$this->dataInsercao'"; }
		if(!$this->id || $this->oldValues['data_publicacao'] != $this->dataPublicacao){ $campo[] = 'data_publicacao';  $valor[] = "'$this->dataPublicacao'"; }
		if(!$this->id || $this->oldValues['status'] != $this->status){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		
		
		$db=new DB('tbl_chamada');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return Chamada::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_chamada');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($idSecao='',$idDestaque='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_chamada');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		if(strlen($idDestaque)>0){
			if($where) $where .= " AND ";
			$where .= "id_destaque = '$idDestaque'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new Chamada($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = Chamada::listar("","",$campos,"id = '$id'",'','1');
		return isset($obj[0])?$obj[0]:null;
	}

	public static function listarXML($idSecao='',$idDestaque='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_chamada');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		if(strlen($idDestaque)>0){
			if($where) $where .= " AND ";
			$where .= "id_destaque = '$idDestaque'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<Chamada id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</Chamada>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($idSecao='',$idDestaque='',$where=''){
		$db=new DB('tbl_chamada');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		if(strlen($idDestaque)>0){
			if($where) $where .= " AND ";
			$where .= "id_destaque = '$idDestaque'";	
		}
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}
	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao($this->idSecao);//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao($this->titulo);//observação pertinente à classe
		$hst->salvar();
	}

}
?>