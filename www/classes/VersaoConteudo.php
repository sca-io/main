<?
class VersaoConteudo{
	private $id;
	private $idSecao;
	private $idConteudo;
	private $assunto;
	private $idioma;
	private $titulo;
	private $texto;
	private $autor;
	private $descricao;
	private $keywords;
	private $imgDestaque;
	private $urlEmbed;
	private $urlAmigavel;
	private $idsRelacionados;
	private $dataInclusao;
	private $dataPublicacao;
	private $status;
	public static $logSql;
	private $oldValues = Array('id'=>'','id_secao'=>'','id_conteudo'=>'','assunto'=>'','idioma'=>'','titulo'=>'','texto'=>'','autor'=>'','descricao'=>'','keywords'=>'','img_destaque'=>'','url_embed'=>'','url_amigavel'=>'','ids_relacionados'=>'','data_inclusao'=>'','data_publicacao'=>'','status'=>'');

        const SALVO = 2;
	const PUBLICADO = 1;
	const INATIVO = 0;
        
	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdSecao($inIdSecao){
		$this->idSecao = $inIdSecao;
	}

	public function getIdSecao(){
		return $this->idSecao;
	}

	public function setIdConteudo($inIdConteudo){
		$this->idConteudo = $inIdConteudo;
	}

	public function getIdConteudo(){
		return $this->idConteudo;
	}

	public function setAssunto($inAssunto){
		$this->assunto = $inAssunto;
	}

	public function getAssunto(){
		return $this->assunto;
	}

	public function setIdioma($inIdioma){
		$this->idioma = $inIdioma;
	}

	public function getIdioma(){
		return $this->idioma;
	}

	public function setTitulo($inTitulo){
		$this->titulo = $inTitulo;
	}

	public function getTitulo(){
		return $this->titulo;
	}

	public function setTexto($inTexto){
		$this->texto = $inTexto;
	}

	public function getTexto(){
		return $this->texto;
	}

	public function setAutor($inAutor){
		$this->autor = $inAutor;
	}

	public function getAutor(){
		return $this->autor;
	}

	public function setDescricao($inDescricao){
		$this->descricao = $inDescricao;
	}

	public function getDescricao(){
		return $this->descricao;
	}

	public function setKeywords($inKeywords){
		$this->keywords = $inKeywords;
	}

	public function getKeywords(){
		return $this->keywords;
	}

	public function setImgDestaque($inImgDestaque){
		$this->imgDestaque = $inImgDestaque;
	}

	public function getImgDestaque(){
		return $this->imgDestaque;
	}

	public function setUrlEmbed($inUrlEmbed){
		$this->urlEmbed = $inUrlEmbed;
	}

	public function getUrlEmbed(){
		return $this->urlEmbed;
	}

	public function setUrlAmigavel($inUrlAmigavel){
		$this->urlAmigavel = $inUrlAmigavel;
	}

	public function getUrlAmigavel(){
		return $this->urlAmigavel;
	}

	public function setIdsRelacionados($inIdsRelacionados){
		$this->idsRelacionados = $inIdsRelacionados;
	}

	public function getIdsRelacionados(){
		return $this->idsRelacionados;
	}

	public function setDataInclusao($inDataInclusao){
		$this->dataInclusao = $inDataInclusao;
	}

	public function getDataInclusao(){
		return $this->dataInclusao;
	}

	public function setDataPublicacao($inDataPublicacao){
		$this->dataPublicacao = $inDataPublicacao;
	}

	public function getDataPublicacao(){
		return $this->dataPublicacao;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus($string=false){
		if($string){
			switch($this->status){
                            case self::PUBLICADO:
                                return "Publicado";
                            break;
                            case self::SALVO:
                                return "Salvo";
                            break;
                            default:
                                return "Inativo";
                        }

		}else{
			return $this->status;
		}
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_versao_conteudo');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['id_secao'])) $this->idSecao=$conteudo['id_secao'];
		if(isset($conteudo['id_conteudo'])) $this->idConteudo=$conteudo['id_conteudo'];
		if(isset($conteudo['assunto'])) $this->assunto=$conteudo['assunto'];
		if(isset($conteudo['idioma'])) $this->idioma=$conteudo['idioma'];
		if(isset($conteudo['titulo'])) $this->titulo=$conteudo['titulo'];
		if(isset($conteudo['texto'])) $this->texto=$conteudo['texto'];
		if(isset($conteudo['autor'])) $this->autor=$conteudo['autor'];
		if(isset($conteudo['descricao'])) $this->descricao=$conteudo['descricao'];
		if(isset($conteudo['keywords'])) $this->keywords=$conteudo['keywords'];
		if(isset($conteudo['img_destaque'])) $this->imgDestaque=$conteudo['img_destaque'];
		if(isset($conteudo['url_embed'])) $this->urlEmbed=$conteudo['url_embed'];
		if(isset($conteudo['url_amigavel'])) $this->urlAmigavel=$conteudo['url_amigavel'];
		if(isset($conteudo['ids_relacionados'])) $this->idsRelacionados=$conteudo['ids_relacionados'];
		if(isset($conteudo['data_inclusao'])) $this->dataInclusao=$conteudo['data_inclusao'];
		if(isset($conteudo['data_publicacao'])) $this->dataPublicacao=$conteudo['data_publicacao'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		
		foreach($conteudo as $k=>$news){ $this->oldValues[$k] = $news;} 
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || $this->oldValues['id_secao'] != $this->idSecao){ $campo[] = 'id_secao';  $valor[] = "'$this->idSecao'"; }
		if(!$this->id || $this->oldValues['id_conteudo'] != $this->idConteudo){ $campo[] = 'id_conteudo';  $valor[] = "'$this->idConteudo'"; }
		if(!$this->id || $this->oldValues['assunto'] != $this->assunto){ $campo[] = 'assunto';  $valor[] = "'$this->assunto'"; }
		if(!$this->id || $this->oldValues['idioma'] != $this->idioma){ $campo[] = 'idioma';  $valor[] = "'$this->idioma'"; }
		if(!$this->id || $this->oldValues['titulo'] != $this->titulo){ $campo[] = 'titulo';  $valor[] = "'$this->titulo'"; }
		if(!$this->id || $this->oldValues['texto'] != $this->texto){ $campo[] = 'texto';  $valor[] = "'$this->texto'"; }
		if(!$this->id || $this->oldValues['autor'] != $this->autor){ $campo[] = 'autor';  $valor[] = "'$this->autor'"; }
		if(!$this->id || $this->oldValues['descricao'] != $this->descricao){ $campo[] = 'descricao';  $valor[] = "'$this->descricao'"; }
		if(!$this->id || $this->oldValues['keywords'] != $this->keywords){ $campo[] = 'keywords';  $valor[] = "'$this->keywords'"; }
		if(!$this->id || $this->oldValues['img_destaque'] != $this->imgDestaque){ $campo[] = 'img_destaque';  $valor[] = "'$this->imgDestaque'"; }
		if(!$this->id || $this->oldValues['url_embed'] != $this->urlEmbed){ $campo[] = 'url_embed';  $valor[] = "'$this->urlEmbed'"; }
		if(!$this->id || $this->oldValues['url_amigavel'] != $this->urlAmigavel){ $campo[] = 'url_amigavel';  $valor[] = "'$this->urlAmigavel'"; }
		if(!$this->id || $this->oldValues['ids_relacionados'] != $this->idsRelacionados){ $campo[] = 'ids_relacionados';  $valor[] = "'$this->idsRelacionados'"; }
		if(!$this->id || $this->oldValues['data_inclusao'] != $this->dataInclusao){ $campo[] = 'data_inclusao';  $valor[] = "'$this->dataInclusao'"; }
		if(!$this->id || $this->oldValues['data_publicacao'] != $this->dataPublicacao){ $campo[] = 'data_publicacao';  $valor[] = "'$this->dataPublicacao'"; }
		if(!$this->id || $this->oldValues['status'] != $this->status){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		
		
		$db=new DB('tbl_versao_conteudo');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return VersaoConteudo::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_versao_conteudo');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($idSecao='',$idConteudo='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_versao_conteudo');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		if(strlen($idConteudo)>0){
			if($where) $where .= " AND ";
			$where .= "id_conteudo = '$idConteudo'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new VersaoConteudo($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		//$obj = VersaoConteudo::listar("","",$campos,"id = $id");
                $obj = VersaoConteudo::listar("","",$campos,"id = $id",'','1');
		return isset($obj[0])?$obj[0]:null;
	}

	public static function listarXML($idSecao='',$idConteudo='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_versao_conteudo');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		if(strlen($idConteudo)>0){
			if($where) $where .= " AND ";
			$where .= "id_conteudo = '$idConteudo'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<VersaoConteudo id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</VersaoConteudo>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($idSecao='',$idConteudo='',$where=''){
		$db=new DB('tbl_versao_conteudo');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		if(strlen($idConteudo)>0){
			if($where) $where .= " AND ";
			$where .= "id_conteudo = '$idConteudo'";	
		}
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}
        
	public static function getIdCategoriasComArqs($idSecao='',$idConteudo='',$where=''){
		$db=new DB('tbl_versao_conteudo');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		if(strlen($idConteudo)>0){
			if($where) $where .= " AND ";
			$where .= "id_conteudo = '$idConteudo'";	
		}	
                $strWhere = ($where!='')?"WHERE $where":'';

                $db->executaQuery("SELECT assunto , count(assunto) qnt FROM tbl_versao_conteudo $strWhere GROUP BY assunto");                
                self::setLogSql($db->log);
     
		$lista = array();	
		while($row = $db->fetchArray()){
                    if($row["qnt"] > 0) $lista[] = $row["assunto"];
		}		
		return $lista;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao($this->getIdSecao());//constante pertinente � classe
		$hst->setIdUsuario(getSession('idUsuarioSapo'));
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao($this->titulo);//observa��o pertinente � classe
		$hst->salvar();
	}

}
?>