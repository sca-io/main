<?
class EmailsEnviados{
	private $id;
	private $idSecao;
	private $idConteudo;
	private $nomeRemetente;
	private $emailRemetente;
	private $nomeDestinatario;
	private $emailDestinatario;
	private $assunto;
	private $texto;
	private $dataInclusao;
	private $status;
	public static $logSql;
	private $oldValues = Array('id'=>'','id_secao'=>'','id_conteudo'=>'','nome_remetente'=>'','email_remetente'=>'','nome_destinatario'=>'','email_destinatario'=>'','assunto'=>'','texto'=>'','data_inclusao'=>'','status'=>'');

	public function setId($inId){
		$this->id = $inId;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdSecao($inIdSecao){
		$this->idSecao = $inIdSecao;
	}

	public function getIdSecao(){
		return $this->idSecao;
	}

	public function setIdConteudo($inIdConteudo){
		$this->idConteudo = $inIdConteudo;
	}

	public function getIdConteudo(){
		return $this->idConteudo;
	}

	public function setNomeRemetente($inNomeRemetente){
		$this->nomeRemetente = $inNomeRemetente;
	}

	public function getNomeRemetente(){
		return $this->nomeRemetente;
	}

	public function setEmailRemetente($inEmailRemetente){
		$this->emailRemetente = $inEmailRemetente;
	}

	public function getEmailRemetente(){
		return $this->emailRemetente;
	}

	public function setNomeDestinatario($inNomeDestinatario){
		$this->nomeDestinatario = $inNomeDestinatario;
	}

	public function getNomeDestinatario(){
		return $this->nomeDestinatario;
	}

	public function setEmailDestinatario($inEmailDestinatario){
		$this->emailDestinatario = $inEmailDestinatario;
	}

	public function getEmailDestinatario(){
		return $this->emailDestinatario;
	}

	public function setAssunto($inAssunto){
		$this->assunto = $inAssunto;
	}

	public function getAssunto(){
		return $this->assunto;
	}

	public function setTexto($inTexto){
		$this->texto = $inTexto;
	}

	public function getTexto(){
		return $this->texto;
	}

	public function setDataInclusao($inDataInclusao){
		$this->dataInclusao = $inDataInclusao;
	}

	public function getDataInclusao(){
		return $this->dataInclusao;
	}

	public function setStatus($inStatus){
		$this->status = $inStatus;
	}

	public function getStatus(){
		return $this->status;
	}

	public function __construct($conteudo=''){
		$this->db=new DB('tbl_emails_enviados');
		if(!is_array($conteudo)){
			$conteudo = Array();
		}
		if(isset($conteudo['id'])) $this->id=$conteudo['id'];
		if(isset($conteudo['id_secao'])) $this->idSecao=$conteudo['id_secao'];
		if(isset($conteudo['id_conteudo'])) $this->idConteudo=$conteudo['id_conteudo'];
		if(isset($conteudo['nome_remetente'])) $this->nomeRemetente=$conteudo['nome_remetente'];
		if(isset($conteudo['email_remetente'])) $this->emailRemetente=$conteudo['email_remetente'];
		if(isset($conteudo['nome_destinatario'])) $this->nomeDestinatario=$conteudo['nome_destinatario'];
		if(isset($conteudo['email_destinatario'])) $this->emailDestinatario=$conteudo['email_destinatario'];
		if(isset($conteudo['assunto'])) $this->assunto=$conteudo['assunto'];
		if(isset($conteudo['texto'])) $this->texto=$conteudo['texto'];
		if(isset($conteudo['data_inclusao'])) $this->dataInclusao=$conteudo['data_inclusao'];
		if(isset($conteudo['status'])) $this->status=$conteudo['status'];
		
		foreach($conteudo as $k=>$news){ $this->oldValues[$k] = $news;} 
	}

	public static function setLogSql($valor){
		
			self::$logSql = $valor;
		
	}

	public static function getLogSql(){
		return self::$logSql;
	}

	public function salvar(){
		
		$campo = array();
		$valor = array();
		if(!$this->id || $this->oldValues['id_secao'] != $this->idSecao){ $campo[] = 'id_secao';  $valor[] = "'$this->idSecao'"; }
		if(!$this->id || $this->oldValues['id_conteudo'] != $this->idConteudo){ $campo[] = 'id_conteudo';  $valor[] = "'$this->idConteudo'"; }
		if(!$this->id || $this->oldValues['nome_remetente'] != $this->nomeRemetente){ $campo[] = 'nome_remetente';  $valor[] = "'$this->nomeRemetente'"; }
		if(!$this->id || $this->oldValues['email_remetente'] != $this->emailRemetente){ $campo[] = 'email_remetente';  $valor[] = "'$this->emailRemetente'"; }
		if(!$this->id || $this->oldValues['nome_destinatario'] != $this->nomeDestinatario){ $campo[] = 'nome_destinatario';  $valor[] = "'$this->nomeDestinatario'"; }
		if(!$this->id || $this->oldValues['email_destinatario'] != $this->emailDestinatario){ $campo[] = 'email_destinatario';  $valor[] = "'$this->emailDestinatario'"; }
		if(!$this->id || $this->oldValues['assunto'] != $this->assunto){ $campo[] = 'assunto';  $valor[] = "'$this->assunto'"; }
		if(!$this->id || $this->oldValues['texto'] != $this->texto){ $campo[] = 'texto';  $valor[] = "'$this->texto'"; }
		if(!$this->id || $this->oldValues['data_inclusao'] != $this->dataInclusao){ $campo[] = 'data_inclusao';  $valor[] = "'$this->dataInclusao'"; }
		if(!$this->id || $this->oldValues['status'] != $this->status){ $campo[] = 'status';  $valor[] = "'$this->status'"; }
		
		
		$db=new DB('tbl_emails_enviados');
		if(!$this->id){
			$db->insert($campo,$valor);
			$this->id =  $db->insertId;
		}else{
			$db->update($campo,$valor,"id = '$this->id'");
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return true;
	}

	public function excluir(){
		return EmailsEnviados::delete($this->id);
	}

	public static function delete($id){
		$db=new DB('tbl_emails_enviados');
		if(trim(strlen($id)>0)){
			$db->delete("id = '$id'");
		}
		return true;
	}

	public static function listar($idSecao='',$idConteudo='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_emails_enviados');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		if(strlen($idConteudo)>0){
			if($where) $where .= " AND ";
			$where .= "id_conteudo = '$idConteudo'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);	
		$lista = array();	
		while($conteudo = $db->fetchArray()){			
			$lista[] = new EmailsEnviados($conteudo);
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function ler($id='',$campos='*'){
		$obj = EmailsEnviados::listar("","",$campos,"id = '$id'",'','1');
		return isset($obj[0])?$obj[0]:null;
	}

	public static function listarXML($idSecao='',$idConteudo='',$campos='*',$where='',$ordem='',$paginacao=''){
		$db=new DB('tbl_emails_enviados');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		if(strlen($idConteudo)>0){
			if($where) $where .= " AND ";
			$where .= "id_conteudo = '$idConteudo'";	
		}
		$db->select($campos,$where,$ordem,$paginacao);		
		while($conteudo = $db->fetchArray()){
			$lista .= "<EmailsEnviados id='$conteudo[id]'>";
			for($i = 0; $i < count($conteudo)/2; $i++ ){				
				$n = mysql_field_name($db->result,$i);
				$valor = (strlen($conteudo[$n]))?$conteudo[$n]:" ";	
				$lista .= "<$n id='$conteudo[id]'>$valor</$n>";				
			}
			$lista .= "</EmailsEnviados>";
		}
		self::setLogSql($db->log);
		//$db->printLog();
		return $lista;
	}

	public static function countListar($idSecao='',$idConteudo='',$where=''){
		$db=new DB('tbl_emails_enviados');
						
		if(strlen($idSecao)>0){
			if($where) $where .= " AND ";
			$where .= "id_secao = '$idSecao'";	
		}				
		if(strlen($idConteudo)>0){
			if($where) $where .= " AND ";
			$where .= "id_conteudo = '$idConteudo'";	
		}
		$res = $db->nRegistros($where); 
		self::setLogSql($db->log);
		//$db->printLog();
		return $res;
	}

	public function inserirHistorico($idUsuario,$ip,$acaoHistorico){
		$hst = new Historico();
		$hst->setIdAcaoHistorico($acaoHistorico);
		$hst->setStatus(ATIVO);
		$hst->setIdConteudo($this->getId());
		$hst->setIdSecao();//constante pertinente à classe
		$hst->setIdUsuario($idUsuario);
		$hst->setData(date("Y-m-d H:i:s "));
		$hst->setIp($ip);
		$hst->setObservacao();//observação pertinente à classe
		$hst->salvar();
	}

}
?>