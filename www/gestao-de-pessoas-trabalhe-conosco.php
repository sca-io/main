<? require_once ('conf.php');
$idSecao = GESTAO_DE_PESSOAS;
$idioma = getIdioma();

$nome_secao = "";
$nome_secao_atual = "";
$brad_crumb = "";
$url_secao = DIRETORIO_RAIZ."gestao-de-pessoas/trabalhe-conosco/";
if($idSecao){   
    if(isset($settSecoes[$idSecao])) $nome_secao = getNomeSecao($idSecao);
    $brad_crumb = $nome_secao;
    $brad_crumb = "<strong>".$brad_crumb."</strong>";	
    $brad_crumb = '<a href="'.$url_secao.'">'.$brad_crumb.'</a>';
    $nome_secao_atual = $nome_secao;

}
$metaTitle = $nome_secao;

?><? include "includes/head.php"?>
<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <?=$brad_crumb?>
            </div>
        </div>
        <div class="empresa gestao-de-pessoas">
            <div class="esquerda">
            	<h2><img src="<?=DIRETORIO_RAIZ?>css/img/tit-trabalhe-conosco<?=getTextoByLang("", "-eng")?>.png" alt="<?=$nome_secao_atual?>" /> </h2>
                <h4><?=getTextoByLang("Os campos com asterisco (*) são de preenchimento obrigatório:", "Fields with an asterisk (*) are required:")?></h4>
                <div class="formulario">
                	<div>
                        <form id="formulario" action="">                            
                            <p class="hidden"><input type="hidden" id="idioma" name="idioma" value="<?=$idioma?>" /></p>
                            <div>
                            	<label id="lbl-nome">*<?=getTextoByLang("Nome completo","Full name")?></label>
                                <input id="nome" name="nome" type="text" size="63" class="padrao1" maxlength="255" />
                            </div>   
                            
                            <div>
                            	<span>
                                    <label id="lbl-endereco">*<?=getTextoByLang("Endereço","Address")?></label>
                                    <input id="endereco" name="endereco" type="text" size="55" class="padrao1" maxlength="255" />
                                </span>
                                <span>
                                    <label id="lbl-num">*<?=getTextoByLang("Número","Number")?></label>
                                    <input id="num" name="num" type="text" size="3" maxlength="10" />
                                </span>
                                <span>
                                    <label id="lbl-comp">&nbsp;&nbsp;<?=getTextoByLang("Complemento","Complement")?></label>                                
                                    <input id="comp" name="comp" type="text" size="20" maxlength="255" />
                                </span>
                            </div>
                            <div>
                            	<span>
                                    <label id="lbl-cidade">*<?=getTextoByLang("Cidade","City")?></label>
                                    <input id="cidade" name="cidade" type="text" size="35" class="padrao1" maxlength="255" />
                                </span>
                                <span>
                                    <label id="lbl-estado">*<?=getTextoByLang("Estado","State")?></label>
                                    <input class="mask-uf" id="estado" name="estado" type="text" size="8" maxlength="100" />
                                </span>
                            </div>
                            <div>
                            	<span>
                                    <label id="lbl-telefone">*<?=getTextoByLang("Telefone","Phone")?></label>
                                    <input class="mask-ddd" id="ddd" name="ddd" type="text" size="2" maxlength="2" onkeyup="onCompleteFocusTo(this,'telefone')" />
                                </span>
                                <span>
                                    <label>&nbsp;</label>
                                    <input id="telefone" name="telefone" type="text" size="15" class="padrao2 mask-tel" />
                                </span>
                                <span>
                                    <label id="lbl-email">*<?=getTextoByLang("E-mail","E-mail")?></label>                                
                                    <input id="email" name="email" type="text" size="50" maxlength="255" />
                                </span>
                            </div>
                            <div>
                            	<span>
                                    <label id="lbl-area">*<?=getTextoByLang("Área de Interesse","Area Of Interest")?></label>
                                    <input id="area" name="area" type="text" size="44" class="padrao1" maxlength="255" />
                                </span>
                                <span>
                                    <label id="lbl-outros">&nbsp;&nbsp;<?=getTextoByLang("Outros","Others")?></label>
                                    <input id="outros" name="outros" type="text" size="50" maxlength="255" />
                                </span>
                            </div>
                            <div>
                            	<span>
                                    <label id="lbl-formacao-acad">*<?=getTextoByLang("Formação Acadêmica","Academic")?></label>
                                    <? /*<input id="formacaoAcad" name="formacaoAcad" type="text" size="41" class="padrao1" maxlength="255" />*/?>
                                    <select id="formacaoAcad" name="formacaoAcad">
                                        <option value=""><?=getTextoByLang("Selecione","Select")?></option>    
                                        <?=geraOptionsFormAcad2("",$idioma); ?>
                                    </select>
                                </span>
                                <span>
                                    <label id="lbl-curso">&nbsp;&nbsp;<?=getTextoByLang("Caso tenha selecionado Superior ou Pós Graduação, especifique o curso abaixo","If you select Top or Graduate, specify the course below")?></label>
                                    <input id="curso" name="curso" type="text" size="71" maxlength="255" />
                                </span>
                            </div>
                            <div>
                            	<span>
                                    <label id="lbl-curso-atual">&nbsp;&nbsp;<?=getTextoByLang("Cursos de Atualização","Refresher Courses")?></label>
                                    <input id="cursoAtual" name="cursoAtual" type="text" size="41" class="padrao1" maxlength="255" />
                                </span>
                                <span>
                                    <label id="lbl-idiomas">&nbsp;&nbsp;<?=getTextoByLang("Idiomas – Especifique qual o nível (básico, intermediário ou fluente)","Language - Select the level (basic, intermediate or fluent)")?></label>
                                    <input id="idiomas" name="idiomas" type="text" class="tipo-idioma" maxlength="255" />
                                </span>
                            </div>
                            <div>
                            	<label id="lbl-dados-profiss">&nbsp;&nbsp;<?=getTextoByLang("Dados profissionais (últimas empresas em que trabalhou e principais atividades exercidas)","Professional details (company you worked last and main activities performed)")?></label>
                                <textarea id="dadosProfiss" name="dadosProfiss" cols="" rows="" ></textarea>                                
                            </div>
                        </form>
                        <a href="javascript:salvarCadGestaoPessoa();" class="bt-enviar<?=getTextoByLang("","-eng")?>" title="<?=getTextoByLang("Enviar","Submit")?>"><?=getTextoByLang("Enviar","Submit")?></a>
                    </div>                    
                </div>
                
                <a href="javascript:history.back();" class="bt-voltar" title="<?=getTextoByLang("Voltar","Return")?>"> &lt; <?=getTextoByLang("Voltar","Return")?></a>
                
            </div>
                        
        </div>
      </div>
    </div>
    <? include "includes/rodape.php"?>
    <script type="text/javascript">
        /*** Mascaras dos campos ***/
        $(".mask-ddd").mask("99");
        $(".mask-uf").mask("aa");
        $(".mask-tel").mask("9999-9999");
        /*** CONSTANTES DE FORMACAO ACADEMICA ***/
        //PT
        var FA_1_GRAU_INCOM = 1;
        var FA_1_GRAU_COM = 2;    
        var FA_2_GRAU_INCOM = 3;
        var FA_2_GRAU_COM = 4; 
        var FA_SUPERIOR_INCOM = 5;
        var FA_SUPERIOR_COM = 6;
        var FA_POS_GRAD = 7;    

        //EN
          var ELEMENTARY_SCHOOL_INCOM = 1;
          var ELEMENTARY_SCHOOL_COM = 2;
          var MIDDLE_SCHOOL_INCOM = 3;    
          var MIDDLE_SCHOOL_COM = 4;   
          var HIGH_SCHOOL_INCOM = 5;
          var HIGH_SCHOOL_COM = 6;
          var HIGHER_EDUCATION_INCOM = 7; 
          var HIGHER_EDUCATION_COM = 8; 
          var GRADUATE_STUDIES = 9;

        /****************************************/
    </script>
</div>
</body>
</html>