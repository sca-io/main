<? require_once ('conf.php');
$idioma = getIdioma();
$idSecao = request('idSecao');
$idSubSecao = request('idSubSecao');
$assunto = request("ass");
$url_amig = request("url_amig");
$idDestaque = '';
$idSecaoCh = $idSubSecao?$idSubSecao:$idSecao;

$textoInicial  = "";
$where = "idioma = $idioma AND status = ".Chamada::PUBLICADO."";
$listaChs = Chamada::listar($idSecaoCh,$idDestaque,"id,titulo,texto",$where," data_publicacao DESC","1");
$chamada = null;
if($listaChs){
    $chamada = $listaChs[0];    
} 
if($chamada){
    $textoInicial = $chamada->getTexto();
    
}
//print Chamada::getLogSql();
$idSecaoSel = 0;
$nome_secao_atual = "";
if($idSecao){
    $url_secao = DIRETORIO_RAIZ."etanol/";   
    if(isset($settSecoes[$idSecao])) $nome_secao = getNomeSecao($idSecao);
    $brad_crumb = '<a href="'.$url_secao.'">'.$nome_secao.'</a>';
    $nome_img_tit = $nome_secao;
    $idSecaoSel = $idSecao;
    $url_self = $url_secao;
    $nome_secao_atual = $nome_secao;
}

if($idSubSecao){
    $nome_sub_secao = getNomeSecao($idSecao,$idSubSecao);
    $brad_crumb .= '&nbsp;&nbsp;/&nbsp;<a href="javascript:;"><strong>'.$nome_sub_secao.'</strong></a>'; 
    $img_tit_secao = DIRETORIO_RAIZ."css/img/tit-".setUrlAmigavel(getNomeSecao($idSecao,$idSubSecao,"",LNG_PT)).  getTextoByLang("", "-eng").".png";
    $url_self = $url_self."certificacoes/";
    $idSecaoSel = $idSubSecao;
    $nome_secao_atual = $nome_sub_secao;
}
$where = "status = ".VersaoConteudo::PUBLICADO." AND idioma = '$idioma'";
$categoriasComArq  = VersaoConteudo::getIdCategoriasComArqs($idSecaoSel,"",$where);


?><? include "includes/head.php"?>
<body>
<div id="sca">
	<div class="container">
      <div class="conteudo">
      	<? include "includes/topo.php"?>
        <div class="breadcrumb">
            <div>
                <?=$brad_crumb?>
            </div>
        </div>
        <div class="etanol legislacoes certificacoes">
            <div class="esquerda">
            	<div class="txt-box">
                    <div>
                    <h2 class="tit-empresa"><img src="<?=$img_tit_secao?>" alt="<?=$nome_secao_atual?>" /> </h2>                       
                        <?=response_html($textoInicial)?> 
                        <? include "includes/filtro-assunto.php"?>
                    </div>
                </div>
                <?
                $listaConts = null; 
                if($url_amig || $assunto):                   
                    $idAssunto = 0;
                    if($url_amig){
                        /*** Rever este fluxo ***/
                        $listaAs = Assunto::listar($idSecaoSel,"id,nome", "status = ".ATIVO." AND idioma = $idioma", "data DESC");  
                        foreach($listaAs as $as){
                            if($url_amig == setUrlAmigavel($as->getNome())) $idAssunto = $as->getId();
                        }
                        /**********************/
                    }
                    if($idAssunto){
                        $where .= " AND assunto = '$idAssunto'";
                        //if($url_amig) $where .= " AND url_amigavel = '$url_amig'";
                        $ncadastro =  VersaoConteudo::countListar($idSecaoSel,"",$where);

                        $pagina = isset($_REQUEST['pagina'])?$_REQUEST['pagina']:0;
                        $total_reg_pag = 10;
                        $init_reg_pag = ($pagina * $total_reg_pag);	 
                        $aux_pagina = $pagina + 1;
                        $total_paginas = $ncadastro/$total_reg_pag;         

                        $listaConts = VersaoConteudo::listar($idSecaoSel,"","id,assunto,titulo,texto",$where,"data_publicacao DESC");  
                    }

                    ?>
                    <div class="texto">
                        <? if($listaConts):?>
                            <? foreach($listaConts as $cont):?>
                                <? $objAss = Assunto::ler($cont->getAssunto());
                                // print Assunto::getLogSql();
                                    $nomeAs = $objAss->getNome();
                                ?>
                                <h3><?=response_html($nomeAs)?></h3>
                                <?=response_html($cont->getTexto())?>  
                            <? endforeach;?>
                        <? else:?>
                            <? if($assunto):?><p><br /><?=getTextoByLang("Nenhum documento encontrado!","No documents found!");?></p> <? endif;?>
                        <? endif;?>
                    </div>
                <? endif;?>
                <? //include "includes/paginacao.php"?>    
                <a href="javascript:history.back()" class="bt-voltar" title="<?=getTextoByLang("Voltar","Return")?>"> &lt; <?=getTextoByLang("Voltar","Return")?></a> 
            </div>
            <? include "includes/direita.php"?>

                        
        </div>
      </div>
    </div>
    <? include "includes/rodape.php"?>
</div>
</body>
</html>